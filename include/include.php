<?php
// error_reporting(E_ALL);
// ini_set('display_errors',1);   
require('config.php');
Class iWMS extends iWMSExt1{
	//MSSQL
	//var $serverConn = array("S1" => array("10.128.1.15","sa","specctypera"), "S2" => array("wawi.jsis.com.sg","sa","specctypera"), "S3" => array("10.128.1.3","sa","express14"), "S4" => array("10.128.1.7","sa","express14"));
	
	var $link = "None"; 
	var $AuditTrail = array();
	
	function __construct($userid){
		//parent::__construct();
		$this->AuditTrail['user_id'] = $userid;
		$this->AuditTrail['ip'] = $_SERVER['REMOTE_ADDR'];
		$this->AuditTrail['browser'] = $this->DetectBrowser();
		$this->MyConnection("S3");
		
	} 
	
	function MyConnection($ctype){
		if ($this->link == "None"){
			
			$serverConn = unserialize(base64_decode(SQL_CONN));
			
			$this->link = mssql_connect($serverConn[0], $serverConn[1], $serverConn[2]);
			$this->AuditTrail['action'] = "Making connection to server";
			if (!$this->link) {
				$this->AuditTrail['remark'] = "Unable to connect to server: ".$ctype;
				$this->AddToAuditTrailToFile();
			} else {
				$this->AuditTrail['remark'] = "Successful connection to server: ".$ctype;
				$this->AddToAuditTrail();
			}
		}
	}
	
	function CloseMyConnection(){
		if ($this->link){
			$this->AuditTrail['action'] = "Closing mssql connection";
			$this->AuditTrail['remark'] = (mssql_close($this->link)) ? "Success": "Failed";
			$this->AddToAuditTrail();
			//mssql_close($this->link);
			$this->link = "None";
		}
	}

	function AddToAuditTrail(){
		if (AuditTrailDB){
			mssql_select_db(DefaultDB);
			mssql_query("insert into AuditTrail (user_id,ip,browser,action,remarks) values ('".$this->AuditTrail['user_id']."','".$this->AuditTrail['ip']."','".$this->AuditTrail['browser']."','".$this->AuditTrail['action']."','".$this->AuditTrail['remark']."')");
		}
		if (AuditTrailTxt){
			$this->AddToAuditTrailToFile();
		}
	}
	
	function AddToAuditTrailToFile(){	
		$myFile = "lastresort/logs.".date("Ymd").".txt";
		$fh = fopen($myFile, 'a+');
		$stringData = "[Date: ".date("Y-m-d H:i:s")."]\t[UserID:".$this->AuditTrail['user_id']."]\t[IP:".$this->AuditTrail['ip']."]\t[Browser:".$this->AuditTrail['browser']."]\t[Action:".$this->AuditTrail['action']."]\t[Remarks:".$this->AuditTrail['remark']."]\n";
		fwrite($fh, $stringData);
		fclose($fh);
	}
	
	function GetWawiCustomer($filter){
		mssql_select_db(DefaultDB);
		
		//$filter = isset($filter) && $filter != '0' ? 'and ID = '.$filter : 'ID !=0';
		//$filter = ($filter) ? 'and ID='.$filter: '';
	
		$this->AuditTrail['action'] = "GetWawiCustomer $filter"; 
		$sql = mssql_query("select ID,WawiIndex,DBName,GST,STG from WawiCustomer where [status] = 1 and ID=".$filter." order by WawiIndex ASC;");

		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		} else {	
			$this->AuditTrail['remark'] = "No Data";
			$data = 'no data'; 
		}
		
		$this->AddToAuditTrail();
		
		//$this->CloseMyConnection();
		
		return $data;
	}
	
	function GetWawiASReportList($WawiID){
		mssql_select_db(DefaultDB);
		
		$this->AuditTrail['action'] = "GetWawiASReportList $WawiID";
		
		$sql = mssql_query("SELECT DBName FROM WawiCustomer WHERE ID=$WawiID AND [status]=1;");
		
		if(mssql_num_rows($sql) > 0){
			$data = mssql_fetch_assoc($sql);
			
			$WawiDB = $data['DBName'];
			
			$sql = mssql_query("select * from ASReportsList where WawiID=$WawiID and [Status]=1");
			
			if(mssql_num_rows($sql) > 0){
				
				$data = '';
				
				while($row[] = mssql_fetch_assoc($sql)){}
				
				mssql_select_db($WawiDB);
				$j=0;
				for ($i=0;$i<count($row);$i++){
					
					$sql = "select Column_Name,DATA_TYPE from Information_schema.Columns with (nolock) where Table_Name like '".$row[$i]['ReportColumns']."' order by Column_Name";
					$getTableStructure = mssql_query($sql);
					
					if (mssql_num_rows($getTableStructure) > 0){
						$data[$j]['ReportColumns'] = $row[$i]['ReportColumns'];
						$data[$j++]['ReportName'] = urlencode($row[$i]['ReportName']);
					}
				}
				
				$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
			}else{
				$this->AuditTrail['remark'] = "No Data";
				$data = 'no data';
			}

		} else {	
			$this->AuditTrail['remark'] = "No Data";
			$data = 'no data';
		}
		
		
		
		$this->AddToAuditTrail();
		//$this->CloseMyConnection();
		
		return $data;
	}
	
	function GetASFilterFields($WawiID,$view){
		mssql_select_db(DefaultDB);
		
		$this->AuditTrail['action'] = "GetASFilterFields $WawiID,$view";
		
		$sql = mssql_query("select * from ASReportFilters where wawiid=$WawiID and viewname='$view' and [status]=1 order by orderlist asc;");
		
		if (mssql_num_rows($sql) > 0){ 
			$data = '';
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			
			$this->AuditTrail['remark'] = "Row count: ".count($data);
			
		}else{
			
			$this->AuditTrail['remark'] = "No Data";
			$data = 'no data';
			
		}
		
		$this->AddToAuditTrail();
		
		//$this->CloseMyConnection();
		
		return $data;
	}
	
	function GetASReportData($WawiID,$view,$field,$filter,$reportname,$range_from,$range_to,$sort_by,$sort_type){

		mssql_select_db(DefaultDB);
		
		$this->AuditTrail['action'] = "GetASReportData $WawiID,$view,$field,$filter,$reportname,$range_from,$range_to,$sort_by,$sort_type";
		
		$sql = mssql_query("SELECT DBName,WawiIndex FROM WawiCustomer WHERE ID=$WawiID AND [status]=1;");
		
		if(mssql_num_rows($sql) > 0){
			$data = mssql_fetch_assoc($sql);
			$WawiDB = $data['DBName'];
			$WawiIndex = $data['DBName'];
			
			$filterval = trim($filter);
			$range_from_val = trim($range_from);
			$range_to_val = trim($range_to);
			
			if ($filterval == ""){
				$params = "1=1";
				if ($range_from_val != '' && $range_to_val != ''){
					$params .= " AND [".$field."] >= '".$range_from_val."' AND [".$field."] <= '".$range_to_val."'";
				}
			} else {
				if (substr_count($filterval, '/') == 2){
					$params = "[".$field."] = '".$filterval."'";
				}elseif(is_numeric($filterval)){
					$params = "[".$field."] = '".$filterval."'";
				}
				else{
					$params = "[".$field."] like '".$filterval."'";
				}
			}
			
			$params .= ($sort_by!='') ? " ORDER BY [".$sort_by."] ".$sort_type: "";
			
			$sqlStatement = "execute ASMainReport2 $WawiID,\"".$WawiDB."\",\"".$reportname."\",\" where $params\"";
			
			$sql = mssql_query($sqlStatement);
			
			if ($sql && mssql_num_rows($sql)){

				$TotalRows = mssql_num_rows($sql);
				
				if ($TotalRows < 1){
					$this->AuditTrail['remark'] = "No Data";
					$return = 'no data';
				}else if ($TotalRows > ReportLimit){
					$params = '';
				
					$params = array("filter" => $filter, "range_from" => $range_from, "range_to" => $range_to, "field" => $field, "sort_by" => $sort_by, "sort_type" => $sort_type, "DBName" => $WawiDB, "WawiID" => $WawiID, "WawiIndex" => $WawiIndex, "reportname" => $reportname, "rptType" => 3);
			
					if ($this->RequestEmailReport($params,1)=='success'){
						$return = $TotalRows;
						$this->AuditTrail['remark'] = 'exceed limit-request email';
					}else{
						$return = 'exceed limit-error';
						$this->AuditTrail['remark'] = 'exceed limit-error';
					}
					
					$this->AuditTrail['remark'] = "Exceed report row limit[".ReportLimit."] -> ".$TotalRows;
					
				}else{
					while($return[] = mssql_fetch_assoc($sql));
					array_pop($return);
					$this->AuditTrail['remark'] = "Successful filter row count: ".count($return);
				}
				
			}else{
				$return = 'no data';
			}

		}else{
			$this->AuditTrail['remark'] = "no db";
			$return = 'no db';
		}
		
		$this->AddToAuditTrail();
		
		return $return;
	}
	
	
	
	function GetWawiReportList($WawiID){
		mssql_select_db(DefaultDB);
		
		$this->AuditTrail['action'] = "GetWawiReportList $WawiID";
		
		$sql = mssql_query("SELECT DBName FROM WawiCustomer WHERE ID=$WawiID AND [status]=1;");
		
		if(mssql_num_rows($sql) > 0){
			$data = mssql_fetch_assoc($sql);
			
			$WawiDB = $data['DBName'];
			
			$sql = mssql_query("select * from ReportsList where WawiID=$WawiID and [Status]=1");
			
			if(mssql_num_rows($sql) > 0){
				
				$data = '';
				
				while($row[] = mssql_fetch_assoc($sql)){}
				
				mssql_select_db($WawiDB);
				$j=0;
				for ($i=0;$i<count($row);$i++){
					
					$sql = "select Column_Name,DATA_TYPE from Information_schema.Columns with (nolock) where Table_Name like '".$row[$i]['ReportColumns']."' order by Column_Name";
					$getTableStructure = mssql_query($sql);
					
					if (mssql_num_rows($getTableStructure) > 0){
						$data[$j]['ReportColumns'] = $row[$i]['ReportColumns'];
						$data[$j++]['ReportName'] = urlencode($row[$i]['ReportName']);
					}
				}
				
				$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
			}else{
				$this->AuditTrail['remark'] = "No Data";
				$data = 'no data';
			}

		} else {	
			$this->AuditTrail['remark'] = "No Data";
			$data = 'no data';
		}
		
		
		
		$this->AddToAuditTrail();
		//$this->CloseMyConnection();
		
		return $data;
	}
	
	function GetFilterFields($WawiID,$view){
		mssql_select_db(DefaultDB);
		
		$this->AuditTrail['action'] = "GetFilterFields $WawiID,$view";
		
		$sql = mssql_query("select * from ReportFilters where wawiid=$WawiID and viewname='$view' and [status]=1 order by orderlist asc;");
		
		if (mssql_num_rows($sql) > 0){ 
			$data = '';
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			
			$this->AuditTrail['remark'] = "Row count: ".count($data);
			
		}else{
			
			$this->AuditTrail['remark'] = "No Data";
			$data = 'no data';
			
		}
		
		$this->AddToAuditTrail();
		
		//$this->CloseMyConnection();
		
		return $data;
	}
	
	function GetReportData($WawiID,$view,$field,$filter,$reportname,$range_from,$range_to,$sort_by,$sort_type){

		mssql_select_db(DefaultDB);
		
		$this->AuditTrail['action'] = "GetReportData $WawiID,$view,$field,$filter,$reportname,$range_from,$range_to,$sort_by,$sort_type";
		
		$sql = mssql_query("SELECT DBName,WawiIndex FROM WawiCustomer WHERE ID=$WawiID AND [status]=1;");
		
		if(mssql_num_rows($sql) > 0){
			$data = mssql_fetch_assoc($sql);
			$WawiDB = $data['DBName'];
			$WawiIndex = $data['DBName'];
			
			$filterval = trim($filter);
			$range_from_val = trim($range_from);
			$range_to_val = trim($range_to);
			
			if ($filterval == ""){
				$params = "1=1";
				if ($range_from_val != '' && $range_to_val != ''){
					$params .= " AND [".$field."] >= '".$range_from_val."' AND [".$field."] <= '".$range_to_val."'";
				}
			} else {
				if (substr_count($filterval, '/') == 2){
					$params = "[".$field."] = '".$filterval."'";
				}elseif(is_numeric($filterval)){
					$params = "[".$field."] = '".$filterval."'";
				}
				else{
					$params = "[".$field."] like '".$filterval."'";
				}
			}
			
			$params .= ($sort_by!='') ? " ORDER BY [".$sort_by."] ".$sort_type: "";
			
			$sqlStatement = "execute MainReport2 $WawiID,\"".$WawiDB."\",\"".$reportname."\",\" where $params\"";
			
			$sql = mssql_query($sqlStatement);
			
			if ($sql && mssql_num_rows($sql)){

				$TotalRows = mssql_num_rows($sql);
				
				if ($TotalRows < 1){
					$this->AuditTrail['remark'] = "No Data";
					$return = 'no data';
				}else if ($TotalRows > ReportLimit){
					$params = '';
				
					$params = array("filter" => $filter, "range_from" => $range_from, "range_to" => $range_to, "field" => $field, "sort_by" => $sort_by, "sort_type" => $sort_type, "DBName" => $WawiDB, "WawiID" => $WawiID, "WawiIndex" => $WawiIndex, "reportname" => $reportname, "rptType" => 1);
			
					if ($this->RequestEmailReport($params,1)=='success'){
						$return = $TotalRows;
						$this->AuditTrail['remark'] = 'exceed limit-request email';
					}else{
						$return = 'exceed limit-error';
						$this->AuditTrail['remark'] = 'exceed limit-error';
					}
					
					$this->AuditTrail['remark'] = "Exceed report row limit[".ReportLimit."] -> ".$TotalRows;
					
				}else{
					while($return[] = mssql_fetch_assoc($sql));
					array_pop($return);
					$this->AuditTrail['remark'] = "Successful filter row count: ".count($return);
				}
				
			}else{
				$return = 'no data';
			}

		}else{
			$this->AuditTrail['remark'] = "no db";
			$return = 'no db';
		}
		
		$this->AddToAuditTrail();
		
		return $return;
	}
	
	function GetPicklistInfo($DBName,$PacklistNumber){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "$DBName GetPicklistInfo $PacklistNumber";
		
		$sql = mssql_query("SELECT     dbo.PackListPosition.DocumentNumber, dbo.PackListPosition.PackListNumber, dbo.PackListPosition.Line, dbo.PackListPosition.DocumentLine, 
                      dbo.PackListPosition.KindOfDocument, dbo.PackListPosition.PartNumber, dbo.PackListPosition.Quantity, dbo.PackListPosition.StockIndex, 
                      dbo.PackListPosition.EntryNumber, dbo.PackListPosition.Box, dbo.Shipboxes.Boxnumber AS ShipBoxNumber, dbo.PackListPosition.carton, 
                      dbo.PackListPosition.Consolnumber, dbo.Shipboxes.Boxnumber, CONVERT(varchar(10), dbo.PackList.TimePacked, 111) AS TimePacked, 
                      CONVERT(varchar(10), dbo.PackList.DatePacked, 111) AS DatePacked
FROM         dbo.PackListPosition INNER JOIN
                      dbo.Shipboxes ON dbo.PackListPosition.PackListNumber = dbo.Shipboxes.Packlistnumber AND 
                      dbo.PackListPosition.DocumentNumber = dbo.Shipboxes.Ordernumber AND 
                      dbo.PackListPosition.EntryNumber = dbo.Shipboxes.Entrynumber INNER JOIN
                      dbo.PackList ON dbo.PackListPosition.PackListNumber = dbo.PackList.PackListnumber
WHERE     (dbo.PackListPosition.PackListNumber = ".$PacklistNumber.");");

		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		} else {	
			$this->AuditTrail['remark'] = "No Data";
			$data = 'no data';
		}
		
		$this->AddToAuditTrail();
		
		//$this->CloseMyConnection();
		
		return $data;
	}
	
	function UndoNormalScanOut($DBName,$PacklistNumber){
		mssql_select_db(DefaultDB);
		$this->AuditTrail['action'] = "$DBName UndoNormalScanOut $PacklistNumber";
		//echo "execute UndoNormalScanOut '".$DBName."',".$PacklistNumber.";";
		$result = mssql_fetch_assoc(mssql_query("execute UndoNormalScanOut '".$DBName."',".$PacklistNumber.";"));
			
		if($result['result']=="Success"){
			$data = 'success';
			$this->AuditTrail['remark'] = "Success";
		} else {	
			$this->AuditTrail['remark'] = "Failed";
			$data = 'failed';
		}

		$this->AddToAuditTrail();
		
		//$this->CloseMyConnection();
		
		return $data;
	}
	
	function GetEntryStockIndex($DBName){
		
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "$DBName GetEntryStockIndex";
		$sql = mssql_query("SELECT DISTINCT StockIndex FROM [Entry] ORDER BY StockIndex;");

		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		} else {	
			$this->AuditTrail['remark'] = "No Data";
			$data = 'no data';
		}
		
		$this->AddToAuditTrail();
		
		//$this->CloseMyConnection();
		
		return $data;
	}
	
	function ResetCycleCount($DBName,$StockIndex){
		mssql_select_db(DefaultDB);
		
		$this->AuditTrail['action'] = "$DBName ResetCycleCount $StockIndex";
		
		$result = mssql_fetch_assoc(mssql_query("execute ResetCycleCount '".$DBName."','".$StockIndex."';"));
			
		if($result['result']=="Success"){
			$data = 'success';
			$this->AuditTrail['remark'] = "Success";
		} else {	
			$this->AuditTrail['remark'] = "Failed";
			$data = 'failed';
		}

		$this->AddToAuditTrail();
		
		//$this->CloseMyConnection();
		
		return $data;
	}
	
	function GetLastCycleCount($DBName,$StockIndex){
		mssql_select_db($DBName);
	
		$this->AuditTrail['action'] = "$DBName GetLastCycleCount $StockIndex";
		
		$result = mssql_query("select TOP 1 convert(varchar(10),ScanDate,111) as ScanDate from [Entry] where StockIndex = '".$StockIndex."' and ScanDate IS NOT NULL order by ScanDate desc;");
		
		if($result && mssql_num_rows($result)){
			$temp = mssql_fetch_assoc($result);
			$data  = $temp['ScanDate'];
			$this->AuditTrail['remark'] = $data;
		} else {	
			$this->AuditTrail['remark'] = "no record";
			$data = 'no data';
		}

		$this->AddToAuditTrail();
		
		return $data;
	}
	
	function GetReceivingFields($WawiID){
		mssql_select_db(DefaultDB);
		
		$this->AuditTrail['action'] = "GetReceivingFields: ".$WawiID;
		
		$sql = mssql_query("SELECT * FROM v_ReceivingSetup WHERE WawiID = '".$WawiID."' and FieldStatus=1 Order By OrderList");

		if(mssql_num_rows($sql) > 0){
			$i=0;
			while($data[$i] = mssql_fetch_assoc($sql)){
				if ($data[$i]['ElementType']=='select'){

					if(strtolower($data[$i]["FieldName"])==strtolower('Supplier')){
						$sql2 = mssql_query("SELECT [Index],[Name1] as [Description] FROM ".$data[$i]['DBName'].".dbo.Supplier ORDER BY [Index] ASC;");
						$temp['Index'] = '';
						$temp['Description'] = '-';
						$data[$i]['Options'][]=$temp;
						if(mssql_num_rows($sql2) > 0){
							while($data[$i]['Options'][] = mssql_fetch_assoc($sql2));
							array_pop($data[$i]['Options']);
						}
					}
					
					if(strtolower($data[$i]["FieldName"])==strtolower('StockIndex')){
						$sql2 = mssql_query("SELECT [Index],[Description] FROM ".$data[$i]['DBName'].".dbo.Stocks Order By [Listsort] ASC;");
						$temp['Index'] = '';
						$temp['Description'] = '-';
						$data[$i]['Options'][]=$temp;
						if(mssql_num_rows($sql2) > 0){
							while($data[$i]['Options'][] = mssql_fetch_assoc($sql2));
							array_pop($data[$i]['Options']);
						}
					}
					
					if(strtolower($data[$i]["FieldName"])==strtolower('PackageType')){
						$slq = mssql_query("SELECT PackageType as [Index], [Description] FROM PackageType WHERE WawiID = ".$WawiID." AND [status]=1 Order By PackageType ASC;");
						$temp['Index'] = '';
						$temp['Description'] = '-';
						$data[$i]['Options'][]=$temp;
						if(mssql_num_rows($slq) > 0){
							while($data[$i]['Options'][] = mssql_fetch_assoc($slq));
							array_pop($data[$i]['Options']);
						}
					}					
					
					if(strtolower($data[$i]["FieldName"])==strtolower('ChipFrom')){
						switch($WawiID){
							case 9:
							case 59:
							case 10:
							case 13:
							case 28:
							case 42:
							case 47:
							case 53:
							case 55:
								$slq = mssql_query("SELECT PackageType as [Index], [Description] FROM PackageType WHERE WawiID = ".$WawiID." AND [status]=1 Order By PackageType ASC;");
								$temp['Index'] = '';
								$temp['Description'] = '-';
								$data[$i]['Options'][]=$temp;
								if(mssql_num_rows($slq) > 0){
									while($data[$i]['Options'][] = mssql_fetch_assoc($slq));
									array_pop($data[$i]['Options']);
								}
							break;
							case 56:
							case 57:
								$slq = mssql_query("SELECT [Index], Description FROM ".$data[$i]['DBName'].".dbo.Customergroups"); 
								$temp['Index'] = '';
								$temp['Description'] = '-';
								$data[$i]['Options'][]=$temp;
								if(mssql_num_rows($slq) > 0){
									while($data[$i]['Options'][] = mssql_fetch_assoc($slq));
									array_pop($data[$i]['Options']);
								} 
							break;
						}
					}
					
					if(strtolower($data[$i]["FieldName"])==strtolower('Comment')){
						switch($WawiID){
							case 14:
							case 30:
							case 31:
								$slq = mssql_query("SELECT PackageType as [Index], [Description] FROM PackageType WHERE WawiID = ".$WawiID." AND [status]=1 Order By PackageType ASC;");
								$temp['Index'] = '';
								$temp['Description'] = '-';
								$data[$i]['Options'][]=$temp;
								if(mssql_num_rows($slq) > 0){
									while($data[$i]['Options'][] = mssql_fetch_assoc($slq));
									array_pop($data[$i]['Options']);
								}
							break;
						}
					}
					
					if(strtolower($data[$i]["FieldName"])==strtolower('Seal')){
						switch($WawiID){
							case 19:
							case 37:
								$slq = mssql_query("SELECT PackageType as [Index], [Description] FROM PackageType WHERE WawiID = ".$WawiID." AND [status]=1 Order By PackageType ASC;");
								$temp['Index'] = '';
								$temp['Description'] = '-';
								$data[$i]['Options'][]=$temp;
								if(mssql_num_rows($slq) > 0){
									while($data[$i]['Options'][] = mssql_fetch_assoc($slq));
									array_pop($data[$i]['Options']);
								}
							break;
							
							case 23:
							case 27:
								$slq = mssql_query("SELECT [Index], Name1 AS [Description] FROM ".$data[$i]['DBName'].".dbo.Customer WHERE [Index]<>'' ORDER BY [Index] ASC;");
								$temp['Index'] = '';
								$temp['Description'] = '-';
								$data[$i]['Options'][]=$temp;
								if(mssql_num_rows($slq) > 0){
									while($data[$i]['Options'][] = mssql_fetch_assoc($slq));
									array_pop($data[$i]['Options']);
								}
							break;
						}
					}
					
					if(strtolower($data[$i]["FieldName"])==strtolower('SupplierOrder')){
						switch($WawiID){
							case 9:
							case 59:
								$temp['Index'] = '';
								$temp['Description'] = '-';
								$data[$i]['Options'][]=$temp;
								
								$temp['Index'] = 'INNER';
								$temp['Description'] = 'INNER';
								$data[$i]['Options'][]=$temp;
								
								$temp['Index'] = 'OUTER';
								$temp['Description'] = 'OUTER';
								$data[$i]['Options'][]=$temp;
							break;
							
							case 22:
								$slq = mssql_query("SELECT PackageType as [Index], [Description] FROM PackageType WHERE WawiID = ".$WawiID." AND [status]=1 Order By PackageType ASC;");
								$temp['Index'] = '';
								$temp['Description'] = '-';
								$data[$i]['Options'][]=$temp;
								if(mssql_num_rows($slq) > 0){
									while($data[$i]['Options'][] = mssql_fetch_assoc($slq));
									array_pop($data[$i]['Options']);
								}
							break;
						}
					}
					
					if(strtolower($data[$i]["FieldName"])==strtolower('TaxScheme')){
						$sql3 = mssql_query("SELECT TaxScheme as [Index], [ID] as 'Description' FROM TaxScheme WHERE [status]=1 AND WawiID=$WawiID Order By TaxScheme ASC;");
						$temp['Index'] = '';
						$temp['Description'] = '-';
						$data[$i]['Options'][]=$temp;
						if(mssql_num_rows($sql3) > 0){
							while($data[$i]['Options'][] = mssql_fetch_assoc($sql3));
							array_pop($data[$i]['Options']);
						}

					}
					

					if(strtolower($data[$i]["FieldName"])==strtolower('Silicon')){
						switch($WawiID){
							case 28:
							case 53:
							case 55:
								$temp['Index'] = '';
								$temp['Description'] = '-';
								$data[$i]['Options'][]=$temp;
								
								$temp['Index'] = 'Silicon';
								$temp['Description'] = 'Silicon';
								$data[$i]['Options'][]=$temp;
								
								$temp['Index'] = 'Non-Silicon';
								$temp['Description'] = 'Non-Silicon';
								$data[$i]['Options'][]=$temp;
							break;
						}
					}

					
					if(strtolower($data[$i]["FieldName"])==strtolower('CountryOfOrigin')){
						switch($WawiID){
							case 28:
							case 53:
							case 55:
								$sql3 = mssql_query("SELECT country_code as [Index], country_name as [Description] FROM STGMM.dbo.Country ORDER BY country_name ASC ");
							break;
							default:
								$sql3 = mssql_query("SELECT country_code as [Index], country_name as [Description] FROM STGMM.dbo.Country ORDER BY country_code ASC ");
							break;
						}
						
						//$sql3 = mssql_query("SELECT country_code as [Index], country_name as [Description] FROM STGMM.dbo.Country ORDER BY country_code ");
						
						if(mssql_num_rows($sql3) > 0){ 
							$temp['Index'] = '';
							$temp['Description'] = '-';
							$data[$i]['Options'][]=$temp;
							while($data[$i]['Options'][] = mssql_fetch_assoc($sql3));
							array_pop($data[$i]['Options']);
						}

					}
					
				}
				$i++;
			}
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		} else {	
			$this->AuditTrail['remark'] = "No Data";
			$data = 'no data';
		}
		
		$this->AddToAuditTrail();
		
		//$this->CloseMyConnection();
		
		return $data;
	}
	
	function InsertIntoImport($wawiindex, $reference, $requestType){
		mssql_select_db("Import");
		
		/*------------------------------+
		| $requestType values are		|
		| 1 = direct request			|
		| 2 = request from a function	|
		+------------------------------*/
		
		if ($requestType==1){
			$this->AuditTrail['action'] = "$wawiindex InsertIntoImport $reference";
		}
		
		$sql = mssql_query("SELECT * FROM Import WHERE Reference = ".$reference." AND WawiIndex='".$wawiindex."'");
		if(mssql_num_rows($sql) > 0){
			//nothing to do
		}else{
			$sql = mssql_query("INSERT INTO Import (Reference, WawiIndex, InvoiceValue, InvoiceValueSGD, FreightCharges, InsCharges, CIFAmount, GSTAmount) VALUES (".$reference.",'".$wawiindex."',0,0,0,0,0,0)");
			if($sql && mssql_rows_affected($this->link)){
				$this->AuditTrail['remark'] = "Successfully inserted import data ".$reference." in ".$wawiindex;
			}else{
				$this->AuditTrail['remark'] = "Failed to insert import data ".$reference." in ".$wawiindex;
			}
		}
		
		if ($requestType==1){
			$this->AddToAuditTrail();
		}
	}
	//$partsDetails = $this->GetPartNumber($post['Partnumber'],$DB['DBName'],2,NULL);
	function GetPartNumber($partnumber,$DBName,$requestType,$fields){
		mssql_select_db($DBName);
		
		/*------------------------------+
		| $requestType values are		|
		| 1 = direct request			|
		| 2 = request from a function	|
		| $partnumber = if null then all|
		+------------------------------*/
		
		$filter = ($partnumber!='null') ? "WHERE Partnumber='$partnumber'": "";
		$fields = ($fields!=NULL) ? $fields : "*";
		
		if ($requestType==1){
			$this->AuditTrail['action'] = "$DBName GetPartNumber ".$partnumber;
		}
			
		$sql = mssql_query("SELECT $fields FROM Parts $filter;");

		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
		}else{
			if ($requestType==1){
				$this->AuditTrail['remark'] = "no data";
			}
			$data = 'no data';
		}
		
		if ($requestType==1){
			$this->AddToAuditTrail();
		}
		
		return $data;
	}
	
	function CheckPartNumber($partnumber,$DBName){
		mssql_select_db($DBName);				
		if ($requestType==1){
			$this->AuditTrail['action'] = "$DBName CheckPartNumber ".$partnumber;
		}
			
		$sql = mssql_query("SELECT * FROM Parts WHERE Partnumber='$partnumber'");
		$partexist = false;
		if(mssql_num_rows($sql) > 0){
			$this->AuditTrail['remark'] = "Partnumber exist";			
			$partexist = true;
		}else{			
			$this->AuditTrail['remark'] = "Partnumber does not exist";			
			$partexist = false;
		}		
		return $partexist;
	}
	
	function SaveReceiving($post,$DB){

		mssql_select_db($DB['DBName']);
		
		$this->AuditTrail['action'] = $DB['WawiIndex']." SaveReceiving ".$post['EntryNumber'];
		
		$sql = mssql_query("SELECT * FROM [Entry] WHERE EntryNumber = ".$post['EntryNumber'].";");

		if(mssql_num_rows($sql) > 0){
			$this->AuditTrail['remark'] = "EntryNumber Already Exist";
			$result = 2;
		}else{
			/*
			switch ($post['WawiID']){
				case 28: //Lattice SG
					$sql = mssql_query("SELECT * FROM [Entry] WHERE LotNumber = '".$post['LotNumber']."';");
					if(mssql_num_rows($sql) > 0){
						$this->AuditTrail['remark'] = "Lot Number Already Exist";
						$this->AddToAuditTrail();
						$result = 15;
						return $result;
					}
				break;
			}
			*/

			$partsDetails = $this->GetPartNumber($post['Partnumber'],$DB['DBName'],2,NULL);
			
			//echo '<pre>';print_r($partsDetails);echo '<pre>';
			
			if ($partsDetails == 'no data'){
				$this->AuditTrail['remark'] = "Partnumber ".$post['Partnumber']." does not exist";
				$result = 3;
			}else if ($partsDetails[0]['PurchaseLock']){
				$this->AuditTrail['remark'] = "Partnumber ".$post['Partnumber']." purchase locked";
				$result = 4;
			}else if ($post['Quantity'] < 1){
				$this->AuditTrail['remark'] = "Quantity invalid";
				$result = 5;
			}else{
			
				if ($DB['GST']){
					if (!isset($post['ForwarderRef'])){
						//$this->AuditTrail['remark'] = "ForwarderRef not set"; 
						//$this->AddToAuditTrail();
						$result = 11;
						return $result;
					}
					$this->InsertIntoImport($DB['WawiIndex'],$post['ForwarderRef'],2);
				}
				
				if ($DB['STG']){
					//reserved for STGMM checking
				}
				
				$keystr = '';
				$valstr = '';

				foreach($post as $key => $value){
					if ($key != 'type' && $key != 'WawiID' && $key != 'barcode' && $key != 'PB'){
						$keystr .= "$key,";
						$valstr .= "'" . trim($value) . "' ,"; 						
					}
				}
				
			
				switch($DB['DBName']){
					case 'AnalogixWawi' :
						$keystr .= "PB,ROHS,E,";
						if(isset($post['PB'])){ 
							$valstr .= "'C:\CCS Programs\Analogix\pb.bmp' ,";
							$valstr .= "'C:\CCS Programs\Analogix\rohs.bmp' ,";
							$valstr .= "'C:\CCS Programs\Analogix\e.bmp' ,";
						}else{ 
							$valstr .= "'C:\CCS Programs\Analogix\nopb.bmp' ,";
							$valstr .= "'C:\CCS Programs\Analogix\norohs.bmp' ,";
							$valstr .= "'C:\CCS Programs\Analogix\noe.bmp' ,";
						}
					break;								
				}
				
				mssql_select_db($DB['DBName']);
				//mssql_query("BEGIN TRAN");
				
				$sql = mssql_query("INSERT INTO [Entry] (".$keystr."EntryDate) VALUES (".$valstr."'".date('Y-m-d H:i:s')."');");
				
				if($sql && mssql_rows_affected($this->link)){
					$this->AuditTrail['remark'] = "Success ".serialize($post);

					$result = ($partsDetails[0]['LifeSensitive']==1) ? 6: 7;
				}else{
					$this->AuditTrail['remark'] = "Failed";
					$result = 8;
				}
				//mssql_query("ROLLBACK");
			}
		}
		
		$this->AddToAuditTrail();
		return $result;
		
	}
	
	function SaveCustomReceiving($post,$DB){
		mssql_select_db($DB['DBName']);
		switch($post['WawiID']){
			case 56 : 
			case 57 : 
				$this->AuditTrail['action'] = $DB['WawiIndex']." SaveCustomReceiving LotNo :".$post['LotNumber'];
				$partsDetails = $this->GetPartNumber($post['Partnumber'],$DB['DBName'],2,NULL);
				if ($partsDetails == 'no data'){
					$this->AuditTrail['remark'] = "Partnumber ".$post['Partnumber']." does not exist";
					$result = 3;
				}else{				 
					if ($DB['GST']){
						if (!isset($post['ForwarderRef'])){
							$result = 11;
							return $result;
						}
						$this->InsertIntoImport($DB['WawiIndex'],$post['ForwarderRef'],2);
					}					
					if ($DB['STG']){
						//reserved for STGMM checking
					}
					
					$keystr = '';
					$valstr = '';

					foreach($post as $key => $value){
						if ($key != 'type' && $key != 'WawiID'){
							$keystr .= "$key,";
							$valstr .= "'" . trim($value) . "' ,"; 						
						}
					}
					mssql_select_db($DB['DBName']);					
					$sql = mssql_query("INSERT INTO [Entry] (".substr($keystr,0,-1).") VALUES (".substr($valstr,0,-1).") ") or die(mssql_get_last_message());
					if($sql && mssql_rows_affected($this->link)){
						$this->AuditTrail['remark'] = "Success ".serialize($post);
						$result = ($partsDetails[0]['LifeSensitive']) ? 6 : 7;
					}else{
						$this->AuditTrail['remark'] = "Failed";
						$result = 8;
					}
					
				}
				
			break;
		}
		$this->AddToAuditTrail();
		return $result;
	}
	function RetainReceivingField($WawiID){
		mssql_select_db(DefaultDB);
		
		$this->AuditTrail['action'] = "RetainReceivingField: ".$WawiID;
		
		$sql = mssql_query("SELECT FieldName FROM v_ReceivingSetup WHERE WawiID = ".$WawiID." AND RetainField=1  Order By OrderList;");

		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "failed";
			$data = 'no data';
		}
		
		$this->AddToAuditTrail();
		
		return $data;
		
	}
	
	function Putaway($DBName,$location,$entrynumber){
		mssql_select_db($DBName);


		$this->AuditTrail['action'] = "$DBName Putaway $DBName,$location,$entrynumber";
		
		$sql = mssql_query("SELECT EntryNumber,Location FROM [Entry] WHERE EntryNumber = ".$entrynumber.";");

		if(mssql_num_rows($sql) > 0){
			$data = mssql_fetch_assoc($sql);
			
			if ($location==$data['Location']){
				$this->AuditTrail['remark'] = "EntryNumber is already at location ".$location;
				$result = array($location,$entrynumber,'success');
			}else{
				$sql = mssql_query("update [entry] set location='$location' where EntryNumber = $entrynumber;");
				if($sql && mssql_rows_affected($this->link)){
					$this->AuditTrail['remark'] = "Update location success. Entry number: ".$entrynumber." | Location: ".$data['Location']." to Location: ".$location;
					$result = array($location,$entrynumber,'success');
				}else{
					$this->AuditTrail['remark'] = "Failed to update entry [".$entrynumber."] from location[".$data['Location']."] to location[".$location."]";
					$result = array($location,$entrynumber,'failed');
				}
			}
			
		}else{
			$this->AuditTrail['remark'] = "entry not found";
			$result = $result = array($location,$entrynumber,'entry not found');
		}
		
		$this->AddToAuditTrail();

		/*
		0 - entry number not exists
		1 - location is same
		2 - success
		3 - failed to update
		*/
		
		return $result;
		
	}
	
	function GetEntryParts($DBName){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "$DBName GetEntryParts";
		
		$sql = mssql_query("SELECT DISTINCT Partnumber FROM [Entry] WHERE Quantity > QuantityOut ORDER BY Partnumber;");

		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "entry partnumber empty";
			$data = 'no data';
		}
		
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	function GetEntryLot($DBName,$partnumber){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "$DBName GetEntryLot $partnumber";

		$sql = mssql_query("SELECT LotNumber FROM [Entry] WHERE Quantity > QuantityOut AND Partnumber='".$partnumber."' Group By LotNumber ORDER BY LotNumber;");

		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "entry partnumber empty";
			$data = 'no data';
		}
		
		$this->AddToAuditTrail();

		return $data;
	}
	
	function GetEntryNumber($DBName,$partnumber){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "$DBName GetEntryNumber $partnumber";
		
		$sql = mssql_query("SELECT EntryNumber FROM [Entry] WHERE Quantity > QuantityOut AND Partnumber='".$partnumber."' ORDER BY EntryNumber;");

		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "entry entrynumber empty";
			$data = 'no data';
		}
		
		$this->AddToAuditTrail();

		return $data;
	}
	
	function GetEntryNumberbyLot($DBName,$partnumber,$lotnumber){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "$DBName GetEntryNumberbyLot $partnumber,$lotnumber";
		
		$sql = mssql_query("SELECT EntryNumber FROM [Entry] WHERE Quantity > QuantityOut AND Partnumber='".$partnumber."' AND LotNumber='".$lotnumber."' ORDER BY EntryNumber;");

		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "entry entrynumber empty";
			$data = 'no data';
		}
		
		$this->AddToAuditTrail();

		return $data;
	}
	
	function GetEntrySum($DBName,$partnumber,$lotnumber,$entrynumber){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "$DBName GetEntrySum $partnumber,$lotnumber,$entrynumber";
		
		$filter = '';
		$filter .= ($lotnumber) ? " AND LotNumber='$lotnumber' ": '';
		$filter .= ($entrynumber) ? " AND EntryNumber=$entrynumber ": '';
		
		$sql = mssql_query("SELECT SUM(Quantity-QuantityOut) AS SumPart FROM Entry WHERE Partnumber='".$partnumber."' $filter;");

		if(mssql_num_rows($sql) > 0){
			$data = mssql_fetch_assoc($sql);
			$this->AuditTrail['remark'] = "Successful filter sum: ".count($data['SumPart']);
		}else{
			$this->AuditTrail['remark'] = "no data";
			$data = 'no data';
		}
		
		$this->AddToAuditTrail();

		return $data;
	}
	
	function EntryComment($DBName,$partnumber,$lotnumber,$entrynumber,$comment,$block){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "$DBName EntryComment $partnumber,$lotnumber,$entrynumber,$comment,$block";
		
		$setfields = " set Comment = '".trim($comment)."'";
		
		if ($block == 0){
			$setfields.=",Blocked = 0";
		} elseif($block == 1){
			$setfields.=",Blocked = (Quantity - QuantityOut)";
		}
		
		$statement = "update [entry] $setfields where Partnumber = '".$partnumber."'";
		$statement.= (strlen($lotnumber) > 0) ? " AND LotNumber='".$lotnumber."'": '';
		$statement.= (strlen($entrynumber) > 0) ? " AND EntryNumber=".$entrynumber: '';
		
		
		$sql = mssql_query($statement);

		if($sql && mssql_rows_affected($this->link)){
			$this->AuditTrail['remark'] = "success";
			$data = 'success';
		}else{
			$this->AuditTrail['remark'] = "failed";
			$data = 'failed';
		}
		
		$this->AddToAuditTrail();

		return $data;
	}

	function GetDivision($DBName){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "$DBName GetDivision";
		
		$sql = mssql_query("SELECT DISTINCT [Index] as Division FROM Stocks ORDER BY [Index];");

		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "no data";
			$data = 'no data';
		}
		
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	function GetPartPerDivision($DBName,$division){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "$DBName GetPartPerDivision $division";
		
		$sql = mssql_query("SELECT DISTINCT Partnumber FROM Entry WHERE (StockIndex='".$division."') AND (QuantityOut<>Quantity) ORDER BY Partnumber;");

		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "no data";
			$data = 'no data';
		}
		
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	function GetEntryTransferList($DBName,$division,$partnumber){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "Filtering $DBName Entry Transfer list division[".$division."]|partnumber[".$partnumber."]";
		
		$sql = mssql_query("SELECT EntryNumber,LotNumber,Quantity,QuantityOut,Invoice,Location FROM Entry WHERE (StockIndex='".$division."') AND (Partnumber='".$partnumber."') AND (QuantityOut<>QUANTITY) ORDER BY EntryNumber;");

		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "no data";
			$data = 'no data';
		}
		
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	function EntryTransfer($DBName,$selectedEntry,$division,$todivision){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "$DBName EntryTransfer [$division-$todivision]: $selectedEntry";
		
		
		if (strtolower($DBName) == "MarvellWawi"){
			$sql = mssql_query("Update [Entry] set Stockindex='".$todivision."',OldDivision='".$division."' where EntryNumber in (".$selectedEntry.");");
		} else {
			$sql = mssql_query("Update [Entry] set Stockindex='".$todivision."' where EntryNumber in (".$selectedEntry.");");
		}

		if($sql && mssql_rows_affected($this->link)){
			$this->AuditTrail['remark'] = "success";
			$data = 'success';
		}else{
			$this->AuditTrail['remark'] = "failed";
			$data = 'failed';
		}
		
		$this->AddToAuditTrail();

		return $data;
	}
	
	
	function ModifyEntryData($DBName,$entrynumber,$description){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "Filtering $DBName PacklistPosition with entrynumber $entrynumber";
		
		$sql = mssql_query("SELECT [DocumentNumber] FROM [PacklistPosition] WHERE [EntryNumber]=".$entrynumber.";");
		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
			
			
			$query = '';
			for ($i=0;$i<count($data);$i++){
				$query .= "UPDATE [OrderPosition] SET [Description]='".$description."' WHERE [Ordernumber]=" .$data[$i]["DocumentNumber"] ."; ";
			}
			
			$this->AuditTrail['action'] = "Update $DBName Entry Data entrynumber $entrynumber";
			$sql = mssql_query($query);
			if($sql && mssql_rows_affected($this->link)){
				$this->AuditTrail['remark'] = "success";
				$data = 'success';
			}else{
				$this->AuditTrail['remark'] = "failed";
				$data = 'failed';
			}
			
			
		}else{
			$this->AuditTrail['remark'] = "no data";
			$data = 'no data'; 
		}
		
		$this->AddToAuditTrail();

		return $data;
	}
	
	
	function GetEntryData($WawiID,$DBName,$action,$entrynumber){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "GetEntryData $WawiID,$DBName,$entrynumber";
		
		switch($action){
			case 1 : $sqlstring = "SELECT TOP 2 *, CONVERT(varchar,EntryDate,120) as DateFormatted FROM [Entry] ORDER BY [EntryNumber] ASC"; break;
			case 2 : $sqlstring = "SELECT TOP 2 *, CONVERT(varchar,EntryDate,120) as DateFormatted FROM [Entry] WHERE [EntryNumber] < $entrynumber ORDER BY [EntryNumber] DESC"; break;
			case 3 : $sqlstring = "SELECT TOP 2 *, CONVERT(varchar,EntryDate,120) as DateFormatted FROM [Entry] WHERE [EntryNumber] > $entrynumber ORDER BY [EntryNumber] ASC"; break;
			case 4 : $sqlstring = "SELECT TOP 2 *, CONVERT(varchar,EntryDate,120) as DateFormatted FROM [Entry] ORDER BY [EntryNumber] DESC"; break;
			case 5 : $sqlstring = "SELECT TOP 1 *, CONVERT(varchar,EntryDate,120) as DateFormatted FROM [Entry] WHERE [EntryNumber] = $entrynumber"; break;
		}
		
		
		$sql = mssql_query($sqlstring); 

		if($sql && mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
		}else{ 
			$this->AuditTrail['remark'] = "entry number does not exist";
			$data = 'no data'; 
		} 
		
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	
	function CheckPackListScan($DBName,$packlistnumber){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "Filtering $DBName packlist number $packlistnumber";
		
		$sql = mssql_query("Select TOP 1 DocumentNumber from Packlist where PackListnumber = $packlistnumber;");

		if(mssql_num_rows($sql) > 0){
			$PL = mssql_fetch_assoc($sql);
			$DocumentNumber = $PL['DocumentNumber'];
			
			$data = $this->CheckScanComplete($DBName,$DocumentNumber,1);
			$result = ($data=='no data' || count($data) < 1) ? 'no data': json_encode($data);
		}else{
			$this->AuditTrail['remark'] = "no data";
			$result = 'no data'; 
		}
		
		$this->AddToAuditTrail();
		
		return $result;
	}
	
	function GetBoxweight($DBName,$packlistnumber){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "$DBName GetBoxweight $packlistnumber";
		$sql = mssql_query("Select * from Boxweight where packlistnumber = $packlistnumber;");

		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "no data";
			$data = 'no data'; 
		}
		
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	function GetBoxWeightByBN($DBName,$boxnumber){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "$DBName GetBoxWeightByBN $boxnumber";
		//echo "Select * from Boxweight where Boxnumber = $boxnumber;";
		//die();
		$sql = mssql_query("Select * from Boxweight where Boxnumber = $boxnumber;");

		if(mssql_num_rows($sql) > 0){
			while($data1[] = mssql_fetch_assoc($sql));
			array_pop($data1);
			$this->AuditTrail['remark'] = "Row count: ".count($data1);
		}else{
			$this->AuditTrail['remark'] = "no data";
			$data1 = 'no data'; 
		}
		
		$this->AddToAuditTrail();
		
		return $data1;
	}
	
	function CheckScanComplete($DBName,$DocumentNumber,$RequestType){
		mssql_select_db($DBName);
		
		if ($RequestType==0){
			$this->AuditTrail['action'] = "$DBName CheckScanComplete $DocumentNumber";
		}
		$sqlpacklistposition = mssql_query("select DISTINCT EntryNumber from PacklistPosition where DocumentNumber = $DocumentNumber;");
		$sqlshipboxes = mssql_query("select DISTINCT EntryNumber from Shipboxes where Ordernumber = $DocumentNumber;");
		
		if (mssql_num_rows($sqlpacklistposition) == mssql_num_rows($sqlshipboxes)){
			
			if ($RequestType==0){
				$this->AuditTrail['remark'] = "tally with row count: ".mssql_num_rows($sqlpacklistposition);
			}
			
			/*
			select Shipboxes.Ordernumber,Shipboxes.EntryNumber,Shipboxes.BoxNumber,Shipboxes.Quantity,
[Entry].PartNumber, Boxweight.[Weight], Boxweight.Dimension
from Shipboxes,[Entry],Boxweight 
Where Shipboxes.Ordernumber = $DocumentNumber AND Shipboxes.EntryNumber = [Entry].EntryNumber
AND Shipboxes.Packlistnumber=Boxweight.Packlistnumber AND Shipboxes.Boxnumber=Boxweight.Boxnumber;
			*/
			
			$sql = mssql_query("SELECT     dbo.Shipboxes.Ordernumber, dbo.Shipboxes.Entrynumber, dbo.Shipboxes.Boxnumber, dbo.Shipboxes.Quantity, dbo.Entry.Partnumber, 
                      dbo.Boxweight.Weight, dbo.Boxweight.Dimension, dbo.PackList.Name as WhoScan
FROM         dbo.Shipboxes INNER JOIN
                      dbo.Entry ON dbo.Shipboxes.Entrynumber = dbo.Entry.EntryNumber INNER JOIN
                      dbo.Boxweight ON dbo.Shipboxes.Packlistnumber = dbo.Boxweight.Packlistnumber AND 
                      dbo.Shipboxes.Boxnumber = dbo.Boxweight.Boxnumber INNER JOIN
                      dbo.PackList ON dbo.Shipboxes.Packlistnumber = dbo.PackList.PackListnumber
WHERE     (dbo.Shipboxes.Ordernumber = $DocumentNumber);");
			if (mssql_num_rows($sql) > 0){
				while($result[] = mssql_fetch_assoc($sql));
				array_pop($result);
			}else{
				$result = 'proceed';
			}

		} else {
			
			if ($RequestType==0){
				$this->AuditTrail['remark'] = "does not tally";
			}
			$result = 'proceed';
		}
		
		if ($RequestType==0){
			$this->AddToAuditTrail();
		}
		
		return $result;
		
	}
	
	function CheckScanOutComplete($DBName,$packlistnumber,$requestype){
		mssql_select_db($DBName);
		
		if ($requestype==0){
			$this->AuditTrail['action'] = "$DBName CheckScanOut $packlistnumber";
		}
		$sql = mssql_query("Select Packlist.DocumentNumber from Packlist,PackListPosition where Packlist.PackListnumber=PackListPosition.PackListNumber and Packlist.PackListnumber=$packlistnumber GROUP BY Packlist.DocumentNumber;"); 
		
		/*
		0 = no PL
		1 = all boxout/tally
		2 = partial
		3 = no scanout data
		4 = error
		*/
		
		if ($sql && mssql_num_rows($sql)){
			$sql2 = mssql_query("select CASE WHEN 
			(select COUNT(DISTINCT EntryNumber) from PacklistPosition where PackListNumber = $packlistnumber)=
			(select COUNT(DISTINCT EntryNumber) from Shipboxes where Packlistnumber = $packlistnumber)
			THEN 1 ELSE 2 END AS tally");
			
			if ($sql2 && mssql_num_rows($sql2)){
				$data = mssql_fetch_assoc($sql2);
				$result = $data['tally'];
				if ($requestype==0){
					$this->AuditTrail['remark'] = "all box out";
				}
			}else if($sql2){
				$result = 3;
				if ($requestype==0){
					$this->AuditTrail['remark'] = "no data";
				}
			}else{
				$result = 4;
				if ($requestype==0){
					$this->AuditTrail['remark'] = "error";
				}
			}
			
		}else{
			$result = 0;
			if ($requestype==0){
				$this->AuditTrail['remark'] = "no data";
			}
		}
		
		if ($requestype==0){
			$this->AddToAuditTrail();
		}
		
		return $result;
	}
	
	function GetScanOut($DBName,$packlistnumber){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "$DBName GetScanOut $packlistnumber";
		
		/*
		$sql = mssql_query("SELECT     dbo.Shipboxes.Ordernumber, dbo.Shipboxes.Entrynumber, dbo.Shipboxes.Boxnumber, dbo.Shipboxes.Quantity, dbo.Entry.Partnumber, 
                      dbo.Boxweight.Weight, dbo.Boxweight.Dimension, dbo.PackList.Name as WhoScan
FROM         dbo.Shipboxes INNER JOIN
                      dbo.Entry ON dbo.Shipboxes.Entrynumber = dbo.Entry.EntryNumber INNER JOIN
                      dbo.Boxweight ON dbo.Shipboxes.Packlistnumber = dbo.Boxweight.Packlistnumber AND 
                      dbo.Shipboxes.Boxnumber = dbo.Boxweight.Boxnumber INNER JOIN
                      dbo.PackList ON dbo.Shipboxes.Ordernumber = dbo.PackList.DocumentNumber
WHERE     (dbo.Shipboxes.Packlistnumber = $packlistnumber);");
	*/
		$sql = mssql_query("SELECT     dbo.Shipboxes.Packlistnumber, dbo.Shipboxes.Entrynumber, dbo.Shipboxes.Boxnumber, dbo.Shipboxes.Quantity, dbo.PackListPosition.PartNumber, dbo.PackListPosition.carton FROM dbo.Shipboxes INNER JOIN
		dbo.PackListPosition ON dbo.Shipboxes.Packlistnumber = dbo.PackListPosition.PackListNumber AND dbo.Shipboxes.Entrynumber = dbo.PackListPosition.EntryNumber
		WHERE (dbo.Shipboxes.Packlistnumber = $packlistnumber);");

		if (mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
		}else{
			$data = 'no data';
			$this->AuditTrail['remark'] = "no data";
		}
		
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	function GetDateCode($DBName,$entrynumber,$requesttype){
		mssql_select_db($DBName);
		
		if ($requesttype==0){
			$this->AuditTrail['action'] = "$DBName GetDateCode $entrynumber";
		}
		$sql = mssql_query("Select TOP 1 [DateCode] from [Entry] where EntryNumber = $entrynumber;");

		if(mssql_num_rows($sql) > 0){
			$result = mssql_fetch_assoc($sql);
			$data = $result['DateCode'];
			if ($requesttype==0){
				$this->AuditTrail['remark'] = $data;
			}
		}else{
			$data = 'no data'; 
			if ($requesttype==0){
				$this->AuditTrail['remark'] = "no data";
			}
		}
		if ($requesttype==0){
			$this->AddToAuditTrail();
		}
		
		return $data;
	}
	
	/* Normal Scanout : Get dimension */
	function GetDimension($WawiID){

		mssql_select_db(DefaultDB);
		
		$this->AuditTrail['action'] = "GetDimension WawiID $WawiID";
		
		$sql = mssql_query("SELECT DimensionType, Dimension, Remarks1, Remarks2, CartonWeight FROM Dimension WHERE WawiID=$WawiID AND [status]=1;");
		
		$row = array();
		
		array_push($row,array("DimensionType" => "n/a", "Dimension" => "", "Remarks1" => "", "Remarks2" => "", "CartonWeight" => "" ));
		
		if ($sql && mssql_num_rows($sql)){
			$this->AuditTrail['remark'] = "Row Count: ".mssql_num_rows($sql);
			while($row[]=mssql_fetch_assoc($sql)){}
			array_pop($row);
		}
		 
		$this->AddToAuditTrail();

		return $row;
	}
	
	function NormalScanOut($DBName,$post){
		mssql_select_db($DBName);
		
		$auditpost = implode(',',$post);
		
		$this->AuditTrail['action'] = "$DBName NormalScanOut ".$auditpost;
		
		$CSC = $this->CheckScanOutComplete($DBName,$post['packlistnumber'],1);
		
		/*
		0 = no PL
		1 = all boxout
		2 = partial
		3 = no scanout data
		4 = error
		
		5 = entry number not exist
		6 = invalid quantity
		7 = already scanned
		8 = error updating packlist
		9 = error inserting shipbox
		10 = error inserting boxweight
		11 = error updating packlistposition box and carton
		*/
		
		if ($CSC==1){
			$data = 1;
			$this->AuditTrail['remark'] = "all box out";
		}else if ($CSC==2 || $CSC==3){
			if ($post['datecode']=='no data'){
				$data = 5;
				$this->AuditTrail['remark'] = "entry number not exist";
			}else{
				$sql = mssql_query("Select DocumentNumber as OrderNumber from PackListPosition where EntryNumber = ".$post['entrynumber']." and PacklistNumber = ".$post['packlistnumber']." and Quantity = ".$post['quantity'].";");

				if (mssql_num_rows($sql) < 1){
					
					$data = 6;
					$this->AuditTrail['remark'] = "invalid quantity";
				} else {
					$row = mssql_fetch_assoc($sql);
					$OrderNumber = $row['OrderNumber'];
					
					$sql = mssql_query("Select * from Shipboxes where Packlistnumber=".$post['packlistnumber']." and Entrynumber = ".$post['entrynumber'].";");
					if (mssql_num_rows($sql) > 0){
						$data = 7;
						$this->AuditTrail['remark'] = "already scanned";
					} else {
					
						$sqldimension = mssql_query("select * from ".DefaultDB.".dbo.Dimension where DimensionType like '".$post['dimentiontype']."' and WawiID=".$post['WawiID']." and [status]=1;");
						if (mssql_num_rows($sqldimension) > 0){ 
							$row = mssql_fetch_assoc($sqldimension);
							$dimensionvalue = $row['Dimension'];
							$CartonWeight = $row['CartonWeight'];
						} else { 
							$dimensionvalue = "";
							$CartonWeight = "";
						}
						//echo "select * from Boxweight where OrderNumber = $OrderNumber and Boxnumber = ".$post['boxnumber'].";";
						
						$sqlbox = mssql_query("select * from Boxweight where OrderNumber = $OrderNumber and Boxnumber = ".$post['boxnumber'].";");
						mssql_query('BEGIN TRAN');
							
						if (mssql_num_rows($sqlbox) < 1){ // Boxnumber do not exists with the corresponding Ordernumber
							
							if (mssql_query("Insert into Boxweight (Boxnumber,Weight,Packlistnumber,Ordernumber,DimensionType,Dimension) values (".$post['boxnumber'].", ".$post['weight'].", ".$post['packlistnumber']." ,$OrderNumber,'".$post['dimentiontype']."','$dimensionvalue')")){
								
								if (mssql_query("Insert into Shipboxes (Ordernumber,Boxnumber,Entrynumber,Quantity,DateCode,Packlistnumber) values ($OrderNumber, ".$post['boxnumber'].", ".$post['entrynumber'].", ".$post['quantity'].", '".$post['datecode']."', ".$post['packlistnumber'].")")){
									
									$NetWeight = $this->ComputeNetWeight($DBName,$post['packlistnumber'],$post['WawiID']);
									
									$sqlupdate = mssql_query("update Packlist set Packlist.Colli = (select COUNT(*) from Boxweight b1 where b1.Packlistnumber=Packlist.PackListnumber ), Packlist.NetWeight = $NetWeight, Packlist.[Name] = '".$post['whoscan']."', Packlist.[weight] = (Select SUM(b2.[Weight]) from Boxweight b2 where b2.Packlistnumber=Packlist.PackListnumber), Packlist.DatePacked = '".date("m/d/Y")."', Packlist.TimePacked = '".date("H:i")."', PackList.Dimension='".$dimensionvalue."' where Packlist.PackListnumber = ".$post['packlistnumber'].";");
									
									if ($sqlupdate && mssql_rows_affected($this->link)){
										
										$sqlPackListPosition = mssql_query("UPDATE PackListPosition
										SET carton=(
										CASE WHEN 
											(SELECT TOP 1 carton from PackListPosition WHERE box=".$post['boxnumber']." AND PackListNumber=".$post['packlistnumber'].") IS NOT NULL 
										THEN 
											(SELECT TOP 1 carton from PackListPosition WHERE box=".$post['boxnumber']." AND PackListNumber=".$post['packlistnumber'].")
										ELSE 
											(SELECT CASE WHEN MAX(carton) IS NULL THEN 1 ELSE MAX(carton)+1 END as tempNbr FROM PackListPosition WHERE PackListNumber=".$post['packlistnumber'].")
										END),
										Box=".$post['boxnumber']." 
										WHERE DocumentNumber=".$OrderNumber." AND PackListNumber=".$post['packlistnumber']." AND EntryNumber=".$post['entrynumber'].";");
										if ($sqlPackListPosition && mssql_rows_affected($this->link)){
											$data = ($this->CheckScanOutComplete($DBName,$post['packlistnumber'],1)==1) ? 1: 2;
											mssql_query('COMMIT');
										}else{
											$data = 11;
											$this->AuditTrail['remark'] = "error updating packlistposition box and carton";
											mssql_query('ROLLBACK');
										}
									}else{
										$data = 8;
										$this->AuditTrail['remark'] = "error updating packlist";
										mssql_query('ROLLBACK');
									}
									
									
									
									
								} else {
									$data = 9;
									$this->AuditTrail['remark'] = "error inserting shipbox";
									mssql_query('ROLLBACK');
								}
							} else {
								$data = 10;
								$this->AuditTrail['remark'] = "error inserting boxweight";
								mssql_query('ROLLBACK');
							}
						} else { // Box weight already exists so just insert on th shipboxes
							
							if (mssql_query("Insert into Shipboxes (Ordernumber,Boxnumber,Entrynumber,Quantity,DateCode,Packlistnumber) values ($OrderNumber, ".$post['boxnumber'].", ".$post['entrynumber'].", ".$post['quantity'].", '".$post['datecode']."', ".$post['packlistnumber'].")")){
								
								$NetWeight = $this->ComputeNetWeight($DBName,$post['packlistnumber'],$post['WawiID']);
								
								$sqlupdate = mssql_query("update Packlist set Packlist.Colli = (select COUNT(*) from Boxweight b1 where b1.Packlistnumber=Packlist.PackListnumber ), Packlist.NetWeight = $NetWeight, Packlist.[Name] = '".$post['whoscan']."', Packlist.[weight] = (Select SUM(b2.[Weight]) from Boxweight b2 where b2.Packlistnumber=Packlist.PackListnumber), Packlist.DatePacked = '".date("m/d/Y")."', Packlist.TimePacked = '".date("H:i")."', PackList.Dimension='".$dimensionvalue."' where Packlist.PackListnumber = ".$post['packlistnumber'].";");
									
								if ($sqlupdate && mssql_rows_affected($this->link)){

									$sqlPackListPosition = mssql_query("UPDATE PackListPosition
									SET carton=(
									CASE WHEN 
										(SELECT TOP 1 carton from PackListPosition WHERE box=".$post['boxnumber']." AND PackListNumber=".$post['packlistnumber'].") IS NOT NULL 
									THEN 
										(SELECT TOP 1 carton from PackListPosition WHERE box=".$post['boxnumber']." AND PackListNumber=".$post['packlistnumber'].")
									ELSE 
										(SELECT CASE WHEN MAX(carton) IS NULL THEN 1 ELSE MAX(carton)+1 END as tempNbr FROM PackListPosition WHERE PackListNumber=".$post['packlistnumber'].")
									END),
									Box=".$post['boxnumber']."
									WHERE DocumentNumber=".$OrderNumber." AND PackListNumber=".$post['packlistnumber']." AND EntryNumber=".$post['entrynumber'].";");
									if ($sqlPackListPosition && mssql_rows_affected($this->link)){
										$data = ($this->CheckScanOutComplete($DBName,$post['packlistnumber'],1)==1) ? 1: 2;
										mssql_query('COMMIT');
									}else{
										$data = 11;
										mssql_query('ROLLBACK');
									}
								}else{
									$data = 8;
									mssql_query('ROLLBACK');
								}
								
							} else {
								$data = 9;
								mssql_query('ROLLBACK');
							}
						}//boxweight
					}//shipboxes
				}//packlistposition
			}//no data
			
		}else if ($CSC==4){
			$data = 4;
			$this->AuditTrail['remark'] = "unexpected error";
		}else if ($CSC==0){
			$data = 0;
			$this->AuditTrail['remark'] = "PicklistNumber not found";
		}
		
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	function ComputeNetWeight($DBName,$PacklistNumber,$WawiID){
		mssql_select_db($DBName);
		
		$gWeight = 0;
		$NetWeight = 0;
		$sql = mssql_query("select partnumber,sum(Quantity) as totalqty,PackageWeight,packlistnumber from packlistposition where PackListNumber = $PacklistNumber AND partnumber IS NOT NULL group by partnumber,PackageWeight,packlistnumber");
		while($row = mssql_fetch_assoc($sql)){
			$partnumber			= $row['partnumber'];
			$totalqty				= $row['totalqty'];
			$PackageWeight  = trim($row['PackageWeight']);
			
			if ($PackageWeight == ''){
				$sqlparts = mssql_query("select WeightPerPiece from parts where Partnumber = '$partnumber'");
				if (mssql_num_rows($sqlparts) > 0){
					$sqlparts = mssql_fetch_assoc($sqlparts);
					$PackageWeight = $sqlparts['WeightPerPiece'];
				} else {
					$PackageWeight = 0;
				}
				
			}
			
			$gWeight = $gWeight + ($totalqty * $PackageWeight);
			
		}
		
		$NetWeight = $gWeight / 1000;
		
		if ($NetWeight==0){
			$sqlNW = mssql_query("select SUM(b.[Weight]-m.CartonWeight) as NetWeight from MasterData.dbo.Dimension m, Boxweight b where m.WawiID=".$WawiID." and m.DimensionType = b.DimensionType
and b.Packlistnumber=$PacklistNumber;");

			if (mssql_num_rows($sqlNW) > 0){
				$row = mssql_fetch_assoc($sqlNW);
				$NetWeight = $row['NetWeight'];
			}
		}
		
		$NetWeight = (!is_numeric($NetWeight)) ? 0: $NetWeight;
		
		return $NetWeight;
	}
	
	
	function UpdateBoxWeight($DBName,$packlistnumber,$boxnumber,$oldweight,$weight,$WawiID){
		mssql_select_db($DBName); 
		
		$this->AuditTrail['action'] = "$DBName UpdateBoxWeight $packlistnumber,$boxnumber,$oldweight,$weight";
		
		$statement = "UPDATE Boxweight SET [Weight]=$weight WHERE Boxnumber = $boxnumber and Packlistnumber=$packlistnumber;";	
		
		mssql_query('BEGIN TRAN');
		
		$sql = mssql_query($statement);

		if($sql && mssql_rows_affected($this->link)){
			
			$NetWeight = $this->ComputeNetWeight($DBName,$packlistnumber,$WawiID);
			
			$sqlupdate = mssql_query("update Packlist set Packlist.NetWeight = $NetWeight, Packlist.[weight] = (Select SUM(b2.[Weight]) from Boxweight b2 where b2.Packlistnumber=Packlist.PackListnumber) where Packlist.PackListnumber = $packlistnumber;");
									
			if ($sqlupdate && mssql_rows_affected($this->link)){
				$data = 'success';
				$this->AuditTrail['remark'] = "success";
				mssql_query('COMMIT');
			}else{
				$data = 'failed';
				mssql_query('ROLLBACK');
			}
			
		}else{
			$this->AuditTrail['remark'] = "failed";
			$data = 'failed';
			mssql_query('ROLLBACK');
		}
		 
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	/* Change Partnumber : Get partnumber - Marketing */
	function GetPartNumberM($DBName){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "Filtering Entry Parts: ".$DBName;
		
		$sql = mssql_query("SELECT PartNumber FROM Parts;");

		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "entry partnumber empty";
			$data = 'no data';
		}
		
		$this->AddToAuditTrail();
		
		return $data;
	}
	/* Change Partnumber : Get partnumber - Partnumber */
	function GetPartNumberP($DBName){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "Filtering Entry Parts: ".$DBName;
		
		$sql = mssql_query("SELECT DISTINCT PartNumber FROM [Entry] WHERE Partnumber NOT IN (SELECT PartNumber FROM PackListPosition) AND QuantityOut=0 AND EntryNumber NOT IN (SELECT EntryNumber FROM PackListPosition GROUP BY EntryNumber) ORDER BY Partnumber");

		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "entry partnumber empty";
			$data = 'no data';
		}
		
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	/* Change Partnumber : Get partnumber - Supplier */
	function GetPartNumberS($DBName){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "Filtering Entry Parts: ".$DBName;
		
		$sql = mssql_query("SELECT DISTINCT SupplierPartnumber FROM [Entry] WHERE QuantityOut=0 AND EntryNumber NOT IN (SELECT EntryNumber FROM PackListPosition GROUP BY EntryNumber) ORDER BY SupplierPartnumber");

		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "entry partnumber empty";
			$data = 'no data';
		}
		
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	/* Change Partnumber : Get VSC Partnumber - Marketing */
	function GetSupplierPartNumber($DBName,$partnumber){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "$DBName Filtering SupplierPartnumber=$partnumber from MarketingPartnumbers: ";
		
		$sql = mssql_query("SELECT SupplierPartnumber FROM MarketingPartnumbers WHERE Partnumber='$partnumber';");

		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "entry entrynumber empty";
			$data = 'no data';
		}
		
		$this->AddToAuditTrail();

		return $data;
	}
	
	/* Change Partnumber : Get entry number - Partnumber */
	function GetEntryNumberP($DBName,$partnumber){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "Filtering Distinct Entry Number: ".$DBName;
		
		$sql = mssql_query("SELECT EntryNumber,PartNumber,Invoice,Location,LotNumber,Quantity FROM [Entry] WHERE PartNumber='$partnumber' AND QuantityOut=0 AND EntryNumber NOT IN (SELECT EntryNumber FROM PackListPosition GROUP BY EntryNumber);");

		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "entry entrynumber empty";
			$data = 'no data';
		}
		
		$this->AddToAuditTrail();

		return $data;
	}
	
	/* Change Partnumber : Get entry number - Supplier */
	function GetEntryNumberS($DBName,$partnumber){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "Filtering Distinct Entry Number: ".$DBName;
		
		$sql = mssql_query("SELECT EntryNumber,PartNumber,Invoice,Location,LotNumber,Quantity FROM [Entry] WHERE SupplierPartNumber='$partnumber' AND QuantityOut=0 AND EntryNumber NOT IN (SELECT EntryNumber FROM PackListPosition GROUP BY EntryNumber);");

		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "entry entrynumber empty";
			$data = 'no data';
		}
		
		$this->AddToAuditTrail();

		return $data;
	}
	
	/* Change Partnumber : GET new partnumber */
	function GetNewPartNumber($DBName,$partnumber){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "Filtering Parts ".$DBName.": ".$partnumber;
		
		$sql = mssql_query("SELECT PartNumber FROM Parts WHERE PartNumber <> '$partnumber'");

		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "entry partnumber empty";
			$data = 'no data';
		}
		 
		$this->AddToAuditTrail();
		
		return $data;
	}
	/*Change Partnumber : Update Partnumber - Marketing */
	function UpdatePartNumberM($DBName,$partnumber,$SupplierPartNumber){
		
		//$DBName = 'DemoWawi';
		
		mssql_select_db($DBName); 
		
		$this->AuditTrail['action'] = "Insert Parts ".$DBName.": ".$partnumber;
		 
		$statement = "INSERT INTO MarketingPartnumbers (SupplierPartNumber,Partnumber) VALUES ('" . trim($SupplierPartNumber) . "','" . trim($partnumber)."')";
			
		$sql = mssql_query($statement);
	 
		if($sql && mssql_rows_affected($this->link)){
			$this->AuditTrail['remark'] = "Successfully inserted partnumber: ".count($data);
			$data = 'success';
		}else{
			$this->AuditTrail['remark'] = "Failed to update partnumber";
			$data = 'failed';
		}
		 
		$this->AddToAuditTrail();
		
		return $data;
	}
	/*Change Partnumber : Update Partnumber - Partnumber*/
	function UpdatePartNumberP($DBName,$partnumber,$entrynumber,$newpartnumber){
		mssql_select_db($DBName); 
		
		$this->AuditTrail['action'] = "$DBName Updating Entry Partnumber from $partnumber to $newpartnumber";
		 
		$statement = "UPDATE [Entry] SET Partnumber='" . $newpartnumber ."' WHERE EntryNumber in (" . $entrynumber . ") AND EntryNumber NOT IN (SELECT EntryNumber FROM PackListPosition GROUP BY EntryNumber) AND QuantityOut=0;";	
		
		$sql = mssql_query($statement);

		if($sql && mssql_rows_affected($this->link)){
			$this->AuditTrail['remark'] = "Successfully update partnumber: ".mssql_rows_affected($this->link);
			$data = 'success';
		}else{
			$this->AuditTrail['remark'] = "Failed to update partnumber";
			$data = 'failed';
		}
		 
		$this->AddToAuditTrail();
		
		return $data;
	}
	/*Change Partnumber : Update Partnumber - Supplier*/
	function UpdatePartNumberS($DBName,$partnumber,$entrynumber,$newpartnumber){
		//$data = $DBName.$partnumber.$entrynumber.$newpartnumber;
		//return $data;
		//exit;
		
		mssql_select_db($DBName); 
		
		$this->AuditTrail['action'] = "$DBName updating entry partnumber from $newpartnumber to $partnumber";
		 
		 
		$entry_statement = "UPDATE [Entry] SET SupplierPartnumber='" . $newpartnumber ."' WHERE EntryNumber in (" . $entrynumber . ") AND EntryNumber NOT IN (SELECT EntryNumber FROM PackListPosition GROUP BY EntryNumber) AND QuantityOut=0;";	
		
		$insert_statement = "INSERT INTO MarketingPartNumbers (SupplierPartNumber,PartNumber) VALUES ('" . trim($newpartnumber) . "','" . trim($partnumber)."');";
		
		
		$updateSQL = mssql_query($entry_statement);
		
		if($updateSQL && mssql_rows_affected($this->link)){
			$rows=mssql_rows_affected($this->link);
			$this->AuditTrail['remark'] = "Successfully updated ".$rows." rows for entry partnumber";
			
			$data = 'success';
			
			$insertSQL = mssql_query($entry_statement);
			
			if($insertSQL && mssql_rows_affected($this->link)){	
				$data = 'success1';
			}		
		}else{
			$this->AuditTrail['remark'] = "Failed to update entry partnumber";
			$data = 'failed';						
		}
		
		 
		$this->AddToAuditTrail();
		
		return $data;
	}
	/* Flight Detail : Upload the file */
	function FlightDetailUploadFile($DBName,$allowedext,$filename,$targetupload){
		
		mssql_select_db($DBName); 
		
		$this->AuditTrail['action'] = "Uploading file with customer: ".$DBName.": ".$filename;
				
		include_once('./modules/flightdetail/fileupload.class.php');		
		$uploader = new qqFileUploader($allowedext);
		$result = $uploader->handleUpload($targetupload,true);
		
		if($result){
			$this->AuditTrail['remark'] = "File uploaded successfully";
		}else{
			$this->AuditTrail['remark'] = "Failed to upload file";
		}
		
		 
		$this->AddToAuditTrail();
		
		
		return $result;
	}
	/* Flight Detail : Extract the file */
	function GetFlightDetailExcelData($DBName,$filename){		
		// error_reporting(E_ALL);
		// ini_set("display_errors", 1); 		
		mssql_select_db($DBName); 
		
		$this->AuditTrail['action'] = "Parsing uploaded file: ".$DBName.": ".$filename;
				
		$target_path = './modules/flightdetail/uploads/'.$filename;		
		
		$ext = pathinfo($target_path, PATHINFO_EXTENSION);		
		
		if($ext == "xls"){
			include_once('./modules/flightdetail/simplexls.class.php');
			$xls = new Spreadsheet_Excel_Reader();
			$xls->setOutputEncoding('CP1251');
			$xls->read($target_path);
			
			for ($i = 0; $i < $xls->sheets[0]['numRows']; $i++) {
				for ($j = 0; $j < $xls->sheets[0]['numCols']; $j++){
					$data[$i][$j] = isset($xls->sheets[0]['cells'][$i+1][$j+1]) ? $xls->sheets[0]['cells'][$i+1][$j+1] : 'NULL';
				}
			}	
		}else if($ext == "xlsx"){		
			include_once('./modules/flightdetail/simplexlsx.class.php');
			$xlsx = new SimpleXLSX($target_path);
			$rawData = $xlsx->rowsEx();
			list($cols,) = $xlsx->dimension();    
			$totalDataCount = count($rawData);
			foreach($rawData as $k => $r) {
				for( $i = 0; $i < $cols; $i++)
					$data[$k][$i] = (isset($r[$i])) ? $r[$i]['value'] : 'NULL';
			}		
		}
		
		unlink($target_path);  
					
		if(count($data)>0){
			$this->AuditTrail['remark'] = "Data found.";
		}else{
			$this->AuditTrail['remark'] = "Cannot parse excel file or file is empty.";
			$data = "no data";
		}		
		 
		$this->AddToAuditTrail();		
		
		return $data;
	}
	/* Flight Detail : Update individual record thru import */
	function UpdateFlightDetail($DBName,$postdata){		
		mssql_select_db($DBName); 
 		include_once('./modules/flightdetail/flightfunctions.php');				
		 $funcname = strtolower($DBName);	
		 $flightdetail = new FlightDetail;			
		
		if (is_callable(array($flightdetail,$funcname))){
			echo $flightdetail->$funcname($postdata);		
		} else {
			echo "error";
		}
	}
	/* Flight Detail : Update individual record */
	function UpdateFlightData($DBName,$postdata){
		mssql_select_db($DBName); 		
		include_once('./modules/flightdetail/flightfunctions.php');		
		$vars = $postdata;
		$FlightDetail = new FlightDetail;
		if ($vars['todo'] == 1){
			$this->AuditTrail['action'] = "Filtering Packlist ".$DBName.": ".$vars['packlistnumber'];
			$data = $FlightDetail->checkpacklist($DBName,$vars);
		} else if($vars['todo'] == 2){
			$this->AuditTrail['action'] = "Updating Packlist ".$DBName.": ".$vars['packlistnumber'];
			$data = $FlightDetail->updateflightdata($DBName,$vars);
		}	
		 
		$this->AddToAuditTrail();

		return $data;

		
	}
	/* Flight Detail : Get shipping method */
	function GetShippingMethod($DBName){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "Filtering Shipping Methods ".$DBName;
		
		$sql = mssql_query("SELECT Forwarder,Description FROM Forwarders ORDER BY Description");

		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "No matching record found.";
			$data = 'no data';
		}
		 
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	/* Flight Detail : Get shipping method */
	function GetUpdatedFlightData($DBName,$packlistnumber){

		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "Filtering Packlist Number ".$DBName;
		
		$ordernumberfetch = mssql_fetch_assoc(mssql_query("select documentnumber from packlist where packlistnumber = $packlistnumber "));
		
		if (strtolower($DBName) == "marvellwawi") {				
			$sqlcheckpacklist = mssql_query("select packlistnumber,mawb,hawb,flight,convert(varchar(10),etd,103) as etd,convert(varchar(10),eta,103) as eta,destination,permitno,forwardingagent,netweight,shippingmethod from packlist where documentnumber = ".$ordernumberfetch['documentnumber']);
		}else{		
			$sqlcheckpacklist = mssql_query("select packlistnumber,mawb,hawb,flight,convert(varchar(10),etd,103) as etd,convert(varchar(10),eta,103) as eta,destination,permitno,forwardingagent from packlist where documentnumber = ".$ordernumberfetch['documentnumber']) or die(mssql_get_last_message());
		}
		
		if (!$sqlcheckpacklist) {
			//die('MSSQL error: ' . mssql_get_last_message());
		}
		
		if(mssql_num_rows($sqlcheckpacklist) > 0){
			while($data[] = mssql_fetch_assoc($sqlcheckpacklist));
			 array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "No matching record found.";
			$data = 'no data';
		}
		 
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	/* Consol Scanout : Get the latest consol number */
	function GetLatestConsolNumber($DBName){

		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "Filtering Consol Number ".$DBName;
		
		$sqlgetconsolnumber = mssql_query("select * from setup where [Entry] = 'ConsolidateNumber'");
		
		if(mssql_num_rows($sqlgetconsolnumber) > 0){
			$sqlgetconsolnumber  = mssql_fetch_assoc($sqlgetconsolnumber);
			$data = $sqlgetconsolnumber['Value'];
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "No matching record found.";
			$data = 'no data';
		}
		 
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	/* Consol Scanout : Generate consol number */
	function GenerateConsolNumber($DBName){

		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "Generate Consol Number ".$DBName;
		
		$sqlgenerateconsolnumber = mssql_query("update setup set [Value] = [Value] + 1 where [Entry] = 'ConsolidateNumber'");
		
		if($sqlgenerateconsolnumber){
			$data = $this->GetLatestConsolNumber($DBName);
			$this->AuditTrail['remark'] = "New consol number has been generated: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "Cannot generate consol number";
			$data = 'no data';
		}
		 
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	/* Consol Scanout : Get dimension type */
	function GetDimensionType($DBName){

		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "Filtering Dimension Type ".$DBName;
		
		$sqlgetdimension = mssql_query("select DimensionType from OnlineDimensionTable");
		
		if(mssql_num_rows($sqlgetdimension) > 0){
			while($data[] = mssql_fetch_assoc($sqlgetdimension)){}
				array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "No matching record found.";
			$data = 'no data';
		}
		 
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	/* Consol Scanout : Save data */
	function ConsolScanout($DBName,$postdata,$user){
		mssql_select_db($DBName); 		
		
		$this->AuditTrail['action'] = "Updating consol data ".$DBName;
		
		include_once('./modules/scanout/consolscanoutfunctions.php');				
		$funcname = strtolower($DBName);	
		$consolscanoutobj = new ConsolScanoutClass;			
		
		if (is_callable(array($consolscanoutobj,$funcname))){
			$result =  $consolscanoutobj->$funcname($postdata,$user);		
			$this->AuditTrail['remark'] = "Consol process returns : [".$result."] See consolscanoutfunctions.php for the description";
		} else {
			$result = "unreachable";
			$this->AuditTrail['remark'] = "Consol process returns : ".$result." function";
		} 
				 
		$this->AddToAuditTrail();

		return $result;
		
	}
	
	/* Undo Consol Scanout : revert consol data */
	function UndoConsolScanout($DBName,$postdata){
		mssql_select_db($DBName); 		
		
		$consol = trim($postdata['consolnum']);
		
		$this->AuditTrail['action'] = "Reverting consol data ".$DBName;
		
		if(!isset($consol)) return "6";
		
		$res = mssql_query("SELECT ConsolNumber FROM shipboxes WHERE ConsolNumber = " . $consol); 
		
		if(mssql_num_rows($res)>0){	 
			
			mssql_query("BEGIN TRAN");
			 
			if(mssql_query("DELETE FROM SHIPBOXES WHERE ConsolNumber = " . $consol)){
				if(mssql_query("DELETE FROM BOXWEIGHT WHERE ConsolNumber = " . $consol)){
					if(mssql_query("UPDATE [Order] SET ConsolNumber = NULL WHERE ConsolNumber = " . $consol)){
						if(mssql_query("UPDATE Packlist SET ConsolNumber = 0 WHERE ConsolNumber = " . $consol)){
							$result = "0";						
							$this->AuditTrail['remark'] = "Consol process returns : [0] - Successful database query[ALL] ";
							mssql_query("COMMIT");
						}else{
							$result = "5";
							$this->AuditTrail['remark'] = "Consol process returns : [5] - Cannot update consol Packlist ";
							mssql_query("ROLLBACK");
						}
					}else{
						$result = "4";					
						$this->AuditTrail['remark'] = "Consol process returns : [4] - Cannot update consol in Order ";
						mssql_query("ROLLBACK");
					}
				}else{
					$result = "3";				
					$this->AuditTrail['remark'] = "Consol process returns : [3] - Cannot delete consol from BOXWEIGHT";
					mssql_query("ROLLBACK");
				}
			}else{
				$result = "2";			
				$this->AuditTrail['remark'] = "Consol process returns : [2] - Cannot delete consol from SHIPBOXES";
				mssql_query("ROLLBACK");
			}
		}else{
			$result = "1";
			$this->AuditTrail['remark'] = "Consol process returns : [1] - Consol number in SHIPBOXES does not exist ";
			
		}
		
		$this->AddToAuditTrail();

		return $result;
		
		// Message Description
		// 0 - Successful database query
		// 1 - Consol number in SHIPBOXES doesn't exist
		// 2 - Cannot delete consol from SHIPBOXES
		// 3 - Cannot delete consol from BOXWEIGHT
		// 4 - Cannot update consol in Order
		// 5 - Cannot update consol Packlist
		// 6 - Consol number is empty

	}
	
	/* Undo Book : Get picklist data */
	function GetPicklistData($DBName,$picklist){
		mssql_select_db($DBName);
		
		$picklist = trim($picklist);
		
		$this->AuditTrail['action'] = "$DBName GetPicklistData $picklist";
		
		$ship_res = mssql_query("SELECT Ordernumber FROM Shipboxes WHERE Packlistnumber=" . $picklist);
		if(mssql_num_rows($ship_res)>=1){
			$scanned = "Yes";
		}else{
			$scanned = "No";		
		}
		
		// Get details from Out table
		$res = mssql_query("SELECT (SELECT SUM(Quantity) FROM Out WHERE PackListNumber=o.PackListNumber ) AS 'Total', (SELECT COUNT(PackListNumber) FROM Out WHERE PackListNumber=o.PackListNumber ) AS 'Position', o.PackListNumber,o.OwnOrderNumber,o.Outnumber,o.Customer FROM Out as o WHERE o.PackListNumber=" . $picklist." ");
		
		if(mssql_num_rows($res) > 0){
			$data[0] = mssql_fetch_assoc($res);
			array_push($data,$data[0]['Scanned'] = $scanned);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "No matching record found.";
			$data = 'no data';
		}
		 
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	/* Undo Book : Get paclist position data */
	function GetPackListPosition($DBName,$picklist){
		mssql_select_db($DBName);
		
		$picklist = trim($picklist);
		
		$this->AuditTrail['action'] = "Filtering Packlist position: ".$DBName." Packlist Number: ".$picklist;
		
		$res = mssql_query("SELECT EntryNumber, Quantity, StockIndex FROM [PackListPosition] WHERE PackListNumber=" . $picklist);
		
		if(mssql_num_rows($res) > 0){
			while($data[] = mssql_fetch_assoc($res)){}
				array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "No matching record found.";
			$data = 'no data';
		}
		 
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	/* Undo Book : start undo booking*/
	function UndoBooking($DBName,$postdata){		
		mssql_select_db($DBName);
		
		extract($postdata);
		$picklist = trim($picklist);
		$ordernumber = trim($ordernumber);
		$outnumber = trim($outnumber);
		
		$this->AuditTrail['action'] = "Start undo booking: ".$DBName." Packlist Number: ".$picklist;
		
		//return "SELECT EntryNumber, Quantity, StockIndex FROM [PackListPosition] WHERE PackListNumber=" . $picklist;
		
		$entry_result = mssql_query("SELECT EntryNumber, Quantity, StockIndex FROM [PackListPosition] WHERE PackListNumber=" . $picklist);
		
		
		
		if(mssql_num_rows($entry_result) > 0){
			while($entry_row =mssql_fetch_assoc($entry_result)){
				//place in one string
				$strentry .= $entry_row["EntryNumber"] . ",";
				$strqty .= $entry_row["Quantity"] . ",";
			}
			
			//Place to array
			$arr_entry = explode(",",$strentry);
			$arr_qty = explode(",",$strqty);
			array_pop($arr_entry);
			array_pop($arr_qty);
			
			$rec_ctr = count($arr_entry);
			$ctr=0;
			for($i=0;$i<$rec_ctr;$i++)
			{
				mssql_query("UPDATE [Entry] SET [QuantityOut]=[QuantityOut]-$arr_qty[$i] WHERE [Entrynumber]=$arr_entry[$i] AND [QuantityOut] > 0")or die( "Error: " . mssql_get_last_message());
				$ctr ++;
			}
			mssql_query("BEGIN TRAN");
			
			$sql  = "UPDATE [OrderPosition] SET PackListnumber=0 WHERE Ordernumber=$ordernumber";
			$sql .= "DELETE FROM [EntryOut] WHERE OutNumber=$outnumber";
			$sql .= "DELETE FROM [Out] WHERE PackListnumber=$picklist";
			$sql .= "DELETE FROM [PackListPosition] WHERE PackListnumber=$picklist";
			$sql .= "DELETE FROM [PackList] WHERE PackListnumber=$picklist";
			$sql .= "DELETE FROM [Shipboxes] WHERE PackListnumber=$picklist";
			$sql .= "DELETE FROM [BoxWeight] WHERE PackListnumber=$picklist";
			$sql .= "DELETE FROM [EntriesGivenForOut] WHERE Ordernumber=$ordernumber";
			
			$result = mssql_query($sql);
			
			if ($result){
				mssql_query("COMMIT");
			}else{
				mssql_query("ROLLBACK");
			}
			
			$data = "succeed"; 
			
		}else{
			$data = "1";
		}
		

		
		 
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	/* Cycle Count : partnumber by entry number */
	function GetPNByEntryNumber($DBName,$entrynumber){
		mssql_select_db($DBName);
		
		$entrynumber = trim($entrynumber);
		
		$this->AuditTrail['action'] = "Filtering Entry : ".$DBName." Entry Number: ".$entrynumber;
		
		$res = mssql_query("SELECT PartNumber FROM Entry WHERE EntryNumber=" . $entrynumber);
		
		if(mssql_num_rows($res) > 0){
			while($data[] = mssql_fetch_assoc($res)){}
				array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "No matching record found.";
			$data = 'no data';
		}
		 
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	/* Cycle Count : update entry */
	function UpdateCycleCount($DBName,$postdata,$user){
		mssql_select_db($DBName);
		
		$scanlocation = trim($postdata["location"]);
		$entry = trim($postdata["entrynumber"]);
		$scanPN = trim($postdata["partnumber"]);
		$scanqty = trim($postdata["quantity"]);
		$scandate = date('Y-m-d');
		$scantime = date('H:i');
		$scanuser = trim($user);
		
		$sql = "UPDATE Entry SET ScanLocation='$scanlocation',ScanPartNumber='$scanPN',ScanQuantity=$scanqty,ScanTime='$scantime',ScanDate='$scandate',Scanuser='$scanuser' WHERE EntryNumber=$entry";
		
		$res = mssql_query($sql) or die(mssql_get_last_message());	
		
		$this->AuditTrail['action'] = "$DBName UpdateCycleCount: ".$entry;
		
		if($res && mssql_rows_affected($this->link)){
			$this->AuditTrail['remark'] = "success";
			$data = 'success';
		}else{
			$this->AuditTrail['remark'] = "failed";
			$data = 'failed';
		}
		 
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	/* Cycle Count : Record Cycle Count */
	function RecordCycleCount($DBName,$postdata){

		$this->AuditTrail['action'] = "$DBName RecordCycleCount: ".$postdata['location'].','.$postdata['entrynumber'].','.$postdata['partnumber'].','.$postdata['quantity'];
		
		mssql_select_db($DBName);
		//select if data is already in within a day
		//need to create as procedure
		//2012/04/20 (Y/m/d)
		
		$sql1 = mssql_query("SELECT * FROM CycleCount WHERE ScanDate='".date('Y/m/d')."' AND EntryNumber=".$postdata['entrynumber'].";");
		
		if ($sql1 && mssql_num_rows($sql1)){
			
			$sql2 = mssql_query("UPDATE CycleCount
			SET [Location]='".$postdata['location']."', [Partnumber]='".$postdata['partnumber']."', [Quantity]=".$postdata['quantity'].",
			[Username]='".$postdata['user']."', [ScanTime]='".date('H:i:s')."'
			WHERE EntryNumber=".$postdata['entrynumber']." AND ScanDate='".date('Y/m/d')."';");

		}else{
			$sql2 = mssql_query("INSERT INTO CycleCount ([Location],[EntryNumber],[Partnumber],[Quantity],[Username])
			VALUES ('".$postdata['location']."',".$postdata['entrynumber'].",'".$postdata['partnumber']."',".$postdata['quantity'].",'".$postdata['user']."');");
		}
		
		if($sql1 && $sql2 && mssql_rows_affected($this->link)){
			$this->AuditTrail['remark'] = "success";
			$data = 'success';
		}else{
			$this->AuditTrail['remark'] = "failed";
			$data = 'failed';
		}
		
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	function GetReceivingByEntry($DBName,$entrynumber){
		mssql_select_db($DBName);

		$this->AuditTrail['action'] = "$DBName GetReceivingByEntry $entrynumber";
		
		$res = mssql_query("SELECT * FROM [Entry] WHERE EntryNumber = $entrynumber;");
		
		if($res && mssql_num_rows($res) > 0){
			while($data[] = mssql_fetch_assoc($res)){}
			array_pop($data);
			$this->AuditTrail['remark'] = "Row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "no data";
			$data = 'no data';
		}
		 
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	
	function LogMein($username,$password){
		mssql_select_db(DefaultDB);
		
		


		$this->AuditTrail['action'] = "LogMeIn $username,$password";
		
		$cipher = new cipher();
		$EncryptedPW = $cipher->encrypt($password);
		//echo "execute LogMeIn2 '$username', '$EncryptedPW'";
		$sql = mssql_query("execute LogMeIn2 '$username', '$EncryptedPW'");
		if($sql && mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			
			if (isset($data[0]['Result']) && $data[0]['Result']=='Failed'){
				$this->AuditTrail['remark'] = "Failed";
				$result = 'failed';
				session_destroy();
			}else{
			
				$result = $this->CheckFirstLogin($username);
				if($result == 'success' || is_numeric($result)){
					mssql_query("UPDATE [User] SET lastlogin = '".date('Y-m-d H:i:s')."' WHERE [Username] = '$username' ");
					$_SESSION[LoginSession] = LoginSessionValue;
					$_SESSION[LoginUserVar] = $username;
					$_SESSION['is_manager'] = $this->is_manager($username);
					$_SESSION['is_admin'] = $this->is_admin($username);
					$customerlistres = mssql_query("SELECT WawiAlias FROM MasterData.dbo.WawiCustomer WHERE [status] = 1");
					while($customerlist[] = mssql_fetch_assoc($customerlistres));
					array_pop($customerlist);
					$_SESSION['WawiList'] = $customerlist;
					$this->AuditTrail['remark'] = "Success";
					
				}
			} 
			
		}else {	
			$this->AuditTrail['remark'] = "Failed";
			$result = 'failed';
			session_destroy();
		}
		
		$this->AddToAuditTrail();
		
		return $result;
		
	}
	
	function GetProgramList($username){
		mssql_select_db(DefaultDB);
		
		$this->AuditTrail['action'] = "GetProgramList ".$username;
		
		$sql = mssql_query("SELECT GSTProgram.ID,ProgramIndex,ProgramName FROM UserAccess INNER JOIN GSTProgram ON UserAccess.GSTProgramID = GSTProgram.ID WHERE UserID = '$username' AND GSTProgram.status = 1 GROUP BY GSTProgram.ID,ProgramIndex,ProgramName ORDER BY GSTProgram.ID");
		
		if ($sql && mssql_num_rows($sql)){
			while($data[]=mssql_fetch_assoc($sql));
			array_pop($data);
		}else if ($sql){
			$data = "no data";
		}else{
			$data = "error";
		} 
		$this->AuditTrail['remark'] = json_encode($data);
		$this->AddToAuditTrail();
		return $data;
	}
	
	function CheckFirstLogin($username){
		mssql_select_db(DefaultDB);
		
		$this->AuditTrail['action'] = "User $username";
		
		$sql = mssql_query("SELECT DATEDIFF(day,GETDATE(),PasswordValidity) as datedifference, * FROM [User] WHERE Username = '$username' "); 
		
		if($sql && mssql_num_rows($sql) > 0){
			$row = mssql_fetch_assoc($sql);
			$this->AuditTrail['remark'] = "Success";
			if($row['datedifference'] <= 0 && $row['FirstLogin']){
				$result = 'expired'; 
			}else if($row['datedifference'] <= PASSWORD_VALIDITY_TRESHOLD && $row['datedifference'] > 0 && $row['FirstLogin']){
				$result = $row['datedifference'];
			}else if( $row['datedifference'] > PASSWORD_VALIDITY_TRESHOLD && $row['FirstLogin']){
				$result = 'success';
			}else{
				$result = 'first';
			}
		}else {	
			$this->AuditTrail['remark'] = "Failed";
			$result = 'failed';
		}
		
		$this->AddToAuditTrail();
		
		return $result;
		
	}
	
	function CheckUserExist($username){
		mssql_select_db(DefaultDB);
		
		$this->AuditTrail['action'] = "User $username";
		
		$sql = mssql_query("SELECT * FROM [User] WHERE Username = '$username'") or die(mssql_get_last_message());
		
		if($sql && mssql_num_rows($sql) > 0){
			$this->AuditTrail['remark'] = "Username [$username] exists";
			$result = true;
		}else {	
			$this->AuditTrail['remark'] = "Username [$username] does not exists";
			$result = false;
		}
		
		$this->AddToAuditTrail();
		
		return $result;
	}
	
	function CheckUserCurrentPassword($username,$password){
		mssql_select_db(DefaultDB);
		
		$this->AuditTrail['action'] = "User $username";
		
		$cipher = new cipher();
		$enpassword = $cipher->encrypt($password);
		 
		$sql = mssql_query("SELECT * FROM [User] WHERE Username = '$username' AND Password = '$enpassword'");
		
		if($sql && mssql_num_rows($sql) > 0){
			$this->AuditTrail['remark'] = "Successful filter row count.".count($row);
			$result = true;
		}else {	
			$this->AuditTrail['remark'] = "No matching record found";
			$result = false;
		}
		
		$this->AddToAuditTrail();
		
		return $result;
	}
	
	function UpdateUserFirstLogin($username,$postdata){
		mssql_select_db(DefaultDB);
		$this->AuditTrail['action'] = "User $username";
		
		$curpassword = isset($postdata['curpassword']) ? trim($postdata['curpassword']) : null;
		$newpassword = isset($postdata['newpassword']) ? trim($postdata['newpassword']) : null;
		if($this->CheckUserExist($username)){
			if($curpassword != $newpassword){
				if($this->CheckUserCurrentPassword($username,$curpassword)){
					$cipher = new cipher();
					$newpassword = $cipher->encrypt($newpassword);
					$passwordvalidity = date('Y-m-d H:m:s', strtotime("+".PASSWORD_VALIDITY." days"));
					$sql = mssql_query("UPDATE [User] SET FirstLogin = 1, Password = '$newpassword', PasswordValidity = '$passwordvalidity'  WHERE Username = '$username'");
					
					if($sql){
						$this->AuditTrail['remark'] = "Successful update";
						$result = 'success';
						$res = mssql_query("SELECT Username,Name,EMail,Password FROM [User] WHERE Username = '$username'");
						if(mssql_num_rows($res) > 0){
							$data = mssql_fetch_assoc($res);
							
							$password = $data['Password'];
							
							$cipher = new cipher('andrew');
							$password = $cipher->decrypt($password);
							
							$res = $this->LogMeIn($username,$password);
							
							require_once 'Mailer/class.phpmailer.php';
							$error = 0;
							$mail = new PHPMailer(true); 		 		
							$mail->IsSMTP(); 	
							$this->AuditTrail['action'] = "Sending Message. To:".$data['EMail'].", from: ".MAIL_SUPPORT;
							try {
								//$mail->Host = SMTP_HOST;
								$mail->AddAddress($data['EMail'],$data['Name']);
								$mail->SetFrom(MAIL_SUPPORT,MAIL_SUPPORT_NAME);
								$mail->Subject = 'iWMS Change Password';
								$mail->MsgHTML('
											<p>Hi '.$data['Name'].',</p>
											<p>Here\'s your login detail.</p>
											<table>
											<tr><td>Username:</td><td>'.$data['Username'].'</td></tr>
											<tr><td>Password:</td><td>'.$password.'</td></tr> 
											</table>							
											<p>You can login <a href="'.SSL.SERVER_HOST.'/'.FOLDER.'/index.php" >here</a>.</p>
											<p>Please keep it as confidential.</p>
											<br/>
											<p>Thank You.</p>
											<p>Dev Team</p>							
										');				
								$mail->Send();
								
								$this->AuditTrail['remark'] = "Message Sent. To:".$data['EMail'].", from: ".MAIL_SUPPORT;
								
								$data = "success";
								
							} catch (phpmailerException $e) {
								$this->AuditTrail['remark'] = "Message Sending Failed. To:".$data['EMail'].", from: ".MAIL_SUPPORT;
								$data = $e->errorMessage(); 
							} catch (Exception $e) {
								$this->AuditTrail['remark'] = "Message Sending Failed. To:".$data['EMail'].", from: ".MAIL_SUPPORT;
								$data = $e->getMessage(); 
							}		
							
							$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
						}else{
							$this->AuditTrail['remark'] = "No matching record found.";
							$data = 'no data';
						}
					}else {	
						$this->AuditTrail['remark'] = "Could not update password";
						$result = 'failed';
					}
				}else{
					$this->AuditTrail['remark'] = "Invalid current password";
					$result = 'invalid';
				}
			}else{
					$this->AuditTrail['remark'] = "Current and new password are the same";
					$result = 'samepassword'; 
			}
		}else{
			$result = 'failed';
		}	
		
		$this->AddToAuditTrail();
		
		return $result;
		
	}
	
	function GetGSTModules($username,$GSTProgramID){
		mssql_select_db(DefaultDB);
		
		$this->AuditTrail['action'] = "GetGSTModules $username,$GSTProgramID";
		$sql = mssql_query("select UserAccess.GSTProgramID as ID,GSTProgram.ProgramIndex,GSTProgram.ProgramName,MenuID,GSTMenuAccess.MenuName,GSTMenuAccess.MenuCategory,GSTMenuAccess.OrderInCategory,GSTMenuAccess.MenuURL 
from UserAccess,GSTMenuAccess,GSTProgram,ProgramModule 
where UserAccess.UserID='$username' and UserAccess.GSTProgramID='$GSTProgramID '
and UserAccess.[Status]=1 and GSTMenuAccess.MenuStatus=1 and GSTProgram.[status]=1
and UserAccess.GSTModuleID=GSTMenuAccess.MenuID
and UserAccess.GSTProgramID=GSTProgram.ID 
and UserAccess.GSTProgramID=ProgramModule.ProgramID
and UserAccess.GSTModuleID=ProgramModule.ModuleID
and ProgramModule.Status=1 and ProgramModule.systype='gst' order by MenuCategory,OrderInCategory");
 
		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		} else {	
			$this->AuditTrail['remark'] = "No Data";
			$data = 'no data';
		}
		
		$this->AddToAuditTrail();
		
		return $data;
	} 
	
	
	
	//select MenuID,MenuName,MenuURL from UserAccess,MenuAccess where UserID='andrew' and WawiID=30 and [Status]=1 and MenuStatus=1 and MenuAccessID=MenuID
	
	/* Support */
	function ContactSupport($postdata,$files,$user){
		error_reporting(E_ALL);
		ini_set("display_errors", 1); 	
		require_once 'Mailer/class.phpmailer.php';
		$error = 0;
		$user = $this->getUserInfo($user);
		$mail = new PHPMailer(true); 		 		
		$mail->IsSMTP(); 	

		$this->AuditTrail['action'] = "Sending Message. To:".MAIL_SUPPORT.", from: ".$user['EMail'];
		
		try {
			$customer = trim($postdata['customer']);
			$category = trim($postdata['category']);
			$subject = trim($postdata['subject']);
			$message = trim(nl2br($postdata['message']));
			//$mail->Host = SMTP_HOST;
			$mail->AddAddress(MAIL_SUPPORT,MAIL_SUPPORT_NAME);
			$mail->SetFrom($user['EMail'],$user['Name']);
			$mail->Subject =  $customer ." - ". $subject ." (".$category.")";
			$mail->MsgHTML($message); 
			$filepaths = array();
			if(count($files['attachment']['name']) > 0){	
				$attachmentcount = count($files['attachment']['name']);
				for($i = 0; $i < $attachmentcount ; $i++){
					if($files['attachment']['name'][$i] != ''){
						$path_of_uploaded_file = "./tmp/" . basename($files['attachment']['name'][$i]);
						$tmp_path = $files['attachment']['tmp_name'][$i];			
						if(is_uploaded_file($tmp_path))
						{
							$input = fopen($tmp_path, "r");
							$temp = tmpfile();
							$realSize = stream_copy_to_stream($input, $temp);
							fclose($input);	
							
							$target = fopen($path_of_uploaded_file, "w");        
							fseek($temp, 0, SEEK_SET);
							stream_copy_to_stream($temp, $target);
							fclose($target);
							
							$mail->AddAttachment($path_of_uploaded_file,basename($files['attachment']['name'][$i]));			 
						}else{
							$error = 'Attachment Error';
							throw new Exception($error);
						}
						$filepaths[] = $path_of_uploaded_file;
					}
				}
			}
			
			if($this->LogSupportMessage($user['Username'],MAIL_SUPPORT,$user['EMail'],$subject,$category,$message)){
				$mail->Send();
			}else{
				throw new Exception('Could not save message to database.');			
			}
			
			$this->AuditTrail['remark'] = "Message Sent. To:".MAIL_SUPPORT.", from: ".$user['EMail'];
			
			foreach($filepaths as $file){
				unlink($file);
			}			
			$data = "success";
			
		} catch (phpmailerException $e) {
			$this->AuditTrail['remark'] = "Message Sending Failed. To:".MAIL_SUPPORT.", From: ".$user['EMail'];
			$data = $e->errorMessage(); 
		} catch (Exception $e) {
			$this->AuditTrail['remark'] = "Message Sending Failed. To:".MAIL_SUPPORT.", From: ".$user['EMail'];
		    $data = $e->getMessage(); 
		}
		
		$this->AddToAuditTrail();
			 
		return $data;
		
	}
	
	function LogSupportMessage($user,$to,$from,$subject,$category,$message){
		mssql_select_db(DefaultDB);
		$this->AuditTrail['action'] = "Saving support log.";
		
		$sqlinsert = "INSERT INTO SupportLog (UserID,FromEmail,ToEmail,Subject,Category,Message) VALUES('$user','$to','$from','$subject','$category','$message')";
		if(mssql_query($sqlinsert)){
			$this->AuditTrail['remark'] = "Message saved successfully";
			$result =  true;
		}else{
			$this->AuditTrail['remark'] = "Could not save message.";
			$result = false;
		}
		$this->AddToAuditTrail();
		return $result;
	}
	
	/* GET User Info */
	
	function getUserInfo($user){
		mssql_select_db(DefaultDB);
		
		$this->AuditTrail['action'] = "Filtering User : ".DefaultDB." Username : ".$user;
		
		$res = mssql_query("SELECT Username,Name,EMail FROM [".DefaultDB."].[dbo].[User] WHERE Username = '" . trim($user)."'");
		
		if(mssql_num_rows($res) > 0){
			$data = mssql_fetch_assoc($res);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "No matching record found.";
			$data = 'no data';
		}
		 
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	
	/* I Forgot */
	
	function iForgot($username){
		mssql_select_db(DefaultDB);
		
		$this->AuditTrail['action'] = "Filtering User : ".DefaultDB." Username : ".$username;
		
		$res = mssql_query("SELECT Username,Name,EMail,Password FROM [".DefaultDB."].[dbo].[User] WHERE username = '" . trim($username)."'");
		
		if(mssql_num_rows($res) > 0){
			$data = mssql_fetch_assoc($res);
			
			$password = $data['Password'];
			
			$cipher = new cipher('andrew');
			
			
			//var_dump($password); die();
			
			$password = $cipher->decrypt($password);
			
			require_once 'Mailer/class.phpmailer.php';
			$error = 0;
			$mail = new PHPMailer(true); 		 		
			$mail->IsSMTP(); 	
			$this->AuditTrail['action'] = "Sending Message. To:".$data['EMail'].", from: ".MAIL_SUPPORT;
			try {
				//$mail->Host = SMTP_HOST;
				//$mail->Port = 465;
				//$mail->SMTPAuth = true; 
				$mail->AddAddress($data['EMail'],$data['Name']);
				$mail->SetFrom(MAIL_SUPPORT,MAIL_SUPPORT_NAME);
				$mail->Subject = 'iForgot : Login Detail';
				$mail->MsgHTML('
							<p>Hi '.$data['Name'].',</p>
							<p>Here\'s your login detail.</p>
							<table>
							<tr><td>Username:</td><td>'.$data['Username'].'</td></tr>
							<tr><td>Password:</td><td>'.$password.'</td></tr>
							</table>							
							<p>You can login <a href="'.SSL.SERVER_HOST.'/'.FOLDER.'/index.php" >here</a>.</p>
							<p>Please keep it as confidential.</p>
							<br/>
							<p>Thank You.</p>
							<p>Dev Team</p>							
						');				
				$mail->Send();
				
				$this->AuditTrail['remark'] = "Message Sent. To:".$data['EMail'].", from: ".MAIL_SUPPORT;
				
				$data = "success|".$data['EMail'];
				
			} catch (phpmailerException $e) {
				$this->AuditTrail['remark'] = "Message Sending Failed. To:".$data['EMail'].", from: ".MAIL_SUPPORT;
				$data = $e->errorMessage(); 
			} catch (Exception $e) {
				$this->AuditTrail['remark'] = "Message Sending Failed. To:".$data['EMail'].", from: ".MAIL_SUPPORT;
				$data = $e->getMessage(); 
			}		
			
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "No matching record found.";
			$data = 'no data';
		}
		 
		$this->AddToAuditTrail(); 
		
		return $data;
	}
	
	function UndoReceiving($DBName,$entrynumber){

		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "$DBName undo receiving with entrynumber $entrynumber";
		
		$sql1 = mssql_query("SELECT * FROM [Entry] WHERE EntryNumber=$entrynumber;");
		
		if(mssql_num_rows($sql1) > 0){
			$sql2 = mssql_query("SELECT * FROM [Entry] WHERE [Entry].EntryNumber=".$entrynumber." and ([Entry].QuantityOut=0 AND [Entry].EntryNumber NOT IN (SELECT PackListPosition.EntryNumber FROM PackListPosition GROUP BY PackListPosition.EntryNumber));");
			if(mssql_num_rows($sql2) > 0){
				$sql3 = mssql_query("DELETE FROM [Entry] WHERE EntryNumber=$entrynumber AND QuantityOut=0 AND EntryNumber NOT IN (SELECT PackListPosition.EntryNumber FROM PackListPosition GROUP BY PackListPosition.EntryNumber);");
				
				if($sql3 && mssql_rows_affected($this->link)){
					$this->AuditTrail['remark'] = "Successful undo receiving";
					$data = 'success';
				}else{
					$this->AuditTrail['remark'] = "Failed to undo receiving";
					$data = 'failed';
				}
				
			}else{
				$this->AuditTrail['remark'] = "entry number already allocated";
				$data = 'used';
			}
		}else{
			$this->AuditTrail['remark'] = "entry number does not exist";
			$data = 'no data';
		}
		 
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	/* Parse barcode string*/
	function Parse2DBarcodeReceiving($DBName,$barcode){
		if(strtolower($DBName) == 'volterrawawitest' || strtolower($DBName) == 'volterrawawi'){
			$barcode = strtoupper($barcode);
			//Find position of Prefix as delimeters	 
			$pos1=strpos($barcode,"1P");
			$pos2=strpos($barcode,"1T");
			$pos3=strpos($barcode,"21L");
			$pos4=strpos($barcode,"20L");
			$pos5=strpos($barcode,"31T");
			$pos6=strpos($barcode,"9D");
			$pos7=strpos($barcode,"Q");
			$pos8=strripos($barcode,"31T");
			$pos9=strripos($barcode,"9D");
			$pos10=strripos($barcode,"Q");
			//Get length of the string per field
			$end1 = $pos2-$pos1;
			$end2 = $pos3-$pos2;
			$end3 = $pos4-$pos3;
			$end4 = $pos5-$pos4;
			$end5 = $pos6-$pos5;
			$end6 = $pos7-$pos6;
			$end7 = $pos8-$pos7;
			$end8 = $pos9-$pos8;
			$end9 = $pos10-$pos9;
			$end10 = strlen($barcode);
			//Trim barcode
			$data['MfrPN']		= substr($barcode,$pos1+2,$end1-2);
			$data['tracecode']	= substr($barcode,$pos2+2,$end2-2);
			$data['prodof']		= substr($barcode,$pos3+3,$end3-3);
			$data['chipsfrom']	= substr($barcode,$pos4+3,$end4-3);
			$data['lotno1']		= substr($barcode,$pos5+3,$end5-3);
			$data['dc1']		= substr($barcode,$pos6+2,$end6-2);
			$data['qty1']		= substr($barcode,$pos7+1,$end7-1);
			$data['lotno2']		= substr($barcode,$pos8+3,$end8-3);
			$data['dc2']		= substr($barcode,$pos9+2,$end9-2);
			$data['qty2']		= substr($barcode,$pos10+1,$end10-1);	
			return $data;
		}else if(strtolower($DBName) == 'demowawi'){
			$barcode = explode('|',$barcode);
			//$data['EntryNumber'] = $barcode[0];
			$data['ForwarderRef'] = $barcode[0];
			$data['Quantity'] = $barcode[1];
			$data['TaxScheme'] = $barcode[2];
			$data['Partnumber'] = $barcode[3];
			$data['StockIndex'] = $barcode[4];
			$data['CountryOfOrigin'] = $barcode[5];
			$data['ChipFrom'] = $barcode[6];
			$data['Supplier'] = $barcode[7];
			return $data;
		}else{
			return 'no data';
		}
		
	}
	
	/* Get the supplier data */
	function GetReceivingSupplier($DBName){
		mssql_select_db($DBName);		
		$this->AuditTrail['action'] = "Filtering Supplier table :".$DBName;		
		$sql = mssql_query("SELECT [Index] FROM Supplier ORDER BY [Index]");		
		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);		
		}else{
			$this->AuditTrail['remark'] = "No matching record";
			$data = 'no data';
		}		 
		$this->AddToAuditTrail();		
		return $data;
	}
	
	/* Get receiving stock index */
	function GetReceivingStockIndex($DBName){
		mssql_select_db($DBName);		
		$this->AuditTrail['action'] = "Filtering Stocks table :".$DBName;		
		$sql = mssql_query("SELECT [Index] FROM Stocks ORDER BY [Index]");		
		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);		
		}else{
			$this->AuditTrail['remark'] = "No matching record";
			$data = 'no data';
		}		 
		$this->AddToAuditTrail();		
		return $data;
	}
	
	/* Get receiving tax scheme */
	function GetReceivingTaxScheme($DBName){
		mssql_select_db($DBName);		
		$this->AuditTrail['action'] = "Filtering TAXSCHEME table :".$DBName;		
		$sql = mssql_query("SELECT TAXSCHEME FROM TAXSCHEME ORDER BY TAXSCHEME");		
		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);		
		}else{
			$this->AuditTrail['remark'] = "No matching record";
			$data = 'no data';
		}		 
		$this->AddToAuditTrail();		
		return $data;
	}
	
	/* Save 2D receiving details */
	function Save2DReceiving($DBName,$post,$user){
		mssql_select_db($DBName);		
		$this->AuditTrail['action'] = "Inserting data to Entry table :".$DBName;		
		
		$forwarderref	=	trim($post["ForwarderRef"]);
		$supplier		=	trim($post["Supplier"]);
		$invoice		=	trim($post["Invoice"]);
		$stockindex		=	trim($post["StockIndex"]);
		$taxscheme		=	trim($post["TaxScheme"]);
		$partnumber		=	trim($post["PartNumber"]);
		$supplierpartnumber = trim($post["SupplierPartNumber"]);
		$location		=	trim($post["Location"]);
		$seal			=	trim($post["Seal"]);
		$productof		=	trim($post["ProductOf"]);
		$chipfrom		=	trim($post["ChipFrom"]);
		$entrynumber1	=	trim($post["EntryNumber1"]);
		$lotnumber1		=	trim($post["LotNumber1"]);
		$datecode1		=	trim($post["DateCode1"]);
		$quantity1		=	trim($post["Quantity1"]);
		$masterbox		=	trim($post["MasterBox"]) == '-' ? '' : trim($post["MasterBox"]);
		$supplierorder	=	trim($post["SupplierOrder"]);
		$sealdate		=	trim($post["SealDate"]);
		$whoscan		=	trim($user); 

		$entrynumber2	=	trim($post["EntryNumber2"]);
		$lotnumber2		=	trim($post["LotNumber2"]);
		$datecode2		=	trim($post["DateCode2"]);
		$quantity2		=	trim($post["Quantity2"]);

		if($this->CheckPartNumber($partnumber,$DBName)){		
			if($entrynumber1!=''&&$lotnumber1!=''&&$datecode1!=''&&$quantity1!=''){
				$qry = "INSERT INTO Entry (ForwarderRef,Supplier,Invoice,StockIndex,TaxScheme,PartNumber,SupplierPartNumber,Location,Seal,ProductOf,ChipFrom,EntryNumber,LotNumber,DateCode,Quantity,MasterBox,SupplierOrder,SealDate,WhoScan,EntryDate) VALUES ('$forwarderref','$supplier','$invoice','$stockindex','$taxscheme','$partnumber','$supplierpartnumber','$location','$seal','$productof','$chipfrom',$entrynumber1,'$lotnumber1','$datecode1',$quantity1,'$masterbox','$supplierorder','$sealdate','$whoscan','".date('Y-m-d H:i:s')."')";
				$res = mssql_query($qry);
			}

			if($entrynumber2!=''&&$lotnumber2!=''&&$datecode2!=''&&$quantity2!=''){
				$qry2 = "INSERT INTO Entry (ForwarderRef,Supplier,Invoice,StockIndex,TaxScheme,PartNumber,SupplierPartNumber,Location,Seal,ProductOf,ChipFrom,EntryNumber,LotNumber,DateCode,Quantity,MasterBox,SupplierOrder,SealDate,WhoScan,EntryDate) VALUES ('$forwarderref','$supplier','$invoice','$stockindex','$taxscheme','$partnumber','$supplierpartnumber','$location','$seal','$productof','$chipfrom',$entrynumber2,'$lotnumber2','$datecode2',$quantity2,'$masterbox','$supplierorder','$sealdate','$whoscan','".date('Y-m-d H:i:s')."')";
				$res2 = mssql_query($qry2);
			}
			if($res || $res2){
				$data = 'success';
				$this->AuditTrail['remark'] = "Success ".serialize($post);		
			}else{
				$this->AuditTrail['remark'] = "Could not save data.";
				$data = 'no data';
			}		 
		}else{
			$data = "Partnumber does not exist";
		}	 
		$this->AddToAuditTrail();		
		return $data;
	}
	
	/* Get Cycle Count Report */
	function GetCycleCountReport($DB,$stockindex,$filter,$scandate){
		
		$DBName = $DB[0]['DBName'];
		$WawiIndex = $DB[0]['WawiIndex'];
		
		mssql_select_db(DefaultDB);		
		$this->AuditTrail['action'] = "$DBName GetCycleCountReport $filter,$stockindex,$scandate";	
		
		$param = '';
		
		switch ($filter){
			case '1':
				$sql = "execute CycleCountReport '".$DBName."',2,'".$stockindex."','".$scandate."','','".$param."'";
			break;
			case '2':
				$param = 'over';
				$sql = "execute CycleCountReport '".$DBName."',1,'".$stockindex."','".$scandate."','quantity','".$param."'";
			break;
			case '3':
				$param = 'less';
				$sql = "execute CycleCountReport '".$DBName."',1,'".$stockindex."','".$scandate."','quantity','".$param."'";
			break;
			case '4':
				$param = '';
				$sql = "execute CycleCountReport '".$DBName."',1,'".$stockindex."','".$scandate."','partnumber','".$param."'";
			break;
			case '5':
				$param = '';
				$sql = "execute CycleCountReport '".$DBName."',1,'".$stockindex."','".$scandate."','location','".$param."'";
			break;
			case '6':
				$param = '';
				$sql = "execute CycleCountReport '".$DBName."',3,'".$stockindex."','".$scandate."','','".$param."'";
			break;
			case '7':
				$param = '';
				$sql = "execute CycleCountReport '".$DBName."',4,'".$stockindex."','".$scandate."','','".$param."'";
			break;
		}
		//echo $sql;
		
		$result = mssql_query($sql);	
		
		if (mssql_num_rows($result) > ReportLimit){
			
			$params = array("DBName" => $DBName,"stockindex" => $stockindex,"scandate" => $scandate,"filter" => $filter,"WawiIndex" => $WawiIndex, "rptType" => 2);
			
			if ($this->RequestEmailReport($params,1)=='success'){
				$data = 'exceed limit';
				$this->AuditTrail['remark'] = 'exceed limit-request email';
			}else{
				$data = 'exceed limit-error';
				$this->AuditTrail['remark'] = 'exceed limit-error';
			}
			
		}else{
			if(mssql_num_rows($result) > 0){
				while($data[] = mssql_fetch_assoc($result));
				array_pop($data);
				$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);		
			}else{
				$this->AuditTrail['remark'] = "No record found";
				$data = 'no data';
			}		 
		}
		
		$this->AddToAuditTrail();	
		return $data;
	}
	
	function RequestEmailReport($params,$requestLvl){
		
		mssql_select_db(DefaultDB);
		
		$DT = date('YmdHis');
		
		switch($params['rptType']){
			case 1:
			case 3:
				$filterval = trim($params['filter']);
				$range_from_val = trim($params['range_from']);
				$range_to_val = trim($params['range_to']);
				
				if ($filterval == ""){
					$p = "1=1";
					if ($range_from_val != '' && $range_to_val != ''){
						$p .= " AND [".$params['field']."] >= '".$range_from_val."' AND [".$params['field']."] <= '".$range_to_val."'";
					}
				} else {
					if (substr_count($filterval, '/') == 2){
						$p = "[".$params['field']."] = '".$filterval."'";
					}elseif(is_numeric($filterval)){
						$p = "[".$params['field']."] = '".$filterval."'";
					}
					else{
						$p = "[".$params['field']."] like '".$filterval."'";
					}
				}
				
				$p .= (isset($params['sort_by']) && $params['sort_by']!='' && isset($params['sort_type'])) ? ' ORDER BY ['.$params['sort_by'].'] '.$params['sort_type']: '';
				
				$procedure = ($params['rptType']==3) ? 'ASMainReport2': 'MainReport2';
				
				$sql = "execute $procedure ".$params['WawiID'].",\"".$params['DBName']."\",\"".$params['reportname']."\",\" where ".str_replace("'","''",$p)."\"";
				
				$sqlemail = "INSERT INTO ReportRequest ([dbname],[sql],[email],[email_title],[file_name],[email_name],[type])
				VALUES ('".$params['DBName']."','".$sql."', (select top 1 [EMail] from [User] where Username='".$_SESSION[LoginUserVar]."'), 'Report Request - ".$params['WawiIndex']."-".$params['reportname']."', '".$params['WawiIndex']."-".$params['reportname']."-".$DT."', (select top 1 [Name] from [User] where Username='".$_SESSION[LoginUserVar]."'), 'report');";
				
				$result = mssql_query($sqlemail);
				
				if($result && mssql_rows_affected($this->link)){
					$data = 'success';
					if ($requestLvl==0){ $this->AuditTrail['remark'] = 'success';}
				}else{
					$data = 'failed';
					if ($requestLvl==0){ $this->AuditTrail['remark'] = 'failed';}
				}
				
			break;
			
			case 2:
							
				$param = '';
				switch ($params['filter']){
					case '1':
						$sql = "execute CycleCountReport '".$params['DBName']."',2,'".$params['stockindex']."','".$params['scandate']."','','".$param."'";
					break;
					case '2':
						$param = 'over';
						$sql = "execute CycleCountReport '".$params['DBName']."',1,'".$params['stockindex']."','".$params['scandate']."','quantity','".$param."'";
					break;
					case '3':
						$param = 'less';
						$sql = "execute CycleCountReport '".$params['DBName']."',1,'".$params['stockindex']."','".$params['scandate']."','quantity','".$param."'";
					break;
					case '4':
						$param = '';
						$sql = "execute CycleCountReport '".$params['DBName']."',1,'".$params['stockindex']."','".$params['scandate']."','partnumber','".$param."'";
					break;
					case '5':
						$param = '';
						$sql = "execute CycleCountReport '".$params['DBName']."',1,'".$params['stockindex']."','".$params['scandate']."','location','".$param."'";
					break;
					case '6':
						$param = '';
						$sql = "execute CycleCountReport '".$params['DBName']."',3,'".$params['stockindex']."','".$params['scandate']."','','".$param."'";
					break;
					case '7':
						$param = '';
						$sql = "execute CycleCountReport '".$params['DBName']."',4,'".$params['stockindex']."','".$params['scandate']."','','".$param."'";
					break;
				}
				
				if ($requestLvl==0){
					$this->AuditTrail['action'] = $params['DBName']." RequestEmailReport ".$params['filter'].",".$params['stockindex'].",".$params['scandate'].",".$param;	
				}
			
				$sqltemp = str_replace("'","''",$sql);
				
				$sqlemail = "INSERT INTO ReportRequest ([dbname],[sql],[email],[email_title],[file_name],[email_name],[type])
				VALUES ('".$params['DBName']."','".$sqltemp."', (select top 1 [EMail] from [User] where Username='".$_SESSION[LoginUserVar]."'), 
				'".$params['WawiIndex']." Cycle Count [".$params['stockindex']."] - Report Request', 
				'".$params['WawiIndex']." Cycle Count - ".str_replace('/','',$params['scandate']).$params['filter'].$param.$DT."', 
				(select top 1 [Name] from [User] where Username='".$_SESSION[LoginUserVar]."'), 'cyclecount');";
				
				$result = mssql_query($sqlemail);
				
				if($result && mssql_rows_affected($this->link)){
					$data = 'success';
					if ($requestLvl==0){ $this->AuditTrail['remark'] = 'success';}
				}else{
					$data = 'failed';
					if ($requestLvl==0){ $this->AuditTrail['remark'] = 'failed';}
				}
				
			break;
		}//switch
		
		if ($requestLvl==0){ $this->AddToAuditTrail();}
		return $data;
	}
	
	
	
	/* Unlock Order */
	function UnlockOrder($DBName,$ordernumber){
		mssql_select_db($DBName);
		$this->AuditTrail['action'] = "$DBName UnlockOrder $ordernumber";
		
		
		$sql=mssql_query("select OrderNumber from [Order] where OrderNumber=".$ordernumber.";");
		if ($sql && mssql_num_rows($sql)){

			$sql2=mssql_query("select freeforwarehousedate,freeforwarehousetime,freeforwarehousename from [Order] where OrderNumber not in (select distinct OrderNumber from Shipboxes) and OrderNumber=".$ordernumber.";");
			
			if ($sql2 && mssql_num_rows($sql2)){
				$sql3=mssql_query("UPDATE [Order] set freeforwarehousedate=null, freeforwarehousetime=null, freeforwarehousename=null WHERE OrderNumber=".$ordernumber.";");

				if($sql3 && mssql_rows_affected($this->link)){
					$data = 'success';
				}else{
					$data = 'failed';
				}
			}else{
				$data = 'shipped';
			}
			
		}else{
			$data = 'no data';
		}
		$this->AuditTrail['remark'] = $data;
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	/* Get Stocks */
	function GetStocks($DBName,$stock){
		mssql_select_db($DBName);
		$this->AuditTrail['action'] = "$DBName GetStocks $stock";
		
		$filter = ($stock!='null') ? "WHERE [Index]='$stock'": "";
		
		$sql=mssql_query("SELECT * FROM [Stocks] $filter ORDER BY [Index] ASC;");
		if ($sql && mssql_num_rows($sql)){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$data = 'no data';
			$this->AuditTrail['remark'] = "no data";
		}
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	/* Update Stock Index */
	function UpdateStockIndex($DBName,$post){
		mssql_select_db($DBName);
		$this->AuditTrail['action'] = "$DBName UpdateStockIndex ".$post['txtindex'];
		
		if (isset($post['chkcalculate']) && $post['chkcalculate']=='on'){
			$post['chkcalculate'] = 1;
		}else{
			$post['chkcalculate'] = 0;
		}
		
		if (isset($post['chkscanonout']) && $post['chkscanonout']=='on'){
			$post['chkscanonout'] = 1;
		}else{
			$post['chkscanonout'] = 0;
		}
		
		$sql=mssql_query("SELECT * FROM [Stocks] WHERE [Index] = '".$post['txtindex']."';");
		
		if ($sql && mssql_num_rows($sql)){

			$sql2 = mssql_query("UPDATE [Stocks] SET [Description]='".$post['txtdescription']."',[Address1]='".$post['txtaddress1']."',[Address2]='".$post['txtaddress2']."',[Street]='".$post['txtstreet']."',[Country]='".$post['txtcty']."',[Zip]='".$post['txtzip']."',[City]='".$post['txtcity']."',[Location]='".$post['txtloc']."',[Calculate]='".$post['chkcalculate']."',[ScanOnout]='".$post['chkscanonout']."',[EMailOnOut]='".$post['txtemailout']."',[EmergencyPhone]='".$post['txtemerg']."'
			WHERE [Index]='".$post['txtindex']."';");
			
			if($sql2 && mssql_rows_affected($this->link)){
				$this->AuditTrail['remark'] = "Success";
				$result = 'success';
			} else {
				$this->AuditTrail['remark'] = "Failed";
				$result = 'failed';
			}
			
		}else{
			$this->AuditTrail['remark'] = "no data";
			$result = 'no data';
		}

		$this->AddToAuditTrail();
		
		return $result;
	}
	
	/* Save Stock Index */
	function SaveStockIndex($DBName,$post){
		mssql_select_db($DBName);
		$this->AuditTrail['action'] = "$DBName SaveStockIndex ".$post['txtindex'];
		
		if (isset($post['chkcalculate']) && $post['chkcalculate']=='on'){
			$post['chkcalculate'] = 1;
		}else{
			$post['chkcalculate'] = 0;
		}
		
		if (isset($post['chkscanonout']) && $post['chkscanonout']=='on'){
			$post['chkscanonout'] = 1;
		}else{
			$post['chkscanonout'] = 0;
		}
		//echo $DBName;
		//echo '<pre>'; print_r($post); echo '<pre>';
		//echo "SELECT * FROM [Stocks] WHERE [Index] = '".$post['txtindex']."';";
		
		$sql=mssql_query("SELECT * FROM [Stocks] WHERE [Index] = '".$post['txtindex']."';");
		
		if ($sql && mssql_num_rows($sql)){
			$this->AuditTrail['remark'] = "Already Exist";
			$result = 'exist';
		}else{
			
			$sql2 = mssql_query("Insert into [Stocks] ([Index],[Description],[Address1],[Address2],[Street],[Country],[Zip],[City],[Location],[Calculate],[ScanOnout],[EMailOnOut],[EmergencyPhone],[CycleCountName]) values ('".$post['txtindex']."','".$post['txtdescription']."','".$post['txtaddress1']."','".$post['txtaddress2']."','".$post['txtstreet']."','".$post['txtcty']."','".$post['txtzip']."','".$post['txtcity']."','".$post['txtloc']."','".$post['chkcalculate']."','".$post['chkscanonout']."','".$post['txtemailout']."','".$post['txtemerg']."','".$post['txtindex']."');");
		
			if($sql2 && mssql_rows_affected($this->link)){
				$this->AuditTrail['remark'] = "Success";
				$result = 'success';
			} else {
				$this->AuditTrail['remark'] = "Failed";
				$result = 'failed';
			}
		}

		$this->AddToAuditTrail();
		
		return $result;
	}
	
	/* Get Check Entry Number */
	function CheckEntryNumber($DBName,$EntryNumber){
		mssql_select_db($DBName);
		$this->AuditTrail['action'] = "$DBName CheckEntryNumber $EntryNumber";
		
		$sql=mssql_query("SELECT * FROM [Entry] WHERE EntryNumber = ".$EntryNumber.";");
		if ($sql && mssql_num_rows($sql)){
			$data = mssql_fetch_assoc($sql);
			$this->AuditTrail['remark'] = "Success";
		}else{
			$data = 'no data';
			$this->AuditTrail['remark'] = 'no data';
		}

		$this->AddToAuditTrail();
		
		return $data;
	}
	
	/* Get Customer Info */
	function GetCustomer($DBName,$index){
		mssql_select_db($DBName);
		$this->AuditTrail['action'] = "$DBName GetCustomer $index";
		
		$filter = ($index!='null') ? "WHERE [Index]='$index'": "";
		
		$sql=mssql_query("SELECT [Index],Name1,Name2,Street,Country,Zip,City,Affiliate,VATIDNumber,CustomerNumber,IncoTerm,IncoCity,Forwarder,ForwarderAccount,SendInvoice,PriceGroup,PaymentTerms,QuoteValidity,Discount,LineDiscount,CollectiveInvoice,IntraStatNbr,ExtCustomerNumber FROM [Customer] $filter ORDER BY [Index] ASC;");
		if ($sql && mssql_num_rows($sql)){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$data = 'no data';
			$this->AuditTrail['remark'] = "no data";
		}
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	function SellingPriceGroups($DBName){
		mssql_select_db($DBName);
		$this->AuditTrail['action'] = "$DBName SellingPriceGroups";
		$sql = mssql_query("Select [Index] from SellingPriceGroups;");
		
		$row = array();
		
		if ($sql && mssql_num_rows($sql)){
			$this->AuditTrail['remark'] = "Row Count: ".mssql_num_rows($sql);
			while($row[]=mssql_fetch_assoc($sql)){}
			array_pop($row);
		}
		array_push($row,array("Index" => "n/a"));
		

		$this->AddToAuditTrail();
		return $row;
		
	}
	
	function PaymentTerms($DBName){
		mssql_select_db($DBName);
		$this->AuditTrail['action'] = "$DBName PaymentTerms";
		
		$sql = mssql_query("Select PaymentTerms from PaymentTerms;");
		
		$row = array();
		if ($sql && mssql_num_rows($sql)){
			$this->AuditTrail['remark'] = "Row Count: ".mssql_num_rows($sql);
			while($row[]=mssql_fetch_assoc($sql)){}
			array_pop($row);
		}
		
		array_push($row,array("PaymentTerms" => "n/a"));
		
		return $row;
	}
	
	/* Save Customer */
	function SaveCustomer($DBName,$post){
		mssql_select_db($DBName);
		$this->AuditTrail['action'] = "$DBName SaveCustomer ".$post['txtindex'];
		
		$post['chkaffiliate'] = ((isset($post['chkaffiliate']) && $post['chkaffiliate']=='on')) ? 1: 0;
		$post['chkInvoicewithPackage'] = ((isset($post['chkInvoicewithPackage']) && $post['chkInvoicewithPackage']=='on')) ? 1: 0;
		$post['chkcollective'] = ((isset($post['chkcollective']) && $post['chkcollective']=='on')) ? 1: 0;
		
		$post['txtdiscount'] = (is_numeric($post['txtdiscount'])) ? $post['txtdiscount']: 0;
		$post['txtlinediscount'] = (is_numeric($post['txtlinediscount'])) ? $post['txtlinediscount']: 0;


		//echo $DBName;
		//echo '<pre>'; print_r($post); echo '<pre>';
		//echo "SELECT * FROM [Stocks] WHERE [Index] = '".$post['txtindex']."';";
		
		$sql=mssql_query("SELECT * FROM [Customer] WHERE [Index] = '".$post['txtindex']."';");
		
		if ($sql && mssql_num_rows($sql)){
			$this->AuditTrail['remark'] = "Already Exist";
			$result = 'exist';
		}else{

			$sql2 = mssql_query("Insert into Customer ([Index],Name1,Name2,Street,Country,Zip,City,Affiliate,VATIDNumber,CustomerNumber,IncoTerm,IncoCity,Forwarder,ForwarderAccount,SendInvoice,PriceGroup,PaymentTerms,QuoteValidity,Discount,LineDiscount,CollectiveInvoice,IntraStatNbr,ExtCustomerNumber) values ('".$post['txtindex']."','".$post['txtname1']."','".$post['txtname2']."','".$post['txtstreet']."','".$post['txtcty']."','".$post['txtzip']."','".$post['txtcity']."',".$post['chkaffiliate'].",'".$post['txtvatid']."','".$post['txtcustnum']."','".$post['txtincoterm']."','".$post['txtincocity']."','".$post['txtforwarder']."','".$post['txtacct']."',".$post['chkInvoicewithPackage'].",'".$post['cmbPriceGroup']."','".$post['cmbPayment']."','".$post['txtquotevalidity']."','".$post['txtdiscount']."','".$post['txtlinediscount']."',".$post['chkcollective'].",'".$post['txtIntraStat']."','".$post['txtextcustnum']."') ");
		
			if($sql2 && mssql_rows_affected($this->link)){
				$this->AuditTrail['remark'] = "Success";
				$result = 'success';
			} else {
				$this->AuditTrail['remark'] = "Failed";
				$result = 'failed';
			}
		
		}

		$this->AddToAuditTrail();
		
		return $result;
	}
	
	/* Update Customer */
	function UpdateCustomer($DBName,$post){
		mssql_select_db($DBName);
		$this->AuditTrail['action'] = "$DBName UpdateCustomer ".$post['txtindex'];
		
		$post['chkaffiliate'] = ((isset($post['chkaffiliate']) && $post['chkaffiliate']=='on')) ? 1: 0;
		$post['chkInvoicewithPackage'] = ((isset($post['chkInvoicewithPackage']) && $post['chkInvoicewithPackage']=='on')) ? 1: 0;
		$post['chkcollective'] = ((isset($post['chkcollective']) && $post['chkcollective']=='on')) ? 1: 0;
		
		$sql=mssql_query("SELECT * FROM [Customer] WHERE [Index] = '".$post['txtindex']."';");
		
		if ($sql && mssql_num_rows($sql)){

			$sql2 = mssql_query("UPDATE [Customer] SET 
			Name1='".$post['txtname1']."', 
			Name2='".$post['txtname2']."', 
			Street='".$post['txtstreet']."', 
			Country='".$post['txtcty']."', 
			Zip='".$post['txtzip']."', 
			City='".$post['txtcity']."', 
			Affiliate=".$post['chkaffiliate'].", 
			VATIDNumber='".$post['txtvatid']."', 
			CustomerNumber='".$post['txtcustnum']."', 
			IncoTerm='".$post['txtincoterm']."', 
			IncoCity='".$post['txtincocity']."', 
			Forwarder='".$post['txtforwarder']."', 
			ForwarderAccount='".$post['txtacct']."', 
			SendInvoice='".$post['chkInvoicewithPackage']."', 
			PriceGroup='".$post['cmbPriceGroup']."', 
			PaymentTerms='".$post['cmbPayment']."', 
			QuoteValidity='".$post['txtquotevalidity']."', 
			Discount=".( isset($post['txtdiscount']) ? $post['txtdiscount'] : 0.00 ).", 
			LineDiscount=".( isset($post['txtlinediscount']) ? $post['txtlinediscount'] : 0.00 ).", 
			CollectiveInvoice='".$post['chkcollective']."', 
			IntraStatNbr='".$post['txtIntraStat']."', 
			ExtCustomerNumber='".$post['txtextcustnum']."'
			WHERE [Index]='".$post['txtindex']."';") or die(mssql_get_last_message());
			
			if($sql2 && mssql_rows_affected($this->link)){
				$this->AuditTrail['remark'] = "Success";
				$result = 'success';
			} else {
				$this->AuditTrail['remark'] = "Failed";
				$result = 'failed';
			}
			
		}else{
			$this->AuditTrail['remark'] = "no data";
			$result = 'no data';
		}

		$this->AddToAuditTrail();
		
		return $result;
	}
	
	/* Save Parts Information */
	
	function SaveParts($DBName,$post){
		mssql_select_db($DBName);
		$this->AuditTrail['action'] = "$DBName SaveParts ".$post['txtpartnumber'];
		
		if($this->GetPartNumber($post['txtpartnumber'],$DBName,2,NULL) == 'no data'){			
			$txtpartnumber = (isset($post['txtpartnumber'])) ? $post['txtpartnumber'] : null;
			$txtpartnumber2 = (isset($post['txtpartnumber2'])) ? $post['txtpartnumber2'] : null;
			$txtdescription1 = (isset($post['txtdescription1'])) ? $post['txtdescription1'] : null;
			$txtdescription2 = (isset($post['txtdescription2'])) ? $post['txtdescription2'] : null;
			$txtdescription3 = (isset($post['txtdescription3'])) ? $post['txtdescription3'] : null;
			$txtpackaging = (isset($post['txtpackaging'])) ? $post['txtpackaging'] : null;
			$txtccnumber = (isset($post['txtccnumber'])) ? $post['txtccnumber'] : null;
			$txtpicturefile = (isset($post['txtpicturefile'])) ? $post['txtpicturefile'] : null;
			$txtpicturetext = (isset($post['txtpicturetext'])) ? $post['txtpicturetext'] : null;
			$txtdocumentfile = (isset($post['txtdocumentfile'])) ? $post['txtdocumentfile'] : null;
			$txtinternalinfo = (isset($post['txtinternalinfo'])) ? $post['txtinternalinfo'] : null;
			$txtexternalinfo = (isset($post['txtexternalinfo'])) ? $post['txtexternalinfo'] : null;
			$txtpcsperkg = (isset($post['txtpcsperkg'])) ? $post['txtpcsperkg'] : 0;
			$txtfactorycomment = (isset($post['txtfactorycomment'])) ? $post['txtfactorycomment'] : null;
			$txtweight = (isset($post['txtweight'])) ? $post['txtweight'] : 0;
			$txtmixprice = (isset($post['txtmixprice'])) ? $post['txtmixprice'] : 0;
			//Checkbox
			$boxcontrolsn = (isset($post['boxcontrolsn'])) ? $post['boxcontrolsn'] : 0;
			$boxenableinternet = (isset($post['boxenableinternet'])) ? $post['boxenableinternet'] : 0;
			$boxwarehouse = (isset($post['boxwarehouse'])) ? $post['boxwarehouse'] : 0;
			$boxdeliverylock = (isset($post['boxdeliverylock'])) ? $post['boxdeliverylock'] : 0;
			$boxstrat = (isset($post['boxstrat'])) ? $post['boxstrat'] : 0;
			$boxconsumable = (isset($post['boxconsumable'])) ? $post['boxconsumable'] : 0;
			$boxreceivinglock = (isset($post['boxreceivinglock'])) ? $post['boxreceivinglock'] : 0;			
			$boxreturnflag = (isset($post['boxreturnflag'])) ? $post['boxreturnflag'] : 0;
			$boxrepairable = (isset($post['boxrepairable'])) ? $post['boxrepairable'] : 0;
			$boxdelicatepart = (isset($post['boxdelicatepart'])) ? $post['boxdelicatepart'] : 0;
			$pcsperkg = (isset($post['txtpcsperkg']) && $post['txtpcsperkg']!='') ? $post['txtpcsperkg']: 0;
			$weight = (isset($post['txtweight']) && $post['txtweight']!='') ? $post['txtweight']: 0;
			
			$sqlstring  = "INSERT INTO Parts ";
			$sqlstring .= "(Partnumber,Partnumber2,Description1,Description2,Description3,Packaging,CommodityCode,PictureFile,PictureText,DocumentFile,InternalInfo,ExternalInfo,MixPrice,ControlSN,EnableInternet,WarehouseBooking,DeliveryLock,LifeSensitive,Consumable,PurchaseLock,PiecesPerKg,ReturnFlag,Repairable,DelicatePart,FactoryComment,WeightPerPiece) values ";
			$sqlstring .= "('".$txtpartnumber."','".$txtpartnumber2."','".$txtdescription1."','".$txtdescription2."','".$txtdescription3."','".$txtpackaging."','".$txtccnumber."','".$txtpicturefile."','".$txtpicturetext."','".$txtdocumentfile."','".$txtinternalinfo."','".$txtexternalinfo."',".$txtmixprice.",".$boxcontrolsn.",".$boxenableinternet.",".$boxwarehouse.",".$boxdeliverylock.",".$boxstrat.",".$boxconsumable.",".$boxreceivinglock.",".$pcsperkg.",".$boxreturnflag.",".$boxrepairable.",".$boxdelicatepart.",'".$txtfactorycomment."',".$weight.")";
			$sql2 = mssql_query($sqlstring) or die(mssql_get_last_message());
			if($sql2 && mssql_rows_affected($this->link)){
				$this->AuditTrail['remark'] = "Success";
				$result = 'success';
			} else {
				$this->AuditTrail['remark'] = "Failed";
				$result = 'failed';
			}
		}else{
			$this->AuditTrail['remark'] = "Partnumber already Exist";
			$result = 'exists';
		}		
		$this->AddToAuditTrail();		
		return $result;
	}
	
	/* Update Parts Information */
	
	function UpdateParts($DBName,$post){
		mssql_select_db($DBName);
		$this->AuditTrail['action'] = "$DBName UpdateParts ".$post['txtpartnumber'];
		
		if($this->GetPartNumber($post['txtpartnumber'],$DBName,2,NULL) != 'no data'){			
			$txtdescription1 = (isset($post['txtdescription1'])) ? $post['txtdescription1'] : null;
			$txtdescription2 = (isset($post['txtdescription2'])) ? $post['txtdescription2'] : null;
			$txtdescription3 = (isset($post['txtdescription3'])) ? $post['txtdescription3'] : null;
			$txtpackaging = (isset($post['txtpackaging'])) ? $post['txtpackaging'] : null;
			$txtccnumber = (isset($post['txtccnumber'])) ? $post['txtccnumber'] : null;
			$txtpicturefile = (isset($post['txtpicturefile'])) ? $post['txtpicturefile'] : null;
			$txtpicturetext = (isset($post['txtpicturetext'])) ? $post['txtpicturetext'] : null;
			$txtdocumentfile = (isset($post['txtdocumentfile'])) ? $post['txtdocumentfile'] : null;
			$txtinternalinfo = (isset($post['txtinternalinfo'])) ? $post['txtinternalinfo'] : null;
			$txtexternalinfo = (isset($post['txtexternalinfo'])) ? $post['txtexternalinfo'] : null;
			$txtpcsperkg = (isset($post['txtpcsperkg'])) ? $post['txtpcsperkg'] : 0;
			$txtfactorycomment = (isset($post['txtfactorycomment'])) ? $post['txtfactorycomment'] : null;
			$txtweight = (isset($post['txtweight'])) ? $post['txtweight'] : 0;
			$txtmixprice = (isset($post['txtmixprice'])) ? $post['txtmixprice'] : 0;
			//Checkboxes
			$boxcontrolsn = (isset($post['boxcontrolsn'])) ? $post['boxcontrolsn'] : 0;
			$boxenableinternet = (isset($post['boxenableinternet'])) ? $post['boxenableinternet'] : 0;
			$boxwarehouse = (isset($post['boxwarehouse'])) ? $post['boxwarehouse'] : 0;
			$boxdeliverylock = (isset($post['boxdeliverylock'])) ? $post['boxdeliverylock'] : 0;
			$boxstrat = (isset($post['boxstrat'])) ? $post['boxstrat'] : 0;
			$boxconsumable = (isset($post['boxconsumable'])) ? $post['boxconsumable'] : 0;
			$boxreceivinglock = (isset($post['boxreceivinglock'])) ? $post['boxreceivinglock'] : 0;			
			$boxreturnflag = (isset($post['boxreturnflag'])) ? $post['boxreturnflag'] : 0;
			$boxrepairable = (isset($post['boxrepairable'])) ? $post['boxrepairable'] : 0;
			$boxdelicatepart = (isset($post['boxdelicatepart'])) ? $post['boxdelicatepart'] : 0;
			
			$sqlstring = "UPDATE Parts SET Description1='".$txtdescription1."',Description2='".$txtdescription2."',Description3='".$txtdescription3."',Packaging='".$txtpackaging."',CommodityCode='".$txtccnumber."',PictureFile='".$txtpicturefile."',PictureText='".$txtpicturetext."',DocumentFile='".$txtdocumentfile."',InternalInfo='".$txtinternalinfo."',ExternalInfo='".$txtexternalinfo."',MixPrice=$txtmixprice,ControlSN=$boxcontrolsn,EnableInternet=$boxenableinternet,WarehouseBooking=$boxwarehouse,DeliveryLock=$boxdeliverylock,LifeSensitive=$boxstrat,Consumable=$boxconsumable,PurchaseLock=$boxreceivinglock,PiecesPerKg=".$txtpcsperkg.",ReturnFlag=$boxreturnflag,Repairable=$boxrepairable,DelicatePart=$boxdelicatepart,FactoryComment='".$txtfactorycomment."',WeightPerPiece=".$txtweight." WHERE Partnumber = '".$post['txtpartnumber']."'";
			
			$sql2 = mssql_query($sqlstring) or die(mssql_get_last_message()); 
			
			if($sql2 && mssql_rows_affected($this->link)){
				$this->AuditTrail['remark'] = "Success";
				$result = 'success';
			} else {
				$this->AuditTrail['remark'] = "Failed";
				$result = 'failed';
			}
		}else{
			$this->AuditTrail['remark'] = "Partnumber already Exist";
			$result = 'not exists';
		}		
		$this->AddToAuditTrail();		
		return $result;
	}
	
	/* Get the quantity list based on partnumber */
	function GetQuantityList($DBName,$partnumber){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "$DBName GetQuantityList : Partnumber".$partnumber;
		
		$res = mssql_query("SELECT (convert(varchar,convert(int,a.Gesamtzugang) - convert(int,a.GesamtAbgang)) + ' ' + (SELECT (case when SUM(b.Quantity) > 0 then '(' + convert(varchar,SUM(b.Quantity)) + ')' else '' end) FROM k_orderedButNotPackedParts b WHERE ((b.StockIndex = a.StockIndex) AND (b.Partnumber = '$partnumber' ))) + ' ' + a.StockIndex) as j FROM k_QuantityPricePerStock a WHERE (Partnumber = '$partnumber')");
		
		if(mssql_num_rows($res) > 0){
			while($data[] = mssql_fetch_assoc($res)){}
			array_pop($data);
		
			array_push($data,array("j" => "Others"));
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "No matching record found.";
			$data = 'no data';
		}
		  
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	/* Get Part in group based on partnumber */
	function GetPartinGroup($DBName,$partnumber){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "$DBName GetPartinGroup : Partnumber".$partnumber;
		
		$res = mssql_query("select GroupIndex from PartsInPartGroup where Partnumber = '$partnumber'");
		
		if(mssql_num_rows($res) > 0){
			while($data[] = mssql_fetch_assoc($res)){}
			array_pop($data);
		
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "No matching record found.";
			$data = 'no data';
		}
		  
		$this->AddToAuditTrail();
		
		return $data;
	}
	/*  Get supplier information */
	function GetSupplier($index,$DBName,$requestType,$fields){
		mssql_select_db($DBName);
		
		/*------------------------------+
		| $requestType values are		|
		| 1 = direct request			|
		| 2 = request from a function	|
		| $index = if null then all|
		+------------------------------*/
		
		$filter = ($index!='null') ? "WHERE [Index]='$index'": "";
		$fields = ($fields!=NULL) ? $fields : "*";
		
		if ($requestType==1){
			$this->AuditTrail['action'] = "$DBName GetSupplier ".$index; 
		}
			
		$sql = mssql_query("SELECT $fields FROM Supplier $filter;");

		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
		}else{
			if ($requestType==1){
				$this->AuditTrail['remark'] = "no data";
			}
			$data = 'no data';
		}
		
		if ($requestType==1){
			$this->AddToAuditTrail();
		}
		
		return $data;
	}
	
	/* Save Supplier Information */
	
	function SaveSupplier($DBName,$post){
		mssql_select_db($DBName);
		$this->AuditTrail['action'] = "$DBName SaveSupplier ".$post['txtpartnumber'];
		
		if($this->GetSupplier($post['txtindex'],$DBName,2,NULL) == 'no data'){			
			$sqlstring = "INSERT INTO [Supplier] ([Index],[Name1],[Name2],[Street],[Country],[Zip],[City],[VATIDNumber],[OwnCustomerNumber],[Terms],[Forwarder]) values ";
			$sqlstring.="('".$post["txtindex"]."','".$post["txtname1"]."','".$post["txtname2"]."','".$post["txtstreet"]."','".$post["txtcty"]."','".$post["txtzip"]."','".$post["txtcity"]."','".$post["txtvatid"]."','".$post["txtcustnbr"]."','".$post["txtterms"]."','".$post["txtforwarder"]."')";
		
			$sql2 = mssql_query($sqlstring);
			if($sql2 && mssql_rows_affected($this->link)){
				$this->AuditTrail['remark'] = "Success";
				$result = 'success';
			} else {
				$this->AuditTrail['remark'] = "Failed";
				$result = 'failed';
			}
		}else{
			$this->AuditTrail['remark'] = "Partnumber already Exist";
			$result = 'exists';
		}		
		$this->AddToAuditTrail();		
		return $result;
	}
	
	/* Update Parts Information */
	
	function UpdateSupplier($DBName,$post){
		mssql_select_db($DBName);
		$this->AuditTrail['action'] = "$DBName UpdateSupplier ".$post['txtpartnumber'];
		
		if($this->GetSupplier($post['txtindex'],$DBName,2,NULL) != 'no data'){	
			
			$sqlstring = "UPDATE [Supplier] set [Name1]='".$post["txtname1"]."',[Name2]='".$post["txtname2"]."',[Street]='".$post["txtstreet"]."',[Country]='".$post["txtcty"]."',[Zip]='".$post["txtzip"]."',[City]='".$post["txtcity"]."',[VATIDNumber]='".$post["txtvatid"]."',[OwnCustomerNumber]='".$post["txtcustnbr"]."',[Terms]='".$post["txtterms"]."',[Forwarder]='".$post["txtforwarder"]."' where [Index]='".$post["txtindex"]."'";
			
			$sql2 = mssql_query($sqlstring); 
			
			if($sql2 && mssql_rows_affected($this->link)){
				$this->AuditTrail['remark'] = "Success";
				$result = 'success';
			} else {
				$this->AuditTrail['remark'] = "Failed";
				$result = 'failed';
			} 
		}else{
			$this->AuditTrail['remark'] = "Partnumber already Exist";
			$result = 'not exists';
		}		
		$this->AddToAuditTrail();		
		return $result;
	}
	
	function is_manager($username){
		mssql_select_db(DefaultDB);		
		$this->AuditTrail['action'] = "Filtering User : ".DefaultDB." Username : ".$username;		
		$is_manager = false;		
		$res = mssql_query("SELECT UsergroupName FROM [v_UserDep&Group] WHERE Username = '" . trim($username)."'");		
		if(mssql_num_rows($res) > 0){
			$name = mssql_result($res,0,'UsergroupName');		
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($row);
			if(strtolower($name) == 'manager' || 
				strtolower($name) == 'managers' ||
				strtolower($name) == 'supervisor' ||
				strtolower($name) == 'supervisors'){
				$is_manager = true;
			}
		}else{ 
			$this->AuditTrail['remark'] = "No matching record found.";
		}		 
		$this->AddToAuditTrail();		
		return $is_manager; 
	}
	
	function is_admin($username){
		mssql_select_db(DefaultDB);		
		$this->AuditTrail['action'] = "Filtering User : ".DefaultDB." Username : ".$username;		
		$is_admin = false;		
		$res = mssql_query("SELECT UsergroupName FROM [v_UserDep&Group] WHERE Username = '" . trim($username)."'");		
		if(mssql_num_rows($res) > 0){
			$name = mssql_result($res,0,'UsergroupName');		
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($row);
			if(strtolower($name) == 'admin' || 
				strtolower($name) == 'administrator' ||
				strtolower($name) == 'administrators'){
				$is_admin = true;
			}
		}else{ 
			$this->AuditTrail['remark'] = "No matching record found.";
		}		 
		$this->AddToAuditTrail();		
		return $is_admin; 
	}
	
	/* Get Entry Data Field*/
	function GetEntryDataFields($WawiID){
		mssql_select_db(DefaultDB);
		
		$this->AuditTrail['action'] = "GetEntryDataFields: ".$WawiID;
		
		$sql = mssql_query("SELECT * FROM v_EntryDataSetup WHERE WawiID = '".$WawiID."' Order By OrderList");

		if(mssql_num_rows($sql) > 0){
			$i=0;
			while($data[$i] = mssql_fetch_assoc($sql)){
				if ($data[$i]['ElementType']=='select'){

					if(strtolower($data[$i]["FieldName"])==strtolower('Supplier')){
						$sql2 = mssql_query("SELECT [Index],[Name1] as [Description] FROM ".$data[$i]['DBName'].".dbo.Supplier ORDER BY [Index] ASC;");
						$temp['Index'] = '';
						$temp['Description'] = '-';
						$data[$i]['Options'][]=$temp;
						if(mssql_num_rows($sql2) > 0){
							while($data[$i]['Options'][] = mssql_fetch_assoc($sql2));
							array_pop($data[$i]['Options']);
						}
					}
					
					if(strtolower($data[$i]["FieldName"])==strtolower('StockIndex')){
						$sql2 = mssql_query("SELECT [Index],[Description] FROM ".$data[$i]['DBName'].".dbo.Stocks Order By [Listsort] ASC;");
						$temp['Index'] = '';
						$temp['Description'] = '-';
						$data[$i]['Options'][]=$temp;
						if(mssql_num_rows($sql2) > 0){
							while($data[$i]['Options'][] = mssql_fetch_assoc($sql2));
							array_pop($data[$i]['Options']);
						}
					}
					
					if(strtolower($data[$i]["FieldName"])==strtolower('PackageType')){
						$slq = mssql_query("SELECT PackageType as [Index], [Description] FROM PackageType WHERE WawiID = ".$WawiID." AND [status]=1 Order By PackageType ASC;");
						$temp['Index'] = '';
						$temp['Description'] = '-';
						$data[$i]['Options'][]=$temp;
						if(mssql_num_rows($slq) > 0){
							while($data[$i]['Options'][] = mssql_fetch_assoc($slq));
							array_pop($data[$i]['Options']);
						}
					}					
					
					if(strtolower($data[$i]["FieldName"])==strtolower('ChipFrom')){
						switch($WawiID){
							case 9:
							case 59:
							case 10:
							case 13:
							case 28:
							case 42:
							case 47:
							case 53:
							case 55:
								$slq = mssql_query("SELECT PackageType as [Index], [Description] FROM PackageType WHERE WawiID = ".$WawiID." AND [status]=1 Order By PackageType ASC;");
								$temp['Index'] = '';
								$temp['Description'] = '-';
								$data[$i]['Options'][]=$temp;
								if(mssql_num_rows($slq) > 0){
									while($data[$i]['Options'][] = mssql_fetch_assoc($slq));
									array_pop($data[$i]['Options']);
								}
							break;
						}
					}
					
					if(strtolower($data[$i]["FieldName"])==strtolower('Comment')){
						switch($WawiID){
							case 14:
							case 30:
							case 31:
								$slq = mssql_query("SELECT PackageType as [Index], [Description] FROM PackageType WHERE WawiID = ".$WawiID." AND [status]=1 Order By PackageType ASC;");
								$temp['Index'] = '';
								$temp['Description'] = '-';
								$data[$i]['Options'][]=$temp;
								if(mssql_num_rows($slq) > 0){
									while($data[$i]['Options'][] = mssql_fetch_assoc($slq));
									array_pop($data[$i]['Options']);
								}
							break;
						}
					}
					
					if(strtolower($data[$i]["FieldName"])==strtolower('Seal')){
						switch($WawiID){
							case 19:
							case 37:
								$slq = mssql_query("SELECT PackageType as [Index], [Description] FROM PackageType WHERE WawiID = ".$WawiID." AND [status]=1 Order By PackageType ASC;");
								$temp['Index'] = '';
								$temp['Description'] = '-';
								$data[$i]['Options'][]=$temp;
								if(mssql_num_rows($slq) > 0){
									while($data[$i]['Options'][] = mssql_fetch_assoc($slq));
									array_pop($data[$i]['Options']);
								}
							break;
							
							case 23:
							case 27:
								$slq = mssql_query("SELECT [Index], Name1 AS [Description] FROM ".$data[$i]['DBName'].".dbo.Customer WHERE [Index]<>'' ORDER BY [Index] ASC;");
								$temp['Index'] = '';
								$temp['Description'] = '-';
								$data[$i]['Options'][]=$temp;
								if(mssql_num_rows($slq) > 0){
									while($data[$i]['Options'][] = mssql_fetch_assoc($slq));
									array_pop($data[$i]['Options']);
								}
							break;
						}
					}
					
					if(strtolower($data[$i]["FieldName"])==strtolower('SupplierOrder')){
						switch($WawiID){ 
							case 9:
							case 59:
								$temp['Index'] = '';
								$temp['Description'] = '-';
								$data[$i]['Options'][]=$temp;
								
								$temp['Index'] = 'INNER';
								$temp['Description'] = 'INNER';
								$data[$i]['Options'][]=$temp;
								
								$temp['Index'] = 'OUTER';
								$temp['Description'] = 'OUTER';
								$data[$i]['Options'][]=$temp;
							break;
							
							case 22:
								$slq = mssql_query("SELECT PackageType as [Index], [Description] FROM PackageType WHERE WawiID = ".$WawiID." AND [status]=1 Order By PackageType ASC;");
								$temp['Index'] = '';
								$temp['Description'] = '-';
								$data[$i]['Options'][]=$temp;
								if(mssql_num_rows($slq) > 0){
									while($data[$i]['Options'][] = mssql_fetch_assoc($slq));
									array_pop($data[$i]['Options']);
								}
							break;
						}
					}
					
					if(strtolower($data[$i]["FieldName"])==strtolower('TaxScheme')){
						$sql3 = mssql_query("SELECT TaxScheme as [Index], [ID] as 'Description' FROM TaxScheme WHERE [status]=1 AND WawiID=$WawiID Order By TaxScheme ASC;");
						$temp['Index'] = '';
						$temp['Description'] = '-';
						$data[$i]['Options'][]=$temp;
						if(mssql_num_rows($sql3) > 0){
							while($data[$i]['Options'][] = mssql_fetch_assoc($sql3));
							array_pop($data[$i]['Options']);
						}

					}
					

					if(strtolower($data[$i]["FieldName"])==strtolower('Silicon')){
						switch($WawiID){
							case 28:
							case 53:
							case 55:
								$temp['Index'] = '';
								$temp['Description'] = '-';
								$data[$i]['Options'][]=$temp;
								
								$temp['Index'] = 'Silicon';
								$temp['Description'] = 'Silicon';
								$data[$i]['Options'][]=$temp;
								
								$temp['Index'] = 'Non-Silicon';
								$temp['Description'] = 'Non-Silicon';
								$data[$i]['Options'][]=$temp;
							break;
						}
					}

					
					if(strtolower($data[$i]["FieldName"])==strtolower('CountryOfOrigin')){
						$sql3 = mssql_query("SELECT country_code as [Index], country_name as [Description] FROM STGMM.dbo.Country;");
						
						if(mssql_num_rows($sql3) > 0){
							$temp['Index'] = '';
							$temp['Description'] = '-';
							$data[$i]['Options'][]=$temp;
							while($data[$i]['Options'][] = mssql_fetch_assoc($sql3));
							array_pop($data[$i]['Options']);
						}

					}
					
				}
				$i++;
			}
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		} else {	
			$this->AuditTrail['remark'] = "No Data";
			$data = 'no data';
		}
		
		$this->AddToAuditTrail();
		
		//$this->CloseMyConnection();
		
		return $data;
	} 
	
	/* Update Entry Data */
	function UpdateEntryData($DB,$postdata){
				
		mssql_select_db($DB['DBName']); 
		
		$this->AuditTrail['action'] = $DB['DBName']." UpdateEntryData ".$postdata['EntryNumber'];
		
		$sql = mssql_query("SELECT * FROM [Entry] WHERE EntryNumber = ".$postdata['EntryNumber'].";");

		if(mssql_num_rows($sql) > 0){ 
		
			$partsDetails = $this->GetPartNumber($postdata['Partnumber'],$DB['DBName'],2,NULL);
			
			if ($partsDetails == 'no data'){
				$this->AuditTrail['remark'] = "Partnumber ".$postdata['Partnumber']." does not exist";
				$result = 3;
			}else if ($postdata['Quantity'] < 1){
				$this->AuditTrail['remark'] = "Quantity invalid";
				$result = 5;
			}else{	
			
				if ($DB['GST']){
					if (!isset($postdata['ForwarderRef'])){
						return 6; 
					}else{ 					
						$this->InsertIntoImport($DB['WawiIndex'],$postdata['ForwarderRef'],2);
					}
				}				
		
				$setfield = '';
				$entrynumber = $postdata['EntryNumber'];
				switch($postdata['WawiID']){
					case 56 : unset($postdata['EntryNumber']); break;
				}
				unset($postdata['WawiID'],$postdata['type'],$postdata['EntryDate'],$postdata['PB'],$postdata['Silicon']);
				
				foreach($postdata as $key => $value){					
					$setfield .= "[$key] = '".trim($value)."',";					
				} 
				
				switch($DB['DBName']){
					case 'AnalogixWawi' :
							if(isset($postdata['PB'])){ 
								$setfield .= " [PB] = 'C:\CCS Programs\Analogix\\pb.bmp' ,";
								$setfield .= " [ROHS] = 'C:\CCS Programs\Analogix\\rohs.bmp' ,";
								$setfield .= " [E] = 'C:\CCS Programs\Analogix\\e.bmp' ,";
							}else{ 
								$setfield .= " [PB] = 'C:\CCS Programs\Analogix\\nopb.bmp' ,";
								$setfield .= " [ROHS] = 'C:\CCS Programs\Analogix\\norohs.bmp' ,";
								$setfield .= " [E] = 'C:\CCS Programs\Analogix\\noe.bmp' ,";
							}
						break;							
				}

				
				$setfield = substr($setfield,0,-1);
				mssql_select_db($DB['DBName']); 
				$sqlstring = "UPDATE [Entry] SET $setfield WHERE [EntryNumber] = '$entrynumber';";
				$sql = mssql_query($sqlstring) or die(mssql_get_last_message());			
				if($sql && mssql_rows_affected($this->link)){
					$this->AuditTrail['remark'] = "Successful row update";
					$result = 1;
				}else{
					$this->AuditTrail['remark'] = "Could not update row";
					$result = 2;
				}
			}
			
		}else{
			$this->AuditTrail['remark'] = "EntryNumber not exist";
			$result = 0;
		}
		
		$this->AddToAuditTrail();
		return $result;
		
	}
	
	function getUserName($user){ 
		mssql_select_db(DefaultDB);		
		$this->AuditTrail['action'] = "Filtering User : ".DefaultDB." Username : ".$user;		
		$res = mssql_query("SELECT Name FROM [".DefaultDB."].[dbo].[User] WHERE Username = '" . trim($user)."'");		
		if(mssql_num_rows($res) > 0){
			$row = mssql_fetch_assoc($res);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($row);
			$data = $row['Name'];
		}else{
			$this->AuditTrail['remark'] = "No matching record found.";
			$data = 'Others';
		}		 
		$this->AddToAuditTrail();		
		return $data;
	}
	 
	/* Send 3B2 for Marvell */
	function Send3B2($DB,$postdata){				
		mssql_select_db($DB['DBName']); 		
		$this->AuditTrail['action'] = $DB['DBName']." Send3B2 ".$postdata['ordernumber'];		
		$sql = mssql_query("SELECT * FROM [Packlist] WHERE PacklistNumber = ".$postdata['packlistnumber'].";");
		if(mssql_num_rows($sql) > 0){
			$ordernumber = mssql_result($sql,0,'DocumentNumber');
			$query = "SELECT * FROM [Order] INNER JOIN Packlist ON [Order].Ordernumber=Packlist.DocumentNumber WHERE Packlist.KindOfDocument='ORDER' AND [Order].Ordernumber='$ordernumber' AND Packlist.Destination<>'' AND Colli Is Not Null";
			$res = mssql_query($query);
			if(mssql_num_rows($res) > 0){
				$shippingmethod = mssql_result($res,0,'ShippingMethod');
				if(strlen(trim($shippingmethod)) == 0){
					$this->AuditTrail['remark'] = "Shipping Method is missing";
					$result = 3;
				}else{
					error_reporting(E_ALL);
					ini_set('display_errors',1);   
					include_once('./modules/flightdetail/writexml.php');	
					$writexml = new WriteXML;
					$response = $writexml->send($res);					
					if($response == "Success"){
						$result = $response;
						$query = "UPDATE [Packlist] SET [TrackingSentDate] = '".date('m/d/Y')."' WHERE PacklistNumber = ".$postdata['packlistnumber'].";";
						if(!mssql_query($query)) $result = "Message Sent but with error[Could not update Packlist.TrackingSentDate]";
						$query = "UPDATE [Order] SET [send3B2] = 'YES' WHERE Ordernumber = '$ordernumber' ";
						if(!mssql_query($query)) $result = "Message Sent but with error[Could not update Order.send3B2]";		
						
					}else{
						$this->AuditTrail['remark'] = $response;
						$result = $response;
					}					
				}
			}else{
				$this->AuditTrail['remark'] = "No data to send";
				$result = 2;
			}			
		}else{
			$this->AuditTrail['remark'] = "Packlist number does not exist";
			$result = 1;
		}		
		$this->AddToAuditTrail();
		return $result;		
	}
	/* Update Yen Value */
	function YenValueUpdate($DB,$postdata){				
		mssql_select_db($DB['DBName']); 		
		$ordernumber = trim($postdata['OrderNumber']);
		$yenvalue = trim($postdata['YenValue']);
		$this->AuditTrail['action'] = $DB['DBName']." YenValueUpdate ".$ordernumber;		
		$sql = mssql_query("SELECT * FROM [Order] WHERE ordernumber='$ordernumber'");
		if(mssql_num_rows($sql) > 0){			
			$query = "UPDATE [Order] SET currency = 'JPY' ,SECONDARYAMOUNT = $yenvalue WHERE ordernumber='$ordernumber' ";
			$res = mssql_query($query);
			if($res && mssql_rows_affected($this->link)){			
				$this->AuditTrail['remark'] = "Successful yen update";
				$result = 'success';				
			}else{
				$this->AuditTrail['remark'] = "Could not update value";
				$result = 'failed';
			}			
		}else{ 
			$this->AuditTrail['remark'] = "Packlist number does not exist";
			$result = 'empty';
		}		
		$this->AddToAuditTrail();
		return $result;		
	}	
	/* Update Max Temp Value */
	function MaxTempUpdate($DB,$postdata){				
		mssql_select_db($DB['DBName']); 		
		$partnumber = trim($postdata['PartNumber']);
		$maxtemp = trim($postdata['MaxTemp'].$postdata['TempSymbol']);
		$this->AuditTrail['action'] = $DB['DBName']." MaxTempUpdate ".$partnumber;		
		$sql = mssql_query("SELECT * FROM [Parts] WHERE partnumber='$partnumber'");
		if(mssql_num_rows($sql) > 0){			
			$query = "UPDATE [Parts] SET [FactoryComment] = '$maxtemp' WHERE Partnumber='$partnumber' ";
			$res = mssql_query($query);
			if($res && mssql_rows_affected($this->link)){			
				$this->AuditTrail['remark'] = "Successful max temp update";
				$result = 'success';				
			}else{
				$this->AuditTrail['remark'] = "Could not update value";
				$result = 'failed';
			}			
		}else{ 
			$this->AuditTrail['remark'] = "Partnumber number does not exist";
			$result = 'empty';
		}		
		$this->AddToAuditTrail();
		return $result;		
	}
	/* Label Logo Update*/
	function LabelLogoUpdate($DB,$postdata){				
		mssql_select_db($DB['DBName']); 		
		$partnumber = trim($postdata['PartNumber']);
		$logo = '';
		switch(trim($postdata['Logo'])){
			case "PB" : $logo = 'C:\CCS Programms\Marvell\pb.bmp'; break;
			case "PBFREE" : $logo = 'C:\CCS Programms\Marvell\nopb.bmp'; break;
			default : $logo = ''; break;
		}			
		$maxtemp = trim($postdata['MaxTemp'].$postdata['TempSymbol']);
		$this->AuditTrail['action'] = $DB['DBName']." LabelLogoUpdate ".$partnumber;		
		$sql = mssql_query("SELECT * FROM [Parts] WHERE partnumber='$partnumber'");
		if(mssql_num_rows($sql) > 0){			
			$query = "UPDATE [Parts] SET [FactoryComment] = '$logo' WHERE Partnumber='$partnumber' ";
			$res = mssql_query($query);
			if($res && mssql_rows_affected($this->link)){			
				$this->AuditTrail['remark'] = "Successful label logo update";
				$result = 'success';				
			}else{
				$this->AuditTrail['remark'] = "Could not update value";
				$result = 'failed';
			}			
		}else{ 
			$this->AuditTrail['remark'] = "Partnumber number does not exist";
			$result = 'empty';
		}		
		$this->AddToAuditTrail();
		return $result;		
	}
	/* QA Check Order*/
	function QACheckOrder($DB,$postdata){				
		mssql_select_db($DB['DBName']); 		
		$ordernumber = trim($postdata['ordernumber']);
		$boxnumber = trim($postdata['boxnumber']);			
		$this->AuditTrail['action'] = $DB['DBName']." QACheckOrder ".$ordernumber;			
		$sql = mssql_query("SELECT * FROM [Shipboxes] WHERE ordernumber='$ordernumber' AND boxnumber='$boxnumber'");
		if(mssql_num_rows($sql) > 0){					
			$this->AuditTrail['remark'] = "Order number exists";
			$result = 'exists';	
		}else{ 
			$this->AuditTrail['remark'] = "Order number does not exists";
			$result = 'notexists';
		}		
		$this->AddToAuditTrail();
		return $result;		
	}
	/* QA Check Inner Label*/
	function QACheckInnerLabel($DB,$postdata){				
		mssql_select_db($DB['DBName']); 		
		$entrynumber = trim($postdata['entrynumber']);
		$partnumber = trim($postdata['partnumber']);
		$partnumber2 = trim($postdata['partnumber2']);
		$lotnumber = trim($postdata['lotnumber']);	
		$quantity = trim($postdata['quantity']);			
		$this->AuditTrail['action'] = $DB['DBName']." QACheckInnerLabel ".$entrynumber;
		//$query = "SELECT * FROM [OrderPosition] WHERE partnumber='$partnumber' ".(isset($partnumber2) ? "AND CustomerPartnumber='$partnumber2'" : "" )." AND lotnumber='$lotnumber' AND quantity IN (select Quantity from PackListPosition where Entrynumber = '$entrynumber' AND Quantity = $quantity) AND Ordernumber IN (SELECT Ordernumber from [Shipboxes] where Entrynumber = '$entrynumber' ) ";
		$query = "select * from PackListPosition where EntryNumber = $entrynumber AND PartNumber IN (SELECT Partnumber FROM OrderPosition WHERE Partnumber = '$partnumber' AND CustomerPartnumber = '$partnumber2' AND LotNumber = $lotnumber) AND Quantity = $quantity ";
		$sql = mssql_query($query);
		if(mssql_num_rows($sql) > 0){					
			$this->AuditTrail['remark'] = "Entry number exists";
			$result = 'exists';		
		}else{ 
			$this->AuditTrail['remark'] = "Entry number does not exists";
			$result = 'notexists';
		}		
		$this->AddToAuditTrail();
		return $result;		
	}
	/* QA Check Outer Label*/
	function QACheckOuterLabel($DB,$postdata){				
		mssql_select_db($DB['DBName']); 		
		$partnumber = trim($postdata['partnumber']);
		$partnumber2 = trim($postdata['partnumber2']);
		$quantity = trim($postdata['quantity']);			
		$this->AuditTrail['action'] = $DB['DBName']." QACheckOuterLabel ".$partnumber;
		$query = "SELECT Partnumber FROM [OrderPosition] WHERE partnumber='$partnumber' ".(isset($partnumber2) ? "AND CustomerPartnumber='$partnumber2'" : "" )." GROUP BY Partnumber,CustomerPartnumber,Ordernumber having SUM(Quantity) >= $quantity ";
		$sql = mssql_query($query);
		if(mssql_num_rows($sql) > 0){					
			$this->AuditTrail['remark'] = "Partnumber exists";
			$result = 'exists';		
		}else{ 
			$this->AuditTrail['remark'] = "Part number does not exists";
			$result = 'notexists';
		}		
		$this->AddToAuditTrail();
		return $result;		
	}
	
	/* QA Check All Inner Label*/
	function QACheckAllInnerLabel($DB,$postdata){				
		mssql_select_db($DB['DBName']); 		
		$ordernumber = trim($postdata['ordernumber']);
		$boxnumber = trim($postdata['boxnumber']);		
		$this->AuditTrail['action'] = "$DBName QACheckAllInnerLabel ".$ordernumber;
		$query = "SELECT dbo.Shipboxes.Ordernumber, dbo.Shipboxes.Boxnumber, dbo.Shipboxes.Entrynumber, dbo.Entry.LotNumber, dbo.OrderPosition.CustomerPartnumber, dbo.Entry.Partnumber,dbo.Shipboxes.Quantity
					FROM dbo.Entry INNER JOIN dbo.Shipboxes ON dbo.Entry.EntryNumber = dbo.Shipboxes.Entrynumber INNER JOIN dbo.OrderPosition ON dbo.Shipboxes.Ordernumber = dbo.OrderPosition.Ordernumber AND dbo.Entry.LotNumber = dbo.OrderPosition.LotNumber
					WHERE (dbo.Shipboxes.Boxnumber = $boxnumber) AND (dbo.Shipboxes.Ordernumber = $ordernumber)";
		$sql = mssql_query($query);
		if(mssql_num_rows($sql) > 0){					
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$result = $data;	
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);			
		}else{ 
			$this->AuditTrail['remark'] = "Order number and Box number does not exists";
			$result = 'no data';
		}		
		$this->AddToAuditTrail();
		return $result;		
	}
	
	/* QA Check All Outer Label*/
	function QACheckAllOuterLabel($DB,$postdata){				
		mssql_select_db($DB['DBName']); 		
		$ordernumber = trim($postdata['ordernumber']);
		$boxnumber = trim($postdata['boxnumber']);		
		$this->AuditTrail['action'] = "$DBName QACheckAllOuterLabel ".$ordernumber;
		$query = "SELECT dbo.Shipboxes.Ordernumber, dbo.Shipboxes.Boxnumber, dbo.OrderPosition.CustomerPartnumber, dbo.Entry.Partnumber, SUM(dbo.Shipboxes.Quantity) as TotalQuantity
					FROM dbo.Entry INNER JOIN dbo.Shipboxes ON dbo.Entry.EntryNumber = dbo.Shipboxes.Entrynumber INNER JOIN dbo.OrderPosition ON dbo.Shipboxes.Ordernumber = dbo.OrderPosition.Ordernumber AND dbo.Entry.LotNumber = dbo.OrderPosition.LotNumber
					GROUP BY dbo.Entry.Partnumber, dbo.OrderPosition.CustomerPartnumber, dbo.Shipboxes.Ordernumber, dbo.Shipboxes.Boxnumber HAVING  (dbo.Shipboxes.Boxnumber = $boxnumber) AND (dbo.Shipboxes.Ordernumber =$ordernumber )";
		$sql = mssql_query($query);
		if(mssql_num_rows($sql) > 0){					
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$result = $data;	
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);			
		}else{ 
			$this->AuditTrail['remark'] = "Order number and Box number does not exists";
			$result = 'no data';
		}		
		$this->AddToAuditTrail();
		return $result;		
	}
	
	/* Advance Receiving : Check invoice number */
	function CheckInvoiceNo($DB,$postdata){				
		mssql_select_db($DB['DBName']); 		
		$invoiceno = trim($postdata['invoiceno']);	
		$this->AuditTrail['action'] = $DB['DBName']." CheckInvoiceNo ".$invoiceno;
		$query = "SELECT * FROM AdvanceReceiving WHERE InvoiceNo = '$invoiceno' ";
		$sql = mssql_query($query);
		if(mssql_num_rows($sql) > 0){					
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$result = $data;	
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);			
		}else{ 
			$this->AuditTrail['remark'] = "Invoice number does not exists";
			$result = 'no data';
		}		
		$this->AddToAuditTrail();
		return $result;		
	}
	/* Advance Receiving : Add/Edit/Delete Invoice*/
	function AdvanceReceivingAction($DB,$postdata){				
		mssql_select_db($DB['DBName']); 		
		$invoiceno = trim($postdata['InvoiceNo']);
		$this->AuditTrail['action'] = $DB['DBName']." AdvanceReceivingAction ".$invoiceno;
		$filter = "InvoiceNo = '$invoiceno'";
		$columns = $this->getTableColumn($DB['DBName'],'AdvanceReceiving');
		$query = "SELECT * FROM AdvanceReceiving WHERE $filter";
		$sql = mssql_query($query);
		if(mssql_num_rows($sql) > 0){
			if($postdata['action'] == 'update'){
				if(is_array($columns) && count($columns) > 0){
					$setfields = '';
					unset($columns[33],$columns[34],$columns[35],$columns[36]);					
					foreach($columns as $columnname){
						if($columnname == 'PDFCreated'){
							$setfields .= " ".$columnname."='0',";
						}else{
							$setfields .= " ".$columnname."='".trim($postdata[$columnname])."',";
						}
					} 
					$setfields = substr($setfields,1,-1); 
					$query = "UPDATE AdvanceReceiving SET $setfields WHERE  $filter";						
					if(mssql_query($query) && mssql_rows_affected($this->link)){
						$this->AuditTrail['remark'] = "Successful row update";			
						$result = 'success';
					}else{
						$this->AuditTrail['remark'] = "Could not update row";			
						$result = 'failed';
					}
				}else{
					$this->AuditTrail['remark'] = "Empty column.";
					$result = 'empty';
				}
				
			}else if($postdata['action'] == 'delete'){
				mssql_query('BEGIN TRAN');
				$query = "DELETE FROM AdvanceReceiving WHERE  $filter";
				if(mssql_query($query) && mssql_rows_affected($this->link)){
					$query = "DELETE FROM AdvanceReceivingDetail WHERE  $filter";
					if(mssql_query($query)){
						mssql_query('COMMIT');
						$this->AuditTrail['remark'] = "Successful row delete";			
						$result = 'deleted';
					}else{
						mssql_query('ROLLBACK');
						$this->AuditTrail['remark'] = "Could not delete row";			
						$result = 'failed';
					}
				}else{
					mssql_query('ROLLBACK');
					$this->AuditTrail['remark'] = "Could not delete row";			
					$result = 'failed';
				}
			}else{
				$this->AuditTrail['remark'] = "Invoice already exists";
				$result = 'exists';
			}
		}else{ 
			foreach($postdata as $k=>$v){
				$postdata[$k] = trim($v);
			}
			unset($postdata['WawiID'],$postdata['type'],$postdata['action']);
			$query = "INSERT INTO AdvanceReceiving (".implode(",",array_keys($postdata)).",TypeOfEntry,JobNo,PermitNo) VALUES('".implode("','",$postdata)."',0,null,null)";
			$res = mssql_query($query) or die(mssql_get_last_message());
			if($res){ 
				$this->AuditTrail['remark'] = "Successful row insert";			
				$result = 'success';
			}else{ 
				$this->AuditTrail['remark'] = "Could not insert row";			
				$result = 'failed';
			}
		}		
		$this->AddToAuditTrail();
		return $result;		
	}
	
	/* Advance Receiving Detail : Get Details*/
	function GetAdvReceivingDetails($DB,$postdata){				
		mssql_select_db($DB['DBName']); 		
		$invoiceno = trim($postdata['invoiceno']);
		if(isset($postdata['partnumber']) && isset($postdata['lotnumber'])){
			$filter = " AND PartNumber = '".trim($postdata['partnumber'])."' AND LotNumber = '".$postdata['lotnumber']."'";
		}
		$this->AuditTrail['action'] = $DB['DBName']." GetAdvReceivingDetails ".$invoiceno;
		$query = "SELECT * FROM AdvanceReceivingDetail WHERE InvoiceNo = '$invoiceno' $filter";
		$sql = mssql_query($query);
		if(mssql_num_rows($sql) > 0){					
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$result = $data;	
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);			
		}else{ 
			$this->AuditTrail['remark'] = "Detail does not exists";
			$result = 'no data';
		}		
		$this->AddToAuditTrail();
		return $result;		
	}
	
	/* Advance Receiving Detail : Add/Edit/Delete Details*/
	function AdvanceReceivingDetailAction($DB,$postdata){				
		mssql_select_db($DB['DBName']); 		
		$invoiceno = trim($postdata['InvoiceNo']);
		$partnumber = trim($postdata['PartNumber']);
		$lotnumber = trim($postdata['LotNumber']);
		$packagetype = trim($postdata['PType']);
		$this->AuditTrail['action'] = $DB['DBName']." AdvanceReceivingDetailAction ".$invoiceno;
		$filter = "InvoiceNo = '$invoiceno'  AND PartNumber = '$partnumber' AND LotNumber = '$lotnumber' AND PType = '$packagetype' ";
		$query = "SELECT * FROM AdvanceReceivingDetail WHERE $filter";
		$sql = mssql_query($query);
		if(mssql_num_rows($sql) > 0){
			if($postdata['action'] == 'update'){
				$columns = $this->getTableColumn($DB['DBName'],'AdvanceReceivingDetail');
				if(is_array($columns) && count($columns) > 0){
					$setfields = '';
					foreach($columns as $columnname){
						$setfields .= " ".$columnname."='".trim($postdata[$columnname])."',";
					}
					$setfields = substr($setfields,1,-1);
					$query = "UPDATE AdvanceReceivingDetail SET $setfields WHERE  $filter";	
					if(mssql_query($query) && mssql_rows_affected($this->link)){
						$this->AuditTrail['remark'] = "Successful row update";			
						$result = 'success';
					}else{
						$this->AuditTrail['remark'] = "Could not update row";			
						$result = 'failed';
					}
				}else{
					$this->AuditTrail['remark'] = "Empty column.";
					$result = 'empty';
				}
				
			}else if($postdata['action'] == 'delete'){
				$query = "DELETE FROM AdvanceReceivingDetail WHERE  $filter";
				if(mssql_query($query) && mssql_rows_affected($this->link)){
					$this->AuditTrail['remark'] = "Successful row delete";			
					$result = 'deleted';
				}else{
					$this->AuditTrail['remark'] = "Could not delete row";			
					$result = 'failed';
				}
			}else{
				$this->AuditTrail['remark'] = "Detail already exists";
				$result = 'exists';
			}
		}else{ 
			foreach($postdata as $k=>$v){
				$postdata[$k] = trim($v);
			}
			$PN = $this->GetPartNumber($partnumber,$DB['DBName'],2,'Partnumber,MixPrice');			
			if( is_array($PN) && isset($PN[0]['Partnumber']) && $PN != 'no data'  && $PN[0]['MixPrice'] !='0.00000'){
				unset($postdata['WawiID'],$postdata['type'],$postdata['action']);
				$query = "INSERT INTO AdvanceReceivingDetail VALUES('".implode("','",$postdata)."')";
				if(mssql_query($query)){
					$this->AuditTrail['remark'] = "Successful row insert";			
					$result = 'success';
				}else{ 
					$this->AuditTrail['remark'] = "Could not insert row";			
					$result = 'failed';
				}
			}else if( is_array($PN) && $PN[0]['MixPrice'] =='0.00000'){
				$this->AuditTrail['remark'] = "Part price is missing.";			
				$result = 'partprice'; 
			}else{
				$this->AuditTrail['remark'] = "Partnumber does not exist.";			
				$result = 'pnnotexist';
			} 
		}		
		$this->AddToAuditTrail();
		return $result;		
	}
	
	/* Advance Receiving : Get Shippers*/
	function GetAdvReceivingShippers($DB,$postdata){				
		mssql_select_db($DB['DBName']); 		
		$this->AuditTrail['action'] = $DB['DBName']." GetAdvReceivingShippers ";
		$query = "select DISTINCT ShipperName,ShipperAddress1,ShipperAddress2,ShipperAddress3,ShipperAddress4 FROM AdvanceReceiving";
		$sql = mssql_query($query);
		if(mssql_num_rows($sql) > 0){					
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$result = $data;	
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);			
		}else{ 
			$this->AuditTrail['remark'] = "No shipper found";
			$result = 'no data';
		}		
		$this->AddToAuditTrail();
		return $result;		
	}
	/* Advance Receiving : Job/Permit Allocation */
	function UpdateARJobPermit($DB,$postdata){				
		mssql_select_db($DB['DBName']); 		
		$this->AuditTrail['action'] = $DB['DBName']." UpdateARJobPermit ";
		$query = "SELECT InvoiceNo FROM AdvanceReceiving WHERE InvoiceDate = '$postdata[shipdate]' ";
		$sql = mssql_query($query);
		if(mssql_num_rows($sql) > 0){		
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);	
			$query = "UPDATE AdvanceReceiving SET JobNo = '$postdata[jobno]', PermitNo = '$postdata[permitno]' WHERE InvoiceDate = '$postdata[shipdate]'  ";
			if(mssql_query($query) && mssql_rows_affected($this->link)){
				$this->AuditTrail['remark'] = "Successful row update";			
				$result = 'success';
			}else{
				$this->AuditTrail['remark'] = "Could not update row";			
				$result = 'failed';
			}			
		}else{
			$this->AuditTrail['remark'] = "No shipper found";
			$result = 'no data';
		}		
		$this->AddToAuditTrail();
		return $result;		
	}
	
	function getTableColumn($DB,$table){
		$data =  array();	
		mssql_select_db($DB);
		$result = mssql_query("SELECT Column_Name FROM Information_schema.Columns WITH (nolock) WHERE Table_Name LIKE '".$table."'");
		while($temp = mssql_fetch_assoc($result)){
			array_push($data,$temp['Column_Name']);
		}
		mssql_free_result($result); 
		return $data;   
	} 
	
	/* Labeling : Get Packlist Number By Order Number*/
	function GetPacklistByOrderNo($DB,$postdata){				
		mssql_select_db($DB['DBName']); 		
		$ordernumber = trim($postdata['ordernumber']);
		$this->AuditTrail['action'] = $DB['DBName']." GetPacklistByOrderNo ".$ordernumber;
		$query = "SELECT PackListNumber FROM PacklistPosition GROUP BY PacklistNumber,DocumentNumber HAVING DocumentNumber = '$ordernumber'";
		$sql = mssql_query($query);
		if(mssql_num_rows($sql) > 0){					
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$result = $data;	
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);			
		}else{ 
			$this->AuditTrail['remark'] = "No packlist found.";
			$result = 'no data';
		}		
		$this->AddToAuditTrail();
		return $result;		
	}
	
	/* Labeling : Send print data to commander */
	function LabelPrintRequest($DB,$postdata){				
		mssql_select_db($DB['DBName']); 		
		$packlistnumber = trim($postdata['packlistnumber']);
		$this->AuditTrail['action'] = $DB['DBName']." LabelPrintRequest ".$packlistnumber;
		switch($postdata['label']){
			case 'inner' : 
				$customer = str_replace(' ','',strtolower($DB['WawiIndex']));			
				include_once("./modules/labeling/inner/$customer.php");
				$IL = new INNERLABEL($DB,$postdata);				
				$result = $IL->printLabel();
			break;
			
			case 'outer' :
				$customer = str_replace(' ','',strtolower($DB['WawiIndex']));			
				include_once("./modules/labeling/outer/$customer.php");
				$OL = new OUTERLABEL($DB,$postdata);				
				$result = $OL->printLabel();
			break;
		}
		
		$this->AddToAuditTrail();
		return $result;		
	}
	
	/* Update Part Price */
	function PartPriceUpdate($DB,$postdata){				
		mssql_select_db($DB['DBName']); 		
		$partnumber = trim($postdata['PartNumber']);
		$price = trim($postdata['Price']);
		$this->AuditTrail['action'] = $DB['DBName']." PartPriceUpdate ".$partnumber;		
		$sql = mssql_query("SELECT Partnumber,MixPrice FROM [Parts] WHERE partnumber='$partnumber'");
		if(mssql_num_rows($sql) > 0){
			if($postdata['action'] == 'update'){
				$query = "UPDATE [Parts] SET [MixPrice] = '$price' WHERE Partnumber='$partnumber' ";
				$res = mssql_query($query);
				if($res && mssql_rows_affected($this->link)){			
					$this->AuditTrail['remark'] = "Successful price update";
					$result = 'success';				
				}else{
					$this->AuditTrail['remark'] = "Could not update price";
					$result = 'failed';
				}	
			}else{
				$this->AuditTrail['remark'] = "Successful row filter.";
				$result = "success|".mssql_result($sql,0,'MixPrice');
			}					
		}else{ 
			$this->AuditTrail['remark'] = "Partnumber number does not exist";
			$result = 'notexist';
		}		
		$this->AddToAuditTrail();
		return $result;		
	}
	 
	function CustomScanout($DBName,$post){
		mssql_select_db($DBName);		
		$auditpost = implode(',',$post);		
		$this->AuditTrail['action'] = "$DBName CustomScanout ".$auditpost;		
		$CSC = $this->CheckScanOutComplete($DBName,$post['packlistnumber'],1);		
	
		switch($post['WawiID']){
			case 56 : 
			case 57 :
				$data = array();
				if ($CSC == 1){
					$data['box'][] = 2;
					$this->AuditTrail['remark'] = "All scanned already";
				}else if (($CSC==2 || $CSC==3) && is_array($post['entrynumber']) ){
					$sql = mssql_query("DELETE FROM Shipboxes WHERE Packlistnumber=".$post['packlistnumber']."");
					foreach($post['entrynumber'] as $entrynumber){
						$sql = mssql_query("SELECT DocumentNumber as OrderNumber FROM PackListPosition WHERE EntryNumber = $entrynumber AND PacklistNumber = ".$post['packlistnumber']);
						if (mssql_num_rows($sql) > 0){					
							$row = mssql_fetch_assoc($sql);
							$OrderNumber = $row['OrderNumber'];					
							$sql = mssql_query("SELECT Packlistnumber FROM Shipboxes WHERE Packlistnumber=".$post['packlistnumber']." AND Entrynumber = $entrynumber ");
							if (mssql_num_rows($sql) > 0){
								$data['line'][$entrynumber] = 3;
								$this->AuditTrail['remark'] = "Scanned already";
							} else {
								$sql = mssql_query("INSERT INTO Shipboxes (Ordernumber,Entrynumber,Packlistnumber) VALUES ($OrderNumber, $entrynumber, ".$post['packlistnumber'].")");
								if ($sql){
									$data['line'][$entrynumber] = 0;
									$this->AuditTrail['remark'] = "Scanned out";
								} else {
									$data['line'][$entrynumber] = 1;
									$this->AuditTrail['remark'] = "Error saving";
								}						
							}
						} else {
							$data['line'][$entrynumber] = 4;
							$this->AuditTrail['remark'] = "Entry number does not exist in packlist position";
						}
					}
				}else if ($CSC==4){
					$data['box'][] = 5;
					$this->AuditTrail['remark'] = "Unexpected error";
				}else if ($CSC==0){
					$data['box'][]= 6;
					$this->AuditTrail['remark'] = "No packlist number found";
				}
				
			break;
			
			default : 
				$this->AuditTrail['remark'] = "Wawi customer not found";
				$data['box'][] = 7;
			break;
		}
		
		$this->AddToAuditTrail();
		return $data;
	}
	
	/* Labeling : Get Packlist Number By Order Number*/
	function GetScanOutStatus($DBName,$postdata){		
		mssql_select_db('MasterData'); 		
		switch($postdata['category']){
			case 1:
				$this->AuditTrail['action'] = $DBName." GetScanOutStatus_1: ".$postdata['packlistnumber'];
				$query = "Execute GetScanOutStatus_1 '".$DBName."','".$postdata['packlistnumber']."'";
			break;
			
			case 2:
				$this->AuditTrail['action'] = $DBName." GetScanOutStatus_2: ".$postdata['ordernumber'];
				$query = "Execute GetScanOutStatus_2 '".$DBName."','".$postdata['ordernumber']."'";
			break;
			
			case 3:
				$this->AuditTrail['action'] = $DBName." GetScanOutStatus_3: ".$postdata['ordernumber'];
				$query = "Execute GetScanOutStatus_3 '".$DBName."','".$postdata['ordernumber']."','".$postdata['FetchData']."'";
			break;
			
			default:
				$result='invalid request';
			break;
		}
		

		$sql = mssql_query($query);
		
		if($sql && mssql_num_rows($sql)){	
			
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$result = $data;
			
			$this->AuditTrail['remark'] = "Total rows: ".mssql_num_rows($sql);
			
		}else if ($sql){
			$this->AuditTrail['remark'] = "empty";		
			$result = 'empty';
		}else{ 
			$this->AuditTrail['remark'] = "error";		
			$result = 'error';
		}		
		$this->AddToAuditTrail();
		return $result;		
	}	
	
	/* For new scanout*/	
	function RecordScanOut($DBName,$ordernumber,$category,$boxArray,$innerArray,$whoscan,$NetWeightEntered,$WawiID){		
		mssql_select_db('MasterData'); 		
		
		switch($category){
			case 1;	break;
			case 2;	break;
			case 3;
			
				$this->AuditTrail['action'] = $DBName." RecordScanOut category $category: ".$ordernumber;
				
				$withError = 0;
				
				$query = '';
				$TotalBoxWeight = 0; // for total weight box weight scanned
				$TotalCartonWeight = 0; // for total weight box weight scanned
				
				$colli = count($boxArray);
				
				for($i=0; $i<count($boxArray); $i++){
					$sql = mssql_query("Execute CheckBoxStatus_3 '".$DBName."','".$ordernumber."','".$boxArray[$i]['boxnumber']."','".$boxArray[$i]['dimensiontype']."'");
					
					$row = mssql_fetch_object($sql);
					
					$boxArray[$i]['remarks'] = $row->result;
					$boxArray[$i]['dimension'] = $row->Dimension;
					$TotalCartonWeight += $boxArray[$i]['cartonweight'] = $row->CartonWeight;
					$TotalBoxWeight += $boxArray[$i]['weight'];
					
					if ($boxArray[$i]['remarks']!='available'){	$withError++; }
					
					for($j=0; $j<count($innerArray); $j++){
						if ($boxArray[$i]['boxnumber']==$innerArray[$j]['boxnumber']){
							if (!isset($innerArray[$j]['remarks'])){
								
								$sql1 = mssql_query("Execute CheckInnerStatus_3 '".$DBName."','".$ordernumber."','".$innerArray[$j]['entrynumber']."','".$innerArray[$j]['quantity']."'");
								
								$row1 = mssql_fetch_object($sql1);
								
								$innerArray[$j]['remarks'] = $row1->result;
								
								if ($innerArray[$j]['remarks']!='available'){ $withError++; }else{
									
									$innerArray[$j]['PackListNumber'] = $row1->PackListNumber;
											
									$query['BoxweightValues'][] = "(".$innerArray[$j]['boxnumber'].", ".$boxArray[$i]['weight'].", ".$innerArray[$j]['PackListNumber'].", '".$boxArray[$i]['dimensiontype']."', '".$boxArray[$i]['dimension']."', ".$ordernumber.")";
									
									$query['PackList'][] = "UPDATE ".$DBName.".dbo.PackList SET [Name] = '".$whoscan."', DatePacked = '".date("m/d/Y")."', TimePacked = '".date("H:i")."' WHERE DocumentNumber=".$ordernumber." AND PacklistNumber=".$innerArray[$j]['PackListNumber']; 							
									
									$query['ShipboxesValues'][] = "(".$ordernumber.", ".$innerArray[$j]['boxnumber'].", ".$innerArray[$j]['entrynumber'].", ".$innerArray[$j]['quantity'].", ".$innerArray[$j]['PackListNumber'].")";
									
									$query['PackListPosition'][] = "UPDATE ".$DBName.".dbo.PackListPosition SET Box=".$innerArray[$j]['boxnumber'].",carton=".$innerArray[$j]['carton']." WHERE EntryNumber=".$innerArray[$j]['entrynumber']." AND PackListnumber=".$innerArray[$j]['PackListNumber'];
								}
	
							}
						}
					}
				}
				
				if ($withError){
					$data['result'] = 'withError';
				}else{
					
					/*-----------------------------------------------+
					| The following script is to remove scan out data|
					+-----------------------------------------------*/
					$sql_0 = "UPDATE ".$DBName.".dbo.PackList set Colli=0, NetWeight=0, Name=null, [Weight]=null, DatePacked=null, TimePacked=null, Dimension=null WHERE DocumentNumber = ".$ordernumber.";";
					$sql_1 = "DELETE FROM ".$DBName.".dbo.Boxweight WHERE Packlistnumber IN (SELECT PackListNumber FROM ".$DBName.".dbo.PackList WHERE DocumentNumber=".$ordernumber.");";
					$sql_2 = "DELETE FROM ".$DBName.".dbo.Shipboxes WHERE Ordernumber = ".$ordernumber.";";
					$sql_3 = "UPDATE ".$DBName.".dbo.PackListPosition set carton=NULL, Box=NULL WHERE DocumentNumber = ".$ordernumber.";";
					
					/*-------------------------------------------+
					| the following script is to record scan out |
					+-------------------------------------------*/
					$sql_4 = "INSERT INTO ".$DBName.".dbo.BoxWeight(Boxnumber,[Weight],Packlistnumber,DimensionType,Dimension,ordernumber) values ".implode(', ',array_unique($query['BoxweightValues']));
					
					$sql_5 = "INSERT INTO ".$DBName.".dbo.Shipboxes(Ordernumber, Boxnumber, Entrynumber, Quantity, Packlistnumber) values ".implode(', ',array_unique($query['ShipboxesValues']));
					$sql_6 = implode('; ',array_unique($query['PackListPosition']));
					$sql_7 = implode('; ',array_unique($query['PackList']));
					
					$sql_8 = "SELECT COUNT(DISTINCT(EntryNumber)) as InnerCountRow FROM ".$DBName.".dbo.PackListPosition WHERE DocumentNumber=".$ordernumber.";";
					
					
					mssql_query('BEGIN TRAN');
					
					if (mssql_query($sql_0)){
						if (mssql_query($sql_1)){
							if (mssql_query($sql_2)){
								if (mssql_query($sql_3)){
									if (mssql_query($sql_4)){
										if (mssql_query($sql_5)){
											if (mssql_query($sql_6)){
												if (mssql_query($sql_7)){
													if ($InnerCountResult = mssql_query($sql_8)){
														
														$InnerCountRow = mssql_fetch_object($InnerCountResult);
														
														
														$data['result'] = ($InnerCountRow->InnerCountRow==count($innerArray)) ? 'complete': 'partial';
														
														if ($data['result']=='complete'){
															
															if ($NetWeightEntered=='auto'){
																
																$sql_11 = mssql_query("execute ComputeNetWeight '".$DBName."','".$ordernumber."','".$TotalBoxWeight."','".$TotalCartonWeight."'");
																if ($sql_11 && mssql_num_rows($sql_11)){
																	$rows_11 = mssql_fetch_assoc($sql_11);

																	$data['Netweight'] = (is_numeric($rows_11['NetWeight'])) ? $rows_11['NetWeight']: $TotalBoxWeight;
																	if ($data['Netweight']<0){
																		$data['result']='15';
																		$data['result2']=$data['Netweight'];
																		mssql_query('ROLLBACK');
																	}
																}else{
																	$data['result']='11';
																	return $data;
																}
															}else{
																$data['Netweight']=$NetWeightEntered;
															}
															
															
															
															$sql_9 = "UPDATE ".$DBName.".dbo.PackList SET Colli=".$colli.", [Weight]=".$TotalBoxWeight.", Netweight=".$data['Netweight']." WHERE DocumentNumber=".$ordernumber.";";
															
															if (mssql_query($sql_9)){
																
																switch($WawiID){
																	case 28: //All under Lattice Wawi
																	
																		$sql_10 = mssql_query("select OrderNumber,PackListNumber,Box 
																		from ".$DBName.".dbo.v_CheckCOO 
																		where Consignee like '%HUAWEI%' and OrderNumber=".$ordernumber."
																		group by OrderNumber,PackListNumber,Box 
																		having COUNT(DISTINCT(COO)) > 1;");
																		
																		if ($sql_10 && mssql_num_rows($sql_10)){
																			while($rows_10[] = mssql_fetch_object($sql_10));
																			array_pop($rows_10);
																			
																			$data['result']='10';
																			$data['result2']=$rows_10;
																			mssql_query('ROLLBACK');
																		}else if ($sql_10){
																			
																			$sql_12 = mssql_query("select Box
																			from ".$DBName.".dbo.v_ZTEFilter 
																			where Shippingname like '%ZTE%'
																			and DocumentNumber=".$ordernumber."
																			group by Box having COUNT(DISTINCT PartNumber)>1");
																			
																			if ($sql_12 && mssql_num_rows($sql_12)){
																				while($rows_12[] = mssql_fetch_object($sql_12));
																				array_pop($rows_12);
																			
																				$data['result']='12';
																				$data['result2']=$rows_12;
																				mssql_query('ROLLBACK');
																			}else if($sql_12){
																				mssql_query('COMMIT');
																				//mssql_query('ROLLBACK');
																			}else{
																				$data['result']='14';
																				mssql_query('ROLLBACK');
																			}

																		}else{
																			$data['result']='13';
																			mssql_query('ROLLBACK');
																		}
																	break;
																	
																	default:
																		mssql_query('COMMIT');
																		//mssql_query('ROLLBACK');
																	break;
																}
																
															}else{
																$data['result'] = '9';
																mssql_query('ROLLBACK');
															}
															
														}else{
															mssql_query('COMMIT');
															//mssql_query('ROLLBACK');
														}
														
													}else{
														$data['result'] = '8';
														mssql_query('ROLLBACK');
													}
												}else{
													$data['result'] = '7';
													mssql_query('ROLLBACK');
												}
											}else{
												$data['result'] = '6';
												mssql_query('ROLLBACK');
											}
										}else{
											$data['result'] = '5';
											mssql_query('ROLLBACK');
										}
									}else{
										$data['result'] = '4';
										mssql_query('ROLLBACK');
									}
								}else{
									$data['result'] = '3';
									mssql_query('ROLLBACK');
								}
							}else{
								$data['result'] = '2';
								mssql_query('ROLLBACK');
							}
						}else{
							$data['result'] = '1';
							mssql_query('ROLLBACK');
						}
					}else{
						$data['result'] = '0';
						mssql_query('ROLLBACK');
					}
						
					
						
				}
				
				$data['boxArray'] = $boxArray;
				$data['innerArray'] = $innerArray;
				$this->AuditTrail['remark'] = $data['result'];
				$this->AddToAuditTrail();
				
				return $data;
			break;
		}
	}
	
	/* For new scanout*/
	function UndoScanOut($DBName,$ordernumber,$whoscan){		
		mssql_select_db('MasterData');
		$this->AuditTrail['action'] = $DBName." UndoScanOut ".$ordernumber;
		
		$sql = mssql_query("Execute UndoScanOut_1 '".$DBName."','".$ordernumber."'");
		
		if ($sql){
			$data = mssql_fetch_assoc($sql);
			$result = $data['result'];
		}else{
			$result= "error";
		}
		$this->AuditTrail['remark'] = "error";
		$this->AddToAuditTrail();
		return $result;
	}
	
	function CheckBooking($DBName,$ordernumber,$entrynumber,$quantity,$whoscan){		
		mssql_select_db($DBName);
		$this->AuditTrail['action'] = $DBName." CheckBooking ".$entrynumber;
		
		$sql = mssql_query("SELECT EntryNumber,SUM(Quantity) as Quantity FROM PackListPosition WHERE DocumentNumber=".$ordernumber." AND EntryNumber=".$entrynumber." group by EntryNumber;");
		
		if ($sql && mssql_num_rows($sql)){
			//$result = 'exist';
			$data = mssql_fetch_assoc($sql);
			
			if ($data['Quantity']!=$quantity){
				$result= "quantity not match";
			}else{
				$result = "success";
			}

		}else if($sql){
			$result= "empty";
		}else{
			$result= "error";
		}
		$this->AuditTrail['remark'] = "error";
		$this->AddToAuditTrail();
		return $result;
	}
}
Class iWMSExt1{
	public function emailme($to,$subject,$message){
		$MAIL_HEADERS = "\n--nextPart\n";
		$MAIL_HEADERS .= "Content-type: text/html; charset=iso-8859-1\r\n";
		$MAIL_HEADERS .= "From: JSI Notification <".MAIL_FROM.">"; //don't forget to add at config.php
		if (mail($to, $subject, $message, $MAIL_HEADERS)){
			echo "success";
		} else {
			echo "failed";
		}
	}
	function BrowserVersion($pos,$BROWSER){
		for ($i=$pos,$v = ''; is_numeric(substr($BROWSER,($i),1)) || substr($BROWSER,($i),1)=='.'; $i++){
			$v .= substr($BROWSER,($i),1);
		}
		return $v;
	}
	
	function DetectBrowser(){
		$b = array('MSIE','Firefox','Chrome','Safari','Opera', 'iPhone');
		$BROWSER = $_SERVER['HTTP_USER_AGENT'];
		$msie = strpos($BROWSER,$b[0]);
		$ffox = strpos($BROWSER,$b[1]);
		$Chrome = strpos($BROWSER,$b[2]);
		$Safari = strpos($BROWSER,$b[3]);
		$Opera = strpos($BROWSER,$b[4]);
		$iPhone = strpos($BROWSER,$b[5]);
		
		if ($msie!==false) {
			$browser_name=substr($BROWSER,$msie,strlen($b[0]));
			$browser_version = $this->BrowserVersion($msie+5,$BROWSER);
		} elseif ($ffox!==false) {
			$browser_name=substr($BROWSER,$ffox,strlen($b[1]));
			$browser_version = $this->BrowserVersion($ffox+8,$BROWSER);
		} elseif ($Chrome!==false) {
			$browser_name=substr($BROWSER,$Chrome,strlen($b[2]));	
			$browser_version = $this->BrowserVersion($Chrome+7,$BROWSER);
		} elseif ($Safari!==false) {
			$browser_name=substr($BROWSER,$Safari,strlen($b[3]));
			$browser_version = $this->BrowserVersion(strpos($BROWSER,'Version')+8,$BROWSER);
		} elseif ($Opera!==false) {		
			$browser_name=substr($BROWSER,$Opera,strlen($b[4]));
			$browser_version = $this->BrowserVersion(strpos($BROWSER,'Version')+8,$BROWSER);
		} elseif ($iPhone!==false) {		
			$browser_name=substr($BROWSER,$iPhone,strlen($b[5]));
			$browser_version = $this->BrowserVersion(strpos($BROWSER,'OS')+3,$BROWSER);
		} else {
			$browser_name='';
			$browser_version=$BROWSER;
		}
		
		if ($browser_name){
			return $browser_name.' - '.$browser_version;
		}else{
			return $BROWSER;	
		}	
	}	
}

/*ENCRYPTION
$cipher = new cipher('andrew');
$data = $cipher->encrypt('testing only');	
*/
class cipher{
    private $securekey, $iv_size, $iv;
		
    function __construct() {
        $this->securekey = hash('sha256',SECRET_KEY,TRUE);
				$this->iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC);
				$this->iv = mcrypt_create_iv($this->iv_size,MCRYPT_RAND);
    }
    function encrypt($input) {
				$input = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $this->securekey, $input, MCRYPT_MODE_ECB, $this->iv));
        return $input;
    }
    function decrypt($input) {
				$input =  trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $this->securekey, base64_decode($input), MCRYPT_MODE_ECB, $this->iv));
        return $input;
    }
}

?>
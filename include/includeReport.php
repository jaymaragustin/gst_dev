<?php
function PrintReport($DBName,$Viewname,$SqlStatement,$FileName){
$conn = mssql_connect("10.128.1.3","sa","express14");
mssql_select_db("MasterData");
$sql = mssql_query($SqlStatement);

while($row[]=mssql_fetch_assoc($sql)){}
array_pop($row);

$getTableStructure = mssql_query("select fieldname from ReportFilters where [status] = 1 and viewname = '$Viewname' and wawiid = '$DBName' order by [orderlist]");
while($rowColumn[]=mssql_fetch_assoc($getTableStructure)){}
array_pop($rowColumn);

$MyTitle = mssql_fetch_assoc(mssql_query("select ReportName from ReportsList where ReportColumns like '$Viewname'"));

date_default_timezone_set('Europe/London');

/** PHPExcel */
require_once 'PHPExcel.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                                         ->setLastModifiedBy("JSI Dev Team")
                                                         ->setTitle("Office 2005 XLS Document")
                                                         ->setSubject("Office 2005 XLS Document")
                                                         ->setDescription("Auto generated report. iWMS")
                                                         ->setKeywords("office 2005 openxml php")
                                                         ->setCategory("Result file");


for($i=0;$i < count($rowColumn);$i++){
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue(getNameFromNumber($i+1)."1",$rowColumn[$i]['fieldname']);
}

$CurrentRow = 2;
for($datacount=0;$datacount < count($row);$datacount++){
	for($i=0;$i < count($rowColumn);$i++){
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue(getNameFromNumber($i+1).$CurrentRow,$row[$datacount][$rowColumn[$i]['fieldname']]);
	}
	$CurrentRow++;
}

// Rename sheet
$objPHPExcel->getActiveSheet()->setTitle($MyTitle['ReportName']);


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$FileName.'.xls"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
}

function getNameFromNumber($num) {
  $numeric = ($num - 1) % 26;
  $letter = chr(65 + $numeric);
  $num2 = intval(($num - 1) / 26);
  if ($num2 > 0) {
      return $this->getNameFromNumber($num2) . $letter;
  } else {
      return $letter;
  }
}
?>
<script src="jQueryUI/jquery-1.7.2.min.js"></script>
<script src="jQueryUI/ui/minified/jquery.ui.core.min.js"></script>
<script src="jQueryUI/ui/minified/jquery.ui.widget.min.js"></script>
<script src="jQueryUI/ui/minified/jquery.ui.position.min.js"></script>
<script src="jQueryUI/ui/minified/jquery.ui.autocomplete.min.js"></script>
<script src="jQueryUI/ui/minified/jquery.ui.accordion.min.js"></script>

<script src="jQueryUI/jquery.tools.min.js"></script>

<script src="js/ddsmoothmenu.js"></script> 
<script>
function is_mobile() {
	var agents = ['android', 'webos', 'iphone', 'ipad', 'blackberry'];
	for(i in agents) {
		if(navigator.userAgent.match('/'+agents[i]+'/i')) {
			return true;
		}
	}
	return false;
}

if(!is_mobile()){
	ddsmoothmenu.init({
		mainmenuid: "templatemo_menu", //menu DIV id
		orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
		classname: 'ddsmoothmenu', //class added to menu's outer DIV
		//customtheme: ["#1c5a80", "#18374a"],
		contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
	});
}
</script>
<!--
<script src="js/cufon-yui.js"></script>
<script src="js/arial.js"></script>
<script src="js/cuf_run.js"></script>
-->
<script src="js/fileuploader.js"></script>

<script src="js/jquery.ajaxSubmit.js"></script>

<script src="js/jquery.rsv.js"></script>
<script src="js/jquery.hotkeys.js"></script> 


<script src="jQueryUI/jQueryDim.js"></script>

<script src="jQueryUI/DataTables/jquery.dataTables.min.js"></script>
<script src="jQueryUI/DataTables/extras/TableTools/media/js/ZeroClipboard.js"></script>
<script src="jQueryUI/DataTables/extras/TableTools/media/js/TableTools.js"></script>
<script src="jQueryUI/DataTables/extras/FixedColumns/media/js/FixedColumns.min.js"></script>

<script src="jQueryUI/ui/jquery-ui-1.8.16.custom.js"></script>

<script src="jQueryUI/ui/i18n/jquery.ui.datepicker-en-GB.js"></script>

<script src="jQueryUI/jQuery.editable.js"></script>
<!-- <script src="jQueryUI/ui/jquery.timepicker.ext.js"></script>-->
<script type="text/javascript" src="js/scripts.js"></script>
<!--<script src="js/webkit.js"></script>-->

<?php
require('config.php');
ini_set('memory_limit', '1024M');
Class iWMS extends iWMSExt1{
	//MSSQL
	var $serverConn = array("S1" => array("10.128.1.15","sa","specctypera"), "S2" => array("wawi.jsis.com.sg","sa","specctypera"), "S3" => array("10.128.1.3","sa","express14"));
	
	var $link = "None";
	var $AuditTrail = array();
	
	function __construct($userid){
		//parent::__construct();
		$this->AuditTrail['user_id'] = $userid;
		$this->AuditTrail['ip'] = $_SERVER['REMOTE_ADDR'];
		$this->AuditTrail['browser'] = $this->DetectBrowser();
		$this->MyConnection("S3");
	}
	
	function MyConnection($ctype){
		if ($this->link == "None"){
			$this->link = mssql_connect($this->serverConn[$ctype][0], $this->serverConn[$ctype][1], $this->serverConn[$ctype][2]);
			$this->AuditTrail['action'] = "Making connection to server";
			if (!$this->link) {
				$this->AuditTrail['remark'] = "Unable to connect to server: ".$ctype;
				$this->AddToAuditTrailToFile();
			} else {
				$this->AuditTrail['remark'] = "Successful connection to server: ".$ctype;
				$this->AddToAuditTrail();
			}
		}
	}
	
	function CloseMyConnection(){
		if ($this->link){
			$this->AuditTrail['action'] = "Closing mssql connection";
			$this->AuditTrail['remark'] = "Closing mssql connection";
			$this->AddToAuditTrail();
			mssql_close($this->link);
			$this->link = "None";
		}
	}

	function AddToAuditTrail(){
		mssql_select_db("MasterData");
		mssql_query("insert into AuditTrail (user_id,ip,browser,action,remarks) values ('".$this->AuditTrail['user_id']."','".$this->AuditTrail['ip']."','".$this->AuditTrail['browser']."','".$this->AuditTrail['action']."','".$this->AuditTrail['remark']."')");
		$this->AddToAuditTrailToFile();
	}
	
	function AddToAuditTrailToFile(){	
		$myFile = "lastresort/logs.".date("Ymd").".txt";
		$fh = fopen($myFile, 'a+');
		$stringData = "[Date: ".date("Y-m-d H:i:s")."]\t[UserID:".$this->AuditTrail['user_id']."]\t[IP:".$this->AuditTrail['ip']."]\t[Browser:".$this->AuditTrail['browser']."]\t[Action:".$this->AuditTrail['action']."]\t[Remarks:".$this->AuditTrail['remark']."]\n";
		fwrite($fh, $stringData);
		fclose($fh);
	}
	
	function GetWawiCustomer($filter){
		mssql_select_db("MasterData");
		
		$filter = ($filter) ? 'and ID='.$filter: '';
		
		$this->AuditTrail['action'] = "Filtering WawiCustomer: all iwms active";
		$sql = mssql_query("select ID,WawiIndex,DBName,GST,STG from WawiCustomer where [status] = 1 ".$filter." order by WawiIndex ASC;");

		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		} else {	
			$this->AuditTrail['remark'] = "No Data";
			$data = 'no data';
		}
		
		$this->AddToAuditTrail();
		
		//$this->CloseMyConnection();
		
		return $data;
	}
	
	function GetWawiReportList($WawiID){
		mssql_select_db("MasterData");
		
		$this->AuditTrail['action'] = "Filtering Wawi Report List: $WawiID";
		
		$sql = mssql_query("SELECT DBName FROM WawiCustomer WHERE ID=$WawiID AND [status]=1;");
		
		if(mssql_num_rows($sql) > 0){
			$data = mssql_fetch_assoc($sql);
			
			$WawiDB = $data['DBName'];
			
			$sql = mssql_query("select * from ReportsList where ReportName like 'WH Re%' or ReportName like 'WH Sc%' or ReportName like 'SC%' or ReportName like 'strat%' or ReportName like 'Inventory%' or ReportName like 'Shipping Log%' or ReportName like 'Receiving Report%'");
			if(mssql_num_rows($sql) > 0){
				
				$data = '';
				
				while($row[] = mssql_fetch_assoc($sql)){}
				
				mssql_select_db($WawiDB);
				$j=0;
				for ($i=0;$i<count($row);$i++){
					
					$sql = "select Column_Name,DATA_TYPE from Information_schema.Columns with (nolock) where Table_Name like '".$row[$i]['ReportColumns']."' order by Column_Name";
					$getTableStructure = mssql_query($sql);
					
					if (mssql_num_rows($getTableStructure) > 0){
						$data[$j]['ReportColumns'] = $row[$i]['ReportColumns'];
						$data[$j++]['ReportName'] = urlencode($row[$i]['ReportName']);
					}
				}
				
				$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
			}else{
				$this->AuditTrail['remark'] = "No Data";
				$data = 'no data';
			}

		} else {	
			$this->AuditTrail['remark'] = "No Data";
			$data = 'no data';
		}
		
		
		
		$this->AddToAuditTrail();
		//$this->CloseMyConnection();
		
		return $data;
	}
	
	function GetFilterFields($WawiID,$view){
		mssql_select_db("MasterData");
		
		$this->AuditTrail['action'] = "Filtering report filter fields: $WawiID,$view";
		
		$sql = mssql_query("SELECT DBName FROM WawiCustomer WHERE ID=$WawiID AND [status]=1;");
		
		if(mssql_num_rows($sql) > 0){
			$data = mssql_fetch_assoc($sql);
			$WawiDB = $data['DBName'];
			mssql_select_db($WawiDB);
			
			$getTableStructure = mssql_query("select Column_Name,DATA_TYPE from Information_schema.Columns with (nolock) where Table_Name like '".$view."' order by Column_Name");
			
			if (mssql_num_rows($getTableStructure) > 0){ 
				$data = '';
				while($row = mssql_fetch_assoc($getTableStructure)){
					$data[] = $row['Column_Name'];
				}
				
				$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
				
			}else{
				
				$this->AuditTrail['remark'] = "No Data";
				$data = 'no data';
				
			}
		}else{
			$this->AuditTrail['remark'] = "No Data";
			$data = 'no data';
		}
		
		$this->AddToAuditTrail();
		
		//$this->CloseMyConnection();
		
		return $data;
	}
	
	function GetReportData($WawiID,$view,$field,$filter,$reportname,$range_from,$range_to){
		mssql_select_db("MasterData");
		
		$this->AuditTrail['action'] = "Filtering report filter fields: $WawiID,$view,$field,$filter,$reportname,$range_from,$range_to";
		
		$sql = mssql_query("SELECT DBName FROM WawiCustomer WHERE ID=$WawiID AND [status]=1;");
		
		if(mssql_num_rows($sql) > 0){
			$data = mssql_fetch_assoc($sql);
			$WawiDB = $data['DBName'];
			
			$filterval = trim($filter);
			$range_from_val = trim($range_from);
			$range_to_val = trim($range_to);
			
			if ($filterval == ""){
				$params = "1=1";
				if ($range_from_val != '' && $range_to_val != ''){
					$params .= " AND [".$field."] >= '".$range_from_val."' AND [".$field."] <= '".$range_to_val."'";
				}
			} else {
				if (substr_count($filterval, '/') == 2){
					$params = "[".$field."] = '".$filterval."'";
				}elseif(is_numeric($filterval)){
					$params = "[".$field."] = '".$filterval."'";
				}
				else{
					$params = "[".$field."] like '".$filterval."'";
				}
			}

			mssql_select_db("MasterData");
			
			$sqlStatement = "execute MainReport \"".$WawiDB."\",\"".$reportname."\",\" where $params\"";
			
			//mssql_select_db("MasterData");
			//$sqlStatement ="SELECT * FROM $WawiDB.dbo.[$reportname] WHERE $params;";

			//return $sqlStatement;
			//echo $sqlStatement;
			
			$sql = mssql_query($sqlStatement);
			
			$data = '';

			while($data[]=mssql_fetch_assoc($sql));
			array_pop($data);
			
			$dataCtr = count($data);
			
			if ($dataCtr < 1){
				$this->AuditTrail['remark'] = "No Data";
				$return = 'no data';
			}else if (count($data) > ReportLimit){
				$this->AuditTrail['remark'] = "Out of Range";
				$return = $dataCtr;
			}else{
				$return = $data;
			}

		}else{
			$this->AuditTrail['remark'] = "no db";
			$return = 'no db';
		}
		
		$this->AddToAuditTrail();
		
		return $return;
	}
	
	function GetPicklistInfo($DBName,$PacklistNumber){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "Filtering Packlistposition: ".$DBName;
		
		$sql = mssql_query("SELECT     dbo.PackListPosition.*, dbo.Shipboxes.Boxnumber AS ShipBoxNumber
FROM         dbo.PackListPosition INNER JOIN
                      dbo.Shipboxes ON dbo.PackListPosition.PackListNumber = dbo.Shipboxes.Packlistnumber AND 
                      dbo.PackListPosition.DocumentNumber = dbo.Shipboxes.Ordernumber AND dbo.PackListPosition.EntryNumber = dbo.Shipboxes.Entrynumber
WHERE     (dbo.PackListPosition.PackListNumber = ".$PacklistNumber.")");

		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		} else {	
			$this->AuditTrail['remark'] = "No Data";
			$data = 'no data';
		}
		
		$this->AddToAuditTrail();
		
		//$this->CloseMyConnection();
		
		return $data;
	}
	
	function UndoNormalScanOut($DBName,$PacklistNumber){
		mssql_select_db("MasterData");
		$this->AuditTrail['action'] = "Undo Normal Scanout ".$DBName.": ".$PacklistNumber;
	
		$result = mssql_fetch_assoc(mssql_query("execute UndoNormalScanOut '".$DBName."',".$PacklistNumber.";"));
			
		if($result['result']=="Success"){
			$data = 'success';
			$this->AuditTrail['remark'] = "Success";
		} else {	
			$this->AuditTrail['remark'] = "Failed";
			$data = 'failed';
		}

		$this->AddToAuditTrail();
		
		//$this->CloseMyConnection();
		
		return $data;
	}
	
	function GetEntryStockIndex($DBName){
		
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "Filtering StockIndex: Entry:".$DBName;
		$sql = mssql_query("SELECT DISTINCT StockIndex FROM [Entry] ORDER BY StockIndex;");

		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		} else {	
			$this->AuditTrail['remark'] = "No Data";
			$data = 'no data';
		}
		
		$this->AddToAuditTrail();
		
		//$this->CloseMyConnection();
		
		return $data;
	}
	
	function ResetCycleCount($DBName,$StockIndex){
		mssql_select_db("MasterData");
		
		$this->AuditTrail['action'] = "Reset Cycle Count ".$DBName.": ".$StockIndex;
		
		$result = mssql_fetch_assoc(mssql_query("execute ResetCycleCount '".$DBName."','".$StockIndex."';"));
			
		if($result['result']=="Success"){
			$data = 'success';
			$this->AuditTrail['remark'] = "Success";
		} else {	
			$this->AuditTrail['remark'] = "Failed";
			$data = 'failed';
		}

		$this->AddToAuditTrail();
		
		//$this->CloseMyConnection();
		
		return $data;
	}
	
	function GetReceivingFields($WawiID){
		mssql_select_db('MasterData');
		
		$this->AuditTrail['action'] = "Filtering ReceivingFields: ".$WawiID;
		
		$sql = mssql_query("SELECT * FROM v_ReceivingSetup WHERE WawiID = '".$WawiID."' Order By OrderList");

		if(mssql_num_rows($sql) > 0){
			$i=0;
			while($data[$i] = mssql_fetch_assoc($sql)){
				if ($data[$i]['ElementType']=='select'){
					
					
					
					if($data[$i]["FieldName"]=='Supplier'){
						//SELECT [Index],[Name1] FROM Supplier
						$sql2 = mssql_query("SELECT [Index],[Name1] as [Description] FROM ".$data[$i]['DBName'].".dbo.Supplier ORDER BY [Index] ASC;");
						
						if(mssql_num_rows($sql2) > 0){
							while($data[$i]['Options'][] = mssql_fetch_assoc($sql2));
							array_pop($data[$i]['Options']);
						}
					}
					
					if($data[$i]["FieldName"]=='StockIndex'){
						$sql2 = mssql_query("SELECT [Index],[Description] FROM ".$data[$i]['DBName'].".dbo.Stocks where [Description] <> '';");
						
						if(mssql_num_rows($sql2) > 0){
							while($data[$i]['Options'][] = mssql_fetch_assoc($sql2));
							array_pop($data[$i]['Options']);
						}

					}
					
					
					if($data[$i]["FieldName"]=='Comment'){
						$sql3 = mssql_query("SELECT PackageType as [Index], [Description] FROM PackageType WHERE WawiID = ".$WawiID." AND [status]=1;");
						
						if(mssql_num_rows($sql3) > 0){
							while($data[$i]['Options'][] = mssql_fetch_assoc($sql3));
							array_pop($data[$i]['Options']);
						}

					}
					
					if($data[$i]["FieldName"]=='TaxScheme'){
						$sql3 = mssql_query("SELECT TaxScheme as [Index], [ID] as 'Description' FROM TaxScheme WHERE [status]=1;");
						
						if(mssql_num_rows($sql3) > 0){
							while($data[$i]['Options'][] = mssql_fetch_assoc($sql3));
							array_pop($data[$i]['Options']);
						}

					}
					
					/*
					if($data[$i]["FieldName"]=='CountryOfOrigin'){
						$sql3 = mssql_query("SELECT country_code as [Index], country_name as [Description] FROM STGMM.dbo.Country;");
						
						if(mssql_num_rows($sql3) > 0){
							while($data[$i]['Options'][] = mssql_fetch_assoc($sql3));
							array_pop($data[$i]['Options']);
						}

					}
					*/
				}
				$i++;
			}
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		} else {	
			$this->AuditTrail['remark'] = "No Data";
			$data = 'no data';
		}
		
		$this->AddToAuditTrail();
		
		//$this->CloseMyConnection();
		
		return $data;
	}
	
	function InsertIntoImport($wawiindex, $reference){
		mssql_select_db("ImportTest");
		
		$this->AuditTrail['action'] = "Filtering Import ".$wawiindex.": ".$reference;
		
		$sql = mssql_query("SELECT * FROM Import WHERE Reference = ".$reference." AND WawiIndex='".$wawiindex."'");
		if(mssql_num_rows($sql) > 0){
			//nothing to do
		}else{
			$sql = mssql_query("INSERT INTO Import (Reference, WawiIndex, InvoiceValue, InvoiceValueSGD, FreightCharges, InsCharges, CIFAmount, GSTAmount) VALUES (".$reference.",'".$wawiindex."',0,0,0,0,0,0)");
			if($sql && mssql_rows_affected($this->link)){
				$this->AuditTrail['remark'] = "Successfully inserted import data ".$reference." in ".$wawiindex;
			}else{
				$this->AuditTrail['remark'] = "Failed to insert import data ".$reference." in ".$wawiindex;
			}
		}
		
		$this->AddToAuditTrail();
		
	}
	
	function GetPartNumber($partnumber,$DBName){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "Filtering Parts ".$DBName.": ".$partnumber;
		
		$sql = mssql_query("SELECT * FROM Parts WHERE Partnumber = '".$partnumber."';");

		if(mssql_num_rows($sql) > 0){
			$data = mssql_fetch_assoc($sql);
		}else{
			$this->AuditTrail['remark'] = "EntryNumber Already Exist";
			$data = 'no data';
		}
		
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	function SaveReceiving($post,$DB){
		
		mssql_select_db($DB['DBName']);
		
		$this->AuditTrail['action'] = "Filtering Receiving ".$DB['WawiIndex'].": ".$post['EntryNumber'];
		
		$sql = mssql_query("SELECT * FROM [Entry] WHERE EntryNumber = ".$post['EntryNumber'].";");

		if(mssql_num_rows($sql) > 0){
			$this->AuditTrail['remark'] = "EntryNumber Already Exist";
			$result = 2;
		}else{
			
			$partsDetails = $this->GetPartNumber($post['Partnumber'],$DB['DBName']);
			
			if ($partsDetails == 'no data'){
				$this->AuditTrail['remark'] = "Partnumber ".$post['Partnumber']." does not exist";
				$result = 3;
			}else if ($partsDetails['PurchaseLock']){
				$this->AuditTrail['remark'] = "Partnumber ".$post['Partnumber']." purchase locked";
				$result = 4;
			}else if ($post['Quantity'] < 1){
				$this->AuditTrail['remark'] = "Quantity invalid";
				$result = 5;
			}else{
			
				if ($DB['GST']){
					$this->InsertIntoImport($DB['WawiIndex'],$post['ForwarderRef']);
				}
				
				if ($DB['STG']){
					//reserved for STGMM checking
				}
				
				$keystr = '';
				$valstr = '';

				foreach($post as $key => $value){
					if ($key != 'type' && $key != 'WawiID'){
						$keystr .= "$key,";
						$valstr .= "'" . trim($value) . "' ,"; 
					}
				}
				
				mssql_select_db($DB['DBName']);
				
				$sql = mssql_query("INSERT INTO [Entry] (".$keystr."EntryDate) VALUES (".$valstr."'".date('Y-m-d H:i:s')."');");
				
				if($sql && mssql_rows_affected($this->link)){
					$this->AuditTrail['remark'] = "Success receiving: ".$post['EntryNumber'];
					$result = ($partsDetails['LifeSensitive']) ? 6: 7;
					
					
				}else{
					$this->AuditTrail['remark'] = "failed receiving: ".$post['EntryNumber'];
					$result = 8;
				}
				
			}
		}
		
		$this->AddToAuditTrail();
		return $result;
		
	}
	
	function RetainReceivingField($WawiID){
		mssql_select_db("MasterData");
		
		$this->AuditTrail['action'] = "Filtering retain receiving field ".$WawiID;
		
		$sql = mssql_query("SELECT FieldName FROM v_ReceivingSetup WHERE WawiID = ".$WawiID." AND RetainField=1  Order By OrderList;");

		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "failed";
			$data = 'no data';
		}
		
		$this->AddToAuditTrail();
		
		return $data;
		
	}
	
	function Putaway($DBName,$location,$entrynumber){
		mssql_select_db($DBName);


		$this->AuditTrail['action'] = "Filtering $DBName Entry: $entrynumber";
		
		$sql = mssql_query("SELECT EntryNumber,Location FROM [Entry] WHERE EntryNumber = ".$entrynumber.";");

		if(mssql_num_rows($sql) > 0){
			$data = mssql_fetch_assoc($sql);
			
			if ($location==$data['Location']){
				$this->AuditTrail['remark'] = "EntryNumber is already at location ".$location;
				$result = '1';
			}else{
				$sql = mssql_query("update [entry] set location='$location' where EntryNumber = $entrynumber;");
				if($sql && mssql_rows_affected($this->link)){
					$this->AuditTrail['remark'] = "Update location success. Entry number: ".$entrynumber." | Location: ".$data['Location']." to Location: ".$location;
					$result = '2';
				}else{
					$this->AuditTrail['remark'] = "Failed to update entry [".$entrynumber."] from location[".$data['Location']."] to location[".$location."]";
					$result = '3';
				}
			}
			
		}else{
			$this->AuditTrail['remark'] = "Failed to update entry";
			$result = '0';
		}
		
		$this->AddToAuditTrail();

		/*
		0 - entry number not exists
		1 - location is same
		2 - success
		3 - failed to update
		*/
		
		return $result;
		
	}
	
	function GetEntryParts($DBName){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "Filtering Distinct Entry Parts: ".$DBName;
		
		$sql = mssql_query("SELECT DISTINCT Partnumber FROM [Entry] WHERE Quantity > QuantityOut ORDER BY Partnumber;");

		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "entry partnumber empty";
			$data = 'no data';
		}
		
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	function GetEntryLot($DBName,$partnumber){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "Filtering Distinct Entry Parts: ".$DBName;

		$sql = mssql_query("SELECT LotNumber FROM [Entry] WHERE Quantity > QuantityOut AND Partnumber='".$partnumber."' ORDER BY LotNumber;");

		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "entry partnumber empty";
			$data = 'no data';
		}
		
		$this->AddToAuditTrail();

		return $data;
	}
	
	function GetEntryNumber($DBName,$partnumber){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "Filtering Distinct Entry Parts: ".$DBName;
		
		$sql = mssql_query("SELECT EntryNumber FROM [Entry] WHERE Quantity > QuantityOut AND Partnumber='".$partnumber."' ORDER BY EntryNumber;");

		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "entry entrynumber empty";
			$data = 'no data';
		}
		
		$this->AddToAuditTrail();

		return $data;
	}
	
	function GetEntryNumberbyLot($DBName,$partnumber,$lotnumber){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "Filtering Distinct Entry Parts: ".$DBName;
		
		$sql = mssql_query("SELECT EntryNumber FROM [Entry] WHERE Quantity > QuantityOut AND Partnumber='".$partnumber."' AND LotNumber='".$lotnumber."' ORDER BY EntryNumber;");

		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "entry entrynumber empty";
			$data = 'no data';
		}
		
		$this->AddToAuditTrail();

		return $data;
	}
	
	function GetEntrySum($DBName,$partnumber,$lotnumber,$entrynumber){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "Filtering $DBName Distinct Entry Parts";
		
		$filter = '';
		$filter .= ($lotnumber) ? " AND LotNumber='$lotnumber' ": '';
		$filter .= ($entrynumber) ? " AND EntryNumber=$entrynumber ": '';
		
		$sql = mssql_query("SELECT SUM(Quantity-QuantityOut) AS SumPart FROM Entry WHERE Partnumber='".$partnumber."' $filter;");

		if(mssql_num_rows($sql) > 0){
			$data = mssql_fetch_assoc($sql);
			$this->AuditTrail['remark'] = "Successful filter sum: ".count($data['SumPart']);
		}else{
			$this->AuditTrail['remark'] = "no data";
			$data = 'no data';
		}
		
		$this->AddToAuditTrail();

		return $data;
	}
	
	function EntryComment($DBName,$partnumber,$lotnumber,$entrynumber,$comment,$block){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "Updating $DBName Entry Comment,$partnumber,$lotnumber,$entrynumber,$comment,$block";
		
		$setfields = " set Comment = '".trim($comment)."'";
		
		if ($block == 0){
			$setfields.=",Blocked = 0";
		} elseif($block == 1){
			$setfields.=",Blocked = (Quantity - QuantityOut)";
		}
		
		$statement = "update [entry] $setfields where Partnumber = '".$partnumber."'";
		$statement.= (strlen($lotnumber) > 0) ? " AND LotNumber='".$lotnumber."'": '';
		$statement.= (strlen($entrynumber) > 0) ? " AND EntryNumber=".$entrynumber: '';
		
		
		$sql = mssql_query($statement);

		if($sql && mssql_rows_affected($this->link)){
			$this->AuditTrail['remark'] = "success";
			$data = 'success';
		}else{
			$this->AuditTrail['remark'] = "failed";
			$data = 'failed';
		}
		
		$this->AddToAuditTrail();

		return $data;
	}

	function GetDivision($DBName){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "Filtering $DBName stocks table";
		
		$sql = mssql_query("SELECT DISTINCT [Index] as Division FROM Stocks ORDER BY [Index];");

		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "no data";
			$data = 'no data';
		}
		
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	function GetPartPerDivision($DBName,$division){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "Filtering $DBName parts per division [".$division."]";
		
		$sql = mssql_query("SELECT DISTINCT Partnumber FROM Entry WHERE (StockIndex='".$division."') AND (QuantityOut<>Quantity) ORDER BY Partnumber;");

		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "no data";
			$data = 'no data';
		}
		
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	function GetEntryTransferList($DBName,$division,$partnumber){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "Filtering $DBName Entry Transfer list division[".$division."]|partnumber[".$partnumber."]";
		
		$sql = mssql_query("SELECT EntryNumber,LotNumber,Quantity,QuantityOut FROM Entry WHERE (StockIndex='".$division."') AND (Partnumber='".$partnumber."') AND (QuantityOut<>QUANTITY) ORDER BY EntryNumber;");

		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "no data";
			$data = 'no data';
		}
		
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	function EntryTransfer($DBName,$selectedEntry,$division,$todivision){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "Updating $DBName Entry Transfer [$division-$todivision]: $selectedEntry";
		
		
		if (strtolower($DBName) == "MarvellWawi"){
			$sql = mssql_query("Update [Entry] set Stockindex='".$todivision."',OldDivision='".$division."' where EntryNumber in (".$selectedEntry.");");
		} else {
			$sql = mssql_query("Update [Entry] set Stockindex='".$todivision."' where EntryNumber in (".$selectedEntry.");");
		}

		if($sql && mssql_rows_affected($this->link)){
			$this->AuditTrail['remark'] = "success";
			$data = 'success';
		}else{
			$this->AuditTrail['remark'] = "failed";
			$data = 'failed';
		}
		
		$this->AddToAuditTrail();

		return $data;
	}
	
	
	function ModifyEntryData($DBName,$entrynumber,$description){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "Filtering $DBName PacklistPosition with entrynumber $entrynumber";
		
		$sql = mssql_query("SELECT [DocumentNumber] FROM [PacklistPosition] WHERE [EntryNumber]=".$entrynumber.";");
		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
			
			
			$query = '';
			for ($i=0;$i<count($data);$i++){
				$query .= "UPDATE [OrderPosition] SET [Description]='".$description."' WHERE [Ordernumber]=" .$data[$i]["DocumentNumber"] ."; ";
			}
			
			$this->AuditTrail['action'] = "Update $DBName Entry Data entrynumber $entrynumber";
			$sql = mssql_query($query);
			if($sql && mssql_rows_affected($this->link)){
				$this->AuditTrail['remark'] = "success";
				$data = 'success';
			}else{
				$this->AuditTrail['remark'] = "failed";
				$data = 'failed';
			}
			
			
		}else{
			$this->AuditTrail['remark'] = "no data";
			$data = 'no data';
		}
		
		$this->AddToAuditTrail();

		return $data;
	}
	
	function CheckPackListScan($DBName,$packlistnumber){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "Filtering $DBName packlist number $packlistnumber";
		
		$sql = mssql_query("Select TOP 1 DocumentNumber from Packlist where PackListnumber = $packlistnumber;");

		if(mssql_num_rows($sql) > 0){
			$PL = mssql_fetch_assoc($sql);
			$DocumentNumber = $PL['DocumentNumber'];
			
			$data = $this->CheckScanComplete($DBName,$DocumentNumber);
			$result = ($data=='no data' || count($data) < 1) ? 'no data': json_encode($data);
		}else{
			$this->AuditTrail['remark'] = "no data";
			$result = 'no data'; 
		}
		
		$this->AddToAuditTrail();
		
		return $result;
	}
	
	function CheckBoxweightScan($DBName,$ordernumber){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "Filtering $DBName boxweight ordernumber $ordernumber";
		
		$sql = mssql_query("Select Boxnumber,[Weight],Dimension from Boxweight where ordernumber = $ordernumber;");

		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "no data";
			$data = 'no data'; 
		}
		
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	function CheckScanComplete($DBName,$DocumentNumber){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "Comparing $DBName DocumentNumber=$DocumentNumber PacklistPosition and Shipboxes count";
		
		$sqlpacklistposition = mssql_query("select * from PacklistPosition where DocumentNumber = $DocumentNumber;");
		$sqlshipboxes = mssql_query("select * from Shipboxes where Ordernumber = $DocumentNumber;");
		
		if (mssql_num_rows($sqlpacklistposition) == mssql_num_rows($sqlshipboxes)){
			$this->AuditTrail['remark'] = "tally with row count: ".mssql_num_rows($sqlpacklistposition);

			$sql = mssql_query("select Shipboxes.Ordernumber,Shipboxes.EntryNumber,Shipboxes.BoxNumber,Shipboxes.Quantity,[Entry].PartNumber from Shipboxes left join [Entry] on Shipboxes.EntryNumber = [Entry].EntryNumber where Shipboxes.Ordernumber = $DocumentNumber");
			if (mssql_num_rows($sql) > 0){
				while($result[] = mssql_fetch_assoc($sql));
				array_pop($result);
			}else{
				$result = 'proceed';
			}

			
		} else {
			$this->AuditTrail['remark'] = "does not tally";
			$result = 'proceed';
		}
		
		$this->AddToAuditTrail();
		
		return $result;
		
	}
	
	function GetDateCode($DBName,$entrynumber){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "Filtering $DBName DateCode with entrynumber $entrynumber";
		
		$sql = mssql_query("Select TOP 1 [DateCode] from [Entry] where EntryNumber = $entrynumber;");

		if(mssql_num_rows($sql) > 0){
			$result = mssql_fetch_assoc($sql);
			$data = $result['DateCode'];

		}else{
			$this->AuditTrail['remark'] = "no data";
			$data = 'no data'; 
		}
		
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	function NormalScanOut($DBName,$packlistnumber,$boxnumber,$weight,$dimentiontype,$entrynumber,$quantity,$whoscan){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "Performing Normal ScanOut";
		
		$sql = mssql_query("Select documentnumber from Packlist where packlistnumber = $packlistnumber;");
		if (mssql_num_rows($sql) < 1){ // check packlist
			$data = 'Invalid Picklist';
		} else { // check quantity
			$row = mssql_fetch_assoc($sql);
			$OrderNumber = $row['documentnumber'];
			
			if($this->CheckScanComplete($DBName,$OrderNumber)!='proceed'){
				$data = 'all box out';
			}else{
				mssql_select_db($DBName);
				
				$sql = mssql_query("Select Entrynumber,DateCode from [Entry] where EntryNumber = $entrynumber;");
				if (mssql_num_rows($sql) < 1){ // Check Entry Number
					$data = "Invalid Entry Number";
				} else {
					$row = mssql_fetch_assoc($sql);
					$DateCode	 = $row['DateCode'];
					
					$sql = mssql_query("Select EntryNumber from PackListPosition where EntryNumber = $entrynumber;");
					if (mssql_num_rows($sql) < 1){
						$data = "Cannot find entry number on packlistposition table";
					} else {
						$sql = mssql_query("Select DocumentNumber from PackListPosition where EntryNumber = $entrynumber and Quantity = $quantity");
						if (mssql_num_rows($sql) < 1){
							$data = "Invalid Quantity";
						} else {
							$sql = mssql_query("Select * from Shipboxes where Ordernumber=$OrderNumber and Entrynumber = $entrynumber");
							if (mssql_num_rows($sql) < 1){
								if (strlen($txtdimension) > 0){
									$sql = mssql_query("select * from tblDimension where DimensionType like '$dimentiontype'");
									if (mssql_num_rows($sql) > 0){ // cannot find the dimensiontype neglectdimension
										$row = mssql_fetch_assoc($sql);
										$dimensionvalue = $row['Dimension'];
									} else {
										$dimensionvalue = "";
									}
								} else {
									$dimensionvalue = "";
								}
								
								$sql = mssql_query("select * from Boxweight where OrderNumber = $OrderNumber and Boxnumber = $boxnumber");
								if (mssql_num_rows($sql) < 1){ // Boxnumber do not exists with the corresponding Ordernumber
									if (mssql_query("Insert into Boxweight (Boxnumber,Weight,Packlistnumber,Ordernumber,DimensionType,Dimension) values ($boxnumber,$weight,$packlistnumber,$OrderNumber,'$dimentiontype','$dimensionvalue')")){
										if (mssql_query("Insert into Shipboxes (Ordernumber,Boxnumber,Entrynumber,Quantity,DateCode,Packlistnumber) values ($OrderNumber,$boxnumber,$entrynumber,$quantity,'$DateCode',$packlistnumber)")){
			
											if($this->CheckScanComplete($DBName,$OrderNumber)!='proceed'){
												$NetWeight = $this->updatemydata($DBName,$OrderNumber);
												$thisweight		 = $NetWeight * 1000;
												
												mssql_select_db($DBName);
												
												$Colli		 = mssql_num_rows(mssql_query("select * from Boxweight where ordernumber = $OrderNumber"));
												mssql_query("update packlist set Colli = $Colli,NetWeight = $NetWeight,[Name] = '$whoscan',weight = $thisweight,DatePacked = '".date("m/d/Y")."',TimePacked = '".date("H:i")."' where documentnumber = $OrderNumber");
											}
											
											$data = "success";
										} else {
											echo "Error in inserting new Shipboxes";
										}
									} else {
										$data = "Error in inserting New Box Details in boxweight";
									}
								} else { // Box weight already exists so just insert on th shipboxes
									if (mssql_query("Insert into Shipboxes (Ordernumber,Boxnumber,Entrynumber,Quantity,DateCode,Packlistnumber) values ($OrderNumber,$boxnumber,$entrynumber,$quantity,'$DateCode',$packlistnumber)")){
										if($this->CheckScanComplete($DBName,$OrderNumber)!='proceed'){
											$NetWeight = $this->updatemydata($DBName,$OrderNumber);
											$thisweight		 = $NetWeight * 1000;
											mssql_select_db($DBName);
											
											$Colli		 = mssql_num_rows(mssql_query("select * from Boxweight where ordernumber = $OrderNumber"));
											mssql_query("update packlist set Colli = $Colli,NetWeight = $NetWeight,[Name] = '$whoscan',weight = $thisweight,DatePacked = '".date("m/d/Y")."',TimePacked = '".date("H:i")."' where documentnumber = $OrderNumber");
										}
										$data = "success";
									} else {
										$data = "Error in inserting new Shipboxes";
									}
								}
								
							} else {
								$data = "Already Exists in Shipboxes";
							} //Already Exists in Shipboxes
						} //Invalid Quantity
					} // Cannot find entry number on packlistposition table
				} // Check Entry Number
			}//check if all scanned
		} // check quantity
		$this->AuditTrail['remark'] = $data;
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	function updatemydata($DBName,$OrderNumber){
		mssql_select_db($DBName);
		
		$gWeight = 0;
		$NetWeight = 0;
		$sql = mssql_query("select partnumber,sum(Quantity) as totalqty,PackageWeight,packlistnumber from packlistposition where documentnumber = $OrderNumber AND partnumber IS NOT NULL group by partnumber,PackageWeight,packlistnumber");
		while($row = mssql_fetch_assoc($sql)){
			$partnumber			= $row['partnumber'];
			$totalqty				= $row['totalqty'];
			$PackageWeight  = trim($row['PackageWeight']);
			$packlistnumber = $row['packlistnumber'];
			
			if ($PackageWeight == ''){
				$sqlparts = mssql_query("select WeightPerPiece from parts where Partnumber = '$partnumber'");
				if (mssql_num_rows($sqlparts) > 0){
					$sqlparts = mssql_fetch_assoc($sqlparts);
					$PackageWeight = $sqlparts['WeightPerPiece'];
				} else {
					$PackageWeight = 0;
				}
			}
			
	
			$gWeight = $gWeight + ($totalqty * $PackageWeight);
		}
		
		$NetWeight = $gWeight / 1000;
		
		return $NetWeight;
	}
	
	
	/* Change Partnumber : Get partnumber - Marketing */
	function GetPartNumberM($DBName){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "Filtering Entry Parts: ".$DBName;
		
		$sql = mssql_query("SELECT PartNumber FROM Parts");

		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "entry partnumber empty";
			$data = 'no data';
		}
		
		$this->AddToAuditTrail();
		
		return $data;
	}
	/* Change Partnumber : Get partnumber - Partnumber */
	function GetPartNumberP($DBName){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "Filtering Entry Parts: ".$DBName;
		
		$sql = mssql_query("SELECT DISTINCT PartNumber FROM Entry WHERE Partnumber NOT IN (SELECT PartNumber FROM PackListPosition) AND Quantity > QuantityOut ORDER BY Partnumber");

		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "entry partnumber empty";
			$data = 'no data';
		}
		
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	/* Change Partnumber : Get partnumber - Supplier */
	function GetPartNumberS($DBName){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "Filtering Entry Parts: ".$DBName;
		
		$sql = mssql_query("SELECT DISTINCT SupplierPartnumber FROM Entry WHERE Quantity > QuantityOut ORDER BY SupplierPartnumber");

		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "entry partnumber empty";
			$data = 'no data';
		}
		
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	/* Change Partnumber : Get VSC Partnumber - Marketing */
	function GetSupplierPartNumber($DBName,$partnumber){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "Filtering Distinct Entry Number: ".$DBName;
		
		$sql = mssql_query("SELECT SupplierPartnumber FROM MarketingPartnumbers WHERE Partnumber='$partnumber'");

		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "entry entrynumber empty";
			$data = 'no data';
		}
		
		$this->AddToAuditTrail();

		return $data;
	}
	
	/* Change Partnumber : Get entry number - Partnumber */
	function GetEntryNumberP($DBName,$partnumber){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "Filtering Distinct Entry Number: ".$DBName;
		
		$sql = mssql_query("SELECT EntryNumber,PartNumber FROM Entry WHERE PartNumber='$partnumber'");

		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "entry entrynumber empty";
			$data = 'no data';
		}
		
		$this->AddToAuditTrail();

		return $data;
	}
	
	/* Change Partnumber : Get entry number - Supplier */
	function GetEntryNumberS($DBName,$partnumber){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "Filtering Distinct Entry Number: ".$DBName;
		
		$sql = mssql_query("SELECT EntryNumber,PartNumber FROM Entry WHERE SupplierPartNumber='$partnumber'");

		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "entry entrynumber empty";
			$data = 'no data';
		}
		
		$this->AddToAuditTrail();

		return $data;
	}
	
	/* Change Partnumber : GET new partnumber */
	function GetNewPartNumber($DBName,$partnumber){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "Filtering Parts ".$DBName.": ".$partnumber;
		
		$sql = mssql_query("SELECT PartNumber FROM Parts WHERE PartNumber <> '$partnumber'");

		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "entry partnumber empty";
			$data = 'no data';
		}
		 
		$this->AddToAuditTrail();
		
		return $data;
	}
	/*Change Partnumber : Update Partnumber - Marketing */
	function UpdatePartNumberM($DBName,$partnumber,$SupplierPartNumber){
		
		$DBName = 'DemoWawi';
		
		mssql_select_db($DBName); 
		
		$this->AuditTrail['action'] = "Insert Parts ".$DBName.": ".$partnumber;
		 
		$statement = "INSERT INTO MarketingPartnumbers (SupplierPartNumber,Partnumber) VALUES ('" . trim($SupplierPartNumber) . "','" . trim($partnumber)."')";
			
		$sql = mssql_query($statement);
	 
		if($sql && mssql_rows_affected($this->link)){
			$this->AuditTrail['remark'] = "Successfully inserted partnumber: ".count($data);
			$data = 'success';
		}else{
			$this->AuditTrail['remark'] = "Failed to update partnumber";
			$data = 'failed';
		}
		 
		$this->AddToAuditTrail();
		
		return $data;
	}
	/*Change Partnumber : Update Partnumber - Partnumber*/
	function UpdatePartNumberP($DBName,$partnumber,$entrynumber,$newpartnumber){
		mssql_select_db($DBName); 
		
		$this->AuditTrail['action'] = "Updating Parts ".$DBName.": ".$partnumber;
		 
		$statement = "UPDATE Entry SET Partnumber='" . $newpartnumber ."' WHERE EntryNumber in (" . $entrynumber . ")";	
		
		$sql = mssql_query($statement);

		if($sql && mssql_rows_affected($this->link)){
			$this->AuditTrail['remark'] = "Successfully update partnumber: ".count($data);
			$data = 'success';
		}else{
			$this->AuditTrail['remark'] = "Failed to update partnumber";
			$data = 'failed';
		}
		 
		$this->AddToAuditTrail();
		
		return $data;
	}
	/*Change Partnumber : Update Partnumber - Supplier*/
	function UpdatePartNumberS($DBName,$partnumber,$entrynumber,$newpartnumber){
		
		mssql_select_db($DBName); 
		
		$this->AuditTrail['action'] = "Updating Parts ".$DBName.": ".$partnumber;
		 
		$entry_statement = "UPDATE Entry SET Partnumber='" . $newpartnumber ."' WHERE EntryNumber in (" . $entrynumber . ")";	
		$insert_statement = "INSERT INTO MarketingPartNumbers (SupplierPartNumber,PartNumber) VALUES ('" . trim($newpartnumber) . "','" . trim($partnumber)."')";
		
		if(mssql_query($entry_statement)){
			$this->AuditTrail['remark'] = "Successfully update partnumber: ".count($data);		
			if(mssql_query($insert_statement)){
				$this->AuditTrail['remark'] = "Successfully insert partnumber: ".count($data);			
				$data = 'success';
			}else{
				$this->AuditTrail['remark'] = "Failed to insert partnumber";
				$data = 'duplicate';	
			}		
		}else{
			$this->AuditTrail['remark'] = "Failed to update partnumber";
			$data = 'failed';						
		}
		
		 
		$this->AddToAuditTrail();
		
		return $data;
	}
	/* Flight Detail : Upload the file */
	function FlightDetailUploadFile($DBName,$allowedext,$filename,$targetupload){
		
		mssql_select_db($DBName); 
		
		$this->AuditTrail['action'] = "Uploading file with customer: ".$DBName.": ".$filename;
				
		include_once('./modules/flightdetail/fileupload.class.php');		
		$uploader = new qqFileUploader($allowedext);
		$result = $uploader->handleUpload($targetupload,true);
		
		if($result){
			$this->AuditTrail['remark'] = "File uploaded successfully";
		}else{
			$this->AuditTrail['remark'] = "Failed to upload file";
		}
		
		 
		$this->AddToAuditTrail();
		
		
		return $result;
	}
	/* Flight Detail : Extract the file */
	function GetFlightDetailExcelData($DBName,$filename){		
		// error_reporting(E_ALL);
		// ini_set("display_errors", 1); 		
		mssql_select_db($DBName); 
		
		$this->AuditTrail['action'] = "Parsing uploaded file: ".$DBName.": ".$filename;
				
		$target_path = './modules/flightdetail/uploads/'.$filename;		
		
		$ext = pathinfo($target_path, PATHINFO_EXTENSION);		
		
		if($ext == "xls"){
			include_once('./modules/flightdetail/simplexls.class.php');
			$xls = new Spreadsheet_Excel_Reader();
			$xls->setOutputEncoding('CP1251');
			$xls->read($target_path);
			
			for ($i = 0; $i < $xls->sheets[0]['numRows']; $i++) {
				for ($j = 0; $j < $xls->sheets[0]['numCols']; $j++){
					$data[$i][$j] = isset($xls->sheets[0]['cells'][$i+1][$j+1]) ? $xls->sheets[0]['cells'][$i+1][$j+1] : 'NULL';
				}
			}	
		}else if($ext == "xlsx"){		
			include_once('./modules/flightdetail/simplexlsx.class.php');
			$xlsx = new SimpleXLSX($target_path);
			$rawData = $xlsx->rowsEx();
			list($cols,) = $xlsx->dimension();    
			$totalDataCount = count($rawData);
			foreach($rawData as $k => $r) {
				for( $i = 0; $i < $cols; $i++)
					$data[$k][$i] = (isset($r[$i])) ? $r[$i]['value'] : 'NULL';
			}		
		}
		
		unlink($target_path);  
					
		if(count($data)>0){
			$this->AuditTrail['remark'] = "Data found.";
		}else{
			$this->AuditTrail['remark'] = "Cannot parse excel file or file is empty.";
			$data = "no data";
		}		
		 
		$this->AddToAuditTrail();		
		
		return $data;
	}
	/* Flight Detail : Update individual record thru import */
	function UpdateFlightDetail($DBName,$postdata){		
		mssql_select_db($DBName); 
 		include_once('./modules/flightdetail/flightfunctions.php');				
		 $funcname = strtolower($DBName);	
		 $flightdetail = new FlightDetail;			
		
		if (is_callable(array($flightdetail,$funcname))){
			echo $flightdetail->$funcname($postdata);		
		} else {
			echo "error";
		}
	}
	/* Flight Detail : Update individual record */
	function UpdateFlightData($DBName,$postdata){
		mssql_select_db($DBName); 		
		include_once('./modules/flightdetail/flightfunctions.php');		
		$vars = $postdata;
		$FlightDetail = new FlightDetail;
		if ($vars['todo'] == 1){
			$this->AuditTrail['action'] = "Filtering Packlist ".$DBName.": ".$vars['packlistnumber'];
			$data = $FlightDetail->checkpacklist($vars);
		} else if($vars['todo'] == 2){
			$this->AuditTrail['action'] = "Updating Packlist ".$DBName.": ".$vars['packlistnumber'];
			$data = $FlightDetail->updateflightdata($DBName,$vars);
		}	
		 
		$this->AddToAuditTrail();

		return $data;

		
	}
	/* Flight Detail : Get shipping method */
	function GetShippingMethod($DBName){
		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "Filtering Shipping Methods ".$DBName;
		
		$sql = mssql_query("SELECT Forwarder,Description FROM Forwarders ORDER BY Description");

		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "No matching record found.";
			$data = 'no data';
		}
		 
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	/* Flight Detail : Get shipping method */
	function GetUpdatedFlightData($DBName,$packlistnumber){

		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "Filtering Packlist Number ".$DBName;
		
		$ordernumberfetch = mssql_fetch_assoc(mssql_query("select documentnumber from packlist where packlistnumber = $packlistnumber "));
		$sqlcheckpacklist = mssql_query("select packlistnumber,mawb,hawb,flight,etd,eta,destination,permitno,netweight,shippingmethod from packlist where documentnumber = ".$ordernumberfetch['documentnumber']);
		
		if(mssql_num_rows($sqlcheckpacklist) > 0){
			while($data[] = mssql_fetch_assoc($sqlcheckpacklist));
			 array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "No matching record found.";
			$data = 'no data';
		}
		 
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	/* Consol Scanout : Get the latest consol number */
	function GetLatestConsolNumber($DBName){

		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "Filtering Consol Number ".$DBName;
		
		$sqlgetconsolnumber = mssql_query("select * from setup where [Entry] = 'ConsolidateNumber'");
		
		if(mssql_num_rows($sqlgetconsolnumber) > 0){
			$sqlgetconsolnumber  = mssql_fetch_assoc($sqlgetconsolnumber);
			$data = $sqlgetconsolnumber['Value'];
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "No matching record found.";
			$data = 'no data';
		}
		 
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	/* Consol Scanout : Generate consol number */
	function GenerateConsolNumber($DBName){

		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "Generate Consol Number ".$DBName;
		
		$sqlgenerateconsolnumber = mssql_query("update setup set [Value] = [Value] + 1 where [Entry] = 'ConsolidateNumber'");
		
		if($sqlgenerateconsolnumber){
			$data = $this->GetLatestConsolNumber($DBName);
			$this->AuditTrail['remark'] = "New consol number has been generated: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "Cannot generate consol number";
			$data = 'no data';
		}
		 
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	/* Consol Scanout : Get dimension type */
	function GetDimensionType($DBName){

		mssql_select_db($DBName);
		
		$this->AuditTrail['action'] = "Filtering Dimension Type ".$DBName;
		
		$sqlgetdimension = mssql_query("select DimensionType from OnlineDimensionTable");
		
		if(mssql_num_rows($sqlgetdimension) > 0){
			while($data[] = mssql_fetch_assoc($sqlgetdimension)){}
				array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "No matching record found.";
			$data = 'no data';
		}
		 
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	/* Consol Scanout : Save data */
	function ConsolScanout($DBName,$postdata){
		mssql_select_db($DBName); 		
		
		$this->AuditTrail['action'] = "Updating consol data ".$DBName;
		
		include_once('./modules/scanout/consolscanoutfunctions.php');				
		$funcname = strtolower($DBName);	
		$consolscanoutobj = new ConsolScanoutClass;			
		
		if (is_callable(array($consolscanoutobj,$funcname))){
			$result =  $consolscanoutobj->$funcname($postdata);		
			$this->AuditTrail['remark'] = "Consol process returns : [".$result."] See consolscanoutfunctions.php for the description";
		} else {
			$result = "unreachable";
			$this->AuditTrail['remark'] = "Consol process returns : ".$result." function";
		} 
				 
		$this->AddToAuditTrail();

		return $result;
		
	}
	
	/* Undo Consol Scanout : revert consol data */
	function UndoConsolScanout($DBName,$postdata){
		mssql_select_db($DBName); 		
		
		$consol = trim($postdata['consolnum']);
		
		$this->AuditTrail['action'] = "Reverting consol data ".$DBName;
		
		if(!isset($consol)) return "6";
		
		$res = mssql_query("SELECT ConsolNumber FROM shipboxes WHERE ConsolNumber = " . $consol); 
		
		if(mssql_num_rows($res)>0){	  
			if(mssql_query("DELETE FROM SHIPBOXES WHERE ConsolNumber = " . $consol)){
				if(mssql_query("DELETE FROM BOXWEIGHT WHERE ConsolNumber = " . $consol)){
					if(mssql_query("UPDATE Order SET ConsolNumber = 0 WHERE ConsolNumber = " . $consol)){
						if(mssql_query("UPDATE Packlist SET ConsolNumber = 0 WHERE ConsolNumber = " . $consol)){
							$result = "0";						
							$this->AuditTrail['remark'] = "Consol process returns : [0] - Successful database query[ALL] ";
						}else{
							$result = "5";
							$this->AuditTrail['remark'] = "Consol process returns : [5] - Cannot update consol Packlist ";
						}
					}else{
						$result = "4";					
						$this->AuditTrail['remark'] = "Consol process returns : [4] - Cannot update consol in Order ";
					}
				}else{
					$result = "3";				
					$this->AuditTrail['remark'] = "Consol process returns : [3] - Cannot delete consol from BOXWEIGHT";
				}
			}else{
				$result = "2";			
				$this->AuditTrail['remark'] = "Consol process returns : [2] - Cannot delete consol from SHIPBOXES";
			}
		}else{
			$result = "1";
			$this->AuditTrail['remark'] = "Consol process returns : [1] - Consol number in SHIPBOXES doesn't exist ";
		}
		
		$this->AddToAuditTrail();

		return $result;
		
		// Message Description
		// 0 - Successful database query
		// 1 - Consol number in SHIPBOXES doesn't exist
		// 2 - Cannot delete consol from SHIPBOXES
		// 3 - Cannot delete consol from BOXWEIGHT
		// 4 - Cannot update consol in Order
		// 5 - Cannot update consol Packlist
		// 6 - Consol number is empty

	}
	
	/* Undo Book : Get picklist data */
	function GetPicklistData($DBName,$picklist){
		mssql_select_db($DBName);
		
		$picklist = trim($picklist);
		
		$this->AuditTrail['action'] = "Filtering Picklist data: ".$DBName." Picklist Number: ".$picklist;
		
		$ship_res = mssql_query("SELECT Ordernumber FROM Shipboxes WHERE Packlistnumber=" . $picklist);
		if(mssql_num_rows($ship_res)>=1){
			$scanned = "Yes";
		}else{
			$scanned = "No";		
		}
		
		// Get details from Out table
		$res = mssql_query("SELECT (SELECT SUM(Quantity) FROM Out WHERE PackListNumber=o.PackListNumber ) AS 'Total', (SELECT COUNT(PackListNumber) FROM Out WHERE PackListNumber=o.PackListNumber ) AS 'Position', o.PackListNumber,o.OwnOrderNumber,o.Outnumber,o.Customer FROM Out as o WHERE o.PackListNumber=" . $picklist." ");
		
		if(mssql_num_rows($res) > 0){
			$data[0] = mssql_fetch_assoc($res);
			array_push($data,$data[0]['Scanned'] = $scanned);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "No matching record found.";
			$data = 'no data';
		}
		 
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	/* Undo Book : Get paclist position data */
	function GetPackListPosition($DBName,$picklist){
		mssql_select_db($DBName);
		
		$picklist = trim($picklist);
		
		$this->AuditTrail['action'] = "Filtering Packlist position: ".$DBName." Packlist Number: ".$picklist;
		
		$res = mssql_query("SELECT EntryNumber, Quantity, StockIndex FROM [PackListPosition] WHERE PackListNumber=" . $picklist);
		
		if(mssql_num_rows($res) > 0){
			while($data[] = mssql_fetch_assoc($res)){}
				array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "No matching record found.";
			$data = 'no data';
		}
		 
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	/* Undo Book : start undo booking*/
	function UndoBooking($DBName,$postdata){		
		mssql_select_db($DBName);
		
		extract($postdata);
		$picklist = trim($picklist);
		$ordernumber = trim($ordernumber);
		$outnumber = trim($outnumber);
		
		$this->AuditTrail['action'] = "Start undo booking: ".$DBName." Packlist Number: ".$picklist;
		
		//return "SELECT EntryNumber, Quantity, StockIndex FROM [PackListPosition] WHERE PackListNumber=" . $picklist;
		
		$entry_result = mssql_query("SELECT EntryNumber, Quantity, StockIndex FROM [PackListPosition] WHERE PackListNumber=" . $picklist);
		
		
		
		if(mssql_num_rows($entry_result) > 0){
			while($entry_row =mssql_fetch_assoc($entry_result)){
				//place in one string
				$strentry .= $entry_row["EntryNumber"] . ",";
				$strqty .= $entry_row["Quantity"] . ",";
			}
			
			//Place to array
			$arr_entry = explode(",",$strentry);
			$arr_qty = explode(",",$strqty);
			array_pop($arr_entry);
			array_pop($arr_qty);
			
			$rec_ctr = count($arr_entry);
			$ctr=0;
			for($i=0;$i<$rec_ctr;$i++)
			{
				mssql_query("UPDATE [Entry] SET [QuantityOut]=[QuantityOut]-$arr_qty[$i] WHERE [Entrynumber]=$arr_entry[$i]")or die( "Error: " . mssql_get_last_message());
				$ctr ++;
			}
			mssql_query("BEGIN TRAN");
			
			$sql  = "UPDATE [OrderPosition] SET PackListnumber=0 WHERE Ordernumber=$ordernumber";
			$sql .= "DELETE FROM [EntryOut] WHERE OutNumber=$outnumber";
			$sql .= "DELETE FROM [Out] WHERE PackListnumber=$picklist";
			$sql .= "DELETE FROM [PackListPosition] WHERE PackListnumber=$picklist";
			$sql .= "DELETE FROM [PackList] WHERE PackListnumber=$picklist";
			$sql .= "DELETE FROM [Shipboxes] WHERE PackListnumber=$picklist";
			$sql .= "DELETE FROM [BoxWeight] WHERE PackListnumber=$picklist";
			$sql .= "DELETE FROM [EntriesGivenForOut] WHERE Ordernumber=$ordernumber";
			
			$result = mssql_query($sql);
			
			if ($result){
				mssql_query("COMMIT");
			}else{
				mssql_query("ROLLBACK");
			}
			
			$data = "succeed"; 
			
		}else{
			$data = "1";
		}
		

		
		 
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	/* Cycle Count : partnumber by entry number */
	function GetPNByEntryNumber($DBName,$entrynumber){
		mssql_select_db($DBName);
		
		$entrynumber = trim($entrynumber);
		
		$this->AuditTrail['action'] = "Filtering Entry : ".$DBName." Entry Number: ".$entrynumber;
		
		$res = mssql_query("SELECT PartNumber FROM Entry WHERE EntryNumber=" . $entrynumber);
		
		if(mssql_num_rows($res) > 0){
			while($data[] = mssql_fetch_assoc($res)){}
				array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "No matching record found.";
			$data = 'no data';
		}
		 
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	/* Cycle Count : update entry */
	function UpdateCycleCount($DBName,$postdata,$user){
		mssql_select_db($DBName);
		
		$scanlocation = trim($postdata["location"]);
		$entry = trim($postdata["entrynumber"]);
		$scanPN = trim($postdata["partnumber"]);
		$scanqty = trim($postdata["quantity"]);
		$scandate = date('Y-m-d');
		$scantime = date('H:i');
		$scanuser = trim($user);
		
		$sql = "UPDATE Entry SET ScanLocation='$scanlocation',ScanPartNumber='$scanPN',ScanQuantity=$scanqty,ScanTime='$scantime',ScanDate='$scandate',Scanuser='$scanuser' WHERE EntryNumber=$entry";
		
		$res = mssql_query($sql) or die(mssql_get_last_message());	
		
		$this->AuditTrail['action'] = "Updating Entry : ".$DBName." with Entry Number: ".$entry;
		
		if($res && mssql_rows_affected($this->link)){
			$this->AuditTrail['remark'] = "Update entry success. Entry number: ".$entry;
			$data = 'success';
		}else{
			$this->AuditTrail['remark'] = "Cannot update entry record";
			$data = 'failed';
		}
		 
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	function LogMein($username,$password){
		mssql_select_db('MasterData');
		
		$this->AuditTrail['action'] = "LogMeIn: $username,$password";
		
		$cipher = new cipher();
		$EncryptedPW = $cipher->encrypt($password);
		
		$sql = mssql_query("execute LogMeIn2 $username, '$EncryptedPW'");
		
		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			
			if (isset($data[0]['Result']) && $data[0]['Result']=='Failed'){
				$this->AuditTrail['remark'] = "Failed";
				$result = 'failed';
				session_destroy();
			}else{
				$_SESSION[LoginSession] = LoginSessionValue;
				$_SESSION[LoginUserVar] = $username;
				
				$_SESSION['WawiList'] = $data;
				$this->AuditTrail['remark'] = "Success";
				$result = 'success';

			}
			
		} else {	
			$this->AuditTrail['remark'] = "Failed";
			$result = 'failed';
			session_destroy();
		}
		
		$this->AddToAuditTrail();
		
		return $result;
		
	}
	
	function GetModules($username,$WawiID){
		mssql_select_db("MasterData");
		
		$this->AuditTrail['action'] = "GetModules $username,$WawiID";
		$sql = mssql_query("select UserAccess.WawiID,WawiCustomer.WawiIndex,MenuID,MenuAccess.MenuName,MenuAccess.MenuURL 
from UserAccess,MenuAccess,WawiCustomer 
where UserAccess.UserID='$username' and UserAccess.WawiID=$WawiID 
and UserAccess.[Status]=1 and MenuAccess.MenuStatus=1 and WawiCustomer.[status]=1
and UserAccess.MenuAccessID=MenuAccess.MenuID
and UserAccess.WawiID=WawiCustomer.ID;");

		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		} else {	
			$this->AuditTrail['remark'] = "No Data";
			$data = 'no data';
		}
		
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	
	
	//select MenuID,MenuName,MenuURL from UserAccess,MenuAccess where UserID='andrew' and WawiID=30 and [Status]=1 and MenuStatus=1 and MenuAccessID=MenuID
	
	/* Support */
	function ContactSupport($DBName,$postdata,$files,$user){
		//error_reporting(E_ALL);
		//ini_set("display_errors", 1); 		
		
		
		require_once 'Mailer/class.phpmailer.php';
		$error = 0;
		$user = $this->getUserInfo($user);
		$mail = new PHPMailer(true); 		 		
		$mail->IsSMTP(); 	

		$this->AuditTrail['action'] = "Sending Message. To:".MAIL_SUPPORT.", from: ".$user['EMail'];
		
		try {
			$mail->Host = SMTP_HOST;
			$mail->AddAddress(MAIL_SUPPORT,MAIL_SUPPORT_NAME);
			$mail->SetFrom($user['EMail'],$user['Name']);
			$mail->Subject = trim($postdata['category'] ." : ". $postdata['subject']);
			$mail->MsgHTML(trim($postdata['message']));
			$filepaths = array();
			if(count($files['attachment']['name']) > 0){	
				$attachmentcount = count($files['attachment']['name']);
				for($i = 0; $i < $attachmentcount ; $i++){
					if($files['attachment']['name'][$i] != ''){
						$path_of_uploaded_file = "./tmp/" . basename($files['attachment']['name'][$i]);
						$tmp_path = $files['attachment']['tmp_name'][$i];			
						if(is_uploaded_file($tmp_path))
						{
							$input = fopen($tmp_path, "r");
							$temp = tmpfile();
							$realSize = stream_copy_to_stream($input, $temp);
							fclose($input);	
							
							$target = fopen($path_of_uploaded_file, "w");        
							fseek($temp, 0, SEEK_SET);
							stream_copy_to_stream($temp, $target);
							fclose($target);
							
							$mail->AddAttachment($path_of_uploaded_file,basename($files['attachment']['name'][$i]));			 
						}else{
							$error = 'Attachment Error';
							//throw new Exception($error);
						}
						$filepaths[] = $path_of_uploaded_file;
					}
				}
			}
			$mail->Send();
			
			$this->AuditTrail['remark'] = "Message Sent. To:".MAIL_SUPPORT.", from: ".$user['EMail'];
			
			foreach($filepaths as $file){
				unlink($file);
			}
			
			$data = "success";
			
		} catch (phpmailerException $e) {
			$this->AuditTrail['remark'] = "Message Sending Failed. To:".MAIL_SUPPORT.", From: ".$user['EMail'];
			$data = $e->errorMessage(); 
		} catch (Exception $e) {
			$this->AuditTrail['remark'] = "Message Sending Failed. To:".MAIL_SUPPORT.", From: ".$user['EMail'];
		    $data = $e->getMessage(); 
		}
			 
		return $data;
		
	}
	
	/* GET User Info */
	
	function getUserInfo($user){
		mssql_select_db(DefaultDB);
		
		$this->AuditTrail['action'] = "Filtering User : ".DefaultDB." Username : ".$user;
		
		$res = mssql_query("SELECT Username,Name,EMail FROM [".DefaultDB."].[dbo].[User] WHERE Username = '" . trim($user)."'");
		
		if(mssql_num_rows($res) > 0){
			$data = mssql_fetch_assoc($res);
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "No matching record found.";
			$data = 'no data';
		}
		 
		$this->AddToAuditTrail();
		
		return $data;
	}
	
	
	/* I Forgot */
	
	function iForgot($email){
		mssql_select_db(DefaultDB);
		
		$this->AuditTrail['action'] = "Filtering User : ".DefaultDB." Email : ".$email;
		
		$res = mssql_query("SELECT Username,Name,EMail,Password FROM [".DefaultDB."].[dbo].[User] WHERE email = '" . trim($email)."'");
		
		if(mssql_num_rows($res) > 0){
			$data = mssql_fetch_assoc($res);
			
			$password = $data['Password'];
			
			$cipher = new cipher('andrew');
			$password = $cipher->decrypt($password);
			
			require_once 'Mailer/class.phpmailer.php';
			$error = 0;
			$mail = new PHPMailer(true); 		 		
			$mail->IsSMTP(); 	
			$this->AuditTrail['action'] = "Sending Message. To:".$data['EMail'].", from: ".MAIL_SUPPORT;
			try {
				$mail->Host = SMTP_HOST;
				$mail->AddAddress($data['EMail'],$data['Name']);
				$mail->SetFrom(MAIL_SUPPORT,MAIL_SUPPORT_NAME);
				$mail->Subject = 'iForgot : Login Detail';
				$mail->MsgHTML('
							<p>Hi '.$data['Name'].',</p>
							<p>Here\'s your login detail.</p>
							<table>
							<tr><td>Username:</td><td>'.$data['Username'].'</td></tr>
							<tr><td>Password:</td><td>'.$password.'</td></tr>
							</table>							
							<p>You can login <a href="'.SSL.SERVER_HOST.'/'.FOLDER.'/index.php" >here</a>.</p>
							<p>Please keep it as confidential.</p>
							<br/>
							<p>Thank You.</p>
							<p>Support Team</p>							
						');				
				$mail->Send();
				
				$this->AuditTrail['remark'] = "Message Sent. To:".$data['EMail'].", from: ".MAIL_SUPPORT;
				
				$data = "success";
				
			} catch (phpmailerException $e) {
				$this->AuditTrail['remark'] = "Message Sending Failed. To:".$data['EMail'].", from: ".MAIL_SUPPORT;
				$data = $e->errorMessage(); 
			} catch (Exception $e) {
				$this->AuditTrail['remark'] = "Message Sending Failed. To:".$data['EMail'].", from: ".MAIL_SUPPORT;
				$data = $e->getMessage(); 
			}		
			
			$this->AuditTrail['remark'] = "Successful filter row count: ".count($data);
		}else{
			$this->AuditTrail['remark'] = "No matching record found.";
			$data = 'no data';
		}
		 
		$this->AddToAuditTrail(); 
		
		return $data;
	}
	
	
	
}

Class iWMSExt1{
	public function emailme($to,$subject,$message){
		$MAIL_HEADERS = "\n--nextPart\n";
		$MAIL_HEADERS .= "Content-type: text/html; charset=iso-8859-1\r\n";
		$MAIL_HEADERS .= "From: JSI Notification <".MAIL_FROM.">"; //don't forget to add at config.php
		if (mail($to, $subject, $message, $MAIL_HEADERS)){
			echo "success";
		} else {
			echo "failed";
		}
	}
	function BrowserVersion($pos,$BROWSER){
		for ($i=$pos,$v = ''; is_numeric(substr($BROWSER,($i),1)) || substr($BROWSER,($i),1)=='.'; $i++){
			$v .= substr($BROWSER,($i),1);
		}
		return $v;
	}
	
	function DetectBrowser(){
		$b = array('MSIE','Firefox','Chrome','Safari','Opera', 'iPhone');
		$BROWSER = $_SERVER['HTTP_USER_AGENT'];
		$msie = strpos($BROWSER,$b[0]);
		$ffox = strpos($BROWSER,$b[1]);
		$Chrome = strpos($BROWSER,$b[2]);
		$Safari = strpos($BROWSER,$b[3]);
		$Opera = strpos($BROWSER,$b[4]);
		$iPhone = strpos($BROWSER,$b[5]);
		
		if ($msie!==false) {
			$browser_name=substr($BROWSER,$msie,strlen($b[0]));
			$browser_version = $this->BrowserVersion($msie+5,$BROWSER);
		} elseif ($ffox!==false) {
			$browser_name=substr($BROWSER,$ffox,strlen($b[1]));
			$browser_version = $this->BrowserVersion($ffox+8,$BROWSER);
		} elseif ($Chrome!==false) {
			$browser_name=substr($BROWSER,$Chrome,strlen($b[2]));	
			$browser_version = $this->BrowserVersion($Chrome+7,$BROWSER);
		} elseif ($Safari!==false) {
			$browser_name=substr($BROWSER,$Safari,strlen($b[3]));
			$browser_version = $this->BrowserVersion(strpos($BROWSER,'Version')+8,$BROWSER);
		} elseif ($Opera!==false) {		
			$browser_name=substr($BROWSER,$Opera,strlen($b[4]));
			$browser_version = $this->BrowserVersion(strpos($BROWSER,'Version')+8,$BROWSER);
		} elseif ($iPhone!==false) {		
			$browser_name=substr($BROWSER,$iPhone,strlen($b[5]));
			$browser_version = $this->BrowserVersion(strpos($BROWSER,'OS')+3,$BROWSER);
		} else {
			$browser_name='';
			$browser_version=$BROWSER;
		}
		
		if ($browser_name){
			return $browser_name.' - '.$browser_version;
		}else{
			return $BROWSER;	
		}	
	}	
}

/*ENCRYPTION
$cipher = new cipher('andrew');
$data = $cipher->encrypt('testing only');	
*/
class cipher{
    private $securekey, $iv_size, $iv;
		
    function __construct() {
        $this->securekey = hash('sha256',SECRET_KEY,TRUE);
				$this->iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC);
				$this->iv = mcrypt_create_iv($this->iv_size,MCRYPT_RAND);
    }
    function encrypt($input) {
				$input = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $this->securekey, $input, MCRYPT_MODE_ECB, $this->iv));
        return $input;
    }
    function decrypt($input) {
				$input =  trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $this->securekey, base64_decode($input), MCRYPT_MODE_ECB, $this->iv));
        return $input;
    }
}

?>
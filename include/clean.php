<?php
if (isset($_REQUEST)){
	foreach ( $_REQUEST as $key => $value ){
		if ( get_magic_quotes_gpc() ){
			${$key} = htmlspecialchars(stripslashes($value),ENT_QUOTES) ;
		}else{
			${$key} = htmlspecialchars($value,ENT_QUOTES) ;
		}
	}
}

if (isset($_GET)){
	foreach ( $_GET as $key => $value ){
		if ( get_magic_quotes_gpc() ){
			${$key} = htmlspecialchars(stripslashes($value),ENT_QUOTES) ;
		}else{
			${$key} = htmlspecialchars($value,ENT_QUOTES) ;
		}
	}
}

?>
<?php
/*--------------------------------------------------+
|	Contains general configurations and settings	|
+--------------------------------------------------*/
//CONFIG	
ini_set('memory_limit', '1024M');
define('SERVER_HOST',$_SERVER['HTTP_HOST'],true);
//define('SERVER_HOST',"10.128.1.15",true);
define('SECRET_KEY','DevTeam@jsi2012ajae^',true);
define('LoginSession',md5("JSI-iWMS"),true);
define('LoginSessionValue',md5("JSI-iWMS-AJA"),true);
define('LoginUserVar','iWMS-User',true);
define('ReportLimit',2000);

define('AuditTrailDB',1);
define('AuditTrailTxt',1);

define('DefaultDB','MasterData',true);

//SERVER
$serverConn = array(
"S1" => array("10.128.50.13\SINT5021","sa","itgd@2017"), 
"S2" => array("10.128.50.13\SINT5021","sa","itgd@2017"), 
"S3" => array("10.128.50.13\SINT5021","sa","itgd@2017"), 
"S4" => array("10.128.50.13\SINT5021","sa","itgd@2017"), 
"S5" => array("10.128.50.13\SINT5021","sa","itgd@2017"));

define('SQL_CONN', base64_encode(serialize($serverConn['S3'])),true);
define('SQL_CONN4', base64_encode(serialize($serverConn['S4'])),true);
define('SQL_CONN5', base64_encode(serialize($serverConn['S5'])),true);

//EMAIL
define('MAIL_DEV','development@jsilogistics.com',true);
define('MAIL_DEV_NAME','JSI Development Team',true);
define('MAIL_SUPPORT','sg-alert@jsilogistics.com',true);
define('MAIL_SUPPORT_NAME','JSI SUPPORT',true);
define('SMTP_HOST','smtp.googlemail.com',true);
define('MAIL_ALERT','sg-alert@jsilogistics.com',true);


//PASSWORD VALIDITY
define('PASSWORD_VALIDITY',90); //no. of days 
define('PASSWORD_VALIDITY_TRESHOLD',10); //no. of days


//URL
if (SERVER_HOST=='wawi.jsis.com.sg'){
	define('SSL','https://',true);
}else{
	define('SSL','http://',true);
}

//define('SSL','https://',true);


$path = parse_url(SSL . SERVER_HOST . $_SERVER['REQUEST_URI']);
$path_folders = explode("/", $path['path']);
array_shift($path_folders);

define('FOLDER',$path_folders[0],true);
define('WEB_URL',SSL . SERVER_HOST . '/' . FOLDER . '/');
define('ROOT',dirname(dirname(__FILE__)),true);

//report
define('EMAIL_REPORT_LINK', WEB_URL.'modules/EmailReportRequest/reports/');
define('EMAIL_REPORT_SUBJECT', 'JSI WMS Reports');


//HTML
$VERSION = 'v1.0';
$TITLE = 'JSI WAWI Online '.$VERSION;
$META_DESCRIPTION = 'JSI WAWI Online Copyright JSI Logistics (S) Pte. Ltd.';
$META_KEYWORDS = 'JSI WAWI Online';
$META_AUTHOR = 'JSI Logistics (S) Pte. Ltd.';
$META_DEVELOPERS = 'JSI Singapore Development Team';

//CSS
$CSS = array();
$CSS[] = 'css/style.css';
$CSS[] = 'css/ddsmoothmenu.css';
$CSS[] = 'jQueryUI/themes/iwms/jquery-ui-1.8.16.custom.css';
$CSS[] = 'jQueryUI/DataTables/table_jui.css';
$CSS[] = 'jQueryUI/DataTables/extras/TableTools/media/css/TableTools_JUI.css';


//HTTP Error Codes
$HtmlErrorCodes = array(
"400"=>"Request is badly formed.",
"401.1"=>"Unauthorized: Access is denied due to invalid credentials.",
"401.2"=>"Unauthorized: Access is denied due to server configuration.",
"401.3"=>"Unauthorized: Access is denied due to an ACL set on the requested resource.",
"401.4"=>"Unauthorized: Authorization failed by filter installed on the Web server.",
"401.5"=>"Unauthorized: Authorization failed by an ISAPI/CGI application.",
"401.6"=>"Unauthorized: URL authorization scope was not found on the server.",
"401.7"=>"Unauthorized: Access denied by URL authorization policy on the Web server.",
"401"=>"Unauthorized: Access is denied due to invalid credentials.",
"403.1"=>"Forbidden: Execute access is denied.",
"403.2"=>"Forbidden: Read access is denied.",
"403.3"=>"Forbidden: Write access is denied.",
"403.4"=>"Forbidden: SSL is required to view this resource.",
"403.5"=>"Forbidden: SSL 128 is required to view this resource.",
"403.6"=>"Forbidden: IP address of the client has been rejected.",
"403.7"=>"Forbidden: SSL client certificate is required.",
"403.8"=>"Forbidden: DNS name of the client is rejected.",
"403.9"=>"Forbidden: Too many clients are trying to connect to the Web server.",
"403.10"=>"Forbidden: Web server is configured to deny Execute access.",
"403.11"=>"Forbidden: Password has been changed.",
"403.12"=>"Forbidden: Client certificate is denied access by the server certificate mapper.",
"403.13"=>"Forbidden: Client certificate has been revoked on the Web server.",
"403.14"=>"Forbidden: Directory listing denied.",
"403.15"=>"Forbidden: Client access licenses have exceeded limits on the Web server.",
"403.16"=>"Forbidden: Client certificate is ill-formed or is not trusted by the Web server.",
"403.17"=>"Forbidden: Client certificate has expired or is not yet valid.",
"403.18"=>"Forbidden: Cannot execute requested URL in the current application pool.",
"403.19"=>"Forbidden: Cannot execute CGIs for the client in this application pool.",
"403.20"=>"Forbidden: Passport logon failed.",
"403"=>"Forbidden: Access is denied.",
"404.1"=>"File or directory not found: Web site not accessible on the requested port.",
"404.2"=>"File or directory not found: Lockdown policy prevents this request.",
"404.3"=>"File or directory not found: MIME map policy prevents this request.",
"404"=>"File or directory not found.",
"405"=>"The HTTP verb used to access this page is not allowed.",
"406"=>"Client browser does not accept the MIME type of the requested page.",
"407"=>"Initial proxy authentication required by the Web server.",
"410"=>"File has been removed.",
"412"=>"Precondition failed.",
"414"=>"The Request-URI is too large.",
"415"=>"Unsupported Media Type.",
"500.11"=>"Server error: Application is shutting down on the Web server.",
"500.12"=>"Server error: Application is busy restarting on the Web server.",
"500.13"=>"Server error: Web server is too busy.",
"500.14"=>"Server error: Invalid application configuration on the server.",
"500.15"=>"Server error: Direct requests for GLOBAL. ASA are not allowed.",
"500.16"=>"Server error: UNC authorization credentials incorrect.",
"500.17"=>"Server error: URL authorization store cannot be found.",
"500.18"=>"Server error: URL authorization store cannot be opened.",
"500.19"=>"Server error: Data for this file is configured improperly in the metabase.",
"500"=>"Internal server error.",
"501"=>"Method not implemented on the Web server.",
"502"=>"Web server received an invalid response while acting as a gateway or proxy server."
);
?>
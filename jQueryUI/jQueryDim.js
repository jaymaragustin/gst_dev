//dimming background
jQuery.extend({
    //dims the screen
    dimScreen: function(speed, opacity, callback) {
        if(jQuery('#__dimScreen').size() > 0) return;
        
        if(typeof speed == 'function') {
            callback = speed;
            speed = null;
        }

        if(typeof opacity == 'function') {
            callback = opacity;
            opacity = null;
        }

        if(speed < 1) {
            var placeholder = opacity;
            opacity = speed;
            speed = placeholder;
        }
        
        if(opacity >= 1) {
            var placeholder = speed;
            speed = opacity;
            opacity = placeholder;
        }

        speed = (speed > 0) ? speed : 500;
        opacity = (opacity > 0) ? opacity : 0.5;
		
        return jQuery('<div style="color:#FFF; font-size:12px;"><div style=" background:green;border-radius: 0 0 10px 10px;color: #FFFFFF;margin: 0 10px;padding: 5px 12px;position: fixed;text-align: center;width: 112px;z-index: 9999;">P l e a s e&nbsp;&nbsp;w a i t . . .</div></div>').attr({
                id: '__dimScreen'
                ,fade_opacity: opacity
                ,speed: speed
            }).css({
            background: '#000'
            ,height: $(window).height() 
            ,left: '0px'
            ,opacity: 0
            ,position: 'fixed'
            ,top: '0px'
            ,width: $(window).width()
            ,zIndex: 999
        }).appendTo(document.body).fadeTo(speed, opacity, callback);
    },
    
    //stops current dimming of the screen
    dimScreenStop: function(callback) {
        var x = jQuery('#__dimScreen');
        var opacity = x.attr('fade_opacity');
        var speed = x.attr('speed');
        x.fadeOut(speed, function() {
            x.remove();
            if(typeof callback == 'function') callback();
        });
    }
	
});

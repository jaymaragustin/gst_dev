<?php 
if (function_exists('ob_gzhandler') && !ini_get('zlib.output_compression')){
	ob_start('ob_gzhandler');
}else{
	ob_start();
}
session_start();
require('include/config.php');
require('include/clean.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>JSI GST Online</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="images/iwms.png">

<?php
for ($i=0; $i<count($CSS);$i++){ echo '<link rel="stylesheet" href="'.$CSS[$i].'">';}
?>
<script src="jQueryUI/jquery-1.5.1.js"></script>

<script src="js/cufon-yui.js"></script>
<script src="js/arial.js"></script>
<script src="js/cuf_run.js"></script>
<script src="jQueryUI/ui/jquery-ui-1.8.16.custom.js"></script>
</head>
<body>
<div class="main">
  <div class="header">
    <div class="header_resize">
		<?php include('tpl/logo.php');?>
        <div class="menu">
            <div id="templatemo_menu" class="ddsmoothmenu">
                <ul>
                    <li class="firstmenu"><a id="menu_item_1" href="<?php echo WEB_URL;?>" target="_new">Home</a></li>
                </ul>
                <br style="clear: left" />
            </div> <!-- end of templatemo_menu -->
        </div>
        <div class="clr"></div>
    </div>
    <div class="headert_text_resize">
        <div class="headert_text"></div>
        <div class="clr"></div>
    </div>
</div>
  <div class="body">
    <div class="body_resize">
          <div class="whole">
          	<div class="content">
            	<h2><a href="#"><?php echo (isset($err)) ?  $err.'&nbsp;': '';?></a>warning</h2>
                <p>We are sorry but your request is invalid.</p>
                <p>You can return to the homepage by clicking <a href="<?php echo WEB_URL;?>" target="_new">here</a>.</p>
            </div>
          </div>
          <div class="clr"></div>
    </div>
</div>
  <?php
  include('tpl/fbg.php');
  include('tpl/footer.php');
  ?>
</div>
</body>
</html>
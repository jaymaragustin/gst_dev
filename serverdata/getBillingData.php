<?php
    /* Indexed column (used for fast and accurate table cardinality) */
    $sIndexColumn = "Partnumber";
     
    /* DB table to use */
    $sTable = "Parts";
 
    /* Database connection information */
	include_once('../include/config.php');
	$serverConn = unserialize(base64_decode(SQL_CONN));
    $gaSql['server']     = $serverConn[0];
    $gaSql['user']       = $serverConn[1];
    $gaSql['password']   = $serverConn[2];
    $gaSql['database']   = "";
 
    /* Columns */ 
	
    $aColumns = strlen($_POST['Fields']) > 0 ? explode(',',$_POST['Fields']) : array();

    /* DB connection */	 
    $gaSql['link'] =  mssql_connect( $gaSql['server'], $gaSql['user'], $gaSql['password']  ) or
        die( "Connection failed: " . mssql_get_last_message() );		
	
	$db = mssql_query("SELECT DBName FROM MasterData.dbo.WawiCustomer WHERE ID = $_POST[WawiID]");
	$gaSql['database'] = mssql_result($db,0,'DBName');
	
	/* Select Database */
	mssql_select_db($gaSql['database']);
	
    /* If column not given */
	if(count($aColumns) < 1){
		$cSql = "SELECT Column_Name as colname FROM Information_schema.Columns WITH (nolock) WHERE Table_Name LIKE '".$sTable."'";
		$cResult = mssql_query($cSql,$gaSql['link']) or die(mssql_get_last_message());
		while ( $r = mssql_fetch_array( $cResult ) ) {
			array_push( $aColumns, $r['colname'] );
		}
	}
         
    /* Paging */ 
	$sLimitFrom = isset($_POST['iDisplayStart']) ? $_POST['iDisplayStart'] : 0;
	$sLimitTo =(int)$_POST['iDisplayStart'] + (int)$_POST['iDisplayLength'];
 
     
     
    /* Ordering */ 
    $sOrder = "";
    if ( isset( $_POST['iSortCol_0'] ) )
    {
        $sOrder = "ORDER BY  ";
        for ( $i=0 ; $i<intval( $_POST['iSortingCols'] ) ; $i++ )
        {
            if ( $_POST[ 'bSortable_'.intval($_POST['iSortCol_'.$i]) ] == "true" )
            {
                $sOrder .= $aColumns[ intval( $_POST['iSortCol_'.$i] ) ]."
                    ".addslashes( $_POST['sSortDir_'.$i] ) .", ";
            }
        }
         
        $sOrder = substr_replace( $sOrder, "", -2 );
        if ( $sOrder == "ORDER BY" )
        {
            $sOrder = "";
        }
    }
     
     
    /* Filtering */
    $sWhere = "";
    if ( isset($_POST['sSearch']) && $_POST['sSearch'] != "" )
    {
        $sWhere = "WHERE (";
        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            $sWhere .= $aColumns[$i]." LIKE '%".addslashes( $_POST['sSearch'] )."%' OR ";
        }
        $sWhere = substr_replace( $sWhere, "", -3 );
        $sWhere .= ')';
    }
     
    /* Individual column filtering */     
    for ( $i=0 ; $i<count($aColumns) ; $i++ )
    {
        if ( isset($_POST['bSearchable_'.$i]) && $_POST['bSearchable_'.$i] == "true" && $_POST['sSearch_'.$i] != '' )
        {
            if ( $sWhere == "" )
            {
                $sWhere = "WHERE ";
            }
            else
            {
                $sWhere .= " AND ";
            }
            $sWhere .= $aColumns[$i]." LIKE '%".addslashes($_POST['sSearch_'.$i])."%' ";
        }
    }
     
    /* Data set length after filtering */
    $sQueryCnt = "SELECT count(".$sIndexColumn.") as counter FROM $sTable $sWhere";

    $rResultCnt = mssql_query( $sQueryCnt,$gaSql['link'] ) or die (" $sQueryCnt: " . mssql_get_last_message());
    $aResultCnt = mssql_result( $rResultCnt, 0 ,'counter' );
    $iFilteredTotal = $aResultCnt;
   
	/* Get data to display */  
	$sLimitTo = $sLimitTo < 0 ? $aResultCnt : $sLimitTo;
	$sFields = implode(',',$aColumns);
	$sQuery = "SELECT $sFields FROM ( SELECT *, ROW_NUMBER() OVER ($sOrder) as row FROM $sTable $sWhere ) a WHERE row > $sLimitFrom and row <= $sLimitTo";
	$rResult = mssql_query($sQuery,$gaSql['link']) or die("$sQuery: " . mssql_get_last_message());
     
     
    /* Total data set length */
    $sQuery = "SELECT COUNT(".$sIndexColumn.") as counter FROM   $sTable";
    $rResultTotal = mssql_query( $sQuery,$gaSql['link'] ) or die(mssql_get_last_message());
    $aResultTotal = mssql_result($rResultTotal,0,'counter');
    $iTotal = $aResultTotal;
     
     
    /* Output */ 
    $output = array(
        "sEcho" => intval($_POST['sEcho']),
        "iTotalRecords" => $iTotal,
        "iTotalDisplayRecords" => $iFilteredTotal,
        "aaData" => array()
    );
     
    while ( $aRow = mssql_fetch_array( $rResult ) )
    {
        $row = array();
        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
          
			if ( $aColumns[$i] == $sIndexColumn )
            {
                /* Special output formatting */
                $row[] = '<a class="stockslist" href="#" onclick="EditPart(\''.$aRow[ trim($aColumns[$i],'[]') ].'\');" >'.$aRow[ trim($aColumns[$i],'[]') ].'</a>';
            }
            else if ( $aColumns[$i] != ' ' )
            {
                /* General output */
                $row[] = $aRow[ $aColumns[$i] ];
            }
        }
        $output['aaData'][] = $row;
    }
         
    echo json_encode( $output );
?>
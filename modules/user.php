<?php 
session_start();
?>
<div id="changepassword">
	<h2 class="flogintitle">Change Password</h2>
	<form name="changepassword" action="" class="globalform" >
		<ol>
			<li>
				<label>Current Password*</label> 
				<input type="hidden" class="text" name="username" id="username" value="<?php echo $_SESSION['iWMS-User']?>" />
				<input type="password" class="text" name="curpassword" id="curpassword" />
			</li>
			<li>
				<label>New Password*</label> 
				<input type="password" class="text" name="newpassword" id="newpassword" />
			</li>
			<li>
				<label>Confirm New Password*</label> 
				<input type="password" class="text" name="conpassword" id="conpassword" />
			</li>
			<li>
				<label>&nbsp;</label> 
				<input type="reset" class="button" value="Clear" id="clear"  />&nbsp; 
				<input type="button" value="Change" id="changepassword-btn"  />
			</li>
		</ol>
	</form>
</div>
<script>
$(function() {
	$( "input:submit, input:button, button, .button").button();

	function msgbox(msg,title,elem){
		$( "#dialog-msgbox:ui-dialog" ).dialog( "destroy" );
		$( "#dialog-msgbox" ).html(msg);
		$( "#dialog-msgbox" ).attr("title",title);
		$("#dialog-msgbox").dialog('option', 'position', 'center');
		$( "#dialog-msgbox" ).dialog({
			modal: true,
			height:'auto',
			show:{ 
				effect:"blind", 
				speed:'slow' 
			},
			hide:'blind',
			buttons: {
				Ok: function() {
					$( this ).dialog( "close" );
					if (elem){
						if(elem == 'redirect'){
							window.location.href = "index.php";	
						}else{
							$('#'+elem).focus().select();
						}
					}
				}
			}
		});
	}	
	
	function ProcessRequest(data,gotoProcess){
		 $.ajax({
			 type: 'POST',
			 url: 'process.php',
			 data: data,
			 success: function(data){
				 
				 switch(gotoProcess){
					 case 'ChangePasswordCallback':
						ChangePasswordCallback(data);
					 break; 
					 
				 }
			 },
			 beforeSend: function(){
				$.dimScreen(1000, 0.5, function() {
					$('#content').fadeIn();
				});
			   },
			 complete: function(){
				 $.dimScreenStop();
			   }
		 });
	}
	
	/*-----------------------------------------------CHANGE PASSWORD--------------------------------------------------*/

	$('#changepassword-btn').click(function(){
			var frules =	[
					"required,curpassword,Please enter your current password.",
					"required,newpassword,Please enter new password.",
					"required,conpassword,Please confirm new password.",
					"same_as,newpassword,conpassword,Please ensure the new passwords you enter are the same.",
					"length>=6,newpassword,Please enter a value that is at least 6 characters long"
					];
			rsv.customErrorHandler = formError;	
			rsv.onCompleteHandler = function(theForm){
				var formValues = $(theForm).serialize();
				ProcessRequest('type=UpdateUserFirstLogin'+ '&' + formValues,'ChangePasswordCallback');				
			};
			rsv.validate(document.forms['changepassword'],frules);
		
	});
	
	function formError(f, errorInfo){		
		var fieldName;
		if(errorInfo.length > 0){
			if (errorInfo[0][0].type == undefined)
				fieldName = errorInfo[0][0][0].name;
			else
				fieldName = errorInfo[0][0].name;				
			msgbox('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0px 4px 4px 0px;"></span>'+errorInfo[0][1]+'</p>','iWMS Alert',fieldName);
		}
	}
	 
	function ChangePasswordCallback(data){
		if(data == 'success'){		
			msgbox('<p><span class="ui-icon ui-icon-info" style="float:left; margin:0px 4px 4px 0px;"></span>Your password has beed updated successfully.</p>','iWMS Confirmation','redirect'); 
		}else if(data == 'samepassword'){
			msgbox('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0px 4px 4px 0px;"></span>New password should not be the same with your current password.</p>','iWMS Alert','newpassword');
		}else if(data == 'invalid'){
			msgbox('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0px 4px 4px 0px;"></span>Invalid current password.</p>','iWMS Alert','curpassword');
		}else {
			msgbox('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0px 4px 4px 0px;"></span>Cannot update your password.Please contact Dev Team</p>','iWMS Alert','curpassword');
		}
	}
	
});
</script>
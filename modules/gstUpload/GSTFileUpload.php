<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
class GSTFileUpload{
	var $sheetData = '';
	var $postData = '';
	var $DBName = '';
	var $conn = '';
	private $logsDir   			= "logs/"; 
	private $LogFileHandler;
	// var $_SESSION['temp']='trial';
	function __construct($db,$post,$xls,$con){
		$this->sheetData = $xls;
		$this->postData = $post;
		$this->DBName = $db;
		$this->LogFileHandler = fopen($this->logsDir.$this->DBName."_".date("Y-m-d").".txt", "a+");		

		$DBName = $this->DBName;
		$sheetData = array();
		$resultArray = array();
        mssql_select_db($DBName);
	}


	public function getSheet($sheet,$index) {

        require('../../../widgets/Excel/PHPExcel.php');

        $inputFileType = PHPExcel_IOFactory::identify($sheet);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($sheet);
        $objReader->setReadDataOnly( true );
        $objPHPExcel->setActiveSheetIndex($index);
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
        
        return $sheetData;
    }

    public function getSheetwithemptyrows($sheet,$index) {

        require('../../../widgets/Excel/PHPExcel.php');

        $inputFileType = PHPExcel_IOFactory::identify($sheet);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($sheet);
        $objReader->setReadDataOnly( true );
        $objPHPExcel->setActiveSheetIndex($index);
        $maxCell = $objPHPExcel->getHighestRowAndColumn();
		$data = $sheet->rangeToArray('A1:' . $maxCell['column'] . $maxCell['row']);
		$data = array_map('array_filter', $data);
        $data = array_filter($data);
        

        return $data;
    }


    public function getSheetwithMerge($sheet,$index,$startrow,$endrow){

    	require('../../../widgets/Excel/PHPExcel.php');

        $inputFileType = PHPExcel_IOFactory::identify($sheet);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($sheet);
        $objReader->setReadDataOnly( true );
        $objPHPExcel->setActiveSheetIndex($index);
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

        $referenceRow=array();
		for ( $row = $startrow+2; $row <= $endrow; $row++ ){
		     for ( $col = 1; $col < 4; $col++ ){
		         if (!$objPHPExcel->getActiveSheet()->getCellByColumnAndRow( $col, $row )->isInMergeRange() || $objPHPExcel->getActiveSheet()->getCellByColumnAndRow( $col, $row )->isMergeRangeValueCell()) {
		             // Cell is not merged cell
		             $sheetData[$row][$col] = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow( $col, $row )->getCalculatedValue();

		             $referenceRow[$col]=$sheetData[$row][$col];  
		             //This will store the value of cell in $referenceRow so that if the next row is merged then it will use this value for the attribute
		          } else {
		             // Cell is part of a merge-range
		             $sheetData[$row][$col]=$referenceRow[$col];  
		             //The value stored for this column in $referenceRow in one of the previous iterations is the value of the merged cell
		          }

		      }
		 }
        return $sheetData;

    }

    public function getHighestrow($sheet,$index){

    	include_once('../../include//PHPExcel/PHPExcel.php');

        $inputFileType = PHPExcel_IOFactory::identify($sheet);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($sheet);
        $objReader->setReadDataOnly( true );
        $row = $objPHPExcel->setActiveSheetIndex($index)->getHighestrow();

        return $row;

    }

    function cleanData($str) {
        $str = mb_detect_encoding($str, "UTF-8") == "UTF-8" ? $str : utf8_encode($str);
        $str = trim(preg_replace('/\s+/',' ',htmlspecialchars_decode(strip_tags($str))));
        return $str;
    }

    public function Supplier($var){
        include_once('../../include/PHPExcel.php');
        $sheetData = array();
        $resultArray = array();
        $inputFileName = $this->postData['file'];
        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $chunkFilter = new chunkReadFilter($this->postData['startRow'],$this->postData['numRows']);
        $objReader->setReadFilter($chunkFilter);
        $objPHPExcel = $objReader->load($inputFileName);
        $objReader->setReadDataOnly(true);
        $objPHPExcel->setActiveSheetIndex(0);
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
        unset($sheetData[1]);

        $lineCount = 1;

        foreach ($sheetData as $key => $value) {
                
            if($key != 1)
            {
                $Supplier = $this->mssql_escape_string(trim($value['A']));

                if(!$this->checkExistingRecord(strtoupper($Supplier)))
                {
                    $arrData = array(
                        'Supplier'  =>  strtoupper($Supplier),
                        'EntryDate' =>  date('Y-m-d H:m:s'),
                        'status'    =>  1
                    );

                    $success = mssql_query("INSERT INTO [dbo].[Supplier]([".implode("],[",array_keys($arrData))."])
                                            VALUES('".implode("','",array_values($arrData))."')");

                    if($success)
                    {
                        array_push($resultArray,array('success',"Excel Line - ".$lineCount." : Supplier $Supplier  successfully save"));
                    }
                    else
                    {
                        array_push($resultArray,array('error',"Excel Line - ".$lineCount." : Unable to save supplier ".$Supplier));
                    }
                }
                else
                {
                    array_push($resultArray,array('error',"Excel Line - ".$lineCount." :Supplier $Supplier already exist"));
                }
                

            }

            $lineCount++;

        }
        return $resultArray;
    }

    public function Forwarder($var){
        include_once('../../include/PHPExcel.php');
        $sheetData = array();
        $resultArray = array();
        $inputFileName = $this->postData['file'];
        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $chunkFilter = new chunkReadFilter($this->postData['startRow'],$this->postData['numRows']);
        $objReader->setReadFilter($chunkFilter);
        $objPHPExcel = $objReader->load($inputFileName);
        $objReader->setReadDataOnly(true);
        $objPHPExcel->setActiveSheetIndex(0);
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
        unset($sheetData[1]);

        $lineCount = 1;

        foreach ($sheetData as $key => $value) {
                
            if($key != 1)
            {
                $Forwarder = $this->mssql_escape_string(trim($value['A']));

                if(!$this->checkExistingRecord(strtoupper($Forwarder),1))
                {
                    $arrData = array(
                        'Forwarder'  =>  strtoupper($Forwarder),
                        'EntryDate' =>  date('Y-m-d H:m:s'),
                        'status'    =>  1
                    );

                    $success = mssql_query("INSERT INTO [dbo].[Forwarder]([".implode("],[",array_keys($arrData))."])
                                            VALUES('".implode("','",array_values($arrData))."')");

                    if($success)
                    {
                        array_push($resultArray,array('success',"Excel Line - ".$lineCount." : Forwarder $Forwarder  successfully save"));
                    }
                    else
                    {
                        array_push($resultArray,array('error',"Excel Line - ".$lineCount." : Unable to save forwarder ".$Forwarder));
                    }
                }
                else
                {
                    array_push($resultArray,array('error',"Excel Line - ".$lineCount." :Forwarder $Forwarder already exist"));
                }
                

            }

            $lineCount++;

        }
        return $resultArray;
    }

    /**
     * @param $todo int
     * 0 = supplier
     * 1 = forwarder
     * default value 0
     * @return boolean
     **/
    function checkExistingRecord($name,$todo = 0)
    {
        if($todo == 0)
        {
            $_sql = "SELECT * FROM Supplier WHERE Supplier = '{$name}'";
        }
        else
        {
            $_sql = "SELECT * FROM Forwarder WHERE Forwarder = '{$name}'";
        }

        $result = mssql_query($_sql);

        if($result)
        {
            if(mssql_num_rows($result) > 0 )
            {
                // if result is greater than 0
                // then return true
                return true;
            }
            
            return false;
        }

        return false;

    }

    function array_column(array $input, $columnKey, $indexKey = null) {
        $array = array();
        foreach ($input as $value) {
            if ( !array_key_exists($columnKey, $value)) {
                trigger_error("Key \"$columnKey\" does not exist in array");
                return false;
            }
            if (is_null($indexKey)) {
                $array[] = $value[$columnKey];
            }
            else {
                if ( !array_key_exists($indexKey, $value)) {
                    trigger_error("Key \"$indexKey\" does not exist in array");
                    return false;
                }
                if ( ! is_scalar($value[$indexKey])) {
                    trigger_error("Key \"$indexKey\" does not contain scalar value");
                    return false;
                }
                $array[$value[$indexKey]] = $value[$columnKey];
            }
        }
        return $array;
    }

    function search($array,$value,$key){
        return array_search($value,$this->array_column($array,$key));
    }


	function GetWawiCustomer($WawiID){
		mssql_select_db('MasterData');

		$sql = mssql_query("select ID,WawiIndex,WawiAlias,DBName,GST,STG,EPcontrol,GroupEmail,SanctionedCountry,Country from WawiCustomer where [status] = 1 and ID=".$WawiID." order by WawiIndex ASC;");
		if(mssql_num_rows($sql) > 0){
			while($data[] = mssql_fetch_assoc($sql));
			array_pop($data);
		} else {	
			$data = 'no data'; 
		}

		return $data;
	}

	function MyCheckDate( $postedDate ) {
		if ( ereg("^[0-9]{4}-[01][0-9]-[0-3][0-9]$",$postedDate) ) {
			list( $year , $month , $day ) = explode('-',$postedDate);
			return( checkdate( $month , $day , $year ) );
		} else {
			return( false );
		}
	}

	function validateDate($date, $format = 'd-M-y')
	{
		$d = DateTime::createFromFormat($format, $date);
		return $d && $d->format($format) == $date;
	}

	function mssql_escape_string($string_to_escape){
		$replaced_string = $this->cleanData(str_replace("'","''",$string_to_escape));
		return $replaced_string;
	}

	function logs($m){		
		$stringData = "[Date: ".date("Y-m-d H:i:s")."]\t[Userid:".$this->postData['UserID']."]\t[TransactionID:".mktime()."]\t[$m]\r\n";
		fwrite($this->LogFileHandler, $stringData);	
	}
}

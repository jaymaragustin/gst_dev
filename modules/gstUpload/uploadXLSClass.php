<?php
// error_reporting(E_ALL);
// ini_set('display_errors', 1);

if (isset($_REQUEST['UserID'])){
	
	//session_start();
	
	require ('../../include/config.php');
	
	
	
	$uploadXLSClass = new uploadXLSClass($_REQUEST);
	$cleanedPostData = $uploadXLSClass->PostVars;
	$fn = isset($cleanedPostData['fn']) ? $cleanedPostData['fn']: false;	
	//return false;
	if($fn){
		
		$uploadXLSClass->$fn($cleanedPostData);	
	}
	
}

class uploadXLSClass{
	public $PostVars;
	public $conn;
	public $CurrentDBName;
	public $CurrentWMSIndex;
	public $CurrentWMSID;
	public $UserID;
	
	function __construct($vars){
		if (is_array($vars) && count($vars) > 1){	
		
			
			array_walk_recursive($vars,array($this,'cleanData'));
			$this->PostVars = $vars;
			
			if (isset($this->PostVars['UserID']) || (strlen($this->PostVars['UserID']) > 1)){
				$this->UserID = $this->PostVars['UserID'];
			} else {
				$this->UserID = "N/A";
			}
			
			$serverConn = unserialize(base64_decode(SQL_CONN));		
			$this->conn = mssql_connect($serverConn[0],$serverConn[1],$serverConn[2]);	
				
			if(!$this->conn){	
				$this->LogMetofile("[ReadAndExecute : Could not connect to database");
				return false;
			}
			
			 $this->CurrentDBName = 'import';//$this->GetWawiDBName();
			 $this->CurrentWMSIndex = 'GSTFileUpload';//$this->GetWawiIndex();
			 
			require_once($this->CurrentWMSIndex.'.php');
			
			if(mssql_select_db($this->CurrentDBName)){
				$this->LogMetofile("[Database Connection : connected to ".$this->CurrentDBName."]");					
			}else{
				$this->LogMetofile("[Database Connection : unable to select database ".$this->CurrentDBName."]");			
				return false;
			}

		}		
	}
	
	// function GetWawiDBName(){
	// 	$sql = mssql_fetch_assoc(mssql_query("SELECT DBName FROM MasterData.dbo.WawiCustomer WHERE ID = ".$this->CurrentWMSID));
	// 	$DBname = $sql['DBName'];
	// 	#mssql_free_result($sql);
	// 	return $DBname;
	// }
	
	// function GetWawiIndex(){	
	// 	$sql = mssql_fetch_assoc(mssql_query("SELECT WawiIndex FROM MasterData.dbo.WawiCustomer WHERE ID = ".$this->CurrentWMSID));
	// 	$WMSIndex = $sql['WawiIndex'];
	// 	#mssql_free_result($sql);
	// 	return $WMSIndex;
	// }
	
	function LogMetofile($stringData){		
		$LogFile = "logs/".$this->CurrentDBName."_".date("Y-m-d").".txt";
		$fh = fopen($LogFile, 'a+');
		$stringData = "[Date: ".date("Y-m-d H:i:s")."]\t[Userid:".$this->UserID."]\t[TransactionID:".mktime()."]\t$stringData\r\n";
		fwrite($fh, $stringData) or die('could not write');
		fclose($fh);
	}
	
	function cleanData(&$str) {
		 $str = trim(preg_replace('/\s+/',' ',htmlspecialchars_decode(strip_tags($str))));
	}
	
	function convertToDate($date) {
		$date = explode("/",$date);
		return $date[2]."-".$date[1]."-".$date[0];
	}
	
	function mssql_escape_string($string_to_escape){
		$replaced_string = str_replace("'","''",$string_to_escape);
		return $replaced_string;
	}
	
	function getDelimiter($key){
		$delimiter = array('comma'=>",",'pipe'=>"|",'tab'=>"\t");
		return $delimiter[$key];
	}
	/********************************************** MAIN FUNCTION ******************************************************/
	
	function UploadFile($post){
		extract($post);
		$UploadDir = "uploads/".$actionType."/";
		if(!is_dir($UploadDir)){	
			mkdir($UploadDir,0777,true);
		}
		include_once('uploadFileClass.php');		
		$uploader = new qqFileUploader($Extension,$SizeLimit);
		$resultArray = $uploader->handleUpload($UploadDir);
		
		if($resultArray['success']){
			$resultArray['file'] = $UploadDir.$resultArray['filename'];
			$resultArray['total'] = $this->getTotalLines($UploadDir.$resultArray['filename'],$Delimiter);
			$resultArray['message'] = "File uploaded successfully";
		}else{
			$resultArray['succes'] = false;
			$resultArray['message'] = $resultArray['error'];
		}		
		$this->LogMetofile("[DATABASE: IMPORT]\t[Action:UploadFile('".$resultArray['filename']."')]\t[Remarks: ".$resultArray['message']."");
	 	header('Content-type: application/json');
		echo json_encode($resultArray);
	}
	
	function getTotalLines($url,$del){
		include_once('../../include/PHPExcel.php');
		if(file_exists($url)){
			$pathinfo = pathinfo($url);
			if(strtoupper($pathinfo['extension']) == 'PDF'){
				return 1;
			}
			$inputFileName = $url;
			$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($inputFileName);			
			if($inputFileType == 'CSV'){	
				$delimiter = $this->getDelimiter($del);
				$objReader->setDelimiter($delimiter); 
			}else{
				$objReader->setReadDataOnly(true);
				$objPHPExcel->setActiveSheetIndex(0);
			}		
			return $objPHPExcel->getActiveSheet()->getHighestRow() - 1;			
		}else{
			return 0;
		}
	}
	
	function readAndExecute($post){
		$this->LogMetofile('bernard -- 0');
		$DBName = $this->CurrentDBName;
		extract($post);
		$resultArray = array();
		if(file_exists($file)){		
			if(isset($ownClass) && $ownClass == 1){
				
				
				//ADDDED
				
				$startRow = $startRow;
				$chunkSize = $numRows;
				$sheetData = array();
				$inputFileName = $file;
				include_once('../../include/PHPExcel.php');
				$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
				$objReader = PHPExcel_IOFactory::createReader($inputFileType);
				
				$chunkFilter = new chunkReadFilter($startRow,$chunkSize);
				$objReader->setReadFilter($chunkFilter);
				
				$objPHPExcel = $objReader->load($inputFileName);
				
				if($inputFileType == 'CSV'){
					$delimiter = $this->getDelimiter($Delimiter);
					$objReader->setDelimiter($delimiter);
				}else{
					$objReader->setReadDataOnly(true);
					$objPHPExcel->setActiveSheetIndex(0);
				}
				$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);	
				
				array_walk_recursive($sheetData,array($this,'mssql_escape_string_data'));
				
				//ADDDED
				
				
				$this->LogMetofile('bernard -- 1');
				$wmsClass = $this->CurrentWMSIndex;
				$actionType = $this->PostVars['actionType'];
				$WMS = new $wmsClass($this->CurrentDBName,$post,$file,$this->conn);				
				$resultArray = $WMS->$actionType();
				
			}else{
				$this->LogMetofile('bernard -- 2');
				$startRow = $startRow;
				$chunkSize = $numRows;
				$sheetData = array();
				$inputFileName = $file;
				include_once('../../include/PHPExcel.php');
				$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
				$objReader = PHPExcel_IOFactory::createReader($inputFileType);
				
				$chunkFilter = new chunkReadFilter($startRow,$chunkSize);
				$objReader->setReadFilter($chunkFilter);
				
				$objPHPExcel = $objReader->load($inputFileName);
				
				if($inputFileType == 'CSV'){
					$delimiter = $this->getDelimiter($Delimiter);
					$objReader->setDelimiter($delimiter);
				}else{
					$objReader->setReadDataOnly(true);
					$objPHPExcel->setActiveSheetIndex(0);
				}
				$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);	 		
				if(count($sheetData) > 0){
					$wmsClass = $this->CurrentWMSIndex;
					$this->LogMetofile("[ReadAndExecute : ".$wmsClass."]");
					$actionType = $actionType;
					$WMS = new $wmsClass($this->CurrentDBName,$post,$sheetData,$this->conn);				
					$resultArray = $WMS->$actionType();
					$this->LogMetofile("[ReadAndExecute : Reading row $startRow to ".($startRow + $chunkSize)."]");
					foreach($resultArray as $result){
						if($result[0] == 'error'){
							$this->LogMetofile("[ReadAndExecute : ".$result[1]."]");
						}				
					}
				}else{
					array_push($resultArray,array('error','File is empty .'));
					$this->LogMetofile("[ReadAndExecute : File is empty.]");
				}			
			}
		}else{
			array_push($resultArray,array('error','File is missing. Kindly try to upload again.'));
			$this->LogMetofile("[ReadAndExecute : File is missing.]");
		}		
		header('Content-type: application/json');
		echo json_encode($resultArray);
	}
	
	function mssql_escape_string_data(&$mssql_escape_string_data){
		
		$pos1	= strpos($mssql_escape_string_data, 'SELECT ');
		$pos2	= strpos($mssql_escape_string_data, 'DELETE ');
		$pos3	= strpos($mssql_escape_string_data, 'DELETE FROM');
		$pos4	= strpos($mssql_escape_string_data, 'DROP TABLE');
		$pos5	= strpos($mssql_escape_string_data, 'INSERT INTO');
		$pos6	= strpos($mssql_escape_string_data, 'UPDATE ');
		$pos7	= strpos($mssql_escape_string_data, 'EXEC ');
		$pos8	= strpos($mssql_escape_string_data, 'TRUNCATE TABLE');
		
		$pos9	= strpos($mssql_escape_string_data, 'EXECUTE ');
		$pos10	= strpos($mssql_escape_string_data, 'ALTER TABLE');
		$pos11 	= strpos($mssql_escape_string_data, 'COMMENT ');
		$pos12 	= strpos($mssql_escape_string_data, 'RENAME ');
		$pos13 	= strpos($mssql_escape_string_data, 'GRANT ');
		$pos14 	= strpos($mssql_escape_string_data, 'REVOKE ');
		$pos15 	= strpos($mssql_escape_string_data, 'COMMIT ');
		$pos16 	= strpos($mssql_escape_string_data, 'MERGE ');
		$pos17 	= strpos($mssql_escape_string_data, 'CREATE TABLE');
		$pos18 	= strpos($mssql_escape_string_data, 'CONNECT ');
		$pos19 	= strpos($mssql_escape_string_data, 'START TRANSACTION ');
		$pos20 	= strpos($mssql_escape_string_data, 'ROLLBACK ');
		
		$pos21	= strpos($mssql_escape_string_data, 'SELECT*');
		$pos22	= strpos($mssql_escape_string_data, 'DELETE[');
		$pos23	= strpos($mssql_escape_string_data, 'EXEC[');
		$pos24	= strpos($mssql_escape_string_data, 'EXECUTE[');
		$pos25	= strpos($mssql_escape_string_data, 'ALTER VIEW');
		$pos26 	= strpos($mssql_escape_string_data, 'CREATE VIEW');
		
		if (
			$pos1	== true ||
			$pos2	== true ||
			$pos3	== true ||
			$pos4	== true ||
			$pos5	== true ||
			$pos6	== true ||
			$pos7	== true ||
			$pos8	== true ||
			$pos9	== true ||
			$pos10	== true ||
			$pos11	== true ||
			$pos12	== true ||
			$pos13	== true ||
			$pos14	== true ||
			$pos15	== true ||
			$pos16	== true ||
			$pos17	== true ||
			$pos18	== true ||
			$pos19	== true ||
			$pos20	== true ||
			$pos21	== true ||
			$pos22	== true ||
			$pos23	== true ||
			$pos24	== true ||
			$pos25	== true ||
			$pos26	== true
		   )
		{
			echo "FOUNDSQLPROBLEM";
			die();
		} else {
			
		}
		
	}
	
	function __destruct() {
       if ($this->conn){
		   mssql_close($this->conn);
	   }
    }
}
?>
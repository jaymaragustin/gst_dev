<?php 
session_start();
$_SESSION['OrderList'] = array("Orders"=>array(),"Success"=>array(),"Errors"=>array());
require ('../../include/referer.checker.php');
require ('../../include/session.checker.php');
?>
<h2><?php include '../../tpl/module_shortcut.php';?>Upload File</h2>
<form class="globalform" id="" name="">
	<ol style="position:relative"> 
		<?php if($_SESSION['defaultWawiID'] == 250){ ?>
		<li>
			<p style="font-style: italic;color:red;">Note: Please select action type to download the available template.</p>
		</li>
		<?php } ?>
		<li>
			<label>Action Type</label>
			<select id="actionType" class="select" name="actionType" style="min-width: 150px;">
                <option value="" del="">Select Here</option>
                <option value="Supplier" del="" numrows="10">Supplier</option>
                <option value="Forwarder" del="" numrows="10">Forwarder</option>
			</select>
			<a  id="GSTDownloadFormat" style="display:none;" href="modules/gstUpload/uploads/GST_Template.xlsx" download target="_blank"><div class="button" style="width: 125px;font-size:12px;margin-left:10px;margin-top:3px;">Download format</div></a>
				
			
		</li>

        <li>
			<label>File to upload</label>		
			<div class="file-upload" style="float: left; margin-top: 10px;">
				<div id="upload-btn" class="button" style="width: 150px;font-size:15px;">Select File</div>
			</div>	
		</li>

		<li>
			<label>Message</label>	
			<div class="msg-scroll-box">
				<span id="progress">Processing...</span>
				<p class="messages upload-msg"></p>	
				<p class="summary upload-msg"></p>	
			</div>
		</li>
	</ol>
	<br class="clear">
</form>
<div id="dialog"></div>
<style>

	.msg-scroll-box{ float: left; width: 750px; margin-top: 10px !important; }
	.messages{ float: left; border: 1px solid #eee; width: 90%; height: 300px; overflow: auto; padding-top: 10px !important; }
	.messages span{ float: left; clear: both; }
	.summary{ float: left; margin-top: 5px !important; width: 90%; }
	.summary span{   cursor: pointer; font-size: 14px; margin-right: 20px;}
	#progress{ float: right; margin: 0 90px 5px; background: url(images/pre-loader.gif) no-repeat left center;  color: #008000; text-indent: 26px; display: table-cell; height: 15px; line-height: 15px; vertical-align: middle; visibility: hidden; }

</style>
<script type="text/javascript">
	var urlWorker = 'modules/gstUpload/uploadXLSClass.php';
	var fileExtension = ['xls','xlsx','csv','pdf','txt'];
	var fileSize = 10*1024*1024;
	var WawiID = '<?php echo $_SESSION['defaultWawiID'];?>';
	var UserID = '<?php echo $_SESSION['iWMS-User'];?>';
	var asnNumber;

	$(function() {		
		$("input:submit, input:button, button, .button").button();
		$.getScript('js/jslibrary.js');
		
		$('#actionType').change(function() {
            if($('#actionType').val() == 'Supplier'){
                $('#GSTDownloadFormat').show();
            } else {
                $("#GSTDownloadFormat").show();
            }
        });
			
			

    var messages = $('p.messages');

    var uploader = new qq.FileUploaderBasic({
            button: $('#upload-btn')[0],
            multiple: false,
            action: urlWorker,
            allowedExtensions: fileExtension,
            sizeLimit: fileSize,
            onSubmit: function(id, fileName){
                uploader.setParams({
                    'actionType': $('#actionType').val(),
                    'WawiID' : WawiID,
                    'Extension' : fileExtension,
                    'SizeLimit' : fileSize,
                    'Delimiter': $('#actionType option:selected').attr('del'),			
                    'UploadDir' : 'uploads/',
                    'fn' : 'UploadFile',
                    'UserID' : UserID			   
                });
                if($('#actionType').val() == ""){
                    alert('Please select action type.');
                    return false;
                }else{
                    if(confirm("Are you sure you want to upload excel file? Action cannot be undone.")){
                        $('#progress').css('visibility','visible');
                        $('p.messages').html('<span class="success">Uploading... ' + '"' + fileName + '" </span>');
                    }else{
                        return false;
                    }
                }
            },
            onComplete : function(d, fileName, responseJSON){
                if (responseJSON.success) {
                    $('p.messages').html('<span class="success">&mdash; Saving completed</span>');
                    $('.messages').append('<span class="success">&mdash; Processing data</span>');

                    if(responseJSON.total > 0){		

                        var numRows = $('#actionType option:selected').attr('numrows');
                        var headerRow = $('#actionType option:selected').attr('header');
                        var totalLines = responseJSON.total;
                        
                        var totalRequest = Math.ceil(totalLines / numRows);
                        var totalctr = 0;
                        var errorctr = 0;
                        var noticectr = 0;
                        $.ajaxSetup({async: false});

                        if(headerRow == 1){
                            totalRequest++;
                        }

                        if(headerRow==4 )
                        var i = 3; //skip 4 rows
                        else if(headerRow==2)
                            var i = 2; //skip 3 rows
                        else if(headerRow==5)
                            var i = 4; //skip 4rows
                        else if(headerRow==8)
                            var i = 7; //skip 7rows
                        else
                            i=1;

                            
                        for (i;i<=totalRequest;i++){

                            startRow = i==1 ? i + 1 : ((i-1) * numRows) + 2; //if i=1 startrow = 2 start at 2nd
                            // startRow = i==1 ? i + 3 : ((i-1) * numRows) + 2;
                            if(headerRow == 1){
                                startRow--; //start first row
                            }
                        
                            //check startRow if equal to i
                            //startRow starts at totalRequest(total row)
                            
                            if(i == startRow)
                            {
                                $.post(urlWorker,
                                {	
                                    'fn':'readAndExecute',
                                    'file':responseJSON.file,
                                    'startRow':startRow,
                                    'numRows':numRows,
                                    'actionType':$('#actionType').val(),
                                    'Delimiter': $('#actionType option:selected').attr('del'),
                                    'ownClass': $('#actionType option:selected').attr('ownclass'),
                                    'Trial': $('#actionType option:selected').attr('trial'),	
                                    'WawiID' : WawiID,
                                    'UserID' : UserID
                                },
                                function(data){
                                    console.log(data);
                                    if(data == 'FOUNDSQLPROBLEM'){
                                        $('p.messages').append('<span class="error">&mdash; We detected invalid data on the file. Please check.</span>');
                                        return false;
                                    }
                                    
                                    $.each(data,function(i,e){
                                        if(e[0] == 'error'){
                                            errorctr++;		
                                        }else if (e[0] == 'notice'){
                                            noticectr++;
                                        }
                                        $('p.messages').append('<span class="'+e[0]+'">&mdash; '+e[1]+'</span>');
                                        $('p.messages').scrollTop($('p.messages')[0].scrollHeight);
                                    });
                                    totalctr += count(data);
                                    $('p.summary').html('<span onclick="viewContent(\'all\');" >Total : <strong>'+totalctr+'</strong></span><span onclick="viewContent(\'success\');">Success : <span style="color:green">'+( totalctr - errorctr - noticectr )+'</span></span><span onclick="viewContent(\'error\');">Error : <span style="color:red">'+ errorctr +'</span></span>');					

                                }
                                );
                            }
                            else
                            {
                                $.post(urlWorker,
                                {	
                                    'fn':'readAndExecute',
                                    'file':responseJSON.file,
                                    'startRow':startRow,
                                    'numRows':numRows,
                                    'actionType':$('#actionType').val(),
                                    'Delimiter': $('#actionType option:selected').attr('del'),
                                    'ownClass': $('#actionType option:selected').attr('ownclass'),
                                    'Trial': $('#actionType option:selected').attr('trial'),	
                                    'WawiID' : WawiID,
                                    'UserID' : UserID
                                },
                                function(data){
                                    console.log(data);
                                    if(data == 'FOUNDSQLPROBLEM'){
                                        $('p.messages').append('<span class="error">&mdash; We detected invalid data on the file. Please check.</span>');
                                        return false;
                                    }
                                    
                                    $.each(data,function(i,e){
                                        if(e[0] == 'error'){
                                            errorctr++;		
                                        }else if (e[0] == 'notice'){
                                            noticectr++;
                                        }
                                        $('p.messages').append('<span class="'+e[0]+'">&mdash; '+e[1]+'</span>');
                                        $('p.messages').scrollTop($('p.messages')[0].scrollHeight);
                                    });
                                    totalctr += count(data);
                                    $('p.summary').html('<span onclick="viewContent(\'all\');" >Total : <strong>'+totalctr+'</strong></span><span onclick="viewContent(\'success\');">Success : <span style="color:green">'+( totalctr - errorctr - noticectr )+'</span></span><span onclick="viewContent(\'error\');">Error : <span style="color:red">'+ errorctr +'</span></span>');					

                                }
                                );
                            }

                        }
                        $('#progress').css('visibility','hidden');
                        $.ajaxSetup({async: false});
                    }else{
                        $('p.messages').append('<span class="success">File is empty.</span>');	
                    }
                }else{
                    $('p.messages').html('<span class="error">Saving failed.</span>');
                }
        $('#progress').css('visibility','hidden'); 
        }
    });	

    viewContent = function(tag){
        $('p.messages span').hide();
        if(tag == 'success'){
            $('p.messages span.success').show();		
        }else if(tag == 'error'){
            $('p.messages span.error').show();
        }else{
            $('p.messages span').show();	
        }
    }

});
</script>
</div>

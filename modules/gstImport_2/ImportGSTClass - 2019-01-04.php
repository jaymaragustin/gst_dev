<?php
if (isset($_POST['todo'])){
	require ('../../include/config.php');
	$ImportGSTClass = new ImportGSTClass($_REQUEST);
	
	if ($_POST['todo'] == 'DataSearch'){
		echo $ImportGSTClass->DataSearch();
	} else if ($_POST['todo'] == 'CustomerList'){
		echo $ImportGSTClass->CustomerList();
	} else if ($_POST['todo'] == 'GetRefNumber'){
		echo $ImportGSTClass->GetRefNumber();
	} else if ($_POST['todo'] == 'ProcessData'){
		echo $ImportGSTClass->ProcessData();
	} else if ($_POST['todo'] == 'UpdateReference'){
		echo $ImportGSTClass->UpdateReference();
	} else if ($_POST['todo'] == 'UpdateReferenceNull'){
		echo $ImportGSTClass->UpdateReferenceNull();
	}
}

Class ImportGSTClass{
	var $PostVars;
	function __construct($vars){
		$this->PostVars = $vars;
		$serverConn = unserialize(base64_decode(SQL_CONN));
		$this->conn = mssql_connect($serverConn[0],$serverConn[1],$serverConn[2]);
		mssql_select_db("Import");
	}
	
	function CustomerList(){
		mssql_select_db('MasterData');
		$data = array();
		
		$sql = mssql_query("select WawiAlias,GSTIndex AS WawiIndex,DBname from wawicustomer where [status] = 1 order by GSTIndex ASC");
		while($CustomerList[] = mssql_fetch_assoc($sql)){}
		array_pop($CustomerList);
		
		$listCurrency = $this->ListCurrency();

		mssql_select_db("Import");
		
		$this->PostVars['WawiIndex'] = $CustomerList[0]['WawiIndex'];
		$RefList = $this->GetRefNumber();
		
		$sql = mssql_query("select top 2 ShipReference from v_ol_ShipImport");
		
		if(mssql_num_rows($sql) > 0){
			$chkrecords = 'data';
		} else {
			$chkrecords = 'nodata';
		}
		
		/*
		unset($sql);
		$sql = mssql_query("select Forwarder from Forwarder where [Status] = 1");
		while($ListofFrowarder[] = mssql_fetch_assoc($sql)){}
		array_pop($ListofFrowarder);
		*/
		unset($sql);
		$sql = mssql_query("select Supplier from Supplier where [Status] = 1 order by Supplier ASC");
		while($ListofSupplier[] = mssql_fetch_assoc($sql)){}
		array_pop($ListofSupplier);
		
		array_push($data,$CustomerList);
		array_push($data,$RefList);
		array_push($data,$listCurrency);
		array_push($data,$this->GSTRate());
		array_push($data,$chkrecords);
		//array_push($data,$ListofFrowarder);
		array_push($data,$ListofSupplier);
		
		return json_encode($data);
	}
	
	function ListCurrency(){
		$sql = mssql_query("SELECT DISTINCT Currency FROM Country ORDER BY Currency");
		if (mssql_num_rows($sql) > 0){
			while($CurrencyList[] = mssql_fetch_assoc($sql)){}
			array_pop($CurrencyList);
		} else {
			$CurrencyList = array(array('Currency' => 'SGD'),array('Currency' => 'USD'));
		}
		
		return $CurrencyList;
	}
	
	function GSTRate(){
		$sql = mssql_query("SELECT Value FROM Setup WHERE Entry='GST'");
		if (mssql_num_rows($sql) > 0){
			$sql = mssql_fetch_assoc($sql);
			$GSTRate = $sql['Value'];
		} else {
			$GSTRate = 7;
		}
		
		return $GSTRate;
	}
	
	function mssqlQuotedString($str){
		$str = str_replace("\\", "\\\\", $str);
		$str = str_replace("'", "''", $str);
		
		return $str;
	}
	
	function DataSearch(){
		if ($this->PostVars['SearchAction'] == 'First'){ // First run
			$sql = mssql_query("select convert(text,VendorInvNo) as VendorInvNo1,* from v_ol_ShipImport where ShipReference = (select top 1 ShipReference from v_ol_ShipImport order by ShipReference ASC)");
		} else if($this->PostVars['SearchAction'] == 'Next'){
			$sql = mssql_query("select convert(text,VendorInvNo) as VendorInvNo1,* from v_ol_ShipImport where ShipReference = (select top 1 ShipReference from v_ol_ShipImport where ShipReference > ".$this->PostVars['ShipReference']." order by ShipReference ASC)");
		} else if($this->PostVars['SearchAction'] == 'Previous'){
			$sql = mssql_query("select convert(text,VendorInvNo) as VendorInvNo1,* from v_ol_ShipImport where ShipReference = (select top 1 ShipReference from v_ol_ShipImport where ShipReference < ".$this->PostVars['ShipReference']." order by ShipReference DESC)");
		} else if($this->PostVars['SearchAction'] == 'Last'){
			$sql = mssql_query("select convert(text,VendorInvNo) as VendorInvNo1,* from v_ol_ShipImport where ShipReference = (select top 1 ShipReference from v_ol_ShipImport order by ShipReference DESC)");
		} else if($this->PostVars['SearchAction'] == 'Search'){
			$sql = mssql_query("select convert(text,VendorInvNo) as VendorInvNo1,* from v_ol_ShipImport where ShipReference = ".$this->PostVars['ShipReference']);
		}

		if (mssql_num_rows($sql) < 1){
			return $this->PostVars['SearchAction'];
		} else {
			$data = array();
			while($row = mssql_fetch_assoc($sql)){
				$row1 = mssql_fetch_assoc(mssql_query("select sum(Quantity) as QtyFromEntry from ".$row['EntryTable']." where ForwarderRef = '".$row['Reference']."'"));
				$row['QtyFromEntry'] = $row1['QtyFromEntry'];
				array_push($data,$row);
			}
			
			//array_push($data,$str);
			
			return json_encode($data);
		}
	}
	
	function GetRefNumber(){
		$DBname = $this->GetQuantityCustomer();
		mssql_select_db("Import");
		
		 $sql = mssql_query("Select ie.ForwarderRef as 'Reference',sum(ie.Quantity) as 'TotalQty' 
							from Import i,".$DBname.".dbo.Entry ie 
							where i.WawiIndex = '".$this->PostVars['WawiIndex']."' and 
							i.ShipReference is null and  
							convert(varchar,ltrim(rtrim(ie.ForwarderRef))) = convert(varchar,ltrim(rtrim(i.Reference))) 
							and (YEAR(ie.EntryDate) = Year(Getdate()))
							GROUP BY ie.ForwarderRef ORDER BY ie.ForwarderRef ASC");

		// $sql = mssql_query("Select ie.ForwarderRef as 'Reference',sum(ie.Quantity) as 'TotalQty' from Import i,".$DBname.".dbo.Entry ie where i.WawiIndex = '".$this->PostVars['WawiIndex']."' and i.ShipReference is null and  convert(varchar,ltrim(rtrim(ie.ForwarderRef))) = convert(varchar,ltrim(rtrim(i.Reference))) GROUP BY ie.ForwarderRef ORDER BY ie.ForwarderRef ASC");
		
		if (mssql_num_rows($sql) < 1){
			$RefList = 'NoRef';
			return $RefList;
		} else {
			while($RefList[] = mssql_fetch_assoc($sql)){}
			array_pop($RefList);
			if ($_POST['todo'] == 'GetRefNumber'){
				return json_encode($RefList);
			} else {
				return $RefList;
			}
		}
	}
	
	function ProcessData(){
		if ($this->PostVars['todochk'] == 'Update'){
			return $this->UpdateGST();
		} else {
			return $this->AddNewGST();
		}
	}
	
	function UpdateGST(){
		$data = array($this->PostVars['txtShipReference']);
		if (mssql_query("update Shipment set [VendorInvNo]='".$this->PostVars['txtVendorInvNo']."',[InTaxRef]='".$this->PostVars['slctax']."',[PermitNo]='".$this->PostVars['txtPermitNo']."',[ExchangeRate]='".$this->PostVars['txtExchangeRate']."',[Currency]='".$this->PostVars['slcCurrency']."',[OriginPort]='".$this->PostVars['txtOriginPort']."',[Supplier]='".$this->mssqlQuotedString($this->PostVars['slcGSTSupplier'])."',[HAWB]='".$this->PostVars['txtHAWB']."',[MAWB]='".$this->PostVars['txtMAWB']."',[TptMode]='".$this->PostVars['txtTptMode']."',[ArrivalDate]='".$this->PostVars['txtArrivalDate']."',[INCOTERM]='".$this->PostVars['slcIncoTerm']."',[Forwarders]='".$this->PostVars['slcForwarders']."',[Comments]='".$this->mssqlQuotedString($this->PostVars['txtComments'])."',[JobType]='".$this->PostVars['slcJobType']."',[Customer]='".$this->PostVars['slcCustomerList1']."',[InvoiceValueSGD]=".$this->PostVars['txtInvoiceValueSGD'].",[FreightCharges]=".$this->PostVars['txtFreightCharges'].",[InsCharges]=".$this->PostVars['txtInsCharges'].",[CIFAmount]=".$this->PostVars['txtCIFAmount'].",[GSTAmount]=".$this->PostVars['txtGSTAmount']." where ShipReference = ".$this->PostVars['txtShipReference'])){
			mssql_query("update [Import] set WawiIndex='".$this->PostVars['slcCustomerList1']."' where ShipReference = ".$this->PostVars['txtShipReference']);
			array_push($data,'successUPDATE');
		} else {
			array_push($data,'errorUpdate');
		}
		return json_encode($data);
	}
	
	function AddNewGST(){
		mssql_select_db("Import");
		mssql_query("BEGIN TRAN");
		$sqlNewShipRec = mssql_fetch_assoc(mssql_query("SELECT Value FROM Setup WHERE Entry='ShipCounter'"));
		$ShipReference = $sqlNewShipRec['Value'] + 1;
		mssql_query("Update Setup set Value = Value + 1 WHERE Entry='ShipCounter'");
		unset($sqlNewShipRec);
		
		$data = array($ShipReference);
		if (mssql_query("Insert into Shipment ([ShipReference],[EnterDate],[VendorInvNo],[InTaxRef],[PermitNo],[ExchangeRate],[Currency],[OriginPort],[Supplier],[HAWB],[MAWB],[TptMode],[ArrivalDate],[INCOTERM],[Forwarders],[Comments],[JobType],[Customer],[InvoiceValueSGD],[FreightCharges],[InsCharges],[CIFAmount],[GSTAmount]) values ($ShipReference,GetDate(),'".$this->PostVars['txtVendorInvNo']."','".$this->PostVars['slctax']."','".$this->PostVars['txtPermitNo']."','".$this->PostVars['txtExchangeRate']."','".$this->PostVars['slcCurrency']."','".$this->PostVars['txtOriginPort']."','".$this->mssqlQuotedString($this->PostVars['slcGSTSupplier'])."','".$this->PostVars['txtHAWB']."','".$this->PostVars['txtMAWB']."','".$this->PostVars['txtTptMode']."','".$this->PostVars['txtArrivalDate']."','".$this->PostVars['slcIncoTerm']."','".$this->PostVars['slcForwarders']."','".$this->mssqlQuotedString($this->PostVars['txtComments'])."','".$this->PostVars['slcJobType']."','".$this->PostVars['WawiIndex']."',".$this->PostVars['txtInvoiceValueSGD'].",".$this->PostVars['txtFreightCharges'].",".$this->PostVars['txtInsCharges'].",".$this->PostVars['txtCIFAmount'].",".$this->PostVars['txtGSTAmount'].")")){
			mssql_query("COMMIT");
			array_push($data,'successADD');
		} else {
			mssql_query("ROLLBACK");
			array_push($data,'erroradd');
		}
		return json_encode($data);
	}
	
	function GetQuantityCustomer(){
		mssql_select_db("MasterData");
		$sql = mssql_fetch_assoc(mssql_query("select DBName from WawiCustomer where [GSTIndex] = '".$this->PostVars['WawiIndex']."'"));
			
		return $sql['DBName'];
	}
	
	function UpdateReference(){
		//32965
		mssql_select_db("Import");
		$sqlShipment = mssql_fetch_assoc(mssql_query("select Customer from Shipment where ShipReference=".$this->PostVars['ShipReference']));
		$Customer = $sqlShipment['Customer'];
		
		$ListRefToUpdate = $this->PostVars['ListRef'];
		foreach($ListRefToUpdate as $RefNum){
			$RefArray = explode("-",$RefNum);
			//2014-04-29 updated due to primary key and incoming reference is duplicate in two different WMS
			mssql_query("Update Import set ShipReference=".$this->PostVars['ShipReference'].",InvoiceValue=".$RefArray[1]." where Reference=".$RefArray[0]." and WawiIndex='".$Customer."';");
			//mssql_query("Update Import set ShipReference=".$this->PostVars['ShipReference'].",WawiIndex='".$Customer."',InvoiceValue=".$RefArray[1]." where Reference=".$RefArray[0]);
		}
		
		$data = array($this->PostVars['ShipReference'],$this->PostVars['action']);
		return json_encode($data);
	}
	
	function UpdateReferenceNull(){
		mssql_select_db('Import');
		mssql_query("Update Import set InvoiceValue='0.000000',ShipReference=Null where Reference = ".$this->PostVars['RefNum']);
	}
}

?>
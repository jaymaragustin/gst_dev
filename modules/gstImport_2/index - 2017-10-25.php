<?php 
//error_reporting(E_ALL);
//ini_set("display_errors",1);
session_start();
require ('../../include/referer.checker.php');
require ('../../include/session.checker.php');

#style="border:1px solid #EEEEEE;"

?>
<style>
	.alignme{
		text-align:center;
	}
</style>
<h2><?php include '../../tpl/module_shortcut.php';?>Import</h2> 

<form onsubmit="return false;" class="globalform"> 
<div id="divMainBody">
	<div style="float: left; padding: 5px; width: 370px;">
		<ol>
	  	<li><label>Shipment Number:</label><input type='text' id="ShipReference" name="ShipReference" class="text" value=""/></li>
		</ol>
	</div>
	<div style="float: left; padding: 5px; width: 510px;">
		<input type="button" name="btnNew" id="btnNew" value="New"  />
		<input type="button" name="btnFirst" id="btnFirst" value="First"  />
		<input type="button" name="btnPrevious" id="btnPrevious" value="Previous"  />
		<input type="button" name="btnNext" id="btnNext" value="Next"  />
		<input type="button" name="btnLast" id="btnLast" value="Last"  />&nbsp;&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;&nbsp;
		<input type="button" name="btnEdit" id="btnEdit" value="Edit"  />
		<input type="button" name="btnDelete" id="btnDelete" value="Delete"  />
	</div>
	<div class="clr"></div>
	<div style="float: left; padding: 5px; width: 370px;">
		<ol>
			<li><label>Customer:</label><input type="text" class="text" readonly id='WawiAlias' name='WawiAlias' /></li>
	  	<li><label>HAWB:</label><input type="text" class="text" readonly id='HAWB' name='HAWB' /></li>
	  	<li><label>MAWB:</label><input type="text" class="text" readonly id='MAWB' name='MAWB' /></li>
	  	<li><label>Flight:</label><input type="text" class="text" readonly id='TptMode' name='TptMode' /></li>
	  	<li><label>Origin Port:</label><input type="text" class="text" readonly id='OriginPort' name='OriginPort' /></li>
	  	<li><label>Arrival Date:</label><input type="text" class="text" readonly id='ArrivalDate' name='ArrivalDate' /></li>
	  	<li><label>INCOTERM:</label><input type="text" class="text" readonly id='INCOTERM' name='INCOTERM' /></li>
	  	<li><label>Vendor/Supplier:</label><input type="text" class="text" readonly id='Supplier' name='Supplier' /></li>
	  	<li><label>Forwarder:</label><input type="text" class="text" readonly id='Forwarders' name='Forwarders' /></li>
	  	<li><label>Comments:</label><input type="text" class="text" readonly id='Comments' name='Comments' /></li>
	  	<li><label>Tax:</label><input type="text" class="text" readonly id='InTaxRef' name='InTaxRef' /></li>
	  	<li><label>Permit No:</label><input type="text" class="text" readonly id='PermitNo' name='PermitNo' /></li>
	  	<li><label>Vendor Invoice:</label><input type="text" class="text" readonly id='VendorInvNo' name='VendorInvNo' /></li>
	  	<li><label>Invoice Currency:</label><input type="text" class="text" readonly id='Currency' name='Currency' /></li>
	  	<li><label>Exchange Rate:</label><input type="text" class="text" readonly id='ExchangeRate' name='ExchangeRate' /></li>
		</ol>
	</div>
	<div style="float: left; padding: 5px; width: 510px;">
			<fieldset>
				<legend>Computation</legend>
				<ol>
					<li><label>Total Invoice:</label><input type="text" class="text" readonly id='InvoiceValue' name='InvoiceValue' /></li>
					<li><label>Vendor Invoice:</label><input type="text" class="text" readonly id='InvoiceValueSGD' name='InvoiceValueSGD' /></li>
					<li><label>Freight Charges:</label><input type="text" class="text" readonly id='FreightCharges' name='FreightCharges' /></li>
					<li><label>Insurance:</label><input type="text" class="text" readonly id='InsCharges' name='InsCharges' /></li>
					<li><label>CIF Amount:</label><input type="text" class="text" readonly id='CIFAmount' name='CIFAmount' /></li>
					<li><label>GST Amount:</label><input type="text" class="text" readonly id='GSTAmount' name='GSTAmount' /></li>
					<li><label>Job Type:</label><input type="text" class="text" readonly id='JobType' name='JobType' /></li>
				</ol>
			</fieldset>
			<div class="clr"></div>
			<fieldset>
				<legend>Reference Number(s)</legend>
				<ol>
					<li>
						<table class="display" id="tblRefNumbers" style="width:380px;"></table>
					</li>
				</ol>
			</fieldset>
	</div>
</div>

<div id="divNewImport">
	<div style="float: left; padding: 5px; width: 370px;">
		<ol>
			<li><label>Customer:</label><select id='slcCustomerList' class='select'></select></li>
			<li id='slcCustomerList1Li'><label>Change to Customer:</label><select id='slcCustomerList1' class='select'></select></li>
			<li><div id="lishipreference"><label>Ship Reference:</label><input type="text" class="text" readonly id='txtShipReference' name='txtShipReference' /></div></li>
		</ol>
	</div>
	<div style="float: right;">
		<input type="button" name="btnSave" id="btnSave" value="Save" />
		<input type="button" name="btnCancel" id="btnCancel" value="Cancel" />
	</div>
	<div id="divAddNewGST">
			<fieldset style="width:880px;">
				<legend>GST Import Details</legend>
				<div id="leftData" style="float: left; padding: 5px; width: 425px;">
					<ol>
				  	<li><label>HAWB:</label><input type="text" class="text" id='txtHAWB' name='txtHAWB' /></li>
				  	<li><label>MAWB:</label><input type="text" class="text" id='txtMAWB' name='txtMAWB' /></li>
				  	<li><label>Flight:</label><input type="text" class="text" id='txtTptMode' name='txtTptMode' /></li>
				  	<li><label>Origin Port:</label><input type="text" class="text" id='txtOriginPort' name='txtOriginPort' /></li>
				  	<li><label>Arrival Date:</label><input type="text" class="text" id='txtArrivalDate' name='txtArrivalDate' /></li>
				  	<li><label>INCOTERM:</label></label><select id="slcIncoTerm" name="slcIncoTerm" class="select"></select></li>
				  	<li><label>Vendor/Supplier:</label><select id="slcGSTSupplier" name="slcGSTSupplier" class="select"></select></li>
				  	<li><label>Forwarder:</label><select id="slcForwarders" name="slcForwarders" class="select"></select></li>
				  	<li><label>Comments:</label><input type="text" class="text" id='txtComments' name='txtComments' /></li>
				  	<li><label>Tax:</label><select id="slcTax" name="slctax" class="select"></select></li>
				  	<li><label>Permit No:</label><input type="text" class="text" id='txtPermitNo' name='txtPermitNo' /></li>
				  	<li><label>Vendor Invoice:</label><input type="text" class="text" id='txtVendorInvNo' name='txtVendorInvNo' /></li>
				  	<li><label>Invoice Currency:</label><select id="slcCurrency" name="slcCurrency" class="select"></select></li>
					</ol>
				</div>
				<div id="rightData" style="float: left; padding: 5px;">
					<ol>
						<li><label>Job Type:</label><select id="slcJobType" name="slcJobType" class="select"></select></li>
						<li><label>Exchange Rate:</label><input type="text" class="text" id='txtExchangeRate' name='txtExchangeRate' /></li>
						<li>
							<fieldset>
								<legend>Computation</legend>
									<ol>
										<li><label>Freight Charges:</label><input type="text" class="text" id='txtFreightCharges' name='txtFreightCharges' /></li>
										<li><label>Total Invoice:</label><input type="text" class="text" id='txtInvoiceValue' name='txtInvoiceValue' readonly /></li>
										<li><label>Vendor Invoice:</label><input type="text" class="text" id='txtInvoiceValueSGD' name='txtInvoiceValueSGD' readonly /></li>
										<li><label>Insurance:</label><input type="text" class="text" id='txtInsCharges' name='txtInsCharges' /></li>
										<li><label>CIF Amount:</label><input type="text" class="text" id='txtCIFAmount' name='txtCIFAmount' /></li>
										<li><label>GST Amount:</label><input type="text" class="text" id='txtGSTAmount' name='txtGSTAmount' readonly /></li>
										<li><label>&nbsp;</label><input type="button" name="btnCalculate" id="btnCalculate" value="Calculate" style="float: right;" /></li>
									</ol>
							</fieldset>
						</li>
						<li>
							<fieldset>
								<legend>Reference Numbers</legend>
									<ol>
										<li><label style="width:120px;">Reference Number:</label><input type="text" class="text" id="txtsearchplist" name="txtsearchplist" style="margin-right:10px;"/><select id='slcUnlistedRefNum' class='select'></select>&nbsp&nbsp;<input type="button" name="btnAddtoList" id="btnAddtoList" value="Add" style="margin-top: 2px;" /></li>
										<li><table class="display" id="tblListRefNumbers" style="width:400px;"></table></li>
									</ol>
							</fieldset>
						</li>
					</ol>
				</div>
			</fieldset>
	</div>
	<div id="divNoAvailable">
		<fieldset style="width:880px;">
			<legend>GST Import Details</legend>
			<ol>
				<li>
					No Available Reference number
				</li>
			</ol>
		</fieldset>
	</div>
</div>
</form>
<script>
var CurrentShipmentReference;
var CurrentRefList = new Object();
var removeThisRef;
var tblListRefNumbers;
var tblRefNumbers;
var CurrentShipment;
var tmpRefArray = [];
var OLGSTRate = 0;

ErrorKeys = new Object();
ErrorKeys['First'] = 'Beginning of the data';
ErrorKeys['Previous'] = 'Beginning of the data';
ErrorKeys['Last'] = 'Last data';
ErrorKeys['Next'] = 'Last data';
ErrorKeys['Search'] = 'Sorry invalid Shipment Number';
ErrorKeys['nodata'] = 'Sorry no available Reference Number';

AddUpdateMSG = new Object();
AddUpdateMSG['successUPDATE'] = 'Update successful for Shipment Reference: <strong><shipreference></strong>';
AddUpdateMSG['errorUpdate'] = 'Unable to update Shipment Reference: <strong><shipreference></strong>. Please check your data and try again later.';
AddUpdateMSG['successADD'] = 'New Shipment Reference: <strong><shipreference></strong> has been created.';
AddUpdateMSG['erroradd'] = 'Unable to create New Shipment Reference. Please check your data and try again later.';

var TaxKeys = ['N.A.','GST PAID','JSI MES','ACMT','VCR','CCR','AMCC MES'];
var IncoTermKeys = ['NA','EXW','FAS','FCA','FOB','CFR','CPT','CIF','CIP','DES','DAF','DAP','DEQ','DDU','DDP','T/T IN ADVANCE'];
var JobTypeKeys = ['Import','Local Delivery','RWK In','RMA','Hand carry'];
var GSTSupplier = ['AECO Technology Co Ltd','AMBIT - HK','AMCC C/O EXPEDITORS','AMKOR - KR','AMKOR - PH','AMKOR - SG','AMKOR - TW','AMKOR - CN','ASE - SG','ASE - TW','ASE - MY','ASE - KOREA','ASIAN INFO - TW','ADVANCE SEMICON - TW','AIRSPAN NETWORK ISREAL LTD','AMCC - US','ASAT - HK','ASCA  TECH - KR','Astralink','AVNET ASIA - SG','Archwave','ARTESYN TECH','ARROW ASIA PAC LTD','AMBIT MICROSYSTEMS -CN','AMI SEMICON - PH','ASIAN INFO TECH - HK','ASUSTEK - CN','AVALON TECH - IN','ALPHA NETWORK - HK','ADVANCED SEMICON - TW','BENCHMARK','BM NAGANO CO LTD','CRESCENT TECH','CORETEK','CARSEM MY','C PAK','CANON - HK','CELESTICA - PH','CELESTICA - TH','CONEXANT - US','CONEXANT - CN','CONEXANT - JP','CHINA INFORMATION - CN','CONSYSTEM','DHL AVIVATION (HK) LTD','DHL CN','DHL Express Magyarorszag','DHL International UK Ltd','DEAJIN - KR','DELTA GREEN - CN','DDS - SIN','DONG GUAN - CN','Double Negative Singapore PL','Dialog Semiconductor Den Bosch','DIGI-KEY CORP','EDOM - HK','Element14','EMBEDDED PLANET - US','EMULEX - US','ESYSLOGIC','FLASH C/O HUBNET','FORTRON CN','FEDEX - CN','FEDEX - HK','FEDEX - US','FLEXTRONICS - MY','FLEXTRONICS - CN','GCT RESEARCH INC - KR','GE MDS LLS US','GIGA SOLUTION - TW','GIGA TECH - HK','Gigaset Communications GmbH','GLOBAL CONTAINER','GLOBAL TESTING - TW','GLOBAL UNICHIP','GLOBAL FOUNDARIES','GTS TRADING','GLOBAL BRANDS - HK','GLOBAL TECH - KR','GREENCHIP','GTC - TW','HYLINE - DE','HANA MICRON INC','HAUPPAUGE - US','HON HAI - HK','HONGFUJIN - HK','HONGFUJIN - CN','HAKUTO','HUA KE ELECTRONICS HK LTD ','INNOTECH - JP','IBM - SIN','IBM - US','IBM US C/O Expeditor Singapore Pte Ltd','Innochip TW','INTEL HK','Interhorizon Corporation','Interhorizon','In-Tech Electronics Ltd','ISMOSYS','IONICS EMS INC','JSI - HK','JSI - US','JSI - TW','JAM TECH - CN','KANEMATSU - JP','KOSTATS','KAWASAKI - JP','KAWASUMI','KWE - TW','KWE - HK','KYEC - TW','LG  ELECT - KR','LITE ON TECH','LOGITRADE','LONGTRUMP - TW','LIGHTS ELECT - ','LSI TECH - SIN','MOTOROLA','MICROTEK - JP','MULTIWAVE - KR','MICRONAS US','MURATA JP','MARVELL DE','MACNICA MY','MEC MY','METATECH - HK','MAINTEK COMPUTER - CN','MARVELL SIN','MES TW','MACNICA MY','Nangang Guixin','NANYA','NANYA TW','NANYA US','Nepes Pte Ltd','NSE SG','NU HORIZONS','PROMATE ELECT','PEAK RESOURCES','RS components','RT MICRO','RYOYO','RTI-HOLDING - HK','SAMSUNG - KR','SAMSUNG - MY','SAMSUNG - US','SAGEM COMMUNICATION CN','SANMINA-SCI','SIRF - SG','SIRF - US','SIRF SG','SIRF US','SMD - SG','SPIL - CN','SPIL - TW','ST MICRO','STATS - KR','STATS - MY','STATS - PH','STATS - SG','STATS - TH','Stats chippac Shanghai CN','SYNERCHIPS - TW','SYNNEX - CN','SYNNEX - HK','Syntech Technologies','SCIENTIFIC - MX','SHINDEN - JP','SERVANTS ','SERTEK - TW','SEQUOIA - UK','Semitech','SINOPRIME  GLOBAL - CN','Siliconware Tech -CN','SILICON TECH CO LTD - JP','SIGURD MIRCOELECTRONICS CORP','SAMSUNG - TW','SAMSUNG - HK','SOLECTRON - MY','SANMINA - MX','SAME TIME - HK','SI - US','SITEL - NL','SITEL - CN','SITEL - HK','Sinotech Communication Ltd - HK','STATS - TW','TSMC NORTH AMERICA - US','TOKYO ELECTRON - JP','UTSTARCOM - CN','TECHNISAT','TELFORD TE','TELFORD SG','TELTONIKA UAB','TSMC (CHINA) COMPANY LTD','UPS SCS US','UTAC - SG','UTAC - CN','UNIFIELD','UTAC THAI','UTAC HK LTD','UNI-ELECTRONICS','USI INTERNATIONAL - HK','VOLTERRA ASIA','VOLTERRA GLOBAL','VINYAS INNOVATIVE - SG','WINTECH','Wintronix - HK','WORLD PEACE - HK','WORLD PEACE - TW','WEIKENG - HK','WEIKENG - TW','WPG - SG','WPI - HK','WPI - TW','WAWI TECH - HK','WISTRON','WINTECH TW','WINTECH HK','XAVI TECH - CN','XAVI TECH - TW','YA YI SHI - HK','Z-COM - CN'];
var GSTForwarders = ['JSI','Freight Forwarder','Courier','Local Transporter'];

$(function() {
	$( "input:submit, input:button, button, .button").button();
	$("#divNewImport").hide();
	$("#lishipreference").hide();
	$('#txtArrivalDate').datepicker({
		dateFormat: 'mm/dd/yy',
		onSelect: function(date) {
            $('#slcIncoTerm').focus();
        }
		});
	
	$("#txtExchangeRate").DecimalMask('9999999.999999');
	$("#txtFreightCharges").DecimalMask('9999999.99');
	$("#txtInsCharges").DecimalMask('9999999.99');
	$("#txtCIFAmount").DecimalMask('9999999.99');

	jQuery.fn.exists = function(){return this.length>0;}
	jQuery.isJson = function(str) {
	 if (jQuery.trim(str) == '') return false;
	 str = str.replace(/\\./g, '@').replace(/"[^"\\\n\r]*"/g, '');
	 return (/^[,:{}\[\]0-9.\-+Eaeflnr-u \n\r\t]*$/).test(str);
	}

	function msgbox(msg,focusField){
		$( "#dialog-msgbox:ui-dialog" ).dialog( "destroy" );
		$( "#dialog-msgbox" ).html(msg);
		$( "#dialog-msgbox" ).attr("title",'GST Import');
		$( "#dialog-msgbox" ).dialog('option', 'position', 'center');
		$( "#dialog-msgbox" ).dialog({
			modal: true,
			height:'auto',
			show:{ 
				effect:"blind", 
				speed:'slow' 
			},
			hide:'blind',
			buttons: {
				Ok: function() {
					$(this).dialog( "close" );
					if (focusField){
						if (focusField == 'ProcessData'){
							$("#btnCancel").click();
							ProcessRequest({'todo':'DataSearch','ShipReference':$("#ShipReference").val(),'SearchAction':'Search'},'SearchListData');
						} else if (focusField == 'ErrorProcessData'){
							// do nothing in order for them to check the data
						} else if (focusField == 'checkRecords'){
							// no available Shipment Reference (no pending Reference # to add)
						} else {
							$('#'+focusField).focus().select();
						}
					}
				}
			}
		});
	}

	function ProcessRequest(data,gotoProcess){
		$.ajax({
		 type: 'POST',
		 url: 'modules/gstImport/ImportGSTClass.php',
		 data: data,
		 success: function(data){
				var CallMeBaby = eval('(' + gotoProcess + ')');
						CallMeBaby(data);
		 },
		 beforeSend: function(){
				$.dimScreen(function() {
					$('#content').fadeIn();
				});
		 },
		 complete: function(){
			 $.dimScreenStop();
		 }
		});
	}
	
	function SearchListData(data){
		if ($.isJson(data) == false){
			msgbox(ErrorKeys[data],'ShipReference');
			$("#ShipReference").val(CurrentShipmentReference);
		} else {
			var RefList = [];
			
			var obj = jQuery.parseJSON(data);

			var totalInv = 0;
			
			CurrentShipmentReference = obj[0]['ShipReference'];
			totalInv = 0;
			totalQty = 0;
			$.each(obj,function(a,b){
				if ($.trim(b['InvoiceValue']) != ''){
					totalInv = parseFloat(b['InvoiceValue']) + totalInv;
				}
				
				if ($.trim(b['QtyFromEntry']) != ''){
					totalQty = parseInt($.trim(b['QtyFromEntry'])) + totalQty;
				}
				RefList.push([b['Reference'],b['QtyFromEntry'], b['InvoiceValue']]);
				
				$.each(b,function(c,d){
					
					if (c == 'VendorInvNo1' || c == 'VendorInvNo'){
							$('#VendorInvNo').val(b['VendorInvNo1']);
					} else {
					
						if ($('#' + c).exists()) {
							if ($.trim(d) == ''){
								d = '-';
							}
							$('#' + c).val(d);
						}
					}
				});
			});
			
			var amt = parseFloat(totalInv);
			$("#InvoiceValue").val(amt.toFixed(2));
			
			if (tblRefNumbers){
				tblRefNumbers.fnDestroy();
			}
			
			tblRefNumbers = $('#tblRefNumbers').dataTable({
					'bFilter': false,
					'bPaginate': false,
					'bJQueryUI': true,
					'aLengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
					'sPaginationType': 'full_numbers',
					"bScrollCollapse": true,      
					"bDestroy": true,      
					"aaData" : RefList,
					"aoColumns": [
						{ "sTitle": "Reference", "sClass":"alignme" },
						{ "sTitle": "Quantity", "sClass":"alignme"  },
						{ "sTitle": "Invoice Value", "sClass":"alignme"  }
					]
				});
				
				
				$("#tblRefNumbers_filter label").css('width','200px');
				$("#tblRefNumbers_wrapper .fg-toolbar").css('height','10px');
				$("#tblRefNumbers_info").removeClass('dataTables_info');
				$("#tblRefNumbers_wrapper .fg-toolbar:first-child").html('Total Quantity: <strong>' + totalQty + '</strong>');
				CurrentShipment = obj;
		}
	}
	
	function CustomerList(data){
		var obj = jQuery.parseJSON(data);

		$.each(obj[0],function(a,b){
     $('#slcCustomerList')
         .append($("<option></option>")
         .attr("value",b['WawiIndex'])
         .text(b['WawiAlias']));
		});
		
		$.each(obj[0],function(a,b){
     $('#slcCustomerList1')
         .append($("<option></option>")
         .attr("value",b['WawiIndex'])
         .text(b['WawiAlias']));
		});
		
		$.each(obj[2],function(a,b){
     $('#slcCurrency')
         .append($("<option></option>")
         .attr("value",b['Currency'])
         .text(b['Currency']));
		});
		
		OLGSTRate = parseFloat(obj[3]);

		$.each(TaxKeys,function(a,b){
     $('#slcTax')
         .append($("<option></option>")
         .attr("value",b)
         .text(b)); 
		});

		$.each(IncoTermKeys,function(a,b){
     $('#slcIncoTerm')
         .append($("<option></option>")
         .attr("value",b)
         .text(b)); 
		});

		$.each(JobTypeKeys,function(a,b){
     $('#slcJobType')
         .append($("<option></option>")
         .attr("value",b)
         .text(b)); 
		});
		
		/*
		if (obj[5].length > 0){
			GSTForwarders = [];
			$.each(obj[5],function(a,b){
				GSTForwarders.push(b['Forwarder']);
			});
		}
		*/
		$.each(GSTForwarders,function(a,b){
     $('#slcForwarders')
         .append($("<option></option>")
         .attr("value",b)
         .text(b)); 
		});
		
		if (obj[5].length > 0){
			GSTSupplier = [];
			$.each(obj[5],function(a,b){
				GSTSupplier.push(b['Supplier']);
			});
		}
		$.each(GSTSupplier,function(a,b){
     $('#slcGSTSupplier')
         .append($("<option></option>")
         .attr("value",b)
         .text(b)); 
		});

		GetRefNumber(obj[1],1);
		
		if (obj[4] == 'nodata'){
			msgbox(ErrorKeys[obj[4]],'checkRecords');
			return false;
		}

		ProcessRequest({'todo':'DataSearch','ShipReference':0,'SearchAction':'First'},'SearchListData');
	}
	
	function GetRefNumber(data,chk){
		if (data == 'NoRef'){
			$("#divAddNewGST").hide();
			$("#divNoAvailable").show();
			$("#btnSave").hide();
		} else {
			var obj;
			if (chk){
				obj = data;
			} else {
				obj = jQuery.parseJSON(data);
			}
			
			$("#divAddNewGST").show();
			$("#divNoAvailable").hide();
			$("#btnSave").show();
			
			CurrentRefList = new Object();
			$("#slcUnlistedRefNum").empty();
			$.each(obj,function(a,b){
				CurrentRefList[b['Reference']] = $.trim(b['TotalQty']);
				$("#slcUnlistedRefNum")
							.append($("<option></option>")
							.attr('value',b['Reference'])
							.text(b['Reference']));
			});
			
			if (tblListRefNumbers){
				tblListRefNumbers.fnDestroy();
			}
			
			tblListRefNumbers = $('#tblListRefNumbers').dataTable({
					'bFilter': false,
					'bPaginate': false,
					'bJQueryUI': true,
					'aLengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
					'sPaginationType': 'full_numbers',
					"bScrollCollapse": true,      
					"bDestroy": true,      
					"aaData" : [],
					"aaSorting": [[1,'asc']],
					"aoColumns": [
						{ "sTitle": "Tools" },
						{ "sTitle": "Reference" },
						{ "sTitle": "Quantity" },
						{ "sTitle": "Inv Value(USD)",'sClass':'classEditInvoice' }
					]
				});
		}
	}
	
	function ListRefNumberForEditing(data){
		$("#slcUnlistedRefNum").empty();
		if (data != 'NoRef'){
			var obj = jQuery.parseJSON(data);
			CurrentRefList = new Object();
			$.each(obj,function(a,b){
				CurrentRefList[b['Reference']] = $.trim(b['TotalQty']);
				$("#slcUnlistedRefNum")
							.append($("<option></option>")
							.attr('value',b['Reference'])
							.text(b['Reference']));
			});
		}
		$.each(tblListRefNumbers.fnGetData(),function(a,b){
			CurrentRefList[b[1]] = $.trim(b[2]);
		});
	}
	
	function UpdateVendorQty(){
		if (tblListRefNumbers){
			if (tblListRefNumbers.fnGetData().length == 0){
				$('#txtInvoiceValueSGD').val('0');
			} else {
				var tmpQtyInvoiceVal = 0;
				$.each(tblListRefNumbers.fnGetData(),function(a,b){
					tmpQtyInvoiceVal = tmpQtyInvoiceVal + parseInt(b[2]);
				});
				$('#txtInvoiceValueSGD').val(tmpQtyInvoiceVal);
			}
		} else {
			$('#txtInvoiceValueSGD').val('0');
		}
	}
	
	function makeedit(){
		var TotalInvValUSD = 0;
		$.each($('.classEditInvoice', tblListRefNumbers.fnGetNodes()),function(a,b){
			TotalInvValUSD = TotalInvValUSD + parseFloat($.trim($(b).html()));
		});

		$('#txtInvoiceValue').val(TotalInvValUSD.toFixed(2));
		$('#btnCalculate').click();
	}
	
	function calculate(){
    var xd1;
    var xd2;
		var xd3;
		var xd4;
		var xd5;
		
		xd1 = parseFloat($("#txtInvoiceValue").val());
		xd2 = parseFloat($("#txtFreightCharges").val());

		if (xd1 > 0){
			var totalINVSGD = xd1 * parseFloat($("#txtExchangeRate").val());
			$("#txtInvoiceValueSGD").val(totalINVSGD.toFixed(2));
			
			xd3 = 0.01 * (totalINVSGD + xd2); //Insurance
			xd4 = parseFloat($("#txtInvoiceValueSGD").val()) + xd2 + xd3; //CIF
			xd5 = (xd4 * OLGSTRate) / 100;
			
			//if ($("#slcForwarders").val() == 'Courier' || $("#slcForwarders").val() == 'Local Transporter'){
			//	$("#txtInsCharges").val('0.00');
			//} else {
				$("#txtInsCharges").val(xd3.toFixed(2));
			//}
			
			$("#txtCIFAmount").val(xd4.toFixed(2));
			$("#txtGSTAmount").val(xd5.toFixed(2));
			checkNaN();
		}
	}
	
	function calculate2(){
    var xd1;
    var xd2;
		var xd3;
		var xd4;

		$("#txtInsCharges").val('0.00');
		$("#txtFreightCharges").val('0.00');
		
		xd1 = parseFloat($("#txtInvoiceValue").val());
		xd2 = parseFloat($("#txtFreightCharges").val());
		if (xd1 > 0){	
			var totalINVSGD = xd1 * parseFloat($("#txtExchangeRate").val());
			$("#txtInvoiceValueSGD").val(totalINVSGD.toFixed(2));
			xd4 = totalINVSGD + xd2; //CIF
			$("#txtCIFAmount").val(xd4.toFixed(2));
			xd5 = (xd4 * OLGSTRate) / 100;
			$("#txtGSTAmount").val(xd5.toFixed(2));
			checkNaN();
		}
	}
	
	function checkNaN(){
			if ($("#txtInsCharges").val() == 'NaN'){
				$("#txtInsCharges").val('0.00');
			}
			
			if ($("#txtCIFAmount").val() == 'NaN'){
				$("#txtCIFAmount").val('0.00');
			}
			
			if ($("#txtGSTAmount").val() == 'NaN'){
				$("#txtGSTAmount").val('0.00');
			}
	}
	
	function ProcessData(data){
		var obj = jQuery.parseJSON(data);
		var msg = AddUpdateMSG[obj[1]];
		var passToMsgBox = '';
	
		if (obj[1] == 'successADD' || obj[1] == 'successUPDATE'){
			$("#ShipReference").val(obj[0]);
			if (tblListRefNumbers.fnGetData().length > 0){
				var sendme = [];
				$.each(tblListRefNumbers.fnGetData(),function(a,b){
					sendme.push(b[1] + '-' + b[3]);
				});
				ProcessRequest({'todo':'UpdateReference','ShipReference':obj[0],'ListRef':sendme,'action':obj[1]},'UpdateReference');
				return false;
			}
			passToMsgBox = 'ProcessData';
		} else {
			passToMsgBox = 'ErrorProcessData';
		}
		
		msg = msg.replace('<shipreference>',obj[0]);
		msgbox(msg,passToMsgBox);
	}
	
	function cleanData(){
		$.each($("#leftData ol").find("input").filter("[type=text]"),function(a,b){
			if ($.trim($(b).val()) == ''){
				$(b).val('-');
			}
		});
		
		$.each($("#rightData ol").find("input").filter("[type=text]"),function(a,b){
			if ($.trim($(b).val()) == ''){
				$(b).val('0.00');
			}
		});
	}
	
	function UpdateReference(data){
		var obj = jQuery.parseJSON(data);
		var msg = AddUpdateMSG[obj[1]];
		msg = msg.replace('<shipreference>',obj[0]);
		msgbox(msg,'ProcessData');
	}
	
	function RefreshListReference(){
		$('.classEditInvoice', tblListRefNumbers.fnGetNodes()).editable(
		 	function(value1, settings) {
				return value1;
		  },{"callback": function( sValue, y ) {
		  	if (sValue == ''){
		  		sValue = '0.00';
		  	}
		  	var aPos = tblListRefNumbers.fnGetPosition(this);
		  	tblListRefNumbers.fnUpdate(sValue,aPos[0],aPos[1],0,0);
		  	makeedit();},"width": "80px"}
		);
	}
	
	function txtCIFUpdateTextField(){
		if ($("#slcForwarders").val() == 'Courier' || $("#slcForwarders").val() == 'Local Transporter'){
			$("#txtCIFAmount").removeAttr('readonly');
			$("#txtInsCharges").attr('readonly','readonly');
			$("#txtFreightCharges").attr('readonly','readonly');
		} else {
			$("#txtCIFAmount").attr('readonly','readonly');
			$("#txtInsCharges").removeAttr('readonly');
			$("#txtFreightCharges").removeAttr('readonly');
		}
	}
	
	$("#btnFirst").click(function(){
		ProcessRequest({'todo':'DataSearch','ShipReference':CurrentShipmentReference,'SearchAction':'First'},'SearchListData');
	});

	$("#btnPrevious").click(function(){
		ProcessRequest({'todo':'DataSearch','ShipReference':CurrentShipmentReference,'SearchAction':'Previous'},'SearchListData');
	});
	
	$("#btnNext").click(function(){
		ProcessRequest({'todo':'DataSearch','ShipReference':CurrentShipmentReference,'SearchAction':'Next'},'SearchListData');
	});
	
	$("#btnLast").click(function(){
		ProcessRequest({'todo':'DataSearch','ShipReference':CurrentShipmentReference,'SearchAction':'Last'},'SearchListData');
	});
	
	$("#ShipReference").keypress(function(event){
		if(event.keyCode == 13){
			if ($.trim($("#ShipReference").val()) == ''){
				msgbox('Please specify ShipmentNumber','ShipReference');
			} else {
				ProcessRequest({'todo':'DataSearch','ShipReference':$("#ShipReference").val(),'SearchAction':'Search'},'SearchListData');
			}
		}
	});
	
	$("#btnNew").click(function(){
		$("#divMainBody").slideUp('slow');
		$("#divNewImport").slideDown('slow');
		$('#btnCalculate').val('Calculate');
		$("#btnSave").val('Save');
		$.each($('.globalform div#divAddNewGST').find('input').filter("[type=text]"),function(a,b){
			$(b).val('');
		});

		$("#txtInvoiceValue").val('0.00');
		$("#txtInvoiceValueSGD").val('0.00');
		$("#txtInsCharges").val('0.00');
		$("#txtCIFAmount").val('0.00');
		$("#txtGSTAmount").val('0.00');
		$("#txtExchangeRate").val('0.00');
		$("#lishipreference").hide();
		$("#slcCustomerList").removeAttr('disabled');
		$('#slcCustomerList').change();
		$('#slcCustomerList1Li').hide();
	});
	
	$("#btnCancel").click(function(){
		$("#divNewImport").slideUp('slow');
		$("#divMainBody").slideDown('slow');
	});
	
	$('#slcCustomerList').change(function(){
		ProcessRequest({'todo':'GetRefNumber','WawiIndex':$('#slcCustomerList').val()},'GetRefNumber');
	});
	
	$('#btnAddtoList').click(function(){
		if ($.trim($('#slcUnlistedRefNum').val()) != ''){
			tblListRefNumbers.fnAddData([
			    "<span style=\"cursor:pointer\" onclick=\"removeThisRef(this," + $('#slcUnlistedRefNum').val() + ")\">Remove</span>",
			    $('#slcUnlistedRefNum').val(),
			    CurrentRefList[$('#slcUnlistedRefNum').val()],
			    "0.00"]
			  );
			$("#slcUnlistedRefNum option[value='" + $('#slcUnlistedRefNum').val() + "']").remove();
			RefreshListReference();
			//UpdateVendorQty();
		}
	});
	
	$("#txtHAWB").keyup(function(event){
		if (event.keyCode == 13){
			if ($.trim($("#txtHAWB").val()) == ''){
				$("#txtHAWB").val('-');
			}
			$("#txtMAWB").focus().select();
		}
	});
	
	$("#txtMAWB").keyup(function(event){
		if (event.keyCode == 13){
			if ($.trim($("#txtMAWB").val()) == ''){
				$("#txtMAWB").val('-');
			}
			$("#txtTptMode").focus().select();
		}
	});
	
	$("#txtTptMode").keyup(function(event){
		if (event.keyCode == 13){
			if ($.trim($("#txtTptMode").val()) == ''){
				$("#txtTptMode").val('-');
			}
			$("#txtOriginPort").focus().select();
		}
	});
	
	$("#txtOriginPort").keyup(function(event){
		if (event.keyCode == 13){
			if ($.trim($("#txtOriginPort").val()) == ''){
				$("#txtOriginPort").val('-');
			}
			$("#txtArrivalDate").focus().select();
		}
	});
	
	$("#slcIncoTerm").keypress(function(event){
		if (event.keyCode == 13){
			$("#slcGSTSupplier").focus().select();
		}
	});
	
	$("#txtComments").keypress(function(event){
		if (event.keyCode == 13){
			if ($.trim($("#txtComments").val()) == ''){
				$("#txtComments").val('-');
			}
			$("#slctax").focus().select();
		}
	});
	
	$("#slctax").keypress(function(event){
		if (event.keyCode == 13){
			$("#txtPermitNo").focus().select();
		}
	});
	
	$("#txtPermitNo").keypress(function(event){
		if (event.keyCode == 13){
			if ($.trim($("#txtPermitNo").val()) == ''){
				$("#txtPermitNo").val('-');
			}
			$("#txtVendorInvNo").focus().select();
		}
	});
	
	$("#txtVendorInvNo").keypress(function(event){
		if (event.keyCode == 13){
			if ($.trim($("#txtVendorInvNo").val()) == ''){
				$("#txtVendorInvNo").val('-');
			}
			$("#txtCurrency").focus().select();
		}
	});
	
	$("#txtCurrency").keypress(function(event){
		if (event.keyCode == 13){
			if ($.trim($("#txtCurrency").val()) == ''){
				$("#txtCurrency").val('0.00');
			}
			$("#slcJobType").focus().select();
		}
	});

	$("#slcJobType").keypress(function(event){
		if (event.keyCode == 13){
			$("#txtExchangeRate").focus().select();
		}
	});
	
	$("#txtExchangeRate").keypress(function(event){
		if (event.keyCode == 13){
			if ($.trim($("#txtExchangeRate").val()) == ''){
				$("#txtExchangeRate").val('0.00');
			}
			$('#btnCalculate').click();
			$("#txtFreightCharges").focus().select();
		}
	});
	
	$("#txtFreightCharges").keypress(function(event){
		if (event.keyCode == 13){
			if ($.trim($("#txtFreightCharges").val()) == ''){
				$("#txtExchangeRate").val('0.00');
			}
			$('#btnCalculate').click();
			$("#slcUnlistedRefNum").focus().select();
		}
	});
	
	$("#txtInsCharges").keypress(function(event){
		if (event.keyCode == 13){
			if($.trim($("#txtInsCharges").val()) == ''){
				$('#btnCalculate').click();
			} else {
				if (parseFloat($("#txtInvoiceValue").val()) > 0){
					var tmpInsCharge = parseFloat($("#txtInsCharges").val());
					var tmpCIFAmount = parseFloat($("#txtCIFAmount").val());
					
					$('#btnCalculate').click();

					$("#txtInsCharges").val(tmpInsCharge.toFixed(2));
					$("#txtCIFAmount").val(tmpCIFAmount.toFixed(2));
					
					var xd2 = parseFloat($("#txtFreightCharges").val());
					var xd3 = parseFloat($("#txtInsCharges").val());
					var xd4 = parseFloat($("#txtInvoiceValueSGD").val()) + xd2 + xd3; //CIF
					var xd5 = (xd4 * OLGSTRate) / 100;
					$("#txtGSTAmount").val(xd5.toFixed(2));
					$("#txtCIFAmount").val(xd4.toFixed(2));
					checkNaN();
				}
			}
		}
	});
	
	$("#txtCIFAmount").keypress(function(event){
		if (event.keyCode == 13){
			if($.trim($("#txtCIFAmount").val()) == ''){
				msgbox('Please enter CIF Amount','txtCIFAmount');
			} else {
				var tmpCIFAmount = parseFloat($("#txtCIFAmount").val());
				$('#btnCalculate').click();
				$("#txtCIFAmount").val(tmpCIFAmount.toFixed(2));
				var xd5 = ($.trim($("#txtCIFAmount").val()) * OLGSTRate) / 100;
				$("#txtGSTAmount").val(xd5.toFixed(2));
				checkNaN();
			}
		}
	});
	
	$("#btnSave").click(function(){
		cleanData();
		
		var ToDo = $("#btnSave").val();
		var Params = new Object;
		Params = {'todo':'ProcessData','todochk':ToDo,'WawiIndex':$("#slcCustomerList").val(),'slcCustomerList1':$("#slcCustomerList1").val()};
		$.each($('.globalform div#divNewImport').find('input,select').serializeArray(),function(a,b){
			Params[b.name] = b.value;
		});
		ProcessRequest(Params,'ProcessData');
	});
	
	$("#btnEdit").click(function(){
		$('#btnCalculate').val('Re-Calculate');
		if (tblListRefNumbers){
			tblListRefNumbers.fnDestroy();
		}
		
		tblListRefNumbers = $('#tblListRefNumbers').dataTable({
				'bFilter': false,
				'bPaginate': false,
				'bJQueryUI': true,
				'aLengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
				'sPaginationType': 'full_numbers',
				"bScrollCollapse": true,      
				"bDestroy": true,
				"aaData" : [],
				"aaSorting": [[1,'asc']],
				"aoColumns": [
					{ "sTitle": "Tools" },
					{ "sTitle": "Reference" },
					{ "sTitle": "Quantity" },
					{ "sTitle": "Inv Value(USD)",'sClass':'classEditInvoice' }
				]
			});
		
		tmpRefArray = [];
		$.each(tblRefNumbers.fnGetData(),function(a,b){ 
			if ($.trim(b[0]) != ''){
				tblListRefNumbers.fnAddData([
				    "<span style=\"cursor:pointer\" onclick=\"removeThisRef(this," + b[0] + ")\">Remove</span>",
				    b[0],
				    b[1],
				    b[2]]
				  );
				 tmpRefArray.push($.trim(b[0]));
			}
		});
		
		var obj = CurrentShipment;
			$.each(obj,function(a,b){
				$.each(b,function(c,d){
					if ($('#txt' + c).exists()) {
						if ($.trim(d) == ''){
							d = '-';
						}
						$('#txt' + c).val(d);
					}
					$('#txtVendorInvNo').val(b['VendorInvNo1']);
				});
			});
		$('#slcCustomerList1Li').show();
		$("#slcCustomerList option").filter(function() {
		    return $(this).text() == $("#WawiAlias").val(); 
		}).attr('selected', true);
		
		$("#slcCustomerList1 option").filter(function() {
		    return $(this).text() == $("#WawiAlias").val(); 
		}).attr('selected', true);

		$("#txtInvoiceValue").val($("#InvoiceValue").val());
 		$("#slcIncoTerm option:contains(" + $("#INCOTERM").val() + ")").attr('selected', 'selected');
 		$("#slcGSTSupplier option:contains(" + $("#Supplier").val() + ")").attr('selected', 'selected');
 		$("#slcTax option:contains(" + $("#InTaxRef").val() + ")").attr('selected', 'selected');
 		$("#slcJobType option:contains(" + $("#JobType").val() + ")").attr('selected', 'selected');
 		//$("#slcCustomerList option:contains(" + $("#WawiAlias").val() + ")").attr('selected', 'selected');
 		$("#slcCurrency option:contains(" + $("#Currency").val() + ")").attr('selected', 'selected');
 		$("#slcForwarders option:contains(" + $("#Forwarders").val() + ")").attr('selected', 'selected');
		
		txtCIFUpdateTextField();
		
		$("#divNoAvailable").hide();
		$("#divAddNewGST").show();
		$("#btnSave").show();
		$("#lishipreference").show();
		$("#slcCustomerList").attr('disabled','disabled');
		
		$("#divMainBody").slideUp('slow');
		$("#divNewImport").slideDown('slow');
		
		RefreshListReference();
		
		$("#btnSave").val('Update');
		
		ProcessRequest({'todo':'GetRefNumber','WawiIndex':$('#slcCustomerList').val()},'ListRefNumberForEditing');
	});
	
	$('#btnCalculate').click(function(){
		if ($('#slcForwarders').val() == 'JSI' || $('#slcForwarders').val() == 'Freight Forwarder'){
			calculate();
		} else {
			calculate2();
		}
	});
	
	$('#slcForwarders').change(function(){
		txtCIFUpdateTextField();
	});
	
	$('#txtsearchplist').keypress(function(event){
		$("#slcUnlistedRefNum option:contains(" + $("#txtsearchplist").val() + ")").attr('selected', 'selected');
	});
	
	$("#btnDelete").click(function(){
		
	});
	
	ProcessRequest({'todo':'CustomerList'},'CustomerList');
	
	removeThisRef = function(obj,ref){		
		var pos = tblListRefNumbers.fnGetPosition( $(obj).closest('tr').get(0));
		tblListRefNumbers.fnDeleteRow(pos);
		//var tmptotalInv = parseFloat($("#txtInvoiceValue").val()) - CurrentRefList[ref];
		$('#slcUnlistedRefNum').append('<option value="'+ ref +'">'+ ref +'</option>');
		makeedit();
		ProcessRequest({'todo':'UpdateReferenceNull','RefNum':ref},'ReferenceUpdated');
	}
	
	function ReferenceUpdated(data){
	}
	
});
</script>
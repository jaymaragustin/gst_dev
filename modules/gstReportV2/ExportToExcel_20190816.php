<?php
//ExportToExcel.php

#error_reporting(E_ALL);
#ini_set('display_errors',1);
ini_set('memory_limit', '1024M');

require('../../../widgets/Excel/PHPExcel.php');
$phpExcel = new PHPExcel();

$styleArrayHeader = array(
	'font' => array(
		'bold' => true,
	)
);

$ActiveSheet = $phpExcel->setActiveSheetIndex(0);
$ActiveSheet->setTitle("Incoming GST Detailed");
printHeaderDetails($ActiveSheet,$styleArrayHeader,$_POST['StartDate'],$_POST['EndDate'],$_POST['TaxRef'],$_POST['Customer'],'INCOMING GST REPORT (DETAILED)');
$tblIncomingDetailedHeader = array('SERIAL#','Customer','Shipment #','Shpmt Rcvd Date','Vendor Inv No.','Total Qty','In Tax Ref','Permit No.','Exchange Rate','Vendor Inv Value','Vendor Inv Value S$','FrtCharges (SGD$)','InsCharges (SGD$)','CIF Amount','GST Amount','Origin Port','Forwarders','Vendor /Supplier','HAWB No.','MAWB No.','TptMode','Arrival Date','Delivery To','IncoTerms','JobType','Comment');
PrintRow($ActiveSheet,$tblIncomingDetailedHeader,1,9,$styleArrayHeader,PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
PrintRow($ActiveSheet,$_POST['tblIncomingDetailed'],1,10,'',PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$phpExcel->createSheet();
$ActiveSheet = $phpExcel->setActiveSheetIndex(1);
$ActiveSheet->setTitle("Outgoing GST Detailed");
printHeaderDetails($ActiveSheet,$styleArrayHeader,$_POST['StartDate'],$_POST['EndDate'],$_POST['TaxRef'],$_POST['Customer'],'OUTGOING GST REPORT (DETAILED)');
$tblIncomingSummaryHeader = array('SERIAL#','Customer','Shipment #','Packlist No #','Shipment Shipped','Cust Inv #','Total Qty','Vendor / Supplier','Out Tax Ref','Permit #','Exchange Rate','Dest Port','Forwarders','Consignee','HAWB No.','MAWB No.','Tpt Mode','Dep Date','Delivery From','Inco Terms','Job Type','Comment');
PrintRow($ActiveSheet,$tblIncomingSummaryHeader,1,9,$styleArrayHeader,PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
PrintRow($ActiveSheet,$_POST['tblOutgoingDetailed'],1,10,'',PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$phpExcel->createSheet();
$ActiveSheet = $phpExcel->setActiveSheetIndex(2);
$ActiveSheet->setTitle("Incoming GST Summary");
printHeaderDetails($ActiveSheet,$styleArrayHeader,$_POST['StartDate'],$_POST['EndDate'],$_POST['TaxRef'],$_POST['Customer'],'INCOMING GST REPORT (SUMMARY)');
$tblIncomingSummaryHeader = array('SERIAL#','Customer','Shipment #','Vendor Inv No.','In Tax Ref','Vendor Inv Value USD','Vendor Inv Value S$','Frt Charges(SGD$)','Ins Charges(SGD$)','CIF Amount','GST Amount','Job Type','Comment');
PrintRow($ActiveSheet,$tblIncomingSummaryHeader,1,9,$styleArrayHeader,PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
PrintRow($ActiveSheet,$_POST['tblIncomingSummary'],1,10,'',PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$phpExcel->createSheet();
$ActiveSheet = $phpExcel->setActiveSheetIndex(3);
$ActiveSheet->setTitle("Outgoing GST Summary");
printHeaderDetails($ActiveSheet,$styleArrayHeader,$_POST['StartDate'],$_POST['EndDate'],$_POST['TaxRef'],$_POST['Customer'],'OUTGOING GST REPORT (SUMMARY)');
$tblIncomingSummaryHeader = array('SERIAL#','Customer','Out Ship Reference','Packlist #','Vendor Inv No.','Cust Inv No.','Total Qty','Out Tax Ref','Cust Inv Value USD','Cust Inv Value S$','GST Amount','Job Type','Permit No','HAWB','MAWB','Flight','Comment');
PrintRow($ActiveSheet,$tblIncomingSummaryHeader,1,9,$styleArrayHeader,PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
PrintRow($ActiveSheet,$_POST['tblOutgoingSummary'],1,10,'',PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

function printHeaderDetails($ActiveSheet,$Style,$StartDate,$EndDate,$TaxRef,$Customer,$TabTitle){
	$ActiveSheet->mergeCells("A1:D1");
	$ActiveSheet->setCellValue("A1", $TabTitle);
	$ActiveSheet->getStyle("A1")->applyFromArray($Style); //Title
	
	$ActiveSheet->setCellValue("H1", "Printed: ".date("m/d/Y"));
	
	$ActiveSheet->setCellValue("H2", "Time: ".date("h:i:s a")); 
	
	$ActiveSheet->setCellValue("B3", "Start Date: $StartDate");
	
	$ActiveSheet->setCellValue("E3", "End Date: $EndDate");

	$ActiveSheet->setCellValue("B5", "Job Type: All");
	
	$ActiveSheet->setCellValue("E5", "Customer: $Customer");
	
	$ActiveSheet->setCellValue("B6", "In Tax Ref: $TaxRef");
}

function msort($array, $key, $sort_flags = SORT_REGULAR) {
    if (is_array($array) && count($array) > 0) {
        if (!empty($key)) {
            $mapping = array();
            foreach ($array as $k => $v) {
                $sort_key = '';
                if (!is_array($key)) {
                    $sort_key = $v[$key];
                } else {
                    // @TODO This should be fixed, now it will be sorted as string
                    foreach ($key as $key_key) {
                        $sort_key .= $v[$key_key];
                    }
                    $sort_flags = SORT_STRING;
                }
                $mapping[$k] = $sort_key;
            }
            asort($mapping, $sort_flags);
            $sorted = array();
            foreach ($mapping as $k => $v) {
                $sorted[] = $array[$k];
            }
            return $sorted;
        }
    }
    return $array;
}

function PrintRow($ActiveSheet,$StringArray,$IntAsciiChar,$StartRow,$Style,$Alignment){
	$InitialKeyAscii = $IntAsciiChar;
	$InitialRow			 = $StartRow;
	
	$Serialno				 = 1;
	
	$AsciiChar = $IntAsciiChar;
	foreach($StringArray as $keyStr => $keyValue){
		if (is_array($keyValue)){
			$AsciiChar = $IntAsciiChar;
			foreach($keyValue as $KeyValue1){
				
				if (trim($KeyValue1) != ''){
					if (isValidDateTime($KeyValue1)){
						$KeyValue1 = date("d/m/Y",strtotime($KeyValue1));
					}
					
					if ($AsciiChar == 1){
						$ActiveSheet->setCellValue(getNameFromNumber($AsciiChar).$StartRow,$Serialno);
						$Serialno++;
					} else {
						$ActiveSheet->setCellValue(getNameFromNumber($AsciiChar).$StartRow,$KeyValue1);
						//$Serialno
					}
					
					
					if(is_numeric($KeyValue1) and !is_float($KeyValue1) and $KeyValue1 > 99999){
						$ActiveSheet->getStyle(getNameFromNumber($AsciiChar).$StartRow)->getNumberFormat()->setFormatCode('#0');
					}
				}
				$AsciiChar++;
			}
			$StartRow++;
		} else {
			$ActiveSheet->setCellValue(getNameFromNumber($AsciiChar).$StartRow, $keyValue);
			$AsciiChar++;
		}
	}
	if (isset($Style) and strlen($Style) > 0){
		$ActiveSheet->getStyle(getNameFromNumber($InitialKeyAscii).$InitialRow.':'.getNameFromNumber($AsciiChar).$StartRow)->applyFromArray($Style);
	}
	
	if (isset($Alignment)){
		$ActiveSheet->getStyle(getNameFromNumber($InitialKeyAscii).$InitialRow.':'.getNameFromNumber($AsciiChar).$StartRow)->getAlignment()->setHorizontal($Alignment);
	}
}

function getNameFromNumber($num) {
    $numeric = ($num - 1) % 26;
    $letter = chr(65 + $numeric);
    $num2 = intval(($num - 1) / 26);
    if ($num2 > 0) {
        return getNameFromNumber($num2) . $letter;
    } else {
        return $letter;
    }
}

function isValidDateTime($dateTime){
    if (preg_match("/^(\d{4})-(\d{2})-(\d{2}) ([01][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/", $dateTime, $matches)) {
        if (checkdate($matches[2], $matches[3], $matches[1])) {
            return true;
        }
    }

    return false;
}

$phpExcel->setActiveSheetIndex(0);

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="GSTReport.xls"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel5');
$objWriter->save('php://output');


exit;
?>
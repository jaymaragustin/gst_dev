<?php 
//error_reporting(E_ALL);
//ini_set("display_errors",1);

//add arrival date on the report
session_start();
require ('../../include/referer.checker.php');
require ('../../include/session.checker.php');

#style="border:1px solid #EEEEEE;"

?>

<style>
	.alignme{
		text-align:center;
		white-space: nowrap;
	}
	table.display td{
		white-space: wrap;
		word-wrap:break-word;
	}
	span.totalNos{ color: #CE2929; font-size: 12px; font-weight: bold;}
</style>

<h2><?php include '../../tpl/module_shortcut.php';?>GST Report v2</h2> 
<script src="modules/gstReport/jquery.fileDownload.js">
</script>

<form onsubmit="return false;" class="globalform">
<ol>
	<div style="float:left;">
		<li>
			<label style="width:100px;">Start Date</label>
			<input type="text" class="text" value="" readonly name="startDate" id="startDate" />
		</li>
		<li>
			<label style="width:100px;">End Date</label>
			<input type="text" class="text" value="" readonly name="endDate" id="endDate" />
		</li>
		<li>
			<label style="width:100px;">&nbsp;</label>
		</li>
	</div>
	<div style="float:left; margin-left: 15px;">
		<li>
			<label style="width:100px;">Customer</label>
			<select class="select" id="slcCustomer" name="slcCustomer"></select>
		</li>
		<li>
			<label style="width:100px;">Tax Reference</label>
			<select class="select" id="slcReference" name="slcReference"></select>
			<span><input type="checkbox" id="checkAll"/>all</span>
		</li>
		<li style="display: none;">
			<label style="width:100px;">IN.Shipment Ref#</label>
			<input type="text" class="text" id="shipmentReference" name="shipmentReference"/>
		</li>
		<li id="entry-show" style="display: none;">
			<label style="width:100px;">Entry Number</label>
			<select type="select"  id="entryNumber" name="entryNumber"></select>
		</li>
	</div>
	<div style="float:left; margin-left: 15px;">
		<li><label style="width:1px;">&nbsp;</label><input type="button" class="button" value="Generate Report" id="generateReport" /></li>
		<li><label style="width:1px;">&nbsp;</label><input type="button" class="button" value="Export to Excel" id="exportToExcel" /></li>
		<li><label style="width:1px;">&nbsp;</label></li>
	</div>
</ol>
</form>
<div class="clr"></div>
<br/>
<div id="DivTabs">
	<div id="tabs">
	  <ul>
	      <li><a href="#tabs-1">Incoming GST Detailed</a></li>
	      <li><a href="#tabs-2">Incoming GST Summary</a></li>
	      <li><a href="#tabs-3">Outgoing GST Detailed</a></li>
	      <li><a href="#tabs-4">Outgoing GST Summary</a></li>
	      <li><a href="#tabs-5">Shipment Picklist</a></li>
	      <li><a href="#tabs-6">Shipment Summary</a></li>
	  </ul>
	  <div id="tabs-1">
	     <table class="display" id="tblIncomingDetailed"></table>
	  </div>
	  <div id="tabs-2">
	     <table class="display" id="tblIncomingSummary"></table>
	  </div>
	  <div id="tabs-3">
			<table class="display" id="tblOutgoingDetailed"></table> 
	  </div>
	  <div id="tabs-4">
			<table class="display" id="tblOutgoingSummary"></table>
	  </div>
	  <div id="tabs-5">
			<table class="display" id="tblPicklist"></table>
	  </div>
	  <div id="tabs-6">
	  <input type="text" class="text" id="txtSReference" placeholder="Reference #" style="width:100px;">
	  <input type="text" class="text" id="txtSEntry" placeholder="SU/Entry Number" style="width:100px;">
	  <input type="text" class="text" id="txtSTaxScheme" placeholder="Tax Scheme" style="width:100px;">
	  <input type="text" class="text" id="txtSIncoming" placeholder="Incoming" style="width:100px;">
	  <input type="text" class="text" id="txtSOutgoing" placeholder="Outgoing" style="width:100px;">
	  <input type="text" class="text" id="txtSBalance" placeholder="Balance" style="width:100px;">
			<table class="display" id="tblShipmentSummary"></table>
	  </div>
	</div>
</div>
<script>
ErrorKeys = new Object();
ErrorKeys['First'] = 'Beginning of the data';
ErrorKeys['Previous'] = 'Beginning of the data';
ErrorKeys['Last'] = 'Last data';
ErrorKeys['Next'] = 'Last data';
ErrorKeys['Search'] = 'Sorry invalid Shipment Number';
ErrorKeys['nodata'] = 'Sorry no available Reference Number';
ErrorKeys['ErrorSearchingShipment'] = 'Shipment Number is a required field in order to Search.';

var TaxReferenceList = new Object(); // get from masterdata - TaxScheme
var CustomerList = new Object();
var CurrentDate = new Date();

var tblIncomingDetailed;
var tblIncomingSummary;
var tblOutgoingDetailed;
var tblOutgoingSummary;
var tblPicklist;
var tblShipmentSummary;

$(function() {
	$("input:submit, input:button, button, .button").button();
	//$('#DivTabs').hide();
	
	$( "#startDate" ).datepicker({		
			dateFormat: 'mm/dd/yy',
	    defaultDate: "+1w",
	    changeMonth: true,
	    onClose: function( selectedDate ) {
	        $( "#endDate" ).datepicker( "option", "minDate", selectedDate );
	    }
	});
	
	$( "#endDate" ).datepicker({
			dateFormat: 'mm/dd/yy',
	    defaultDate: "+1w",
	    changeMonth: true,
	    onClose: function( selectedDate ) {
	        $( "#startDate" ).datepicker( "option", "maxDate", selectedDate );
	    }
	});

	tblIncomingDetailed = $('#tblIncomingDetailed').dataTable({
			"sDom": '<"H">Tlfr<"F"tip>',
			"oTableTools": {
				"aButtons": [
					{
						"sExtends":    "collection",
						"sButtonText": "Save As",
						"aButtons":    [ "xls" ]
					}
				]
			},
			'bFilter': true,
			'bSortable' : false,
			//'aaSortingFixed': [[3,'asc'],[2,'asc']],
			'bPaginate': true,
			'bJQueryUI': true,
			'aLengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
			'sPaginationType': 'full_numbers',
			"bScrollCollapse": true,  
			"sScrollX":"100%",    
			"bDestroy": true,
			"aoColumns": [
				{"sTitle":"S/No","sWidth":"80px","sClass":"alignme"},
				{"sTitle":"Customer","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Shipment #","sWidth":"90px","sClass":"alignme"},
				{"sTitle":"Shpmt Rcvd Date","sWidth":"130px","sClass":"alignme"},
				{"sTitle":"VendorInvNo.","sWidth":"120px","sClass":"alignme"},
				//{"sTitle":"Total Inv No.","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Total Qty","sWidth":"90px","sClass":"alignme"},
				{"sTitle":"Tax Reference","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Permit No.","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Exchange Rate","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Vendor Inv Value (USD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Vendor Inv Value (SGD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"FrtCharges (SGD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"InsCharges (SGD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"CIF Amount (SGD)","sWidth":"90px","sClass":"alignme"},
				{"sTitle":"GST Amount (SGD)","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Origin Port","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Forwarders","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Vendor /Supplier","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"HAWB No.","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"MAWB No.","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"TptMode","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Arrival Date","sWidth":"110px","sClass":"alignme"},
				{"sTitle":"Delivery To","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"IncoTerms","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"JobType","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Comment","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Incoming Reference","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Description","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"SU/Entry#.","sWidth":"120px","sClass":"alignme"}
			]
		});

	tblIncomingSummary = $('#tblIncomingSummary').dataTable({
			"sDom": '<"H">Tlfr<"F"tip>',
			"oTableTools": {
				"aButtons": [
					{
						"sExtends":    "collection",
						"sButtonText": "Save As",
						"aButtons":    [ "xls" ]
					}
				]
			},
			'bFilter': true,
			'bPaginate': true,
			'bJQueryUI': true,
			'aLengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
			'sPaginationType': 'full_numbers',
			"bScrollCollapse": true,  
			"sScrollX":"100%",    
			"bDestroy": true,
			"aoColumns": [
				{"sTitle":"S/No","sWidth":"80px","sClass":"alignme"},
				{"sTitle":"Customer","sWidth":"110px","sClass":"alignme"},
				{"sTitle":"Shipment #:","sWidth":"110px","sClass":"alignme"},
				{"sTitle":"Shpmt Rcvd Date","sWidth":"130px","sClass":"alignme"},
				{"sTitle":"Vendor /Supplier","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Vendor Inv No.","sWidth":"140px","sClass":"alignme"},
				//{"sTitle":"Total Inv No.","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Tax Reference","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Permit No.","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Vendor Inv Value (USD)","sWidth":"160px","sClass":"alignme"},
				{"sTitle":"Vendor Inv Value (SGD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Frt Charges (SGD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Ins Charges (SGD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"CIF Amount (SGD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"GST Amount (SGD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Job Type","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Comment","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Description","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"SU/Entry#.","sWidth":"120px","sClass":"alignme"}
			]
		});

	tblOutgoingDetailed = $('#tblOutgoingDetailed').dataTable({
			"sDom": '<"H">Tlfr<"F"tip>',
			"oTableTools": {
				"aButtons": [
					{
						"sExtends":    "collection",
						"sButtonText": "Save As",
						"aButtons":    [ "xls" ]
					}
				]
			},
			'bFilter': true,
			'bSortable' : false,
			//'aaSortingFixed': [[4,'asc'],[2,'asc']],
			'bPaginate': true,
			'bJQueryUI': true,
			'aLengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
			'sPaginationType': 'full_numbers',
			"bScrollCollapse": true,  
			"sScrollX":"100%",    
			"bDestroy": true,
			"aoColumns": [
				{"sTitle":"S/No","sWidth":"80px","sClass":"alignme"},
				{"sTitle":"Customer","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Shipment Reference #","sWidth":"90px","sClass":"alignme"},
				{"sTitle":"Packlist No #","sWidth":"130px","sClass":"alignme"},
				{"sTitle":"Shipment Shipped.","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Invoice.","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Cust Inv #","sWidth":"200px","sClass":"alignme"},
				{"sTitle":"Total Qty","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Vendor / Supplier","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"In Tax Ref / Out Tax Ref","sWidth":"180px","sClass":"alignme"},
				{"sTitle":"Permit #","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Exchange Rate","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Dest Port","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Forwarders","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Consignee","sWidth":"180px","sClass":"alignme"},
				{"sTitle":"HAWB No.","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"MAWB No.","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Tpt Mode","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Dep Date","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Delivery From","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Inco Terms","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Job Type","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Description","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Cust Inv Value (USD)","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Cust Inv Value (SGD)","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"GST Amount (SGD)","sWidth":"100px","sClass":"alignme"}
			]
		});
		
	tblOutgoingSummary = $('#tblOutgoingSummary').dataTable({
			"sDom": '<"H">Tlfr<"F"tip>',
			"oTableTools": {
				"aButtons": [
					{
						"sExtends":    "collection",
						"sButtonText": "Save As",
						"aButtons":    [ "xls" ]
					}
				]
			},
			'bFilter': true,
			'bPaginate': true,
			'bJQueryUI': true,
			'aLengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
			'sPaginationType': 'full_numbers',
			"bScrollCollapse": true,  
			"sScrollX":"100%",    
			"bDestroy": true,
			"aoColumns": [
				{"sTitle":"S/No","sWidth":"80px","sClass":"alignme"},
				{"sTitle":"Customer","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Shipment Reference #","sWidth":"170px","sClass":"alignme"},
				{"sTitle":"Packlist No #","sWidth":"130px","sClass":"alignme"},
				{"sTitle":"Cust Inv No.","sWidth":"200px","sClass":"alignme"},
				{"sTitle":"Total Qty","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"In Tax Ref / Out Tax Ref","sWidth":"180px","sClass":"alignme"},
				{"sTitle":"Cust Inv Value (USD)","sWidth":"180px","sClass":"alignme"},
				{"sTitle":"Cust Inv Value (SGD)","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"GST Amount (SGD)","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Job Type","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Permit No","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"HAWB","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"MAWB","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Flight","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Description","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Shipment Shipped Date","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Consignee","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Departure Date","sWidth":"120px","sClass":"alignme"}
			]
		});

	tblPicklist = $('#tblPicklist').dataTable({
			"sDom": '<"H">Tlfr<"F"tip>',
			"oTableTools": {
				"aButtons": [
					{
						"sExtends":    "collection",
						"sButtonText": "Save As",
						"aButtons":    [ "xls" ]
					}
				]
			},
			'bFilter': true,
			'bSortable' : false,
			//'aaSortingFixed': [[3,'asc'],[2,'asc']],
			'bPaginate': true,
			'bJQueryUI': true,
			'aLengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
			'sPaginationType': 'full_numbers',
			"bScrollCollapse": true,  
			"sScrollX":"100%",    
			"bDestroy": true,
			"aoColumns": [
				{"sTitle":"#","sWidth":"80px","sClass":"alignme"},
				{"sTitle":"IN Shipment Ref #","sWidth":"80px","sClass":"alignme"},
				{"sTitle":"Outgoing Picklist #","sWidth":"80px","sClass":"alignme"},
				{"sTitle":"Tax Scheme","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Box Entry/SU #","sWidth":"90px","sClass":"alignme"},
				{"sTitle":"Total Qty","sWidth":"120px","sClass":"alignme"}
			]
		});
	$('#tblShipmentSummary').append("<tfoot><tr><td style='text-align:center'>#</td><td style='text-align:center'>Reference #</td><td style='text-align:center'>SU/Entry Number</td><td style='text-align:center'>Tax Scheme</td><td style='text-align:center'>Incoming</td><td style='text-align:center'>Outgoing</td><td style='text-align:center'>Balance</td></tr></tfoot>")
	tblShipmentSummary = $('#tblShipmentSummary').dataTable({
			"sDom": '<"H">Tlfr<"F"tip>',
			"oTableTools": {
				"aButtons": [
					{
						"sExtends":    "collection",
						"sButtonText": "Save As",
						"aButtons":    [ "xls" ]
					}
				]
			},
			'bFilter': true,
			'bSortable' : false,
			//'aaSortingFixed': [[3,'asc'],[2,'asc']],
			'bPaginate': true,
			'bJQueryUI': true,
			'aLengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
			'sPaginationType': 'full_numbers',
			"bScrollCollapse": true,  
			"sScrollX":"100%",    
			"bDestroy": true,
			fnFooterCallback: function ( nRow, aaData, iStart, iEnd, aiDisplay ) {
				var iTotal = 0;
				var iTotal2 = 0;
				for ( var i=iStart ; i<iEnd ; i++ )	{ 

					iTotal += aaData[ aiDisplay[i] ][4] != 0 ? aaData[ aiDisplay[i] ][4] : 0; 
					iTotal2 += aaData[ aiDisplay[i] ][4] != 0 ? aaData[ aiDisplay[i] ][5] : 0; 
				}
				
				var nCells = nRow.getElementsByTagName('td');
				nCells[4].innerHTML = 'Total Incoming: <span class="totalNos">' + iTotal+'</span>';
				nCells[5].innerHTML = 'Total Outgoing: <span class="totalNos">' + iTotal2+'</span>';				
			},
			"aoColumns": [
				{"sTitle":"#","sWidth":"80px","sClass":"alignme"},
				{"sTitle":"Reference #","sWidth":"80px","sClass":"alignme"},
				{"sTitle":"SU/Entry Number","sWidth":"80px","sClass":"alignme"},
				{"sTitle":"Tax Scheme","sWidth":"90px","sClass":"alignme"},
				{"sTitle":"Incoming","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Outgoing","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Balance","sWidth":"120px","sClass":"alignme"}
			],
			fnRowCallback: function( nRow, aData, iDisplayIndex ) {
				$('td:eq(0)', nRow).attr('field','#');
				$('td:eq(1)', nRow).attr('field','Reference #');
				$('td:eq(2)', nRow).attr('field','SU/Entry Number');
				$('td:eq(3)', nRow).attr('field','Tax Scheme');
				$('td:eq(4)', nRow).attr('field','Incoming');
				$('td:eq(5)', nRow).attr('field','Outgoing');
				$('td:eq(6)', nRow).attr('field','Balance');
				return nRow;
			},
			fnPreDrawCallback: function(oSettings) {						
				$('#scanTable_filter input').attr('placeholder','Search');
			},
			fnDrawCallback: function( oSettings ) {
				
			}	
		});

	$("#tabs").tabs({
		select: function(event, ui) {
			var selected = ui.panel.id;
			switch(selected){
				case 'tabs-1':
					var t = setTimeout(function(){$('#tblIncomingDetailed').dataTable().fnAdjustColumnSizing();},1);
				break;				
				case 'tabs-2':
					var t = setTimeout(function(){$('#tblIncomingSummary').dataTable().fnAdjustColumnSizing();},1);
				break;
				case 'tabs-3':
					var t = setTimeout(function(){$('#tblOutgoingDetailed').dataTable().fnAdjustColumnSizing();},1);
				break;
				case 'tabs-4':
					var t = setTimeout(function(){$('#tblOutgoingSummary').dataTable().fnAdjustColumnSizing();},1);
				break;	
				case 'tabs-5':
					var t = setTimeout(function(){$('#tblPicklist').dataTable().fnAdjustColumnSizing();},1);
				case 'tabs-6':
					var t = setTimeout(function(){$('#tblShipmentSummary').dataTable().fnAdjustColumnSizing();},1);
				break;					
			} 
		}
	});	
	jQuery.fn.exists = function(){return this.length>0;}
	jQuery.isJson = function(str) {
	 if (jQuery.trim(str) == '') return false;
	 str = str.replace(/\\./g, '@').replace(/"[^"\\\n\r]*"/g, '');
	 return (/^[,:{}\[\]0-9.\-+Eaeflnr-u \n\r\t]*$/).test(str);
	}
	
	Number.prototype.padLeft = function(base,chr){
	    var  len = (String(base || 10).length - String(this).length)+1;
	    return len > 0? new Array(len).join(chr || '0')+this : this;
	}

	CurrentDate = [(CurrentDate.getMonth() + 1).padLeft(),CurrentDate.getDate().padLeft(),CurrentDate.getFullYear()].join('/');
	$('#startDate').val(CurrentDate);
	$('#endDate').val(CurrentDate);
	
	/*
	var astr = '';
	$.each(tblIncomingDetailed.fnSettings().aoColumns,function(a,b){
		astr = astr + "'" + b.sTitle + "',";
	});
	console.log(astr); */

	function NASort(a, b) {    
	    if (a.innerHTML == 'NA') {
	        return 1;   
	    }
	    else if (b.innerHTML == 'NA') {
	        return -1;   
	    }       
	    return (parseInt(a.innerHTML) > parseInt(b.innerHTML)) ? 1 : -1;
	};

	function ProcessRequest(data,gotoProcess){
		$.ajax({
		 type: 'POST',
		 url: 'modules/gstReportV2/GSTReportClass.php',
		 data: data,
		 success: function(data){
				var CallMeBaby = eval('(' + gotoProcess + ')');
						CallMeBaby(data);
		 },
		 beforeSend: function(){
				$.dimScreen(function() {
					$('#content').fadeIn();
				});
		 },
		 complete: function(){
			 $.dimScreenStop();
		 }
		});
	}
	

	function msgbox(msg,focusField){
		$( "#dialog-msgbox:ui-dialog" ).dialog( "destroy" );
		$( "#dialog-msgbox" ).html(msg);
		$( "#dialog-msgbox" ).attr("title",'GST Report');
		$( "#dialog-msgbox" ).dialog('option', 'position', 'center');
		$( "#dialog-msgbox" ).dialog({
			modal: true,
			height:'auto',
			show:{ 
				effect:"blind", 
				speed:'slow' 
			},
			hide:'blind',
			buttons: {
				Ok: function() {
					$(this).dialog( "close" );
				}
			}
		});
	}
	
	function PrepareForm(obj){
		
		$.each(obj[0],function(a,b){
			//b[0] - wawiindex
			//b[1] - wawialias
			//CurrentAvaiLablePicklistPerCustomer[b[0]['PackListNumber']] = [];
			CustomerList[b[0]] = b[1];
		});
		TaxReferenceList['All'] = ['All'];
		$.each(obj[1],function(a,b){
			TaxReferenceList[a] = [];
			$.each(b,function(c,d){
				$.each(d,function(f,g){
					TaxReferenceList[a].push(g);
				});
			});
		});

		$('#slcCustomer')
		   .append($("<option></option>")
		   .attr("value",'All')
		   .text('All'));

		$.each(CustomerList,function(a,b){
	     $('#slcCustomer')
	         .append($("<option></option>")
	         .attr("value",a)
	         .text(b)); 
		});
		$('#slcCustomer').change();
	}
	
	function generateReport(objAll){
		var obj = new Object(); //Incoming Data
		var obj1= new Object(); //Outgoing Data
		var tmpArray;
		var i = 1;
		
		tblIncomingDetailed.fnClearTable();
		tblIncomingSummary.fnClearTable();
		obj = objAll[0];
		
		if (obj != 'nodata'){
			tmpArray = [];
			$('#tabs > ul').tabs({ selected: 1 });
			$.each(obj[0],function(a,c){
				$.each(c,function(d,b){
					//tmpArray.push([i,b['WawiAlias'],b['ShipReference'],b['ArrivalDate'],b['VendorInvNo'],b['totalInvoice'],b['Quantity'],b['InTaxRef'],b['PermitNo'],b['ExchangeRate'],parseFloat(b['InvoiceValue']).toFixed(2),parseFloat(b['InvoiceValueSGD']).toFixed(2),parseFloat(b['FreightCharges']).toFixed(2),parseFloat(b['InsCharges']).toFixed(2),parseFloat(b['CIFAmount']).toFixed(2),parseFloat(b['GSTAmount']).toFixed(2),b['OriginPort'],b['Forwarders'],b['Supplier'],b['HAWB'],b['MAWB'],b['TptMode'],b['ArrivalDate'],b['JSI'],b['INCOTERM'],b['JobType'],b['Comment'],b['IncomingReference'],b['Description'],b['ReceiptDate']]);
					var customer = '';
					if(b['WawiAlias'] == 'ADI'){
						customer = 'ADI 3rd Floor';
					}else if(b['WawiAlias'] == 'ADI 3rd Floor'){
						customer = 'ADI 3rd Floor';
					}else{
						customer = b['WawiAlias']
					}
					tmpArray.push([i,customer,b['ShipReference'],b['ArrivalDate'],b['VendorInvNos'],b['suQuantity']/*b['Quantity']*/,b['InTaxRef'],b['PermitNo'],b['ExchangeRate'],parseFloat(b['InvoiceValue1']).toFixed(2),parseFloat(b['InvoiceValueSGD2']).toFixed(2),parseFloat(b['FreightCharges']).toFixed(2),parseFloat(b['InsCharges']).toFixed(2),parseFloat(b['CIFAmount2']).toFixed(2),parseFloat(b['GSTAmount2']).toFixed(2),b['OriginPort'],b['Forwarders'],b['Supplier'],b['HAWB'],b['MAWB'],b['TptMode'],b['ArrivalDate'],b['JSI'],b['INCOTERM'],b['JobType'],b['Comment'],b['IncomingReference'],b['Description'],b['EntrySU']]);
					i++;
				});
			});
			tblIncomingDetailed.fnAddData(tmpArray);
			
			$('#tabs > ul').tabs({ selected: 2 });
			tmpArray = [];
			i = 1;
			$.each(obj[1],function(a,c){
				$.each(c,function(d,b){
					//tmpArray.push([i,b['Customer'],b['ShipReference'],b['ArrivalDate'],b['Supplier'],b['VendorInvNo'],b['totalInvoice'],b['InTaxRef'],b['PermitNo'],parseFloat(b['InvoiceValue']).toFixed(2),parseFloat(b['InvoiceValueSGD']).toFixed(2),parseFloat(b['FreightCharges']).toFixed(2),parseFloat(b['InsCharges']).toFixed(2),parseFloat(b['CIFAmount']).toFixed(2),parseFloat(b['GSTAmount']).toFixed(2),b['JobType'],b['Comments'],b['Description']]);
					var customer = '';
					if(b['Customer'] == 'ADI'){
						customer = 'ADI 3rd Floor';
					}else if(b['Customer'] == 'ADI 3rd Floor'){
						customer = 'ADI 3rd Floor';
					}else{
						customer = b['Customer']
					}
					tmpArray.push([i,customer,b['ShipReference'],b['ArrivalDate1'],b['Supplier'],b['VendorInvNo2'],b['InTaxRef'],b['PermitNo'],parseFloat(b['InvoiceValue']).toFixed(2),parseFloat(b['InvoiceValueSGD']).toFixed(2),parseFloat(b['FreightCharges']).toFixed(2),parseFloat(b['InsCharges']).toFixed(2),parseFloat(b['CIFAmount']).toFixed(2),parseFloat(b['GSTAmount']).toFixed(2),b['JobType'],b['Comments'],b['Description'],b['EntrySU']]);
					i++;
				});
			});
			tblIncomingSummary.fnAddData(tmpArray);
		}

		obj1 = objAll[1];
		tblOutgoingDetailed.fnClearTable();
		tblOutgoingSummary.fnClearTable();
		
		if (obj1 != 'nodata'){
			tmpArray = [];
			i = 1;
			$.each(obj1[0],function(c,d){
				$.each(d,function(e,f){
					$.each(f,function(a,b){
						tmpArray.push([i,b['WawiIndex'],b['OutShipReference'],b['PacklistNumber'],b['ShipDate1'],b['Invoice'],b['CustInvNo1'],b['TotalQty'],b['Supplier'],b['TaxScheme'],b['PermitNo'],b['ExchangeRate'],b['Destination'],b['Forwarders'],b['CustomerName1'],b['HAWB'],b['MAWB'],b['Flight'],b['DepDate1'],'JSI',b['IncoTerms'],b['JobType'],b['Remarks'],parseFloat(b['CustInvValue']).toFixed(2),parseFloat(b['InvoiceValueSGD']).toFixed(2),parseFloat(b['GstAmount']).toFixed(2)]);
						i++;
					});
				});
			});
			tblOutgoingDetailed.fnAddData(tmpArray);
			
			tmpArray = [];
			i = 1;
			
			
			var outSR = '';
			var tax = '';
			$.each(obj1[1],function(c,d){
				$.each(d,function(a,b){
					console.log(b['ShipDate1']);
					if(outSR == b['OutShipReference']){
						outSR = b['OutShipReference'];
						tmpArray.push([i,b['WawiIndex'],b['OutShipReference'],b['PacklistNumber'],b['CustInvNo1'],b['TotalQty'],b['TaxScheme'],'','','',b['JobType'],b['PermitNo'],b['HAWB'],b['MAWB'],b['Flight'],b['Remarks'],b['ShipDate1'],b['CustomerName1'],b['DepDate']]);
					}else{
						outSR = b['OutShipReference'];
						tmpArray.push([i,b['WawiIndex'],b['OutShipReference'],b['PacklistNumber'],b['CustInvNo1'],b['TotalQty'],b['TaxScheme'],b['TotalCustInvValueUSD'],b['CustInvValueSGD'],b['GSTAmount'],b['JobType'],b['PermitNo'],b['HAWB'],b['MAWB'],b['Flight'],b['Remarks'],b['ShipDate1'],b['CustomerName1'],b['DepDate']]);
					}
					
					i++;
				});
			});
			tblOutgoingSummary.fnAddData(tmpArray);
			
		}

		obj2 = objAll[2];

		tblPicklist.fnClearTable();
		if(obj2 != 'nodata'){
			tmpArray = [];
			i = 1;
			$.each(obj2,function(c,d){
				$.each(d,function(x,v){
					tmpArray.push([i,v['ShipReference'],v['PackListNumber'],v['InTaxRef'],v['EntryNumber'],v['Quantity']]);
						i++;
				});
			});
			tblPicklist.fnAddData(tmpArray);
		}

		obj3 = objAll[3];

		tblShipmentSummary.fnClearTable();
		if(obj3 != 'nodata'){
			tmpArray = [];
			i = 1;
			$.each(obj3,function(c,d){
				$.each(d,function(x,v){
					tmpArray.push([i,v['ShipReference'],v['EntryNumber'],v['InTaxRef'],v['TotalIncoming'],v['TotalOutgoing'],v['RemainingBalance']]);
						i++;
				});
			});
			tblShipmentSummary.fnAddData(tmpArray);
		}
	}

	$('#shipmentReference').keyup(function(event){
	
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if(keycode == '13'){
			$('#entry-show').css({'display':''});
			ProcessRequest('todo=getEntryByReference&shipmentReference='+$('#shipmentReference').val()+'&customer='+$('#slcCustomer').val(),'displayEntry');
		}

	});
	
	function displayEntry(data){
		$('#entryNumber').empty();
		$('#entryNumber').append($('<option>', { 
		        value: 'All',
		        text : 'All'
		    }));
		if(data != 'nodata'){
			$.each(data, function (i, item) {
			
			    $('#entryNumber').append($('<option>', { 
			        value: item.EntryNumber,
			        text : item.EntryNumber
			    }));
			});
		}
		
	}

	$('#slcCustomer').change(function(){
		$('#slcReference').empty();
		if (TaxReferenceList[$('#slcCustomer').val()] != 'No TaxScheme' && TaxReferenceList[$('#slcCustomer').val()].length > 1){
	     $('#slcReference')
	         .append($("<option></option>")
	         .attr("value",'All')
	         .text('All'));
		}
		
		$.each(TaxReferenceList[$('#slcCustomer').val()],function(a,b){
	     $('#slcReference')
	         .append($("<option></option>")
	         .attr("value",b)
	         .text(b));
		});
	});
	
	$('#generateReport').click(function(){
		var check = 'false';
		if ($('#checkAll').is(":checked"))
		{
			check = 'true';
			if($('#slcCustomer').val() != 'All'){
				ProcessRequest({'todo':'generateReport','startDate':$('#startDate').val(),'endDate':$('#endDate').val(),'slcCustomer':$('#slcCustomer').val(),'slcReference':$('#slcReference').val(),'ShipReference':$('#txtSReference').val(),'EntryNumber':$('#txtSEntry').val(),'TaxScheme':$('#txtSTaxScheme').val(),'Incoming':$('#txtSIncoming').val(),'Outgoing':$('#txtSOutgoing').val(),'Balance':$('#txtSBalance').val(),'Search':check},'generateReport');
			}else{
				alert('Please choose customer');
			}
		}
		else{
			ProcessRequest({'todo':'generateReport','startDate':$('#startDate').val(),'endDate':$('#endDate').val(),'slcCustomer':$('#slcCustomer').val(),'slcReference':$('#slcReference').val(),'ShipReference':$('#txtSReference').val(),'EntryNumber':$('#txtSEntry').val(),'TaxScheme':$('#txtSTaxScheme').val(),'Incoming':$('#txtSIncoming').val(),'Outgoing':$('#txtSOutgoing').val(),'Balance':$('#txtSBalance').val(),'Search':check},'generateReport');
		}

		console.log(check);
		
	});
	
	$('#exportToExcel').click(function(e){
		var tblIncomingDetailedCount = tblIncomingDetailed.fnGetData().length;
		var tblIncomingSummaryCount  = tblIncomingSummary.fnGetData().length;
		var tblOutgoingDetailedCount = tblOutgoingDetailed.fnGetData().length;
		var tblOutgoingSummaryCount	 = tblOutgoingSummary.fnGetData().length;
		var tblPicklistCount	 	 = tblPicklist.fnGetData().length;
		var tblShipmentSummaryCount	 = tblShipmentSummary.fnGetData().length;
		
		if (tblIncomingDetailedCount == 0 && tblIncomingSummaryCount == 0 && tblOutgoingDetailedCount == 0 && tblOutgoingSummaryCount == 0 && tblPicklistCount == 0 && tblShipmentSummaryCount == 0){
			msgbox('No data to Export');
		} else {
			$.fileDownload('modules/gstReport/ExportToExcel.php', { httpMethod : "POST", data: { StartDate : $('#startDate').val(), EndDate : $('#endDate').val(), TaxRef : $('#slcReference').val(), Customer : $('#slcCustomer').val(), tblIncomingDetailed : tblIncomingDetailed.fnGetData(),tblIncomingSummary : tblIncomingSummary.fnGetData(),tblOutgoingDetailed : tblOutgoingDetailed.fnGetData(),tblOutgoingSummary : tblOutgoingSummary.fnGetData(),tblPicklistCount:tblPicklist.fnGetData(),tblShipmentSummaryCount:tblShipmentSummary.fnGetData() }});
		}
		
	});
	
	ProcessRequest({'todo':'PrepareForm'},'PrepareForm');
	
});	
</script>
<?php
//error_reporting(-1);
//ini_set('display_errors', 1);
//ini_set('memory_limit', '1024M');

//set_time_limit(0);
//ini_set('max_execution_time', '0');

if (isset($_POST['todo'])){
	require ('../../include/config.php');
	$GSTReportClass = new GSTReportClass($_REQUEST);
	
	
	header('Content-Type: application/json');
	if ($_POST['todo'] == 'generateReportViaMail'){
		echo $GSTReportClass->generateReportViaMail();
	}elseif($_POST['todo'] == 'generateIncomingDetailed'){
		echo $GSTReportClass->generateIncomingDetailed();
	}elseif($_POST['todo'] == 'generateIncomingSummary'){
		echo $GSTReportClass->generateIncomingSummary();
	}elseif($_POST['todo'] == 'generateOutgoingDetailed'){
		echo $GSTReportClass->generateOutgoingDetailed();
	}elseif($_POST['todo'] == 'generateOutgoingSummary'){
		echo $GSTReportClass->generateOutgoingSummary();
	}elseif($_POST['todo'] == 'generateShipmentPicklist'){
		echo $GSTReportClass->generateShipmentPicklist();
	}elseif($_POST['todo'] == 'generateShipmentSummary'){
		echo $GSTReportClass->generateShipmentSummary();
	}
}

Class GSTReportClass{
	var $PostVars;
	function __construct($vars){
		$this->PostVars = $vars;
		$serverConn = unserialize(base64_decode(SQL_CONN));
		$this->conn = mssql_connect($serverConn[0],$serverConn[1],$serverConn[2]);
		mssql_select_db("Import");
		
		foreach($this->PostVars as $a => $b){
			$this->PostVars[$a] = $this->mssqlQuotedString($b);
		}
		register_shutdown_function(array($this, 'callRegisteredShutdown'));
	}
	
    public function callRegisteredShutdown() {
		//$a = error_get_last();
        //var_dump($a);
        //var_dump('-----------------------------------------------------');
		//die();
    }
	
	function mssqlQuotedString($str){
		$str = str_replace("\\", "\\\\", $str);
		$str = str_replace("'", "''", $str);
		
		return $str;
	}
	
	function array_orderby(){
	    $args = func_get_args();
	    $data = array_shift($args);
	    foreach ($args as $n => $field) {
	        if (is_string($field)) {
	            $tmp = array();
	            foreach ($data as $key => $row)
	                $tmp[$key] = $row[$field];
	            $args[$n] = $tmp;
	            }
	    }
	    $args[] = &$data;
	    call_user_func_array('array_multisort', $args);
	    return array_pop($args);
	}
	
	function mssql_escape($str) {
		return str_replace("'", "''", $str);
	}
	
	function AnalyzeSQL($sql){
		if (mssql_num_rows($sql) > 0){
			while($row[] = mssql_fetch_assoc($sql)){}
			array_pop($row);
		} else {
			$row = 'nodata';
		}
		
		return $row;
	}

	function GSTRate(){
		$sql = mssql_query("SELECT Value FROM Setup WHERE Entry='GST'");
		if (mssql_num_rows($sql) > 0){
			$sql = mssql_fetch_assoc($sql);
			$GSTRate = $sql['Value'];
		} else {
			$GSTRate = 7;
		}
		
		return $GSTRate;
	}
	
	function generateReportViaMail(){
		mssql_select_db("Import");
		extract($this->PostVars);
		
		$result = array('success'=>true,'message'=>'','fromProcess'=>trim($fromProcess));
		
		$chkIncomingDetailed	= (trim($chkIncomingDetailed) == "true") ? 1:0;
		$chkIncomingSummary		= (trim($chkIncomingSummary) == "true") ? 1:0;
		$chkOutgoingDetailed	= (trim($chkOutgoingDetailed) == "true") ? 1:0;
		$chkOutgoingSummary		= (trim($chkOutgoingSummary) == "true") ? 1:0;
		$chkShipmentPicklist	= (trim($chkShipmentPicklist) == "true") ? 1:0;
		$chkShipmentSummary		= (trim($chkShipmentSummary) == "true") ? 1:0;
		
		$searchColumn	= trim($searchColumn);
		$searchCon		= trim($searchCon);
		$searchInput1	= ($searchColumn == 'Date') ? date('Y/m/d', strtotime(str_replace('/', '-', trim($searchInput1)))):trim($searchInput1);
		//$searchInput2	= ($searchCon != 'between') ? '':trim($searchInput2);
		$searchInput2	= trim($searchInput2);
		if($searchCon == 'between'){
			$searchInput2	= ($searchColumn == 'Date') ? date('Y/m/d', strtotime(str_replace('/', '-', trim($searchInput2)))):trim($searchInput2);
		}
		$SearchValue	= trim($searchmainValue);
		$SearchBy		= trim($UserID);
		$SearchDate		= date('Y-m-d H:i:s');
		
		$getUserData = mssql_query("SELECT TOP 1 * FROM [MasterData].[dbo].[User] WHERE [Username] = '".$SearchBy."'");
		if (mssql_num_rows($getUserData) > 0){
			$userData = mssql_fetch_assoc($getUserData);
			$emailReceiver = $userData['EMail'];
		} else {
			$emailReceiver = 'globaldev@omnilogistics.com';
		}
		
		$insertRequest = "INSERT INTO ReportEmailRequest  ([filterColumn]
												  ,[filterCondition]
												  ,[filterSearch1]
												  ,[filterSearch2]
												  ,[filterFrom]
												  ,[userEmail]
												  ,[IncomingDetailed]
												  ,[IncomingSummary]
												  ,[OutgoingDetailed]
												  ,[OutgoingSummary]
												  ,[ShipmentPicklist]
												  ,[ShipmentSummary]
												  ,[UserID]
												  ,[EnterDate])
				  VALUES ('".$searchColumn."',
						  '".$searchCon."',
						  '".$searchInput1."',
						  '".$searchInput2."',
						  '".$SearchValue."',
						  '".$emailReceiver."',
						  '".$chkIncomingDetailed."',
						  '".$chkIncomingSummary."',
						  '".$chkOutgoingDetailed."',
						  '".$chkOutgoingSummary."',
						  '".$chkShipmentPicklist."',
						  '".$chkShipmentSummary."',
						  '".$SearchBy."',
						  '".$SearchDate."');";
		
		if (mssql_query($insertRequest)){
			$result['success'] = true;
			if(trim($fromProcess) == 'dateexceed'){
				$result['message'] = 'Daterange exceeded to 1 week. Request will be sent via Email.';
			} else {
				$result['message'] = 'Reports cannot load all data. Full request will be sent via Email.';
			}
		} else {
			$result['success'] = true;
			if(trim($fromProcess) == 'dateexceed'){
				$result['message'] = 'Failed to insert request for email. Please try again.';
			} else {
				$result['message'] = 'Failed to insert request for email. System will display only the loaded data.';
			}
		}
		
		return json_encode($result);
	}
	
	function generateIncomingDetailed(){
		
		extract($this->PostVars);
		mssql_select_db("ADISGWMS");
		$data = array();
		
		$searchColumn	= trim($searchColumn);
		$searchCon		= trim($searchCon);
		$searchInput1	= ($searchColumn == 'Date') ? date('Y/m/d', strtotime(str_replace('/', '-', trim($searchInput1)))):trim($searchInput1);
		if($searchCon == 'between'){
			$searchInput2	= ($searchColumn == 'Date') ? date('Y/m/d', strtotime(str_replace('/', '-', trim($searchInput2)))):trim($searchInput2);
		}
		
		$incDetailedColumn = array(
			'VendorInvNo'	=> 'VendorInvNos',
			'TaxScheme'		=> 'InTaxRef',
			'HAWB'			=> 'HAWB',
			'Date'			=> 'SearchDate',
			'Permit'		=> 'PermitNo',
			'PartNoDesc'	=> 'Description',
			'ShipmentNo'	=> 'ShipReference',
			'SUNumber'		=> 'EntrySU'
		);
		//'Date'			=> 'ArrivalDate',
		
		$SearchValue		= trim($searchmainValue);
		$SearchValueFilter	= "";
		if($searchColumn == 'TaxScheme' || ($searchColumn != 'TaxScheme' && $searchCon == 'notequalto') || ($searchColumn != 'HAWB' && $searchColumn != 'Permit' && $searchColumn != 'TaxScheme' && $searchCon == 'contains')){
			
			if($searchCon == 'equalto'){
				$SearchValueFilter .= " AND ".$incDetailedColumn[$searchColumn]." = '".$SearchValue."'";
			} elseif($searchCon == 'notequalto'){
				$SearchValueFilter .= " AND ".$incDetailedColumn[$searchColumn]." != '".$SearchValue."'";
			} elseif($searchCon == 'contains'){
				$SearchValueFilter .= " AND ".$incDetailedColumn[$searchColumn]." LIKE '%".$SearchValue."%'";
			} elseif($searchCon == 'greaterthan'){
				$SearchValueFilter .= " AND ".$incDetailedColumn[$searchColumn]." > '".$SearchValue."'";
			} elseif($searchCon == 'lessthan'){
				$SearchValueFilter .= " AND ".$incDetailedColumn[$searchColumn]." < '".$SearchValue."'";
			}
			
			$searchCon = 'between';
			$searchColumn = 'Date';
			
			$searchInput1	= ($searchColumn == 'Date') ? date('Y/m/d', strtotime(str_replace('/', '-', trim($searchInput1)))):trim($searchInput1);
			if($searchCon == 'between'){
				$searchInput2	= ($searchColumn == 'Date') ? date('Y/m/d', strtotime(str_replace('/', '-', trim($searchInput2)))):trim($searchInput2);
			}
			
		}
		
		$incDetailedFilter = "";
		
		if($searchCon == 'equalto'){
			$incDetailedFilter .= " ".$incDetailedColumn[$searchColumn]." = '".$searchInput1."'";
		} elseif($searchCon == 'notequalto'){
			$incDetailedFilter .= " ".$incDetailedColumn[$searchColumn]." != '".$searchInput1."'";
		} elseif($searchCon == 'contains'){
			$incDetailedFilter .= " ".$incDetailedColumn[$searchColumn]." LIKE '%".$searchInput1."%'";
		} elseif($searchCon == 'greaterthan'){
			$incDetailedFilter .= " ".$incDetailedColumn[$searchColumn]." > '".$searchInput1."'";
		} elseif($searchCon == 'lessthan'){
			$incDetailedFilter .= " ".$incDetailedColumn[$searchColumn]." < '".$searchInput1."'";
		} elseif($searchCon == 'between'){
			$incDetailedFilter .= " ".$incDetailedColumn[$searchColumn]." >= '".$searchInput1."' AND ".$incDetailedColumn[$searchColumn]." <= '".$searchInput2."'";
			//$incDetailedFilter .= " ".$incDetailedColumn[$searchColumn]." BETWEEN '".$searchInput1."' AND '".$searchInput2."'";
		}
		
		if($searchCon == 'equalto'){
			
			if($searchColumn == 'PartNoDesc'){
				
				$sqlString = "SELECT *, CASE WHEN(ROW_NUMBER() OVER(PARTITION BY HAWB,InTaxRef ORDER BY [SearchDate] ASC, [ShipReference] ASC) = 1) THEN HAWBTotalQty ELSE 0 END as HAWBQty,
							  CASE WHEN(ROW_NUMBER() OVER(PARTITION BY ShipReference ORDER BY [SearchDate] ASC, [ShipReference] ASC) = 1) THEN FreightCharges ELSE 0 END as ShipmentFreightCharges,
							  CASE WHEN(ROW_NUMBER() OVER(PARTITION BY ShipReference ORDER BY [SearchDate] ASC, [ShipReference] ASC) = 1) THEN InsCharges ELSE 0 END as ShipmentInsCharges,
							  CASE WHEN(ROW_NUMBER() OVER(PARTITION BY ShipReference ORDER BY [SearchDate] ASC, [ShipReference] ASC) = 1) THEN CIFAmount ELSE 0 END as ShipmentCIFAmount,
							  CASE WHEN(ROW_NUMBER() OVER(PARTITION BY ShipReference ORDER BY [SearchDate] ASC, [ShipReference] ASC) = 1) THEN GSTAmount ELSE 0 END as ShipmentGSTAmount
							  FROM [v_GSTReport_IncomingDetailed_grouped] WITH (NOLOCK)
							  WHERE VendorInvNo IN (SELECT DISTINCT [Invoice] FROM [Entry] WITH (NOLOCK) WHERE [Partnumber] = '".$searchInput1."')
							  ORDER BY [SearchDate] ASC, [ShipReference] ASC";
				
			} elseif($searchColumn == 'SUNumber'){
				
				$sqlGetInvoice = "SELECT TOP 1 [Invoice] FROM [Entry] WITH (NOLOCK) WHERE [EntryNumber] = '".$searchInput1."'";
				$sqlGetInvoice = mssql_query($sqlGetInvoice);
				if (mssql_num_rows($sqlGetInvoice) > 0){
					$InvoiceData = mssql_fetch_assoc($sqlGetInvoice);
					
					$searchInput1	= trim($InvoiceData['Invoice']);
					
					$sqlString = "SELECT *, CASE WHEN(ROW_NUMBER() OVER(PARTITION BY HAWB,InTaxRef ORDER BY [SearchDate] ASC, [ShipReference] ASC) = 1) THEN HAWBTotalQty ELSE 0 END as HAWBQty,
								  CASE WHEN(ROW_NUMBER() OVER(PARTITION BY ShipReference ORDER BY [SearchDate] ASC, [ShipReference] ASC) = 1) THEN FreightCharges ELSE 0 END as ShipmentFreightCharges,
								  CASE WHEN(ROW_NUMBER() OVER(PARTITION BY ShipReference ORDER BY [SearchDate] ASC, [ShipReference] ASC) = 1) THEN InsCharges ELSE 0 END as ShipmentInsCharges,
								  CASE WHEN(ROW_NUMBER() OVER(PARTITION BY ShipReference ORDER BY [SearchDate] ASC, [ShipReference] ASC) = 1) THEN CIFAmount ELSE 0 END as ShipmentCIFAmount,
								  CASE WHEN(ROW_NUMBER() OVER(PARTITION BY ShipReference ORDER BY [SearchDate] ASC, [ShipReference] ASC) = 1) THEN GSTAmount ELSE 0 END as ShipmentGSTAmount
								  FROM [v_GSTReport_IncomingDetailed_grouped] WITH (NOLOCK) WHERE VendorInvNo = '".$searchInput1."' ORDER BY [SearchDate] ASC, [ShipReference] ASC";
					
				}
				
			} else {
				$sqlString = "SELECT *, CASE WHEN(ROW_NUMBER() OVER(PARTITION BY HAWB,InTaxRef ORDER BY [SearchDate] ASC, [ShipReference] ASC) = 1) THEN HAWBTotalQty ELSE 0 END as HAWBQty,
							  CASE WHEN(ROW_NUMBER() OVER(PARTITION BY ShipReference ORDER BY [SearchDate] ASC, [ShipReference] ASC) = 1) THEN FreightCharges ELSE 0 END as ShipmentFreightCharges,
							  CASE WHEN(ROW_NUMBER() OVER(PARTITION BY ShipReference ORDER BY [SearchDate] ASC, [ShipReference] ASC) = 1) THEN InsCharges ELSE 0 END as ShipmentInsCharges,
							  CASE WHEN(ROW_NUMBER() OVER(PARTITION BY ShipReference ORDER BY [SearchDate] ASC, [ShipReference] ASC) = 1) THEN CIFAmount ELSE 0 END as ShipmentCIFAmount,
							  CASE WHEN(ROW_NUMBER() OVER(PARTITION BY ShipReference ORDER BY [SearchDate] ASC, [ShipReference] ASC) = 1) THEN GSTAmount ELSE 0 END as ShipmentGSTAmount
							  FROM [v_GSTReport_IncomingDetailed_grouped] WITH (NOLOCK) WHERE ".$incDetailedFilter." ".$SearchValueFilter." ORDER BY [SearchDate] ASC, [ShipReference] ASC";
			}
			
		} else {
			$sqlString = "SELECT *, CASE WHEN(ROW_NUMBER() OVER(PARTITION BY HAWB,InTaxRef ORDER BY [SearchDate] ASC, [ShipReference] ASC) = 1) THEN HAWBTotalQty ELSE 0 END as HAWBQty,
						  CASE WHEN(ROW_NUMBER() OVER(PARTITION BY ShipReference ORDER BY [SearchDate] ASC, [ShipReference] ASC) = 1) THEN FreightCharges ELSE 0 END as ShipmentFreightCharges,
						  CASE WHEN(ROW_NUMBER() OVER(PARTITION BY ShipReference ORDER BY [SearchDate] ASC, [ShipReference] ASC) = 1) THEN InsCharges ELSE 0 END as ShipmentInsCharges,
						  CASE WHEN(ROW_NUMBER() OVER(PARTITION BY ShipReference ORDER BY [SearchDate] ASC, [ShipReference] ASC) = 1) THEN CIFAmount ELSE 0 END as ShipmentCIFAmount,
						  CASE WHEN(ROW_NUMBER() OVER(PARTITION BY ShipReference ORDER BY [SearchDate] ASC, [ShipReference] ASC) = 1) THEN GSTAmount ELSE 0 END as ShipmentGSTAmount
						  FROM [v_GSTReport_IncomingDetailed_grouped] WITH (NOLOCK) WHERE ".$incDetailedFilter." ".$SearchValueFilter." ORDER BY [SearchDate] ASC, [ShipReference] ASC";
		}
		
		//$sqlString = "SELECT *, CASE WHEN(ROW_NUMBER() OVER(PARTITION BY HAWB,InTaxRef  ORDER BY HAWB,InTaxRef ASC) = 1) THEN HAWBTotalQty ELSE 0 END as HAWBQty FROM [v_GSTReport_IncomingDetailed_grouped] WITH (NOLOCK) WHERE ".$incDetailedFilter." ORDER BY [SearchDate] ASC, [ShipReference] ASC";
		$sql = mssql_query($sqlString);
		if (mssql_num_rows($sql) > 0){
			
			$setRowHAWB = array();
			while($tmprow = mssql_fetch_assoc($sql)){
				
				//$rowShipHAWB = $tmprow['HAWB'];
				//if (in_array($rowShipHAWB, $setRowHAWB)){
				//	$tmprow['HAWBTotalQty'] = '';
				//} else {
				//	array_push($setRowHAWB, $rowShipHAWB);
				//}
				
				$tmprow['HAWBTotalQty'] = ($tmprow['HAWBQty'] == 0) ? '':$tmprow['HAWBQty'];
				$tmprow['FreightCharges'] = ($tmprow['ShipmentFreightCharges'] == 0) ? 0.00:$tmprow['ShipmentFreightCharges'];
				$tmprow['InsCharges'] = ($tmprow['ShipmentInsCharges'] == 0) ? 0.00:$tmprow['ShipmentInsCharges'];
				$tmprow['CIFAmount2'] = ($tmprow['ShipmentCIFAmount'] == 0) ? 0.00:$tmprow['ShipmentCIFAmount'];
				$tmprow['GSTAmount2'] = ($tmprow['ShipmentGSTAmount'] == 0) ? 0.00:$tmprow['ShipmentGSTAmount'];
				
				$data[] = $tmprow;
			}
			
		} else {
			$data = "nodata";
		}
		
		return json_encode($data);
		
	}
	
	function generateIncomingSummary(){
		
		extract($this->PostVars);
		mssql_select_db("ADISGWMS");
		$data = array();
		
		$searchColumn	= trim($searchColumn);
		$searchCon		= trim($searchCon);
		$searchInput1	= ($searchColumn == 'Date') ? date('Y/m/d', strtotime(str_replace('/', '-', trim($searchInput1)))):trim($searchInput1);
		if($searchCon == 'between'){
			$searchInput2	= ($searchColumn == 'Date') ? date('Y/m/d', strtotime(str_replace('/', '-', trim($searchInput2)))):trim($searchInput2);
		}
		
		$incSummaryColumn = array(
			'VendorInvNo'	=> 'VendorInvNo2',
			'TaxScheme'		=> 'InTaxRef',
			'HAWB'			=> 'HAWB',
			'Date'			=> 'SearchDate',
			'Permit'		=> 'PermitNo',
			'PartNoDesc'	=> 'Description1',
			'ShipmentNo'	=> 'ShipReference',
			'SUNumber'		=> 'EntrySU'
		);
		//'Date'			=> 'ArrivalDate1',
		
		$SearchValue		= trim($searchmainValue);
		$SearchValueFilter	= "";
		if($searchColumn == 'TaxScheme' || ($searchColumn != 'TaxScheme' && $searchCon == 'notequalto') || ($searchColumn != 'HAWB' && $searchColumn != 'Permit' && $searchColumn != 'TaxScheme' && $searchCon == 'contains')){
			
			if($searchCon == 'equalto'){
				$SearchValueFilter .= " AND ".$incSummaryColumn[$searchColumn]." = '".$SearchValue."'";
			} elseif($searchCon == 'notequalto'){
				$SearchValueFilter .= " AND ".$incSummaryColumn[$searchColumn]." != '".$SearchValue."'";
			} elseif($searchCon == 'contains'){
				$SearchValueFilter .= " AND ".$incSummaryColumn[$searchColumn]." LIKE '%".$SearchValue."%'";
			} elseif($searchCon == 'greaterthan'){
				$SearchValueFilter .= " AND ".$incSummaryColumn[$searchColumn]." > '".$SearchValue."'";
			} elseif($searchCon == 'lessthan'){
				$SearchValueFilter .= " AND ".$incSummaryColumn[$searchColumn]." < '".$SearchValue."'";
			}
			
			$searchCon = 'between';
			$searchColumn = 'Date';
			
			$searchInput1	= ($searchColumn == 'Date') ? date('Y/m/d', strtotime(str_replace('/', '-', trim($searchInput1)))):trim($searchInput1);
			if($searchCon == 'between'){
				$searchInput2	= ($searchColumn == 'Date') ? date('Y/m/d', strtotime(str_replace('/', '-', trim($searchInput2)))):trim($searchInput2);
			}
			
		}
		
		$incSummaryFilter = "";
		
		if($searchCon == 'equalto'){
			$incSummaryFilter .= " ".$incSummaryColumn[$searchColumn]." = '".$searchInput1."'";
		} elseif($searchCon == 'notequalto'){
			$incSummaryFilter .= " ".$incSummaryColumn[$searchColumn]." != '".$searchInput1."'";
		} elseif($searchCon == 'contains'){
			$incSummaryFilter .= " ".$incSummaryColumn[$searchColumn]." LIKE '%".$searchInput1."%'";
		} elseif($searchCon == 'greaterthan'){
			$incSummaryFilter .= " ".$incSummaryColumn[$searchColumn]." > '".$searchInput1."'";
		} elseif($searchCon == 'lessthan'){
			$incSummaryFilter .= " ".$incSummaryColumn[$searchColumn]." < '".$searchInput1."'";
		} elseif($searchCon == 'between'){
			$incSummaryFilter .= " ".$incSummaryColumn[$searchColumn]." >= '".$searchInput1."' AND ".$incSummaryColumn[$searchColumn]." <= '".$searchInput2."'";
			//$incSummaryFilter .= " ".$incSummaryColumn[$searchColumn]." BETWEEN '".$searchInput1."' AND '".$searchInput2."'";
		}
		
		$sqlString = "SELECT *, CASE WHEN(ROW_NUMBER() OVER(PARTITION BY HAWB,InTaxRef ORDER BY [SearchDate] ASC, [ShipReference] ASC) = 1) THEN HAWBTotalQty ELSE 0 END as HAWBQty,
					  CASE WHEN(ROW_NUMBER() OVER(PARTITION BY ShipReference ORDER BY [SearchDate] ASC, [ShipReference] ASC) = 1) THEN TotalFreightCharges ELSE 0 END as ShipmentFreightCharges,
					  CASE WHEN(ROW_NUMBER() OVER(PARTITION BY ShipReference ORDER BY [SearchDate] ASC, [ShipReference] ASC) = 1) THEN TotalInsCharges ELSE 0 END as ShipmentInsCharges,
					  CASE WHEN(ROW_NUMBER() OVER(PARTITION BY ShipReference ORDER BY [SearchDate] ASC, [ShipReference] ASC) = 1) THEN TotalCIFAmount ELSE 0 END as ShipmentCIFAmount,
					  CASE WHEN(ROW_NUMBER() OVER(PARTITION BY ShipReference ORDER BY [SearchDate] ASC, [ShipReference] ASC) = 1) THEN TotalGSTAmount ELSE 0 END as ShipmentGSTAmount
					  FROM [v_GSTReport_IncomingSummary_grouped] WITH (NOLOCK) WHERE ".$incSummaryFilter." ".$SearchValueFilter." ORDER BY [SearchDate] ASC, [ShipReference] ASC";
		$sql = mssql_query($sqlString);
		if (mssql_num_rows($sql) > 0){
			
			$setRowHAWB = array();
			while($tmprow = mssql_fetch_assoc($sql)){
				
				//$rowShipHAWB = $tmprow['HAWB'];
				//if (in_array($rowShipHAWB, $setRowHAWB)){
				//	$tmprow['HAWBTotalQty'] = '';
				//} else {
				//	array_push($setRowHAWB, $rowShipHAWB);
				//}
				
				$tmprow['HAWBTotalQty'] = ($tmprow['HAWBQty'] == 0) ? '':$tmprow['HAWBQty'];
				$tmprow['FreightCharges'] = ($tmprow['ShipmentFreightCharges'] == 0) ? 0.00:$tmprow['ShipmentFreightCharges'];
				$tmprow['InsCharges'] = ($tmprow['ShipmentInsCharges'] == 0) ? 0.00:$tmprow['ShipmentInsCharges'];
				$tmprow['CIFAmount'] = ($tmprow['ShipmentCIFAmount'] == 0) ? 0.00:$tmprow['ShipmentCIFAmount'];
				$tmprow['GSTAmount'] = ($tmprow['ShipmentGSTAmount'] == 0) ? 0.00:$tmprow['ShipmentGSTAmount'];
				
				$data[] = $tmprow;
			}
			
		} else {
			$data = "nodata";
		}
		
		return json_encode($data);
		
	}
	
	function generateOutgoingDetailed(){
		
		extract($this->PostVars);
		mssql_select_db("ADISGWMS");
		$data = array();
		
		$searchColumn	= trim($searchColumn);
		$searchCon		= trim($searchCon);
		$searchInput1	= ($searchColumn == 'Date') ? date('Y/m/d', strtotime(str_replace('/', '-', trim($searchInput1)))):trim($searchInput1);
		if($searchCon == 'between'){
			$searchInput2	= ($searchColumn == 'Date') ? date('Y/m/d', strtotime(str_replace('/', '-', trim($searchInput2)))):trim($searchInput2);
		}
		
		$outDetailedColumn = array(
			'VendorInvNo'	=> 'DN',
			'TaxScheme'		=> 'OutTaxRef',
			'HAWB'			=> 'HAWB',
			'Date'			=> 'SearchDate',
			'Permit'		=> 'PermitNo',
			'PartNoDesc'	=> 'MaterialDescriptions',
			'ShipmentNo'	=> 'ShipmentRef',
			'SUNumber'		=> 'SUNumbers'
		);
		//'Date'			=> 'CargoReleasedDate',
		
		$SearchValue		= trim($searchmainValue);
		$SearchValueFilter	= "";
		if($searchColumn == 'TaxScheme' || ($searchColumn != 'TaxScheme' && $searchCon == 'notequalto') || ($searchColumn != 'HAWB' && $searchColumn != 'Permit' && $searchColumn != 'TaxScheme' && $searchCon == 'contains')){
			
			if($searchCon == 'equalto'){
				$SearchValueFilter .= " AND ".$outDetailedColumn[$searchColumn]." = '".$SearchValue."'";
			} elseif($searchCon == 'notequalto'){
				$SearchValueFilter .= " AND ".$outDetailedColumn[$searchColumn]." != '".$SearchValue."'";
			} elseif($searchCon == 'contains'){
				$SearchValueFilter .= " AND ".$outDetailedColumn[$searchColumn]." LIKE '%".$SearchValue."%'";
			} elseif($searchCon == 'greaterthan'){
				$SearchValueFilter .= " AND ".$outDetailedColumn[$searchColumn]." > '".$SearchValue."'";
			} elseif($searchCon == 'lessthan'){
				$SearchValueFilter .= " AND ".$outDetailedColumn[$searchColumn]." < '".$SearchValue."'";
			}
			
			$searchCon = 'between';
			$searchColumn = 'Date';
			
			$searchInput1	= ($searchColumn == 'Date') ? date('Y/m/d', strtotime(str_replace('/', '-', trim($searchInput1)))):trim($searchInput1);
			if($searchCon == 'between'){
				$searchInput2	= ($searchColumn == 'Date') ? date('Y/m/d', strtotime(str_replace('/', '-', trim($searchInput2)))):trim($searchInput2);
			}
			
		}
		
		$outDetailedFilter = "";
		
		if($searchCon == 'equalto'){
			$outDetailedFilter .= " ".$outDetailedColumn[$searchColumn]." = '".$searchInput1."'";
		} elseif($searchCon == 'notequalto'){
			$outDetailedFilter .= " ".$outDetailedColumn[$searchColumn]." != '".$searchInput1."'";
		} elseif($searchCon == 'contains'){
			$outDetailedFilter .= " ".$outDetailedColumn[$searchColumn]." LIKE '%".$searchInput1."%'";
		} elseif($searchCon == 'greaterthan'){
			$outDetailedFilter .= " ".$outDetailedColumn[$searchColumn]." > '".$searchInput1."'";
		} elseif($searchCon == 'lessthan'){
			$outDetailedFilter .= " ".$outDetailedColumn[$searchColumn]." < '".$searchInput1."'";
		} elseif($searchCon == 'between'){
			$outDetailedFilter .= " ".$outDetailedColumn[$searchColumn]." >= '".$searchInput1."' AND ".$outDetailedColumn[$searchColumn]." <= '".$searchInput2."'";
			//$outDetailedFilter .= " ".$outDetailedColumn[$searchColumn]." BETWEEN '".$searchInput1."' AND '".$searchInput2."'";
		}
		
		$sqlString = "SELECT *, CASE WHEN(ROW_NUMBER() OVER(PARTITION BY HAWB, TaxScheme ORDER BY [SearchDate] ASC, [ShipmentRef] ASC, [HAWB] ASC, [PermitNo] ASC) = 1) THEN ShipTotalQty ELSE 0 END as HAWBQty, CASE WHEN(ROW_NUMBER() OVER(PARTITION BY HAWB, TaxScheme ORDER BY [SearchDate] ASC, [ShipmentRef] ASC, [HAWB] ASC, [PermitNo] ASC) = 1) THEN ShipTotalValueSGD ELSE '0' END as HAWBTotalValue FROM [v_GSTOutgoingDetailed] WITH (NOLOCK) WHERE ".$outDetailedFilter." ".$SearchValueFilter." ORDER BY [SearchDate] ASC, [ShipmentRef] ASC, [HAWB] ASC, [PermitNo] ASC";
		
		$sql = mssql_query($sqlString);
		if (mssql_num_rows($sql) > 0){
			
			$setRowHAWB = array();
			while($tmprow = mssql_fetch_assoc($sql)){
				
				//$rowShipHAWB = $tmprow['HAWB'];
				//if (in_array($rowShipHAWB, $setRowHAWB)){
				//	$tmprow['ShipTotalQty'] = '';
				//	$tmprow['ShipTotalValueSGD'] = '';
				//} else {
				//	array_push($setRowHAWB, $rowShipHAWB);
				//}
				
				$tmprow['ShipTotalQty'] = ($tmprow['HAWBQty'] == 0) ? '':$tmprow['HAWBQty'];
				$tmprow['ShipTotalValueSGD'] = ($tmprow['HAWBTotalValue'] == 0) ? '':$tmprow['HAWBTotalValue'];
				
				$data[] = $tmprow;
			}
			
		} else {
			$data = "nodata";
		}
		
		return json_encode($data);
		
	}
	
	function generateOutgoingSummary(){
		
		extract($this->PostVars);
		mssql_select_db("ADISGWMS");
		$data = array();
		
		$searchColumn	= trim($searchColumn);
		$searchCon		= trim($searchCon);
		$searchInput1	= ($searchColumn == 'Date') ? date('Y/m/d', strtotime(str_replace('/', '-', trim($searchInput1)))):trim($searchInput1);
		if($searchCon == 'between'){
			$searchInput2	= ($searchColumn == 'Date') ? date('Y/m/d', strtotime(str_replace('/', '-', trim($searchInput2)))):trim($searchInput2);
		}
		
		$outSummaryColumn = array(
			'VendorInvNo'	=> 'DN',
			'TaxScheme'		=> 'OutTaxRef',
			'HAWB'			=> 'HAWB',
			'Date'			=> 'SearchDate',
			'Permit'		=> 'PermitNo',
			'PartNoDesc'	=> 'MaterialDescriptions',
			'ShipmentNo'	=> 'ShipmentRef',
			'SUNumber'		=> 'SUNumbers'
		);
		//'Date'			=> 'CargoReleasedDate',
		
		$SearchValue		= trim($searchmainValue);
		$SearchValueFilter	= "";
		if($searchColumn == 'TaxScheme' || ($searchColumn != 'TaxScheme' && $searchCon == 'notequalto') || ($searchColumn != 'HAWB' && $searchColumn != 'Permit' && $searchColumn != 'TaxScheme' && $searchCon == 'contains')){
			
			if($searchCon == 'equalto'){
				$SearchValueFilter .= " AND ".$outSummaryColumn[$searchColumn]." = '".$SearchValue."'";
			} elseif($searchCon == 'notequalto'){
				$SearchValueFilter .= " AND ".$outSummaryColumn[$searchColumn]." != '".$SearchValue."'";
			} elseif($searchCon == 'contains'){
				$SearchValueFilter .= " AND ".$outSummaryColumn[$searchColumn]." LIKE '%".$SearchValue."%'";
			} elseif($searchCon == 'greaterthan'){
				$SearchValueFilter .= " AND ".$outSummaryColumn[$searchColumn]." > '".$SearchValue."'";
			} elseif($searchCon == 'lessthan'){
				$SearchValueFilter .= " AND ".$outSummaryColumn[$searchColumn]." < '".$SearchValue."'";
			}
			
			$searchCon = 'between';
			$searchColumn = 'Date';
			
			$searchInput1	= ($searchColumn == 'Date') ? date('Y/m/d', strtotime(str_replace('/', '-', trim($searchInput1)))):trim($searchInput1);
			if($searchCon == 'between'){
				$searchInput2	= ($searchColumn == 'Date') ? date('Y/m/d', strtotime(str_replace('/', '-', trim($searchInput2)))):trim($searchInput2);
			}
			
		}
		
		$outSummaryFilter = "";
		
		if($searchCon == 'equalto'){
			$outSummaryFilter .= " ".$outSummaryColumn[$searchColumn]." = '".$searchInput1."'";
		} elseif($searchCon == 'notequalto'){
			$outSummaryFilter .= " ".$outSummaryColumn[$searchColumn]." != '".$searchInput1."'";
		} elseif($searchCon == 'contains'){
			$outSummaryFilter .= " ".$outSummaryColumn[$searchColumn]." LIKE '%".$searchInput1."%'";
		} elseif($searchCon == 'greaterthan'){
			$outSummaryFilter .= " ".$outSummaryColumn[$searchColumn]." > '".$searchInput1."'";
		} elseif($searchCon == 'lessthan'){
			$outSummaryFilter .= " ".$outSummaryColumn[$searchColumn]." < '".$searchInput1."'";
		} elseif($searchCon == 'between'){
			$outSummaryFilter .= " ".$outSummaryColumn[$searchColumn]." >= '".$searchInput1."' AND ".$outSummaryColumn[$searchColumn]." <= '".$searchInput2."'";
			//$outSummaryFilter .= " ".$outSummaryColumn[$searchColumn]." BETWEEN '".$searchInput1."' AND '".$searchInput2."'";
		}
		
		$sqlString = "SELECT *, CASE WHEN(ROW_NUMBER() OVER(PARTITION BY HAWB, OutTaxRef ORDER BY [SearchDate] ASC, [ShipmentRef] ASC, [HAWB] ASC, [PermitNo] ASC) = 1) THEN ShipTotalQty ELSE 0 END as HAWBQty, CASE WHEN(ROW_NUMBER() OVER(PARTITION BY HAWB, OutTaxRef ORDER BY [SearchDate] ASC, [ShipmentRef] ASC, [HAWB] ASC, [PermitNo] ASC) = 1) THEN ShipTotalValueSGD ELSE '0' END as HAWBTotalValue FROM [v_GSTOutgoingSummary] WHERE ".$outSummaryFilter." ".$SearchValueFilter." ORDER BY [SearchDate] ASC, [ShipmentRef] ASC, [HAWB] ASC, [PermitNo] ASC";
		
		$sql = mssql_query($sqlString);
		//return json_encode(mssql_get_last_message());
		//var_dump(mssql_get_last_message());
		//die();
		if (mssql_num_rows($sql) > 0){
			
			$setRowHAWB = array();
			while($tmprow = mssql_fetch_assoc($sql)){
				
				//$rowShipHAWB = $tmprow['HAWB'];
				//if (in_array($rowShipHAWB, $setRowHAWB)){
				//	$tmprow['ShipTotalQty'] = '';
				//} else {
				//	array_push($setRowHAWB, $rowShipHAWB);
				//}
				
				$tmprow['ShipTotalQty'] = ($tmprow['HAWBQty'] == 0) ? '':$tmprow['HAWBQty'];
				$tmprow['ShipTotalValueSGD'] = ($tmprow['HAWBTotalValue'] == 0) ? '':$tmprow['HAWBTotalValue'];
				
				$data[] = $tmprow;
			}
			
		} else {
			$data = "nodata";
		}
		
		return json_encode($data);
		
	}

	function generateShipmentPicklist(){
		
		extract($this->PostVars);
		mssql_select_db("ADISGWMS");
		$data = array();
		
		$searchColumn	= trim($searchColumn);
		$searchCon		= trim($searchCon);
		$searchInput1	= ($searchColumn == 'Date') ? date('Y/m/d', strtotime(str_replace('/', '-', trim($searchInput1)))):trim($searchInput1);
		if($searchCon == 'between'){
			$searchInput2	= ($searchColumn == 'Date') ? date('Y/m/d', strtotime(str_replace('/', '-', trim($searchInput2)))):trim($searchInput2);
		}
		
		$picklistColumn = array(
			'VendorInvNo'	=> 'CustomerReference',
			'TaxScheme'		=> 'OutTaxScheme',
			'HAWB'			=> 'HAWB',
			'Date'			=> 'SearchDate',
			'Permit'		=> 'OutPermit',
			'PartNoDesc'	=> 'OutPart',
			'ShipmentNo'	=> 'OutRef',
			'SUNumber'		=> 'OutEntry'
		);
		//'Date'			=> 'ReleasedDate',
		
		$SearchValue		= trim($searchmainValue);
		$SearchValueFilter	= "";
		if($searchColumn == 'TaxScheme' || ($searchColumn != 'TaxScheme' && $searchCon == 'notequalto') || ($searchColumn != 'HAWB' && $searchColumn != 'Permit' && $searchColumn != 'TaxScheme' && $searchCon == 'contains')){
			
			if($searchCon == 'equalto'){
				$SearchValueFilter .= " AND ".$picklistColumn[$searchColumn]." = '".$SearchValue."'";
			} elseif($searchCon == 'notequalto'){
				$SearchValueFilter .= " AND ".$picklistColumn[$searchColumn]." != '".$SearchValue."'";
			} elseif($searchCon == 'contains'){
				$SearchValueFilter .= " AND ".$picklistColumn[$searchColumn]." LIKE '%".$SearchValue."%'";
			} elseif($searchCon == 'greaterthan'){
				$SearchValueFilter .= " AND ".$picklistColumn[$searchColumn]." > '".$SearchValue."'";
			} elseif($searchCon == 'lessthan'){
				$SearchValueFilter .= " AND ".$picklistColumn[$searchColumn]." < '".$SearchValue."'";
			}
			
			$searchCon = 'between';
			$searchColumn = 'Date';
			
			$searchInput1	= ($searchColumn == 'Date') ? date('Y/m/d', strtotime(str_replace('/', '-', trim($searchInput1)))):trim($searchInput1);
			if($searchCon == 'between'){
				$searchInput2	= ($searchColumn == 'Date') ? date('Y/m/d', strtotime(str_replace('/', '-', trim($searchInput2)))):trim($searchInput2);
			}
			
		}
		
		$picklistFilter = "";
		
		if($searchCon == 'equalto'){
			$picklistFilter .= " ".$picklistColumn[$searchColumn]." = '".$searchInput1."'";
		} elseif($searchCon == 'notequalto'){
			$picklistFilter .= " ".$picklistColumn[$searchColumn]." != '".$searchInput1."'";
		} elseif($searchCon == 'contains'){
			$picklistFilter .= " ".$picklistColumn[$searchColumn]." LIKE '%".$searchInput1."%'";
		} elseif($searchCon == 'greaterthan'){
			$picklistFilter .= " ".$picklistColumn[$searchColumn]." > '".$searchInput1."'";
		} elseif($searchCon == 'lessthan'){
			$picklistFilter .= " ".$picklistColumn[$searchColumn]." < '".$searchInput1."'";
		} elseif($searchCon == 'between'){
			$picklistFilter .= " ".$picklistColumn[$searchColumn]." >= '".$searchInput1."' AND ".$picklistColumn[$searchColumn]." <= '".$searchInput2."'";
			//$picklistFilter .= " ".$picklistColumn[$searchColumn]." BETWEEN '".$searchInput1."' AND '".$searchInput2."'";
		}
		
		$sqlString = "SELECT * FROM [v_GSTShipmentPicklist_Union] WHERE ".$picklistFilter." ".$SearchValueFilter." ORDER BY [TargetDate2] ASC, [OutRef] ASC, [HAWB] ASC, [OutPermit] ASC, [OutTaxScheme] ASC";
		$sql = mssql_query($sqlString);
		if (mssql_num_rows($sql) > 0){
			
			$setRowSU = array();
			$setRowSUArray = array();
			$setLastEntry = '';
			$setLastSUBal = 0;
			while($tmprow = mssql_fetch_assoc($sql)){
				
				$rowShipRef = $tmprow['InEntry'];
				if (in_array($rowShipRef, $setRowSU)){
					$tmprow['TotalIncomingQty'] = 0;
				} else {
					$tmprow['TotalIncomingQty'] = $tmprow['InQty'];
					array_push($setRowSU, $rowShipRef);
					$setRowSUArray[$rowShipRef] = array();
				}
				
				$tmprow['TotalBalanceQty'] = $tmprow['SUBal'];
				$tmprow['SUBal'] = $tmprow['SUBalPerDN'];
				
				$data[] = $tmprow;
				
			}
			
		} else {
			$data = "nodata";
		}
		
		return json_encode($data);
		
	}

	function generateShipmentSummary(){
		
		extract($this->PostVars);
		mssql_select_db("ADISGWMS");
		$data = array();
		
		$searchColumn	= trim($searchColumn);
		$searchCon		= trim($searchCon);
		$searchInput1	= ($searchColumn == 'Date') ? date('Y/m/d', strtotime(str_replace('/', '-', trim($searchInput1)))):trim($searchInput1);
		if($searchCon == 'between'){
			$searchInput2	= ($searchColumn == 'Date') ? date('Y/m/d', strtotime(str_replace('/', '-', trim($searchInput2)))):trim($searchInput2);
		}
		
		$shipSummaryColumn = array(
			'VendorInvNo'	=> 'Invoice',
			'TaxScheme'		=> 'InTaxScheme',
			'HAWB'			=> 'AirwayBill',
			'Date'			=> 'SearchDate',
			'Permit'		=> 'InPermit',
			'PartNoDesc'	=> 'InPart',
			'ShipmentNo'	=> 'InRef',
			'SUNumber'		=> 'InEntry'
		);
		//'Date'			=> 'ReceivedDate',
		
		$SearchValue		= trim($searchmainValue);
		$SearchValueFilter	= "";
		if($searchColumn == 'TaxScheme' || ($searchColumn != 'TaxScheme' && $searchCon == 'notequalto') || ($searchColumn != 'HAWB' && $searchColumn != 'Permit' && $searchColumn != 'TaxScheme' && $searchCon == 'contains')){
			
			if($searchCon == 'equalto'){
				$SearchValueFilter .= " AND ".$shipSummaryColumn[$searchColumn]." = '".$SearchValue."'";
			} elseif($searchCon == 'notequalto'){
				$SearchValueFilter .= " AND ".$shipSummaryColumn[$searchColumn]." != '".$SearchValue."'";
			} elseif($searchCon == 'contains'){
				$SearchValueFilter .= " AND ".$shipSummaryColumn[$searchColumn]." LIKE '%".$SearchValue."%'";
			} elseif($searchCon == 'greaterthan'){
				$SearchValueFilter .= " AND ".$shipSummaryColumn[$searchColumn]." > '".$SearchValue."'";
			} elseif($searchCon == 'lessthan'){
				$SearchValueFilter .= " AND ".$shipSummaryColumn[$searchColumn]." < '".$SearchValue."'";
			}
			
			$searchCon = 'between';
			$searchColumn = 'Date';
			
			$searchInput1	= ($searchColumn == 'Date') ? date('Y/m/d', strtotime(str_replace('/', '-', trim($searchInput1)))):trim($searchInput1);
			if($searchCon == 'between'){
				$searchInput2	= ($searchColumn == 'Date') ? date('Y/m/d', strtotime(str_replace('/', '-', trim($searchInput2)))):trim($searchInput2);
			}
			
		}
		
		$shipSummaryFilter = "";
		
		if($searchCon == 'equalto'){
			$shipSummaryFilter .= " ".$shipSummaryColumn[$searchColumn]." = '".$searchInput1."'";
		} elseif($searchCon == 'notequalto'){
			$shipSummaryFilter .= " ".$shipSummaryColumn[$searchColumn]." != '".$searchInput1."'";
		} elseif($searchCon == 'contains'){
			$shipSummaryFilter .= " ".$shipSummaryColumn[$searchColumn]." LIKE '%".$searchInput1."%'";
		} elseif($searchCon == 'greaterthan'){
			$shipSummaryFilter .= " ".$shipSummaryColumn[$searchColumn]." > '".$searchInput1."'";
		} elseif($searchCon == 'lessthan'){
			$shipSummaryFilter .= " ".$shipSummaryColumn[$searchColumn]." < '".$searchInput1."'";
		} elseif($searchCon == 'between'){
			$shipSummaryFilter .= " ".$shipSummaryColumn[$searchColumn]." >= '".$searchInput1."' AND ".$shipSummaryColumn[$searchColumn]." <= '".$searchInput2."'";
			//$shipSummaryFilter .= " ".$shipSummaryColumn[$searchColumn]." BETWEEN '".$searchInput1."' AND '".$searchInput2."'";
		}
		
		$sqlString = "SELECT * FROM [v_GSTShipmentPicklistSummary_Union] WHERE ".$shipSummaryFilter." ".$SearchValueFilter." ORDER BY [ReceivedDate2] ASC, [InRef] ASC, [AirwayBill] ASC, [InPermit] ASC, [InTaxScheme] ASC";
		$sql = mssql_query($sqlString);
		if (mssql_num_rows($sql) > 0){
			
			$setRowSU = array();
			$setRowSUArray = array();
			$setLastEntry = '';
			$setLastSUBal = 0;
			while($tmprow = mssql_fetch_assoc($sql)){
				
				$rowShipRef = $tmprow['InEntry'];
				if (in_array($rowShipRef, $setRowSU)){
					$tmprow['TotalIncomingQty'] = 0;
				} else {
					$tmprow['TotalIncomingQty'] = $tmprow['InQty'];
					array_push($setRowSU, $rowShipRef);
					$setRowSUArray[$rowShipRef] = array();
				}
				
				$tmprow['TotalBalanceQty'] = $tmprow['SUBal'];
				$tmprow['SUBal'] = $tmprow['SUBalPerDN'];
				
				$data[] = $tmprow;
				
			}
			
		} else {
			$data = "nodata";
		}
		
		return json_encode($data);
		
	}
	
}

?>

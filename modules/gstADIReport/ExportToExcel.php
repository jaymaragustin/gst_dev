<?php
//ExportToExcel.php

//error_reporting(E_ALL);
//ini_set('display_errors',1);
ini_set('memory_limit', '1024M');

require('../../../widgets/Excel/PHPExcel.php');
$phpExcel = new PHPExcel();

$styleArrayHeader = array(
	'font' => array(
		'bold' => true,
	)
);

$_POST['tblIncomingDetailed'] = isset($_POST['tblIncomingDetailed']) ? $_POST['tblIncomingDetailed']:array();
$_POST['tblOutgoingDetailed'] = isset($_POST['tblOutgoingDetailed']) ? $_POST['tblOutgoingDetailed']:array();
$_POST['tblIncomingSummary'] = isset($_POST['tblIncomingSummary']) ? $_POST['tblIncomingSummary']:array();
$_POST['tblOutgoingSummary'] = isset($_POST['tblOutgoingSummary']) ? $_POST['tblOutgoingSummary']:array();
$_POST['tblPicklistCount'] = isset($_POST['tblPicklistCount']) ? $_POST['tblPicklistCount']:array();
$_POST['tblShipmentSummaryCount'] = isset($_POST['tblShipmentSummaryCount']) ? $_POST['tblShipmentSummaryCount']:array();

$iTotalQty = 0;
$iTotalShipQty = 0;
$iVendorValUSD = 0;
$iVendorValSGD = 0;
$iFrtCharges = 0;
$iInsCharges = 0;
$iCIFAmount = 0;
$iGSTAmount = 0;

if(COUNT($_POST['tblIncomingDetailed']) > 0){
	foreach($_POST['tblIncomingDetailed'] as $IncomingDetailedData){
		$iTotalQty = $iTotalQty + $IncomingDetailedData[5];
		$iTotalShipQty = $iTotalShipQty + $IncomingDetailedData[6];
		$iVendorValUSD = $iVendorValUSD + str_replace(",","",$IncomingDetailedData[10]);
		$iVendorValSGD = $iVendorValSGD + str_replace(",","",$IncomingDetailedData[11]);
		$iFrtCharges = $iFrtCharges + str_replace(",","",$IncomingDetailedData[12]);
		$iInsCharges = $iInsCharges + str_replace(",","",$IncomingDetailedData[13]);
		$iCIFAmount = $iCIFAmount + str_replace(",","",$IncomingDetailedData[14]);
		$iGSTAmount = $iGSTAmount + str_replace(",","",$IncomingDetailedData[15]);
	}
}

$iVendorValUSD = number_format($iVendorValUSD,2);
$iVendorValSGD = number_format($iVendorValSGD,2);
$iFrtCharges = number_format($iFrtCharges,2);
$iInsCharges = number_format($iInsCharges,2);
$iCIFAmount = number_format($iCIFAmount,2);
$iGSTAmount = number_format($iGSTAmount,2);

$Customer = ($_POST['Customer'] == 'ADI' ? 'ADI 3rd Floor' : $_POST['Customer']);
$ActiveSheet = $phpExcel->setActiveSheetIndex(0);
$ActiveSheet->setTitle("Incoming GST Detailed");
printHeaderDetails($ActiveSheet,$styleArrayHeader,$_POST['StartDate'],$_POST['EndDate'],$_POST['TaxRef'],$Customer,'INCOMING GST REPORT (DETAILED)');
$tblIncomingDetailedHeader = array('SERIAL#','Customer','Shipment #','Shpmt Rcvd Date','VendorInvNo.','Total Qty','Total Qty(HAWB)','Tax Reference','Permit No.','Exchange Rate','Vendor Inv Value (USD)','Vendor Inv Value (SGD)','FrtCharges (SGD)','InsCharges (SGD)','CIF Amount (SGD)','GST Amount (SGD)','Origin Port','Forwarders','Vendor/Supplier','HAWB No.','MAWB No.','TptMode','Arrival Date','Delivery To','IncoTerms','JobType','Comment','Incoming Reference','Description','SU/Entry#.','Consignment order (Y/N)','Previous SU# for consignment');
//$tblIncomingDetailedFooter = array('SERIAL#','Customer','Shipment #','Shpmt Rcvd Date','VendorInvNo.','Total Qty : '.$iTotalQty,'Total Qty(HAWB) : '.$iTotalShipQty,'Tax Reference','Permit No.','Exchange Rate','Vendor Inv Value (USD) : '.$iVendorValUSD,'Vendor Inv Value (SGD) : '.$iVendorValSGD,'Frt Charges (SGD) : '.$iFrtCharges,'Ins Charges (SGD) : '.$iInsCharges,'CIF Amount (SGD) : '.$iCIFAmount,'GST Amount (SGD) : '.$iGSTAmount,'Origin Port','Forwarders','Vendor/Supplier','HAWB No.','MAWB No.','TptMode','Arrival Date','Delivery To','IncoTerms','JobType','Comment','Incoming Reference','Description','SU/Entry#.','Consignment order (Y/N)','Previous SU# for consignment');
$tblIncomingDetailedFooter = array('','','','','',$iTotalQty,$iTotalShipQty,'','','',$iVendorValUSD,$iVendorValSGD,$iFrtCharges,$iInsCharges,$iCIFAmount,$iGSTAmount,'','','','','','','','','','','','','','','','');
PrintRow($ActiveSheet,$tblIncomingDetailedHeader,1,5,$styleArrayHeader,PHPExcel_Style_Alignment::HORIZONTAL_CENTER,"Incoming GST Detailed");
PrintRow($ActiveSheet,$_POST['tblIncomingDetailed'],1,6,'',PHPExcel_Style_Alignment::HORIZONTAL_CENTER,"Incoming GST Detailed");
$countArray = COUNT($_POST['tblIncomingDetailed']) + 6;
PrintRow($ActiveSheet,$tblIncomingDetailedFooter,1,$countArray,$styleArrayHeader,PHPExcel_Style_Alignment::HORIZONTAL_CENTER,"Incoming GST Detailed");

$iTotalQty = 0;
$iTotalShipQty = 0;
$iCustInvValUSD = 0;
$iCustInvValSGD = 0;
$iGSTAmount = 0;
$iOtherCharges = 0;
$iTotalInvoiceValue = 0;
if(COUNT($_POST['tblOutgoingDetailed']) > 0){
	foreach($_POST['tblOutgoingDetailed'] as $OutgoingDetailedData){
		$iTotalQty = $iTotalQty + $OutgoingDetailedData[9];
		$iTotalShipQty = $iTotalShipQty + $OutgoingDetailedData[10];
		$iCustInvValUSD = $iCustInvValUSD + str_replace(",","",$OutgoingDetailedData[26]);
		$iCustInvValSGD = $iCustInvValSGD + str_replace(",","",$OutgoingDetailedData[27]);
		$iGSTAmount = $iGSTAmount + str_replace(",","",$OutgoingDetailedData[28]);
		$iOtherCharges = $iOtherCharges + str_replace(",","",$OutgoingDetailedData[29]);
		$iTotalInvoiceValue = $iTotalInvoiceValue + str_replace(",","",$OutgoingDetailedData[30]);
	}
}

$iCustInvValUSD = number_format($iCustInvValUSD,2);
$iCustInvValSGD = number_format($iCustInvValSGD,2);
$iGSTAmount = number_format($iGSTAmount,2);
$iOtherCharges = number_format($iOtherCharges,2);
$iTotalInvoiceValue = number_format($iTotalInvoiceValue,2);

$phpExcel->createSheet();
$ActiveSheet = $phpExcel->setActiveSheetIndex(1);
$ActiveSheet->setTitle("Outgoing GST Detailed");
printHeaderDetails($ActiveSheet,$styleArrayHeader,$_POST['StartDate'],$_POST['EndDate'],$_POST['TaxRef'],$Customer,'OUTGOING GST REPORT (DETAILED)');
$tblIncomingSummaryHeader = array('SERIAL#','Customer','Shipment #','Packlist No #','Shipment Release Date.','Shipment No.','Vendor Inv No.','Vendor / Supplier','Comm Inv No/DN.','Total Qty','Total Qty(HAWB)','Unit Price','Out Tax Ref','Permit #','Exchange Rate','Dest Port','Forwarders','Consignee','HAWB No.','MAWB No.','Tpt Mode','Dep Date','Delivery From','Inco Terms','Job Type','Comment','Customer Inv Value(USD)','Customer Inv Value(SGD)','GST Amount(SGD)','Other Charges (SGD)','Total Invoice Value (SGD)','Total Value','OUT SU #','Description');
//$tblIncomingSummaryFooter = array('SERIAL#','Customer','Shipment #','Packlist No #','Shipment Release Date.','Shipment No.','Vendor Inv No.','Vendor / Supplier','Comm Inv No/DN.','Total Qty : '.$iTotalQty,'Total Qty(HAWB) : '.$iTotalShipQty,'Unit Price','Out Tax Ref','Permit #','Exchange Rate','Dest Port','Forwarders','Consignee','HAWB No.','MAWB No.','Tpt Mode','Dep Date','Delivery From','Inco Terms','Job Type','Comment','Customer Inv Value(USD) : '.$iCustInvValUSD,'Customer Inv Value(SGD) : '.$iCustInvValSGD,'GST Amount(SGD) : '.$iGSTAmount,'Total Value','OUT SU #','Description');
$tblIncomingSummaryFooter = array('','','','','','','','','',$iTotalQty,$iTotalShipQty,'','','','','','','','','','','','','','','',$iCustInvValUSD,$iCustInvValSGD,$iGSTAmount,$iOtherCharges,$iTotalInvoiceValue,'','','');
PrintRow($ActiveSheet,$tblIncomingSummaryHeader,1,5,$styleArrayHeader,PHPExcel_Style_Alignment::HORIZONTAL_CENTER,"Outgoing GST Detailed");
PrintRow($ActiveSheet,$_POST['tblOutgoingDetailed'],1,6,'',PHPExcel_Style_Alignment::HORIZONTAL_CENTER,"Outgoing GST Detailed");
$countArray = COUNT($_POST['tblOutgoingDetailed']) + 6;
PrintRow($ActiveSheet,$tblIncomingSummaryFooter,1,$countArray,$styleArrayHeader,PHPExcel_Style_Alignment::HORIZONTAL_CENTER,"Outgoing GST Detailed");


$iTotalQty = 0;
$iTotalShipQty = 0;
$iVendorValUSD = 0;
$iVendorValSGD = 0;
$iFrtCharges = 0;
$iInsCharges = 0;
$iCIFAmount = 0;
$iGSTAmount = 0;
if(COUNT($_POST['tblIncomingSummary']) > 0){
	foreach($_POST['tblIncomingSummary'] as $IncomingSummaryData){
		$iTotalQty = $iTotalQty + $IncomingSummaryData[8];
		$iTotalShipQty = $iTotalShipQty + $IncomingSummaryData[9];
		$iVendorValUSD = $iVendorValUSD + str_replace(",","",$IncomingSummaryData[10]);
		$iVendorValSGD = $iVendorValSGD + str_replace(",","",$IncomingSummaryData[11]);
		$iFrtCharges = $iFrtCharges + str_replace(",","",$IncomingSummaryData[12]);
		$iInsCharges = $iInsCharges + str_replace(",","",$IncomingSummaryData[13]);
		$iCIFAmount = $iCIFAmount + str_replace(",","",$IncomingSummaryData[14]);
		$iGSTAmount = $iGSTAmount + str_replace(",","",$IncomingSummaryData[15]);
	}
}

$iVendorValUSD = number_format($iVendorValUSD,2);
$iVendorValSGD = number_format($iVendorValSGD,2);
$iFrtCharges = number_format($iFrtCharges,2);
$iInsCharges = number_format($iInsCharges,2);
$iCIFAmount = number_format($iCIFAmount,2);
$iGSTAmount = number_format($iGSTAmount,2);

$phpExcel->createSheet();
$ActiveSheet = $phpExcel->setActiveSheetIndex(2);
$ActiveSheet->setTitle("Incoming GST Summary");
printHeaderDetails($ActiveSheet,$styleArrayHeader,$_POST['StartDate'],$_POST['EndDate'],$_POST['TaxRef'],$Customer,'INCOMING GST REPORT (SUMMARY)');
$tblIncomingSummaryHeader = array('SERIAL#','Customer','Shipment #','Shpmt Rcvd Date','Vendor/Supplier','Vendor Inv No.','Tax Reference','Permit No.','Total Qty','Total Qty(HAWB)','Vendor Inv Value (USD)','Vendor Inv Value (SGD)','Frt Charges (SGD)','Ins Charges (SGD)','CIF Amount (SGD)','GST Amount (SGD)','Job Type','HAWB No.','MAWB No.','Dep Date','Comment','Description','SU/Entry#.','Consignment order (Y/N)','Previous SU# for consignment');
//$tblIncomingSummaryFooter = array('SERIAL#','Customer','Shipment #','Shpmt Rcvd Date','Vendor/Supplier','Vendor Inv No.','Tax Reference','Permit No.','Total Qty : '.$iTotalQty,'Total Qty(HAWB) : '.$iTotalShipQty,'Vendor Inv Value (USD) : '.$iVendorValUSD,'Vendor Inv Value (SGD) : '.$iVendorValSGD,'Frt Charges (SGD) : '.$iFrtCharges,'Ins Charges (SGD) : '.$iInsCharges,'CIF Amount (SGD) : '.$iCIFAmount,'GST Amount (SGD) : '.$iGSTAmount,'Job Type','HAWB No.','MAWB No.','Dep Date','Comment','Description','SU/Entry#.','Consignment order (Y/N)','Previous SU# for consignment');
$tblIncomingSummaryFooter = array('','','','','','','','',$iTotalQty,$iTotalShipQty,$iVendorValUSD,$iVendorValSGD,$iFrtCharges,$iInsCharges,$iCIFAmount,$iGSTAmount,'','','','','','','','','');
PrintRow($ActiveSheet,$tblIncomingSummaryHeader,1,5,$styleArrayHeader,PHPExcel_Style_Alignment::HORIZONTAL_CENTER,"Incoming GST Summary");
PrintRow($ActiveSheet,$_POST['tblIncomingSummary'],1,6,'',PHPExcel_Style_Alignment::HORIZONTAL_CENTER,"Incoming GST Summary");
$countArray = COUNT($_POST['tblIncomingSummary']) + 6;
PrintRow($ActiveSheet,$tblIncomingSummaryFooter,1,$countArray,$styleArrayHeader,PHPExcel_Style_Alignment::HORIZONTAL_CENTER,"Incoming GST Summary");


$iTotalQty = 0;
$iTotalShipQty = 0;
$iCustInvValUSD = 0;
$iCustInvValSGD = 0;
$iGSTAmount = 0;
$iOtherCharges = 0;
$iTotalInvoiceValue = 0;
if(COUNT($_POST['tblOutgoingSummary']) > 0){
	foreach($_POST['tblOutgoingSummary'] as $OutgoingSummaryData){
		$iTotalQty = $iTotalQty + $OutgoingSummaryData[8];
		$iTotalShipQty = $iTotalShipQty + $OutgoingSummaryData[9];
		$iCustInvValUSD = $iCustInvValUSD + str_replace(",","",$OutgoingSummaryData[25]);
		$iCustInvValSGD = $iCustInvValSGD + str_replace(",","",$OutgoingSummaryData[26]);
		$iGSTAmount = $iGSTAmount + str_replace(",","",$OutgoingSummaryData[27]);
		$iOtherCharges = $iOtherCharges + str_replace(",","",$OutgoingSummaryData[28]);
		$iTotalInvoiceValue = $iTotalInvoiceValue + str_replace(",","",$OutgoingSummaryData[29]);
	}
}

$iCustInvValUSD = number_format($iCustInvValUSD,2);
$iCustInvValSGD = number_format($iCustInvValSGD,2);
$iGSTAmount = number_format($iGSTAmount,2);
$iOtherCharges = number_format($iOtherCharges,2);
$iTotalInvoiceValue = number_format($iTotalInvoiceValue,2);

$phpExcel->createSheet();
$ActiveSheet = $phpExcel->setActiveSheetIndex(3);
$ActiveSheet->setTitle("Outgoing GST Summary");
printHeaderDetails($ActiveSheet,$styleArrayHeader,$_POST['StartDate'],$_POST['EndDate'],$_POST['TaxRef'],$Customer,'OUTGOING GST REPORT (SUMMARY)');
$tblIncomingSummaryHeader = array('SERIAL#','Customer','Shipment #','Shipment Release Date.','Shipment No.','Vendor Inv No.','Vendor / Supplier','Comm Inv No/DN.','Total Qty','Total Qty(HAWB)','Unit Price','Out Tax Ref','Permit #','Exchange Rate','Dest Port','Forwarders','Consignee','HAWB No.','MAWB No.','Tpt Mode','Dep Date','Delivery From','Inco Terms','Job Type','Comment','Customer Inv Value(USD)','Customer Inv Value(SGD)','GST Amount(SGD)','Other Charges (SGD)','Total Invoice Value (SGD)','Total Value','OUT SU #','Description');
//$tblIncomingSummaryFooter = array('SERIAL#','Customer','Shipment #','Shipment Release Date.','Shipment No.','Vendor Inv No.','Vendor / Supplier','Comm Inv No/DN.','Total Qty : '.$iTotalQty,'Total Qty(HAWB) : '.$iTotalShipQty,'Unit Price','Out Tax Ref','Permit #','Exchange Rate','Dest Port','Forwarders','Consignee','HAWB No.','MAWB No.','Tpt Mode','Dep Date','Delivery From','Inco Terms','Job Type','Comment','Customer Inv Value(USD) : '.$iCustInvValUSD,'Customer Inv Value(SGD) : '.$iCustInvValSGD,'GST Amount(SGD) : '.$iGSTAmount,'Total Value','OUT SU #','Description');
$tblIncomingSummaryFooter = array('','','','','','','','',$iTotalQty,$iTotalShipQty,'','','','','','','','','','','','','','','',$iCustInvValUSD,$iCustInvValSGD,$iGSTAmount,$iOtherCharges,$iTotalInvoiceValue,'','','');
PrintRow($ActiveSheet,$tblIncomingSummaryHeader,1,5,$styleArrayHeader,PHPExcel_Style_Alignment::HORIZONTAL_CENTER,"Outgoing GST Summary");
PrintRow($ActiveSheet,$_POST['tblOutgoingSummary'],1,6,'',PHPExcel_Style_Alignment::HORIZONTAL_CENTER,"Outgoing GST Summary");
$countArray = COUNT($_POST['tblOutgoingSummary']) + 6;
PrintRow($ActiveSheet,$tblIncomingSummaryFooter,1,$countArray,$styleArrayHeader,PHPExcel_Style_Alignment::HORIZONTAL_CENTER,"Outgoing GST Summary");


$TotalOutgoingQty 	= 0;
$TotalIncomingQty 	= 0;
$TotalBalanceQty 	= 0;
if(COUNT($_POST['tblPicklistCount']) > 0){
	foreach($_POST['tblPicklistCount'] as $PicklistData){
		$TotalOutgoingQty = $TotalOutgoingQty + $PicklistData[11];
		$TotalIncomingQty = $TotalIncomingQty + $PicklistData[27];
		$TotalBalanceQty = $TotalBalanceQty +$PicklistData[28];
	}
}

$phpExcel->createSheet();
$ActiveSheet = $phpExcel->setActiveSheetIndex(4);
$ActiveSheet->setTitle("Shipment Picklist");
printHeaderDetails($ActiveSheet,$styleArrayHeader,$_POST['StartDate'],$_POST['EndDate'],$_POST['TaxRef'],$Customer,'Shipment Picklist');
$tblIncomingSummaryHeader = array('#','Out Reference #','Shipment Release Date','Out WMS Reference','Picklist No.','DN No.','Unit Price (USD)','Out HAWB No.','Out Permit No.','Part No.','Tax Scheme','Total Outgoing Qty','Out Box Entry/SU#','In Reference#','Shpmt Rcvd Date','In WMS Reference','Vendor / Supplier Name','Vendor Inv No','In HAWB No','In Permit No.','Part No.','Tax Scheme','Incoming (QTY','In Box Entry/SU#','Balance (QTY)','Bal Box Entry/SU#','Previous SU#');
//$tblIncomingSummaryFooter = array('#','Out Reference #','Shipment Release Date','Out WMS Reference','Picklist No.','DN No.','Unit Price (USD)','Out HAWB No.','Out Permit No.','Part No.','Tax Scheme','Total Outgoing Qty : '.$TotalOutgoingQty,'Out Box Entry/SU#','In Reference#','Shpmt Rcvd Date','In WMS Reference','Vendor / Supplier Name','Vendor Inv No','In HAWB No','In Permit No.','Part No.','Tax Scheme','Incoming (QTY) : '.$TotalIncomingQty,'In Box Entry/SU#','Balance (QTY) : '.$TotalBalanceQty,'Bal Box Entry/SU#','Previous SU#');
$tblIncomingSummaryFooter = array('','','','','','','','','','','',$TotalOutgoingQty,'','','','','','','','','','',$TotalIncomingQty,'',$TotalBalanceQty,'','');
PrintRow($ActiveSheet,$tblIncomingSummaryHeader,1,5,$styleArrayHeader,PHPExcel_Style_Alignment::HORIZONTAL_CENTER,"Shipment Picklist");
PrintRow($ActiveSheet,$_POST['tblPicklistCount'],1,6,'',PHPExcel_Style_Alignment::HORIZONTAL_CENTER,"Shipment Picklist");
$countArray = COUNT($_POST['tblPicklistCount']) + 6;
PrintRow($ActiveSheet,$tblIncomingSummaryFooter,1,$countArray,$styleArrayHeader,PHPExcel_Style_Alignment::HORIZONTAL_CENTER,"Shipment Picklist Footer");


$TotalIncomingQty 	= 0;
$TotalOutgoingQty 	= 0;
$TotalBalanceQty 	= 0;
if(COUNT($_POST['tblShipmentSummaryCount']) > 0){
	foreach($_POST['tblShipmentSummaryCount'] as $ShipmentSummaryData){
		$TotalIncomingQty = $TotalIncomingQty + $ShipmentSummaryData[26];
		$TotalOutgoingQty = $TotalOutgoingQty + $ShipmentSummaryData[22];
		$TotalBalanceQty = $TotalBalanceQty +$ShipmentSummaryData[27];
	}
}

$phpExcel->createSheet();
$ActiveSheet = $phpExcel->setActiveSheetIndex(5);
$ActiveSheet->setTitle("Shipment Summary");
printHeaderDetails($ActiveSheet,$styleArrayHeader,$_POST['StartDate'],$_POST['EndDate'],$_POST['TaxRef'],$_POST['Customer'],'Shipment Summary');
$tblIncomingSummaryHeader = array('#','In Reference#','Shpmt Rcvd Date','In WMS Reference','Vendor / Supplier Name','Vendor Inv No','In HAWB No','In Permit No.','Part No.','Tax Scheme','Incoming (QTY)','In Box Entry/SU#','Out Reference #','Shipment Release Date','Out WMS Reference','Picklist No.','DN No.','Unit Price (USD)','Out HAWB No.','Out Permit No.','Part No.','Tax Scheme','Total Outgoing Qty','Out Box Entry/SU#','Balance (QTY)','Bal Box Entry/SU#');
//$tblIncomingSummaryFooter = array('#','In Reference#','Shpmt Rcvd Date','In WMS Reference','Vendor / Supplier Name','Vendor Inv No','In HAWB No','In Permit No.','Part No.','Tax Scheme','Incoming (QTY) : '.$TotalIncomingQty ,'In Box Entry/SU#','Out Reference #','Shipment Release Date','Out WMS Reference','Picklist No.','DN No.','Unit Price (USD)','Out HAWB No.','Out Permit No.','Part No.','Tax Scheme','Total Outgoing Qty : '.$TotalOutgoingQty, 'Out Box Entry/SU#','Balance (QTY) : '.$TotalBalanceQty, 'Bal Box Entry/SU#');
$tblIncomingSummaryFooter = array('','','','','','','','','','',$TotalIncomingQty ,'','','','','','','','','','','',$TotalOutgoingQty, '',$TotalBalanceQty, '');
PrintRow($ActiveSheet,$tblIncomingSummaryHeader,1,5,$styleArrayHeader,PHPExcel_Style_Alignment::HORIZONTAL_CENTER,"Shipment Summary");
PrintRow($ActiveSheet,$_POST['tblShipmentSummaryCount'],1,6,'',PHPExcel_Style_Alignment::HORIZONTAL_CENTER,"Shipment Summary");
$countArray = COUNT($_POST['tblShipmentSummaryCount']) + 6;
PrintRow($ActiveSheet,$tblIncomingSummaryFooter,1,$countArray,$styleArrayHeader,PHPExcel_Style_Alignment::HORIZONTAL_CENTER,"Shipment Summary Footer");

function printHeaderDetails($ActiveSheet,$Style,$StartDate,$EndDate,$TaxRef,$Customer,$TabTitle){
	$ActiveSheet->mergeCells("A1:D1");
	$ActiveSheet->setCellValue("A1", $TabTitle);
	$ActiveSheet->getStyle("A1")->applyFromArray($Style); //Title
	
	//$ActiveSheet->getStyle('A1:AE1')->getAlignment()->setWrapText(true); 
	//$ActiveSheet->getColumnDimension("A1:Z1")->setAutoSize(true);
	//$ActiveSheet->getColumnDimension("H1")->setAutoSize(true);
	//$ActiveSheet->getStyle("A1")->getAlignment()->setWrapText(true);
	//$sheet->getStyle("D$row")->getAlignment()->setWrapText(true);
	//$ActiveSheet->getColumnDimension("A1")->setWidth(200);
	
	$ActiveSheet->setCellValue("H1", "Printed: ".date("m/d/Y"));
	$ActiveSheet->setCellValue("H2", "Time: ".date("h:i:s a")); 
	$ActiveSheet->setCellValue("H3", "Customer: $Customer");
	
}

function msort($array, $key, $sort_flags = SORT_REGULAR) {
    if (is_array($array) && count($array) > 0) {
        if (!empty($key)) {
            $mapping = array();
            foreach ($array as $k => $v) {
                $sort_key = '';
                if (!is_array($key)) {
                    $sort_key = $v[$key];
                } else {
                    // @TODO This should be fixed, now it will be sorted as string
                    foreach ($key as $key_key) {
                        $sort_key .= $v[$key_key];
                    }
                    $sort_flags = SORT_STRING;
                }
                $mapping[$k] = $sort_key;
            }
            asort($mapping, $sort_flags);
            $sorted = array();
            foreach ($mapping as $k => $v) {
                $sorted[] = $array[$k];
            }
            return $sorted;
        }
    }
    return $array;
}

function PrintRow($ActiveSheet,$StringArray,$IntAsciiChar,$StartRow,$Style,$Alignment,$ReportTitle){
	$InitialKeyAscii 	= $IntAsciiChar;
	$InitialRow			= $StartRow;
	
	$Serialno 			= 1;
	
	$AsciiChar 			= $IntAsciiChar;
	
	$TotalIncomingQty 	= 0;
	$TotalOutgoingQty 	= 0;
	$TotalBalanceQty 	= 0;
	
	foreach($StringArray as $keyStr => $keyValue){
		if (is_array($keyValue)){
			$AsciiChar = $IntAsciiChar;
			$columnCtr1 = 0;
			
			foreach($keyValue as $KeyValue1){
				
				if (trim($KeyValue1) != ''){
					
					$isFloat = false;
					
					if (isValidDateTime($KeyValue1)){
						$KeyValue1 = date("d/m/Y",strtotime($KeyValue1));
					}
					
					if ($KeyValue1 == 'null'){
						$KeyValue1 = '';
					}
					
					$isInvValue = false;
					$isExchangeRate = false;
					$isHAWB = false;
					$isInvUSD = false;
					$isInvSGD = false;
					$isGSTAmount = false;
					if($ReportTitle == "Incoming GST Detailed" && $columnCtr1 == 9){
						$isExchangeRate = true;
					}
					
					if($ReportTitle == "Incoming GST Detailed" && ($columnCtr1 == 10 || $columnCtr1 == 11 || $columnCtr1 == 12 || $columnCtr1 == 13 || $columnCtr1 == 14 || $columnCtr1 == 15)){
						$isInvValue = true;
					}
					
					if($ReportTitle == "Incoming GST Summary" && ($columnCtr1 == 10 || $columnCtr1 == 11 || $columnCtr1 == 12 || $columnCtr1 == 13 || $columnCtr1 == 14 || $columnCtr1 == 15)){
						$isInvValue = true;
					}
					
					//if($ReportTitle == "Incoming GST Summary" && $columnCtr1 == 27){
					//	$isExchangeRate = true;
					//}
					if($ReportTitle == "Outgoing GST Detailed" && $columnCtr1 == 14){
						$isExchangeRate = true;
					}
					
					if($ReportTitle == "Outgoing GST Detailed" && $columnCtr1 == 26){
						$isInvUSD = true;
					}
					
					if($ReportTitle == "Outgoing GST Detailed" && $columnCtr1 == 27){
						$isInvSGD = true;
					}
					
					if($ReportTitle == "Outgoing GST Detailed" && $columnCtr1 == 28){
						$isGSTAmount = true;
					}
					
					if($ReportTitle == "Outgoing GST Detailed" && $columnCtr1 == 29){
						$isInvSGD = true;
					}
					
					if($ReportTitle == "Outgoing GST Detailed" && $columnCtr1 == 30){
						$isInvSGD = true;
					}
					
					if($ReportTitle == "Outgoing GST Summary" && $columnCtr1 == 13){
						$isExchangeRate = true;
					}
					
					if($ReportTitle == "Outgoing GST Summary" && $columnCtr1 == 25){
						$isInvUSD = true;
					}
					
					if($ReportTitle == "Outgoing GST Summary" && $columnCtr1 == 26){
						$isInvSGD = true;
					}
					
					if($ReportTitle == "Outgoing GST Summary" && $columnCtr1 == 27){
						$isGSTAmount = true;
					}
					
					if($ReportTitle == "Outgoing GST Summary" && $columnCtr1 == 28){
						$isInvSGD = true;
					}
					
					if($ReportTitle == "Outgoing GST Summary" && $columnCtr1 == 29){
						$isInvSGD = true;
					}
					
					if($ReportTitle == "Outgoing GST Detailed" && $columnCtr1 == 18){
						$isHAWB = true;
					}
					if($ReportTitle == "Outgoing GST Summary" && $columnCtr1 == 17){
						$isHAWB = true;
					}
					
					if($ReportTitle == "Shipment Picklist" && $columnCtr1 == 27){
						$TotalIncomingQty = $TotalIncomingQty + $KeyValue1;
						$KeyValue1 = '';
					}
					
					if($ReportTitle == "Shipment Picklist" && $columnCtr1 == 28){
						$TotalBalanceQty = $TotalBalanceQty + $KeyValue1;
						$KeyValue1 = '';
					}
					
					if($ReportTitle == "Shipment Summary" && $columnCtr1 == 26){
						$TotalIncomingQty = $TotalIncomingQty + $KeyValue1;
						$KeyValue1 = '';
					}
					
					if($ReportTitle == "Shipment Summary" && $columnCtr1 == 27){
						$TotalBalanceQty = $TotalBalanceQty + $KeyValue1;
						$KeyValue1 = '';
					}
					
					if ($AsciiChar == 1){
						$ActiveSheet->setCellValue(getNameFromNumber($AsciiChar).$StartRow,$Serialno);
						$Serialno++;
					} else {
						$ActiveSheet->setCellValue(getNameFromNumber($AsciiChar).$StartRow,$KeyValue1);
						//$Serialno
					}
					
					if($isHAWB){
						$ActiveSheet->setCellValueExplicit(getNameFromNumber($AsciiChar).$StartRow, $KeyValue1,PHPExcel_Cell_DataType::TYPE_STRING);
					}
					
					if(is_numeric($KeyValue1) and !is_float($KeyValue1) and $KeyValue1 > 99999 and !$isHAWB){
						$ActiveSheet->getStyle(getNameFromNumber($AsciiChar).$StartRow)->getNumberFormat()->setFormatCode('#0');
					}
					
					if(is_numeric($KeyValue1) and $isExchangeRate){
						$ActiveSheet->getStyle(getNameFromNumber($AsciiChar).$StartRow)->getNumberFormat()->setFormatCode('0.0000');
					} else if(is_numeric($KeyValue1) and (preg_match('/^\d+\.{1}\d+$/', $KeyValue1) or $isInvUSD or $isInvSGD or $isGSTAmount) and !$isHAWB){
						$ActiveSheet->getStyle(getNameFromNumber($AsciiChar).$StartRow)->getNumberFormat()->setFormatCode('0.00');
					}
					
					//if(is_numeric($KeyValue1) and $KeyValue1 == '0'){
					if($isInvValue){
						$ActiveSheet->getStyle(getNameFromNumber($AsciiChar).$StartRow)->getNumberFormat()->setFormatCode('0.00');
					}
					
				}
				$AsciiChar++;
				$columnCtr1++;
			}
			$StartRow++;
		} else {
			
			if($ReportTitle == "Incoming GST Detailed" && ($AsciiChar == 11 || $AsciiChar == 12 || $AsciiChar == 13 || $AsciiChar == 14 || $AsciiChar == 15 || $AsciiChar == 16)){
				$ActiveSheet->getStyle(getNameFromNumber($AsciiChar).$StartRow)->getNumberFormat()->setFormatCode('0.00');
			}
			
			if($ReportTitle == "Incoming GST Summary" && ($AsciiChar == 11 || $AsciiChar == 12 || $AsciiChar == 13 || $AsciiChar == 14 || $AsciiChar == 15 || $AsciiChar == 16)){
				$ActiveSheet->getStyle(getNameFromNumber($AsciiChar).$StartRow)->getNumberFormat()->setFormatCode('0.00');
			}
			
			if($ReportTitle == "Outgoing GST Detailed" && ($AsciiChar == 27 || $AsciiChar == 28 || $AsciiChar == 29 || $AsciiChar == 30 || $AsciiChar == 31)){
				$ActiveSheet->getStyle(getNameFromNumber($AsciiChar).$StartRow)->getNumberFormat()->setFormatCode('0.00');
			}
			
			if($ReportTitle == "Outgoing GST Summary" && ($AsciiChar == 26 || $AsciiChar == 27 || $AsciiChar == 28 || $AsciiChar == 29 || $AsciiChar == 30)){
				$ActiveSheet->getStyle(getNameFromNumber($AsciiChar).$StartRow)->getNumberFormat()->setFormatCode('0.00');
			}
			
			$ActiveSheet->setCellValue(getNameFromNumber($AsciiChar).$StartRow, $keyValue);
			$AsciiChar++;
		}
	}
	if (isset($Style) and strlen($Style) > 0){
		$ActiveSheet->getStyle(getNameFromNumber($InitialKeyAscii).$InitialRow.':'.getNameFromNumber($AsciiChar).$StartRow)->applyFromArray($Style);
	}
	
	if (isset($Alignment)){
		$ActiveSheet->getStyle(getNameFromNumber($InitialKeyAscii).$InitialRow.':'.getNameFromNumber($AsciiChar).$StartRow)->getAlignment()->setHorizontal($Alignment);
	}
}

function getNameFromNumber($num) {
    $numeric = ($num - 1) % 26;
    $letter = chr(65 + $numeric);
    $num2 = intval(($num - 1) / 26);
    if ($num2 > 0) {
        return getNameFromNumber($num2) . $letter;
    } else {
        return $letter;
    }
}

function isValidDateTime($dateTime){
    if (preg_match("/^(\d{4})-(\d{2})-(\d{2}) ([01][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/", $dateTime, $matches)) {
        if (checkdate($matches[2], $matches[3], $matches[1])) {
            return true;
        }
    }

    return false;
}

$phpExcel->setActiveSheetIndex(0);

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="GSTReport.xls"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel5');
$objWriter->save('php://output');


exit;
?>

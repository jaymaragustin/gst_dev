<?php
if (isset($_POST['todo'])){
	
	require_once "../../../widgets/JSIMailClass.php";
	
	require ('../../include/config.php');
	$ExportGSTClass = new ExportGSTClass($_REQUEST);
	
	header('Content-Type: application/json');
	if ($_POST['todo'] == 'DataSearch'){
		echo $ExportGSTClass->DataSearch();
	} else if ($_POST['todo'] == 'prepareExportModule'){
		echo $ExportGSTClass->prepareExportModule();
	} else if ($_POST['todo'] == 'GetAvailablePacklistNumber'){
		echo $ExportGSTClass->GetAvailablePacklistNumber();
	} else if ($_POST['todo'] == 'ProcessData'){
		echo $ExportGSTClass->ProcessData();
	} else if ($_POST['todo'] == 'UpdateReference'){
		echo $ExportGSTClass->UpdateReference();
	} else if ($_POST['todo'] == 'UpdateReferenceNull'){
		echo $ExportGSTClass->UpdateReferenceNull();
	} else if ($_POST['todo'] == 'getPacklistData'){
		echo $ExportGSTClass->getPacklistData();
	} else if ($_POST['todo'] == 'GetAvailableShipmentNumber'){
		echo $ExportGSTClass->GetAvailableShipmentNumber();
	} else if ($_POST['todo'] == 'ADIGetShipment'){
		echo $ExportGSTClass->ADIGetShipment();
	} else if ($_POST['todo'] == 'ProcessADIData'){
		echo $ExportGSTClass->ProcessADIData();
	} else if ($_POST['todo'] == 'DataADISearch'){
		echo $ExportGSTClass->DataADISearch();
	} else if ($_POST['todo'] == 'ADIGetEditShipment'){
		echo $ExportGSTClass->ADIGetEditShipment();
	} else if ($_POST['todo'] == 'checkeEditmode'){
		echo $ExportGSTClass->checkeEditmode();
	} else if ($_POST['todo'] == 'validateOTP'){
		echo $ExportGSTClass->validateOTP();
	} else if ($_POST['todo'] == 'validateChanges'){
		echo $ExportGSTClass->validateChanges();
	}
}

Class ExportGSTClass{
	var $PostVars;
	function __construct($vars){
		$this->PostVars = $vars;
		$serverConn = unserialize(base64_decode(SQL_CONN));
		$this->conn = mssql_connect($serverConn[0],$serverConn[1],$serverConn[2]);
		mssql_select_db("Import");
		
		foreach($this->PostVars as $a => $b){
			$this->PostVars[$a] = $this->mssqlQuotedString($b);
		}
	}
	
	function mssqlQuotedString($str){
		$str = str_replace("\\", "\\\\", $str);
		$str = str_replace("'", "''", $str);
		
		return $str;
	}
	
	function getDestination(){
		mssql_select_db("STGMM");
		$sql = mssql_query("select country_code from country order by country_code ASC");
		while($row[] = mssql_fetch_assoc($sql)){}
		array_pop($row);
		
		return $row;
	}
	
	function prepareExportModule(){
		mssql_select_db("Import");
		$data = array();
		
		$sql = mssql_query("select GSTIndex AS WawiIndex,WawiAlias from MasterData.dbo.WawiCustomer where MasterData.dbo.WawiCustomer.GST = 1 and MasterData.dbo.WawiCustomer.status = 1 group by GSTIndex,WawiAlias order by GSTIndex");
		while($CustomerList[] = mssql_fetch_assoc($sql)){}
		array_pop($CustomerList);
		
		$sql = mssql_query("select top 2 ShipmentNumber from v_ol_ExportShipment");
		
		if(mssql_num_rows($sql) > 0){
			$chkrecords = 'data';
		} else {
			$chkrecords = 'nodata';
		}
		
		unset($sql);
		$sql = mssql_query("select Forwarder from Forwarder where [Status] = 1 order by Forwarder ASC");
		$ListofFrowarder = array();
		while($row = mssql_fetch_assoc($sql)){
			$ListofFrowarder[$row['Forwarder']] = $row['Forwarder'];
		}

		unset($sql);
		$sql = mssql_query("select Supplier from Supplier where [Status] = 1 order by Forwarder ASC");
		$ListofSupplier = array();
		while($row = mssql_fetch_assoc($sql)){
			$ListofSupplier[$row['Supplier']] = $row['Supplier'];
		}
		
		array_push($data,$CustomerList);
		array_push($data,$this->GSTRate());
		array_push($data,$chkrecords);
		array_push($data,$ListofFrowarder);
		array_push($data,$this->getDestination());

		return json_encode($data);
	}
	
	function ListCurrency(){
		$sql = mssql_query("SELECT DISTINCT Currency FROM Country ORDER BY Currency");
		if (mssql_num_rows($sql) > 0){
			while($CurrencyList[] = mssql_fetch_assoc($sql)){}
			array_pop($CurrencyList);
		} else {
			$CurrencyList = array(array('Currency' => 'SGD'),array('Currency' => 'USD'));
		}
		
		return $CurrencyList;
	}
	
	function GSTRate(){
		$sql = mssql_query("SELECT Value FROM Setup WHERE Entry='GST'");
		if (mssql_num_rows($sql) > 0){
			$sql = mssql_fetch_assoc($sql);
			$GSTRate = $sql['Value'];
		} else {
			$GSTRate = 7;
		}
		
		return $GSTRate;
	}
	
	function DataSearch(){
		if ($this->PostVars['SearchAction'] == 'First'){ // First run
			$sql = mssql_query("select *,convert(text,CustInvNo1) as CustInvNo from v_ol_ExportShipment where ShipmentNumber = (select top 1 ShipmentNumber from v_ol_ExportShipment order by ShipmentNumber ASC)");
		} else if($this->PostVars['SearchAction'] == 'Next'){
			$sql = mssql_query("select *,convert(text,CustInvNo1) as CustInvNo from v_ol_ExportShipment where ShipmentNumber = (select top 1 ShipmentNumber from v_ol_ExportShipment where ShipmentNumber > ".$this->PostVars['ShipmentNumber']." order by ShipmentNumber ASC)");
		} else if($this->PostVars['SearchAction'] == 'Previous'){
			$sql = mssql_query("select *,convert(text,CustInvNo1) as CustInvNo from v_ol_ExportShipment where ShipmentNumber = (select top 1 ShipmentNumber from v_ol_ExportShipment where ShipmentNumber < ".$this->PostVars['ShipmentNumber']." order by ShipmentNumber DESC)");
		} else if($this->PostVars['SearchAction'] == 'Last'){
			$sql = mssql_query("select *,convert(text,CustInvNo1) as CustInvNo from v_ol_ExportShipment where ShipmentNumber = (select top 1 ShipmentNumber from v_ol_ExportShipment order by ShipmentNumber DESC)");
		} else if($this->PostVars['SearchAction'] == 'Search'){
			$sql = mssql_query("select *,convert(text,CustInvNo1) as CustInvNo from v_ol_ExportShipment where ShipmentNumber = ".$this->PostVars['ShipmentNumber']);
		}

		if (mssql_num_rows($sql) < 1){
			return json_encode($this->PostVars['SearchAction']);
		} else {
			$data = array();
			$data1 = array();
			
			while($row = mssql_fetch_assoc($sql)){
				$sqlplist = mssql_query($row['plist']);
				$WawiIndex = $row['WawiIndex'];


				if (mssql_num_rows($sqlplist) > 0){
					unset($rowPlist);
					$rowPlist = array();
					while($rowPerPlist = mssql_fetch_assoc($sqlplist)){
						//WawiIndex = '$WawiIndex' add 
						$sqlGetFromExportInvValue = mssql_fetch_assoc(mssql_query("select InvoiceValue from export where reference = ".$row['Packlist']." and TaxScheme = '".$rowPerPlist['TaxScheme']."' and WawiIndex = '$WawiIndex' " ));
						
						$rowPerPlist['InvoiceValue'] = number_format($sqlGetFromExportInvValue['InvoiceValue'],2, '.', '');
						array_push($rowPlist,$rowPerPlist);
					}
					
					$tmpPlist[$row['Packlist']] = $rowPlist;
				} else {
					$tmpPlist[$row['Packlist']] = 'nodata';
				}
				
				array_push($data,$row);
			}
			
			array_push($data1,$data);
			array_push($data1,$tmpPlist);
			array_push($data1,$this->AvailablePlist($WawiIndex));
			
			return json_encode($data1);
		}
	}
	
	function DataADISearch(){
		if ($this->PostVars['SearchAction'] == 'First'){ // First run
			$sql = mssql_query("SELECT PackListPosition.PackListNumber, [Order].CustomerReference as DN, [SAPDailyShipment].[ShipmentNumber], Entry.TaxScheme, [Order].[ShippingCountry], CONVERT(VARCHAR(10), [Order].ShippedDateTime, 103) as CargoReleasedDate, [Order].GSTIncoTerm as IncoTerm, [Order].[Forwarder], Outgoing_ADIShipment.HAWB, [Order].CustomerName1 as ShipTo, PackListPosition.Partnumber, SUM(PackListPosition.Quantity) AS TotalQty, [SAPDailyShipment].[UnitPrice],
								(SUM(PackListPosition.Quantity) * [SAPDailyShipment].[UnitPrice]) AS [ShipmentValue], [Entry].[Invoice],Outgoing_ADIShipment.PermitNo, CONVERT(VARCHAR(10), Outgoing_ADIShipment.PermitDate, 103) as PermitDate, Outgoing_ADIShipment.JobType, Outgoing_ADIShipment.Comment,Outgoing_ADIShipment.CustInvValue, Outgoing_ADIShipment.ExchangeRate, Outgoing_ADIShipment.InvoiceValueSGD, Outgoing_ADIShipment.GSTAmountValue, (CASE WHEN ([DockReceiving].Supplier IS NULL) THEN (DR.Supplier) ELSE [DockReceiving].Supplier END) AS Supplier, Outgoing_ADIShipment.MAWB, Outgoing_ADIShipment.Flight, CONVERT(VARCHAR(10), Outgoing_ADIShipment.DepDate, 103) as DepDate, Outgoing_ADIShipment.DeliveryFrom, Outgoing_ADIShipment.OtherCharge, Outgoing_ADIShipment.TotalInvoiceValue
								FROM ADISGWMS.dbo.PackListPosition
								INNER JOIN ADISGWMS.dbo.PackList ON PackList.PackListNumber = PackListPosition.PackListNumber AND PackList.DocumentNumber = PackListPosition.DocumentNumber INNER JOIN ADISGWMS.dbo.[Order] ON [Order].OrderNumber = PackList.DocumentNumber INNER JOIN ADISGWMS.dbo.Entry ON PackListPosition.EntryNumber = Entry.EntryNumber INNER JOIN ADISGWMS.dbo.SAPDailyShipment ON CONVERT(VARCHAR, [SAPDailyShipment].[DN]) = [Order].CustomerReference AND [SAPDailyShipment].[PartNumber] = PackListPosition.Partnumber
								INNER JOIN Import.dbo.Outgoing_ADIShipment ON Outgoing_ADIShipment.ShipmentNo = [SAPDailyShipment].[ShipmentNumber] LEFT OUTER JOIN ADISGWMS.dbo.[DockReceiving] WITH (NOLOCK) ON [Entry].ForwarderRef = DockReceiving.IncomingRef AND DockReceiving.IncomingRef != 0 AND DockReceiving.IncomingRef IS NOT NULL
								LEFT OUTER JOIN ADISGWMS.dbo.[Entry] AS EE ON EE.[EntryNumber] = [Entry].[Masterbox] LEFT OUTER JOIN ADISGWMS.dbo.[DockReceiving] AS DR ON EE.ForwarderRef = DR.IncomingRef AND DR.IncomingRef != 0 AND DR.IncomingRef IS NOT NULL
								WHERE Import.dbo.Outgoing_ADIShipment.[EnterBy] != 'FromMigration' AND LEN([Order].OrderNumber) > 6
								GROUP BY PackListPosition.PackListNumber, [Order].CustomerReference, Entry.TaxScheme, [Order].DNStatus, CONVERT(VARCHAR(10), [Order].ShippedDateTime, 103), [Order].GSTIncoTerm, [Order].CustomerName1, [SAPDailyShipment].[ShipmentNumber], [SAPDailyShipment].[UnitPrice], [SAPDailyShipment].[ShipmentValue], [Entry].[Invoice], [Order].[ShippingCountry], [Order].[Forwarder], Outgoing_ADIShipment.HAWB, PackListPosition.Partnumber,Outgoing_ADIShipment.PermitNo, CONVERT(VARCHAR(10), Outgoing_ADIShipment.PermitDate, 103), Outgoing_ADIShipment.JobType, Outgoing_ADIShipment.Comment,Outgoing_ADIShipment.CustInvValue, Outgoing_ADIShipment.ExchangeRate, Outgoing_ADIShipment.InvoiceValueSGD, Outgoing_ADIShipment.GSTAmountValue, (CASE WHEN ([DockReceiving].Supplier IS NULL) THEN (DR.Supplier) ELSE [DockReceiving].Supplier END), Outgoing_ADIShipment.MAWB, Outgoing_ADIShipment.Flight, CONVERT(VARCHAR(10), Outgoing_ADIShipment.DepDate, 103), Outgoing_ADIShipment.DeliveryFrom, Outgoing_ADIShipment.OtherCharge, Outgoing_ADIShipment.TotalInvoiceValue
								Having [Order].DNStatus = 'CARGO RELEASED' AND [SAPDailyShipment].[ShipmentNumber] = (select top 1 ShipmentNo from Import.dbo.[Outgoing_ADIShipment] order by ShipmentNo ASC)");
		} else if($this->PostVars['SearchAction'] == 'Next'){
			$sql = mssql_query("SELECT PackListPosition.PackListNumber, [Order].CustomerReference as DN, [SAPDailyShipment].[ShipmentNumber], Entry.TaxScheme, [Order].[ShippingCountry], CONVERT(VARCHAR(10), [Order].ShippedDateTime, 103) as CargoReleasedDate, [Order].GSTIncoTerm as IncoTerm, [Order].[Forwarder], Outgoing_ADIShipment.HAWB, [Order].CustomerName1 as ShipTo, PackListPosition.Partnumber, SUM(PackListPosition.Quantity) AS TotalQty, [SAPDailyShipment].[UnitPrice],
								(SUM(PackListPosition.Quantity) * [SAPDailyShipment].[UnitPrice]) AS [ShipmentValue], [Entry].[Invoice],Outgoing_ADIShipment.PermitNo, CONVERT(VARCHAR(10), Outgoing_ADIShipment.PermitDate, 103) as PermitDate, Outgoing_ADIShipment.JobType, Outgoing_ADIShipment.Comment,Outgoing_ADIShipment.CustInvValue, Outgoing_ADIShipment.ExchangeRate, Outgoing_ADIShipment.InvoiceValueSGD, Outgoing_ADIShipment.GSTAmountValue, (CASE WHEN ([DockReceiving].Supplier IS NULL) THEN (DR.Supplier) ELSE [DockReceiving].Supplier END) AS Supplier, Outgoing_ADIShipment.MAWB, Outgoing_ADIShipment.Flight, CONVERT(VARCHAR(10), Outgoing_ADIShipment.DepDate, 103) as DepDate, Outgoing_ADIShipment.DeliveryFrom, Outgoing_ADIShipment.OtherCharge, Outgoing_ADIShipment.TotalInvoiceValue
								FROM ADISGWMS.dbo.PackListPosition
								INNER JOIN ADISGWMS.dbo.PackList ON PackList.PackListNumber = PackListPosition.PackListNumber AND PackList.DocumentNumber = PackListPosition.DocumentNumber INNER JOIN ADISGWMS.dbo.[Order] ON [Order].OrderNumber = PackList.DocumentNumber INNER JOIN ADISGWMS.dbo.Entry ON PackListPosition.EntryNumber = Entry.EntryNumber INNER JOIN ADISGWMS.dbo.SAPDailyShipment ON CONVERT(VARCHAR, [SAPDailyShipment].[DN]) = [Order].CustomerReference AND [SAPDailyShipment].[PartNumber] = PackListPosition.Partnumber
								INNER JOIN Import.dbo.Outgoing_ADIShipment ON Outgoing_ADIShipment.ShipmentNo = [SAPDailyShipment].[ShipmentNumber] LEFT OUTER JOIN ADISGWMS.dbo.[DockReceiving] WITH (NOLOCK) ON [Entry].ForwarderRef = DockReceiving.IncomingRef AND DockReceiving.IncomingRef != 0 AND DockReceiving.IncomingRef IS NOT NULL
								LEFT OUTER JOIN ADISGWMS.dbo.[Entry] AS EE ON EE.[EntryNumber] = [Entry].[Masterbox] LEFT OUTER JOIN ADISGWMS.dbo.[DockReceiving] AS DR ON EE.ForwarderRef = DR.IncomingRef AND DR.IncomingRef != 0 AND DR.IncomingRef IS NOT NULL
								WHERE Import.dbo.Outgoing_ADIShipment.[EnterBy] != 'FromMigration' AND LEN([Order].OrderNumber) > 6
								GROUP BY PackListPosition.PackListNumber, [Order].CustomerReference, Entry.TaxScheme, [Order].DNStatus, CONVERT(VARCHAR(10), [Order].ShippedDateTime, 103), [Order].GSTIncoTerm, [Order].CustomerName1, [SAPDailyShipment].[ShipmentNumber], [SAPDailyShipment].[UnitPrice], [SAPDailyShipment].[ShipmentValue], [Entry].[Invoice], [Order].[ShippingCountry], [Order].[Forwarder], Outgoing_ADIShipment.HAWB, PackListPosition.Partnumber,Outgoing_ADIShipment.PermitNo, CONVERT(VARCHAR(10), Outgoing_ADIShipment.PermitDate, 103), Outgoing_ADIShipment.JobType, Outgoing_ADIShipment.Comment,Outgoing_ADIShipment.CustInvValue, Outgoing_ADIShipment.ExchangeRate, Outgoing_ADIShipment.InvoiceValueSGD, Outgoing_ADIShipment.GSTAmountValue, (CASE WHEN ([DockReceiving].Supplier IS NULL) THEN (DR.Supplier) ELSE [DockReceiving].Supplier END), Outgoing_ADIShipment.MAWB, Outgoing_ADIShipment.Flight, CONVERT(VARCHAR(10), Outgoing_ADIShipment.DepDate, 103), Outgoing_ADIShipment.DeliveryFrom, Outgoing_ADIShipment.OtherCharge, Outgoing_ADIShipment.TotalInvoiceValue
								Having [Order].DNStatus = 'CARGO RELEASED' AND [SAPDailyShipment].[ShipmentNumber] = (select top 1 ShipmentNo from Import.dbo.[Outgoing_ADIShipment] where ShipmentNo > ".$this->PostVars['ShipmentNumber']." order by ShipmentNo ASC)");
		} else if($this->PostVars['SearchAction'] == 'Previous'){
			$sql = mssql_query("SELECT PackListPosition.PackListNumber, [Order].CustomerReference as DN, [SAPDailyShipment].[ShipmentNumber], Entry.TaxScheme, [Order].[ShippingCountry], CONVERT(VARCHAR(10), [Order].ShippedDateTime, 103) as CargoReleasedDate, [Order].GSTIncoTerm as IncoTerm, [Order].[Forwarder], Outgoing_ADIShipment.HAWB, [Order].CustomerName1 as ShipTo, PackListPosition.Partnumber, SUM(PackListPosition.Quantity) AS TotalQty, [SAPDailyShipment].[UnitPrice],
								(SUM(PackListPosition.Quantity) * [SAPDailyShipment].[UnitPrice]) AS [ShipmentValue], [Entry].[Invoice],Outgoing_ADIShipment.PermitNo, CONVERT(VARCHAR(10), Outgoing_ADIShipment.PermitDate, 103) as PermitDate, Outgoing_ADIShipment.JobType, Outgoing_ADIShipment.Comment,Outgoing_ADIShipment.CustInvValue, Outgoing_ADIShipment.ExchangeRate, Outgoing_ADIShipment.InvoiceValueSGD, Outgoing_ADIShipment.GSTAmountValue, (CASE WHEN ([DockReceiving].Supplier IS NULL) THEN (DR.Supplier) ELSE [DockReceiving].Supplier END) AS Supplier, Outgoing_ADIShipment.MAWB, Outgoing_ADIShipment.Flight, CONVERT(VARCHAR(10), Outgoing_ADIShipment.DepDate, 103) as DepDate, Outgoing_ADIShipment.DeliveryFrom, Outgoing_ADIShipment.OtherCharge, Outgoing_ADIShipment.TotalInvoiceValue
								FROM ADISGWMS.dbo.PackListPosition
								INNER JOIN ADISGWMS.dbo.PackList ON PackList.PackListNumber = PackListPosition.PackListNumber AND PackList.DocumentNumber = PackListPosition.DocumentNumber INNER JOIN ADISGWMS.dbo.[Order] ON [Order].OrderNumber = PackList.DocumentNumber INNER JOIN ADISGWMS.dbo.Entry ON PackListPosition.EntryNumber = Entry.EntryNumber INNER JOIN ADISGWMS.dbo.SAPDailyShipment ON CONVERT(VARCHAR, [SAPDailyShipment].[DN]) = [Order].CustomerReference AND [SAPDailyShipment].[PartNumber] = PackListPosition.Partnumber
								INNER JOIN Import.dbo.Outgoing_ADIShipment ON Outgoing_ADIShipment.ShipmentNo = [SAPDailyShipment].[ShipmentNumber] LEFT OUTER JOIN ADISGWMS.dbo.[DockReceiving] WITH (NOLOCK) ON [Entry].ForwarderRef = DockReceiving.IncomingRef AND DockReceiving.IncomingRef != 0 AND DockReceiving.IncomingRef IS NOT NULL
								LEFT OUTER JOIN ADISGWMS.dbo.[Entry] AS EE ON EE.[EntryNumber] = [Entry].[Masterbox] LEFT OUTER JOIN ADISGWMS.dbo.[DockReceiving] AS DR ON EE.ForwarderRef = DR.IncomingRef AND DR.IncomingRef != 0 AND DR.IncomingRef IS NOT NULL
								WHERE Import.dbo.Outgoing_ADIShipment.[EnterBy] != 'FromMigration' AND LEN([Order].OrderNumber) > 6
								GROUP BY PackListPosition.PackListNumber, [Order].CustomerReference, Entry.TaxScheme, [Order].DNStatus, CONVERT(VARCHAR(10), [Order].ShippedDateTime, 103), [Order].GSTIncoTerm, [Order].CustomerName1, [SAPDailyShipment].[ShipmentNumber], [SAPDailyShipment].[UnitPrice], [SAPDailyShipment].[ShipmentValue], [Entry].[Invoice], [Order].[ShippingCountry], [Order].[Forwarder], Outgoing_ADIShipment.HAWB, PackListPosition.Partnumber,Outgoing_ADIShipment.PermitNo, CONVERT(VARCHAR(10), Outgoing_ADIShipment.PermitDate, 103), Outgoing_ADIShipment.JobType, Outgoing_ADIShipment.Comment,Outgoing_ADIShipment.CustInvValue, Outgoing_ADIShipment.ExchangeRate, Outgoing_ADIShipment.InvoiceValueSGD, Outgoing_ADIShipment.GSTAmountValue, (CASE WHEN ([DockReceiving].Supplier IS NULL) THEN (DR.Supplier) ELSE [DockReceiving].Supplier END), Outgoing_ADIShipment.MAWB, Outgoing_ADIShipment.Flight, CONVERT(VARCHAR(10), Outgoing_ADIShipment.DepDate, 103), Outgoing_ADIShipment.DeliveryFrom, Outgoing_ADIShipment.OtherCharge, Outgoing_ADIShipment.TotalInvoiceValue
								Having [Order].DNStatus = 'CARGO RELEASED' AND [SAPDailyShipment].[ShipmentNumber] = (select top 1 ShipmentNo from Import.dbo.[Outgoing_ADIShipment] where ShipmentNo < ".$this->PostVars['ShipmentNumber']." order by ShipmentNo DESC)");
		} else if($this->PostVars['SearchAction'] == 'Last'){
			$sql = mssql_query("SELECT PackListPosition.PackListNumber, [Order].CustomerReference as DN, [SAPDailyShipment].[ShipmentNumber], Entry.TaxScheme, [Order].[ShippingCountry], CONVERT(VARCHAR(10), [Order].ShippedDateTime, 103) as CargoReleasedDate, [Order].GSTIncoTerm as IncoTerm, [Order].[Forwarder], Outgoing_ADIShipment.HAWB, [Order].CustomerName1 as ShipTo, PackListPosition.Partnumber, SUM(PackListPosition.Quantity) AS TotalQty, [SAPDailyShipment].[UnitPrice],
								(SUM(PackListPosition.Quantity) * [SAPDailyShipment].[UnitPrice]) AS [ShipmentValue], [Entry].[Invoice],Outgoing_ADIShipment.PermitNo, CONVERT(VARCHAR(10), Outgoing_ADIShipment.PermitDate, 103) as PermitDate, Outgoing_ADIShipment.JobType, Outgoing_ADIShipment.Comment,Outgoing_ADIShipment.CustInvValue, Outgoing_ADIShipment.ExchangeRate, Outgoing_ADIShipment.InvoiceValueSGD, Outgoing_ADIShipment.GSTAmountValue, (CASE WHEN ([DockReceiving].Supplier IS NULL) THEN (DR.Supplier) ELSE [DockReceiving].Supplier END) AS Supplier, Outgoing_ADIShipment.MAWB, Outgoing_ADIShipment.Flight, CONVERT(VARCHAR(10), Outgoing_ADIShipment.DepDate, 103) as DepDate, Outgoing_ADIShipment.DeliveryFrom, Outgoing_ADIShipment.OtherCharge, Outgoing_ADIShipment.TotalInvoiceValue
								FROM ADISGWMS.dbo.PackListPosition
								INNER JOIN ADISGWMS.dbo.PackList ON PackList.PackListNumber = PackListPosition.PackListNumber AND PackList.DocumentNumber = PackListPosition.DocumentNumber INNER JOIN ADISGWMS.dbo.[Order] ON [Order].OrderNumber = PackList.DocumentNumber INNER JOIN ADISGWMS.dbo.Entry ON PackListPosition.EntryNumber = Entry.EntryNumber INNER JOIN ADISGWMS.dbo.SAPDailyShipment ON CONVERT(VARCHAR, [SAPDailyShipment].[DN]) = [Order].CustomerReference AND [SAPDailyShipment].[PartNumber] = PackListPosition.Partnumber
								INNER JOIN Import.dbo.Outgoing_ADIShipment ON Outgoing_ADIShipment.ShipmentNo = [SAPDailyShipment].[ShipmentNumber] LEFT OUTER JOIN ADISGWMS.dbo.[DockReceiving] WITH (NOLOCK) ON [Entry].ForwarderRef = DockReceiving.IncomingRef AND DockReceiving.IncomingRef != 0 AND DockReceiving.IncomingRef IS NOT NULL
								LEFT OUTER JOIN ADISGWMS.dbo.[Entry] AS EE ON EE.[EntryNumber] = [Entry].[Masterbox] LEFT OUTER JOIN ADISGWMS.dbo.[DockReceiving] AS DR ON EE.ForwarderRef = DR.IncomingRef AND DR.IncomingRef != 0 AND DR.IncomingRef IS NOT NULL
								WHERE Import.dbo.Outgoing_ADIShipment.[EnterBy] != 'FromMigration' AND LEN([Order].OrderNumber) > 6
								GROUP BY PackListPosition.PackListNumber, [Order].CustomerReference, Entry.TaxScheme, [Order].DNStatus, CONVERT(VARCHAR(10), [Order].ShippedDateTime, 103), [Order].GSTIncoTerm, [Order].CustomerName1, [SAPDailyShipment].[ShipmentNumber], [SAPDailyShipment].[UnitPrice], [SAPDailyShipment].[ShipmentValue], [Entry].[Invoice], [Order].[ShippingCountry], [Order].[Forwarder], Outgoing_ADIShipment.HAWB, PackListPosition.Partnumber,Outgoing_ADIShipment.PermitNo, CONVERT(VARCHAR(10), Outgoing_ADIShipment.PermitDate, 103), Outgoing_ADIShipment.JobType, Outgoing_ADIShipment.Comment,Outgoing_ADIShipment.CustInvValue, Outgoing_ADIShipment.ExchangeRate, Outgoing_ADIShipment.InvoiceValueSGD, Outgoing_ADIShipment.GSTAmountValue, (CASE WHEN ([DockReceiving].Supplier IS NULL) THEN (DR.Supplier) ELSE [DockReceiving].Supplier END), Outgoing_ADIShipment.MAWB, Outgoing_ADIShipment.Flight, CONVERT(VARCHAR(10), Outgoing_ADIShipment.DepDate, 103), Outgoing_ADIShipment.DeliveryFrom, Outgoing_ADIShipment.OtherCharge, Outgoing_ADIShipment.TotalInvoiceValue
								Having [Order].DNStatus = 'CARGO RELEASED' AND [SAPDailyShipment].[ShipmentNumber] = (select top 1 ShipmentNo from Import.dbo.[Outgoing_ADIShipment] order by ShipmentNo DESC)");
		} else if($this->PostVars['SearchAction'] == 'Search'){
			$sql = mssql_query("SELECT PackListPosition.PackListNumber, [Order].CustomerReference as DN, [SAPDailyShipment].[ShipmentNumber], Entry.TaxScheme, [Order].[ShippingCountry], CONVERT(VARCHAR(10), [Order].ShippedDateTime, 103) as CargoReleasedDate, [Order].GSTIncoTerm as IncoTerm, [Order].[Forwarder], Outgoing_ADIShipment.HAWB, [Order].CustomerName1 as ShipTo, PackListPosition.Partnumber, SUM(PackListPosition.Quantity) AS TotalQty, [SAPDailyShipment].[UnitPrice],
								(SUM(PackListPosition.Quantity) * [SAPDailyShipment].[UnitPrice]) AS [ShipmentValue], [Entry].[Invoice],Outgoing_ADIShipment.PermitNo, CONVERT(VARCHAR(10), Outgoing_ADIShipment.PermitDate, 103) as PermitDate, Outgoing_ADIShipment.JobType, Outgoing_ADIShipment.Comment,Outgoing_ADIShipment.CustInvValue, Outgoing_ADIShipment.ExchangeRate, Outgoing_ADIShipment.InvoiceValueSGD, Outgoing_ADIShipment.GSTAmountValue, (CASE WHEN ([DockReceiving].Supplier IS NULL) THEN (DR.Supplier) ELSE [DockReceiving].Supplier END) AS Supplier, Outgoing_ADIShipment.MAWB, Outgoing_ADIShipment.Flight, CONVERT(VARCHAR(10), Outgoing_ADIShipment.DepDate, 103) as DepDate, Outgoing_ADIShipment.DeliveryFrom, Outgoing_ADIShipment.OtherCharge, Outgoing_ADIShipment.TotalInvoiceValue
								FROM ADISGWMS.dbo.PackListPosition
								INNER JOIN ADISGWMS.dbo.PackList ON PackList.PackListNumber = PackListPosition.PackListNumber AND PackList.DocumentNumber = PackListPosition.DocumentNumber INNER JOIN ADISGWMS.dbo.[Order] ON [Order].OrderNumber = PackList.DocumentNumber INNER JOIN ADISGWMS.dbo.Entry ON PackListPosition.EntryNumber = Entry.EntryNumber INNER JOIN ADISGWMS.dbo.SAPDailyShipment ON CONVERT(VARCHAR, [SAPDailyShipment].[DN]) = [Order].CustomerReference AND [SAPDailyShipment].[PartNumber] = PackListPosition.Partnumber
								INNER JOIN Import.dbo.Outgoing_ADIShipment ON Outgoing_ADIShipment.ShipmentNo = [SAPDailyShipment].[ShipmentNumber] LEFT OUTER JOIN ADISGWMS.dbo.[DockReceiving] WITH (NOLOCK) ON [Entry].ForwarderRef = DockReceiving.IncomingRef AND DockReceiving.IncomingRef != 0 AND DockReceiving.IncomingRef IS NOT NULL
								LEFT OUTER JOIN ADISGWMS.dbo.[Entry] AS EE ON EE.[EntryNumber] = [Entry].[Masterbox] LEFT OUTER JOIN ADISGWMS.dbo.[DockReceiving] AS DR ON EE.ForwarderRef = DR.IncomingRef AND DR.IncomingRef != 0 AND DR.IncomingRef IS NOT NULL
								WHERE Import.dbo.Outgoing_ADIShipment.[EnterBy] != 'FromMigration' AND LEN([Order].OrderNumber) > 6
								GROUP BY PackListPosition.PackListNumber, [Order].CustomerReference, Entry.TaxScheme, [Order].DNStatus, CONVERT(VARCHAR(10), [Order].ShippedDateTime, 103), [Order].GSTIncoTerm, [Order].CustomerName1, [SAPDailyShipment].[ShipmentNumber], [SAPDailyShipment].[UnitPrice], [SAPDailyShipment].[ShipmentValue], [Entry].[Invoice], [Order].[ShippingCountry], [Order].[Forwarder], Outgoing_ADIShipment.HAWB, PackListPosition.Partnumber,Outgoing_ADIShipment.PermitNo, CONVERT(VARCHAR(10), Outgoing_ADIShipment.PermitDate, 103), Outgoing_ADIShipment.JobType, Outgoing_ADIShipment.Comment,Outgoing_ADIShipment.CustInvValue, Outgoing_ADIShipment.ExchangeRate, Outgoing_ADIShipment.InvoiceValueSGD, Outgoing_ADIShipment.GSTAmountValue, (CASE WHEN ([DockReceiving].Supplier IS NULL) THEN (DR.Supplier) ELSE [DockReceiving].Supplier END), Outgoing_ADIShipment.MAWB, Outgoing_ADIShipment.Flight, CONVERT(VARCHAR(10), Outgoing_ADIShipment.DepDate, 103), Outgoing_ADIShipment.DeliveryFrom, Outgoing_ADIShipment.OtherCharge, Outgoing_ADIShipment.TotalInvoiceValue
								Having [Order].DNStatus = 'CARGO RELEASED' AND [SAPDailyShipment].[ShipmentNumber] = '".$this->PostVars['ShipmentNumber']."'");
		}
		
		if($this->PostVars['UserID'] == 'dev/jagustin'){
			//var_dump(mssql_get_last_message());
			//die();
		}

		if (mssql_num_rows($sql) < 1){
			//if($this->PostVars['SearchAction'] == 'Search'){
			//	return json_encode($this->PostVars['SearchAction']);
			//} else {
				return json_encode("nodata");
			//}
		} else {
			$data = array();
			while($row = mssql_fetch_assoc($sql)){
				array_push($data,$row);
			}
			return json_encode($data);
		}
	}

	function getPacklistData(){
		
		if($this->PostVars['slcCustomers'] == 'ADISGWMS'){
			$this->PostVars['slcCustomers'] = 'ADIDGTWMS';
		}
		
		$sqlMaster = mssql_query("select STGIndex,STG,DBName from MasterData.dbo.WawiCustomer where MasterData.dbo.WawiCustomer.WawiIndex='".$this->PostVars['slcCustomers']."'; ");
		if (mssql_num_rows($sqlMaster)){
			$CustomerInfoArr = mssql_fetch_assoc($sqlMaster);
			$DBName = $CustomerInfoArr['DBName'];

			$sql = $this->AnalyzeSQL(mssql_query("exec p_ExportPacklist '".$DBName."',".$this->PostVars['packlistnumber']));
		}
		

		return json_encode($sql);
	}

	function AnalyzeSQL($sql){
		if (mssql_num_rows($sql) > 0){
			while($row[] = mssql_fetch_assoc($sql)){}
			array_pop($row);
		} else {
			$row = 'nodata';
		}
		
		return $row;
	}
	
	function AvailablePlist($WawiIndex){
		$sql = mssql_query("select top(5000) plist,WawiIndex from v_ol_exportAvailablePlist where WawiIndex = '".$WawiIndex."' group by plist,WawiIndex");
		if (mssql_num_rows($sql) > 0){
			$data = array();
			while($row = mssql_fetch_assoc($sql)){
				$sqlplist = mssql_query($row['plist']);
				if (mssql_num_rows($sqlplist) > 0){
					unset($rowPlist);
					while($rowPlist[] = mssql_fetch_assoc($sqlplist)){}
					array_pop($rowPlist);
					array_push($data,$rowPlist);
				}
			}
			
			if (count($data) == 0){
				$data = "nodata";
			}
		} else {
			$data = "nodata";
		}
		
		return $data;
	}
	
	function GetAvailablePacklistNumber(){
		mssql_select_db("Import");
		return json_encode($this->AvailablePlist($this->PostVars['WawiIndex']));
	}
	
	function GetAvailableShipmentNumber(){
		
		mssql_select_db("Import");
		
		if($this->PostVars['WawiIndex'] == 'ADISGWMS'){
			$this->PostVars['WawiIndex'] = 'ADIDGTWMS';
		}
		
		$sqlMaster = mssql_query("select TOP 1 STGIndex,STG,DBName from MasterData.dbo.WawiCustomer where MasterData.dbo.WawiCustomer.WawiIndex='".$this->PostVars['WawiIndex']."'; ");
		if (mssql_num_rows($sqlMaster)){
			$CustomerInfoArr = mssql_fetch_assoc($sqlMaster);
			$DBName = $CustomerInfoArr['DBName'];
		
			//$sql = mssql_query("select ShipmentNumber from ".$DBName.".dbo.SAPDailyShipment where GSTProcessed = 0 group by ShipmentNumber");
			$sql = mssql_query("SELECT [SAPDailyShipment].ShipmentNumber FROM ".$DBName.".dbo.SAPDailyShipment INNER JOIN ".$DBName.".dbo.[Order] ON CAST([SAPDailyShipment].[DN] AS VARCHAR) = [Order].CustomerReference WHERE [SAPDailyShipment].GSTProcessed = 0 AND [Order].DNStatus = 'CARGO RELEASED' AND LEN([Order].OrderNumber) > 6 AND [SAPDailyShipment].ShipmentNumber NOT IN (SELECT DISTINCT [ShipmentNo] FROM Import.dbo.Outgoing_ADIShipment WITH (NOLOCK))  GROUP BY [SAPDailyShipment].ShipmentNumber,[Order].DNStatus");
			if (mssql_num_rows($sql) > 0){
				
				while($data[] = mssql_fetch_assoc($sql)){}
				array_pop($data);
				
			} else {
				$data = "nodata";
			}
		
		} else {
			$data = "nodata";
		}
		
		return json_encode($data);
	}
	
	function ADIGetShipment(){
		
		mssql_select_db("Import");
		
		if($this->PostVars['WawiIndex'] == 'ADISGWMS'){
			$this->PostVars['WawiIndex'] = 'ADIDGTWMS';
		}
		
		$sqlMaster = mssql_query("select TOP 1 STGIndex,STG,DBName from MasterData.dbo.WawiCustomer where MasterData.dbo.WawiCustomer.WawiIndex='".$this->PostVars['WawiIndex']."'; ");
		if (mssql_num_rows($sqlMaster)){
			$CustomerInfoArr = mssql_fetch_assoc($sqlMaster);
			$DBName = $CustomerInfoArr['DBName'];
		
			$sql = mssql_query("SELECT PackListPosition.PackListNumber, [Order].CustomerReference as DN, [SAPDailyShipment].[ShipmentNumber], Entry.TaxScheme, [Order].[ShippingCountry], CONVERT(VARCHAR(10), [Order].ShippedDateTime, 103) as CargoReleasedDate, [Order].GSTIncoTerm as IncoTerm, [Order].[Forwarder], PackList.HAWB, [Order].CustomerName1 as ShipTo, PackListPosition.Partnumber, SUM(PackListPosition.Quantity) AS TotalQty, [SAPDailyShipment].[UnitPrice],
								(SUM(PackListPosition.Quantity) * [SAPDailyShipment].[UnitPrice]) AS [ShipmentValue], [Entry].[Invoice], (CASE WHEN ([DockReceiving].Supplier IS NULL) THEN (DR.Supplier) ELSE [DockReceiving].Supplier END) AS Supplier
								FROM ".$DBName.".dbo.PackListPosition
								INNER JOIN ".$DBName.".dbo.PackList ON PackList.PackListNumber = PackListPosition.PackListNumber AND PackList.DocumentNumber = PackListPosition.DocumentNumber
								INNER JOIN ".$DBName.".dbo.[Order] ON [Order].OrderNumber = PackList.DocumentNumber
								INNER JOIN ".$DBName.".dbo.Entry ON PackListPosition.EntryNumber = Entry.EntryNumber
								INNER JOIN ".$DBName.".dbo.SAPDailyShipment ON CONVERT(VARCHAR, [SAPDailyShipment].[DN]) = [Order].CustomerReference AND [SAPDailyShipment].[PartNumber] = PackListPosition.Partnumber
								LEFT OUTER JOIN ".$DBName.".dbo.[DockReceiving] WITH (NOLOCK) ON [Entry].ForwarderRef = DockReceiving.IncomingRef AND DockReceiving.IncomingRef != 0 AND DockReceiving.IncomingRef IS NOT NULL
								LEFT OUTER JOIN ADISGWMS.dbo.[Entry] AS EE ON EE.[EntryNumber] = [Entry].[Masterbox] LEFT OUTER JOIN ADISGWMS.dbo.[DockReceiving] AS DR ON EE.ForwarderRef = DR.IncomingRef AND DR.IncomingRef != 0 AND DR.IncomingRef IS NOT NULL
								GROUP BY PackListPosition.PackListNumber, [Order].CustomerReference, Entry.TaxScheme, [Order].DNStatus, CONVERT(VARCHAR(10), [Order].ShippedDateTime, 103), [Order].GSTIncoTerm, [Order].CustomerName1, [SAPDailyShipment].[ShipmentNumber], [SAPDailyShipment].[UnitPrice], [SAPDailyShipment].[ShipmentValue], [Entry].[Invoice], [Order].[ShippingCountry], [Order].[Forwarder], PackList.HAWB, PackListPosition.Partnumber, (CASE WHEN ([DockReceiving].Supplier IS NULL) THEN (DR.Supplier) ELSE [DockReceiving].Supplier END)
								Having [Order].DNStatus = 'CARGO RELEASED' AND [SAPDailyShipment].[ShipmentNumber] = '".$this->PostVars['ShipmentNumber']."'");
			if (mssql_num_rows($sql) > 0){
				
				while($data[] = mssql_fetch_assoc($sql)){}
				array_pop($data);
				
			} else {
				$data = "nodata";
			}
		
		} else {
			$data = "nodata";
		}
		
		return json_encode($data);
	}
	
	function ADIGetEditShipment(){
		
		mssql_select_db("Import");
		
		if($this->PostVars['WawiIndex'] == 'ADISGWMS'){
			$this->PostVars['WawiIndex'] = 'ADIDGTWMS';
		}
		
		$sqlMaster = mssql_query("select TOP 1 STGIndex,STG,DBName from MasterData.dbo.WawiCustomer where MasterData.dbo.WawiCustomer.WawiIndex='".$this->PostVars['WawiIndex']."'; ");
		if (mssql_num_rows($sqlMaster)){
			$CustomerInfoArr = mssql_fetch_assoc($sqlMaster);
			$DBName = $CustomerInfoArr['DBName'];
		
			$sql = mssql_query("SELECT PackListPosition.PackListNumber, [Order].CustomerReference as DN, [SAPDailyShipment].[ShipmentNumber], Entry.TaxScheme, [Order].[ShippingCountry], CONVERT(VARCHAR(10), [Order].ShippedDateTime, 103) as CargoReleasedDate, [Order].GSTIncoTerm as IncoTerm, [Order].[Forwarder], Outgoing_ADIShipment.HAWB, [Order].CustomerName1 as ShipTo, PackListPosition.Partnumber, SUM(PackListPosition.Quantity) AS TotalQty, [SAPDailyShipment].[UnitPrice],
								(SUM(PackListPosition.Quantity) * [SAPDailyShipment].[UnitPrice]) AS [ShipmentValue], [Entry].[Invoice], (CASE WHEN ([DockReceiving].Supplier IS NULL) THEN (DR.Supplier) ELSE [DockReceiving].Supplier END) AS Supplier, Outgoing_ADIShipment.PermitNo, CONVERT(VARCHAR(10), Outgoing_ADIShipment.PermitDate, 103) as PermitDate, Outgoing_ADIShipment.JobType, Outgoing_ADIShipment.Comment,Outgoing_ADIShipment.CustInvValue, Outgoing_ADIShipment.ExchangeRate, Outgoing_ADIShipment.InvoiceValueSGD, Outgoing_ADIShipment.GSTAmountValue, Outgoing_ADIShipment.MAWB, Outgoing_ADIShipment.Flight, CONVERT(VARCHAR(10), Outgoing_ADIShipment.DepDate, 103) as DepDate, Outgoing_ADIShipment.DeliveryFrom, Outgoing_ADIShipment.OtherCharge, Outgoing_ADIShipment.TotalInvoiceValue
								FROM ADISGWMS.dbo.PackListPosition
								INNER JOIN ADISGWMS.dbo.PackList ON PackList.PackListNumber = PackListPosition.PackListNumber AND PackList.DocumentNumber = PackListPosition.DocumentNumber INNER JOIN ADISGWMS.dbo.[Order] ON [Order].OrderNumber = PackList.DocumentNumber INNER JOIN ADISGWMS.dbo.Entry ON PackListPosition.EntryNumber = Entry.EntryNumber INNER JOIN ADISGWMS.dbo.SAPDailyShipment ON CONVERT(VARCHAR, [SAPDailyShipment].[DN]) = [Order].CustomerReference AND [SAPDailyShipment].[PartNumber] = PackListPosition.Partnumber
								INNER JOIN Import.dbo.Outgoing_ADIShipment ON Outgoing_ADIShipment.ShipmentNo = [SAPDailyShipment].[ShipmentNumber]
								LEFT OUTER JOIN ADISGWMS.dbo.[DockReceiving] WITH (NOLOCK) ON [Entry].ForwarderRef = DockReceiving.IncomingRef AND DockReceiving.IncomingRef != 0 AND DockReceiving.IncomingRef IS NOT NULL
								LEFT OUTER JOIN ADISGWMS.dbo.[Entry] AS EE ON EE.[EntryNumber] = [Entry].[Masterbox] LEFT OUTER JOIN ADISGWMS.dbo.[DockReceiving] AS DR ON EE.ForwarderRef = DR.IncomingRef AND DR.IncomingRef != 0 AND DR.IncomingRef IS NOT NULL
								WHERE Import.dbo.Outgoing_ADIShipment.[EnterBy] != 'FromMigration' AND LEN([Order].OrderNumber) > 6
								GROUP BY PackListPosition.PackListNumber, [Order].CustomerReference, Entry.TaxScheme, [Order].DNStatus, CONVERT(VARCHAR(10), [Order].ShippedDateTime, 103), [Order].GSTIncoTerm, [Order].CustomerName1, [SAPDailyShipment].[ShipmentNumber], [SAPDailyShipment].[UnitPrice], [SAPDailyShipment].[ShipmentValue], [Entry].[Invoice], (CASE WHEN ([DockReceiving].Supplier IS NULL) THEN (DR.Supplier) ELSE [DockReceiving].Supplier END), [Order].[ShippingCountry], [Order].[Forwarder], Outgoing_ADIShipment.HAWB, PackListPosition.Partnumber,Outgoing_ADIShipment.PermitNo, CONVERT(VARCHAR(10), Outgoing_ADIShipment.PermitDate, 103), Outgoing_ADIShipment.JobType, Outgoing_ADIShipment.Comment,Outgoing_ADIShipment.CustInvValue, Outgoing_ADIShipment.ExchangeRate, Outgoing_ADIShipment.InvoiceValueSGD, Outgoing_ADIShipment.GSTAmountValue, Outgoing_ADIShipment.MAWB, Outgoing_ADIShipment.Flight, CONVERT(VARCHAR(10), Outgoing_ADIShipment.DepDate, 103), Outgoing_ADIShipment.DeliveryFrom, Outgoing_ADIShipment.OtherCharge, Outgoing_ADIShipment.TotalInvoiceValue
								Having [Order].DNStatus = 'CARGO RELEASED' AND [SAPDailyShipment].[ShipmentNumber] = '".$this->PostVars['ShipmentNumber']."'");
								
			if (mssql_num_rows($sql) > 0){
				
				while($data[] = mssql_fetch_assoc($sql)){}
				array_pop($data);
				
			} else {
				$data = "nodata";
			}
		
		} else {
			$data = "nodata";
		}
		
		return json_encode($data);
	}
	
	function ProcessData(){
		if ($this->PostVars['btnSaveUpdate'] == 'Update'){
			return $this->UpdateGST();
		} else {
			return $this->AddNewGST();
		}
	}
	
	function ProcessADIData(){
		if ($this->PostVars['btnSaveUpdate'] == 'Update'){
			return $this->UpdateADIGST();
		} else {
			return $this->AddNewADIGST();
		}
	}
	
	function UpdateGST(){
		$data = array($this->PostVars['txtShipmentNumber']);
		mssql_query("BEGIN TRAN");
		
		$PD = trim($this->PostVars['txtPermitDate']);
		if ($PD=='' || $PD=='-' || $PD=='NA' || $PD=='N/A' || $PD=='TBA'){
			$mYqry = "Update Outgoing_Shipment set [Customer] = '".$this->PostVars['slcCustomers']."',[MAWB] = '".$this->PostVars['txtMAWB']."',[HAWB] = '".$this->PostVars['txtHAWB']."',[Flight] = '".$this->PostVars['txtFlight']."',[DepDate] = '".$this->PostVars['txtDepDate']."',[ShipDate] = '".$this->PostVars['txtShipDate']."',[Destination] = '".$this->PostVars['slcDestination']."',[Forwarders] = '".$this->PostVars['slcForwarders']."',[CustInvNo] = '".$this->PostVars['txtCustInvNo']."',[PermitNo] = '".$this->PostVars['txtPermitNo']."',[CustInvValue] = ".$this->PostVars['txtCustInvValue'].",[ExchangeRate] = ".$this->PostVars['txtExchangeRate'].",[USDUnitPrice] = ".$this->PostVars['txtUSDUnitPrice'].",[JobType] = '".$this->PostVars['slcJobType']."',[InvoiceValueSGD] = ".$this->PostVars['txtInvoiceValueSGD'].",[Remarks] = '".$this->PostVars['txtRemarks']."',[PermitDate] = NULL where OutShipReference = ".$this->PostVars['txtShipmentNumber'];
		}else{
			$mYqry = "Update Outgoing_Shipment set [Customer] = '".$this->PostVars['slcCustomers']."',[MAWB] = '".$this->PostVars['txtMAWB']."',[HAWB] = '".$this->PostVars['txtHAWB']."',[Flight] = '".$this->PostVars['txtFlight']."',[DepDate] = '".$this->PostVars['txtDepDate']."',[ShipDate] = '".$this->PostVars['txtShipDate']."',[Destination] = '".$this->PostVars['slcDestination']."',[Forwarders] = '".$this->PostVars['slcForwarders']."',[CustInvNo] = '".$this->PostVars['txtCustInvNo']."',[PermitNo] = '".$this->PostVars['txtPermitNo']."',[CustInvValue] = ".$this->PostVars['txtCustInvValue'].",[ExchangeRate] = ".$this->PostVars['txtExchangeRate'].",[USDUnitPrice] = ".$this->PostVars['txtUSDUnitPrice'].",[JobType] = '".$this->PostVars['slcJobType']."',[InvoiceValueSGD] = ".$this->PostVars['txtInvoiceValueSGD'].",[Remarks] = '".$this->PostVars['txtRemarks']."',[PermitDate] = '".$this->PostVars['txtPermitDate']."' where OutShipReference = ".$this->PostVars['txtShipmentNumber'];
		}
		
		if (mssql_query($mYqry)){
			
			$result = $this->UpdateSTGMS('update',$this->PostVars['txtShipmentNumber']);	
			if ($result[0]!='success'){
				mssql_query("ROLLBACK");
				array_push($data,$result[1],$result[2]);
			}else{
				mssql_query("COMMIT");
				array_push($data,'successUPDATE');
			}

		} else {
			mssql_query("ROLLBACK");
			array_push($data,'errorUpdate');
		}
		
		return json_encode($data);
	}
	
	function UpdateSTGMS($request,$OutShipReference){
		$tempDN = explode("/",$this->PostVars['txtCustInvNo']);
		$errorCtr = 0;
		
		if ($request=='add' || $request=='update'){
		
			if (count($tempDN)){
			
				mssql_query("BEGIN TRAN");
				
				$sqlMaster = mssql_query("select STGIndex,STG,DBName from MasterData.dbo.WawiCustomer where MasterData.dbo.WawiCustomer.WawiIndex='".$this->PostVars['slcCustomers']."'; ");
				
				if (mssql_num_rows($sqlMaster)){
					$CustomerInfoArr = mssql_fetch_assoc($sqlMaster);
					$STGIndex = $CustomerInfoArr['STGIndex'];
					$STG = $CustomerInfoArr['STG'];
					$DBName = $CustomerInfoArr['DBName'];
					
					if ($STG==1){
						
						for ($i=0; $i<count($tempDN); $i++){
							
							$wmsQry = mssql_query("SELECT * FROM ".$DBName.".dbo.[Order] WHERE CustomerReference='".$tempDN[$i]."'; ");
							$wmsRows = mssql_num_rows($wmsQry);
							
							if ($wmsRows>0){
								
								$query = mssql_query("SELECT * FROM STGMM.dbo.PermitInfo WHERE permit_client_name='".$STGIndex."' AND permit_dn_number='".$tempDN[$i]."';");
								$rows = mssql_num_rows($query);
								if ($rows>0){									
								
									$PD = trim($this->PostVars['txtPermitDate']);
									if ($PD=='' || $PD=='-' || $PD=='NA' || $PD=='N/A' || $PD=='TBA'){
										$mYqry = "Update STGMM.dbo.PermitInfo
										set permit_permit_number='".$this->PostVars['txtPermitNo']."',
										permit_date=NULL,
										deleted=0,
										permit_depdate='".$this->PostVars['txtDepDate']."', datemodified=convert(varchar, getdate(), 120),
										permit_hawb='".$this->PostVars['txtHAWB']."', [user_id]='".$this->PostVars['UserID']."'
										WHERE permit_client_name='".$STGIndex."' AND permit_dn_number='".$tempDN[$i]."';";
									}else{
										$mYqry = "Update STGMM.dbo.PermitInfo
										set permit_permit_number='".$this->PostVars['txtPermitNo']."',
										permit_date='".$this->PostVars['txtPermitDate']."',
										deleted=0,
										permit_depdate='".$this->PostVars['txtDepDate']."', datemodified=convert(varchar, getdate(), 120),
										permit_hawb='".$this->PostVars['txtHAWB']."', [user_id]='".$this->PostVars['UserID']."'
										WHERE permit_client_name='".$STGIndex."' AND permit_dn_number='".$tempDN[$i]."';";
									}
								
									$qryUpdate=mssql_query($mYqry);
									if (!$qryUpdate){
										mssql_query("ROLLBACK");
										return array('failed','errSTGMSUpdate','');
										break;
									}
									mssql_free_result($qryUpdate);
								}else{
									$PD = trim($this->PostVars['txtPermitDate']);
									if ($PD=='' || $PD=='-' || $PD=='NA' || $PD=='N/A' || $PD=='TBA'){
										$mYqry = "Insert into STGMM.dbo.PermitInfo (permit_client_name,permit_dn_number,permit_permit_number,permit_depdate,permit_entry_date,permit_hawb,[user_id],OutShipReference)
										values ('".$STGIndex."','".$tempDN[$i]."','".$this->PostVars['txtPermitNo']."','".$this->PostVars['txtDepDate']."',convert(varchar, getdate(), 120),'".$this->PostVars['txtHAWB']."','".$this->PostVars['UserID']."', ".$OutShipReference."); ";
									}else{
										$mYqry = "Insert into STGMM.dbo.PermitInfo (permit_client_name,permit_dn_number,permit_permit_number,permit_depdate,permit_entry_date,permit_hawb,[user_id],OutShipReference,permit_date)
										values ('".$STGIndex."','".$tempDN[$i]."','".$this->PostVars['txtPermitNo']."','".$this->PostVars['txtDepDate']."',convert(varchar, getdate(), 120),'".$this->PostVars['txtHAWB']."','".$this->PostVars['UserID']."', ".$OutShipReference.",'".$this->PostVars['txtPermitDate']."'); ";
									}
									
									$qryInsert=mssql_query($mYqry);
									mssql_free_result($qryInsert);
									if (!$qryInsert){
										mssql_query("ROLLBACK");
										return array('failed','errSTGMSInsert','');
										break;
									}
								}

								mssql_free_result($query);
								unset($rows);
								
							}else{
								mssql_query("ROLLBACK");
								return array('failed','errDNnotExist',$tempDN[$i]);
								break;
							}
							mssql_free_result($wmsQry);
							unset($wmsRows);
						}
						
						$dnImplode = implode("','",$tempDN);
						$qryDel = mssql_query("UPDATE STGMM.dbo.PermitInfo set deleted=1 where permit_client_name='".$STGIndex."'
						and permit_dn_number not in ('".$dnImplode."') and OutShipReference=".$OutShipReference."; ");
						if (!$qryDel){
							mssql_query("ROLLBACK");
							return array('failed','errDeletePermit','');
						}else{						
							mssql_query("COMMIT");
							return array('success','','');
						}
					}else{
						mssql_query("COMMIT");
						return array('success','','');
					}
				}else{
					mssql_query("ROLLBACK");
					return array('failed','errMasterData','');
				}
			}else{
				return array('failed','errCustInvNo','');
			}
		}else{
			return array('failed','errInvalidRequest','');
		}
		
	}
	
	function AddNewGST(){
		mssql_select_db("Import");
		$sqlNewShipRec = mssql_fetch_assoc(mssql_query("SELECT Value FROM Setup WHERE Entry='OutShipCounter'"));
		$ShipReference = $sqlNewShipRec['Value'] + 1;
		$data = array($ShipReference);
		
		mssql_query("BEGIN TRAN");
		mssql_query("Update Setup set Value = Value + 1 WHERE Entry='OutShipCounter'");
		unset($sqlNewShipRec);
		
		$PD = trim($this->PostVars['txtPermitDate']);
		if ($PD=='' || $PD=='-' || $PD=='NA' || $PD=='N/A' || $PD=='TBA'){
			$mYqry = "Insert into Outgoing_Shipment ([OutShipReference],[Customer],[MAWB],[HAWB],[Flight],[DepDate],[ShipDate],[Destination],[Forwarders],[CustInvNo],[PermitNo],[CustInvValue],[ExchangeRate],[USDUnitPrice],[JobType],[InvoiceValueSGD],[Remarks]) values ($ShipReference,'".$this->PostVars['slcCustomers']."','".$this->PostVars['txtMAWB']."','".$this->PostVars['txtHAWB']."','".$this->PostVars['txtFlight']."','".$this->PostVars['txtDepDate']."','".$this->PostVars['txtShipDate']."','".$this->PostVars['slcDestination']."','".$this->PostVars['slcForwarders']."','".$this->PostVars['txtCustInvNo']."','".$this->PostVars['txtPermitNo']."',".$this->PostVars['txtCustInvValue'].",".$this->PostVars['txtExchangeRate'].",".$this->PostVars['txtUSDUnitPrice'].",'".$this->PostVars['slcJobType']."',".$this->PostVars['txtInvoiceValueSGD'].",'".$this->PostVars['txtRemarks']."'); ";
		}else{
			$mYqry = "Insert into Outgoing_Shipment ([OutShipReference],[Customer],[MAWB],[HAWB],[Flight],[DepDate],[ShipDate],[Destination],[Forwarders],[CustInvNo],[PermitNo],[CustInvValue],[ExchangeRate],[USDUnitPrice],[JobType],[InvoiceValueSGD],[Remarks],[PermitDate]) values ($ShipReference,'".$this->PostVars['slcCustomers']."','".$this->PostVars['txtMAWB']."','".$this->PostVars['txtHAWB']."','".$this->PostVars['txtFlight']."','".$this->PostVars['txtDepDate']."','".$this->PostVars['txtShipDate']."','".$this->PostVars['slcDestination']."','".$this->PostVars['slcForwarders']."','".$this->PostVars['txtCustInvNo']."','".$this->PostVars['txtPermitNo']."',".$this->PostVars['txtCustInvValue'].",".$this->PostVars['txtExchangeRate'].",".$this->PostVars['txtUSDUnitPrice'].",'".$this->PostVars['slcJobType']."',".$this->PostVars['txtInvoiceValueSGD'].",'".$this->PostVars['txtRemarks']."','".$this->PostVars['txtPermitDate']."'); ";
		}
		
		if (mssql_query($mYqry)){
			
			if (trim($this->PostVars['txtCustInvNo'])!=''){
				$result = $this->UpdateSTGMS('add',$ShipReference);
				
				if ($result[0]!='success'){
					mssql_query("ROLLBACK");
					array_push($data,$result[1],$result[2]);
				}else{
					mssql_query("COMMIT");
					array_push($data,'successADD');
				}
			}else{
				mssql_query("ROLLBACK");
				array_push($data,'erroradd');
			}
			
		} else {
			mssql_query("ROLLBACK");
			array_push($data,'erroradd');
		}

		return json_encode($data);
	}
	
	function AddNewADIGST(){
		mssql_select_db("Import");
		
		if($this->PostVars['slcCustomers'] == 'ADISGWMS'){
			$this->PostVars['slcCustomers'] = 'ADIDGTWMS';
		}
		
		$Customer			= trim($this->PostVars['slcCustomers']);
		$ShipmentNo			= trim($this->PostVars['txtShipmentNumber']);
		$PermitNo			= trim($this->PostVars['adiPermitNo']);
		$PermitDate			= date('Y-m-d', strtotime(str_replace('/', '-', trim($this->PostVars['adiPermitDate']))));
		$JobType			= trim($this->PostVars['adiJobType']);
		$Comment			= trim($this->PostVars['adiRemarks']);
		$CustInvValue		= trim($this->PostVars['adiCustInvValue']);
		$ExchangeRate		= trim($this->PostVars['adiExchangeRate']);
		$InvoiceValueSGD	= trim($this->PostVars['adiInvoiceValueSGD']);
		$OtherChargesSGD	= trim($this->PostVars['adiOtherChargesSGD']);
		$TotalInvoiceValue	= trim($this->PostVars['adiTotalInvoiceValue']);
		$GSTAmountValue		= trim($this->PostVars['adiGSTAmount']);
		$HAWB				= trim($this->PostVars['adiHAWB']);
		$MAWB				= trim($this->PostVars['adiMAWB']);
		$Flight				= trim($this->PostVars['adiFlight']);
		$DepDate			= date('Y-m-d', strtotime(str_replace('/', '-', trim($this->PostVars['adiDepDate']))));
		$DeliveryFrom		= trim($this->PostVars['adiDeliveryFrom']);
		$EnterBy			= trim($this->PostVars['UserID']);
		$EnterDate			= date('Y-m-d H:i:s');
		
		$data = array($ShipmentNo);
		
		$sqlMaster = mssql_query("select TOP 1 STGIndex,STG,DBName from MasterData.dbo.WawiCustomer where MasterData.dbo.WawiCustomer.WawiIndex='".$this->PostVars['slcCustomers']."'; ");
		if (mssql_num_rows($sqlMaster)){
			$CustomerInfoArr = mssql_fetch_assoc($sqlMaster);
			$DBName = $CustomerInfoArr['DBName'];
		
			if ($PermitNo != ''){
				
				mssql_query("BEGIN TRAN");
				
				$getOriginalHAWB = mssql_query("SELECT TOP 1 [Packlist].[HAWB] FROM ".$DBName.".dbo.[Packlist] WITH (NOLOCK) INNER JOIN ".$DBName.".dbo.[Order] ON [Order].[OrderNumber] = [Packlist].[DocumentNumber] WHERE [Order].[MoNumber] = '".$ShipmentNo."' AND [Order].[Forwarder] NOT LIKE '%ups%'");
				$getOriginalHAWBArr = mssql_fetch_assoc($getOriginalHAWB);
				$OrigHAWB = $getOriginalHAWBArr['HAWB'];
				
				$updateHAWBErr = 0;
				
				if($OrigHAWB != $HAWB){
					
					$DNDetailsData = $this->PostVars['DNDetails'];
					
					$OrderNumbers = array();
					foreach($DNDetailsData as $DNRowsData){
						array_push($OrderNumbers, $DNRowsData[0]);
					}
					$OrderNumbers = array_unique($OrderNumbers);
					
					foreach($OrderNumbers as $OrderValue){
						$updatePacklistHAWB = "UPDATE ADISGWMS.dbo.Packlist SET [HAWB] = '".$HAWB."', [HAWBOrig] = '".$OrigHAWB."', [HAWBRevisedFrom] = 'GST' WHERE [DocumentNumber] = '".$OrderValue."'";
						if(!mssql_query($updatePacklistHAWB)){
							$updateHAWBErr++;
						}
					}
					
				}
				
				if($updateHAWBErr == 0){
				
					$mYqry = "Insert into Outgoing_ADIShipment ([Customer],[ShipmentNo],[PermitNo],[PermitDate],[JobType],[Comment],[EnterBy],[EnterDate],[CustInvValue],[ExchangeRate],[InvoiceValueSGD],[GSTAmountValue],[HAWB],[MAWB],[Flight],[DepDate],[DeliveryFrom],[OtherCharge],[TotalInvoiceValue],[HAWBOrig])
							  values ('".$Customer."','".$ShipmentNo."','".$PermitNo."','".$PermitDate."','".$JobType."','".$Comment."','".$EnterBy."','".$EnterDate."','".$CustInvValue."','".$ExchangeRate."','".$InvoiceValueSGD."','".$GSTAmountValue."','".$HAWB."','".$MAWB."','".$Flight."','".$DepDate."','".$DeliveryFrom."','".$OtherChargesSGD."','".$TotalInvoiceValue."','".$OrigHAWB."');";
					
					if (mssql_query($mYqry)){
						
						$mYupdateqry = "UPDATE ".$DBName.".dbo.SAPDailyShipment SET [GSTProcessed] = 1, [GSTProcessedBy] = '".$EnterBy."', [GSTProcessedDate] = '".$EnterDate."' WHERE [ShipmentNumber] = '".$ShipmentNo."'";
						
						if(mssql_query($mYupdateqry)){
							
							$DNDetails = $this->PostVars['DNDetails'];
							
							$grouped_DNDetails = array(array('OrderNumber' => $DNDetails[0][0], 'Partnumber' => $DNDetails[0][4], 'UnitPrice' => $DNDetails[0][6]));
		
							foreach($DNDetails as $DNRowData){
								
								$inarray = 0;
								
								foreach($grouped_DNDetails as $grouped_DNData){
									
									if($grouped_DNData['OrderNumber'] == $DNRowData[0] && $grouped_DNData['Partnumber'] == $DNRowData[4]){
										$inarray++;
									}
									
								}
								
								if($inarray == 0){
									$forgroup_array = array('OrderNumber' => $DNRowData[0], 'Partnumber' => $DNRowData[4], 'UnitPrice' => $DNRowData[6]);
									array_push($grouped_DNDetails, $forgroup_array);
								}
								
							}
							
							$SAPShipmentData = array();
		
							$getSAPShipmentDetails = mssql_query("SELECT [DN],[PartNumber],[Quantity],[UnitPrice] FROM ".$DBName.".dbo.[SAPDailyShipment] WHERE [ShipmentNumber] = '".$ShipmentNo."' GROUP BY [DN],[PartNumber],[Quantity],[UnitPrice]");
							
							$hasError = 0;
							
							if (mssql_num_rows($getSAPShipmentDetails) > 0){
								
								while($SAPShipmentData[] = mssql_fetch_assoc($getSAPShipmentDetails)){}
								array_pop($SAPShipmentData);
								
								foreach($grouped_DNDetails as $grouped_DNData){
									
									foreach($SAPShipmentData as $SAPShipmentData_array){
										
										if($grouped_DNData['OrderNumber'] == $SAPShipmentData_array['DN'] && $grouped_DNData['Partnumber'] == $SAPShipmentData_array['PartNumber']){
											
											if($grouped_DNData['UnitPrice'] != $SAPShipmentData_array['UnitPrice']){
												
												$totalValue = $SAPShipmentData_array['Quantity'] * $grouped_DNData['UnitPrice'];
												$updateSAPDailyShipmentQuery = "UPDATE ".$DBName.".dbo.SAPDailyShipment SET [UnitPrice] = '".$grouped_DNData['UnitPrice']."', [ShipmentValue] = '".$totalValue."' WHERE [ShipmentNumber] = '".$ShipmentNo."' AND [DN] = '".$SAPShipmentData_array['DN']."' AND [PartNumber] = '".$SAPShipmentData_array['PartNumber']."'";
												$updateSAPDailyShipment = mssql_query($updateSAPDailyShipmentQuery);
												
												if($updateSAPDailyShipment && mssql_rows_affected($this->conn)){
													
												}else{
													$hasError++;
												}
											}
											
										}
										
									}
									
								}
								
							} else {
								$hasError++;
							}
							
							if($hasError == 0){
								mssql_query("COMMIT");
								array_push($data,'successADD'); 
							} else {
								mssql_query("ROLLBACK");
								array_push($data,'erroradd');
							}
							
						} else {
							mssql_query("ROLLBACK");
							array_push($data,'erroradd');
						}
						
					} else {
						mssql_query("ROLLBACK");
						array_push($data,'erroradd');
					}
					
				} else {
					mssql_query("ROLLBACK");
					array_push($data,'erroradd');
				}
			
			}else{
				array_push($data,'erroradd');
			}
		
		} else {
			array_push($data,'erroradd');
		}

		return json_encode($data);
	}
	
	function UpdateADIGST(){
		mssql_select_db("Import");
		
		if($this->PostVars['slcCustomers'] == 'ADISGWMS'){
		
			$Customer			= trim($this->PostVars['slcCustomers']);
			$ShipmentNo			= trim($this->PostVars['txtShipmentEdit']);
			$PermitNo			= trim($this->PostVars['adiPermitNo']);
			$PermitDate			= date('Y-m-d', strtotime(str_replace('/', '-', trim($this->PostVars['adiPermitDate']))));
			$JobType			= trim($this->PostVars['adiJobType']);
			$Comment			= trim($this->PostVars['adiRemarks']);
			$CustInvValue		= trim($this->PostVars['adiCustInvValue']);
			$ExchangeRate		= trim($this->PostVars['adiExchangeRate']);
			$InvoiceValueSGD	= trim($this->PostVars['adiInvoiceValueSGD']);
			$OtherChargesSGD	= trim($this->PostVars['adiOtherChargesSGD']);
			$TotalInvoiceValue	= trim($this->PostVars['adiTotalInvoiceValue']);
			$GSTAmountValue		= trim($this->PostVars['adiGSTAmount']);
			$HAWB				= trim($this->PostVars['adiHAWB']);
			$MAWB				= trim($this->PostVars['adiMAWB']);
			$Flight				= trim($this->PostVars['adiFlight']);
			$DepDate			= date('Y-m-d', strtotime(str_replace('/', '-', trim($this->PostVars['adiDepDate']))));
			$DeliveryFrom		= trim($this->PostVars['adiDeliveryFrom']);
			$EnterBy			= trim($this->PostVars['UserID']);
			$EnterDate			= date('Y-m-d H:i:s');
			
			$data = array($ShipmentNo);
			
			if ($PermitNo != ''){
			
				mssql_query("BEGIN TRAN");
				
				$getOriginalHAWB = mssql_query("SELECT TOP 1 [Packlist].[HAWB] FROM ADISGWMS.dbo.[Packlist] WITH (NOLOCK) INNER JOIN ADISGWMS.dbo.[Order] ON [Order].[OrderNumber] = [Packlist].[DocumentNumber] WHERE [Order].[MoNumber] = '".$ShipmentNo."' AND [Order].[Forwarder] NOT LIKE '%ups%'");
				$getOriginalHAWBArr = mssql_fetch_assoc($getOriginalHAWB);
				$OrigHAWB = $getOriginalHAWBArr['HAWB'];
				
				$updateHAWBErr = 0;
				
				if($OrigHAWB != $HAWB){
					
					$DNDetailsData = $this->PostVars['DNDetails'];
					
					$OrderNumbers = array();
					foreach($DNDetailsData as $DNRowsData){
						array_push($OrderNumbers, $DNRowsData[0]);
					}
					$OrderNumbers = array_unique($OrderNumbers);
					
					foreach($OrderNumbers as $OrderValue){
						$updatePacklistHAWB = "UPDATE ADISGWMS.dbo.Packlist SET [HAWB] = '".$HAWB."', [HAWBOrig] = '".$OrigHAWB."', [HAWBRevisedFrom] = 'GST' WHERE [DocumentNumber] = '".$OrderValue."'";
						if(!mssql_query($updatePacklistHAWB)){
							$updateHAWBErr++;
						}
					}
					
				}
				
				if($updateHAWBErr == 0){
					
					$mYupdateqry = "UPDATE Outgoing_ADIShipment SET [PermitNo] = '".$PermitNo."', [PermitDate] = '".$PermitDate."', [JobType] = '".$JobType."', [Comment] = '".$Comment."', [EditBy] = '".$EnterBy."', [EditDate] = '".$EnterDate."', [CustInvValue] = '".$CustInvValue."', [ExchangeRate] = '".$ExchangeRate."', [InvoiceValueSGD] = '".$InvoiceValueSGD."', [GSTAmountValue] = '".$GSTAmountValue."', [HAWB] = '".$HAWB."', [HAWBOrig] = [HAWB], [MAWB] = '".$MAWB."', [Flight] = '".$Flight."', [DepDate] = '".$DepDate."', [DeliveryFrom] = '".$DeliveryFrom."', [OtherCharge] = '".$OtherChargesSGD."', [TotalInvoiceValue] = '".$TotalInvoiceValue."' WHERE [ShipmentNo] = '".$ShipmentNo."'";
					
					if(mssql_query($mYupdateqry)){
						
						$DNDetails = $this->PostVars['DNDetails'];
						
						$grouped_DNDetails = array(array('OrderNumber' => $DNDetails[0][0], 'Partnumber' => $DNDetails[0][4], 'UnitPrice' => $DNDetails[0][6]));
	
						foreach($DNDetails as $DNRowData){
							
							$inarray = 0;
							
							foreach($grouped_DNDetails as $grouped_DNData){
								
								if($grouped_DNData['OrderNumber'] == $DNRowData[0] && $grouped_DNData['Partnumber'] == $DNRowData[4]){
									$inarray++;
								}
								
							}
							
							if($inarray == 0){
								$forgroup_array = array('OrderNumber' => $DNRowData[0], 'Partnumber' => $DNRowData[4], 'UnitPrice' => $DNRowData[6]);
								array_push($grouped_DNDetails, $forgroup_array);
							}
							
						}
						
						//var_dump($grouped_DNDetails);
						
						$SAPShipmentData = array();
	
						$getSAPShipmentDetails = mssql_query("SELECT [DN],[PartNumber],[Quantity],[UnitPrice] FROM ADISGWMS.dbo.[SAPDailyShipment] WHERE [ShipmentNumber] = '".$ShipmentNo."' GROUP BY [DN],[PartNumber],[Quantity],[UnitPrice]");
						
						$hasError = 0;
						
						if (mssql_num_rows($getSAPShipmentDetails) > 0){
							
							while($SAPShipmentData[] = mssql_fetch_assoc($getSAPShipmentDetails)){}
							array_pop($SAPShipmentData);
							
							foreach($grouped_DNDetails as $grouped_DNData){
								
								foreach($SAPShipmentData as $SAPShipmentData_array){
									
									if($grouped_DNData['OrderNumber'] == $SAPShipmentData_array['DN'] && $grouped_DNData['Partnumber'] == $SAPShipmentData_array['PartNumber']){
										
										if($grouped_DNData['UnitPrice'] != $SAPShipmentData_array['UnitPrice']){
											
											$totalValue = $SAPShipmentData_array['Quantity'] * $grouped_DNData['UnitPrice'];
											$updateSAPDailyShipmentQuery = "UPDATE ADISGWMS.dbo.SAPDailyShipment SET [UnitPrice] = '".$grouped_DNData['UnitPrice']."', [ShipmentValue] = '".$totalValue."' WHERE [ShipmentNumber] = '".$ShipmentNo."' AND [DN] = '".$SAPShipmentData_array['DN']."' AND [PartNumber] = '".$SAPShipmentData_array['PartNumber']."'";
											$updateSAPDailyShipment = mssql_query($updateSAPDailyShipmentQuery);
											
											if($updateSAPDailyShipment && mssql_rows_affected($this->conn)){
												
											}else{
												$hasError++;
											}
										}
										
									}
									
								}
								
							}
							
						} else {
							$hasError++;
						}
						
						if($hasError == 0){
							mssql_query("COMMIT");
							array_push($data,'successUPDATE'); 
						} else {
							mssql_query("ROLLBACK");
							array_push($data,'erroradd');
						}
						
					} else {
						mssql_query("ROLLBACK");
						array_push($data,'erroradd');
					}
					
				} else {
					mssql_query("ROLLBACK");
					array_push($data,'erroradd');
				}
			
			}else{
				array_push($data,'erroradd');
			}
		
		} else {
			array_push($data,'erroradd');
		}

		return json_encode($data);
	}
	
	function validateChanges(){
		mssql_select_db("ADISGWMS");
		extract($this->PostVars);
		
		$result_array = array("status"=>false,"message"=>"");
		
		$grouped_DNDetails = array(array('OrderNumber' => $DNDetails[0][0], 'Partnumber' => $DNDetails[0][4], 'UnitPrice' => $DNDetails[0][6]));
		
		foreach($DNDetails as $DNRowData){
			
			$inarray = 0;
			
			foreach($grouped_DNDetails as $grouped_DNData){
				
				if($grouped_DNData['OrderNumber'] == $DNRowData[0] && $grouped_DNData['Partnumber'] == $DNRowData[4]){
					$inarray++;
				}
				
			}
			
			if($inarray == 0){
				$forgroup_array = array('OrderNumber' => $DNRowData[0], 'Partnumber' => $DNRowData[4], 'UnitPrice' => $DNRowData[6]);
				array_push($grouped_DNDetails, $forgroup_array);
			}
			
		}
		
		$SAPShipmentData = array();
		
		$getSAPShipmentDetails = mssql_query("SELECT [DN],[PartNumber],[UnitPrice] FROM [SAPDailyShipment] WHERE [ShipmentNumber] = '".$ShipmentNumber."' GROUP BY [DN],[PartNumber],[UnitPrice]");
		
		if (mssql_num_rows($getSAPShipmentDetails) > 0){
			
			while($SAPShipmentData[] = mssql_fetch_assoc($getSAPShipmentDetails)){}
			array_pop($SAPShipmentData);
			
			$hasChanges = 0;
			$zeroChanges = 0;
		
			foreach($grouped_DNDetails as $grouped_DNData){
				
				foreach($SAPShipmentData as $SAPShipmentData_array){
					
					if($grouped_DNData['OrderNumber'] == $SAPShipmentData_array['DN'] && $grouped_DNData['Partnumber'] == $SAPShipmentData_array['PartNumber']){
						
						if($SAPShipmentData_array['UnitPrice'] > 0){
							
							if($grouped_DNData['UnitPrice'] != $SAPShipmentData_array['UnitPrice']){
								$hasChanges++;
							}
							
						} else {
							if($grouped_DNData['UnitPrice'] != $SAPShipmentData_array['UnitPrice']){
								$zeroChanges++;
							}
						}
						
					}
					
				}
				
			}
			
			if($hasChanges > 0){
				
				$otpindex = $ShipmentNumber.'-'.date('YmdHis').'-'.substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 12);
				$otp = substr(str_shuffle("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 6);
				
				$insertOTP = "Insert into Import.dbo.Outgoing_ADIShipment_OTP([OTPIndex],[OTP]) values ('".$otpindex."','".$otp."');";
				
				if (mssql_query($insertOTP)){
					
					$getSupervisors = mssql_query("SELECT DISTINCT [email] FROM Import.dbo.[SupervisorAccess] WHERE [module] = 'Export' AND [disabled] = 0");
		
					if (mssql_num_rows($getSupervisors) > 0){
						
						$SupervisorsArray = array();
						while($SupervisorsArray[] = mssql_fetch_assoc($getSupervisors)){}
						array_pop($SupervisorsArray);
						
						$SupervisorsEmails = array();
						foreach($SupervisorsArray as $SupervisorsData){
							array_push($SupervisorsEmails, $SupervisorsData['email']);
						}
					
						$JSIMailClass = new JSIMailClass();
						$JSIMailClass->emailfrom = 'sysalert-wms@omnilogistics.com';
						$JSIMailClass->namefrom = 'OMNI System Notification';
						$JSIMailClass->emailreplyto = 'donotreply@omnilogistics.com';
						$JSIMailClass->namereplyto = 'OMNI ALERT';
						
						$JSIMailClass->subject  = "GST - OTP";
						$JSIMailClass->setfrom  = "GST - OTP";
						$JSIMailClass->emailto  = $SupervisorsEmails;
						$JSIMailClass->emailBCC	= array('jmagustin@omnilogistics.com');	
						//$JSIMailClass->emailBCC	= array('jmagustin@omnilogistics.com');	
						//$JSIMailClass->emailBCC	= array('globaldev@omnilogistics.com');	
						
						$emailhtml = '<div style="font-size: 16px; font-weight: bold; border:0px;">OTP : '.$otp.'</div>';
						
						$variables['title']        		= 'GST Export - OTP';
						$variables['firstparagraph']	= 'Please use the OTP below to update the changes on the GST Export Module.';
						$variables['html']         		= $emailhtml; 
						
						$template = file_get_contents("email_template/otp.html");
						foreach($variables as $key => $value)
						{
							$template = str_replace('{{'.$key.'}}', $value, $template);
						}

						$JSIMailClass->message = $template;

						$emailSent = $JSIMailClass->sendMail(); 
						
						if($emailSent){
							$result_array = array("status"=>false,"message"=>"NeedOTP","OTPIndex"=>$otpindex);
						} else {
							$result_array = array("status"=>false,"message"=>"OTPnotSent");
						}
						
					} else {
						$result_array = array("status"=>false,"message"=>"Failed to retrieved supervisors email for OTP Receiver. Please try again.");
					}
					
				} else {
					$result_array = array("status"=>false,"message"=>"OTPnotSent");
				}
				
			} elseif($zeroChanges > 0) {
				$result_array = array("status"=>true,"message"=>"NoNeedOTP");
			} else {
				$result_array = array("status"=>true,"message"=>"Proceed");
			}
			
		} else {
			$result_array = array("status"=>false,"message"=>"Failed to retrieved data for validation. Please try again.");
		}
		
		return json_encode($result_array);
		
	}
	
	function validateOTP(){
		mssql_select_db("Import");
		extract($this->PostVars);
		
		$result_array = array("status"=>false,"message"=>"","OTPIndex"=>$OTPindex);
		
		$validateOTP = mssql_query("SELECT * FROM [Outgoing_ADIShipment_OTP] WHERE [OTPIndex] = '".$OTPindex."' AND [OTP] = '".$OTP."'");
		
		if (mssql_num_rows($validateOTP) > 0){
			$result_array = array("status"=>true,"message"=>"OTP Correct.","OTPIndex"=>$OTPindex);
		} else {
			$result_array = array("status"=>false,"message"=>"Incorrect OTP. Please try again.","OTPIndex"=>$OTPindex);
		}
		
		return json_encode($result_array);
		
	}
	
	function checkeEditmode(){
		mssql_select_db("ADISGWMS");
		extract($this->PostVars);
		
		$result_array = array("status"=>false,"message"=>"");
		
		$notzero = 0;
		foreach($DNDetails as $DNRowData){
			
			$notzero++;
			
		}
		
		if($notzero > 0){
			
			$otpindex = $ShipmentNumber.'-'.date('YmdHis').'-'.substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 12);
			$otp = substr(str_shuffle("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 6);
			
			$insertOTP = "Insert into Import.dbo.Outgoing_ADIShipment_OTP([OTPIndex],[OTP]) values ('".$otpindex."','".$otp."');";
			
			if (mssql_query($insertOTP)){
				
				$getSupervisors = mssql_query("SELECT DISTINCT [email] FROM Import.dbo.[SupervisorAccess] WHERE [module] = 'Export' AND [disabled] = 0");
	
				if (mssql_num_rows($getSupervisors) > 0){
					
					$SupervisorsArray = array();
					while($SupervisorsArray[] = mssql_fetch_assoc($getSupervisors)){}
					array_pop($SupervisorsArray);
					
					$SupervisorsEmails = array();
					foreach($SupervisorsArray as $SupervisorsData){
						array_push($SupervisorsEmails, $SupervisorsData['email']);
					}
				
					$JSIMailClass = new JSIMailClass();
					$JSIMailClass->emailfrom = 'sysalert-wms@omnilogistics.com';
					$JSIMailClass->namefrom = 'OMNI System Notification';
					$JSIMailClass->emailreplyto = 'donotreply@omnilogistics.com';
					$JSIMailClass->namereplyto = 'OMNI ALERT';
					
					$JSIMailClass->subject  = "GST - OTP";
					$JSIMailClass->setfrom  = "GST - OTP";
					$JSIMailClass->emailto  = $SupervisorsEmails;
					$JSIMailClass->emailBCC	= array('jmagustin@omnilogistics.com');	
					//$JSIMailClass->emailBCC	= array('jmagustin@omnilogistics.com');	
					//$JSIMailClass->emailBCC	= array('globaldev@omnilogistics.com');	
					
					$emailhtml = '<div style="font-size: 16px; font-weight: bold; border:0px;">OTP : '.$otp.'</div>';
					
					$variables['title']        		= 'GST Export - OTP';
					$variables['firstparagraph']	= 'Please use the OTP below to update the changes on the GST Export Module.';
					$variables['html']         		= $emailhtml; 
					
					$template = file_get_contents("email_template/otp.html");
					foreach($variables as $key => $value)
					{
						$template = str_replace('{{'.$key.'}}', $value, $template);
					}

					$JSIMailClass->message = $template;

					$emailSent = $JSIMailClass->sendMail(); 
					
					if($emailSent){
						$result_array = array("status"=>false,"message"=>"NeedOTP","OTPIndex"=>$otpindex);
					} else {
						$result_array = array("status"=>false,"message"=>"OTP Failed to send to supervisors. Please try again.");
					}
					
				} else {
					$result_array = array("status"=>false,"message"=>"Failed to retrieved supervisors email for OTP Receiver. Please try again.");
				}
				
			} else {
				$result_array = array("status"=>false,"message"=>"OTP Failed to send to supervisors. Please try again.");
			}
			
		} else {
			$result_array = array("status"=>true,"message"=>"NoNeedOTP");
		}
		
		return json_encode($result_array);
		
	}
	
	function GetQuantityCustomer(){
		mssql_select_db("MasterData");
		$sql = mssql_fetch_assoc(mssql_query("select DBName from WawiCustomer where [WawiIndex] = '".$this->PostVars['WawiIndex']."'"));
			
		return $sql['DBName'];
	}
	
	function UpdateReference(){

		mssql_select_db("Import");
		$sqlShipment = mssql_fetch_assoc(mssql_query("select Customer from Outgoing_Shipment where OutShipReference=".$this->PostVars['ShipReference']));
		$Customer = $sqlShipment['Customer'];
		
		$ListRefToUpdate = $this->PostVars['ListRef'];
		foreach($ListRefToUpdate as $RefNum){
			$RefArray = explode("-",$RefNum);
			mssql_query("Update Export set OutShipReference=".$this->PostVars['ShipReference'].",InvoiceValue=".$RefArray[1].",Qty=".$RefArray[3].",InvoiceValueSGD=".$RefArray[4]." where Reference=".$RefArray[0]." and TaxScheme='".$RefArray[2]."' and wawiindex = '".$Customer."'");
		}
		
		$data = array($this->PostVars['ShipReference'],$this->PostVars['action']);

		return json_encode($data);
	}
	
	function UpdateReferenceNull(){
		mssql_select_db('Import');
		mssql_query("Update Export set OutShipReference=NULL,Qty=NULL,InvoiceValueSGD=NULL,InvoiceValue=NULL where Reference= ".$this->PostVars['RefNum']);
	}
}

?>

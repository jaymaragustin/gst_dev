<?php
if (isset($_POST['todo'])){
	require ('../../include/config.php');
	$ExportGSTClass = new ExportGSTClass($_REQUEST);
	
	header('Content-Type: application/json');
	if ($_POST['todo'] == 'DataSearch'){
		echo $ExportGSTClass->DataSearch();
	} else if ($_POST['todo'] == 'prepareExportModule'){
		echo $ExportGSTClass->prepareExportModule();
	} else if ($_POST['todo'] == 'GetAvailablePacklistNumber'){
		echo $ExportGSTClass->GetAvailablePacklistNumber();
	} else if ($_POST['todo'] == 'ProcessData'){
		echo $ExportGSTClass->ProcessData();
	} else if ($_POST['todo'] == 'UpdateReference'){
		echo $ExportGSTClass->UpdateReference();
	} else if ($_POST['todo'] == 'UpdateReferenceNull'){
		echo $ExportGSTClass->UpdateReferenceNull();
	} else if ($_POST['todo'] == 'getPacklistData'){
		echo $ExportGSTClass->getPacklistData();
	} else if ($_POST['todo'] == 'GetAvailableShipmentNumber'){
		echo $ExportGSTClass->GetAvailableShipmentNumber();
	} else if ($_POST['todo'] == 'ADIGetShipment'){
		echo $ExportGSTClass->ADIGetShipment();
	} else if ($_POST['todo'] == 'ProcessADIData'){
		echo $ExportGSTClass->ProcessADIData();
	} else if ($_POST['todo'] == 'DataADISearch'){
		echo $ExportGSTClass->DataADISearch();
	} else if ($_POST['todo'] == 'ADIGetEditShipment'){
		echo $ExportGSTClass->ADIGetEditShipment();
	}
}

Class ExportGSTClass{
	var $PostVars;
	function __construct($vars){
		$this->PostVars = $vars;
		$serverConn = unserialize(base64_decode(SQL_CONN));
		$this->conn = mssql_connect($serverConn[0],$serverConn[1],$serverConn[2]);
		mssql_select_db("Import");
		
		foreach($this->PostVars as $a => $b){
			$this->PostVars[$a] = $this->mssqlQuotedString($b);
		}
	}
	
	function mssqlQuotedString($str){
		$str = str_replace("\\", "\\\\", $str);
		$str = str_replace("'", "''", $str);
		
		return $str;
	}
	
	function getDestination(){
		mssql_select_db("STGMM");
		$sql = mssql_query("select country_code from country order by country_code ASC");
		while($row[] = mssql_fetch_assoc($sql)){}
		array_pop($row);
		
		return $row;
	}
	
	function prepareExportModule(){
		mssql_select_db("Import");
		$data = array();
		
		$sql = mssql_query("select GSTIndex AS WawiIndex,WawiAlias from MasterData.dbo.WawiCustomer where MasterData.dbo.WawiCustomer.GST = 1 and MasterData.dbo.WawiCustomer.status = 1 group by GSTIndex,WawiAlias order by GSTIndex");
		while($CustomerList[] = mssql_fetch_assoc($sql)){}
		array_pop($CustomerList);
		
		$sql = mssql_query("select top 2 ShipmentNumber from v_ol_ExportShipment");
		
		if(mssql_num_rows($sql) > 0){
			$chkrecords = 'data';
		} else {
			$chkrecords = 'nodata';
		}
		
		unset($sql);
		$sql = mssql_query("select Forwarder from Forwarder where [Status] = 1 order by Forwarder ASC");
		$ListofFrowarder = array();
		while($row = mssql_fetch_assoc($sql)){
			$ListofFrowarder[$row['Forwarder']] = $row['Forwarder'];
		}

		unset($sql);
		$sql = mssql_query("select Supplier from Supplier where [Status] = 1 order by Forwarder ASC");
		$ListofSupplier = array();
		while($row = mssql_fetch_assoc($sql)){
			$ListofSupplier[$row['Supplier']] = $row['Supplier'];
		}
		
		array_push($data,$CustomerList);
		array_push($data,$this->GSTRate());
		array_push($data,$chkrecords);
		array_push($data,$ListofFrowarder);
		array_push($data,$this->getDestination());

		return json_encode($data);
	}
	
	function ListCurrency(){
		$sql = mssql_query("SELECT DISTINCT Currency FROM Country ORDER BY Currency");
		if (mssql_num_rows($sql) > 0){
			while($CurrencyList[] = mssql_fetch_assoc($sql)){}
			array_pop($CurrencyList);
		} else {
			$CurrencyList = array(array('Currency' => 'SGD'),array('Currency' => 'USD'));
		}
		
		return $CurrencyList;
	}
	
	function GSTRate(){
		$sql = mssql_query("SELECT Value FROM Setup WHERE Entry='GST'");
		if (mssql_num_rows($sql) > 0){
			$sql = mssql_fetch_assoc($sql);
			$GSTRate = $sql['Value'];
		} else {
			$GSTRate = 7;
		}
		
		return $GSTRate;
	}
	
	function DataSearch(){
		if ($this->PostVars['SearchAction'] == 'First'){ // First run
			$sql = mssql_query("select *,convert(text,CustInvNo1) as CustInvNo from v_ol_ExportShipment where ShipmentNumber = (select top 1 ShipmentNumber from v_ol_ExportShipment order by ShipmentNumber ASC)");
		} else if($this->PostVars['SearchAction'] == 'Next'){
			$sql = mssql_query("select *,convert(text,CustInvNo1) as CustInvNo from v_ol_ExportShipment where ShipmentNumber = (select top 1 ShipmentNumber from v_ol_ExportShipment where ShipmentNumber > ".$this->PostVars['ShipmentNumber']." order by ShipmentNumber ASC)");
		} else if($this->PostVars['SearchAction'] == 'Previous'){
			$sql = mssql_query("select *,convert(text,CustInvNo1) as CustInvNo from v_ol_ExportShipment where ShipmentNumber = (select top 1 ShipmentNumber from v_ol_ExportShipment where ShipmentNumber < ".$this->PostVars['ShipmentNumber']." order by ShipmentNumber DESC)");
		} else if($this->PostVars['SearchAction'] == 'Last'){
			$sql = mssql_query("select *,convert(text,CustInvNo1) as CustInvNo from v_ol_ExportShipment where ShipmentNumber = (select top 1 ShipmentNumber from v_ol_ExportShipment order by ShipmentNumber DESC)");
		} else if($this->PostVars['SearchAction'] == 'Search'){
			$sql = mssql_query("select *,convert(text,CustInvNo1) as CustInvNo from v_ol_ExportShipment where ShipmentNumber = ".$this->PostVars['ShipmentNumber']);
		}

		if (mssql_num_rows($sql) < 1){
			return json_encode($this->PostVars['SearchAction']);
		} else {
			$data = array();
			$data1 = array();
			
			while($row = mssql_fetch_assoc($sql)){
				$sqlplist = mssql_query($row['plist']);
				$WawiIndex = $row['WawiIndex'];


				if (mssql_num_rows($sqlplist) > 0){
					unset($rowPlist);
					$rowPlist = array();
					while($rowPerPlist = mssql_fetch_assoc($sqlplist)){
						//WawiIndex = '$WawiIndex' add 
						$sqlGetFromExportInvValue = mssql_fetch_assoc(mssql_query("select InvoiceValue from export where reference = ".$row['Packlist']." and TaxScheme = '".$rowPerPlist['TaxScheme']."' and WawiIndex = '$WawiIndex' " ));
						
						$rowPerPlist['InvoiceValue'] = number_format($sqlGetFromExportInvValue['InvoiceValue'],2, '.', '');
						array_push($rowPlist,$rowPerPlist);
					}
					
					$tmpPlist[$row['Packlist']] = $rowPlist;
				} else {
					$tmpPlist[$row['Packlist']] = 'nodata';
				}
				
				array_push($data,$row);
			}
			
			array_push($data1,$data);
			array_push($data1,$tmpPlist);
			array_push($data1,$this->AvailablePlist($WawiIndex));
			
			return json_encode($data1);
		}
	}
	
	function DataADISearch(){
		if ($this->PostVars['SearchAction'] == 'First'){ // First run
			$sql = mssql_query("SELECT PackListPosition.PackListNumber, [Order].CustomerReference as DN, [SAPDailyShipment].[ShipmentNumber], Entry.TaxScheme, [Order].[ShippingCountry], CONVERT(VARCHAR(10), [Order].ShippedDateTime, 101) as CargoReleasedDate, [Order].GSTIncoTerm as IncoTerm, [Order].[Forwarder], PackList.HAWB, [Order].CustomerName1 as ShipTo, PackListPosition.Partnumber, SUM(PackListPosition.Quantity) AS TotalQty, [SAPDailyShipment].[UnitPrice], [SAPDailyShipment].[ShipmentValue], [Entry].[Invoice],Outgoing_ADIShipment.PermitNo, CONVERT(VARCHAR(10), Outgoing_ADIShipment.PermitDate, 101) as PermitDate, Outgoing_ADIShipment.JobType, Outgoing_ADIShipment.Comment,Outgoing_ADIShipment.CustInvValue, Outgoing_ADIShipment.ExchangeRate, Outgoing_ADIShipment.InvoiceValueSGD, Outgoing_ADIShipment.GSTAmountValue
								FROM ADISGWMS.dbo.PackListPosition
								INNER JOIN ADISGWMS.dbo.PackList ON PackList.PackListNumber = PackListPosition.PackListNumber AND PackList.DocumentNumber = PackListPosition.DocumentNumber INNER JOIN ADISGWMS.dbo.[Order] ON [Order].OrderNumber = PackList.DocumentNumber INNER JOIN ADISGWMS.dbo.Entry ON PackListPosition.EntryNumber = Entry.EntryNumber INNER JOIN ADISGWMS.dbo.SAPDailyShipment ON [SAPDailyShipment].[DN] = [Order].CustomerReference AND [SAPDailyShipment].[PartNumber] = PackListPosition.Partnumber
								INNER JOIN Import.dbo.Outgoing_ADIShipment ON Outgoing_ADIShipment.ShipmentNo = [SAPDailyShipment].[ShipmentNumber]
								GROUP BY PackListPosition.PackListNumber, [Order].CustomerReference, Entry.TaxScheme, [Order].DNStatus, CONVERT(VARCHAR(10), [Order].ShippedDateTime, 101), [Order].GSTIncoTerm, [Order].CustomerName1, [SAPDailyShipment].[ShipmentNumber], [SAPDailyShipment].[UnitPrice], [SAPDailyShipment].[ShipmentValue], [Entry].[Invoice], [Order].[ShippingCountry], [Order].[Forwarder], PackList.HAWB, PackListPosition.Partnumber,Outgoing_ADIShipment.PermitNo, CONVERT(VARCHAR(10), Outgoing_ADIShipment.PermitDate, 101), Outgoing_ADIShipment.JobType, Outgoing_ADIShipment.Comment,Outgoing_ADIShipment.CustInvValue, Outgoing_ADIShipment.ExchangeRate, Outgoing_ADIShipment.InvoiceValueSGD, Outgoing_ADIShipment.GSTAmountValue
								Having [Order].DNStatus = 'CARGO RELEASED' AND [SAPDailyShipment].[ShipmentNumber] = (select top 1 ShipmentNumber from ADISGWMS.dbo.[SAPDailyShipment] order by ShipmentNumber ASC)");
		} else if($this->PostVars['SearchAction'] == 'Next'){
			$sql = mssql_query("SELECT PackListPosition.PackListNumber, [Order].CustomerReference as DN, [SAPDailyShipment].[ShipmentNumber], Entry.TaxScheme, [Order].[ShippingCountry], CONVERT(VARCHAR(10), [Order].ShippedDateTime, 101) as CargoReleasedDate, [Order].GSTIncoTerm as IncoTerm, [Order].[Forwarder], PackList.HAWB, [Order].CustomerName1 as ShipTo, PackListPosition.Partnumber, SUM(PackListPosition.Quantity) AS TotalQty, [SAPDailyShipment].[UnitPrice], [SAPDailyShipment].[ShipmentValue], [Entry].[Invoice],Outgoing_ADIShipment.PermitNo, CONVERT(VARCHAR(10), Outgoing_ADIShipment.PermitDate, 101) as PermitDate, Outgoing_ADIShipment.JobType, Outgoing_ADIShipment.Comment,Outgoing_ADIShipment.CustInvValue, Outgoing_ADIShipment.ExchangeRate, Outgoing_ADIShipment.InvoiceValueSGD, Outgoing_ADIShipment.GSTAmountValue
								FROM ADISGWMS.dbo.PackListPosition
								INNER JOIN ADISGWMS.dbo.PackList ON PackList.PackListNumber = PackListPosition.PackListNumber AND PackList.DocumentNumber = PackListPosition.DocumentNumber INNER JOIN ADISGWMS.dbo.[Order] ON [Order].OrderNumber = PackList.DocumentNumber INNER JOIN ADISGWMS.dbo.Entry ON PackListPosition.EntryNumber = Entry.EntryNumber INNER JOIN ADISGWMS.dbo.SAPDailyShipment ON [SAPDailyShipment].[DN] = [Order].CustomerReference AND [SAPDailyShipment].[PartNumber] = PackListPosition.Partnumber
								INNER JOIN Import.dbo.Outgoing_ADIShipment ON Outgoing_ADIShipment.ShipmentNo = [SAPDailyShipment].[ShipmentNumber]
								GROUP BY PackListPosition.PackListNumber, [Order].CustomerReference, Entry.TaxScheme, [Order].DNStatus, CONVERT(VARCHAR(10), [Order].ShippedDateTime, 101), [Order].GSTIncoTerm, [Order].CustomerName1, [SAPDailyShipment].[ShipmentNumber], [SAPDailyShipment].[UnitPrice], [SAPDailyShipment].[ShipmentValue], [Entry].[Invoice], [Order].[ShippingCountry], [Order].[Forwarder], PackList.HAWB, PackListPosition.Partnumber,Outgoing_ADIShipment.PermitNo, CONVERT(VARCHAR(10), Outgoing_ADIShipment.PermitDate, 101), Outgoing_ADIShipment.JobType, Outgoing_ADIShipment.Comment,Outgoing_ADIShipment.CustInvValue, Outgoing_ADIShipment.ExchangeRate, Outgoing_ADIShipment.InvoiceValueSGD, Outgoing_ADIShipment.GSTAmountValue
								Having [Order].DNStatus = 'CARGO RELEASED' AND [SAPDailyShipment].[ShipmentNumber] = (select top 1 ShipmentNumber from ADISGWMS.dbo.[SAPDailyShipment] where ShipmentNumber > ".$this->PostVars['ShipmentNumber']." order by ShipmentNumber ASC)");
		} else if($this->PostVars['SearchAction'] == 'Previous'){
			$sql = mssql_query("SELECT PackListPosition.PackListNumber, [Order].CustomerReference as DN, [SAPDailyShipment].[ShipmentNumber], Entry.TaxScheme, [Order].[ShippingCountry], CONVERT(VARCHAR(10), [Order].ShippedDateTime, 101) as CargoReleasedDate, [Order].GSTIncoTerm as IncoTerm, [Order].[Forwarder], PackList.HAWB, [Order].CustomerName1 as ShipTo, PackListPosition.Partnumber, SUM(PackListPosition.Quantity) AS TotalQty, [SAPDailyShipment].[UnitPrice], [SAPDailyShipment].[ShipmentValue], [Entry].[Invoice],Outgoing_ADIShipment.PermitNo, CONVERT(VARCHAR(10), Outgoing_ADIShipment.PermitDate, 101) as PermitDate, Outgoing_ADIShipment.JobType, Outgoing_ADIShipment.Comment,Outgoing_ADIShipment.CustInvValue, Outgoing_ADIShipment.ExchangeRate, Outgoing_ADIShipment.InvoiceValueSGD, Outgoing_ADIShipment.GSTAmountValue
								FROM ADISGWMS.dbo.PackListPosition
								INNER JOIN ADISGWMS.dbo.PackList ON PackList.PackListNumber = PackListPosition.PackListNumber AND PackList.DocumentNumber = PackListPosition.DocumentNumber INNER JOIN ADISGWMS.dbo.[Order] ON [Order].OrderNumber = PackList.DocumentNumber INNER JOIN ADISGWMS.dbo.Entry ON PackListPosition.EntryNumber = Entry.EntryNumber INNER JOIN ADISGWMS.dbo.SAPDailyShipment ON [SAPDailyShipment].[DN] = [Order].CustomerReference AND [SAPDailyShipment].[PartNumber] = PackListPosition.Partnumber
								INNER JOIN Import.dbo.Outgoing_ADIShipment ON Outgoing_ADIShipment.ShipmentNo = [SAPDailyShipment].[ShipmentNumber]
								GROUP BY PackListPosition.PackListNumber, [Order].CustomerReference, Entry.TaxScheme, [Order].DNStatus, CONVERT(VARCHAR(10), [Order].ShippedDateTime, 101), [Order].GSTIncoTerm, [Order].CustomerName1, [SAPDailyShipment].[ShipmentNumber], [SAPDailyShipment].[UnitPrice], [SAPDailyShipment].[ShipmentValue], [Entry].[Invoice], [Order].[ShippingCountry], [Order].[Forwarder], PackList.HAWB, PackListPosition.Partnumber,Outgoing_ADIShipment.PermitNo, CONVERT(VARCHAR(10), Outgoing_ADIShipment.PermitDate, 101), Outgoing_ADIShipment.JobType, Outgoing_ADIShipment.Comment,Outgoing_ADIShipment.CustInvValue, Outgoing_ADIShipment.ExchangeRate, Outgoing_ADIShipment.InvoiceValueSGD, Outgoing_ADIShipment.GSTAmountValue
								Having [Order].DNStatus = 'CARGO RELEASED' AND [SAPDailyShipment].[ShipmentNumber] = (select top 1 ShipmentNumber from ADISGWMS.dbo.[SAPDailyShipment] where ShipmentNumber < ".$this->PostVars['ShipmentNumber']." order by ShipmentNumber DESC)");
		} else if($this->PostVars['SearchAction'] == 'Last'){
			$sql = mssql_query("SELECT PackListPosition.PackListNumber, [Order].CustomerReference as DN, [SAPDailyShipment].[ShipmentNumber], Entry.TaxScheme, [Order].[ShippingCountry], CONVERT(VARCHAR(10), [Order].ShippedDateTime, 101) as CargoReleasedDate, [Order].GSTIncoTerm as IncoTerm, [Order].[Forwarder], PackList.HAWB, [Order].CustomerName1 as ShipTo, PackListPosition.Partnumber, SUM(PackListPosition.Quantity) AS TotalQty, [SAPDailyShipment].[UnitPrice], [SAPDailyShipment].[ShipmentValue], [Entry].[Invoice],Outgoing_ADIShipment.PermitNo, CONVERT(VARCHAR(10), Outgoing_ADIShipment.PermitDate, 101) as PermitDate, Outgoing_ADIShipment.JobType, Outgoing_ADIShipment.Comment,Outgoing_ADIShipment.CustInvValue, Outgoing_ADIShipment.ExchangeRate, Outgoing_ADIShipment.InvoiceValueSGD, Outgoing_ADIShipment.GSTAmountValue
								FROM ADISGWMS.dbo.PackListPosition
								INNER JOIN ADISGWMS.dbo.PackList ON PackList.PackListNumber = PackListPosition.PackListNumber AND PackList.DocumentNumber = PackListPosition.DocumentNumber INNER JOIN ADISGWMS.dbo.[Order] ON [Order].OrderNumber = PackList.DocumentNumber INNER JOIN ADISGWMS.dbo.Entry ON PackListPosition.EntryNumber = Entry.EntryNumber INNER JOIN ADISGWMS.dbo.SAPDailyShipment ON [SAPDailyShipment].[DN] = [Order].CustomerReference AND [SAPDailyShipment].[PartNumber] = PackListPosition.Partnumber
								INNER JOIN Import.dbo.Outgoing_ADIShipment ON Outgoing_ADIShipment.ShipmentNo = [SAPDailyShipment].[ShipmentNumber]
								GROUP BY PackListPosition.PackListNumber, [Order].CustomerReference, Entry.TaxScheme, [Order].DNStatus, CONVERT(VARCHAR(10), [Order].ShippedDateTime, 101), [Order].GSTIncoTerm, [Order].CustomerName1, [SAPDailyShipment].[ShipmentNumber], [SAPDailyShipment].[UnitPrice], [SAPDailyShipment].[ShipmentValue], [Entry].[Invoice], [Order].[ShippingCountry], [Order].[Forwarder], PackList.HAWB, PackListPosition.Partnumber,Outgoing_ADIShipment.PermitNo, CONVERT(VARCHAR(10), Outgoing_ADIShipment.PermitDate, 101), Outgoing_ADIShipment.JobType, Outgoing_ADIShipment.Comment,Outgoing_ADIShipment.CustInvValue, Outgoing_ADIShipment.ExchangeRate, Outgoing_ADIShipment.InvoiceValueSGD, Outgoing_ADIShipment.GSTAmountValue
								Having [Order].DNStatus = 'CARGO RELEASED' AND [SAPDailyShipment].[ShipmentNumber] = (select top 1 ShipmentNumber from ADISGWMS.dbo.[SAPDailyShipment] order by ShipmentNumber DESC)");
		} else if($this->PostVars['SearchAction'] == 'Search'){
			$sql = mssql_query("SELECT PackListPosition.PackListNumber, [Order].CustomerReference as DN, [SAPDailyShipment].[ShipmentNumber], Entry.TaxScheme, [Order].[ShippingCountry], CONVERT(VARCHAR(10), [Order].ShippedDateTime, 101) as CargoReleasedDate, [Order].GSTIncoTerm as IncoTerm, [Order].[Forwarder], PackList.HAWB, [Order].CustomerName1 as ShipTo, PackListPosition.Partnumber, SUM(PackListPosition.Quantity) AS TotalQty, [SAPDailyShipment].[UnitPrice], [SAPDailyShipment].[ShipmentValue], [Entry].[Invoice],Outgoing_ADIShipment.PermitNo, CONVERT(VARCHAR(10), Outgoing_ADIShipment.PermitDate, 101) as PermitDate, Outgoing_ADIShipment.JobType, Outgoing_ADIShipment.Comment,Outgoing_ADIShipment.CustInvValue, Outgoing_ADIShipment.ExchangeRate, Outgoing_ADIShipment.InvoiceValueSGD, Outgoing_ADIShipment.GSTAmountValue
								FROM ADISGWMS.dbo.PackListPosition
								INNER JOIN ADISGWMS.dbo.PackList ON PackList.PackListNumber = PackListPosition.PackListNumber AND PackList.DocumentNumber = PackListPosition.DocumentNumber INNER JOIN ADISGWMS.dbo.[Order] ON [Order].OrderNumber = PackList.DocumentNumber INNER JOIN ADISGWMS.dbo.Entry ON PackListPosition.EntryNumber = Entry.EntryNumber INNER JOIN ADISGWMS.dbo.SAPDailyShipment ON [SAPDailyShipment].[DN] = [Order].CustomerReference AND [SAPDailyShipment].[PartNumber] = PackListPosition.Partnumber
								INNER JOIN Import.dbo.Outgoing_ADIShipment ON Outgoing_ADIShipment.ShipmentNo = [SAPDailyShipment].[ShipmentNumber]
								GROUP BY PackListPosition.PackListNumber, [Order].CustomerReference, Entry.TaxScheme, [Order].DNStatus, CONVERT(VARCHAR(10), [Order].ShippedDateTime, 101), [Order].GSTIncoTerm, [Order].CustomerName1, [SAPDailyShipment].[ShipmentNumber], [SAPDailyShipment].[UnitPrice], [SAPDailyShipment].[ShipmentValue], [Entry].[Invoice], [Order].[ShippingCountry], [Order].[Forwarder], PackList.HAWB, PackListPosition.Partnumber,Outgoing_ADIShipment.PermitNo, CONVERT(VARCHAR(10), Outgoing_ADIShipment.PermitDate, 101), Outgoing_ADIShipment.JobType, Outgoing_ADIShipment.Comment,Outgoing_ADIShipment.CustInvValue, Outgoing_ADIShipment.ExchangeRate, Outgoing_ADIShipment.InvoiceValueSGD, Outgoing_ADIShipment.GSTAmountValue
								Having [Order].DNStatus = 'CARGO RELEASED' AND [SAPDailyShipment].[ShipmentNumber] = '".$this->PostVars['ShipmentNumber']."'");
		}

		if (mssql_num_rows($sql) < 1){
			//if($this->PostVars['SearchAction'] == 'Search'){
			//	return json_encode($this->PostVars['SearchAction']);
			//} else {
				return json_encode("nodata");
			//}
		} else {
			$data = array();
			while($row = mssql_fetch_assoc($sql)){
				array_push($data,$row);
			}
			return json_encode($data);
		}
	}

	function getPacklistData(){
		
		if($this->PostVars['slcCustomers'] == 'ADISGWMS'){
			$this->PostVars['slcCustomers'] = 'ADIDGTWMS';
		}
		
		$sqlMaster = mssql_query("select STGIndex,STG,DBName from MasterData.dbo.WawiCustomer where MasterData.dbo.WawiCustomer.WawiIndex='".$this->PostVars['slcCustomers']."'; ");
		if (mssql_num_rows($sqlMaster)){
			$CustomerInfoArr = mssql_fetch_assoc($sqlMaster);
			$DBName = $CustomerInfoArr['DBName'];

			$sql = $this->AnalyzeSQL(mssql_query("exec p_ExportPacklist '".$DBName."',".$this->PostVars['packlistnumber']));
		}
		

		return json_encode($sql);
	}

	function AnalyzeSQL($sql){
		if (mssql_num_rows($sql) > 0){
			while($row[] = mssql_fetch_assoc($sql)){}
			array_pop($row);
		} else {
			$row = 'nodata';
		}
		
		return $row;
	}
	
	function AvailablePlist($WawiIndex){
		$sql = mssql_query("select top(5000) plist,WawiIndex from v_ol_exportAvailablePlist where WawiIndex = '".$WawiIndex."' group by plist,WawiIndex");
		if (mssql_num_rows($sql) > 0){
			$data = array();
			while($row = mssql_fetch_assoc($sql)){
				$sqlplist = mssql_query($row['plist']);
				if (mssql_num_rows($sqlplist) > 0){
					unset($rowPlist);
					while($rowPlist[] = mssql_fetch_assoc($sqlplist)){}
					array_pop($rowPlist);
					array_push($data,$rowPlist);
				}
			}
			
			if (count($data) == 0){
				$data = "nodata";
			}
		} else {
			$data = "nodata";
		}
		
		return $data;
	}
	
	function GetAvailablePacklistNumber(){
		mssql_select_db("Import");
		return json_encode($this->AvailablePlist($this->PostVars['WawiIndex']));
	}
	
	function GetAvailableShipmentNumber(){
		
		mssql_select_db("Import");
		
		if($this->PostVars['WawiIndex'] == 'ADISGWMS'){
			$this->PostVars['WawiIndex'] = 'ADIDGTWMS';
		}
		
		$sqlMaster = mssql_query("select TOP 1 STGIndex,STG,DBName from MasterData.dbo.WawiCustomer where MasterData.dbo.WawiCustomer.WawiIndex='".$this->PostVars['WawiIndex']."'; ");
		if (mssql_num_rows($sqlMaster)){
			$CustomerInfoArr = mssql_fetch_assoc($sqlMaster);
			$DBName = $CustomerInfoArr['DBName'];
		
			//$sql = mssql_query("select ShipmentNumber from ".$DBName.".dbo.SAPDailyShipment where GSTProcessed = 0 group by ShipmentNumber");
			$sql = mssql_query("SELECT [SAPDailyShipment].ShipmentNumber FROM ".$DBName.".dbo.SAPDailyShipment INNER JOIN ".$DBName.".dbo.[Order] ON CAST([SAPDailyShipment].[DN] AS VARCHAR) = [Order].CustomerReference WHERE [SAPDailyShipment].GSTProcessed = 0 AND [Order].DNStatus = 'CARGO RELEASED' GROUP BY [SAPDailyShipment].ShipmentNumber,[Order].DNStatus");
			if (mssql_num_rows($sql) > 0){
				
				while($data[] = mssql_fetch_assoc($sql)){}
				array_pop($data);
				
			} else {
				$data = "nodata";
			}
		
		} else {
			$data = "nodata";
		}
		
		return json_encode($data);
	}
	
	function ADIGetShipment(){
		
		mssql_select_db("Import");
		
		if($this->PostVars['WawiIndex'] == 'ADISGWMS'){
			$this->PostVars['WawiIndex'] = 'ADIDGTWMS';
		}
		
		$sqlMaster = mssql_query("select TOP 1 STGIndex,STG,DBName from MasterData.dbo.WawiCustomer where MasterData.dbo.WawiCustomer.WawiIndex='".$this->PostVars['WawiIndex']."'; ");
		if (mssql_num_rows($sqlMaster)){
			$CustomerInfoArr = mssql_fetch_assoc($sqlMaster);
			$DBName = $CustomerInfoArr['DBName'];
		
			$sql = mssql_query("SELECT PackListPosition.PackListNumber, [Order].CustomerReference as DN, [SAPDailyShipment].[ShipmentNumber], Entry.TaxScheme, [Order].[ShippingCountry], CONVERT(VARCHAR(10), [Order].ShippedDateTime, 101) as CargoReleasedDate, [Order].GSTIncoTerm as IncoTerm, [Order].[Forwarder], PackList.HAWB, [Order].CustomerName1 as ShipTo, PackListPosition.Partnumber, SUM(PackListPosition.Quantity) AS TotalQty, [SAPDailyShipment].[UnitPrice], [SAPDailyShipment].[ShipmentValue], [Entry].[Invoice]
								FROM ".$DBName.".dbo.PackListPosition
								INNER JOIN ".$DBName.".dbo.PackList ON PackList.PackListNumber = PackListPosition.PackListNumber AND PackList.DocumentNumber = PackListPosition.DocumentNumber
								INNER JOIN ".$DBName.".dbo.[Order] ON [Order].OrderNumber = PackList.DocumentNumber
								INNER JOIN ".$DBName.".dbo.Entry ON PackListPosition.EntryNumber = Entry.EntryNumber
								INNER JOIN ".$DBName.".dbo.SAPDailyShipment ON [SAPDailyShipment].[DN] = [Order].CustomerReference AND [SAPDailyShipment].[PartNumber] = PackListPosition.Partnumber
								GROUP BY PackListPosition.PackListNumber, [Order].CustomerReference, Entry.TaxScheme, [Order].DNStatus, CONVERT(VARCHAR(10), [Order].ShippedDateTime, 101), [Order].GSTIncoTerm, [Order].CustomerName1, [SAPDailyShipment].[ShipmentNumber], [SAPDailyShipment].[UnitPrice], [SAPDailyShipment].[ShipmentValue], [Entry].[Invoice], [Order].[ShippingCountry], [Order].[Forwarder], PackList.HAWB, PackListPosition.Partnumber
								Having [Order].DNStatus = 'CARGO RELEASED' AND [SAPDailyShipment].[ShipmentNumber] = '".$this->PostVars['ShipmentNumber']."'");
			if (mssql_num_rows($sql) > 0){
				
				while($data[] = mssql_fetch_assoc($sql)){}
				array_pop($data);
				
			} else {
				$data = "nodata";
			}
		
		} else {
			$data = "nodata";
		}
		
		return json_encode($data);
	}
	
	function ADIGetEditShipment(){
		
		mssql_select_db("Import");
		
		if($this->PostVars['WawiIndex'] == 'ADISGWMS'){
			$this->PostVars['WawiIndex'] = 'ADIDGTWMS';
		}
		
		$sqlMaster = mssql_query("select TOP 1 STGIndex,STG,DBName from MasterData.dbo.WawiCustomer where MasterData.dbo.WawiCustomer.WawiIndex='".$this->PostVars['WawiIndex']."'; ");
		if (mssql_num_rows($sqlMaster)){
			$CustomerInfoArr = mssql_fetch_assoc($sqlMaster);
			$DBName = $CustomerInfoArr['DBName'];
		
			$sql = mssql_query("SELECT PackListPosition.PackListNumber, [Order].CustomerReference as DN, [SAPDailyShipment].[ShipmentNumber], Entry.TaxScheme, [Order].[ShippingCountry], CONVERT(VARCHAR(10), [Order].ShippedDateTime, 101) as CargoReleasedDate, [Order].GSTIncoTerm as IncoTerm, [Order].[Forwarder], PackList.HAWB, [Order].CustomerName1 as ShipTo, PackListPosition.Partnumber, SUM(PackListPosition.Quantity) AS TotalQty, [SAPDailyShipment].[UnitPrice], [SAPDailyShipment].[ShipmentValue], [Entry].[Invoice],Outgoing_ADIShipment.PermitNo, CONVERT(VARCHAR(10), Outgoing_ADIShipment.PermitDate, 101) as PermitDate, Outgoing_ADIShipment.JobType, Outgoing_ADIShipment.Comment,Outgoing_ADIShipment.CustInvValue, Outgoing_ADIShipment.ExchangeRate, Outgoing_ADIShipment.InvoiceValueSGD, Outgoing_ADIShipment.GSTAmountValue
								FROM ADISGWMS.dbo.PackListPosition
								INNER JOIN ADISGWMS.dbo.PackList ON PackList.PackListNumber = PackListPosition.PackListNumber AND PackList.DocumentNumber = PackListPosition.DocumentNumber INNER JOIN ADISGWMS.dbo.[Order] ON [Order].OrderNumber = PackList.DocumentNumber INNER JOIN ADISGWMS.dbo.Entry ON PackListPosition.EntryNumber = Entry.EntryNumber INNER JOIN ADISGWMS.dbo.SAPDailyShipment ON [SAPDailyShipment].[DN] = [Order].CustomerReference AND [SAPDailyShipment].[PartNumber] = PackListPosition.Partnumber
								INNER JOIN Import.dbo.Outgoing_ADIShipment ON Outgoing_ADIShipment.ShipmentNo = [SAPDailyShipment].[ShipmentNumber]
								GROUP BY PackListPosition.PackListNumber, [Order].CustomerReference, Entry.TaxScheme, [Order].DNStatus, CONVERT(VARCHAR(10), [Order].ShippedDateTime, 101), [Order].GSTIncoTerm, [Order].CustomerName1, [SAPDailyShipment].[ShipmentNumber], [SAPDailyShipment].[UnitPrice], [SAPDailyShipment].[ShipmentValue], [Entry].[Invoice], [Order].[ShippingCountry], [Order].[Forwarder], PackList.HAWB, PackListPosition.Partnumber,Outgoing_ADIShipment.PermitNo, CONVERT(VARCHAR(10), Outgoing_ADIShipment.PermitDate, 101), Outgoing_ADIShipment.JobType, Outgoing_ADIShipment.Comment,Outgoing_ADIShipment.CustInvValue, Outgoing_ADIShipment.ExchangeRate, Outgoing_ADIShipment.InvoiceValueSGD, Outgoing_ADIShipment.GSTAmountValue
								Having [Order].DNStatus = 'CARGO RELEASED' AND [SAPDailyShipment].[ShipmentNumber] = '".$this->PostVars['ShipmentNumber']."'");
								
			if (mssql_num_rows($sql) > 0){
				
				while($data[] = mssql_fetch_assoc($sql)){}
				array_pop($data);
				
			} else {
				$data = "nodata";
			}
		
		} else {
			$data = "nodata";
		}
		
		return json_encode($data);
	}
	
	function ProcessData(){
		if ($this->PostVars['btnSaveUpdate'] == 'Update'){
			return $this->UpdateGST();
		} else {
			return $this->AddNewGST();
		}
	}
	
	function ProcessADIData(){
		if ($this->PostVars['btnSaveUpdate'] == 'Update'){
			return $this->UpdateADIGST();
		} else {
			return $this->AddNewADIGST();
		}
	}
	
	function UpdateGST(){
		$data = array($this->PostVars['txtShipmentNumber']);
		mssql_query("BEGIN TRAN");
		
		$PD = trim($this->PostVars['txtPermitDate']);
		if ($PD=='' || $PD=='-' || $PD=='NA' || $PD=='N/A' || $PD=='TBA'){
			$mYqry = "Update Outgoing_Shipment set [Customer] = '".$this->PostVars['slcCustomers']."',[MAWB] = '".$this->PostVars['txtMAWB']."',[HAWB] = '".$this->PostVars['txtHAWB']."',[Flight] = '".$this->PostVars['txtFlight']."',[DepDate] = '".$this->PostVars['txtDepDate']."',[ShipDate] = '".$this->PostVars['txtShipDate']."',[Destination] = '".$this->PostVars['slcDestination']."',[Forwarders] = '".$this->PostVars['slcForwarders']."',[CustInvNo] = '".$this->PostVars['txtCustInvNo']."',[PermitNo] = '".$this->PostVars['txtPermitNo']."',[CustInvValue] = ".$this->PostVars['txtCustInvValue'].",[ExchangeRate] = ".$this->PostVars['txtExchangeRate'].",[USDUnitPrice] = ".$this->PostVars['txtUSDUnitPrice'].",[JobType] = '".$this->PostVars['slcJobType']."',[InvoiceValueSGD] = ".$this->PostVars['txtInvoiceValueSGD'].",[Remarks] = '".$this->PostVars['txtRemarks']."',[PermitDate] = NULL where OutShipReference = ".$this->PostVars['txtShipmentNumber'];
		}else{
			$mYqry = "Update Outgoing_Shipment set [Customer] = '".$this->PostVars['slcCustomers']."',[MAWB] = '".$this->PostVars['txtMAWB']."',[HAWB] = '".$this->PostVars['txtHAWB']."',[Flight] = '".$this->PostVars['txtFlight']."',[DepDate] = '".$this->PostVars['txtDepDate']."',[ShipDate] = '".$this->PostVars['txtShipDate']."',[Destination] = '".$this->PostVars['slcDestination']."',[Forwarders] = '".$this->PostVars['slcForwarders']."',[CustInvNo] = '".$this->PostVars['txtCustInvNo']."',[PermitNo] = '".$this->PostVars['txtPermitNo']."',[CustInvValue] = ".$this->PostVars['txtCustInvValue'].",[ExchangeRate] = ".$this->PostVars['txtExchangeRate'].",[USDUnitPrice] = ".$this->PostVars['txtUSDUnitPrice'].",[JobType] = '".$this->PostVars['slcJobType']."',[InvoiceValueSGD] = ".$this->PostVars['txtInvoiceValueSGD'].",[Remarks] = '".$this->PostVars['txtRemarks']."',[PermitDate] = '".$this->PostVars['txtPermitDate']."' where OutShipReference = ".$this->PostVars['txtShipmentNumber'];
		}
		
		if (mssql_query($mYqry)){
			
			$result = $this->UpdateSTGMS('update',$this->PostVars['txtShipmentNumber']);	
			if ($result[0]!='success'){
				mssql_query("ROLLBACK");
				array_push($data,$result[1],$result[2]);
			}else{
				mssql_query("COMMIT");
				array_push($data,'successUPDATE');
			}

		} else {
			mssql_query("ROLLBACK");
			array_push($data,'errorUpdate');
		}
		
		return json_encode($data);
	}
	
	function UpdateSTGMS($request,$OutShipReference){
		$tempDN = explode("/",$this->PostVars['txtCustInvNo']);
		$errorCtr = 0;
		
		if ($request=='add' || $request=='update'){
		
			if (count($tempDN)){
			
				mssql_query("BEGIN TRAN");
				
				$sqlMaster = mssql_query("select STGIndex,STG,DBName from MasterData.dbo.WawiCustomer where MasterData.dbo.WawiCustomer.WawiIndex='".$this->PostVars['slcCustomers']."'; ");
				
				if (mssql_num_rows($sqlMaster)){
					$CustomerInfoArr = mssql_fetch_assoc($sqlMaster);
					$STGIndex = $CustomerInfoArr['STGIndex'];
					$STG = $CustomerInfoArr['STG'];
					$DBName = $CustomerInfoArr['DBName'];
					
					if ($STG==1){
						
						for ($i=0; $i<count($tempDN); $i++){
							
							$wmsQry = mssql_query("SELECT * FROM ".$DBName.".dbo.[Order] WHERE CustomerReference='".$tempDN[$i]."'; ");
							$wmsRows = mssql_num_rows($wmsQry);
							
							if ($wmsRows>0){
								
								$query = mssql_query("SELECT * FROM STGMM.dbo.PermitInfo WHERE permit_client_name='".$STGIndex."' AND permit_dn_number='".$tempDN[$i]."';");
								$rows = mssql_num_rows($query);
								if ($rows>0){									
								
									$PD = trim($this->PostVars['txtPermitDate']);
									if ($PD=='' || $PD=='-' || $PD=='NA' || $PD=='N/A' || $PD=='TBA'){
										$mYqry = "Update STGMM.dbo.PermitInfo
										set permit_permit_number='".$this->PostVars['txtPermitNo']."',
										permit_date=NULL,
										deleted=0,
										permit_depdate='".$this->PostVars['txtDepDate']."', datemodified=convert(varchar, getdate(), 120),
										permit_hawb='".$this->PostVars['txtHAWB']."', [user_id]='".$this->PostVars['UserID']."'
										WHERE permit_client_name='".$STGIndex."' AND permit_dn_number='".$tempDN[$i]."';";
									}else{
										$mYqry = "Update STGMM.dbo.PermitInfo
										set permit_permit_number='".$this->PostVars['txtPermitNo']."',
										permit_date='".$this->PostVars['txtPermitDate']."',
										deleted=0,
										permit_depdate='".$this->PostVars['txtDepDate']."', datemodified=convert(varchar, getdate(), 120),
										permit_hawb='".$this->PostVars['txtHAWB']."', [user_id]='".$this->PostVars['UserID']."'
										WHERE permit_client_name='".$STGIndex."' AND permit_dn_number='".$tempDN[$i]."';";
									}
								
									$qryUpdate=mssql_query($mYqry);
									if (!$qryUpdate){
										mssql_query("ROLLBACK");
										return array('failed','errSTGMSUpdate','');
										break;
									}
									mssql_free_result($qryUpdate);
								}else{
									$PD = trim($this->PostVars['txtPermitDate']);
									if ($PD=='' || $PD=='-' || $PD=='NA' || $PD=='N/A' || $PD=='TBA'){
										$mYqry = "Insert into STGMM.dbo.PermitInfo (permit_client_name,permit_dn_number,permit_permit_number,permit_depdate,permit_entry_date,permit_hawb,[user_id],OutShipReference)
										values ('".$STGIndex."','".$tempDN[$i]."','".$this->PostVars['txtPermitNo']."','".$this->PostVars['txtDepDate']."',convert(varchar, getdate(), 120),'".$this->PostVars['txtHAWB']."','".$this->PostVars['UserID']."', ".$OutShipReference."); ";
									}else{
										$mYqry = "Insert into STGMM.dbo.PermitInfo (permit_client_name,permit_dn_number,permit_permit_number,permit_depdate,permit_entry_date,permit_hawb,[user_id],OutShipReference,permit_date)
										values ('".$STGIndex."','".$tempDN[$i]."','".$this->PostVars['txtPermitNo']."','".$this->PostVars['txtDepDate']."',convert(varchar, getdate(), 120),'".$this->PostVars['txtHAWB']."','".$this->PostVars['UserID']."', ".$OutShipReference.",'".$this->PostVars['txtPermitDate']."'); ";
									}
									
									$qryInsert=mssql_query($mYqry);
									mssql_free_result($qryInsert);
									if (!$qryInsert){
										mssql_query("ROLLBACK");
										return array('failed','errSTGMSInsert','');
										break;
									}
								}

								mssql_free_result($query);
								unset($rows);
								
							}else{
								mssql_query("ROLLBACK");
								return array('failed','errDNnotExist',$tempDN[$i]);
								break;
							}
							mssql_free_result($wmsQry);
							unset($wmsRows);
						}
						
						$dnImplode = implode("','",$tempDN);
						$qryDel = mssql_query("UPDATE STGMM.dbo.PermitInfo set deleted=1 where permit_client_name='".$STGIndex."'
						and permit_dn_number not in ('".$dnImplode."') and OutShipReference=".$OutShipReference."; ");
						if (!$qryDel){
							mssql_query("ROLLBACK");
							return array('failed','errDeletePermit','');
						}else{						
							mssql_query("COMMIT");
							return array('success','','');
						}
					}else{
						mssql_query("COMMIT");
						return array('success','','');
					}
				}else{
					mssql_query("ROLLBACK");
					return array('failed','errMasterData','');
				}
			}else{
				return array('failed','errCustInvNo','');
			}
		}else{
			return array('failed','errInvalidRequest','');
		}
		
	}
	
	function AddNewGST(){
		mssql_select_db("Import");
		$sqlNewShipRec = mssql_fetch_assoc(mssql_query("SELECT Value FROM Setup WHERE Entry='OutShipCounter'"));
		$ShipReference = $sqlNewShipRec['Value'] + 1;
		$data = array($ShipReference);
		
		mssql_query("BEGIN TRAN");
		mssql_query("Update Setup set Value = Value + 1 WHERE Entry='OutShipCounter'");
		unset($sqlNewShipRec);
		
		$PD = trim($this->PostVars['txtPermitDate']);
		if ($PD=='' || $PD=='-' || $PD=='NA' || $PD=='N/A' || $PD=='TBA'){
			$mYqry = "Insert into Outgoing_Shipment ([OutShipReference],[Customer],[MAWB],[HAWB],[Flight],[DepDate],[ShipDate],[Destination],[Forwarders],[CustInvNo],[PermitNo],[CustInvValue],[ExchangeRate],[USDUnitPrice],[JobType],[InvoiceValueSGD],[Remarks]) values ($ShipReference,'".$this->PostVars['slcCustomers']."','".$this->PostVars['txtMAWB']."','".$this->PostVars['txtHAWB']."','".$this->PostVars['txtFlight']."','".$this->PostVars['txtDepDate']."','".$this->PostVars['txtShipDate']."','".$this->PostVars['slcDestination']."','".$this->PostVars['slcForwarders']."','".$this->PostVars['txtCustInvNo']."','".$this->PostVars['txtPermitNo']."',".$this->PostVars['txtCustInvValue'].",".$this->PostVars['txtExchangeRate'].",".$this->PostVars['txtUSDUnitPrice'].",'".$this->PostVars['slcJobType']."',".$this->PostVars['txtInvoiceValueSGD'].",'".$this->PostVars['txtRemarks']."'); ";
		}else{
			$mYqry = "Insert into Outgoing_Shipment ([OutShipReference],[Customer],[MAWB],[HAWB],[Flight],[DepDate],[ShipDate],[Destination],[Forwarders],[CustInvNo],[PermitNo],[CustInvValue],[ExchangeRate],[USDUnitPrice],[JobType],[InvoiceValueSGD],[Remarks],[PermitDate]) values ($ShipReference,'".$this->PostVars['slcCustomers']."','".$this->PostVars['txtMAWB']."','".$this->PostVars['txtHAWB']."','".$this->PostVars['txtFlight']."','".$this->PostVars['txtDepDate']."','".$this->PostVars['txtShipDate']."','".$this->PostVars['slcDestination']."','".$this->PostVars['slcForwarders']."','".$this->PostVars['txtCustInvNo']."','".$this->PostVars['txtPermitNo']."',".$this->PostVars['txtCustInvValue'].",".$this->PostVars['txtExchangeRate'].",".$this->PostVars['txtUSDUnitPrice'].",'".$this->PostVars['slcJobType']."',".$this->PostVars['txtInvoiceValueSGD'].",'".$this->PostVars['txtRemarks']."','".$this->PostVars['txtPermitDate']."'); ";
		}
		
		if (mssql_query($mYqry)){
			
			if (trim($this->PostVars['txtCustInvNo'])!=''){
				$result = $this->UpdateSTGMS('add',$ShipReference);
				
				if ($result[0]!='success'){
					mssql_query("ROLLBACK");
					array_push($data,$result[1],$result[2]);
				}else{
					mssql_query("COMMIT");
					array_push($data,'successADD');
				}
			}else{
				mssql_query("ROLLBACK");
				array_push($data,'erroradd');
			}
			
		} else {
			mssql_query("ROLLBACK");
			array_push($data,'erroradd');
		}

		return json_encode($data);
	}
	
	function AddNewADIGST(){
		mssql_select_db("Import");
		
		if($this->PostVars['slcCustomers'] == 'ADISGWMS'){
			$this->PostVars['slcCustomers'] = 'ADIDGTWMS';
		}
		
		$Customer			= trim($this->PostVars['slcCustomers']);
		$ShipmentNo			= trim($this->PostVars['txtShipmentNumber']);
		$PermitNo			= trim($this->PostVars['adiPermitNo']);
		$PermitDate			= trim($this->PostVars['adiPermitDate']);
		$JobType			= trim($this->PostVars['adiJobType']);
		$Comment			= trim($this->PostVars['adiRemarks']);
		$CustInvValue		= trim($this->PostVars['adiCustInvValue']);
		$ExchangeRate		= trim($this->PostVars['adiExchangeRate']);
		$InvoiceValueSGD	= trim($this->PostVars['adiInvoiceValueSGD']);
		$GSTAmountValue		= trim($this->PostVars['adiGSTAmount']);
		$EnterBy			= trim($this->PostVars['UserID']);
		$EnterDate			= date('Y-m-d H:i:s');
		
		$data = array($ShipmentNo);
		
		$sqlMaster = mssql_query("select TOP 1 STGIndex,STG,DBName from MasterData.dbo.WawiCustomer where MasterData.dbo.WawiCustomer.WawiIndex='".$this->PostVars['slcCustomers']."'; ");
		if (mssql_num_rows($sqlMaster)){
			$CustomerInfoArr = mssql_fetch_assoc($sqlMaster);
			$DBName = $CustomerInfoArr['DBName'];
		
			if ($PermitNo != ''){
			
				mssql_query("BEGIN TRAN");
			
				$mYqry = "Insert into Outgoing_ADIShipment ([Customer],[ShipmentNo],[PermitNo],[PermitDate],[JobType],[Comment],[EnterBy],[EnterDate],[CustInvValue],[ExchangeRate],[InvoiceValueSGD],[GSTAmountValue])
						  values ('".$Customer."','".$ShipmentNo."','".$PermitNo."','".$PermitDate."','".$JobType."','".$Comment."','".$EnterBy."','".$EnterDate."','".$CustInvValue."','".$ExchangeRate."','".$InvoiceValueSGD."','".$GSTAmountValue."');";
				
				if (mssql_query($mYqry)){
					
					$mYupdateqry = "UPDATE ".$DBName.".dbo.SAPDailyShipment SET [GSTProcessed] = 1, [GSTProcessedBy] = '".$EnterBy."', [GSTProcessedDate] = '".$EnterDate."' WHERE [ShipmentNumber] = '".$ShipmentNo."'";
					
					if(mssql_query($mYupdateqry)){
						mssql_query("COMMIT");
						array_push($data,'successADD'); 
					} else {
						mssql_query("ROLLBACK");
						array_push($data,'erroradd');
					}
					
				} else {
					mssql_query("ROLLBACK");
					array_push($data,'erroradd');
				}
			
			}else{
				array_push($data,'erroradd');
			}
		
		} else {
			array_push($data,'erroradd');
		}

		return json_encode($data);
	}
	
	function UpdateADIGST(){
		mssql_select_db("Import");
		
		if($this->PostVars['slcCustomers'] == 'ADISGWMS'){
		
			$Customer			= trim($this->PostVars['slcCustomers']);
			$ShipmentNo			= trim($this->PostVars['txtShipmentNumber']);
			$PermitNo			= trim($this->PostVars['adiPermitNo']);
			$PermitDate			= trim($this->PostVars['adiPermitDate']);
			$JobType			= trim($this->PostVars['adiJobType']);
			$Comment			= trim($this->PostVars['adiRemarks']);
			$CustInvValue		= trim($this->PostVars['adiCustInvValue']);
			$ExchangeRate		= trim($this->PostVars['adiExchangeRate']);
			$InvoiceValueSGD	= trim($this->PostVars['adiInvoiceValueSGD']);
			$GSTAmountValue		= trim($this->PostVars['adiGSTAmount']);
			$EnterBy			= trim($this->PostVars['UserID']);
			$EnterDate			= date('Y-m-d H:i:s');
			
			$data = array($ShipmentNo);
			
			if ($PermitNo != ''){
				
					$mYupdateqry = "UPDATE Outgoing_ADIShipment SET [PermitNo] = '".$PermitNo."', [PermitDate] = '".$PermitDate."', [JobType] = '".$JobType."', [Comment] = '".$Comment."', [EditBy] = '".$EnterBy."', [EditDate] = '".$EnterDate."', [CustInvValue] = '".$CustInvValue."', [ExchangeRate] = '".$ExchangeRate."', [InvoiceValueSGD] = '".$InvoiceValueSGD."', [GSTAmountValue] = '".$GSTAmountValue."' WHERE [ShipmentNo] = '".$ShipmentNo."'";
					
					if(mssql_query($mYupdateqry)){
						array_push($data,'successUPDATE'); 
					} else {
						array_push($data,'erroradd');
					}
			
			}else{
				array_push($data,'erroradd');
			}
		
		} else {
			array_push($data,'erroradd');
		}

		return json_encode($data);
	}
	
	function GetQuantityCustomer(){
		mssql_select_db("MasterData");
		$sql = mssql_fetch_assoc(mssql_query("select DBName from WawiCustomer where [WawiIndex] = '".$this->PostVars['WawiIndex']."'"));
			
		return $sql['DBName'];
	}
	
	function UpdateReference(){

		mssql_select_db("Import");
		$sqlShipment = mssql_fetch_assoc(mssql_query("select Customer from Outgoing_Shipment where OutShipReference=".$this->PostVars['ShipReference']));
		$Customer = $sqlShipment['Customer'];
		
		$ListRefToUpdate = $this->PostVars['ListRef'];
		foreach($ListRefToUpdate as $RefNum){
			$RefArray = explode("-",$RefNum);
			mssql_query("Update Export set OutShipReference=".$this->PostVars['ShipReference'].",InvoiceValue=".$RefArray[1].",Qty=".$RefArray[3].",InvoiceValueSGD=".$RefArray[4]." where Reference=".$RefArray[0]." and TaxScheme='".$RefArray[2]."' and wawiindex = '".$Customer."'");
		}
		
		$data = array($this->PostVars['ShipReference'],$this->PostVars['action']);

		return json_encode($data);
	}
	
	function UpdateReferenceNull(){
		mssql_select_db('Import');
		mssql_query("Update Export set OutShipReference=NULL,Qty=NULL,InvoiceValueSGD=NULL,InvoiceValue=NULL where Reference= ".$this->PostVars['RefNum']);
	}
}

?>
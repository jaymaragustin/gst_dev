<?php 
//error_reporting(E_ALL);
//ini_set("display_errors",1);

//add arrival date on the report
session_start();
require ('../../include/referer.checker.php');
require ('../../include/session.checker.php');

#style="border:1px solid #EEEEEE;"

?>

<!--<link rel="stylesheet" href="css/bootstrap.css">-->
<script type='text/javascript' src='js/jquery.dataTables.columnFilter.js'></script>

<style>
	.alignme{
		text-align: center;
		white-space: nowrap;
	}
	.alignmewrap{
		text-align: center;
		word-wrap: break-word !important;
		white-space: normal !important;
	}
	table.display td{
		white-space: normal !important;
		word-wrap: break-word !important;
	}
	span.totalNos{ color: #CE2929; font-size: 12px; font-weight: bold;}
	.ui-tabs .ui-tabs-nav li a {
		font-size: 11px !important;
	}
</style>

<h2><?php include '../../tpl/module_shortcut.php';?>ADI GST Report</h2> 
<script src="modules/gstADIReport/jquery.fileDownload.js">
</script>

<form onsubmit="return false;" class="globalform">
<ol>
	<div style="float:left;">
		<li style="display: none;">
			<label style="width:100px;">Start Date</label>
			<input type="text" class="text" value="" readonly name="startDate" id="startDate" />
		</li>
		<li style="display: none;">
			<label style="width:100px;">End Date</label>
			<input type="text" class="text" value="" readonly name="endDate" id="endDate" />
		</li>
		<li style="display: none;">
			<label style="width:100px;">&nbsp;</label>
		</li>
		<table id="reportSelection">
			<tbody>
				<tr>
					<td width="15px"><input type="checkbox" class="chkallReport" id="chkall" name="searchReport" value="all" style="margin: 5px !important;" /></td>
					<td width="50px">All</td>
					<td width="15px"><input type="checkbox" class="chkoneReport" id="chkIncomingDetailed" name="searchReport" value="IncomingDetailed" style="margin: 5px !important;"/></td>
					<td width="180px">Incoming GST Detailed</td>
					<td width="15px"><input type="checkbox" class="chkoneReport" id="chkOutgoingDetailed" name="searchReport" value="OutgoingDetailed" style="margin: 5px !important;"/></td>
					<td width="180px">Outgoing GST Detailed</td>
					<td width="15px"><input type="checkbox" class="chkoneReport" id="chkShipmentPicklist" name="searchReport" value="ShipmentPicklist" style="margin: 5px !important;"/></td>
					<td width="180px">Shipment Picklist</td>
					<td width="15px"></td>
					<td width="180px"><input type="button" class="button" style="height: 30px !important; width: 120px;" value="Export to Excel" id="exportToExcel" /></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td><input type="checkbox" class="chkoneReport" id="chkIncomingSummary" name="searchReport" value="IncomingSummary" style="margin: 5px !important;"/></td>
					<td>Incoming GST Summary</td>
					<td><input type="checkbox" class="chkoneReport" id="chkOutgoingSummary" name="searchReport" value="OutgoingSummary" style="margin: 5px !important;"/></td>
					<td>Outgoing GST Summary</td>
					<td><input type="checkbox" class="chkoneReport" id="chkShipmentSummary" name="searchReport" value="ShipmentSummary" style="margin: 5px !important;"/></td>
					<td>Shipment Summary</td>
				</tr>
			</tbody>
		</table>
		<table>
			<tr>
				<td width=""><label style="">Column</label></td>
				<td width="" style="padding-left: 15px !important;"><span></td>
				<td width="" style="padding-left: 15px !important;"><label style="">Filter</label></td>
				<td width="" style="padding-left: 15px !important;"></td>
			</tr>
			<tr>
				<td>
					<select class="select" id="searchColumn" name="searchColumn">
						<option value="VendorInvNo" class="thisText">Vendor Inv No.</option>
						<option value="TaxScheme" class="thisText">Tax Reference</option>
						<option value="HAWB" class="thisText">HAWB</option>
						<option value="Date" class="thisDate">Date</option>
						<option value="Permit" class="thisText">Permit</option>
						<option value="PartNoDesc" class="thisText">Description/Part no.</option>
						<option value="ShipmentNo" class="thisText">Shipment #</option>
						<option value="SUNumber" class="thisText">SU/Entry#</option>
					</select>
				</td>
				<td style="padding-left: 15px !important;">
					<select class="select" id="searchCon" name="searchCon">
						<option value="equalto">Equal To</option>
						<option value="notequalto">Not Equal To</option>
						<option value="contains">Contains</option>
						<!--<option value="greaterthan">Greater Than</option>
						<option value="lessthan">Less Than</option>-->
						<option value="between">Between</option>
					</select>
				</td>
				<td style="padding-left: 15px !important;">
					<input type="text" class="text" style="height: 18px !important; display: none; margin-right: 20px !important;" id="searchmainValue" name="searchmainValue" placeholder="Search Value"/>
					<input type="text" class="text" style="height: 18px !important;" id="searchInput1" name="searchInput1"/>
					<label class="filter-between" style="display: none; width: 12px !important; margin-left: 10px !important;">to</label>
					<input type="text" class="text filter-between" style="display: none; height: 18px !important; margin-left: 10px !important;" id="searchInput2" name="searchInput2"/>
				</td>
				<td style="padding-left: 25px !important;">
					<input type="button" class="button" style="height: 30px !important; width: 100px;" value="Search" id="btnSearch" />
				</td>
			</tr>
		</table>
		
	</div>
	<div style="float:left; margin-left: 15px;">
		<li style="display:none;">
			<label style="width:100px;">Tax Reference</label>
			<select class="select" id="slcReference" name="slcReference"></select>
			<span><input type="checkbox" id="checkAll"/>all</span>
		</li>
	</div>
</ol>
</form>
<div class="clr"></div>
<br/>
<div id="DivTabs">
	<div id="tabs">
	  <ul>
	      <li><a href="#tabs-1">Incoming GST Detailed</a></li>
	      <li><a href="#tabs-2">Incoming GST Summary</a></li>
	      <li><a href="#tabs-3">Outgoing GST Detailed</a></li>
	      <li><a href="#tabs-4">Outgoing GST Summary</a></li>
	      <li><a href="#tabs-5">Shipment Picklist</a></li>
	      <li><a href="#tabs-6">Shipment Summary</a></li>
	  </ul>
	  <div id="tabs-1">
		<!--<input type="text" class="text" id="txtFilterThis" placeholder="Reference #" style="width:100px;">-->
	     <table class="display" id="tblIncomingDetailed">
			<thead></thead>
			<tbody></tbody>
			<tfoot>
				<tr>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
				</tr>
			</tfoot>
		</table>
	  </div>
	  <div id="tabs-2">
	     <table class="display" id="tblIncomingSummary">
			<thead></thead>
			<tbody></tbody>
			<tfoot>
				<tr>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
					<td align="center"></th>
				</tr>
			</tfoot>
		</table>
	  </div>
	  <div id="tabs-3">
			<table class="display" id="tblOutgoingDetailed">
				<thead></thead>
				<tbody></tbody>
				<tfoot>
					<tr>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
					</tr>
				</tfoot>
			</table>	
	  </div>
	  <div id="tabs-4">
			<table class="display" id="tblOutgoingSummary">
				<thead></thead>
				<tbody></tbody>
				<tfoot>
					<tr>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
					</tr>
				</tfoot>
			</table>
	  </div>
	  <div id="tabs-5">
			<table class="display" id="tblPicklist">
				<thead></thead>
				<tbody></tbody>
				<tfoot>
					<tr>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
					</tr>
				</tfoot>
			</table>
	  </div>
	  <div id="tabs-6">
			<input type="text" class="text" id="txtSReference" placeholder="Reference #" style="width:100px; display:none;">
			<input type="text" class="text" id="txtSEntry" placeholder="SU/Entry Number" style="width:100px; display:none;">
			<input type="text" class="text" id="txtSTaxScheme" placeholder="Tax Scheme" style="width:100px; display:none;">
			<input type="text" class="text" id="txtSIncoming" placeholder="Incoming" style="width:100px; display:none;">
			<input type="text" class="text" id="txtSOutgoing" placeholder="Outgoing" style="width:100px; display:none;">
			<input type="text" class="text" id="txtSBalance" placeholder="Balance" style="width:100px; display:none;">
			<table class="display" id="tblShipmentSummary">
				<thead></thead>
				<tbody></tbody>
				<tfoot>
					<tr>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
						<td align="center"></th>
					</tr>
				</tfoot>
			</table>
	  </div>
	</div>
</div>
<script>
ErrorKeys = new Object();
ErrorKeys['First'] = 'Beginning of the data';
ErrorKeys['Previous'] = 'Beginning of the data';
ErrorKeys['Last'] = 'Last data';
ErrorKeys['Next'] = 'Last data';
ErrorKeys['Search'] = 'Sorry invalid Shipment Number';
ErrorKeys['nodata'] = 'Sorry no available Reference Number';
ErrorKeys['ErrorSearchingShipment'] = 'Shipment Number is a required field in order to Search.';

var TaxReferenceList = new Object(); // get from masterdata - TaxScheme
var CustomerList = new Object();
var CurrentDate = new Date();

var tblIncomingDetailed;
var tblIncomingSummary;
var tblOutgoingDetailed;
var tblOutgoingSummary;
var tblPicklist;
var tblShipmentSummary;

var isGenerateIncomingDetailed = false;
var isGenerateIncomingSummary = false;
var isGenerateOutgoingDetailed = false;
var isGenerateOutgoingSummary = false;
var isGenerateShipmentPicklist = false;
var isGenerateShipmentSummary = false;

var ajaxReportError = false;
var isDimStart = false;

$(function() {
	$("input:submit, input:button, button, .button").button();
	//$('#DivTabs').hide();
	
	$( "#startDate" ).datepicker({		
			dateFormat: 'mm/dd/yy',
	    defaultDate: "+1w",
	    changeMonth: true,
	    onClose: function( selectedDate ) {
	        $( "#endDate" ).datepicker( "option", "minDate", selectedDate );
	    }
	});
	
	$( "#endDate" ).datepicker({
		dateFormat: 'mm/dd/yy',
	    defaultDate: "+1w",
	    changeMonth: true,
	    onClose: function( selectedDate ) {
	        $( "#startDate" ).datepicker( "option", "maxDate", selectedDate );
	    }
	});
	
	$('.chkallReport').on('change', function () {
		$('.chkoneReport').prop('checked', this.checked);
	});

	$('.chkoneReport').on('change', function(){
		
		var chkoneReportCtr = $('.chkoneReport').length;
		var checkedCtr = 0;
		
		$('.chkoneReport').each(function() {
			if(this.checked){
				checkedCtr++;
			}
		});
		
		if(checkedCtr == 0){
			$('.chkallReport').prop({
				indeterminate: false,
				checked: false
			});
		} else if(checkedCtr > 0 && checkedCtr < chkoneReportCtr){
			$('.chkallReport').prop({
				indeterminate: true,
				checked: false
			});
		} else if(checkedCtr == chkoneReportCtr){
			$('.chkallReport').prop({
				indeterminate: false,
				checked: true
			});
		}
		
	});

	tblIncomingDetailed = $('#tblIncomingDetailed').dataTable({
			"sDom": '<"H">Tlfr<"F"tip>',
			"oTableTools": {
				"aButtons": [
					{
						"sExtends":    "collection",
						"sButtonText": "Save As",
						"aButtons":    [ "xls" ]
					}
				]
			},
			//"bDeferRender": true,
			//"bAutoWidth": false,  
			//'aaSortingFixed': [[3,'asc'],[2,'asc']],
			'bFilter': true,
			'bSortable' : false,
			'bPaginate': true,
			'bJQueryUI': true,
			'aLengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
			'sPaginationType': 'full_numbers',
			"bScrollCollapse": true,  
			"sScrollX":"100%",    
			"bDestroy": true,
			"aoColumns": [
				{"sTitle":"S/No","sWidth":"80px","sClass":"alignme"},
				{"sTitle":"Customer","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Shipment #","sWidth":"90px","sClass":"alignme"},
				{"sTitle":"Shpmt Rcvd Date","sWidth":"130px","sClass":"alignme"},
				{"sTitle":"VendorInvNo.","sWidth":"350px","sClass":"alignmewrap"},
				{"sTitle":"Total Qty","sWidth":"90px","sClass":"alignme"},
				{"sTitle":"Total Qty(HAWB)","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Tax Reference","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Permit No.","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Exchange Rate","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Vendor Inv Value (USD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Vendor Inv Value (SGD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"FrtCharges (SGD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"InsCharges (SGD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"CIF Amount (SGD)","sWidth":"90px","sClass":"alignme"},
				{"sTitle":"GST Amount (SGD)","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Origin Port","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Forwarders","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Vendor /Supplier","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"HAWB No.","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"MAWB No.","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"TptMode","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Arrival Date","sWidth":"110px","sClass":"alignme"},
				{"sTitle":"Delivery To","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"IncoTerms","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"JobType","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Comment","sWidth":"350px","sClass":"alignmewrap"},
				{"sTitle":"Incoming Reference","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Description","sWidth":"500px","sClass":"alignmewrap"},
				{"sTitle":"SU/Entry#.","sWidth":"500px","sClass":"alignmewrap"},
				{"sTitle":"Consignment order (Y/N)","sWidth":"150px","sClass":"alignme"},
				{"sTitle":"Previous SU# for consignment","sWidth":"250px","sClass":"alignme"}
			],
			fnDrawCallback: function( oSettings ) {
				$('#tblIncomingDetailed tr td').css('white-space','normal');
			},
			fnFooterCallback: function ( nRow, aaData, iStart, iEnd, aiDisplay ) {
				var iTotalQty = 0;
				var iTotalShipQty = 0;
				var iVendorValUSD = 0;
				var iVendorValSGD = 0;
				var iFrtCharges = 0;
				var iInsCharges = 0;
				var iCIFAmount = 0;
				var iGSTAmount = 0;
				
				$.each(aaData,function(ikey,ivalue){
					iTotalQty		+= ivalue[5] != 0 ? parseInt(ivalue[5]) : 0;
					iTotalShipQty	+= ivalue[6] != 0 ? parseInt(ivalue[6]) : 0;
					iVendorValUSD	+= ivalue[10] != 0 ? parseFloat(ivalue[10]) : 0;
					iVendorValSGD	+= ivalue[11] != 0 ? parseFloat(ivalue[11]) : 0;
					iFrtCharges		+= ivalue[12] != 0 ? parseFloat(ivalue[12]) : 0;
					iInsCharges		+= ivalue[13] != 0 ? parseFloat(ivalue[13]) : 0;
					iCIFAmount		+= ivalue[14] != 0 ? parseFloat(ivalue[14]) : 0;
					iGSTAmount		+= ivalue[15] != 0 ? parseFloat(ivalue[15]) : 0;
				});
				
				var nCells = nRow.getElementsByTagName('td');
				nCells[5].innerHTML  = iTotalQty;
				nCells[6].innerHTML  = iTotalShipQty;
				nCells[10].innerHTML = parseFloat(iVendorValUSD).toFixed(2);
				nCells[11].innerHTML = parseFloat(iVendorValSGD).toFixed(2);
				nCells[12].innerHTML = parseFloat(iFrtCharges).toFixed(2);
				nCells[13].innerHTML = parseFloat(iInsCharges).toFixed(2);
				nCells[14].innerHTML = parseFloat(iCIFAmount).toFixed(2);
				nCells[15].innerHTML = parseFloat(iGSTAmount).toFixed(2);			
			}
		})
		
		//$(".tblIncomingDetailed_header").find('.filter_column').find("input").each(function() {
		//	
		//	var thisTextbox = $.trim($(this).val());
		//	
		//	if(thisTextbox == 'VendorInvNo.' || thisTextbox == 'Comment' || thisTextbox == 'Description' || thisTextbox == 'SU/Entry#.'){
		//		$(this).css("width", "350px");
		//	}
		//	
		//	if(thisTextbox == 'Previous SU# for consignment'){
		//		$(this).css("width", "250px");
		//	}
		//	
		//});
		
	tblIncomingSummary = $('#tblIncomingSummary').dataTable({
			"sDom": '<"H">Tlfr<"F"tip>',
			"oTableTools": {
				"aButtons": [
					{
						"sExtends":    "collection",
						"sButtonText": "Save As",
						"aButtons":    [ "xls" ]
					}
				]
			},
			'bFilter': true,
			'bPaginate': true,
			'bJQueryUI': true,
			'aLengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
			'sPaginationType': 'full_numbers',
			"bScrollCollapse": true,  
			"sScrollX":"100%",    
			"bDestroy": true,
			"aoColumns": [
				{"sTitle":"S/No","sWidth":"80px","sClass":"alignme"},
				{"sTitle":"Customer","sWidth":"110px","sClass":"alignme"},
				{"sTitle":"Shipment #:","sWidth":"110px","sClass":"alignme"},
				{"sTitle":"Shpmt Rcvd Date","sWidth":"130px","sClass":"alignme"},
				{"sTitle":"Vendor/Supplier","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Vendor Inv No.","sWidth":"350px","sClass":"alignmewrap"},
				{"sTitle":"Tax Reference","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Permit No.","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Total Qty","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Total Qty(HAWB)","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Vendor Inv Value (USD)","sWidth":"160px","sClass":"alignme"},
				{"sTitle":"Vendor Inv Value (SGD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Frt Charges (SGD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Ins Charges (SGD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"CIF Amount (SGD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"GST Amount (SGD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Job Type","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"HAWB No.","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"MAWB No.","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Dep Date","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Comment","sWidth":"350px","sClass":"alignmewrap"},
				{"sTitle":"Description","sWidth":"500px","sClass":"alignmewrap"},
				{"sTitle":"SU/Entry#.","sWidth":"500px","sClass":"alignmewrap"},
				{"sTitle":"Consignment order (Y/N)","sWidth":"150px","sClass":"alignme"},
				{"sTitle":"Previous SU# for consignment","sWidth":"200px","sClass":"alignme"}
			],
			fnDrawCallback: function( oSettings ) {
				$('#tblIncomingSummary tr td').css('white-space','normal');
			},
			fnFooterCallback: function ( nRow, aaData, iStart, iEnd, aiDisplay ) {
				var iTotalQty = 0;
				var iTotalShipQty = 0;
				var iVendorValUSD = 0;
				var iVendorValSGD = 0;
				var iFrtCharges = 0;
				var iInsCharges = 0;
				var iCIFAmount = 0;
				var iGSTAmount = 0;
				
				$.each(aaData,function(ikey,ivalue){
					iTotalQty		+= ivalue[8] != 0 ? parseInt(ivalue[8]) : 0;
					iTotalShipQty	+= ivalue[9] != 0 ? parseInt(ivalue[9]) : 0;
					iVendorValUSD	+= ivalue[10] != 0 ? parseFloat(ivalue[10]) : 0;
					iVendorValSGD	+= ivalue[11] != 0 ? parseFloat(ivalue[11]) : 0;
					iFrtCharges		+= ivalue[12] != 0 ? parseFloat(ivalue[12]) : 0;
					iInsCharges		+= ivalue[13] != 0 ? parseFloat(ivalue[13]) : 0;
					iCIFAmount		+= ivalue[14] != 0 ? parseFloat(ivalue[14]) : 0;
					iGSTAmount		+= ivalue[15] != 0 ? parseFloat(ivalue[15]) : 0;
				});
				
				var nCells = nRow.getElementsByTagName('td');
				nCells[8].innerHTML  = iTotalQty;
				nCells[9].innerHTML  = iTotalShipQty;
				nCells[10].innerHTML = parseFloat(iVendorValUSD).toFixed(2);
				nCells[11].innerHTML = parseFloat(iVendorValSGD).toFixed(2);
				nCells[12].innerHTML = parseFloat(iFrtCharges).toFixed(2);
				nCells[13].innerHTML = parseFloat(iInsCharges).toFixed(2);
				nCells[14].innerHTML = parseFloat(iCIFAmount).toFixed(2);
				nCells[15].innerHTML = parseFloat(iGSTAmount).toFixed(2);			
			}
		});

	tblOutgoingDetailed = $('#tblOutgoingDetailed').dataTable({
			"sDom": '<"H">Tlfr<"F"tip>',
			"oTableTools": {
				"aButtons": [
					{
						"sExtends":    "collection",
						"sButtonText": "Save As",
						"aButtons":    [ "xls" ]
					}
				]
			},
			'bFilter': true,
			'bSortable' : false,
			//'aaSortingFixed': [[4,'asc'],[2,'asc']],
			'bPaginate': true,
			'bJQueryUI': true,
			'aLengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
			'sPaginationType': 'full_numbers',
			"bScrollCollapse": true,  
			"sScrollX":"100%",    
			"bDestroy": true,
			"aoColumns": [
				{"sTitle":"S/No","sWidth":"80px","sClass":"alignme"},
				{"sTitle":"Customer","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Shipment #","sWidth":"90px","sClass":"alignme"},
				{"sTitle":"Packlist No #","sWidth":"130px","sClass":"alignme"},
				{"sTitle":"Shipment Release Date.","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Shipment No.","sWidth":"200px","sClass":"alignme"},
				{"sTitle":"Vendor Inv No.","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Vendor/Supplier","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Comm Inv No/DN.","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Total Qty","sWidth":"180px","sClass":"alignme"},
				{"sTitle":"Total Qty(HAWB)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Unit Price","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Out Tax Ref","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Permit #","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Exchange Rate","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Dest Port","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Forwarders","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Consignee","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"HAWB No.","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"MAWB No.","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Tpt Mode","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Dep Date","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Delivery From","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Inco Terms","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Job Type","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Comment","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Customer Inv Value(USD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Customer Inv Value(SGD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"GST Amount(SGD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Total Value(HAWB SGD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"OUT SU #","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Description","sWidth":"140px","sClass":"alignme"}
			],
			fnDrawCallback: function( oSettings ) {
				$('#tblOutgoingDetailed tr td').css('white-space','normal');
			},
			fnFooterCallback: function ( nRow, aaData, iStart, iEnd, aiDisplay ) {
				var iTotalQty = 0;
				var iTotalShipQty = 0;
				var iCustInvValUSD = 0;
				var iCustInvValSGD = 0;
				var iGSTAmount = 0;
				
				$.each(aaData,function(ikey,ivalue){
					iTotalQty		+= ivalue[9] != 0 ? parseInt(ivalue[9]) : 0;
					iTotalShipQty	+= ivalue[10] != 0 ? parseInt(ivalue[10]) : 0;
					//iCustInvValUSD	+= ivalue[26] != 0 ? parseFloat(ivalue[26].replace(/,/g, "")) : 0;
					//iCustInvValSGD	+= ivalue[27] != 0 ? parseFloat(ivalue[27].replace(/,/g, "")) : 0;
					//iGSTAmount		+= ivalue[28] != 0 ? parseFloat(ivalue[28].replace(/,/g, "")) : 0;
					iCustInvValUSD	+= ivalue[26] != 0 ? parseFloat(ivalue[26]) : 0;
					iCustInvValSGD	+= ivalue[27] != 0 ? parseFloat(ivalue[27]) : 0;
					iGSTAmount		+= ivalue[28] != 0 ? parseFloat(ivalue[28]) : 0;
				});
				
				var nCells = nRow.getElementsByTagName('td');
				nCells[9].innerHTML  = iTotalQty;
				nCells[10].innerHTML = iTotalShipQty;
				nCells[26].innerHTML = parseFloat(iCustInvValUSD).toFixed(2);
				nCells[27].innerHTML = parseFloat(iCustInvValSGD).toFixed(2);
				nCells[28].innerHTML = parseFloat(iGSTAmount).toFixed(2);			
			}
		});
		
	tblOutgoingSummary = $('#tblOutgoingSummary').dataTable({
			"sDom": '<"H">Tlfr<"F"tip>',
			"oTableTools": {
				"aButtons": [
					{
						"sExtends":    "collection",
						"sButtonText": "Save As",
						"aButtons":    [ "xls" ]
					}
				]
			},
			'bFilter': true,
			'bPaginate': true,
			'bJQueryUI': true,
			'aLengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
			'sPaginationType': 'full_numbers',
			"bScrollCollapse": true,  
			"sScrollX":"100%",    
			"bDestroy": true,
			"aoColumns": [
				{"sTitle":"S/No","sWidth":"80px","sClass":"alignme"},
				{"sTitle":"Customer","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Shipment #","sWidth":"90px","sClass":"alignme"},
				{"sTitle":"Shipment Release Date.","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Shipment No.","sWidth":"200px","sClass":"alignme"},
				{"sTitle":"Vendor Inv No.","sWidth":"400px","sClass":"alignmewrap"},
				{"sTitle":"Vendor/Supplier","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Comm Inv No/DN.","sWidth":"400px","sClass":"alignmewrap"},
				{"sTitle":"Total Qty","sWidth":"180px","sClass":"alignme"},
				{"sTitle":"Total Qty(HAWB)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Unit Price","sWidth":"400px","sClass":"alignmewrap"},
				{"sTitle":"Out Tax Ref","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Permit #","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Exchange Rate","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Dest Port","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Forwarders","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Consignee","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"HAWB No.","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"MAWB No.","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Tpt Mode","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Dep Date","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Delivery From","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Inco Terms","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Job Type","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Comment","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Customer Inv Value(USD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Customer Inv Value(SGD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"GST Amount(SGD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Total Value(HAWB SGD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"OUT SU #","sWidth":"400px","sClass":"alignmewrap"},
				{"sTitle":"Description","sWidth":"400px","sClass":"alignmewrap"}
			],
			fnDrawCallback: function( oSettings ) {
				$('#tblOutgoingSummary tr td').css('white-space','normal');
			},
			fnFooterCallback: function ( nRow, aaData, iStart, iEnd, aiDisplay ) {
				var iTotalQty = 0;
				var iTotalShipQty = 0;
				var iCustInvValUSD = 0;
				var iCustInvValSGD = 0;
				var iGSTAmount = 0;
				
				$.each(aaData,function(ikey,ivalue){
					iTotalQty		+= ivalue[8] != 0 ? parseInt(ivalue[8]) : 0;
					iTotalShipQty	+= ivalue[9] != 0 ? parseInt(ivalue[9]) : 0;
					//iCustInvValUSD	+= ivalue[25] != 0 ? parseFloat(ivalue[25].replace(/,/g, "")) : 0;
					//iCustInvValSGD	+= ivalue[26] != 0 ? parseFloat(ivalue[26].replace(/,/g, "")) : 0;
					//iGSTAmount		+= ivalue[27] != 0 ? parseFloat(ivalue[27].replace(/,/g, "")) : 0;
					iCustInvValUSD	+= ivalue[25] != 0 ? parseFloat(ivalue[25]) : 0;
					iCustInvValSGD	+= ivalue[26] != 0 ? parseFloat(ivalue[26]) : 0;
					iGSTAmount		+= ivalue[27] != 0 ? parseFloat(ivalue[27]) : 0;
				});
				
				var nCells = nRow.getElementsByTagName('td');
				nCells[8].innerHTML  = iTotalQty;
				nCells[9].innerHTML  = iTotalShipQty;
				nCells[25].innerHTML = parseFloat(iCustInvValUSD).toFixed(2);
				nCells[26].innerHTML = parseFloat(iCustInvValSGD).toFixed(2);
				nCells[27].innerHTML = parseFloat(iGSTAmount).toFixed(2);				
			}
		});

	tblPicklist = $('#tblPicklist').dataTable({
			"sDom": '<"H">Tlfr<"F"tip>',
			"oTableTools": {
				"aButtons": [
					{
						"sExtends":    "collection",
						"sButtonText": "Save As",
						"aButtons":    [ "xls" ]
					}
				]
			},
			'bFilter': true,
			'bSortable' : false,
			//'aaSortingFixed': [[3,'asc'],[2,'asc']],
			'bPaginate': true,
			'bJQueryUI': true,
			'aLengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
			'sPaginationType': 'full_numbers',
			"bScrollCollapse": true,  
			"sScrollX":"100%",    
			"bDestroy": true,
			"aoColumns": [
				{"sTitle":"#","sWidth":"80px","sClass":"alignme"},
				{"sTitle":"Out Reference #","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Shipment Release Date","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Out WMS Reference","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Picklist No.","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"DN No.","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Unit Price (USD)","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Out HAWB No.","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Out Permit No.","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Part No.","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Tax Scheme","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Total Outgoing Qty","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Out Box Entry/SU#","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"In Reference#","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Shpmt Rcvd Date","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"In WMS Reference","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Vendor/Supplier Name","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Vendor Inv No","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"In HAWB No","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"In Permit No.","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Part No.","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Tax Scheme","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Incoming (QTY)","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"In Box Entry/SU#","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Balance (QTY)","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Bal Box Entry/SU#","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Previous SU#","sWidth":"120px","sClass":"alignme"}
			],
			fnDrawCallback: function( oSettings ) {
				$('#tblPicklist tr td').css('white-space','normal');
			},
			fnFooterCallback: function ( nRow, aaData, iStart, iEnd, aiDisplay ) {
				var iTotalOutQty = 0;
				var iTotalInQty = 0;
				var iTotalBalQty = 0;
				
				$.each(aaData,function(ikey,ivalue){
					iTotalOutQty += ivalue[11] != 0 ? (ivalue[11] != null ? parseInt(ivalue[11]):0) : 0;
					iTotalInQty += ivalue[27] != 0 ? (ivalue[27] != null ? parseInt(ivalue[27]):0) : 0;
					iTotalBalQty += ivalue[28] != 0 ? (ivalue[28] != null ? parseInt(ivalue[28]):0) : 0;
				});
				
				var nCells = nRow.getElementsByTagName('td');
				nCells[11].innerHTML  = iTotalOutQty;
				nCells[22].innerHTML  = iTotalInQty;
				nCells[24].innerHTML  = iTotalBalQty;		
			}
		});
		
	tblShipmentSummary = $('#tblShipmentSummary').dataTable({
			"sDom": '<"H">Tlfr<"F"tip>',
			"oTableTools": {
				"aButtons": [
					{
						"sExtends":    "collection",
						"sButtonText": "Save As",
						"aButtons":    [ "xls" ]
					}
				]
			},
			'bFilter': true,
			'bSortable' : false,
			//'aaSortingFixed': [[3,'asc'],[2,'asc']],
			'bPaginate': true,
			'bJQueryUI': true,
			'aLengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
			'sPaginationType': 'full_numbers',
			"bScrollCollapse": true,  
			"sScrollX":"100%",    
			"bDestroy": true,
			"aoColumns": [
				{"sTitle":"#","sWidth":"80px","sClass":"alignme"},
				{"sTitle":"In Reference#","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Shpmt Rcvd Date","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"In WMS Reference","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Vendor / Supplier Name","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Vendor Inv No","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"In HAWB No","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"In Permit No.","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Part No.","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Tax Scheme","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Incoming (QTY)","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"In Box Entry/SU#","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Out Reference #","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Shipment Release Date","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Out WMS Reference","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Picklist No.","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"DN No.","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Unit Price (USD)","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Out HAWB No.","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Out Permit No.","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Part No.","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Tax Scheme","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Total Outgoing Qty","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Out Box Entry/SU#","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Balance (QTY)","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Bal Box Entry/SU#","sWidth":"120px","sClass":"alignme"}
			],
			fnDrawCallback: function( oSettings ) {
				$('#tblShipmentSummary tr td').css('white-space','normal');
			},
			fnFooterCallback: function ( nRow, aaData, iStart, iEnd, aiDisplay ) {
				var iTotalInQty = 0;
				var iTotalOutQty = 0;
				var iTotalBalQty = 0;
				
				$.each(aaData,function(ikey,ivalue){
					iTotalInQty += ivalue[26] != 0 ? (ivalue[26] != null ? parseInt(ivalue[26]):0) : 0;
					iTotalOutQty += ivalue[22] != 0 ? (ivalue[22] != null ? parseInt(ivalue[22]):0) : 0;
					iTotalBalQty += ivalue[27] != 0 ? (ivalue[27] != null ? parseInt(ivalue[27]):0) : 0;
				});
				
				var nCells = nRow.getElementsByTagName('td');
				nCells[10].innerHTML  = iTotalInQty;
				nCells[22].innerHTML  = iTotalOutQty;
				nCells[24].innerHTML  = iTotalBalQty;		
			}
		});

	$("#tabs").tabs({
		select: function(event, ui) {
			var selected = ui.panel.id;
			switch(selected){
				case 'tabs-1':
					var t = setTimeout(function(){$('#tblIncomingDetailed').dataTable().fnAdjustColumnSizing();},1);
				break;				
				case 'tabs-2':
					var t = setTimeout(function(){$('#tblIncomingSummary').dataTable().fnAdjustColumnSizing();},1);
				break;
				case 'tabs-3':
					var t = setTimeout(function(){$('#tblOutgoingDetailed').dataTable().fnAdjustColumnSizing();},1);
				break;
				case 'tabs-4':
					var t = setTimeout(function(){$('#tblOutgoingSummary').dataTable().fnAdjustColumnSizing();},1);
				break;	
				case 'tabs-5':
					var t = setTimeout(function(){$('#tblPicklist').dataTable().fnAdjustColumnSizing();},1);
				case 'tabs-6':
					var t = setTimeout(function(){$('#tblShipmentSummary').dataTable().fnAdjustColumnSizing();},1);
				break;					
			} 
		}
	});	
	jQuery.fn.exists = function(){return this.length>0;}
	jQuery.isJson = function(str) {
	 if (jQuery.trim(str) == '') return false;
	 str = str.replace(/\\./g, '@').replace(/"[^"\\\n\r]*"/g, '');
	 return (/^[,:{}\[\]0-9.\-+Eaeflnr-u \n\r\t]*$/).test(str);
	}
	
	Number.prototype.padLeft = function(base,chr){
	    var  len = (String(base || 10).length - String(this).length)+1;
	    return len > 0? new Array(len).join(chr || '0')+this : this;
	}

	CurrentDate = [CurrentDate.getDate().padLeft(),(CurrentDate.getMonth() + 1).padLeft(),CurrentDate.getFullYear()].join('/');
	
	function ProcessRequest(data,gotoProcess){
		if('<?php echo $_SESSION[LoginUserVar];?>' == 'dev/jagustin'){
			//var urlForprocess = "modules/gstADIReport/GSTReportClassDev.php";
			var urlForprocess = "modules/gstADIReport/GSTReportClass.php";
		} else {
			var urlForprocess = "modules/gstADIReport/GSTReportClass.php";
		}
		data.UserID='<?php echo (isset($_SESSION[LoginUserVar])) ? $_SESSION[LoginUserVar]: 'none';?>';
		$.ajax({
			type: 'POST',
			url: urlForprocess,
			data: data,
			success: function(data){
				var CallMeBaby = eval('(' + gotoProcess + ')');
				CallMeBaby(data);
			},
			beforeSend: function(){
				if(!isDimStart){
					$.dimScreen(function() {
						$('#content').fadeIn();
					});
				}
			},
			complete: function(){
				$.dimScreenStop();
			}
		});
	}

	function ProcessEmailRequest(data,gotoProcess){
		if('<?php echo $_SESSION[LoginUserVar];?>' == 'dev/jagustin'){
			//var urlForprocess = "modules/gstADIReport/GSTReportClassDev.php";
			var urlForprocess = "modules/gstADIReport/GSTReportClass.php";
		} else {
			var urlForprocess = "modules/gstADIReport/GSTReportClass.php";
		}
		data.UserID='<?php echo (isset($_SESSION[LoginUserVar])) ? $_SESSION[LoginUserVar]: 'none';?>';
		$.ajax({
			type: 'POST',
			url: urlForprocess,
			data: data,
			async: false,
			success: function(data){
				var CallMeBaby = eval('(' + gotoProcess + ')');
				CallMeBaby(data);
			},
			beforeSend: function(){
				if(!isDimStart){
					$.dimScreen(function() {
						$('#content').fadeIn();
					});
				}
			},
			complete: function(){
				$.dimScreenStop();
			}
		});
	}

	function ProcessReportRequest(data,gotoProcess){
		if('<?php echo $_SESSION[LoginUserVar];?>' == 'dev/jagustin'){
			//var urlForprocess = "modules/gstADIReport/GSTReportClassDev.php";
			var urlForprocess = "modules/gstADIReport/GSTReportClass.php";
		} else {
			var urlForprocess = "modules/gstADIReport/GSTReportClass.php";
		}
		$.ajax({
			type: 'POST',
			url: urlForprocess,
			data: data,
			async: true,
			success: function(data){
				var CallMeBaby = eval('(' + gotoProcess + ')');
				CallMeBaby(data);
			},
			beforeSend: function(){
				//dimStart();
			},
			complete: function(){
				//dimStop();
			},
			error: function(jqXHR, textStatus, errorThrown){
				
				var responseTextRes = jqXHR.responseText;
				var responseTextChecker =  responseTextRes.includes("Maximum execution time of 300 seconds exceeded")
				if(jqXHR.status != 200 || responseTextChecker){
					ajaxReportError = true;
					
					if(gotoProcess == 'generateIncomingDetailed'){
						isGenerateIncomingDetailed = false;
					}
					
					if(gotoProcess == 'generateIncomingSummary'){
						isGenerateIncomingSummary = false;
					}
					
					if(gotoProcess == 'generateOutgoingDetailed'){
						isGenerateOutgoingDetailed = false;
					}
					
					if(gotoProcess == 'generateOutgoingSummary'){
						isGenerateOutgoingSummary = false;
					}
					
					if(gotoProcess == 'generateShipmentPicklist'){
						isGenerateShipmentPicklist = false;
					}
					
					if(gotoProcess == 'generateShipmentSummary'){
						isGenerateShipmentSummary = false;
					}
					
					if(!isGenerateIncomingDetailed &&
					   !isGenerateIncomingSummary &&
					   !isGenerateOutgoingDetailed &&
					   !isGenerateOutgoingSummary &&
					   !isGenerateShipmentPicklist &&
					   !isGenerateShipmentSummary)
					{
						
						saveRequestReportViaMail();
						
					}
					
				}
				
			}
		});
	}
	
	function ajaxfnReportError(gotoProcess){
		
		if(gotoProcess == 'generateIncomingDetailed'){
			isGenerateIncomingDetailed = false;
		}
		
		if(gotoProcess == 'generateIncomingSummary'){
			isGenerateIncomingDetailed = false;
		}
		
		if(gotoProcess == 'generateOutgoingDetailed'){
			isGenerateIncomingDetailed = false;
		}
		
		if(gotoProcess == 'generateOutgoingSummary'){
			isGenerateIncomingDetailed = false;
		}
		
		if(gotoProcess == 'generateShipmentPicklist'){
			isGenerateIncomingDetailed = false;
		}
		
		if(gotoProcess == 'generateShipmentSummary'){
			isGenerateIncomingDetailed = false;
		}
		
		if(!isGenerateIncomingDetailed &&
		   !isGenerateIncomingSummary &&
		   !isGenerateOutgoingDetailed &&
		   !isGenerateOutgoingSummary &&
		   !isGenerateShipmentPicklist &&
		   !isGenerateShipmentSummary)
		{
			
			saveRequestReportViaMail();
			
		}
		
	}
	
	function dimStart(){
		console.log('dimScreenStart');
		isDimStart = false;
		$.dimScreen(function() {
			$('#content').fadeIn();
		});
		isDimStart = true;
	}
	
	function dimStop(){
		console.log('dimScreenStop');
		if(isDimStart){
			$.dimScreenStop();
		}
	}
	

	function msgbox(msg,focusField){
		$( "#dialog-msgbox:ui-dialog" ).dialog( "destroy" );
		$( "#dialog-msgbox" ).html(msg);
		$( "#dialog-msgbox" ).attr("title",'GST Report');
		$( "#dialog-msgbox" ).dialog('option', 'position', 'center');
		$( "#dialog-msgbox" ).dialog({
			modal: true,
			height:'auto',
			show:{ 
				effect:"blind", 
				speed:'slow' 
			},
			hide:'blind',
			buttons: {
				Ok: function() {
					$(this).dialog( "close" );
				}
			}
		});
	}
	
	
	$('#btnSearch').click(function(){
		
		isGenerateIncomingDetailed = false;
		isGenerateIncomingSummary = false;
		isGenerateOutgoingDetailed = false;
		isGenerateOutgoingSummary = false;
		isGenerateShipmentPicklist = false;
		isGenerateShipmentSummary = false;
		
		var chkall					= $('#chkall').is(':checked');
		var chkIncomingDetailed		= $('#chkIncomingDetailed').is(':checked');
		var chkIncomingSummary		= $('#chkIncomingSummary').is(':checked');
		var chkOutgoingDetailed		= $('#chkOutgoingDetailed').is(':checked');
		var chkOutgoingSummary		= $('#chkOutgoingSummary').is(':checked');
		var chkShipmentPicklist		= $('#chkShipmentPicklist').is(':checked');
		var chkShipmentSummary		= $('#chkShipmentSummary').is(':checked');
		
		if(!chkall && !chkIncomingDetailed && !chkIncomingSummary && !chkOutgoingDetailed && !chkOutgoingSummary && !chkShipmentPicklist && !chkShipmentSummary){
			msgbox('Please select report!');
			return false;
		}
		
		if($.trim($('#searchInput1').val()) == ''){
			msgbox('Please input Filter!');
			return false;
		}
		
		if($.trim($('#searchCon').val()) == 'between' && $.trim($('#searchInput2').val()) == ''){
			msgbox('Please input Filter to!');
			return false;
		}
		
		if($.trim($('#searchColumn').val()) == 'TaxScheme' && $.trim($('#searchmainValue').val()) == ''){
			msgbox('Please input Search Value!');
			return false;
		}
		
		if($.trim($('#searchColumn').val()) != 'TaxScheme' &&
		   $.trim($('#searchColumn').val()) != 'Date' &&
		   ($.trim($('#searchCon').val()) == 'notequalto' || $.trim($('#searchCon').val()) == 'contains') && $.trim($('#searchmainValue').val()) == ''){
			msgbox('Please input Search Value!');
			return false;
		}
		
		if($.trim($('#searchCon').val()) == 'between' && $.trim($('#searchColumn').val()) == 'Date'){
			
			var dataInput1 = $.trim($('#searchInput1').val());
			var dataInput2 = $.trim($('#searchInput2').val());
			
			var converteddataInput1 = dataInput1.split('/');
			converteddataInput1 = converteddataInput1[2]+'-'+converteddataInput1[1]+'-'+converteddataInput1[0];
			
			var converteddataInput2 = dataInput2.split('/');
			converteddataInput2 = converteddataInput2[2]+'-'+converteddataInput2[1]+'-'+converteddataInput2[0];
			
			var converteddataInput1 = new Date(converteddataInput1);
			var converteddataInput2 = new Date(converteddataInput2);
			
			var daysTotal = ((converteddataInput2- converteddataInput1) / (1000 * 60 * 60 * 24)) + 1;
			
			if(daysTotal > 7){
				
				ProcessRequest({'todo':'generateReportViaMail',
							'fromProcess':'dateexceed',
							'chkall':chkall,
							'chkIncomingDetailed':chkIncomingDetailed,
							'chkIncomingSummary':chkIncomingSummary,
							'chkOutgoingDetailed':chkOutgoingDetailed,
							'chkOutgoingSummary':chkOutgoingSummary,
							'chkShipmentPicklist':chkShipmentPicklist,
							'chkShipmentSummary':chkShipmentSummary,
							'searchColumn':$('#searchColumn').val(),
							'searchColumn':$('#searchColumn').val(),
							'searchCon':$('#searchCon').val(),
							'searchInput1':$('#searchInput1').val(),
							'searchInput2':$('#searchInput2').val(),
							'searchmainValue':$('#searchmainValue').val()
				},'generateReportViaMail');
				
				return false;
			}
			
		}
		
		if(chkIncomingDetailed){
			isGenerateIncomingDetailed = true;
		}
		
		if(chkIncomingSummary){
			isGenerateIncomingSummary = true;
		}
		
		if(chkOutgoingDetailed){
			isGenerateOutgoingDetailed = true;
		}
		
		if(chkOutgoingSummary){
			isGenerateOutgoingSummary = true;
		}
		
		if(chkShipmentPicklist){
			isGenerateShipmentPicklist = true;
		}
		
		if(chkShipmentSummary){
			isGenerateShipmentSummary = true;
		}
		
		dimStart();
		setTimeout(function(){
			generateEachReport();
		}, 3000);
		return false;
		
	});
	
	function generateEachReport(){
		
		ajaxReportError = false;
		
		tblIncomingDetailed.fnClearTable();
		tblIncomingSummary.fnClearTable();
		tblOutgoingDetailed.fnClearTable();
		tblOutgoingSummary.fnClearTable();
		tblPicklist.fnClearTable();
		tblShipmentSummary.fnClearTable();
		
		if(isGenerateIncomingDetailed){
			ProcessReportRequest({'todo':'generateIncomingDetailed',
							'searchColumn':$('#searchColumn').val(),
							'searchCon':$('#searchCon').val(),
							'searchInput1':$('#searchInput1').val(),
							'searchInput2':$('#searchInput2').val(),
							'searchmainValue':$('#searchmainValue').val()
			},'generateIncomingDetailed');
		}
		
		if(isGenerateIncomingSummary){
			ProcessReportRequest({'todo':'generateIncomingSummary',
							'searchColumn':$('#searchColumn').val(),
							'searchCon':$('#searchCon').val(),
							'searchInput1':$('#searchInput1').val(),
							'searchInput2':$('#searchInput2').val(),
							'searchmainValue':$('#searchmainValue').val()
			},'generateIncomingSummary');
		}
		
		if(isGenerateOutgoingDetailed){
			ProcessReportRequest({'todo':'generateOutgoingDetailed',
							'searchColumn':$('#searchColumn').val(),
							'searchCon':$('#searchCon').val(),
							'searchInput1':$('#searchInput1').val(),
							'searchInput2':$('#searchInput2').val(),
							'searchmainValue':$('#searchmainValue').val()
			},'generateOutgoingDetailed');
		}
		
		if(isGenerateOutgoingSummary){
			ProcessReportRequest({'todo':'generateOutgoingSummary',
							'searchColumn':$('#searchColumn').val(),
							'searchCon':$('#searchCon').val(),
							'searchInput1':$('#searchInput1').val(),
							'searchInput2':$('#searchInput2').val(),
							'searchmainValue':$('#searchmainValue').val()
			},'generateOutgoingSummary');
		}
		
		if(isGenerateShipmentPicklist){
			ProcessReportRequest({'todo':'generateShipmentPicklist',
							'searchColumn':$('#searchColumn').val(),
							'searchCon':$('#searchCon').val(),
							'searchInput1':$('#searchInput1').val(),
							'searchInput2':$('#searchInput2').val(),
							'searchmainValue':$('#searchmainValue').val()
			},'generateShipmentPicklist');
		}
		
		if(isGenerateShipmentSummary){
			ProcessReportRequest({'todo':'generateShipmentSummary',
							'searchColumn':$('#searchColumn').val(),
							'searchCon':$('#searchCon').val(),
							'searchInput1':$('#searchInput1').val(),
							'searchInput2':$('#searchInput2').val(),
							'searchmainValue':$('#searchmainValue').val()
			},'generateShipmentSummary');
		}
		
		//if(ajaxReportError){
		//	
		//	//dimStop();
		//	
		//	var chkall					= $('#chkall').is(':checked');
		//	var chkIncomingDetailed		= $('#chkIncomingDetailed').is(':checked');
		//	var chkIncomingSummary		= $('#chkIncomingSummary').is(':checked');
		//	var chkOutgoingDetailed		= $('#chkOutgoingDetailed').is(':checked');
		//	var chkOutgoingSummary		= $('#chkOutgoingSummary').is(':checked');
		//	var chkShipmentPicklist		= $('#chkShipmentPicklist').is(':checked');
		//	var chkShipmentSummary		= $('#chkShipmentSummary').is(':checked');
		//	
		//	ProcessRequest({'todo':'generateReportViaMail',
		//					'fromProcess':'witherror',
		//					'chkall':chkall,
		//					'chkIncomingDetailed':chkIncomingDetailed,
		//					'chkIncomingSummary':chkIncomingSummary,
		//					'chkOutgoingDetailed':chkOutgoingDetailed,
		//					'chkOutgoingSummary':chkOutgoingSummary,
		//					'chkShipmentPicklist':chkShipmentPicklist,
		//					'chkShipmentSummary':chkShipmentSummary,
		//					'searchColumn':$('#searchColumn').val(),
		//					'searchColumn':$('#searchColumn').val(),
		//					'searchCon':$('#searchCon').val(),
		//					'searchInput1':$('#searchInput1').val()
		//	},'generateReportViaMail');
		//	
		//}
		
	}
	
	function saveRequestReportViaMail(){
		
		var chkall					= $('#chkall').is(':checked');
		var chkIncomingDetailed		= $('#chkIncomingDetailed').is(':checked');
		var chkIncomingSummary		= $('#chkIncomingSummary').is(':checked');
		var chkOutgoingDetailed		= $('#chkOutgoingDetailed').is(':checked');
		var chkOutgoingSummary		= $('#chkOutgoingSummary').is(':checked');
		var chkShipmentPicklist		= $('#chkShipmentPicklist').is(':checked');
		var chkShipmentSummary		= $('#chkShipmentSummary').is(':checked');
		
		ProcessEmailRequest({'todo':'generateReportViaMail',
						'fromProcess':'witherror',
						'chkall':chkall,
						'chkIncomingDetailed':chkIncomingDetailed,
						'chkIncomingSummary':chkIncomingSummary,
						'chkOutgoingDetailed':chkOutgoingDetailed,
						'chkOutgoingSummary':chkOutgoingSummary,
						'chkShipmentPicklist':chkShipmentPicklist,
						'chkShipmentSummary':chkShipmentSummary,
						'searchColumn':$('#searchColumn').val(),
						'searchColumn':$('#searchColumn').val(),
						'searchCon':$('#searchCon').val(),
						'searchInput1':$('#searchInput1').val(),
						'searchInput2':$('#searchInput2').val(),
						'searchmainValue':$('#searchmainValue').val()
		},'generateReportViaMail');
		
	}
	
	function generateReportViaMail(result){
		
		if(result.success){
			
			if(result.fromProcess == 'dateexceed'){
				tblIncomingDetailed.fnClearTable();
				tblIncomingSummary.fnClearTable();
				tblOutgoingDetailed.fnClearTable();
				tblOutgoingSummary.fnClearTable();
				tblPicklist.fnClearTable();
				tblShipmentSummary.fnClearTable();
			}
			
			msgbox(result.message);
		} else {
			
			if(result.fromProcess == 'dateexceed'){
				tblIncomingDetailed.fnClearTable();
				tblIncomingSummary.fnClearTable();
				tblOutgoingDetailed.fnClearTable();
				tblOutgoingSummary.fnClearTable();
				tblPicklist.fnClearTable();
				tblShipmentSummary.fnClearTable();
			}
			
			msgbox(result.message);
		}
		
	}
	
	function generateIncomingDetailed(reportData){
		
		//tblIncomingDetailed.fnClearTable();
		
		if (reportData != 'nodata'){
			
			var i = 1;
			tmpArray = [];
			$('#tabs > ul').tabs({ selected: 1 });
			
			$.each(reportData,function(a,b){
				tmpArray.push([i,
							   b['WawiAlias'],
							   b['ShipReference'],
							   b['ArrivalDate'],
							   b['VendorInvNos'],
							   b['suQuantity'],
							   b['HAWBTotalQty'],
							   b['InTaxRef'],
							   b['PermitNo'],
							   b['ExchangeRate'],
							   parseFloat(b['InvoiceValue1']).toFixed(2),
							   parseFloat(b['InvoiceValueSGD2']).toFixed(2),
							   parseFloat(b['FreightCharges']).toFixed(2),
							   parseFloat(b['InsCharges']).toFixed(2),
							   parseFloat(b['CIFAmount2']).toFixed(2)
							   ,parseFloat(b['GSTAmount2']).toFixed(2),
							   b['OriginPort'],
							   b['Forwarders'],
							   b['Supplier'],
							   b['HAWB'],
							   b['MAWB'],
							   b['TptMode'],
							   b['ArrivalDate'],
							   b['OMNI'],
							   b['INCOTERM'],
							   b['JobType'],
							   b['Comment'],
							   b['IncomingReference'],
							   b['Description'],
							   b['EntrySU'],
							   b['isConsigned'],
							   b['SUConsignedHistory']]);
				i++;
			});
			tblIncomingDetailed.fnAddData(tmpArray);
		}
		
		isGenerateIncomingDetailed = false;
		if(!isGenerateIncomingDetailed &&
		   !isGenerateIncomingSummary &&
		   !isGenerateOutgoingDetailed &&
		   !isGenerateOutgoingSummary &&
		   !isGenerateShipmentPicklist &&
		   !isGenerateShipmentSummary)
		{
			
			if(ajaxReportError){
				saveRequestReportViaMail();
			} else {
				dimStop();
			}
			
		}
		
	}
	
	function generateIncomingSummary(reportData){
		
		//tblIncomingSummary.fnClearTable();
		
		if (reportData != 'nodata'){
			
			var i = 1;
			tmpArray = [];
			$('#tabs > ul').tabs({ selected: 2 });
			
			$.each(reportData,function(a,b){
				tmpArray.push([i,
							   b['Customer'],
							   b['ShipReference'],
							   b['ArrivalDate1'],
							   b['Supplier'],
							   b['VendorInvNo2'],
							   b['InTaxRef'],
							   b['PermitNo'],
							   b['ShipmentQty'],
							   b['HAWBTotalQty'],
							   parseFloat(b['InvoiceValue']).toFixed(2),
							   parseFloat(b['InvoiceValueSGD']).toFixed(2),
							   parseFloat(b['FreightCharges']).toFixed(2),
							   parseFloat(b['InsCharges']).toFixed(2),
							   parseFloat(b['CIFAmount']).toFixed(2),
							   parseFloat(b['GSTAmount']).toFixed(2),
							   b['JobType'],
							   b['HAWB'],
							   b['MAWB'],
							   b['TptMode'],
							   b['Comments'],
							   b['Description1'],
							   b['EntrySU'],
							   b['isConsigned'],
							   b['SUConsignedHistory']]);
				i++;
			});
			tblIncomingSummary.fnAddData(tmpArray);
		}
		
		isGenerateIncomingSummary = false;
		if(!isGenerateIncomingDetailed &&
		   !isGenerateIncomingSummary &&
		   !isGenerateOutgoingDetailed &&
		   !isGenerateOutgoingSummary &&
		   !isGenerateShipmentPicklist &&
		   !isGenerateShipmentSummary)
		{
			
			if(ajaxReportError){
				saveRequestReportViaMail();
			} else {
				dimStop();
			}
			
		}
		
	}
	
	function generateOutgoingDetailed(reportData){
		
		//tblOutgoingDetailed.fnClearTable();
		
		if (reportData != 'nodata'){
			
			var i = 1;
			tmpArray = [];
			$('#tabs > ul').tabs({ selected: 2 });
			
			$.each(reportData,function(a,b){
				tmpArray.push([i,
							   b['Customer'],
							   b['ShipmentRef'],
							   b['PackListNumber'],
							   b['CargoReleasedDate'],
							   b['ShipmentNumber'],
							   b['CustInvoice'],
							   b['CustSupplier'],
							   b['DN'],
							   b['TotalQty'],
							   b['ShipTotalQty'],
							   b['UnitPrice'],
							   b['OutTaxRef'],
							   b['PermitNo'],
							   b['ExchangeRate'],
							   b['ShippingCountry'],
							   b['Forwarder'],
							   b['ShipTo'],
							   b['HAWB'],
							   b['MAWB'],
							   b['TptMode'],
							   b['DepDate'],
							   b['DeliveryFrom'],
							   b['IncoTerm'],
							   b['JobType'],
							   b['Comment'],
							   b['CustInvValue'],
							   b['InvoiceValueSGD'],
							   b['GSTAmountValue'],
							   b['ShipTotalValueSGD'],
							   b['SUNumbers'],
							   b['MaterialDescriptions']]);
				i++;
			});
			tblOutgoingDetailed.fnAddData(tmpArray);
		}
		
		isGenerateOutgoingDetailed = false;
		if(!isGenerateIncomingDetailed &&
		   !isGenerateIncomingSummary &&
		   !isGenerateOutgoingDetailed &&
		   !isGenerateOutgoingSummary &&
		   !isGenerateShipmentPicklist &&
		   !isGenerateShipmentSummary)
		{
			
			if(ajaxReportError){
				saveRequestReportViaMail();
			} else {
				dimStop();
			}
			
		}
		
	}
	
	function generateOutgoingSummary(reportData){
		
		//tblOutgoingSummary.fnClearTable();
		
		if (reportData != 'nodata'){
			
			var i = 1;
			tmpArray = [];
			$('#tabs > ul').tabs({ selected: 2 });
			
			$.each(reportData,function(a,b){
				tmpArray.push([i,
							   b['Customer'],
							   b['ShipmentRef'],
							   b['CargoReleasedDate'],
							   b['ShipmentNumber'],
							   b['CustInvoice'],
							   b['CustSupplier'],
							   b['DN'],
							   b['TotalQty'],
							   b['ShipTotalQty'],
							   b['UnitPrice'],
							   b['OutTaxRef'],
							   b['PermitNo'],
							   b['ExchangeRate'],
							   b['ShippingCountry'],
							   b['Forwarder'],
							   b['ShipTo'],
							   b['HAWB'],
							   b['MAWB'],
							   b['TptMode'],
							   b['DepDate'],
							   b['DeliveryFrom'],
							   b['IncoTerm'],
							   b['JobType'],
							   b['Comment'],
							   b['CustInvValue'],
							   b['InvoiceValueSGD'],
							   b['GSTAmountValue'],
							   b['ShipTotalValueSGD'],
							   b['SUNumbers'],
							   b['MaterialDescriptions']]);
				i++;
			});
			tblOutgoingSummary.fnAddData(tmpArray);
		}
		
		isGenerateOutgoingSummary = false;
		if(!isGenerateIncomingDetailed &&
		   !isGenerateIncomingSummary &&
		   !isGenerateOutgoingDetailed &&
		   !isGenerateOutgoingSummary &&
		   !isGenerateShipmentPicklist &&
		   !isGenerateShipmentSummary)
		{
			
			if(ajaxReportError){
				saveRequestReportViaMail();
			} else {
				dimStop();
			}
			
		}
		
	}
	
	function generateShipmentPicklist(reportData){
		
		//tblPicklist.fnClearTable();
		
		if (reportData != 'nodata'){
			
			var i = 1;
			tmpArray = [];
			$('#tabs > ul').tabs({ selected: 2 });
			
			$.each(reportData,function(a,b){
				tmpArray.push([i,
							   b['OutRef'],
							   b['ReleasedDate'],
							   b['OutWMSRef'],
							   b['PackListnumber'],
							   b['CustomerReference'],
							   b['UnitPrice'],
							   b['HAWB'],
							   b['OutPermit'],
							   b['OutPart'],
							   b['OutTaxScheme'],
							   b['OutQty'],
							   b['OutEntry'],
							   b['InRef'],
							   b['ReceivedDate'],
							   b['InWMSRef'],
							   b['Supplier'],
							   b['Invoice'],
							   b['AirwayBill'],
							   b['InPermit'],
							   b['InPart'],
							   b['InTaxScheme'],
							   b['InQty'],
							   b['InEntry'],
							   b['SUBal'],
							   b['BalEntry'],
							   b['PreviousEntry'],
							   b['TotalIncomingQty'],
							   b['TotalBalanceQty']]);
				i++;
			});
			tblPicklist.fnAddData(tmpArray);
		}
		
		isGenerateShipmentPicklist = false;
		if(!isGenerateIncomingDetailed &&
		   !isGenerateIncomingSummary &&
		   !isGenerateOutgoingDetailed &&
		   !isGenerateOutgoingSummary &&
		   !isGenerateShipmentPicklist &&
		   !isGenerateShipmentSummary)
		{
			
			if(ajaxReportError){
				saveRequestReportViaMail();
			} else {
				dimStop();
			}
			
		}
		
	}
	
	function generateShipmentSummary(reportData){
		
		//tblShipmentSummary.fnClearTable();
		
		if (reportData != 'nodata'){
			
			var i = 1;
			tmpArray = [];
			$('#tabs > ul').tabs({ selected: 2 });
			
			$.each(reportData,function(a,b){
				tmpArray.push([i,
							   b['InRef'],
							   b['ReceivedDate'],
							   b['InWMSRef'],
							   b['Supplier'],
							   b['Invoice'],
							   b['AirwayBill'],
							   b['InPermit'],
							   b['InPart'],
							   b['InTaxScheme'],
							   b['InQty'],
							   b['InEntry'],
							   b['OutRef'],
							   b['ReleasedDate'],
							   b['OutWMSRef'],
							   b['PackListnumber'],
							   b['CustomerReference'],
							   b['UnitPrice'],
							   b['HAWB'],
							   b['OutPermit'],
							   b['OutPart'],
							   b['OutTaxScheme'],
							   b['OutQty'],
							   b['OutEntry'],
							   b['SUBal'],
							   b['BalEntry'],
							   b['TotalIncomingQty'],
							   b['TotalBalanceQty']]);
				i++;
			});
			tblShipmentSummary.fnAddData(tmpArray);
		}
		
		isGenerateShipmentSummary = false;
		if(!isGenerateIncomingDetailed &&
		   !isGenerateIncomingSummary &&
		   !isGenerateOutgoingDetailed &&
		   !isGenerateOutgoingSummary &&
		   !isGenerateShipmentPicklist &&
		   !isGenerateShipmentSummary)
		{
			
			if(ajaxReportError){
				saveRequestReportViaMail();
			} else {
				dimStop();
			}
			
		}
		
	}
	
	
	//$( "#searchInput1" ).datepicker({		
	//		dateFormat: 'mm/dd/yy',
	//    defaultDate: "+1w",
	//    changeMonth: true,
	//    onClose: function( selectedDate ) {
	//        $( "#searchInput2" ).datepicker( "option", "minDate", selectedDate );
	//    }
	//});
	//
	//$( "#searchInput2" ).datepicker({
	//	dateFormat: 'mm/dd/yy',
	//    defaultDate: "+1w",
	//    changeMonth: true,
	//    onClose: function( selectedDate ) {
	//        $( "#searchInput1" ).datepicker( "option", "maxDate", selectedDate );
	//    }
	//});
	
	
	//Date Filter
	$('#searchColumn').change(function(){
		
		$('#searchCon').val('equalto');
		$('#searchCon option[value="notequalto"]').attr("disabled", false);
		$('#searchCon option[value="contains"]').attr("disabled", false);
		$('#searchCon option[value="between"]').attr("disabled", false);
		$(".filter-between").hide();
		$('#searchmainValue').hide();
		$('#searchmainValue').val('');
		
		//switch($('select[name="searchColumn"] option:selected').attr('class')){
		switch($('select[name="searchColumn"] option:selected').val()){
			case 'Date':
				$("#searchInput1").datepicker({		
					dateFormat: 'dd/mm/yy',
					defaultDate: "+1w",
					changeMonth: true,
					onClose: function( selectedDate ) {
						
						//var convertedDate = selectedDate.split('/');
						//convertedDate = convertedDate[2]+'-'+convertedDate[1]+'-'+convertedDate[0];
						//
						//var getNewDate = new Date(convertedDate);
						//getNewDate.setDate(getNewDate.getDate() + 6);
						//getNewDate = [getNewDate.getDate().padLeft(),(getNewDate.getMonth() + 1).padLeft(),getNewDate.getFullYear()].join('/');
						//
						//$('#searchInput2').val(selectedDate);
						//$("#searchInput2").datepicker("option","minDate",selectedDate );
						//$("#searchInput2").datepicker("option","maxDate",getNewDate );
						
						$('#searchInput2').val(selectedDate);
						$("#searchInput2").datepicker("option","minDate",selectedDate );
					}
				});
				$("#searchInput2").datepicker({		
					dateFormat: 'dd/mm/yy',
					defaultDate: "+1w",
					changeMonth: true,
					minDate: 0,
					//maxDate: 6,
					onClose: function( selectedDate ) {
						
					}
				});
				$('#searchCon option[value="notequalto"]').attr("disabled", true);
				$('#searchCon option[value="contains"]').attr("disabled", true);
				$('#searchInput1').val(CurrentDate);
				$('#searchInput2').val(CurrentDate);
			break;
			case 'TaxScheme':
				$('#searchmainValue').show();
				$("#searchInput1").datepicker({		
					dateFormat: 'dd/mm/yy',
					defaultDate: "+1w",
					changeMonth: true,
					onClose: function( selectedDate ) {
						
						var convertedDate = selectedDate.split('/');
						convertedDate = convertedDate[2]+'-'+convertedDate[1]+'-'+convertedDate[0];
						
						var getNewDate = new Date(convertedDate);
						getNewDate.setDate(getNewDate.getDate() + 30);
						getNewDate = [getNewDate.getDate().padLeft(),(getNewDate.getMonth() + 1).padLeft(),getNewDate.getFullYear()].join('/');
						
						$('#searchInput2').val(selectedDate);
						$("#searchInput2").datepicker("option","minDate",selectedDate );
						$("#searchInput2").datepicker("option","maxDate",getNewDate );
						
						//$('#searchInput2').val(selectedDate);
						//$("#searchInput2").datepicker("option","minDate",selectedDate );
						//$("#searchInput2").datepicker("option","maxDate","+1m" );
					}
				});
				$("#searchInput2").datepicker({		
					dateFormat: 'dd/mm/yy',
					defaultDate: "+1w",
					changeMonth: true,
					minDate: 0,
					//maxDate: 6,
					onClose: function( selectedDate ) {
						
					}
				});
				
				var getNewDate = new Date();
				getNewDate.setDate(getNewDate.getDate() + 30);
				getNewDate = [getNewDate.getDate().padLeft(),(getNewDate.getMonth() + 1).padLeft(),getNewDate.getFullYear()].join('/');
				
				$('#searchInput1').val(CurrentDate);
				$('#searchInput2').val(CurrentDate);
				$("#searchInput2").datepicker("option","maxDate",getNewDate );
				$(".filter-between").show();
				$('#searchCon option[value="between"]').attr("disabled", true);
			break;
			default:
				$("#searchInput1, #searchInput2").datepicker("destroy").val('');
			break;
		}

	})
	
	$('#searchCon').change(function(){
		
		if($('select[name="searchColumn"] option:selected').val() != 'TaxScheme'){
			$('#searchmainValue').hide();
		}
		
		switch($('select[name="searchCon"] option:selected').val()){
			case 'between':
				$(".filter-between").show();
				if($('select[name="searchColumn"] option:selected').val() != 'Date'){
					$("#searchInput1, #searchInput2").datepicker("destroy").val('');
				}
			break;
			case 'notequalto':
			case 'contains':
				if($('select[name="searchColumn"] option:selected').val() != 'TaxScheme'){
					$('#searchmainValue').show();
					
					$("#searchInput1").datepicker({		
						dateFormat: 'dd/mm/yy',
						defaultDate: "+1w",
						changeMonth: true,
						onClose: function( selectedDate ) {
							
							var convertedDate = selectedDate.split('/');
							convertedDate = convertedDate[2]+'-'+convertedDate[1]+'-'+convertedDate[0];
							
							var getNewDate = new Date(convertedDate);
							getNewDate.setDate(getNewDate.getDate() + 30);
							getNewDate = [getNewDate.getDate().padLeft(),(getNewDate.getMonth() + 1).padLeft(),getNewDate.getFullYear()].join('/');
							
							$('#searchInput2').val(selectedDate);
							$("#searchInput2").datepicker("option","minDate",selectedDate );
							$("#searchInput2").datepicker("option","maxDate",getNewDate );
							
						}
					});
					$("#searchInput2").datepicker({		
						dateFormat: 'dd/mm/yy',
						defaultDate: "+1w",
						changeMonth: true,
						minDate: 0,
						onClose: function( selectedDate ) {
							
						}
					});
					
					var getNewDate = new Date();
					getNewDate.setDate(getNewDate.getDate() + 30);
					getNewDate = [getNewDate.getDate().padLeft(),(getNewDate.getMonth() + 1).padLeft(),getNewDate.getFullYear()].join('/');
					
					$('#searchInput1').val(CurrentDate);
					$('#searchInput2').val(CurrentDate);
					$("#searchInput2").datepicker("option","maxDate",getNewDate );
				
					$(".filter-between").show();
				}
			break;
			default:
				if($('select[name="searchColumn"] option:selected').val() != 'TaxScheme'){
					$("#searchInput1, #searchInput2").datepicker("destroy").val('');
					$(".filter-between").hide();
				}
			break;
		}

	})
	
	//Date Filter
	
	$('#txtFilterThis').on('keyup change clear',function(){
		tblIncomingDetailed.fnFilter($(this).val(), 4);
	});
	
	$('#exportToExcel').click(function(e){
		
		var tblIncomingDetailedSetting	 = tblIncomingDetailed.fnSettings();
		var tblIncomingDetailedArray = [];
		$.each(tblIncomingDetailedSetting.aiDisplay,function(ikey,arrayKey){
			tblIncomingDetailedArray.push(tblIncomingDetailed.fnGetData()[arrayKey]);
		});
		
		var tblIncomingSummarySetting	 = tblIncomingSummary.fnSettings();
		var tblIncomingSummaryArray = [];
		$.each(tblIncomingSummarySetting.aiDisplay,function(ikey,arrayKey){
			tblIncomingSummaryArray.push(tblIncomingSummary.fnGetData()[arrayKey]);
		});
		
		var tblOutgoingDetailedSetting	 = tblOutgoingDetailed.fnSettings();
		var tblOutgoingDetailedArray = [];
		$.each(tblOutgoingDetailedSetting.aiDisplay,function(ikey,arrayKey){
			tblOutgoingDetailedArray.push(tblOutgoingDetailed.fnGetData()[arrayKey]);
		});
		
		var tblOutgoingSummarySetting	 = tblOutgoingSummary.fnSettings();
		var tblOutgoingSummaryArray = [];
		$.each(tblOutgoingSummarySetting.aiDisplay,function(ikey,arrayKey){
			tblOutgoingSummaryArray.push(tblOutgoingSummary.fnGetData()[arrayKey]);
		});
		
		var tblPicklistSetting	 = tblPicklist.fnSettings();
		var tblPicklistArray = [];
		$.each(tblPicklistSetting.aiDisplay,function(ikey,arrayKey){
			tblPicklistArray.push(tblPicklist.fnGetData()[arrayKey]);
		});
		
		var tblShipmentSummarySetting	 = tblShipmentSummary.fnSettings();
		var tblShipmentSummaryArray = [];
		$.each(tblShipmentSummarySetting.aiDisplay,function(ikey,arrayKey){
			tblShipmentSummaryArray.push(tblShipmentSummary.fnGetData()[arrayKey]);
		});
		
		
		var tblIncomingDetailedCount = tblIncomingDetailedArray.length;
		var tblIncomingSummaryCount  = tblIncomingSummaryArray.length;
		var tblOutgoingDetailedCount = tblOutgoingDetailedArray.length;
		var tblOutgoingSummaryCount	 = tblOutgoingSummaryArray.length;
		var tblPicklistCount	 	 = tblPicklistArray.length;
		var tblShipmentSummaryCount	 = tblShipmentSummaryArray.length;
		
		if (tblIncomingDetailedCount == 0 && tblIncomingSummaryCount == 0 && tblOutgoingDetailedCount == 0 && tblOutgoingSummaryCount == 0 && tblPicklistCount == 0 && tblShipmentSummaryCount == 0){
			msgbox('No data to Export');
		} else {
			$.fileDownload('modules/gstADIReport/ExportToExcel.php', 
								{ 	httpMethod : "POST",
									data: { StartDate : $('#startDate').val(),
									EndDate : $('#endDate').val(),
									TaxRef : $('#slcReference').val(),
									Customer : 'ADISGWMS',
									tblIncomingDetailed : tblIncomingDetailedArray,
									tblIncomingSummary : tblIncomingSummaryArray,
									tblOutgoingDetailed : tblOutgoingDetailedArray,
									tblOutgoingSummary : tblOutgoingSummaryArray,
									tblPicklistCount: tblPicklistArray,
									tblShipmentSummaryCount: tblShipmentSummaryArray
								 }});
		}
		
	});
	
	tblIncomingDetailed.fnDraw();
	tblIncomingSummary.fnDraw();
	tblOutgoingDetailed.fnDraw();
	tblOutgoingSummary.fnDraw();
	tblPicklist.fnDraw();
	tblShipmentSummary.fnDraw();
	
});	
</script>
<?php 
//error_reporting(E_ALL);
//ini_set("display_errors",1);

//add arrival date on the report
session_start();
require ('../../include/referer.checker.php');
require ('../../include/session.checker.php');

#style="border:1px solid #EEEEEE;"

?>

<!--<link rel="stylesheet" href="css/bootstrap.css">-->
<script type='text/javascript' src='js/jquery.dataTables.columnFilter.js'></script>

<style>
	.alignme{
		text-align: center;
		white-space: nowrap;
	}
	.alignmewrap{
		text-align: center;
		word-wrap: break-word !important;
		white-space: normal !important;
	}
	table.display td{
		white-space: normal !important;
		word-wrap: break-word !important;
	}
	span.totalNos{ color: #CE2929; font-size: 12px; font-weight: bold;}
	.ui-tabs .ui-tabs-nav li a {
		font-size: 11px !important;
	}
</style>

<h2><?php include '../../tpl/module_shortcut.php';?>ADI GST Report</h2> 
<script src="modules/gstADIReport/jquery.fileDownload.js">
</script>

<form onsubmit="return false;" class="globalform">
<ol>
	<div style="float:left;">
		<li style="display: none;">
			<label style="width:100px;">Start Date</label>
			<input type="text" class="text" value="" readonly name="startDate" id="startDate" />
		</li>
		<li style="display: none;">
			<label style="width:100px;">End Date</label>
			<input type="text" class="text" value="" readonly name="endDate" id="endDate" />
		</li>
		<li style="display: none;">
			<label style="width:100px;">&nbsp;</label>
		</li>
		<table id="reportSelection">
			<tbody>
				<tr>
					<td width="15px"><input type="checkbox" class="chkallReport" id="chkall" name="searchReport" value="all" style="margin: 5px !important;" /></td>
					<td width="50px">All</td>
					<td width="15px"><input type="checkbox" class="chkoneReport" id="chkIncomingDetailed" name="searchReport" value="IncomingDetailed" style="margin: 5px !important;"/></td>
					<td width="180px">Incoming GST Detailed</td>
					<td width="15px"><input type="checkbox" class="chkoneReport" id="chkOutgoingDetailed" name="searchReport" value="OutgoingDetailed" style="margin: 5px !important;"/></td>
					<td width="180px">Outgoing GST Detailed</td>
					<td width="15px"><input type="checkbox" class="chkoneReport" id="chkShipmentPicklist" name="searchReport" value="ShipmentPicklist" style="margin: 5px !important;"/></td>
					<td width="180px">Shipment Picklist</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td><input type="checkbox" class="chkoneReport" id="chkIncomingSummary" name="searchReport" value="IncomingSummary" style="margin: 5px !important;"/></td>
					<td>Incoming GST Summary</td>
					<td><input type="checkbox" class="chkoneReport" id="chkOutgoingSummary" name="searchReport" value="OutgoingSummary" style="margin: 5px !important;"/></td>
					<td>Outgoing GST Summary</td>
					<td><input type="checkbox" class="chkoneReport" id="chkShipmentSummary" name="searchReport" value="ShipmentSummary" style="margin: 5px !important;"/></td>
					<td>Shipment Summary</td>
				</tr>
			</tbody>
		</table>
		<table>
			<tr>
				<td width=""><label style="">Column</label></td>
				<td width="" style="padding-left: 15px !important;"><span></td>
				<td width="" style="padding-left: 15px !important;"><label style="">Filter</label></td>
				<td width="" style="padding-left: 15px !important;"></td>
			</tr>
			<tr>
				<td>
					<select class="select" id="searchColumn" name="searchColumn">
						<option value="VendorInvNo" class="thisText">Vendor Inv No.</option>
						<option value="TaxScheme" class="thisText">Tax Reference</option>
						<option value="HAWB" class="thisText">HAWB</option>
						<option value="Date" class="thisDate">Date</option>
						<option value="Permit" class="thisText">Permit</option>
						<option value="PartNoDesc" class="thisText">Description/Part no.</option>
						<option value="ShipmentNo" class="thisText">Shipment #</option>
						<option value="SUNumber" class="thisText">SU/Entry#</option>
					</select>
				</td>
				<td style="padding-left: 15px !important;">
					<select class="select" id="searchCon" name="searchCon">
						<option value="equalto">Equal To</option>
						<option value="notequalto">Not Equal To</option>
						<option value="contains">Contains</option>
						<option value="greaterthan">Greater Than</option>
						<option value="lessthan">Less Than</option>
						<option value="between">Between</option>
					</select>
				</td>
				<td style="padding-left: 15px !important;">
					<input type="text" class="text" style="height: 18px !important;" id="searchInput1" name="searchInput1"/>
					<label class="filter-between" style="display: none; width: 12px !important; margin-left: 10px !important;">to</label>
					<input type="text" class="text filter-between" style="display: none; height: 18px !important; margin-left: 10px !important;" id="searchInput2" name="searchInput2"/>
				</td>
				<td style="padding-left: 25px !important;">
					<input type="button" class="button" style="height: 30px !important; width: 100px;" value="Search" id="btnSearch" />
					<input type="button" class="button" style="height: 30px !important; width: 120px; margin-left: 15px;" value="Export to Excel" id="exportToExcel" />
				</td>
			</tr>
		</table>
		
	</div>
	<div style="float:left; margin-left: 15px;">
		<li style="display:none;">
			<label style="width:100px;">Customer</label>
			<select class="select" id="slcCustomer" name="slcCustomer"></select>
		</li>
		<li style="display:none;">
			<label style="width:100px;">Tax Reference</label>
			<select class="select" id="slcReference" name="slcReference"></select>
			<span><input type="checkbox" id="checkAll"/>all</span>
		</li>
		<li style="display: none;">
			<label style="width:100px;">IN.Shipment Ref#</label>
			<input type="text" class="text" id="shipmentReference" name="shipmentReference"/>
		</li>
		<li id="entry-show" style="display: none;">
			<label style="width:100px;">Entry Number</label>
			<select type="select"  id="entryNumber" name="entryNumber"></select>
		</li>
	</div>
	<div style="float:left; margin-left: 15px; display: none;">
		<li><label style="width:1px;">&nbsp;</label><input type="button" class="button" value="Generate Report" id="generateReport" /></li>
		<!--<li><label style="width:1px;">&nbsp;</label><input type="button" class="button" value="Export to Excel" id="exportToExcel" /></li>-->
		<li><label style="width:1px;">&nbsp;</label></li>
	</div>
</ol>
</form>
<div class="clr"></div>
<br/>
<div id="DivTabs">
	<div id="tabs">
	  <ul>
	      <li><a href="#tabs-1">Incoming GST Detailed</a></li>
	      <li><a href="#tabs-2">Incoming GST Summary</a></li>
	      <li><a href="#tabs-3">Outgoing GST Detailed</a></li>
	      <li><a href="#tabs-4">Outgoing GST Summary</a></li>
	      <li><a href="#tabs-5">Shipment Picklist</a></li>
	      <li><a href="#tabs-6">Shipment Summary</a></li>
	  </ul>
	  <div id="tabs-1">
		<!--<input type="text" class="text" id="txtFilterThis" placeholder="Reference #" style="width:100px;">-->
	     <table class="display" id="tblIncomingDetailed">
			<thead></thead>
			<tbody></tbody>
			<tfoot>
				<tr>
					<td align="center">S/No</th>
					<td align="center">Customer</th>
					<td align="center">Shipment #</th>
					<td align="center">Shpmt Rcvd Date</th>
					<td align="center">VendorInvNo.</th>
					<td align="center">Total Qty</th>
					<td align="center">Tax Reference</th>
					<td align="center">Permit No.</th>
					<td align="center">Exchange Rate</th>
					<td align="center">Vendor Inv Value (USD)</th>
					<td align="center">Vendor Inv Value (SGD)</th>
					<td align="center">FrtCharges (SGD)</th>
					<td align="center">InsCharges (SGD)</th>
					<td align="center">CIF Amount (SGD)</th>
					<td align="center">GST Amount (SGD)</th>
					<td align="center">Origin Port</th>
					<td align="center">Forwarders</th>
					<td align="center">Vendor /Supplier</th>
					<td align="center">HAWB No.</th>
					<td align="center">MAWB No.</th>
					<td align="center">TptMode</th>
					<td align="center">Arrival Date</th>
					<td align="center">Delivery To</th>
					<td align="center">IncoTerms</th>
					<td align="center">JobType</th>
					<td align="center">Comment</th>
					<td align="center">Incoming Reference</th>
					<td align="center">Description</th>
					<td align="center">SU/Entry#.</th>
					<td align="center">Consignment order (Y/N)</th>
					<td align="center">Previous SU# for consignment</th>
				</tr>
			</tfoot>
		</table>
	  </div>
	  <div id="tabs-2">
	     <table class="display" id="tblIncomingSummary">
			<thead></thead>
			<tbody></tbody>
			<tfoot>
				<tr>
					<td align="center">S/No</th>
					<td align="center">Customer</th>
					<td align="center">Shipment #</th></th>
					<td align="center">Shpmt Rcvd Date</th>
					<td align="center">Vendor/Supplier</th>
					<td align="center">Vendor Inv No.</th>
					<td align="center">Tax Reference</th>
					<td align="center">Permit No.</th>
					<td align="center">Total Qty</th>
					<td align="center">Vendor Inv Value (USD)</th>
					<td align="center">Vendor Inv Value (SGD)</th>
					<td align="center">Frt Charges (SGD)</th>
					<td align="center">Ins Charges (SGD)</th>
					<td align="center">CIF Amount (SGD)</th>
					<td align="center">GST Amount (SGD)</th>
					<td align="center">Job Type</th>
					<td align="center">HAWB No.</th>
					<td align="center">MAWB No.</th>
					<td align="center">Dep Date</th>
					<td align="center">Comment</th>
					<td align="center">Description</th>
					<td align="center">SU/Entry#.</th>
					<td align="center">Consignment order (Y/N)</th>
					<td align="center">Previous SU# for consignment</th>
				</tr>
			</tfoot>
		</table>
	  </div>
	  <div id="tabs-3">
			<table class="display" id="tblOutgoingDetailed">
				<thead></thead>
				<tbody></tbody>
				<tfoot>
					<tr>
						<td align="center">S/No</th>
						<td align="center">Customer</th>
						<td align="center">Shipment #</th>
						<td align="center">Packlist No #</th>
						<td align="center">Shipment Release Date.</th>
						<td align="center">Shipment No.</th>
						<td align="center">Vendor Inv No.</th>
						<td align="center">Vendor/Supplier</th>
						<td align="center">Comm Inv No/DN.</th>
						<td align="center">Total Qty</th>
						<td align="center">Total Qty(Shipment)</th>
						<td align="center">Unit Price</th>
						<td align="center">Out Tax Ref</th>
						<td align="center">Permit #</th>
						<td align="center">Exchange Rate</th>
						<td align="center">Dest Port</th>
						<td align="center">Forwarders</th>
						<td align="center">Consignee</th>
						<td align="center">HAWB No.</th>
						<td align="center">MAWB No.</th>
						<td align="center">Tpt Mode</th>
						<td align="center">Dep Date</th>
						<td align="center">Delivery From</th>
						<td align="center">Inco Terms</th>
						<td align="center">Job Type</th>
						<td align="center">Comment</th>
						<td align="center">Customer Inv Value(USD)</th>
						<td align="center">Customer Inv Value(SGD)</th>
						<td align="center">GST Amount(SGD)</th>
						<td align="center">Total Value(HAWB SGD)</th>
						<td align="center">OUT SU #</th>
						<td align="center">Description</th>
					</tr>
				</tfoot>
			</table>	
	  </div>
	  <div id="tabs-4">
			<table class="display" id="tblOutgoingSummary">
				<thead></thead>
				<tbody></tbody>
				<tfoot>
					<tr>
						<td align="center">S/No</th>
						<td align="center">Customer</th>
						<td align="center">Shipment #</th>
						<td align="center">Shipment Release Date.</th>
						<td align="center">Shipment No.</th>
						<td align="center">Vendor Inv No.</th>
						<td align="center">Vendor/Supplier</th>
						<td align="center">Comm Inv No/DN.</th>
						<td align="center">Total Qty</th>
						<td align="center">Unit Price</th>
						<td align="center">Out Tax Ref</th>
						<td align="center">Permit #</th>
						<td align="center">Exchange Rate</th>
						<td align="center">Dest Port</th>
						<td align="center">Forwarders</th>
						<td align="center">Consignee</th>
						<td align="center">HAWB No.</th>
						<td align="center">MAWB No.</th>
						<td align="center">Tpt Mode</th>
						<td align="center">Dep Date</th>
						<td align="center">Delivery From</th>
						<td align="center">Inco Terms</th>
						<td align="center">Job Type</th>
						<td align="center">Comment</th>
						<td align="center">Customer Inv Value(USD)</th>
						<td align="center">Customer Inv Value(SGD)</th>
						<td align="center">GST Amount(SGD)</th>
						<td align="center">Total Value(HAWB SGD)</th>
						<td align="center">OUT SU #</th>
						<td align="center">Description</th>
					</tr>
				</tfoot>
			</table>
	  </div>
	  <div id="tabs-5">
			<table class="display" id="tblPicklist">
				<thead></thead>
				<tbody></tbody>
				<tfoot>
					<tr>
						<td align="center">#</th>
						<td align="center">Out Reference #</th>
						<td align="center">Shipment Release Date</th>
						<td align="center">Out WMS Reference</th>
						<td align="center">Picklist No.</th>
						<td align="center">DN No.</th>
						<td align="center">Unit Price (USD)</th>
						<td align="center">Out HAWB No.</th>
						<td align="center">Out Permit No.</th>
						<td align="center">Part No.</th>
						<td align="center">Tax Scheme</th>
						<td align="center">Total Outgoing Qty</th>
						<td align="center">Out Box Entry/SU#</th>
						<td align="center">In Reference#</th>
						<td align="center">Shpmt Rcvd Date</th>
						<td align="center">In WMS Reference</th>
						<td align="center">Vendor/Supplier Name</th>
						<td align="center">Vendor Inv No</th>
						<td align="center">In HAWB No</th>
						<td align="center">In Permit No.</th>
						<td align="center">Part No.</th>
						<td align="center">Tax Scheme</th>
						<td align="center">Incoming (QTY)</th>
						<td align="center">In Box Entry/SU#</th>
						<td align="center">Balance (QTY)</th>
						<td align="center">Bal Box Entry/SU#</th>
						<td align="center">Previous SU#</th>
					</tr>
				</tfoot>
			</table>
	  </div>
	  <div id="tabs-6">
			<input type="text" class="text" id="txtSReference" placeholder="Reference #" style="width:100px; display:none;">
			<input type="text" class="text" id="txtSEntry" placeholder="SU/Entry Number" style="width:100px; display:none;">
			<input type="text" class="text" id="txtSTaxScheme" placeholder="Tax Scheme" style="width:100px; display:none;">
			<input type="text" class="text" id="txtSIncoming" placeholder="Incoming" style="width:100px; display:none;">
			<input type="text" class="text" id="txtSOutgoing" placeholder="Outgoing" style="width:100px; display:none;">
			<input type="text" class="text" id="txtSBalance" placeholder="Balance" style="width:100px; display:none;">
			<table class="display" id="tblShipmentSummary">
				<thead></thead>
				<tbody></tbody>
				<tfoot>
					<tr>
						<td align="center">#</th>
						<td align="center">In Reference#</th>
						<td align="center">Shpmt Rcvd Date</th>
						<td align="center">In WMS Reference</th>
						<td align="center">Vendor/Supplier Name</th>
						<td align="center">Vendor Inv No</th>
						<td align="center">In HAWB No</th>
						<td align="center">In Permit No.</th>
						<td align="center">Part No.</th>
						<td align="center">Tax Scheme</th>
						<td align="center">Incoming (QTY)</th>
						<td align="center">In Box Entry/SU#</th>
						<td align="center">Out Reference #</th>
						<td align="center">Shipment Release Date</th>
						<td align="center">Out WMS Reference</th>
						<td align="center">Picklist No.</th>
						<td align="center">DN No.</th>
						<td align="center">Unit Price (USD)</th>
						<td align="center">Out HAWB No.</th>
						<td align="center">Out Permit No.</th>
						<td align="center">Part No.</th>
						<td align="center">Tax Scheme</th>
						<td align="center">Total Outgoing Qty</th>
						<td align="center">Out Box Entry/SU#</th>
						<td align="center">Balance (QTY)</th>
						<td align="center">Bal Box Entry/SU#</th>
					</tr>
				</tfoot>
			</table>
	  </div>
	</div>
</div>
<script>
ErrorKeys = new Object();
ErrorKeys['First'] = 'Beginning of the data';
ErrorKeys['Previous'] = 'Beginning of the data';
ErrorKeys['Last'] = 'Last data';
ErrorKeys['Next'] = 'Last data';
ErrorKeys['Search'] = 'Sorry invalid Shipment Number';
ErrorKeys['nodata'] = 'Sorry no available Reference Number';
ErrorKeys['ErrorSearchingShipment'] = 'Shipment Number is a required field in order to Search.';

var TaxReferenceList = new Object(); // get from masterdata - TaxScheme
var CustomerList = new Object();
var CurrentDate = new Date();

var tblIncomingDetailed;
var tblIncomingSummary;
var tblOutgoingDetailed;
var tblOutgoingSummary;
var tblPicklist;
var tblShipmentSummary;

$(function() {
	$("input:submit, input:button, button, .button").button();
	//$('#DivTabs').hide();
	
	$( "#startDate" ).datepicker({		
			dateFormat: 'mm/dd/yy',
	    defaultDate: "+1w",
	    changeMonth: true,
	    onClose: function( selectedDate ) {
	        $( "#endDate" ).datepicker( "option", "minDate", selectedDate );
	    }
	});
	
	$( "#endDate" ).datepicker({
			dateFormat: 'mm/dd/yy',
	    defaultDate: "+1w",
	    changeMonth: true,
	    onClose: function( selectedDate ) {
	        $( "#startDate" ).datepicker( "option", "maxDate", selectedDate );
	    }
	});
	
	$('.chkallReport').on('change', function () {
		$('.chkoneReport').prop('checked', this.checked);
	});

	$('.chkoneReport').on('change', function(){
		
		var chkoneReportCtr = $('.chkoneReport').length;
		var checkedCtr = 0;
		
		$('.chkoneReport').each(function() {
			if(this.checked){
				checkedCtr++;
			}
		});
		
		if(checkedCtr == 0){
			$('.chkallReport').prop({
				indeterminate: false,
				checked: false
			});
		} else if(checkedCtr > 0 && checkedCtr < chkoneReportCtr){
			$('.chkallReport').prop({
				indeterminate: true,
				checked: false
			});
		} else if(checkedCtr == chkoneReportCtr){
			$('.chkallReport').prop({
				indeterminate: false,
				checked: true
			});
		}
		
	});

	tblIncomingDetailed = $('#tblIncomingDetailed').dataTable({
			"sDom": '<"H">Tlfr<"F"tip>',
			"oTableTools": {
				"aButtons": [
					{
						"sExtends":    "collection",
						"sButtonText": "Save As",
						"aButtons":    [ "xls" ]
					}
				]
			},
			//"bDeferRender": true,
			//"bAutoWidth": false,  
			//'aaSortingFixed': [[3,'asc'],[2,'asc']],
			'bFilter': true,
			'bSortable' : false,
			'bPaginate': true,
			'bJQueryUI': true,
			'aLengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
			'sPaginationType': 'full_numbers',
			"bScrollCollapse": true,  
			"sScrollX":"100%",    
			"bDestroy": true,
			"aoColumns": [
				{"sTitle":"S/No","sWidth":"80px","sClass":"alignme"},
				{"sTitle":"Customer","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Shipment #","sWidth":"90px","sClass":"alignme"},
				{"sTitle":"Shpmt Rcvd Date","sWidth":"130px","sClass":"alignme"},
				{"sTitle":"VendorInvNo.","sWidth":"350px","sClass":"alignmewrap"},
				{"sTitle":"Total Qty","sWidth":"90px","sClass":"alignme"},
				{"sTitle":"Tax Reference","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Permit No.","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Exchange Rate","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Vendor Inv Value (USD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Vendor Inv Value (SGD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"FrtCharges (SGD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"InsCharges (SGD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"CIF Amount (SGD)","sWidth":"90px","sClass":"alignme"},
				{"sTitle":"GST Amount (SGD)","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Origin Port","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Forwarders","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Vendor /Supplier","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"HAWB No.","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"MAWB No.","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"TptMode","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Arrival Date","sWidth":"110px","sClass":"alignme"},
				{"sTitle":"Delivery To","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"IncoTerms","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"JobType","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Comment","sWidth":"350px","sClass":"alignmewrap"},
				{"sTitle":"Incoming Reference","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Description","sWidth":"500px","sClass":"alignmewrap"},
				{"sTitle":"SU/Entry#.","sWidth":"500px","sClass":"alignmewrap"},
				{"sTitle":"Consignment order (Y/N)","sWidth":"150px","sClass":"alignme"},
				{"sTitle":"Previous SU# for consignment","sWidth":"250px","sClass":"alignme"}
			],
			fnDrawCallback: function( oSettings ) {
				$('#tblIncomingDetailed tr td').css('white-space','normal');
			},
			fnFooterCallback: function ( nRow, aaData, iStart, iEnd, aiDisplay ) {
				var iTotalQty = 0;
				var iVendorValUSD = 0;
				var iVendorValSGD = 0;
				var iFrtCharges = 0;
				var iInsCharges = 0;
				var iCIFAmount = 0;
				var iGSTAmount = 0;
				
				$.each(aaData,function(ikey,ivalue){
					iTotalQty += ivalue[5] != 0 ? parseInt(ivalue[5]) : 0;
					iVendorValUSD	+= ivalue[9] != 0 ? parseFloat(ivalue[9]) : 0;
					iVendorValSGD	+= ivalue[10] != 0 ? parseFloat(ivalue[10]) : 0;
					iFrtCharges		+= ivalue[11] != 0 ? parseFloat(ivalue[11]) : 0;
					iInsCharges		+= ivalue[12] != 0 ? parseFloat(ivalue[12]) : 0;
					iCIFAmount		+= ivalue[13] != 0 ? parseFloat(ivalue[13]) : 0;
					iGSTAmount		+= ivalue[14] != 0 ? parseFloat(ivalue[14]) : 0;
				});
				
				var nCells = nRow.getElementsByTagName('td');
				nCells[5].innerHTML  = 'Total Qty : <span class="totalNos">' + iTotalQty+'</span>';
				nCells[9].innerHTML  = 'Vendor Inv Value (USD) : <span class="totalNos">' + parseFloat(iVendorValUSD).toFixed(2)+'</span>';
				nCells[10].innerHTML = 'Vendor Inv Value (SGD) : <span class="totalNos">' + parseFloat(iVendorValSGD).toFixed(2)+'</span>';
				nCells[11].innerHTML = 'Total FrtCharges (SGD) : <span class="totalNos">' + parseFloat(iFrtCharges).toFixed(2)+'</span>';
				nCells[12].innerHTML = 'Total InsCharges (SGD) : <span class="totalNos">' + parseFloat(iInsCharges).toFixed(2)+'</span>';
				nCells[13].innerHTML = 'Total CIF Amount (SGD) : <span class="totalNos">' + parseFloat(iCIFAmount).toFixed(2)+'</span>';
				nCells[14].innerHTML = 'Total GST Amount (SGD) : <span class="totalNos">' + parseFloat(iGSTAmount).toFixed(2)+'</span>';			
			}
		})
		
		//$(".tblIncomingDetailed_header").find('.filter_column').find("input").each(function() {
		//	
		//	var thisTextbox = $.trim($(this).val());
		//	
		//	if(thisTextbox == 'VendorInvNo.' || thisTextbox == 'Comment' || thisTextbox == 'Description' || thisTextbox == 'SU/Entry#.'){
		//		$(this).css("width", "350px");
		//	}
		//	
		//	if(thisTextbox == 'Previous SU# for consignment'){
		//		$(this).css("width", "250px");
		//	}
		//	
		//});
		
	tblIncomingSummary = $('#tblIncomingSummary').dataTable({
			"sDom": '<"H">Tlfr<"F"tip>',
			"oTableTools": {
				"aButtons": [
					{
						"sExtends":    "collection",
						"sButtonText": "Save As",
						"aButtons":    [ "xls" ]
					}
				]
			},
			'bFilter': true,
			'bPaginate': true,
			'bJQueryUI': true,
			'aLengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
			'sPaginationType': 'full_numbers',
			"bScrollCollapse": true,  
			"sScrollX":"100%",    
			"bDestroy": true,
			"aoColumns": [
				{"sTitle":"S/No","sWidth":"80px","sClass":"alignme"},
				{"sTitle":"Customer","sWidth":"110px","sClass":"alignme"},
				{"sTitle":"Shipment #:","sWidth":"110px","sClass":"alignme"},
				{"sTitle":"Shpmt Rcvd Date","sWidth":"130px","sClass":"alignme"},
				{"sTitle":"Vendor/Supplier","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Vendor Inv No.","sWidth":"350px","sClass":"alignmewrap"},
				{"sTitle":"Tax Reference","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Permit No.","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Total Qty","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Vendor Inv Value (USD)","sWidth":"160px","sClass":"alignme"},
				{"sTitle":"Vendor Inv Value (SGD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Frt Charges (SGD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Ins Charges (SGD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"CIF Amount (SGD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"GST Amount (SGD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Job Type","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"HAWB No.","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"MAWB No.","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Dep Date","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Comment","sWidth":"350px","sClass":"alignmewrap"},
				{"sTitle":"Description","sWidth":"500px","sClass":"alignmewrap"},
				{"sTitle":"SU/Entry#.","sWidth":"500px","sClass":"alignmewrap"},
				{"sTitle":"Consignment order (Y/N)","sWidth":"150px","sClass":"alignme"},
				{"sTitle":"Previous SU# for consignment","sWidth":"200px","sClass":"alignme"}
			],
			fnDrawCallback: function( oSettings ) {
				$('#tblIncomingSummary tr td').css('white-space','normal');
			},
			fnFooterCallback: function ( nRow, aaData, iStart, iEnd, aiDisplay ) {
				var iTotalQty = 0;
				var iVendorValUSD = 0;
				var iVendorValSGD = 0;
				var iFrtCharges = 0;
				var iInsCharges = 0;
				var iCIFAmount = 0;
				var iGSTAmount = 0;
				
				$.each(aaData,function(ikey,ivalue){
					iTotalQty += ivalue[8] != 0 ? parseInt(ivalue[8]) : 0;
					iVendorValUSD	+= ivalue[9] != 0 ? parseFloat(ivalue[9]) : 0;
					iVendorValSGD	+= ivalue[10] != 0 ? parseFloat(ivalue[10]) : 0;
					iFrtCharges		+= ivalue[11] != 0 ? parseFloat(ivalue[11]) : 0;
					iInsCharges		+= ivalue[12] != 0 ? parseFloat(ivalue[12]) : 0;
					iCIFAmount		+= ivalue[13] != 0 ? parseFloat(ivalue[13]) : 0;
					iGSTAmount		+= ivalue[14] != 0 ? parseFloat(ivalue[14]) : 0;
				});
				
				var nCells = nRow.getElementsByTagName('td');
				nCells[8].innerHTML  = 'Total Qty : <span class="totalNos">' + iTotalQty+'</span>';
				nCells[9].innerHTML  = 'Vendor Inv Value (USD) : <span class="totalNos">' + parseFloat(iVendorValUSD).toFixed(2)+'</span>';
				nCells[10].innerHTML = 'Vendor Inv Value (SGD) : <span class="totalNos">' + parseFloat(iVendorValSGD).toFixed(2)+'</span>';
				nCells[11].innerHTML = 'Total FrtCharges (SGD) : <span class="totalNos">' + parseFloat(iFrtCharges).toFixed(2)+'</span>';
				nCells[12].innerHTML = 'Total InsCharges (SGD) : <span class="totalNos">' + parseFloat(iInsCharges).toFixed(2)+'</span>';
				nCells[13].innerHTML = 'Total CIF Amount (SGD) : <span class="totalNos">' + parseFloat(iCIFAmount).toFixed(2)+'</span>';
				nCells[14].innerHTML = 'Total GST Amount (SGD) : <span class="totalNos">' + parseFloat(iGSTAmount).toFixed(2)+'</span>';			
			}
		});

	tblOutgoingDetailed = $('#tblOutgoingDetailed').dataTable({
			"sDom": '<"H">Tlfr<"F"tip>',
			"oTableTools": {
				"aButtons": [
					{
						"sExtends":    "collection",
						"sButtonText": "Save As",
						"aButtons":    [ "xls" ]
					}
				]
			},
			'bFilter': true,
			'bSortable' : false,
			//'aaSortingFixed': [[4,'asc'],[2,'asc']],
			'bPaginate': true,
			'bJQueryUI': true,
			'aLengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
			'sPaginationType': 'full_numbers',
			"bScrollCollapse": true,  
			"sScrollX":"100%",    
			"bDestroy": true,
			"aoColumns": [
				{"sTitle":"S/No","sWidth":"80px","sClass":"alignme"},
				{"sTitle":"Customer","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Shipment #","sWidth":"90px","sClass":"alignme"},
				{"sTitle":"Packlist No #","sWidth":"130px","sClass":"alignme"},
				{"sTitle":"Shipment Release Date.","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Shipment No.","sWidth":"200px","sClass":"alignme"},
				{"sTitle":"Vendor Inv No.","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Vendor/Supplier","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Comm Inv No/DN.","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Total Qty","sWidth":"180px","sClass":"alignme"},
				{"sTitle":"Total Qty(Shipment)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Unit Price","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Out Tax Ref","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Permit #","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Exchange Rate","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Dest Port","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Forwarders","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Consignee","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"HAWB No.","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"MAWB No.","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Tpt Mode","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Dep Date","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Delivery From","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Inco Terms","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Job Type","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Comment","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Customer Inv Value(USD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Customer Inv Value(SGD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"GST Amount(SGD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Total Value(HAWB SGD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"OUT SU #","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Description","sWidth":"140px","sClass":"alignme"}
			],
			fnDrawCallback: function( oSettings ) {
				$('#tblOutgoingDetailed tr td').css('white-space','normal');
			},
			fnFooterCallback: function ( nRow, aaData, iStart, iEnd, aiDisplay ) {
				var iTotalQty = 0;
				var iTotalShipQty = 0;
				var iCustInvValUSD = 0;
				var iCustInvValSGD = 0;
				var iGSTAmount = 0;
				
				$.each(aaData,function(ikey,ivalue){
					iTotalQty += ivalue[9] != 0 ? parseInt(ivalue[9]) : 0;
					iTotalShipQty += ivalue[10] != 0 ? parseInt(ivalue[10]) : 0;
					iCustInvValUSD	+= ivalue[26] != 0 ? parseFloat(ivalue[26].replace(",", "")) : 0;
					iCustInvValSGD	+= ivalue[27] != 0 ? parseFloat(ivalue[27].replace(",", "")) : 0;
					iGSTAmount		+= ivalue[28] != 0 ? parseFloat(ivalue[28].replace(",", "")) : 0;
				});
				
				var nCells = nRow.getElementsByTagName('td');
				nCells[9].innerHTML  = 'Total Qty : <span class="totalNos">' + iTotalQty+'</span>';
				nCells[10].innerHTML  = 'Total Qty(Shipment) : <span class="totalNos">' + iTotalShipQty+'</span>';
				nCells[26].innerHTML  = 'Customer Inv Value(USD) : <span class="totalNos">' + parseFloat(iCustInvValUSD).toFixed(2)+'</span>';
				nCells[27].innerHTML = 'Customer Inv Value(SGD) : <span class="totalNos">' + parseFloat(iCustInvValSGD).toFixed(2)+'</span>';
				nCells[28].innerHTML = 'GST Amount(SGD) : <span class="totalNos">' + parseFloat(iGSTAmount).toFixed(2)+'</span>';			
			}
		});
		
	tblOutgoingSummary = $('#tblOutgoingSummary').dataTable({
			"sDom": '<"H">Tlfr<"F"tip>',
			"oTableTools": {
				"aButtons": [
					{
						"sExtends":    "collection",
						"sButtonText": "Save As",
						"aButtons":    [ "xls" ]
					}
				]
			},
			'bFilter': true,
			'bPaginate': true,
			'bJQueryUI': true,
			'aLengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
			'sPaginationType': 'full_numbers',
			"bScrollCollapse": true,  
			"sScrollX":"100%",    
			"bDestroy": true,
			"aoColumns": [
				{"sTitle":"S/No","sWidth":"80px","sClass":"alignme"},
				{"sTitle":"Customer","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Shipment #","sWidth":"90px","sClass":"alignme"},
				{"sTitle":"Shipment Release Date.","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Shipment No.","sWidth":"200px","sClass":"alignme"},
				{"sTitle":"Vendor Inv No.","sWidth":"400px","sClass":"alignmewrap"},
				{"sTitle":"Vendor/Supplier","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Comm Inv No/DN.","sWidth":"400px","sClass":"alignmewrap"},
				{"sTitle":"Total Qty","sWidth":"180px","sClass":"alignme"},
				{"sTitle":"Unit Price","sWidth":"400px","sClass":"alignmewrap"},
				{"sTitle":"Out Tax Ref","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Permit #","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Exchange Rate","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Dest Port","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Forwarders","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Consignee","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"HAWB No.","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"MAWB No.","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Tpt Mode","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Dep Date","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Delivery From","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Inco Terms","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Job Type","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Comment","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Customer Inv Value(USD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Customer Inv Value(SGD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"GST Amount(SGD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Total Value(HAWB SGD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"OUT SU #","sWidth":"400px","sClass":"alignmewrap"},
				{"sTitle":"Description","sWidth":"400px","sClass":"alignmewrap"}
			],
			fnDrawCallback: function( oSettings ) {
				$('#tblOutgoingSummary tr td').css('white-space','normal');
			},
			fnFooterCallback: function ( nRow, aaData, iStart, iEnd, aiDisplay ) {
				var iTotalQty = 0;
				var iCustInvValUSD = 0;
				var iCustInvValSGD = 0;
				var iGSTAmount = 0;
				
				$.each(aaData,function(ikey,ivalue){
					iTotalQty += ivalue[8] != 0 ? parseInt(ivalue[8]) : 0;
					iCustInvValUSD	+= ivalue[24] != 0 ? parseFloat(ivalue[24].replace(",", "")) : 0;
					iCustInvValSGD	+= ivalue[25] != 0 ? parseFloat(ivalue[25].replace(",", "")) : 0;
					iGSTAmount		+= ivalue[26] != 0 ? parseFloat(ivalue[26].replace(",", "")) : 0;
				});
				
				var nCells = nRow.getElementsByTagName('td');
				nCells[8].innerHTML  = 'Total Qty : <span class="totalNos">' + iTotalQty+'</span>';
				nCells[24].innerHTML  = 'Customer Inv Value(USD) : <span class="totalNos">' + parseFloat(iCustInvValUSD).toFixed(2)+'</span>';
				nCells[25].innerHTML = 'Customer Inv Value(SGD) : <span class="totalNos">' + parseFloat(iCustInvValSGD).toFixed(2)+'</span>';
				nCells[26].innerHTML = 'GST Amount(SGD) : <span class="totalNos">' + parseFloat(iGSTAmount).toFixed(2)+'</span>';				
			}
		});

	tblPicklist = $('#tblPicklist').dataTable({
			"sDom": '<"H">Tlfr<"F"tip>',
			"oTableTools": {
				"aButtons": [
					{
						"sExtends":    "collection",
						"sButtonText": "Save As",
						"aButtons":    [ "xls" ]
					}
				]
			},
			'bFilter': true,
			'bSortable' : false,
			//'aaSortingFixed': [[3,'asc'],[2,'asc']],
			'bPaginate': true,
			'bJQueryUI': true,
			'aLengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
			'sPaginationType': 'full_numbers',
			"bScrollCollapse": true,  
			"sScrollX":"100%",    
			"bDestroy": true,
			"aoColumns": [
				{"sTitle":"#","sWidth":"80px","sClass":"alignme"},
				{"sTitle":"Out Reference #","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Shipment Release Date","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Out WMS Reference","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Picklist No.","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"DN No.","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Unit Price (USD)","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Out HAWB No.","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Out Permit No.","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Part No.","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Tax Scheme","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Total Outgoing Qty","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Out Box Entry/SU#","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"In Reference#","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Shpmt Rcvd Date","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"In WMS Reference","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Vendor/Supplier Name","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Vendor Inv No","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"In HAWB No","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"In Permit No.","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Part No.","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Tax Scheme","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Incoming (QTY)","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"In Box Entry/SU#","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Balance (QTY)","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Bal Box Entry/SU#","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Previous SU#","sWidth":"120px","sClass":"alignme"}
			],
			fnDrawCallback: function( oSettings ) {
				$('#tblPicklist tr td').css('white-space','normal');
			},
			fnFooterCallback: function ( nRow, aaData, iStart, iEnd, aiDisplay ) {
				var iTotalOutQty = 0;
				var iTotalInQty = 0;
				var iTotalBalQty = 0;
				
				$.each(aaData,function(ikey,ivalue){
					iTotalOutQty += ivalue[11] != 0 ? parseInt(ivalue[11]) : 0;
					iTotalInQty += ivalue[27] != 0 ? parseInt(ivalue[27]) : 0;
					iTotalBalQty += ivalue[28] != 0 ? parseInt(ivalue[28]) : 0;
				});
				
				var nCells = nRow.getElementsByTagName('td');
				nCells[11].innerHTML  = 'Total Outgoing Qty : <span class="totalNos">' + iTotalOutQty+'</span>';
				nCells[22].innerHTML  = 'Incoming (QTY) : <span class="totalNos">' + iTotalInQty+'</span>';
				nCells[24].innerHTML  = 'Balance (QTY) : <span class="totalNos">' + iTotalBalQty+'</span>';		
			}
		});
		
	tblShipmentSummary = $('#tblShipmentSummary').dataTable({
			"sDom": '<"H">Tlfr<"F"tip>',
			"oTableTools": {
				"aButtons": [
					{
						"sExtends":    "collection",
						"sButtonText": "Save As",
						"aButtons":    [ "xls" ]
					}
				]
			},
			'bFilter': true,
			'bSortable' : false,
			//'aaSortingFixed': [[3,'asc'],[2,'asc']],
			'bPaginate': true,
			'bJQueryUI': true,
			'aLengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
			'sPaginationType': 'full_numbers',
			"bScrollCollapse": true,  
			"sScrollX":"100%",    
			"bDestroy": true,
			"aoColumns": [
				{"sTitle":"#","sWidth":"80px","sClass":"alignme"},
				{"sTitle":"In Reference#","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Shpmt Rcvd Date","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"In WMS Reference","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Vendor / Supplier Name","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Vendor Inv No","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"In HAWB No","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"In Permit No.","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Part No.","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Tax Scheme","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Incoming (QTY)","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"In Box Entry/SU#","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Out Reference #","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Shipment Release Date","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Out WMS Reference","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Picklist No.","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"DN No.","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Unit Price (USD)","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Out HAWB No.","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Out Permit No.","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Part No.","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Tax Scheme","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Total Outgoing Qty","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Out Box Entry/SU#","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Balance (QTY)","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Bal Box Entry/SU#","sWidth":"120px","sClass":"alignme"}
			],
			fnDrawCallback: function( oSettings ) {
				$('#tblShipmentSummary tr td').css('white-space','normal');
			},
			fnFooterCallback: function ( nRow, aaData, iStart, iEnd, aiDisplay ) {
				var iTotalInQty = 0;
				var iTotalOutQty = 0;
				var iTotalBalQty = 0;
				
				$.each(aaData,function(ikey,ivalue){
					iTotalInQty += ivalue[26] != 0 ? parseInt(ivalue[26]) : 0;
					iTotalOutQty += ivalue[22] != 0 ? parseInt(ivalue[22]) : 0;
					iTotalBalQty += ivalue[27] != 0 ? parseInt(ivalue[27]) : 0;
				});
				
				var nCells = nRow.getElementsByTagName('td');
				nCells[10].innerHTML  = 'Incoming (QTY) : <span class="totalNos">' + iTotalInQty+'</span>';
				nCells[22].innerHTML  = 'Total Outgoing Qty : <span class="totalNos">' + iTotalOutQty+'</span>';
				nCells[24].innerHTML  = 'Balance (QTY) : <span class="totalNos">' + iTotalBalQty+'</span>';		
			}
		});

	$("#tabs").tabs({
		select: function(event, ui) {
			var selected = ui.panel.id;
			switch(selected){
				case 'tabs-1':
					var t = setTimeout(function(){$('#tblIncomingDetailed').dataTable().fnAdjustColumnSizing();},1);
				break;				
				case 'tabs-2':
					var t = setTimeout(function(){$('#tblIncomingSummary').dataTable().fnAdjustColumnSizing();},1);
				break;
				case 'tabs-3':
					var t = setTimeout(function(){$('#tblOutgoingDetailed').dataTable().fnAdjustColumnSizing();},1);
				break;
				case 'tabs-4':
					var t = setTimeout(function(){$('#tblOutgoingSummary').dataTable().fnAdjustColumnSizing();},1);
				break;	
				case 'tabs-5':
					var t = setTimeout(function(){$('#tblPicklist').dataTable().fnAdjustColumnSizing();},1);
				case 'tabs-6':
					var t = setTimeout(function(){$('#tblShipmentSummary').dataTable().fnAdjustColumnSizing();},1);
				break;					
			} 
		}
	});	
	jQuery.fn.exists = function(){return this.length>0;}
	jQuery.isJson = function(str) {
	 if (jQuery.trim(str) == '') return false;
	 str = str.replace(/\\./g, '@').replace(/"[^"\\\n\r]*"/g, '');
	 return (/^[,:{}\[\]0-9.\-+Eaeflnr-u \n\r\t]*$/).test(str);
	}
	
	Number.prototype.padLeft = function(base,chr){
	    var  len = (String(base || 10).length - String(this).length)+1;
	    return len > 0? new Array(len).join(chr || '0')+this : this;
	}

	CurrentDate = [(CurrentDate.getMonth() + 1).padLeft(),CurrentDate.getDate().padLeft(),CurrentDate.getFullYear()].join('/');
	$('#startDate').val(CurrentDate);
	$('#endDate').val(CurrentDate);
	
	/*
	var astr = '';
	$.each(tblIncomingDetailed.fnSettings().aoColumns,function(a,b){
		astr = astr + "'" + b.sTitle + "',";
	});
	console.log(astr); */

	function NASort(a, b) {    
	    if (a.innerHTML == 'NA') {
	        return 1;   
	    }
	    else if (b.innerHTML == 'NA') {
	        return -1;   
	    }       
	    return (parseInt(a.innerHTML) > parseInt(b.innerHTML)) ? 1 : -1;
	};

	function ProcessRequest(data,gotoProcess){
		$.ajax({
		 type: 'POST',
		 url: 'modules/gstADIReport/GSTReportClass.php',
		 data: data,
		 success: function(data){
				var CallMeBaby = eval('(' + gotoProcess + ')');
						CallMeBaby(data);
		 },
		 beforeSend: function(){
				$.dimScreen(function() {
					$('#content').fadeIn();
				});
		 },
		 complete: function(){
			 $.dimScreenStop();
		 }
		});
	}
	

	function msgbox(msg,focusField){
		$( "#dialog-msgbox:ui-dialog" ).dialog( "destroy" );
		$( "#dialog-msgbox" ).html(msg);
		$( "#dialog-msgbox" ).attr("title",'GST Report');
		$( "#dialog-msgbox" ).dialog('option', 'position', 'center');
		$( "#dialog-msgbox" ).dialog({
			modal: true,
			height:'auto',
			show:{ 
				effect:"blind", 
				speed:'slow' 
			},
			hide:'blind',
			buttons: {
				Ok: function() {
					$(this).dialog( "close" );
				}
			}
		});
	}
	
	function PrepareForm(obj){
		
		$.each(obj[0],function(a,b){
			//b[0] - wawiindex
			//b[1] - wawialias
			//CurrentAvaiLablePicklistPerCustomer[b[0]['PackListNumber']] = [];
			CustomerList[b[0]] = b[1];
		});
		TaxReferenceList['All'] = ['All'];
		$.each(obj[1],function(a,b){
			TaxReferenceList[a] = [];
			$.each(b,function(c,d){
				$.each(d,function(f,g){
					TaxReferenceList[a].push(g);
				});
			});
		});

		//$('#slcCustomer')
		//   .append($("<option></option>")
		//   .attr("value",'All')
		//   .text('All'));

		$('#slcCustomer')
		   .append($("<option></option>")
		   .attr("value",'ADISGWMS')
		   .text('ADI SG'));

		//$.each(CustomerList,function(a,b){
	    // $('#slcCustomer')
	    //     .append($("<option></option>")
	    //     .attr("value",a)
	    //     .text(b)); 
		//});
		$('#slcCustomer').change();
	}
	
	function generateReport(objAll){
		var obj = new Object(); //Incoming Data
		var obj1= new Object(); //Outgoing Data
		var tmpArray;
		var i = 1;
		
		obj = objAll[0];
		tblIncomingDetailed.fnClearTable();
		tblIncomingSummary.fnClearTable();
			
		if (obj[0] != 'nodata'){
			tmpArray = [];
			$('#tabs > ul').tabs({ selected: 1 });
			$.each(obj[0],function(a,c){
				$.each(c,function(d,b){
					tmpArray.push([i,
								   b['WawiAlias'],
								   b['ShipReference'],
								   b['ArrivalDate'],
								   b['VendorInvNos'],
								   b['suQuantity'],
								   b['InTaxRef'],
								   b['PermitNo'],
								   b['ExchangeRate'],
								   parseFloat(b['InvoiceValue1']).toFixed(2),
								   parseFloat(b['InvoiceValueSGD2']).toFixed(2),
								   parseFloat(b['FreightCharges']).toFixed(2),
								   parseFloat(b['InsCharges']).toFixed(2),
								   parseFloat(b['CIFAmount2']).toFixed(2)
								   ,parseFloat(b['GSTAmount2']).toFixed(2),
								   b['OriginPort'],
								   b['Forwarders'],
								   b['Supplier'],
								   b['HAWB'],
								   b['MAWB'],
								   b['TptMode'],
								   b['ArrivalDate'],
								   b['OMNI'],
								   b['INCOTERM'],
								   b['JobType'],
								   b['Comment'],
								   b['IncomingReference'],
								   b['Description'],
								   b['EntrySU'],
								   b['isConsigned'],
								   b['SUConsignedHistory']]);
					i++;
				});
			});
			tblIncomingDetailed.fnAddData(tmpArray);
		}
		
		if (obj[1] != 'nodata'){
			$('#tabs > ul').tabs({ selected: 2 });
			tmpArray = [];
			i = 1;
			$.each(obj[1],function(a,c){
				$.each(c,function(d,b){
					tmpArray.push([i,
								   b['Customer'],
								   b['ShipReference'],
								   b['ArrivalDate1'],
								   b['Supplier'],
								   b['VendorInvNo2'],
								   b['InTaxRef'],
								   b['PermitNo'],
								   b['ShipmentQty'],
								   parseFloat(b['InvoiceValue']).toFixed(2),
								   parseFloat(b['InvoiceValueSGD']).toFixed(2),
								   parseFloat(b['FreightCharges']).toFixed(2),
								   parseFloat(b['InsCharges']).toFixed(2),
								   parseFloat(b['CIFAmount']).toFixed(2),
								   parseFloat(b['GSTAmount']).toFixed(2),
								   b['JobType'],
								   b['HAWB'],
								   b['MAWB'],
								   b['TptMode'],
								   b['Comments'],
								   b['Description1'],
								   b['EntrySU'],
								   b['isConsigned'],
								   b['SUConsignedHistory']]);
					i++;
				});
			});
			tblIncomingSummary.fnAddData(tmpArray);
		}

		obj1 = objAll[1];
		tblOutgoingDetailed.fnClearTable();
		tblOutgoingSummary.fnClearTable();
		
		if (obj1[0] != 'nodata'){
			tmpArray = [];
			i = 1;
			$.each(obj1[0],function(c,d){
				$.each(d,function(a,b){
					tmpArray.push([i,
								   b['Customer'],
								   b['ShipmentRef'],
								   b['PackListNumber'],
								   b['CargoReleasedDate'],
								   b['ShipmentNumber'],
								   b['CustInvoice'],
								   b['CustSupplier'],
								   b['DN'],
								   b['TotalQty'],
								   b['ShipTotalQty'],
								   b['UnitPrice'],
								   b['OutTaxRef'],
								   b['PermitNo'],
								   b['ExchangeRate'],
								   b['ShippingCountry'],
								   b['Forwarder'],
								   b['ShipTo'],
								   b['HAWB'],
								   b['MAWB'],
								   b['TptMode'],
								   b['DepDate'],
								   b['DeliveryFrom'],
								   b['IncoTerm'],
								   b['JobType'],
								   b['Comment'],
								   b['CustInvValue'],
								   b['InvoiceValueSGD'],
								   b['GSTAmountValue'],
								   b['ShipTotalValueSGD'],
								   b['SUNumbers'],
								   b['MaterialDescriptions']]);
					i++;
				});
			});
			tblOutgoingDetailed.fnAddData(tmpArray);
		}
		
		if (obj1[1] != 'nodata'){
			tmpArray = [];
			i = 1;
			$.each(obj1[1],function(c,d){
				$.each(d,function(a,b){
					tmpArray.push([i,
								   b['Customer'],
								   b['ShipmentRef'],
								   b['CargoReleasedDate'],
								   b['ShipmentNumber'],
								   b['CustInvoice'],
								   b['CustSupplier'],
								   b['DN'],
								   b['TotalQty'],
								   b['UnitPrice'],
								   b['OutTaxRef'],
								   b['PermitNo'],
								   b['ExchangeRate'],
								   b['ShippingCountry'],
								   b['Forwarder'],
								   b['ShipTo'],
								   b['HAWB'],
								   b['MAWB'],
								   b['TptMode'],
								   b['DepDate'],
								   b['DeliveryFrom'],
								   b['IncoTerm'],
								   b['JobType'],
								   b['Comment'],
								   b['CustInvValue'],
								   b['InvoiceValueSGD'],
								   b['GSTAmountValue'],
								   b['ShipTotalValueSGD'],
								   b['SUNumbers'],
								   b['MaterialDescriptions']]);
					
					i++;
				});
			});
			tblOutgoingSummary.fnAddData(tmpArray);
		}

		obj2 = objAll[2];
		tblPicklist.fnClearTable();
		if(obj2 != 'nodata'){
			tmpArray = [];
			i = 1;
			$.each(obj2,function(c,d){
				$.each(d,function(x,v){
					tmpArray.push([i,
								   v['OutRef'],
								   v['ReleasedDate'],
								   v['OutWMSRef'],
								   v['PackListnumber'],
								   v['CustomerReference'],
								   v['UnitPrice'],
								   v['HAWB'],
								   v['OutPermit'],
								   v['OutPart'],
								   v['OutTaxScheme'],
								   v['OutQty'],
								   v['OutEntry'],
								   v['InRef'],
								   v['ReceivedDate'],
								   v['InWMSRef'],
								   v['Supplier'],
								   v['Invoice'],
								   v['AirwayBill'],
								   v['InPermit'],
								   v['InPart'],
								   v['InTaxScheme'],
								   v['InQty'],
								   v['InEntry'],
								   v['SUBal'],
								   v['BalEntry'],
								   v['PreviousEntry'],
								   v['TotalIncomingQty'],
								   v['TotalBalanceQty']]);
						i++;
				});
			});
			tblPicklist.fnAddData(tmpArray);
		}

		obj3 = objAll[3];
		tblShipmentSummary.fnClearTable();
		if(obj3 != 'nodata'){
			tmpArray = [];
			i = 1;
			$.each(obj3,function(c,d){
				$.each(d,function(x,v){
					tmpArray.push([i,
								   v['InRef'],
								   v['ReceivedDate'],
								   v['InWMSRef'],
								   v['Supplier'],
								   v['Invoice'],
								   v['AirwayBill'],
								   v['InPermit'],
								   v['InPart'],
								   v['InTaxScheme'],
								   v['InQty'],
								   v['InEntry'],
								   v['OutRef'],
								   v['ReleasedDate'],
								   v['OutWMSRef'],
								   v['PackListnumber'],
								   v['CustomerReference'],
								   v['UnitPrice'],
								   v['HAWB'],
								   v['OutPermit'],
								   v['OutPart'],
								   v['OutTaxScheme'],
								   v['OutQty'],
								   v['OutEntry'],
								   v['SUBal'],
								   v['BalEntry'],
								   v['TotalIncomingQty'],
								   v['TotalBalanceQty']]);
						i++;
				});
			});
			tblShipmentSummary.fnAddData(tmpArray);
		}
	}

	$('#shipmentReference').keyup(function(event){
	
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if(keycode == '13'){
			$('#entry-show').css({'display':''});
			ProcessRequest('todo=getEntryByReference&shipmentReference='+$('#shipmentReference').val()+'&customer='+$('#slcCustomer').val(),'displayEntry');
		}

	});
	
	function displayEntry(data){
		$('#entryNumber').empty();
		$('#entryNumber').append($('<option>', { 
		        value: 'All',
		        text : 'All'
		    }));
		if(data != 'nodata'){
			$.each(data, function (i, item) {
			
			    $('#entryNumber').append($('<option>', { 
			        value: item.EntryNumber,
			        text : item.EntryNumber
			    }));
			});
		}
		
	}

	$('#slcCustomer').change(function(){
		$('#slcReference').empty();
		if (TaxReferenceList[$('#slcCustomer').val()] != 'No TaxScheme' && TaxReferenceList[$('#slcCustomer').val()].length > 1){
	     $('#slcReference')
	         .append($("<option></option>")
	         .attr("value",'All')
	         .text('All'));
		}
		
		$.each(TaxReferenceList[$('#slcCustomer').val()],function(a,b){
	     $('#slcReference')
	         .append($("<option></option>")
	         .attr("value",b)
	         .text(b));
		});
	});
	
	$('#generateReport').click(function(){
		var check = 'false';
		if ($('#checkAll').is(":checked"))
		{
			check = 'true';
			if($('#slcCustomer').val() != 'All'){
				ProcessRequest({'todo':'generateReport','startDate':$('#startDate').val(),'endDate':$('#endDate').val(),'slcCustomer':$('#slcCustomer').val(),'slcReference':$('#slcReference').val(),'ShipReference':$('#txtSReference').val(),'EntryNumber':$('#txtSEntry').val(),'TaxScheme':$('#txtSTaxScheme').val(),'Incoming':$('#txtSIncoming').val(),'Outgoing':$('#txtSOutgoing').val(),'Balance':$('#txtSBalance').val(),'Search':check},'generateReport');
			}else{
				alert('Please choose customer');
			}
		}
		else{
			ProcessRequest({'todo':'generateReport','startDate':$('#startDate').val(),'endDate':$('#endDate').val(),'slcCustomer':$('#slcCustomer').val(),'slcReference':$('#slcReference').val(),'ShipReference':$('#txtSReference').val(),'EntryNumber':$('#txtSEntry').val(),'TaxScheme':$('#txtSTaxScheme').val(),'Incoming':$('#txtSIncoming').val(),'Outgoing':$('#txtSOutgoing').val(),'Balance':$('#txtSBalance').val(),'Search':check},'generateReport');
		}

		//console.log(check);
		
	});
	
	
	$('#btnSearch').click(function(){
		
		var chkall					= $('#chkall').is(':checked');
		var chkIncomingDetailed		= $('#chkIncomingDetailed').is(':checked');
		var chkIncomingSummary		= $('#chkIncomingSummary').is(':checked');
		var chkOutgoingDetailed		= $('#chkOutgoingDetailed').is(':checked');
		var chkOutgoingSummary		= $('#chkOutgoingSummary').is(':checked');
		var chkShipmentPicklist		= $('#chkShipmentPicklist').is(':checked');
		var chkShipmentSummary		= $('#chkShipmentSummary').is(':checked');
		
		if(!chkall && !chkIncomingDetailed && !chkIncomingSummary && !chkOutgoingDetailed && !chkOutgoingSummary && !chkShipmentPicklist && !chkShipmentSummary){
			msgbox('Please select report!');
			return false;
		}
		
		if($.trim($('#searchInput1').val()) == ''){
			msgbox('Please input Filter!');
			return false;
		}
		
		ProcessRequest({'todo':'generateReport',
						'chkall':chkall,
						'chkIncomingDetailed':chkIncomingDetailed,
						'chkOutgoingDetailed':chkOutgoingDetailed,
						'chkShipmentPicklist':chkShipmentPicklist,
						'chkIncomingSummary':chkIncomingSummary,
						'chkOutgoingSummary':chkOutgoingSummary,
						'chkShipmentSummary':chkShipmentSummary,
						'searchColumn':$('#searchColumn').val(),
						'searchCon':$('#searchCon').val(),
						'searchInput1':$('#searchInput1').val(),
						'searchInput2':$('#searchInput2').val()
		},'generateReport');
		
	});
	
	
	//Date Filter
	$('#searchColumn').change(function(){
		
		$('#searchCon').val('equalto');
		$(".filter-between").hide();
		
		switch($('select[name="searchColumn"] option:selected').attr('class')){
			case 'thisDate':
				$("#searchInput1, #searchInput2").datepicker({		
					dateFormat: 'dd/mm/yy',
					defaultDate: "+1w",
					changeMonth: true,
					onClose: function( selectedDate ) {
						console.log(selectedDate);
						//$("#searchInput2").datepicker("option","minDate",selectedDate );
					}
				});
				$('#searchInput1').val(CurrentDate);
				$('#searchInput2').val(CurrentDate);
			break;
			default:
				$("#searchInput1, #searchInput2").datepicker("destroy").val('');
		}

	})
	
	$('#searchCon').change(function(){
		
		switch($('select[name="searchCon"] option:selected').val()){
			case 'between':
				$(".filter-between").show();
			break;
			default:
				$(".filter-between").hide();
		}

	})
	
	//Date Filter
	
	$('#txtFilterThis').on('keyup change clear',function(){
		tblIncomingDetailed.fnFilter($(this).val(), 4);
	});
	
	$('#exportToExcel').click(function(e){
		
		var tblIncomingDetailedSetting	 = tblIncomingDetailed.fnSettings();
		var tblIncomingDetailedArray = [];
		$.each(tblIncomingDetailedSetting.aiDisplay,function(ikey,arrayKey){
			tblIncomingDetailedArray.push(tblIncomingDetailed.fnGetData()[arrayKey]);
		});
		
		var tblIncomingSummarySetting	 = tblIncomingSummary.fnSettings();
		var tblIncomingSummaryArray = [];
		$.each(tblIncomingSummarySetting.aiDisplay,function(ikey,arrayKey){
			tblIncomingSummaryArray.push(tblIncomingSummary.fnGetData()[arrayKey]);
		});
		
		var tblOutgoingDetailedSetting	 = tblOutgoingDetailed.fnSettings();
		var tblOutgoingDetailedArray = [];
		$.each(tblOutgoingDetailedSetting.aiDisplay,function(ikey,arrayKey){
			tblOutgoingDetailedArray.push(tblOutgoingDetailed.fnGetData()[arrayKey]);
		});
		
		var tblOutgoingSummarySetting	 = tblOutgoingSummary.fnSettings();
		var tblOutgoingSummaryArray = [];
		$.each(tblOutgoingSummarySetting.aiDisplay,function(ikey,arrayKey){
			tblOutgoingSummaryArray.push(tblOutgoingSummary.fnGetData()[arrayKey]);
		});
		
		var tblPicklistSetting	 = tblPicklist.fnSettings();
		var tblPicklistArray = [];
		$.each(tblPicklistSetting.aiDisplay,function(ikey,arrayKey){
			tblPicklistArray.push(tblPicklist.fnGetData()[arrayKey]);
		});
		
		var tblShipmentSummarySetting	 = tblShipmentSummary.fnSettings();
		var tblShipmentSummaryArray = [];
		$.each(tblShipmentSummarySetting.aiDisplay,function(ikey,arrayKey){
			tblShipmentSummaryArray.push(tblShipmentSummary.fnGetData()[arrayKey]);
		});
		
		
		var tblIncomingDetailedCount = tblIncomingDetailedArray.length;
		var tblIncomingSummaryCount  = tblIncomingSummaryArray.length;
		var tblOutgoingDetailedCount = tblOutgoingDetailedArray.length;
		var tblOutgoingSummaryCount	 = tblOutgoingSummaryArray.length;
		var tblPicklistCount	 	 = tblPicklistArray.length;
		var tblShipmentSummaryCount	 = tblShipmentSummaryArray.length;
		
		if (tblIncomingDetailedCount == 0 && tblIncomingSummaryCount == 0 && tblOutgoingDetailedCount == 0 && tblOutgoingSummaryCount == 0 && tblPicklistCount == 0 && tblShipmentSummaryCount == 0){
			msgbox('No data to Export');
		} else {
			$.fileDownload('modules/gstADIReport/ExportToExcel.php', 
								{ 	httpMethod : "POST",
									data: { StartDate : $('#startDate').val(),
									EndDate : $('#endDate').val(),
									TaxRef : $('#slcReference').val(),
									Customer : $('#slcCustomer').val(),
									tblIncomingDetailed : tblIncomingDetailedArray,
									tblIncomingSummary : tblIncomingSummaryArray,
									tblOutgoingDetailed : tblOutgoingDetailedArray,
									tblOutgoingSummary : tblOutgoingSummaryArray,
									tblPicklistCount: tblPicklistArray,
									tblShipmentSummaryCount: tblShipmentSummaryArray
								 }});
		}
		
	});
	
	tblIncomingDetailed.fnDraw();
	tblIncomingSummary.fnDraw();
	tblOutgoingDetailed.fnDraw();
	tblOutgoingSummary.fnDraw();
	tblPicklist.fnDraw();
	tblShipmentSummary.fnDraw();
	
	ProcessRequest({'todo':'PrepareForm'},'PrepareForm');
	
});	
</script>
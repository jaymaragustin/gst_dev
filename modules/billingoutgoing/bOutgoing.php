<?php
if (isset($_POST['todo'])){
	require ('../../include/config.php');
	$BillingOutgoing = new BillingOutgoing($_REQUEST);
	
	if ($_POST['todo'] == 'GetAllCustomer'){
		echo $BillingOutgoing->GetAllCustomer();
	} elseif ($_POST['todo'] == 'GetPicklist'){
		echo $BillingOutgoing->GetPicklist();
	} elseif ($_POST['todo'] == 'CheckPacklistNumber'){
		echo $BillingOutgoing->CheckPacklistNumber();
	} elseif ($_POST['todo'] == 'AddUpdateData'){
		echo $BillingOutgoing->AddUpdateData();
	} elseif ($_POST['todo'] == 'DeleteDatanow'){
		echo $BillingOutgoing->DeleteDatanow();
	}elseif ($_POST['todo'] == 'GetListOfUnattendedRef'){
		echo $BillingOutgoing->GetListOfUnattendedRef(); 
	}elseif ($_POST['todo'] == 'GetListOfAttendedRef'){
		echo $BillingOutgoing->GetListOfAttendedRef(); 
	}
}

class BillingOutgoing{
	var $PostVars;
	
	function __construct($vars){
		$this->PostVars = $vars;
		$serverConn = unserialize(base64_decode(SQL_CONN));
		$this->conn = mssql_connect($serverConn[0],$serverConn[1],$serverConn[2]);
	}
	
	function GetAllCustomer(){
		mssql_select_db("MasterData");
		$sql = mssql_query("select WawiIndex,WawiAlias from WawiCustomer where [status] = 1");
		while($row[]=mssql_fetch_assoc($sql));
		array_pop($row);

		return json_encode($row);
	}
	
	function GetWawiID(){
		mssql_select_db("MasterData");
		$sql = mssql_fetch_assoc(mssql_query("select ID from WawiCustomer where [WawiIndex] = '".$this->PostVars['WawiIndex']."'"));
		
		return $sql['ID'];
	}
	
	function GetWawiDBName(){
		mssql_select_db("MasterData");
		$sql = mssql_fetch_assoc(mssql_query("select DBName from WawiCustomer where [WawiIndex] = '".$this->PostVars['WawiIndex']."'"));
		
		return $sql['DBName'];
	}

	function GetPicklist(){
		
		$DBName = $this->GetWawiDBName();
		
		mssql_select_db("Import");
		echo "execute billingOutgoing '".$this->PostVars['DateFilter']."','".$this->PostVars['WawiIndex']."','$DBName'";
		$sql = mssql_query("execute billingOutgoing '".$this->PostVars['DateFilter']."','".$this->PostVars['WawiIndex']."','$DBName'");
		//$sql = mssql_query("select * from Import_Billing where WawiIndex = '".$this->PostVars['WawiIndex']."'");
		
		if (mssql_num_rows($sql) < 1){
			return "no data";
		} else {
			while($row[]=mssql_fetch_assoc($sql));
			array_pop($row);
	
			return json_encode($row);
		}
	}

	function CheckPacklistNumber(){

		mssql_select_db("MasterData");
		
		$dbname = mssql_fetch_assoc(mssql_query("select DBName from wawicustomer where WawiIndex = '".$this->PostVars['WawiIndex']."'"));
		mssql_select_db($dbname['DBName']);
		$sql = mssql_query("select * from [Packlist] where packlistnumber = '".$this->PostVars['txtpicklist']."'");
		
		if (mssql_num_rows($sql) < 1){
			return "err1";
		} else {
			mssql_select_db("Import");
			$sql = mssql_query("select * from Export_Billing where WawiIndex = '".$this->PostVars['WawiIndex']."' and PackListNo = '".$this->PostVars['txtpicklist']."'");
			
			if (mssql_num_rows($sql) < 1){
				return "no data";
			} else {
				while($row[]=mssql_fetch_assoc($sql));
				array_pop($row);
		
				return json_encode($row);
			}
			
		}
		
	}
	
	function GetListOfUnattendedRef(){		
		mssql_select_db("MasterData");		
		$dbname = mssql_fetch_assoc(mssql_query("select DBName from wawicustomer where WawiIndex = '".$this->PostVars['WawiIndex']."'"));
		mssql_select_db("Import");
		$res = mssql_query("exec billingOutgoingList '".$this->PostVars['datepacked']."','".$this->PostVars['clsCustomerName']."','$dbname[DBName]','UNATTENDED'");
		if(mssql_num_rows($res) > 0){
			while($row[] = mssql_fetch_assoc($res));
				array_pop($row);		
			return json_encode($row);
		}else{
			return 'no data';
		}
	}
	
	function GetListOfAttendedRef(){		
		mssql_select_db("MasterData");		
		$dbname = mssql_fetch_assoc(mssql_query("select DBName from wawicustomer where WawiIndex = '".$this->PostVars['WawiIndex']."'"));
		mssql_select_db("Import");
		
		$res = mssql_query("exec billingOutgoingList '".$this->PostVars['datepacked']."','".$this->PostVars['clsCustomerName']."','$dbname[DBName]','ATTENDED'");
		if(mssql_num_rows($res) > 0){
			while($row[] = mssql_fetch_assoc($res));
				array_pop($row);		
			return json_encode($row); 
		}else{
			return 'no data'; 
		}
	}
	
	function AddUpdateData(){
		$this->PostVars['WawiIndex'] = $this->PostVars['clsCustomerName'];
		$this->PostVars['RefNumber'] = $this->PostVars['txtrefnum'];
		
		foreach ($this->PostVars as $key => $value){
			if(!is_numeric($this->PostVars[$key])){
				$this->PostVars[$key] = $this->mssqlQuotedString($value);
			}
		}
		
		$wawiID = $this->GetWawiID();
		
		if ($this->CheckPacklistNumber() == 'no data'){
			// add new
			mssql_select_db("Import");
			$this->TransactionLog($this->PostVars['WawiIndex'],$wawiID,$this->PostVars['txtpicklist'],$this->PostVars['UserID'],'ADD','N/A','BILLING EXPORT');
			if (mssql_query("Insert into Export_Billing (PackListNo,WawiIndex,WawiID,JobNo,PONo,PaymentTerms,TotalBilling,TaxClass,Remarks,Username,UserUpdated) values ('".$this->PostVars['txtpicklist']."','".$this->PostVars['WawiIndex']."',".$wawiID.",'".$this->PostVars['txtdcjob']."','".$this->PostVars['txtpcno']."','".$this->PostVars['txtlcno']."','".$this->PostVars['txttotalbilling']."','".$this->PostVars['txttaxclass']."','".$this->PostVars['txtremarks']."','".$this->PostVars['UserID']."','".$this->PostVars['UserID']."')")){
				return 'addnewsuccess';
			} else {
				return 'addnewfailed';
			}
		} else {
			// update
			mssql_select_db("Import");
			$this->TransactionLog($this->PostVars['WawiIndex'],$wawiID,$this->PostVars['txtpicklist'],$this->PostVars['UserID'],'UPDATE','N/A','BILLING EXPORT');
			if (mssql_query("Update Export_Billing set JobNo='".$this->PostVars['txtdcjob']."',TaxClass='".$this->PostVars['txttaxclass']."',PONo='".$this->PostVars['txtpcno']."',TotalBilling='".$this->PostVars['txttotalbilling']."',Remarks='".$this->PostVars['txtremarks']."',UserUpdated='".$this->PostVars['UserID']."',DateUpdated=getdate(),PaymentTerms='".$this->PostVars['txtlcno']."' where PackListNo = '".$this->PostVars['txtpicklist']."' and WawiIndex = '".$this->PostVars['WawiIndex']."'")){
				return 'updatesuccess';
			} else {
				return 'updatefailed';
			}
		}
	}
	
	function DeleteDatanow(){
		$this->PostVars['WawiIndex'] = $this->PostVars['clsCustomerName'];
		$this->PostVars['RefNumber'] = $this->PostVars['txtrefnum'];
		
		foreach ($this->PostVars as $key => $value){
			if(!is_numeric($this->PostVars[$key])){
				$this->PostVars[$key] = $this->mssqlQuotedString($value);
			}
		}
		
		$wawiID = $this->GetWawiID();
		
		mssql_select_db("Import");
		mssql_query("BEGIN TRAN");
		if ($this->TransactionLog($this->PostVars['WawiIndex'],$wawiID,$this->PostVars['txtpicklist'],$this->PostVars['UserID'],'DELETE','N/A','BILLING EXPORT')){
			if (mssql_query("delete from Export_Billing where PackListNo = '".$this->PostVars['txtpicklist']."' and WawiIndex = '".$this->PostVars['WawiIndex']."'")){
				mssql_query("COMMIT");
				return 'successdeleted';
			} else {
				mssql_query("ROLLBACK");
				return 'deleted2';
			}
		} else {
			mssql_query("ROLLBACK");
			return 'deleted1';
		}
	}
	
	function mssqlQuotedString($str){
		$str = str_replace("\\", "\\\\", $str);
		$str = str_replace("'", "''", $str);
		
		return $str;
	}
	
	function TransactionLog($WawiIndex,$WawiID,$Reference,$UserID,$TransactionType,$Details,$Module){
		$Details = $this->mssqlQuotedString($Details);
		if (mssql_query("Insert into TransactionLogs (WawiIndex,WawiID,Reference,UserID,TrasactionDate,TransactionType,Details,Module) values ('$WawiIndex',$WawiID,'$Reference','$UserID',GETDATE(),'$TransactionType','$Details','$Module')")){
			return true;
		} else {
			return false;
		}
	}
	
}

?>
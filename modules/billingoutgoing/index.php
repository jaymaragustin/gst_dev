<style type="text/css">
	.globalform input.text {
    width: 190px;
	}
	
	.globalform{
		 font-size:12px;
	}
</style>
<?php 
//error_reporting(E_ALL);
//ini_set("display_errors",1);
session_start();
require ('../../include/referer.checker.php');
require ('../../include/session.checker.php');
?>
<h2><?php include '../../tpl/module_shortcut.php';?>Outgoing</h2> 

<?php if (isset($_SESSION['defaultProgramID']) && $_SESSION['defaultProgramID'] && isset($_SESSION['defaultProgramName']) && $_SESSION['defaultProgramName']){
$wawiid = $_SESSION['defaultProgramID'];
?>
<table id="tblDetails" width="756" border="0" cellpadding="0" cellspacing="0" class="globalform" style="border-spacing:2px; margin: 15px auto;">
  <tr>
    <td><strong>Customer Name:</strong></td>
    <td colspan="4"><select class="select" name="clsCustomerName" id="clsCustomerName"></select></td>
  </tr>
  <tr>
    <td width="21%"><strong>Picklist #:</strong></td>
    <td width="21%"><input class="text" type="text" name="txtpicklist" id="txtpicklist" /></td>
    <td width="17%">&nbsp;</td>
    <td width="21%">&nbsp;</td>
    <td width="20%">&nbsp;</td>
  </tr>
  <tr>
    <td><strong>DC Job No:</strong></td>
    <td><input class="text" type="text" name="txtdcjob" id="txtdcjob" /></td>
    <td>&nbsp;</td>
    <td><strong>Tax Class:</strong></td>
    <td><input class="text" type="text" name="txttaxclass" id="txttaxclass" /></td>
  </tr>
  <tr>
    <td><strong>PO No:</strong></td>
    <td><input class="text" type="text" name="txtpcno" id="txtpcno" /></td>
    <td>&nbsp;</td>
    <td><strong>Payment Term / LC No:</strong></td>
    <td><input class="text" type="text" name="txtlcno" id="txtlcno" /></td>
  </tr>
  <tr>
    <td><strong>Total Billing:</strong></td>
    <td><input class="text" type="text" name="txttotalbilling" id="txttotalbilling" /></td>
    <td>&nbsp;</td>
    <td><strong>Remarks:</strong></td>
    <td><input class="text" type="text" name="txtremarks" id="txtremarks" /></td>
  </tr>
  <tr>
    <td colspan="5" align="right">
    	<input type="button" name="btncancel" id="btncancel" value="Cancel" />&nbsp;&nbsp;&nbsp;
    	<input type="button" name="btnDelete" id="btnDelete" value="Delete" />&nbsp;&nbsp;&nbsp;
    	<input type="button" name="btnAddNew" id="btnAddNew" value="Add New >>" />
   	</td>
  </tr>
</table>
<div id="myTabs">
  <ul>
      <li><a href="#tab1" onclick="javascript:void(0);">Unattended</a></li>
      <li><a href="#tab2" onclick="javascript:void(0);">Attended</a></li>
  </ul>
  <div id="tab1">
	<form class="globalform">
		<label>Select Date : </label>
		<input type="text" class="text date-pick" name="datepacked1" id="datepacked1" value="<?php echo date('m/d/Y');?>" />
	</form>
  	<table class="display" id="tblunattended"></table> 
  </div>
  <div id="tab2">
	<form class="globalform">
		<label>Select Date : </label>
		<input type="text" class="text date-pick" name="datepacked" id="datepacked" value="<?php echo date('m/d/Y');?>" />
	</form>
  	<table class="display" id="tblattended"></table>
  </div>
</div>
<script>
$(function() {
	$( "input:submit, input:button, button, .button").button();
	$("#btncancel").hide();
	$("#btnDelete").hide();
	$("#myTabs").tabs({
		select: function(event, ui) {
			 $('.DTTT_collection').remove();
			var selected = ui.panel.id;
			switch(selected){ 
				case 'tab1':
					ProcessRequest({'todo':'GetListOfUnattendedRef','WawiIndex':$('#clsCustomerName').val(),'clsCustomerName':$('#clsCustomerName').val(),'datepacked':$('#datepacked1').val()},'GetListOfUnattendedRef'); 
				break;				
				case 'tab2':
					ProcessRequest({'todo':'GetListOfAttendedRef','WawiIndex':$('#clsCustomerName').val(),'clsCustomerName':$('#clsCustomerName').val(),'datepacked':$('#datepacked').val()},'GetListOfAttendedRef'); 
				break;						
			}
		}
	});
	$(".date-pick").datepicker({dateFormat:"mm/dd/yy"});
	
	function ProcessRequest(data,gotoProcess){
			var urlString = 'modules/billingoutgoing/bOutgoing.php';

			if (data['todo'] == 'AddUpdateData' || data['todo'] == 'DeleteDatanow'){
				urlString = 'modules/billingoutgoing/bOutgoing.php?' + $("#tblDetails").find('input,select').serialize()
			} 

		 $.extend(data,{UserID: '<?php echo $_SESSION[LoginUserVar]; ?>'});

		 $.ajax({
			 type: 'POST',
			 url: urlString,
			 data: data,
			 success: function(data){
			 	var CallMeBaby = eval('(' + gotoProcess + ')');
						CallMeBaby(data);
			 },
			 beforeSend: function(){
				$.dimScreen(10, 0.2, function() {
					$('#content').fadeIn();
				});
			 },
			 complete: function(){
				 $.dimScreenStop();
			 }
		 });
	}

	function msgboxYesNo(msg,gotoProcess){
		$( "#dialog-msgbox:ui-dialog" ).dialog( "destroy" );
		$( "#dialog-msgbox" ).html(msg);
		$( "#dialog-msgbox" ).attr("title",'Billing Export Module');
		$( "#dialog-msgbox" ).dialog('option', 'position', 'center');
		$( "#dialog-msgbox" ).dialog({
			modal: true,
			height:'auto',
			show:{ 
				effect:"blind", 
				speed:'slow' 
			},
			hide:'blind',
			buttons: {
				Yes: function() {
					$(this).dialog( "close" );
					if (gotoProcess == 'DeleteDatanow'){
						DeleteDatanow();
					}
				},
				No: function() {
					$(this).dialog( "close" );
				}
			}
		});
	}
	
	function msgbox(msg,elem){
		$( "#dialog-msgbox:ui-dialog" ).dialog( "destroy" );
		$( "#dialog-msgbox" ).html(msg);
		$( "#dialog-msgbox" ).attr("title",'GST Outgoing Module');
		$( "#dialog-msgbox" ).dialog('option', 'position', 'center');
		$( "#dialog-msgbox" ).dialog({
			modal: true,
			height:'auto',
			show:{ 
				effect:"blind", 
				speed:'slow' 
			},
			hide:'blind',
			buttons: {
				Ok: function() {
					$(this).dialog( "close" );
					if (elem){
						$('#'+elem).focus().select();
					}
				}
			}
		});
	}
	
	function GetAllCustomer(data){
		var obj = jQuery.parseJSON(data);
		$.each(obj, function(key, value) {
		     $('#clsCustomerName')
		         .append($("<option></option>")
		         .attr("value",value['WawiIndex'])
		         .text(value['WawiAlias']));
		});
		
		//ProcessRequest({'todo':'GetPicklist','WawiIndex':$('#clsCustomerName').val(),'DateFilter':'<?php echo date("m/d/Y"); ?>'},'GetPicklist');
		ProcessRequest({'todo':'GetListOfUnattendedRef','WawiIndex':$('#clsCustomerName').val(),'clsCustomerName':$('#clsCustomerName').val()},'GetListOfUnattendedRef'); 
		ProcessRequest({'todo':'GetListOfAttendedRef','WawiIndex':$('#clsCustomerName').val(),'clsCustomerName':$('#clsCustomerName').val()},'GetListOfAttendedRef'); 
	}

	function GetPicklist(data){
		return false;
		if (data == "no data"){
			msgbox("No available Picklist for <strong>" + $('#clsCustomerName option:selected').text() + "</strong>",'txtpicklist');
		} else {
			var obj = jQuery.parseJSON(data);
			console.log(obj);
		}
		$("#txtpicklist").focus();
	}
	
	function CheckPacklistNumber(data){
		if (data == 'err1'){ // invalid packlist under customer
			msgbox('Sorry. Packlistnumber is invalid under the customer ' + $("#clsCustomerName option:selected").text(),'txtpicklist');
		} else if (data == 'no data'){ // valid packlist but not yet in the table export_billing table
			$("#txtdcjob").selected().focus();
		} else {
			var obj = jQuery.parseJSON(data);
					obj = obj[0];
			$("#txtpicklist").attr('readonly','true');
			$("#btncancel").show();
			$("#btnDelete").show();
			$("#btnAddNew").val('Update');
			$("#txtdcjob").selected().focus();
			
			$("#txtdcjob").val(obj['JobNo']);
			$("#txtpcno").val(obj['PONo']);
			$("#txttotalbilling").val(obj['TotalBilling']);
			$("#txttaxclass").val(obj['TaxClass']);
			$("#txtlcno").val(obj['PaymentTerms']);
			$("#txtremarks").val(obj['Remarks']);
			
		}
	}
	
	function AddUpdateDataResult(data){
		console.log(data);
		if (data == 'err1'){
			msgbox("Sorry cannot find the Packlist #: " + $("#txtpicklist").val() + "<br />Please check your packlistnumber again",'txtpicklist');
		} else if (data == 'addnewsuccess'){
			$("#btncancel").click();
			msgbox('New Export Reference has been added.','txtpicklist');
		} else if(data == 'addnewsuccess'){
			msgbox('Unable to add New Export Reference. <br />Please check your data and try again','txtpicklist');
		} else if(data == 'updatesuccess'){
			$("#btncancel").click();
			msgbox('Updating of Export Reference success','txtpicklist');
		} else if(data == 'updatefailed'){
			msgbox('Unable to Update Export Reference. <br />Please check your data and try again','txtpicklist');
		}
	}
	
	function DeleteDatanow(){
		ProcessRequest({'todo':'DeleteDatanow'},'DeleteDataResult');
	}
	
	function DeleteDataResult(data){
		$("#btncancel").click();
		if (data == 'deleted1'){ 
			msgbox("Sorry unable to delete Picklist No. (err code 1)",'txtpicklist');
		} else if (data == 'deleted2'){
			msgbox("Sorry unable to delete Picklist No. (err code 2)",'txtpicklist');
		} else if (data == 'successdeleted'){
			msgbox("Picklist No. has been deleted",'txtpicklist');
		}
	}
	
	$("#clsCustomerName").change(function(){
		//ProcessRequest({'todo':'GetPicklist','WawiIndex':$('#clsCustomerName').val(),'DateFilter':'<?php echo date("m/d/Y"); ?>'},'GetPicklist');
		ProcessRequest({'todo':'GetListOfUnattendedRef','WawiIndex':$('#clsCustomerName').val(),'clsCustomerName':$('#clsCustomerName').val(),'datepacked':$('#datepacked1').val()},'GetListOfUnattendedRef'); 
		ProcessRequest({'todo':'GetListOfAttendedRef','WawiIndex':$('#clsCustomerName').val(),'clsCustomerName':$('#clsCustomerName').val(),'datepacked':$('#datepacked').val()},'GetListOfAttendedRef'); 
	});
	
	$("#txtpicklist").keypress(function(event){
		if(event.keyCode == 13){
			if($.trim($(this).val()) == ''){
				msgbox('Picklist field is a required field','txtpicklist');
			} else {
				//ProcessRequest({'todo':'CheckPacklistNumber'},'CheckPacklistNumber');
				ProcessRequest({'todo':'CheckPacklistNumber','txtpicklist':$('#txtpicklist').val()},'CheckPacklistNumber');
			}
		}
	});
	
	$("#btncancel").click(function(){ 
			$("#btncancel").hide();
			$("#btnDelete").hide();
			$("#btnAddNew").val('Add New >>');
			$("#txtpicklist").removeAttr('readonly');
			$("#tblDetails").find("input[type=text]").val('');
			$("#txtpicklist").selected().focus();
			
	});
	
	$("#txtdcjob").keypress(function(event){
		if(event.keyCode == 13){
			if($.trim($(this).val()) == ''){
				msgbox('Sorry DC Job No is a required field','txtdcjob');
			} else {
				$("#txtpcno").selected().focus();
			}
		}
	});
	
	$("#txtpcno").keypress(function(event){
		if(event.keyCode == 13){
			if($.trim($(this).val()) == ''){
				msgbox('Sorry PO No is a required field','txtpcno');
			} else {
				$("#txttotalbilling").selected().focus();
			}
		}
	});
	
	$("#txttotalbilling").keypress(function(event){
		if(event.keyCode == 13){
			if($.trim($(this).val()) == ''){
				msgbox('Sorry Total Bill is a required field','txttotalbilling');
			} else {
				$("#txttaxclass").selected().focus();
			}
		}
	});
	
	$("#txttaxclass").keypress(function(event){
		if(event.keyCode == 13){
			if($.trim($(this).val()) == ''){
				msgbox('Sorry Tax Class is a required field','txttaxclass');
			} else {
				$("#txtlcno").selected().focus();
			}
		}
	});
	
	$("#txtlcno").keypress(function(event){
		if(event.keyCode == 13){
			if($.trim($(this).val()) == ''){
				msgbox('Sorry Payment Term / LC No is a required field','txtlcno');
			} else {
				$("#txtremarks").selected().focus();
			}
		}
	});
	
	$("#txtremarks").keypress(function(event){
		if(event.keyCode == 13){
			$("#btnAddNew").click();
		}
	});
	
	$("#btnAddNew").click(function(){
		if($.trim($("#txtpicklist").val()) == ''){
			msgbox("Picklist is a required field. Remarks field not included.",'txtpicklist');
		} else if($.trim($("#txtdcjob").val()) == ''){
			msgbox("Job # is a required field. Remarks field not included.",'txtdcjob');
		} else if($.trim($("#txtpcno").val()) == ''){
			msgbox("PO # is a required field. Remarks field not included.",'txtpcno');
		} else if($.trim($("#txttotalbilling").val()) == ''){
			msgbox("Total Billing is a required field. Remarks field not included.",'txttotalbilling');
		} else if($.trim($("#txttaxclass").val()) == ''){
			msgbox("Tax Class is a required field. Remarks field not included.",'txttaxclass');
		} else if($.trim($("#txtlcno").val()) == ''){
			msgbox("Payment Term is a required field. Remarks field not included.",'txtlcno');
		} else {
			ProcessRequest({'todo':'AddUpdateData'},'AddUpdateDataResult');
		}
	});
	
	$("#btnDelete").click(function(){
		msgboxYesNo("Are you sure you want to delete Picklist #: <strong>" + $("#txtpicklist").val() + "</strong>?",'DeleteDatanow');
	});
	
	$("#txttotalbilling").DecimalMask('9999999.99');
	
	ProcessRequest({'todo':'GetAllCustomer'},'GetAllCustomer');
	
	
	/* GET UNATTENDED DATA */	
	function GetListOfUnattendedRef(data){
		if(data != 'no data'){
			var dataobj = $.parseJSON(data);
			var data = [];
			$.each(dataobj, function (i, e) { 
				var arr = $.map(e,function(value, key){ return value;});
				arr.push('<a href="javascript:void(0)" class="add-value" ref="'+e.PackListnumber+'">Add</a>');   
				data.push(arr); 
			}); 
		}else{ 
			var data = [];
		} 
		 	
		var tblunattended = $('#tblunattended').dataTable( {		
				"sDom": '<"H">Tlfr<"F"tip>',
				"oTableTools": {
					"aButtons": [
						"copy",
						{
							"sExtends":    "collection",
							"sButtonText": "Save As",
							"aButtons":    [ "csv", "xls", "pdf" ]
						}
					] 
				},
				'bFilter': true,
				'bPaginate': true,
				'bJQueryUI': true,
				'aLengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
				'sPaginationType': 'full_numbers',
				"sScrollX": "100%", 
				"bScrollCollapse": true,      
				"bDestroy": true,      
				"aaData" : data,
				"aoColumns": [
					{ "sTitle": "Packed Date" },
					{ "sTitle": "Packllist Number" },
					{ "sTitle": "Action" }
				]
			} ); 
		// $('.add-value').die().live('click',function(){
			// $("#txtpicklist").val($(this).attr('ref'));
			// ProcessRequest({'todo':'CheckPacklistNumber','WawiIndex':$('#clsCustomerName').val(),'txtpicklist':$(this).attr('ref')},'CheckPacklistNumber');
		// });
		
			$('.add-value').live('click',function(){
				alert('live');
			$("#txtpicklist").val($(this).attr('ref'));
			ProcessRequest({'todo':'CheckPacklistNumber','WawiIndex':$('#clsCustomerName').val(),'txtpicklist':$(this).attr('ref')},'CheckPacklistNumber');
		});
	}
	
	/* GET ATTENDED DATA */	
	function GetListOfAttendedRef(data){
		if(data != 'no data'){
			var dataobj = $.parseJSON(data);
			var data = [];
			$.each(dataobj, function (i, e) { 
				var arr = $.map(e,function(value, key){ return value;});
				arr.push('<a href="javascript:void(0)" class="update-value" ref="'+e.PackListnumber+'">Update</a>');   
				data.push(arr); 
			}); 
		}else{ 
			var data = [];
		} 
		 	
		var tblattended = $('#tblattended').dataTable( {		
				"sDom": '<"H">Tlfr<"F"tip>',
				"oTableTools": {
					"aButtons": [
						"copy",
						{
							"sExtends":    "collection",
							"sButtonText": "Save As",
							"aButtons":    [ "csv", "xls", "pdf" ]
						}
					] 
				},
				'bFilter': true,
				'bPaginate': true,
				'bJQueryUI': true,
				'aLengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
				'sPaginationType': 'full_numbers',
				"sScrollX": "100%", 
				"bScrollCollapse": true,      
				"bDestroy": true,      
				"aaData" : data,
				"aoColumns": [
					{ "sTitle": "Packed Date" },
					{ "sTitle": "Packllist Number" },
					{ "sTitle": "Action" }
				]
			} ); 
		$('.update-value').die().live('click',function(){
			$("#txtpicklist").val($(this).attr('ref'));
			ProcessRequest({'todo':'CheckPacklistNumber','WawiIndex':$('#clsCustomerName').val(),'txtpicklist':$(this).attr('ref')},'CheckPacklistNumber');
		});
	}
	
	$('#datepacked1').change(function(){
		ProcessRequest({'todo':'GetListOfUnattendedRef','WawiIndex':$('#clsCustomerName').val(),'clsCustomerName':$('#clsCustomerName').val(),'datepacked':$('#datepacked1').val()},'GetListOfUnattendedRef'); 
	});
	$('#datepacked').change(function(){
		ProcessRequest({'todo':'GetListOfAttendedRef','WawiIndex':$('#clsCustomerName').val(),'clsCustomerName':$('#clsCustomerName').val(),'datepacked':$('#datepacked').val()},'GetListOfAttendedRef'); 
	}); 
	
});
</script>
<?php 
}else{
	?>
	<p><span class="ui-icon ui-icon-alert" style="float:left; margin:3px 4px 4px 0px;"></span>Please select Wawi Customer</td>
	<?php
}?>

<?php
ini_set('memory_limit', '1024M');

if (isset($_POST['todo'])){
	require ('../../include/config.php');
	$GSTReportClass = new GSTReportClass($_REQUEST);
	
	
	header('Content-Type: application/json');
	if ($_POST['todo'] == 'PrepareForm'){
		echo $GSTReportClass->PrepareForm();
	}elseif($_POST['todo'] == 'generateReport'){
		echo $GSTReportClass->generateReport();
	}
}

Class GSTReportClass{
	var $PostVars;
	function __construct($vars){
		$this->PostVars = $vars;
		$serverConn = unserialize(base64_decode(SQL_CONN));
		$this->conn = mssql_connect($serverConn[0],$serverConn[1],$serverConn[2]);
		mssql_select_db("Import");
		
		foreach($this->PostVars as $a => $b){
			$this->PostVars[$a] = $this->mssqlQuotedString($b);
		}
	}
	
	function mssqlQuotedString($str){
		$str = str_replace("\\", "\\\\", $str);
		$str = str_replace("'", "''", $str);
		
		return $str;
	}
	
	function PrepareForm(){
		mssql_select_db('MasterData');
		$sql = mssql_query("select ID,WawiIndex,WawiAlias from WawiCustomer Where [status] = 1 and GST = 1 order by WawiAlias");

		while($row = mssql_fetch_assoc($sql)){
			if (!isset($CustomerList[$row['WawiIndex']])){
				$CustomerList[$row['WawiIndex']] = array();
			}
			
			$CustomerList[$row['WawiIndex']] = array($row['WawiIndex'],$row['WawiAlias']);
			$sqlTaxScheme = mssql_query("select TaxScheme from TaxScheme where [status] = 1 and WawiID = ".$row['ID']);
			if (mssql_num_rows($sqlTaxScheme) > 0){
				while($TaxSchemeList[$row['WawiIndex']][] = mssql_fetch_assoc($sqlTaxScheme)){}
				array_pop($TaxSchemeList[$row['WawiIndex']]);
			} else {
				$TaxSchemeList[$row['WawiIndex']][0] = array('TaxScheme' => 'No TaxScheme');
			}
		}
		
		$data = array();
		array_push($data,$CustomerList);
		array_push($data,$TaxSchemeList);
		
		return json_encode($data);
	}
	
	function generateReport(){
		$data = array();
		array_push($data,$this->GetIncomingReport());
		
		//echo '<pre>'; print_r($data); echo '</pre>';
		
		array_push($data,$this->GetOutgoingReport());
		
		return json_encode($data);
	}
	
	function GetIncomingReport(){
		$sqlString = "SELECT convert(text,VendorInvNo) as NewVendorInvNo,* FROM v_ol_Shipment WHERE ArrivalDate between '".$this->PostVars['startDate']."' and '".$this->PostVars['endDate']."'";
		$sqlString1= "SELECT ShipReference,WawiAlias as Customer,convert(text,VendorInvNo) as VendorInvNo,InTaxRef,InvoiceValue,InvoiceValueSGD,FreightCharges,InsCharges,CIFAmount,GSTAmount,JobType,Comments FROM Import.dbo.v_ol_Shipment WHERE <changeme> Import.dbo.v_ol_Shipment.ArrivalDate between '".$this->PostVars['startDate']."' and '".$this->PostVars['endDate']."'";
		
		if (trim($this->PostVars['slcCustomer']) != 'All'){
			$sqlString.=" and Customer='".trim($this->PostVars['slcCustomer'])."'";
			$sqlString1.=" and Import.dbo.v_ol_Shipment.Customer='".trim($this->PostVars['slcCustomer'])."'";
		}
		
		if (trim($this->PostVars['slcReference']) != 'All' and trim($this->PostVars['slcReference']) != 'No TaxScheme'){
			$sqlString.=" and InTaxRef='".trim($this->PostVars['slcReference'])."'";
			$sqlString1.=" and Import.dbo.v_ol_Shipment.InTaxRef='".trim($this->PostVars['slcReference'])."'";
		}
		
		$sqlString.=" Order By Customer ASC";
		$sqlString1.=" order by Import.dbo.v_ol_Shipment.Customer ASC";
		
		$sql = mssql_query($sqlString);
		
		if (mssql_num_rows($sql) > 0){
			#INCOMING REPORT
			$CurrentWawi = '';
			$sqlIncoming 				= array();
			$sqlIncomingSummary = array();
			
			while($row = mssql_fetch_assoc($sql)){
				if ($CurrentWawi != $row['DBName']){
					$CurrentWawi = $row['DBName'];
					mssql_select_db($CurrentWawi);
				}
				
				$tmpdata = array();
				$tmpdata = $this->AnalyzeSQL(mssql_query("SELECT '".$row['ShipReference']."' as ShipReference,'".$row['WawiAlias']."' as WawiAlias,convert(varchar(10),convert(datetime,'".$row['ArrivalDate']."',101),103) as ArrivalDate,convert(text,'".$row['NewVendorInvNo']."') as VendorInvNo,'".$row['InTaxRef']."' as InTaxRef,'".$row['PermitNo']."' as PermitNo,'".$row['OriginPort']."' as OriginPort,'".$this->mssql_escape($row['Forwarders'])."' as Forwarders,'".$this->mssql_escape($row['Supplier'])."' as Supplier,'".$row['HAWB']."' as HAWB,'".$row['MAWB']."' as MAWB,'".$row['TptMode']."' as TptMode,'JSI' as JSI,'".$row['INCOTERM']."' as INCOTERM,'".$row['JobType']."' as JobType,'".$this->mssql_escape($row['Comments'])."' as Comment,* FROM v_GSTreport2 where ShipReference=".$row['ShipReference']));
				
				if(is_array($tmpdata)){
					array_push($sqlIncoming,$tmpdata);
				}
				
				$sqlSummary = str_replace("<changeme>"," Import.dbo.v_ol_Shipment.ShipReference in (select ShipReference from v_GSTreport2 where ShipReference = ".$row['ShipReference'].") and ",$sqlString1);
				$tmpdata = array();
				$tmpdata = $this->AnalyzeSQL(mssql_query($sqlSummary));
				if(is_array($tmpdata)){
					array_push($sqlIncomingSummary,$tmpdata);
				}
			}
			
			$data = array();
			
			$newIncoming = array();
			foreach($sqlIncoming as $arrTemp){
				array_push($newIncoming,$arrTemp[0]);
			}
			
			$newIncomingSummary = array();
			foreach($sqlIncomingSummary as $arrTemp){
				array_push($newIncomingSummary,$arrTemp[0]);
			}

			$sorted = $this->array_orderby($newIncoming, 'ArrivalDate', SORT_ASC, 'ShipReference', SORT_ASC);
			array_push($data,array($sorted));

			unset($sorted);
			$sorted = $this->array_orderby($newIncomingSummary, 'ShipReference', SORT_ASC);
			array_push($data,array($sorted));
			
			return $data;
		} else {
			return 'nodata';
		}
	}
	
	function GetOutgoingReport(){
		$sqlString = "SELECT convert(varchar,DepDate,103) as DepDate1,convert(varchar,convert(datetime,ShipDate),103) as ShipDate1,convert(text,CustInvNo) as 'CustInvNo1',* FROM <changeme> WHERE WawiIndex = '<changWawiIndex>' and convert(datetime,ShipDate) between '".$this->PostVars['startDate']."' and '".$this->PostVars['endDate']."'";
		$sqlString2= "SELECT convert(text,CustInvNo) as 'CustInvNo1',* FROM <changeme> WHERE WawiIndex = '<changWawiIndex>' and convert(datetime,ShipDate) between '".$this->PostVars['startDate']."' and '".$this->PostVars['endDate']."'";

		if (trim($this->PostVars['slcReference']) != 'All' and trim($this->PostVars['slcReference']) != 'No TaxScheme'){
			$sqlString.=" and taxscheme='".trim($this->PostVars['slcReference'])."'";
			$sqlString2.=" and taxscheme='".trim($this->PostVars['slcReference'])."'";
		}
		
		mssql_select_db('MasterData');
		if (trim($this->PostVars['slcCustomer']) == 'All'){
			$sql = mssql_query("select DBName,WawiIndex,WawiAlias from WawiCustomer Where [status] = 1 and GST = 1");
		}	else {
			$sql = mssql_query("select DBName,WawiIndex,WawiAlias from WawiCustomer Where [status] = 1 and GST = 1 and WawiIndex = '".trim($this->PostVars['slcCustomer'])."'");
		}
		
		$tempOutGoingDetail = array();
		$tempOutGoingSummary= array();
		$CurrentWawi				= '';

		while($rowMasterData = mssql_fetch_assoc($sql)){
			$CurrentWawi = $rowMasterData['DBName'];
			$CurrentWawiIndex = $rowMasterData['WawiIndex'];
			$CurrentWawiAlias = $rowMasterData['WawiAlias'];
			mssql_select_db($CurrentWawi);

			$sqlData = str_replace("<changeme>","v_GSTreportOut2New",$sqlString);
			$sqlData = str_replace("<changWawiIndex>",$CurrentWawiIndex,$sqlData);
			$sqlData .= " order by WawiIndex asc, ShipDate asc, OutShipReference asc, PacklistNumber asc";

			$tmpdata = array();
			//$tmpdata = $this->AnalyzeSQL(mssql_query($sqlData));
			
			$sqlres = mssql_query($sqlData);
			if (mssql_num_rows($sqlres) > 0){
				while($tmprow = mssql_fetch_assoc($sqlres)){
					$tmprow['WawiIndex']=$CurrentWawiAlias;
					$row[] = $tmprow;
				}
			} else {
				$row = 'nodata';
			}
			
			$tmpdata = $row;
			unset($row);
			
			if(is_array($tmpdata)){
				array_push($tempOutGoingDetail,$tmpdata);
			}
			
			$sqlData2 = str_replace("<changeme>","v_GSTreportOut2NewSummary",$sqlString2);
			$sqlData2 = str_replace("<changWawiIndex>",$CurrentWawiIndex,$sqlData2);
			$sqlData2 .= " order by WawiIndex asc, ShipDate asc, OutShipReference asc, PacklistNumber asc";
			
			$tmpdata = array();
			//$tmpdata = $this->AnalyzeSQL(mssql_query($sqlData2));
			
			$sqlres2 = mssql_query($sqlData2);
			if (mssql_num_rows($sqlres2) > 0){
				while($tmprow2 = mssql_fetch_assoc($sqlres2)){
					$tmprow2['WawiIndex']=$CurrentWawiAlias;
					$row2[] = $tmprow2;
				}
			} else {
				$row2 = 'nodata';
			}
			
			$tmpdata = $row2;
			unset($row2);
			
			if(is_array($tmpdata)){
				array_push($tempOutGoingSummary,$tmpdata);
			}
		}
		
		$data = array();
		if (trim($this->PostVars['slcCustomer']) == 'All'){
			
			//for ($i=0;$i<count($tempOutGoingDetail);$i++){
				//$sorted = $this->array_orderby($tempOutGoingDetail[$i],'ShipDate1', SORT_ASC, 'OutShipReference', SORT_ASC);
				//unset($sorted);
			//}
			//$sorted = $this->array_orderby($tempOutGoingDetail[0], 'ShipDate1', SORT_ASC, 'OutShipReference', SORT_ASC);
			array_push($data,$tempOutGoingDetail);
			//unset($sorted);
			
			//for ($j=0;$j<count($tempOutGoingSummary);$j++){
				//$sorted = $this->array_orderby($tempOutGoingSummary[$j],'WawiIndex', SORT_ASC, 'OutShipReference', SORT_ASC);
				//unset($sorted);
			//}
			array_push($data,$tempOutGoingSummary);
		} else {
			//$sorted = $this->array_orderby($tempOutGoingDetail[0], 'ShipDate1', SORT_ASC, 'OutShipReference', SORT_ASC);
			array_push($data,array($tempOutGoingDetail[0]));
			//unset($sorted);
			
			
			//$sorted = $this->array_orderby($tempOutGoingSummary[0], 'OutShipReference', SORT_ASC);
			array_push($data,array($tempOutGoingSummary[0]));
		}
		
		if (!is_array($tempOutGoingDetail[0])){
			return 'nodata';
		}
		return $data;
	}
	
	function array_orderby(){
	    $args = func_get_args();
	    $data = array_shift($args);
	    foreach ($args as $n => $field) {
	        if (is_string($field)) {
	            $tmp = array();
	            foreach ($data as $key => $row)
	                $tmp[$key] = $row[$field];
	            $args[$n] = $tmp;
	            }
	    }
	    $args[] = &$data;
	    call_user_func_array('array_multisort', $args);
	    return array_pop($args);
	}
	
	function mssql_escape($str) {
    return str_replace("'", "''", $str);
	}
	
	function AnalyzeSQL($sql){
		if (mssql_num_rows($sql) > 0){
			while($row[] = mssql_fetch_assoc($sql)){}
			array_pop($row);
		} else {
			$row = 'nodata';
		}
		
		return $row;
	}
	
}

?>
<?php
ini_set('memory_limit', '1024M');

if (isset($_POST['todo'])){
	require ('../../include/config.php');
	$GSTReportClass = new GSTReportClass($_REQUEST);
	
	
	header('Content-Type: application/json');
	if ($_POST['todo'] == 'PrepareForm'){
		echo $GSTReportClass->PrepareForm();
	}elseif($_POST['todo'] == 'generateReport'){
		echo $GSTReportClass->generateReport();
	}elseif($_POST['todo'] == 'getEntryByReference'){
		echo $GSTReportClass->getEntryByReference();
	}
}

Class GSTReportClass{
	var $PostVars;
	function __construct($vars){
		$this->PostVars = $vars;
		$serverConn = unserialize(base64_decode(SQL_CONN));
		$this->conn = mssql_connect($serverConn[0],$serverConn[1],$serverConn[2]);
		mssql_select_db("Import");
		
		foreach($this->PostVars as $a => $b){
			$this->PostVars[$a] = $this->mssqlQuotedString($b);
		}
	}
	
	function mssqlQuotedString($str){
		$str = str_replace("\\", "\\\\", $str);
		$str = str_replace("'", "''", $str);
		
		return $str;
	}
	
	function PrepareForm(){
		mssql_select_db('MasterData');
		$sql = mssql_query("select ID,WawiIndex,WawiAlias from WawiCustomer Where [status] = 1 and GST = 1 order by WawiAlias");

		while($row = mssql_fetch_assoc($sql)){
			if (!isset($CustomerList[$row['WawiIndex']])){
				$CustomerList[$row['WawiIndex']] = array();
			}
			
			if($row['ID'] == 215){
				$row['WawiAlias'] = 'ADI SG';
			}
			$customer = ($row['WawiIndex'] == 'ADIDGTWMS' ? 'ADISGWMS' : $row['WawiIndex']);
			$CustomerList[$row['WawiIndex']] = array($customer,$row['WawiAlias']);
			$sqlTaxScheme = mssql_query("select TaxScheme from TaxScheme where [status] = 1 and WawiID = ".$row['ID']);
			if (mssql_num_rows($sqlTaxScheme) > 0){
				while($TaxSchemeList[$customer][] = mssql_fetch_assoc($sqlTaxScheme)){}
				array_pop($TaxSchemeList[$customer]);
			} else {
				$TaxSchemeList[$customer][0] = array('TaxScheme' => 'No TaxScheme');
			}
		}
		
		$data = array();
		array_push($data,$CustomerList);
		array_push($data,$TaxSchemeList);
		
		return json_encode($data);
	}
	
	function generateReport(){
		$data = array();
		array_push($data,$this->GetIncomingReport());
		
		//echo '<pre>'; print_r($data); echo '</pre>';
		
		array_push($data,$this->GetOutgoingReport());

		array_push($data,$this->getpicklistByReference());

		array_push($data,$this->shipmentSummary());
		
		return json_encode($data);
	}
	
	function GetIncomingReport(){
		if($this->PostVars['Search'] == 'true'){
			$sqlString = "SELECT convert(text,VendorInvNo) as NewVendorInvNo,* FROM v_ol_Shipment";

			$sqlString1= "SELECT ShipReference,WawiAlias as Customer,convert(text,VendorInvNo) as VendorInvNo,<changeme1>,<changeme2>,* FROM Import.dbo.v_ol_Shipment WHERE <changeme> ";
		}else{
			$sqlString = "SELECT convert(text,VendorInvNo) as NewVendorInvNo,* FROM v_ol_Shipment WHERE ArrivalDate between '".$this->PostVars['startDate']."' and '".$this->PostVars['endDate']."'";
			//$sqlString1= "SELECT ShipReference,WawiAlias as Customer,convert(text,VendorInvNo) as VendorInvNos,<changeme1>,<changeme2>,* FROM Import.dbo.v_ol_Shipment WHERE <changeme> Import.dbo.v_ol_Shipment.ArrivalDate between '".$this->PostVars['startDate']."' and '".$this->PostVars['endDate']."'";
			$sqlString1= "SELECT ShipReference,WawiAlias as Customer,convert(text,VendorInvNo) as VendorInvNo,<changeme1>,<changeme2>,* FROM Import.dbo.v_ol_Shipment WHERE <changeme> Import.dbo.v_ol_Shipment.ArrivalDate between '".$this->PostVars['startDate']."' and '".$this->PostVars['endDate']."'";
		}
		
		
		if (trim($this->PostVars['slcCustomer']) != 'All' && $this->PostVars['Search'] == 'true'){
			$sqlString.=" WHERE Customer='".trim($this->PostVars['slcCustomer'])."'";
			$sqlString1.=" Import.dbo.v_ol_Shipment.Customer='".trim($this->PostVars['slcCustomer'])."'";
		}else if (trim($this->PostVars['slcCustomer']) != 'All'){
			$sqlString.=" and Customer='".trim($this->PostVars['slcCustomer'])."'";
			$sqlString1.=" and Import.dbo.v_ol_Shipment.Customer='".trim($this->PostVars['slcCustomer'])."'";
		}
		
		if (trim($this->PostVars['slcReference']) != 'All' and trim($this->PostVars['slcReference']) != 'No TaxScheme'){
			$sqlString.=" and InTaxRef='".trim($this->PostVars['slcReference'])."'";
			$sqlString1.=" and Import.dbo.v_ol_Shipment.InTaxRef='".trim($this->PostVars['slcReference'])."'";
		}

		
		$sqlString.=" Order By Customer ASC";
		$sqlString1.=" order by Import.dbo.v_ol_Shipment.Customer ASC";
		
		$sql = mssql_query($sqlString);
		if (mssql_num_rows($sql) > 0){
			#INCOMING REPORT
			$CurrentWawi = '';
			$sqlIncoming 				= array();
			$sqlIncomingSummary = array();
			$t = array();
			while($row = mssql_fetch_assoc($sql)){
				if ($CurrentWawi != $row['DBName']){
					$CurrentWawi = $row['DBName'];
					mssql_select_db($CurrentWawi);
				}


				$tmpdata = array();
				$InvoiceBreakDown = $this->AnalyzeSQL(mssql_query("SELECT Invoice FROM v_GSTreport2 WHERE ShipReference=".$row['ShipReference']));
				
				$suEntry = '';
				foreach ($InvoiceBreakDown as $key => $v) {
					$whereSU = $this->AnalyzeSQL(mssql_query("SELECT [dbo].fn_InvoiceBreakDown('". $v['Invoice'] ."') as EntryNumber"));
					foreach($whereSU as $whereK => $whereV){
						$whereEntry = $whereV["EntryNumber"];
						$tmpdata = $this->AnalyzeSQL(mssql_query("SELECT (SELECT SUM(COALESCE (dbo.Entry.Quantity, 0)) FROM dbo.Entry WITH (NOLOCK) WHERE EntryNumber IN ($whereEntry)) as suQuantity,'".$row['ShipReference']."' as ShipReference,'".$row['WawiAlias']."' as WawiAlias,convert(varchar(10),convert(datetime,'".$row['ArrivalDate']."',101),103) as ArrivalDate,convert(text,'".$row['NewVendorInvNo']."') as VendorInvNos,'".$row['InTaxRef']."' as InTaxRef,'".$row['PermitNo']."' as PermitNo,'".$row['OriginPort']."' as OriginPort,'".$this->mssql_escape($row['Forwarders'])."' as Forwarders,'".$this->mssql_escape($row['Supplier'])."' as Supplier,'".$row['HAWB']."' as HAWB,'".$row['MAWB']."' as MAWB,'".$row['TptMode']."' as TptMode,'JSI' as JSI,'".$row['INCOTERM']."' as INCOTERM,'".$row['JobType']."' as JobType,'".$this->mssql_escape($row['Comments'])."' as Comment,'".$this->mssql_escape($row['Description'])."' as Description,'".$this->mssql_escape($row['IncomingReference'])."' as IncomingReference,'".$this->mssql_escape($row['ReceiptDate'])."' as ReceiptDate,'".$v['Invoice']."' as VendorInvNo,[dbo].fn_InvoiceBreakDown(".(is_numeric($v['Invoice']) ? $v['Invoice'] : 0 ).") as EntrySU,* FROM v_GSTreport2 where ShipReference=".$row['ShipReference']));
						//var_dump("SELECT (SELECT SUM(COALESCE (dbo.Entry.Quantity, 0)) FROM dbo.Entry WHERE EntryNumber IN ($whereEntry)) as suQuantity,'".$row['ShipReference']."' as ShipReference,'".$row['WawiAlias']."' as WawiAlias,convert(varchar(10),convert(datetime,'".$row['ArrivalDate']."',101),103) as ArrivalDate,convert(text,'".$row['NewVendorInvNo']."') as VendorInvNos,'".$row['InTaxRef']."' as InTaxRef,'".$row['PermitNo']."' as PermitNo,'".$row['OriginPort']."' as OriginPort,'".$this->mssql_escape($row['Forwarders'])."' as Forwarders,'".$this->mssql_escape($row['Supplier'])."' as Supplier,'".$row['HAWB']."' as HAWB,'".$row['MAWB']."' as MAWB,'".$row['TptMode']."' as TptMode,'JSI' as JSI,'".$row['INCOTERM']."' as INCOTERM,'".$row['JobType']."' as JobType,'".$this->mssql_escape($row['Comments'])."' as Comment,'".$this->mssql_escape($row['Description'])."' as Description,'".$this->mssql_escape($row['IncomingReference'])."' as IncomingReference,'".$this->mssql_escape($row['ReceiptDate'])."' as ReceiptDate,'".$v['Invoice']."' as VendorInvNo,[dbo].fn_InvoiceBreakDown(".(is_numeric($v['Invoice']) ? $v['Invoice'] : 0 ).") as EntrySU,* FROM v_GSTreport2 where ShipReference=".$row['ShipReference']);		
						if(is_array($tmpdata)){
							array_push($sqlIncoming,$tmpdata);
						}
					}
					

					$suEntry.= $sqlIncoming[0]['EntrySU'].',';
				}

				$suEntry = substr($suEntry, 0,-1);
				$sqlSummary = str_replace("<changeme1>","'".$row['VendorInvNo']."' as VendorInvNo2",$sqlString1);
				$sqlSummary = str_replace("<changeme2>","'".$suEntry."' as EntrySU",$sqlSummary);
				
				$sqlSummary = str_replace("<changeme>"," Import.dbo.v_ol_Shipment.ShipReference in (select ShipReference from v_GSTreport2 where ShipReference = ".$row['ShipReference'].") and ",$sqlSummary);
				//var_dump($sqlSummary);
				$tmpdata = array();
				$tmpdata = $this->AnalyzeSQL(mssql_query($sqlSummary));
				if(is_array($tmpdata)){
					array_push($sqlIncomingSummary,$tmpdata);
				}
			}
			
			$data = array();
			
			$newIncoming = array();
			foreach($sqlIncoming as $arrTemp){
				array_push($newIncoming,$arrTemp[0]);
			}
			
			$newIncomingSummary = array();
			foreach($sqlIncomingSummary as $arrTemp){
				array_push($newIncomingSummary,$arrTemp[0]);
			}

			$sorted = $this->array_orderby($newIncoming, 'ArrivalDate', SORT_ASC, 'ShipReference', SORT_ASC);
			array_push($data,array($sorted));

			unset($sorted);
			$sorted = $this->array_orderby($newIncomingSummary, 'ShipReference', SORT_ASC);
			array_push($data,array($sorted));
			
			return $data;
		} else {
			return 'nodata';
		}
	}

	function getpicklistByReference(){
		mssql_select_db('Import');
		$db = "SELECT * FROM v_ol_Shipment";
		if(isset($this->PostVars['slcCustomer']) && !empty($this->PostVars['slcCustomer']) & $this->PostVars['slcCustomer'] != 'All'){
			$db.=" WHERE Customer ='".$this->PostVars['slcCustomer']."'";

			$where = array();

			if(!empty($this->PostVars['slcCustomer'])){
				array_push($where,array('WawiIndex'=>$this->PostVars['slcCustomer']) );
			}
			if($this->PostVars['slcReference'] != 'All'){
				array_push($where,array('InTaxRef'=>$this->PostVars['slcReference']) );
				//$where .= " WHERE InTaxRef = '".$this->PostVars['slcReference']."'";
			}
		}
		$result = mssql_query($db);
		$CurrentWawi = '';
		$data = 'nodata';

		$clause = '';
		if(count($where) > 0){
			foreach ($where as $key => $value) {
				foreach ($value as $k => $v) {
					if($clause != ''){
						$clause .= ' AND '.$k.'=\''.$v.'\'';
					}else{
						$clause .= 'WHERE '.$k.'=\''.$v.'\'';
					}
				}
				
			}
		}

		$summary = array();
		$i = 1;
		while ($itemRow = mssql_fetch_assoc($result)) {
			if ($CurrentWawi != $itemRow['DBName']){
				$CurrentWawi = $itemRow['DBName'];
				// var_dump($CurrentWawi." ".$i++);
				mssql_select_db($CurrentWawi);
				array_push($summary,$this->AnalyzeSQL(mssql_query("SELECT * FROM v_GSTShipPacklist ".$clause)));
			}

			// if($this->PostVars['slcReference'] != 'All'){
			// 	$where .= " WHERE InTaxRef = '".$this->PostVars['slcReference']."'";
			// }
			$data = $summary;
		}
		//var_dump($data);
		return $data;
	}

	function shipmentSummary(){
		mssql_select_db('Import');
		$db = "SELECT * FROM v_ol_Shipment";
		if(isset($this->PostVars['slcCustomer']) && !empty($this->PostVars['slcCustomer']) & $this->PostVars['slcCustomer'] != 'All'){
			$db.=" WHERE Customer ='".$this->PostVars['slcCustomer']."'";

			$where = array();
			
			if(!empty($this->PostVars['ShipReference'])){
				$item = array('ShipReference'=>$this->PostVars['ShipReference']);
				array_push($where, $item );
			}
			if(!empty($this->PostVars['slcCustomer'])){
				array_push($where,array('WawiIndex'=>$this->PostVars['slcCustomer']) );
			}
			if(!empty($this->PostVars['EntryNumber'])){
				array_push($where, array('EntryNumber'=>$this->PostVars['EntryNumber']));
			}
			if(!empty($this->PostVars['TaxScheme'])){
				array_push($where, array('InTaxRef'=>$this->PostVars['TaxScheme']));
			}
			if(!empty($this->PostVars['Incoming'])){
				array_push($where, array('TotalIncoming'=>$this->PostVars['Incoming']));
			}
			if(!empty($this->PostVars['Outgoing'])){
				array_push($where, array('TotalOutgoing'=>$this->PostVars['Outgoing']));
			}
			if(!empty($this->PostVars['Balance'])){
				array_push($where, array('RemainingBalance'=>$this->PostVars['Balance']));
			}
		}
		$result = mssql_query($db);
		$CurrentWawi = '';
		$data = 'nodata';

		$clause = '';
		if(count($where) > 0){
			foreach ($where as $key => $value) {
				foreach ($value as $k => $v) {
					if($clause != ''){
						$clause .= ' AND '.$k.'=\''.$v.'\'';
					}else{
						$clause .= 'WHERE '.$k.'=\''.$v.'\'';
					}
				}
				
			}
		}
		$summary = array();
		while ($itemRow = mssql_fetch_assoc($result)) {
			if ($CurrentWawi != $itemRow['DBName']){
				$CurrentWawi = $itemRow['DBName'];
				//var_dump($CurrentWawi);
				mssql_select_db($CurrentWawi);
				array_push($summary,$this->AnalyzeSQL(mssql_query("SELECT * FROM v_GSTShipmentSummary ".$clause)));
			}
			
			$data = $summary;
		}

		return $data;
	}
	
	function GetOutgoingReport(){
		if($this->PostVars['Search'] == 'true'){
			$sqlString = "SELECT convert(varchar,DepDate,103) as DepDate1,convert(varchar,convert(datetime,ShipDate),103) as ShipDate1,convert(text,CustInvNo) as 'CustInvNo1',* FROM <changeme> WHERE WawiIndex = '<changWawiIndex>' ";
			$sqlString2= "SELECT convert(text,CustInvNo) as 'CustInvNo1',[dbo].fn_InvoiceBreakDown2(PacklistNumber) as Invoice,* FROM <changeme> WHERE WawiIndex = '<changWawiIndex>' ";
		}else{
			$sqlString = "SELECT convert(varchar,DepDate,103) as DepDate1,convert(varchar,convert(datetime,ShipDate),103) as ShipDate1,convert(text,CustInvNo) as 'CustInvNo1',* FROM <changeme> WHERE WawiIndex = '<changWawiIndex>' and convert(datetime,ShipDate) between '".$this->PostVars['startDate']."' and '".$this->PostVars['endDate']."'";
			$sqlString2= "SELECT convert(text,CustInvNo) as 'CustInvNo1',[dbo].fn_InvoiceBreakDown2(PacklistNumber) as Invoice,* FROM <changeme> WHERE WawiIndex = '<changWawiIndex>' and convert(datetime,ShipDate) between '".$this->PostVars['startDate']."' and '".$this->PostVars['endDate']."'";
		}
		if (trim($this->PostVars['slcReference']) != 'All' and trim($this->PostVars['slcReference']) != 'No TaxScheme'){
			$sqlString.=" and taxscheme='".trim($this->PostVars['slcReference'])."'";
			$sqlString2.=" and taxscheme='".trim($this->PostVars['slcReference'])."'";
		}
		
		mssql_select_db('MasterData');
		if (trim($this->PostVars['slcCustomer']) == 'All'){
			$sql = mssql_query("select DBName,WawiIndex,WawiAlias from WawiCustomer Where [status] = 1 and GST = 1");
		}	else {
			$sql = mssql_query("select DBName,WawiIndex,WawiAlias from WawiCustomer Where [status] = 1 and GST = 1 and WawiIndex = '".trim($this->PostVars['slcCustomer'])."'");
		}
		
		$tempOutGoingDetail = array();
		$tempOutGoingSummary= array();
		$CurrentWawi				= '';

		while($rowMasterData = mssql_fetch_assoc($sql)){
			$CurrentWawi = $rowMasterData['DBName'];
			$CurrentWawiIndex = $rowMasterData['WawiIndex'];
			$CurrentWawiAlias = $rowMasterData['WawiAlias'];
			mssql_select_db($CurrentWawi);

			$sqlData = str_replace("<changeme>","v_GSTreportOut2New",$sqlString);
			$sqlData = str_replace("<changWawiIndex>",$CurrentWawiIndex,$sqlData);
			$sqlData .= " order by WawiIndex asc, ShipDate asc, OutShipReference asc, PacklistNumber asc";

			$tmpdata = array();
			//$tmpdata = $this->AnalyzeSQL(mssql_query($sqlData));
			
			$sqlres = mssql_query($sqlData);
			if (mssql_num_rows($sqlres) > 0){
				while($tmprow = mssql_fetch_assoc($sqlres)){
					$tmprow['WawiIndex']=$CurrentWawiAlias;
					$row[] = $tmprow;

					$tmp = mssql_query("SELECT Invoice,SUM(Quantity) as Quantity FROM v_GSTInvoices WHERE PackListNumber = ".$tmprow['PacklistNumber']." group by Invoice");

					while($items = mssql_fetch_assoc($tmp)){
						$rows[] = $this->AnalyzeSQL(mssql_query("SELECT '".$tmprow[	'TaxScheme']."' as TaxScheme,'".$tmprow['PermitNo']."' as PermitNo,'".$tmprow['ExchangeRate']."' as ExchangeRate,'".$tmprow['CustInvValue']."' as CustInvValue,'".$tmprow['CustInvValueUSD']."' as CustInvValueUSD,'".$tmprow['Destination']."' as Destination,'".$tmprow['Forwarders']."' as Forwarders,'".$tmprow['HAWB']."' as HAWB,'".$tmprow['MAWB']."' as MAWB,'".$tmprow['Flight']."' as Flight,'".$tmprow['DepDate']."' as DepDate,'".$tmprow['JobType']."' as JobType,'".$items['Quantity']."' as TotalQty,'".$tmprow['CustomerName1']."' as CustomerName1,'".$tmprow['IncoTerms']."' as IncoTerms,'".$tmprow['Remarks']."' as Remarks,'".$tmprow['InvoiceValueSGD']."' as InvoiceValueSGD,'".$tmprow['GstAmount']."' as GstAmount,'".$tmprow['Supplier']."' as Supplier,'".$tmprow['ShipDate']."' as ShipDate,'".$tmprow['PacklistNumber']."' as PacklistNumber,'".$tmprow['OutShipReference']."' as OutShipReference,'".$tmprow['CustInvNo1']."' as CustInvNo1,'".$items['Invoice']."' as Invoice, '".$tmprow['WawiIndex']."' as WawiIndex,'".$tmprow['DepDate1']."' as DepDate1,'".$tmprow['ShipDate1']."' as ShipDate1"));
						
					}
					
				}
			} else {
				$rows = 'nodata';
			}
			
			 $tmpdata = $rows;
			unset($rows);
			
			if(is_array($tmpdata)){
				array_push($tempOutGoingDetail,$tmpdata);
			}
			
			$sqlData2 = str_replace("<changeme>","v_GSTreportOut2NewSummary",$sqlString2);
			$sqlData2 = str_replace("<changWawiIndex>",$CurrentWawiIndex,$sqlData2);
			$sqlData2 .= " order by WawiIndex asc, ShipDate asc, OutShipReference asc, PacklistNumber asc";
			
			$tmpdata = array();
			//$tmpdata = $this->AnalyzeSQL(mssql_query($sqlData2));
			$sqlres2 = mssql_query($sqlData2);
			if (mssql_num_rows($sqlres2) > 0){
				while($tmprow2 = mssql_fetch_assoc($sqlres2)){
					$tmprow2['WawiIndex']=$CurrentWawiAlias;
					$row2[] = $tmprow2;
				}
			} else {
				$row2 = 'nodata';
			}
			
			$tmpdata = $row2;
			unset($row2);
			
			if(is_array($tmpdata)){
				array_push($tempOutGoingSummary,$tmpdata);
			}
		}
		
		$data = array();
		if (trim($this->PostVars['slcCustomer']) == 'All'){
			
			//for ($i=0;$i<count($tempOutGoingDetail);$i++){
				//$sorted = $this->array_orderby($tempOutGoingDetail[$i],'ShipDate1', SORT_ASC, 'OutShipReference', SORT_ASC);
				//unset($sorted);
			//}
			//$sorted = $this->array_orderby($tempOutGoingDetail[0], 'ShipDate1', SORT_ASC, 'OutShipReference', SORT_ASC);
			array_push($data,$tempOutGoingDetail);
			//unset($sorted);
			
			//for ($j=0;$j<count($tempOutGoingSummary);$j++){
				//$sorted = $this->array_orderby($tempOutGoingSummary[$j],'WawiIndex', SORT_ASC, 'OutShipReference', SORT_ASC);
				//unset($sorted);
			//}
			array_push($data,$tempOutGoingSummary);
		} else {
			//$sorted = $this->array_orderby($tempOutGoingDetail[0], 'ShipDate1', SORT_ASC, 'OutShipReference', SORT_ASC);
			array_push($data,array($tempOutGoingDetail[0]));
			//unset($sorted);
			
			
			//$sorted = $this->array_orderby($tempOutGoingSummary[0], 'OutShipReference', SORT_ASC);
			array_push($data,array($tempOutGoingSummary[0]));
		}
		
		if (!is_array($tempOutGoingDetail[0])){
			return 'nodata';
		}
		return $data;
	}
	
	function array_orderby(){
	    $args = func_get_args();
	    $data = array_shift($args);
	    foreach ($args as $n => $field) {
	        if (is_string($field)) {
	            $tmp = array();
	            foreach ($data as $key => $row)
	                $tmp[$key] = $row[$field];
	            $args[$n] = $tmp;
	            }
	    }
	    $args[] = &$data;
	    call_user_func_array('array_multisort', $args);
	    return array_pop($args);
	}
	
	function mssql_escape($str) {
    return str_replace("'", "''", $str);
	}
	
	function AnalyzeSQL($sql){
		if (mssql_num_rows($sql) > 0){
			while($row[] = mssql_fetch_assoc($sql)){}
			array_pop($row);
		} else {
			$row = 'nodata';
		}
		
		return $row;
	}

	function GSTRate(){
		$sql = mssql_query("SELECT Value FROM Setup WHERE Entry='GST'");
		if (mssql_num_rows($sql) > 0){
			$sql = mssql_fetch_assoc($sql);
			$GSTRate = $sql['Value'];
		} else {
			$GSTRate = 7;
		}
		
		return $GSTRate;
	}
	
}

?>
<div id="login">
	<h2>Login</h2>
	<form action="" class="globalform" >
		<ol>
			<li>   
				<label>Username *</label>
				<input type="text" id="login-username" />
			</li>
			<li>
				<label>Password *</label>
				<input type="password" id="login-password" />
			</li>
			<!--<tr>
				<th></th>
				<td valign="top"><input type="checkbox" class="checkbox-size" id="login-check" /><label for="login-check">Remember me</label></td>
			</tr>-->
			<li>
				<label>&nbsp;</label>
				<input type="button" value="Login" id="login-btn"  />&nbsp;
				<input type="button" value="Forgot" id="iforgot"  />
			</li>
		</ol>
    </form>
</div>

<div id="forgot">
	<h2>Forgot</h2>
	<form name="iforgot" class="globalform" id="iforgot" action="" method="post">
		<ol>
			<li>
				<label>Username*</label>
				<input type="text" name="username" id="username" />
			</li>
			<li>
				<label>&nbsp;</label>
				<input type="button" value="Submit" id="forgot-btn"  />
				<input type="button" value="Back to Log In" id="back-btn"  />
			</li>
		</ol>
	</form>
</div>

<div id="firstlogin" style="display:none;">
	<h2 class="flogintitle">First login? Change your password.</h2>
	<form name="firstlogin" action="" class="globalform" >
		<ol>
			<li>
				<label>Username</label> 
				<input type="text" name="username2" id="username2" />
				<span class="req">*<span>
			</li>
			<li>
				<label>Current Password</label> 
				<input type="password" name="curpassword" id="curpassword" />
				<span class="req">*<span>
			</li>
			<li>
				<label>New Password</label> 
				<input type="password" name="newpassword" id="newpassword" />
				<span class="req">*<span>
			</li>
			<li>
				<label>Confirm New Password</label> 
				<input type="password" name="conpassword" id="conpassword" />
				<span class="req">*<span>
			</li>
			<li> 
				<label>&nbsp;</label> 
				<input type="reset" class="button" value="Clear" id="clear"  />&nbsp; 
				<input type="button" value="Change" id="firstlogin-btn"  />
			</li>
		</ol>
	</form>
</div>

<script>
$(function() {
	$( "input:submit, input:button, button, .button").button();
	$("#iforgot").click(function(){
		$("#login").slideUp('slow');
		$("#forgot").slideDown('slow');
	});
	$("#back-btn").click(function(){
		$("#login").slideDown('slow');
		$("#forgot").slideUp('slow');
	});
	
	function msgbox(msg,title,elem){
		$( "#dialog-msgbox:ui-dialog" ).dialog( "destroy" );
		$( "#dialog-msgbox" ).html(msg);
		$( "#dialog-msgbox" ).attr("title",title);
		$("#dialog-msgbox").dialog('option', 'position', 'center');
		$( "#dialog-msgbox" ).dialog({
			modal: true,
			height:'auto',
			show:{ 
				effect:"blind", 
				speed:'slow' 
			},
			hide:'blind',
			buttons: {
				Ok: function() {
					$( this ).dialog( "close" );
					if (elem){
						if(elem == 'redirect'){
							window.location.href = "index.php";	
						}else{
							$('#'+elem).focus().select();
						}
					}
				}
			}
		});
	}	
	
	function ProcessRequest(data,gotoProcess){
		 $.ajax({
			 type: 'POST',
			 url: 'process.php',
			 data: data,
			 success: function(data){
				 
				 switch(gotoProcess){
	 
					 case 'LogMeIn':
						LogMeIn(data);
					 break; 
					 
					  case 'iForgotCallback':
						iForgotCallback(data);
					 break;
					 
					 case 'CheckFirstLoginCallback':
						CheckFirstLoginCallback(data);
					 break; 
					 
					 case 'UpdateUserFirstLoginCallback':
						UpdateUserFirstLoginCallback(data);
					 break; 
					 
				 }
			 },
			 beforeSend: function(){
				$.dimScreen(1000, 0.5, function() {
					$('#content').fadeIn();
				});
			   },
			 complete: function(){
				 $.dimScreenStop();
			   }
		 });
	}
	
	/*-----------------------------------------------START LOG IN--------------------------------------------------*/
	//ProcessRequest('type=CheckFirstLogin','CheckFirstLoginCallback');
	
	
	$('#login-username').focus();
	$('#login-username').keypress(function(event) {
		if ( event.which == 13 ) {if ($(this).val()){$('#login-password').focus().select();}}
	});
	
	$('#login-password').keypress(function(event) {
		if ( event.which == 13 ) {if ($(this).val()){$("#login-btn").click();}
		}
	});
	
	$('#login-btn').click(function(){
		if (!$('#login-username').val()){
			msgbox('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0px 4px 4px 0px;"></span>Please enter username</p>','WMS Alert','login-username');
			return false;
		}
		if (!$('#login-password').val()){
			msgbox('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0px 4px 4px 0px;"></span>Please enter password</p>','WMS Alert','login-password');
			return false;
		}
		ProcessRequest('username=' + $('#login-username').val() + '&password=' + $('#login-password').val() + '&type=LogMeIn','LogMeIn');
	});
	
	
	
	
	function LogMeIn(data){
		if(data == 'expired'){
			$('#username2').val($('#login-username').val());
			$('.flogintitle').html('Your password has been expired. Change your password.');	 	
			$("#login").hide('fast');
			$("#firstlogin").show('slow');
			$("#firstlogin input#curpassword").focus().select();
		}else if(data == 'first'){
			$('#username2').val($('#login-username').val());
			$('.flogintitle').html('First login? Change your password.');	 	
			$("#login").hide('fast');
			$("#firstlogin").show('slow');
			$("#firstlogin input#curpassword").focus().select();
		}else if(data == 'success'){
			
			window.location.href = "index.php";	
			/*
			$( "#dialog-msgbox:ui-dialog" ).dialog( "destroy" );
			$( "#dialog-msgbox" ).html('<p><span class="ui-icon ui-icon-info" style="float:left; margin:0px 4px 4px 0px;"></span>Login Success</p>');
			$( "#dialog-msgbox" ).attr("title",'iWMS Confirmation');
			$("#dialog-msgbox").dialog('option', 'position', 'center');
			$( "#dialog-msgbox" ).dialog({
				modal: true,
				height:'auto',
				show:{ 
					effect:"blind", 
					speed:'slow' 
				},
				hide:'blind',
				buttons: {
					Ok: function() {
						$( this ).dialog( "close" );
						window.location.href = "index.php";	
					}
				}
			});
			*/
		}else if(data == 'failed'){
			msgbox('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0px 4px 4px 0px;"></span>Login Failed</p>','WMS Alert','login-password');
		}else if(/^-{0,1}\d*\.{0,1}\d+$/.test(data)){
			$( "#dialog-msgbox:ui-dialog" ).dialog( "destroy" );
					$( "#dialog-msgbox" ).html('<p><span class="ui-icon ui-icon-info" style="float:left; margin:0px 4px 4px 0px;"></span>Your password wil expire in less than '+data+' day(s). Change password?</p>');
					$( "#dialog-msgbox" ).attr("title",'iWMS Alert');
					$("#dialog-msgbox").dialog('option', 'position', 'center');
					$( "#dialog-msgbox" ).dialog({
						modal: true,
						height:'auto',
						show:{ 
							effect:"blind", 
							speed:'slow' 
						},
						hide:'blind',
						buttons: {
							"Later": function() {
								$(this).dialog("close");
								window.location.href = "index.php";	 
							},
							"Yes": function() {
								$(this).dialog("close");
								$('.flogintitle').html('Your password wil expire in less than '+data+' day(s). Change your password.');							
								$('#username2').val($('#login-username').val());
								$("#login").hide('fast');
								$("#firstlogin").show('slow');
								$("#firstlogin input#curpassword").focus().select();
							}
						}
					});
		}
	
	}
	
	

	
	/*-----------------------------------------------START LOG IN--------------------------------------------------*/
	
	/*-----------------------------------------------START FIRST LOG IN--------------------------------------------------*/
	function CheckFirstLoginCallback(data){
		if(data != 'failed'){
			if(data != 'success'){
				if(data == 'expired'){
					$('.flogintitle').html('Your password has been expired. Change your password.');	 	
					$("#login").hide('fast');
					$("#firstlogin").show('slow');
					$("#firstlogin input#curpassword").focus().select();
				}else{
					
				}
					
			}else {		
				$("#login").hide('fast');
				$("#firstlogin").show('slow');
				$("#firstlogin input#curpassword").focus().select();
			}
		}else{
			window.location.href = "index.php";		
		}
	}
	
	
	$('#firstlogin-btn').click(function(){
			var frules =	[
					"required,username2,Please enter your username.",
					"required,curpassword,Please enter your current password.",
					"required,newpassword,Please enter new password.",
					"required,conpassword,Please confirm new password.",
					"same_as,newpassword,conpassword,Please ensure the new passwords you enter are the same.",
					"length>=6,newpassword,Please enter a value that is at least 6 characters long"
					];
			rsv.customErrorHandler = formError2;	
			rsv.onCompleteHandler = formSubmit2;
			rsv.validate(document.forms['firstlogin'],frules);
		
	});
	
	function formSubmit2(theForm){
		var formValues = $(theForm).serialize();
		ProcessRequest('type=UpdateUserFirstLogin'+ '&username='+$('#username2').val()+'&' + formValues,'UpdateUserFirstLoginCallback');
	}
	
	function formError2(f, errorInfo){		
		var fieldName;
		if(errorInfo.length > 0){
			// radio button
			if (errorInfo[0][0].type == undefined)
				fieldName = errorInfo[0][0][0].name;
			else
				fieldName = errorInfo[0][0].name;
				
			msgbox('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0px 4px 4px 0px;"></span>'+errorInfo[0][1]+'</p>','WMS Alert',fieldName);
		}
	}
	 
	function UpdateUserFirstLoginCallback(data){
		if(data == 'success'){		
			//msgbox('<p><span class="ui-icon ui-icon-info" style="float:left; margin:0px 4px 4px 0px;"></span>Your password has beed updated successfully.</p>','WMS Confirmation','redirect'); 
			window.location.href = "index.php";	
		}else if(data == 'samepassword'){
			msgbox('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0px 4px 4px 0px;"></span>New password should not be the same with your current password.</p>','WMS Alert','newpassword');
		}else if(data == 'invalid'){
			msgbox('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0px 4px 4px 0px;"></span>Invalid current password.</p>','WMS Alert','curpassword');
		}else {
			msgbox('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0px 4px 4px 0px;"></span>Cannot update your password.Please contact support</p>','WMS Alert','curpassword');
		}
	}
	
	/*-----------------------------------------------END FIRST LOG IN--------------------------------------------------*/
	
	/*-----------------------------------------------FORGOT LOG IN--------------------------------------------------*/
	
	$('#forgot-btn').click(function(){
		var rules =	[
					"required,username,Please enter your username."
					];
			rsv.customErrorHandler = formError;	
			rsv.onCompleteHandler = formSubmit;
			rsv.validate(document.forms['iforgot'],rules);
		
	});
	
	function formSubmit(theForm){
		var formValues = $(theForm).serialize();
		ProcessRequest('type=iForgot&'+formValues,'iForgotCallback');
	} 
	
	function formError(f, errorInfo){		
		var fieldName;
		if(errorInfo.length > 0){
			if (errorInfo[0][0].type == undefined)
				fieldName = errorInfo[0][0][0].name;
			else
				fieldName = errorInfo[0][0].name;
				
			msgbox('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0px 4px 4px 0px;"></span>'+errorInfo[0][1]+'</p>','WMS Alert','c-'+fieldName);
		}
	}
	
	function iForgotCallback(data){
		if(data.indexOf( "success" ) !== -1){

			data = data.split('|');
			var email = data[1];
			msgbox('<p><span class="ui-icon ui-icon-info" style="float:left; margin:0px 4px 4px 0px;"></span>Your login detail has been sent to <strong>'+email+'</strong><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please check your email. </p>','WMS Confirmation','login-email');
		}else{
			msgbox('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0px 4px 4px 0px;"></span>Could not retrieve your login details. Please contact Dev Team.</p>','WMS Alert','username');
		}
	}
	
});
</script>
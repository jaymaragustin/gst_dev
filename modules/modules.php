<?php 
session_start();
require ('../include/referer.checker.php');
require ('../include/session.checker.php');

$_SESSION['defaultProgramID'] = $_GET['ID'];
$_SESSION['defaultProgramName'] = $_GET['ProgramName'];
?>
<h2 id="module_title"><a style="text-decoration:none; color:#417684;" href="modules/programs.php" id="programlist">Programs</a> &raquo; <span style="color:#417684;"><?php echo isset($_SESSION['defaultProgramName']) ? $_SESSION['defaultProgramName'].' : ': '';?>Modules </span></h2>
<a title="Back to Program List" href="modules/programs.php" class="back-btn-programs" ><< Program List</a>
<ul id="module_items"></ul>
<div class="clr"></div>
<br/><br/><br/>
<script>

$(function() {
	$('.back-btn').button();
	function loadContent(href){ 
		$.ajax({
			 type: 'POST',
			 url: href,
			 success: function(data){
				var content = data;
				
				$(".content").html(content);				
				
			 },
			 beforeSend: function(){
				Dim(1);
			   },
			 complete: function(){
				Dim(0);
			   }
		 });
	 }	
	 
	 function Dim(value){
		 if (value==1)
			 $.dimScreen(1000, 0.5, function() {
				//$('.content').fadeIn();
			});
		 else{
			 $.dimScreenStop();
		 }
	 }
	 
	 function msgbox(msg,title,elem){
		$( "#dialog-msgbox:ui-dialog" ).dialog( "destroy" );
		$( "#dialog-msgbox" ).html(msg);
		$( "#dialog-msgbox" ).attr("title",title);
		$("#dialog-msgbox").dialog('option', 'position', 'center');
		$( "#dialog-msgbox" ).dialog({
			modal: true,
			height:'auto',
			show:{ 
				effect:"blind", 
				speed:'slow' 
			},
			hide:'blind',
			buttons: {
				Ok: function() {
					$( this ).dialog( "close" );
					if (elem){
						$('#'+elem).focus().select();
					}
				}
			}
		});
	}	
	 
	 function ProcessRequest(data,gotoProcess){
		 $.ajax({
			 type: 'POST',
			 url: 'process.php',
			 data: data,
			 success: function(data){
				 
				 switch(gotoProcess){
	 
					 case 'GetGSTModules':
						GetGSTModules(data);
					 break; 
					 
				 }
			 },
			 beforeSend: function(){
				Dim(1);
			   },
			 complete: function(){
				 Dim(0);
			   }
		 });
	} 
	
	/*-----------------------------------------------START LOG IN--------------------------------------------------*/
	
	ProcessRequest('type=GetGSTModules','GetGSTModules');
	
	function GetGSTModules(data){
		if (data=='no data'){
			msgbox('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0px 4px 4px 0px;"></span>No available module(s) allowed for this user</p>','iWMS Alert',0);
		}else{
			
			var obj = jQuery.parseJSON(data);
			if (obj.length){
				$('#module_items').html('');
				$('#module_items').empty();
				for (var i=0;i<obj.length;i++){
					if (obj[i].MenuID==12 || obj[i].MenuID==43 || obj[i].MenuID==44){ 
						$('#module_items').append('<li class="cat-'+obj[i].MenuCategory+'"><a class="module_item" id="icon_'+obj[i].MenuID+'" href="'+obj[i].MenuURL+'<?php echo strtolower(str_replace(' ','',$_SESSION['defaultProgramName']))?>.php"><span>'+obj[i].MenuName+'</span></a></li>');
					}else{
						$('#module_items').append('<li class="cat-'+obj[i].MenuCategory+'"><a class="module_item" id="icon_'+obj[i].MenuID+'" href="'+obj[i].MenuURL+'"><span>'+obj[i].MenuName+'</span></a></li>');
					}
				}
				
				$('li.cat-1').wrapAll('<fieldset></fieldset>').parent('fieldset').prepend('<legend class="mod_legend"><span class="cat-header">Warehousing</span></legend>');
				$('li.cat-2').wrapAll('<fieldset></fieldset>').parent('fieldset').prepend('<legend class="mod_legend"><span class="cat-header">Inventory</span></legend>');
				$('li.cat-3').wrapAll('<fieldset></fieldset>').parent('fieldset').prepend('<legend class="mod_legend"><span class="cat-header">Shipping</span></legend>');
				$('li.cat-4').wrapAll('<fieldset></fieldset>').parent('fieldset').prepend('<legend class="mod_legend"><span class="cat-header">Report</span></legend>');
				$('li.cat-5').wrapAll('<fieldset></fieldset>').parent('fieldset').prepend('<legend class="mod_legend"><span class="cat-header">Administrative</span></legend>');
				$('li.cat-6').wrapAll('<fieldset></fieldset>').parent('fieldset').prepend('<legend class="mod_legend"><span class="cat-header">Miscellaneous</span></legend>');
				
				$('.mod_legend').css({'border-bottom': '1px solid #dde4e9','font-size': '13px','padding-bottom': '5px','padding-top': '15px','text-transform': 'uppercase','width': '98%'})
				$('.mod_legend span').click(function(){
					var catHeader = $(this);
					catHeader.toggleClass('arrow-e').parents('fieldset').children('li').slideToggle();
					if(catHeader.hasClass('arrow-e'))
						catHeader.parent('legend').css('border-bottom','1px solid #ffffff');
					else
						catHeader.parent('legend').css('border-bottom','1px solid #dde4e9');
				});
				
				var hoverelem = '<span class="stripe"></span>';				
				$('#module_items li').mouseover(function(){ $(this).append(hoverelem);}).mouseout(function(){ $(this).find('.stripe').remove(); });
				
				$('#module_items a').click(function() {
					if ($(this).attr('href')!='#'){
						loadContent(this.href);
						return false;
					}
					return false;
				});
				
				$('#module_wawititle span').html('<?php echo $_SESSION['defaultProgramName'];?>' +' : '+'Modules');
							
				if (obj.length==1){	$('#icon_'+obj[0].MenuID).click();	}
			}else{
				$('#module_items').html('');
				$('#module_items').empty();
			}
			

		}
	}
	
	$(".back-btn-programs, #programlist").click(function(){
		if ($(this).attr('href')!='#'){
			loadContent(this.href);
			$("#back_link").html('');
			return false;
		}
	});
	$("#back_link").html('');
	$(".back-btn-programs").appendTo("#back_link");
	
	/*-----------------------------------------------START LOG IN--------------------------------------------------*/
});
</script>
<?php 
session_start();
require '../include/referer.checker.php';
require '../include/session.checker.php';

?>
<h2>Programs</h2>
<ul id="program_items">
	<?php
	echo date("H:i:s");
	if (count($_SESSION['ProgramList'])){
		for ($i=0;$i<count($_SESSION['ProgramList']);$i++){
			echo '<li><a class="program_item" id="icon_'.$_SESSION['ProgramList'][$i]['ID'].'" href="modules/modules.php?ID='.$_SESSION['ProgramList'][$i]['ID'].'&ProgramName='.$_SESSION['ProgramList'][$i]['ProgramName'].'" title="'.$_SESSION['ProgramList'][$i]['ProgramName'].'">'.$_SESSION['ProgramList'][$i]['ProgramName'].'</a></li>';
		}
	}
	?>
	
</ul>
<div class="clr"></div>
<script>
$(function() {
	function loadContent(href){ 
		$.ajax({
			 type: 'POST',
			 url: href,
			 success: function(data){
				var content = data;
				
				if (content=='logout'){
					$(window.location).attr('href', 'index.php');
					return false;
				}
				
				$(".content").html(content);				
				
			 },
			 beforeSend: function(){
				Dim(1);
			   },
			 complete: function(){
				Dim(0);
			   }
		 });
	 }	
	 
	 function Dim(value){
		 if (value==1)
			 $.dimScreen(1000, 0.5, function() {
				//$('.content').fadeIn();
			});
		 else{
			 $.dimScreenStop();
		 }
	 }
	 
	 $('#program_items a').click(function() {
		if ($(this).attr('href')!='#'){
			loadContent(this.href);
			return false;
		}
		return false;
	});
	
	<?php if (count($_SESSION['ProgramList'])==1){	echo "$('#icon_".$_SESSION['ProgramList'][0]['ID']."').click();";}	?>
});
</script>


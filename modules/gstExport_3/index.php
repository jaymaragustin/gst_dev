<?php 
//error_reporting(E_ALL);
//ini_set("display_errors",1);
session_start();
require ('../../include/referer.checker.php');
require ('../../include/session.checker.php');

#style="border:1px solid #EEEEEE;"

?>

<link href="css/select2.css" rel="stylesheet">
<script src="js/select2.js"></script>

<style>
	.alignme{
		text-align:center;
	}
	
	#tblShipmentNumberEditing_filter, #tblShipmentNumberEditing_filter label{
		width: 300px !important;
	}
	
	#tblShipmentNumberDetails_filter, #tblShipmentNumberDetails_filter label{
		width: 300px !important;
	}
	
</style>
<h2><?php include '../../tpl/module_shortcut.php';?>Export</h2> 
<form onsubmit="return false;" class="globalform"> 
<div id="divMainBody">
	<div style="float: left; padding: 5px; width: 370px;">
		<ol>
	  	<li><label>is ADI</label><input type="checkbox" id="isADI" name="isADI" value="isADI"></li>
	  	<li><label>Shipment Number:</label><input type='text' id="ShipmentNumber" name="ShipmentNumber" class="text" value=""/></li>
		</ol>
	</div>
	<div style="float: left; padding: 5px; width: 510px;">
		<input type="button" name="btnNew" id="btnNew" value="New"  />
		<input type="button" name="btnFirst" id="btnFirst" value="First"  />
		<input type="button" name="btnPrevious" id="btnPrevious" value="Previous"  />
		<input type="button" name="btnNext" id="btnNext" value="Next"  />
		<input type="button" name="btnLast" id="btnLast" value="Last"  />&nbsp;&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;&nbsp;
		<input type="button" name="btnEdit" id="btnEdit" value="Edit"  />
		<input type="button" name="btnDelete" id="btnDelete" value="Delete"  />
	</div>
	<div class="clr"></div>
	<div id="mainDivLeft" style="float: left; padding: 5px; width: 370px;">
		<ol>
			<li><label>Customer:</label><input type="text" class="text" readonly id='WawiAlias' name='WawiAlias' /></li>
			<li><label>HAWB:</label><input type="text" class="text" readonly id='HAWB' name='HAWB' /></li>
			<li><label>MAWB:</label><input type="text" class="text" readonly id='MAWB' name='MAWB' /></li>
			<li><label>Flight:</label><input type="text" class="text" readonly id='Flight' name='Flight' /></li>
			<li><label>Dep Date:</label><input type="text" class="text" readonly id='DepDate' name='DepDate' /></li>
			<li><label>Ship Date:</label><input type="text" class="text" readonly id='ShipDate' name='ShipDate' /></li>
			<li><label>Destination:</label><input type="text" class="text" readonly id='Destination' name='Destination' /></li>
			<li><label>Forwarder:</label><input type="text" class="text" readonly id='Forwarders' name='Forwarders' /></li>
			<li><label>Job Type:</label><input type="text" class="text" readonly id='JobType' name='JobType' /></li>
			<li><label>Comment:</label><input type="text" class="text" readonly id='Remarks' name='Remarks' /></li>
		</ol>
	</div>
	<div id="mainDivRight" style="float: left; padding: 5px; width: 510px;">
			<ol>
				<li><label>Cust Inv No:</label><input type="text" class="text" readonly id='CustInvNo' maxlength="600" name='CustInvNo' /></li>
				<li><label>Permit No:</label><input type="text" class="text" readonly id='PermitNo' name='PermitNo' /></li>
				<li><label>Permit Date:</label><input type="text" class="text" readonly id='PermitDate' name='PermitDate' /></li>
				<li><label>USD Unit:</label><input type="text" class="text" readonly id='USDUnitPrice' name='USDUnitPrice' /></li>
				<li><label>Cust Inv(USD):</label><input type="text" class="text" readonly id='CustInvValue' name='CustInvValue' /></li>
				<li><label>Exchange:</label><input type="text" class="text" readonly id='ExchangeRate' name='ExchangeRate' /></li>
				<li><label>Cust Inv(SGD):</label><input type="text" class="text" readonly id='InvoiceValueSGD' name='InvoiceValueSGD' /></li>
				<li><label>GST Amount:</label><input type="text" class="text" readonly id='GSTAmount' name='GSTAmount' /></li>
			</ol>
			<div class="clr"></div>
			<fieldset>
				<legend>Picklist Number(s)</legend>
				<ol>
					<li>
						<table class="display" id="tblPicklistNumbers" style="width:380px;"></table>
					</li>
				</ol>
			</fieldset>
	</div>
	
	
	<div id="adimainDivLeft" style="float: left; padding: 5px; width: 370px;">
		<ol>
		<li><label>Destination:</label><input type="text" class="text" id="adidataDestination" name="adidataDestination" readonly /></li>
		<li><label>Forwarder:</label><input type="text" class="text" id="adidataForwarders" name="adidataForwarders" readonly /></li>
		<li><label>HAWB:</label><input type="text" class="text" id="adidataHAWB" name="adidataHAWB" readonly /></li>
		<li><label>Cargo Release Date:</label><input type="text" class="text" id="adidataShipDate" name="adidataShipDate" readonly /></li>
		<li><label>Job Type:</label><input type="text" class="text" id="adidataJobType" name="adidataJobType" readonly /></li>
		<li><label>Comment:</label><input type="text" class="text" id="adidataRemarks" name="adidataRemarks" readonly /></li>
		<li><label>MAWB:</label><input type="text" class="text" id="adidataMAWB" name="adidataMAWB" readonly /></li>
		<li><label>Flight:</label><input type="text" class="text" id="adidataFlight" name="adidataFlight" readonly /></li>
		
		</ol>
	</div>
	
	<div id="adimainDivRight" style="float: left; padding: 5px; width: 510px;">
		<ol>
			<li><label>Permit No:</label><input type="text" class="text" id="adidataPermitNo" name="adidataPermitNo" readonly /></li>
			<li><label>Permit Date:</label><input type="text" class="text" id="adidataPermitDate" name="adidataPermitDate" readonly /></li>
			<li><label>Cust Inv(USD):</label><input type="text" class="text" readonly id="adidataCustInvValue" name="adidataCustInvValue" readonly /></li>
			<li><label>Exchange:</label><input type="text" class="text" id="adidataExchangeRate" name="adidataExchangeRate" readonly /></li>
			<li><label>Cust Inv(SGD):</label><input type="text" class="text" readonly id="adidataInvoiceValueSGD" name="adidataInvoiceValueSGD" readonly /></li>
			<li><label>GST Amount(SGD):</label><input type="text" class="text" readonly id="adidataGSTAmount" name="adidataGSTAmount" readonly /></li>
			<li><label>Dep Date:</label><input type="text" class="text" id="adidataDepDate" name="adidataDepDate" readonly /></li>
			<li><label>Delivery From:</label><input type="text" class="text" id="adidataDeliveryFrom" name="adidataDeliveryFrom" readonly /></li>
		</ol>
		<div class="clr"></div>
	</div>
	
	<div class="clr"></div>
	<div id="adimainCenterDiv" style="width:900px;" >
		<fieldset>
			<legend>DN Details</legend>
			<ol>
				<li>
					<table class="display" id="tblShipmentNumberDetails" ></table>
				</li>
			</ol>
		</fieldset>
	</div>
	
</div>

<div id="divNewEditBody">
	<div style="float: left; padding: 5px; width: 370px;">
		<ol>
	  	<li><label>Customer Name:</label><select id="slcCustomers" name="slcCustomers" class="select"><option value="" selected>Select here</option></select></li>
	  	<li id='EditShipmentNumber'><label>Shipment Number:</label><input type="text" readonly class="text" id='txtShipmentNumber' name='txtShipmentNumber' /></li>
		</ol>
	</div>
	<div style="float: right; padding: 5px;">
		<input type="button" name="btnSaveUpdate" id="btnSaveUpdate" value="Save"  />
		<input type="button" name="btnCancel" id="btnCancel" value="Cancel"  />
	</div>
	<div class="clr"><br/></div>
	<div id="divFullEdit">
		
		<div id="rightDiv" style="float: left; padding: 5px; width: 370px;">
			<ol>
		  	<li><label>HAWB:</label><input type="text" class="text" id='txtHAWB' name='txtHAWB' /></li>
		  	<li><label>MAWB:</label><input type="text" class="text" id='txtMAWB' name='txtMAWB' /></li>
		  	<li><label>Flight:</label><input type="text" class="text" id='txtFlight' name='txtFlight' /></li>
		  	<li><label>Dep Date:</label><input type="text" class="text" id='txtDepDate' name='txtDepDate' /></li>
		  	<li><label>Ship Date:</label><input type="text" class="text" id='txtShipDate' name='txtShipDate' /></li>
		  	<li><label>Destination:</label><select id="slcDestination" name="slcDestination" class="select"></select></li>
		  	<li><label>Forwarder:</label><select id="slcForwarders" name="slcForwarders" class="select"></select></li>
		  	<li><label>Job Type:</label><select id="slcJobType" name="slcJobType" class="select"></select></li>
		  	<li><label>Comment:</label><input type="text" class="text" id='txtRemarks' name='txtRemarks' /></li>
			</ol>
		</div>
		
		<div id="leftDiv" style="float: left; padding: 5px; width: 510px;">
			<ol>
				<li><label>Cust Inv No:</label><input type="text" class="text" id='txtCustInvNo' maxlength="600" name='txtCustInvNo' /></li>
				<li><label>Permit No:</label><input type="text" class="text" id='txtPermitNo' name='txtPermitNo' /></li>
				<li><label>Permit Date:</label><input type="text" class="text" id='txtPermitDate' name='txtPermitDate' /></li>
				<li><label>USD Unit:</label><input type="text" class="text" id='txtUSDUnitPrice' name='txtUSDUnitPrice' /></li>
				<li><label>Cust Inv(USD):</label><input type="text" class="text" readonly id='txtCustInvValue' name='txtCustInvValue' /></li>
				<li><label>Exchange:</label><input type="text" class="text" id='txtExchangeRate' name='txtExchangeRate' /></li>
				<li><label>Cust Inv(SGD):</label><input type="text" class="text" readonly id='txtInvoiceValueSGD' name='txtInvoiceValueSGD' /></li>
				<li><label>GST Amount:</label><input type="text" class="text" readonly id='txtGSTAmount' name='txtGSTAmount' /></li>
			</ol>
			<div class="clr"></div>
			<fieldset>
				<legend>Picklist Number(s)</legend>
				<ol>
					<li>
						<label style="width: 60px;">Picklist:</label><input type="text" class="text" id="txtsearchplist" name="txtsearchplist" style="margin-right:10px;"/><select id="slcAvailablePlist" name="slcAvailablePlist" class="select"></select> &nbsp;&nbsp;&nbsp;<input type="button" id="btnAddToList" name="btnAddToList" value="Add To List" style="margin-top:3px;" />
						<table class="display" id="tblPicklistNumbersEditing" style="width:450px;"></table>
					</li>
				</ol>
			</fieldset>
		</div>
		
	</div>
	
	
	<div id="ADIdivFullEdit">
		
		<div id="adirightDiv" style="float: left; padding: 5px; width: 370px;">
			<ol>
			<li>
				<label>Shipment Number:</label>
				<select id="txtShipment" name="txtShipment" class="select" style="width: 136px;"></select>
				<input type="text" class="text" id="txtShipmentEdit" name="txtShipmentEdit" style="display:none;" readonly />
			</li>
			
		  	<li><label>Destination:</label><input type="text" class="text" id="adiDestination" name="adiDestination" readonly /></li>
		  	<li><label>Forwarder:</label><input type="text" class="text" id="adiForwarders" name="adiForwarders" readonly /></li>
		  	<li><label>HAWB:</label><input type="text" class="text" id="adiHAWB" name="adiHAWB" readonly /></li>
		  	<li><label>Cargo Release Date:</label><input type="text" class="text" id="adiShipDate" name="adiShipDate" readonly /></li>
			<li><label>Job Type:</label><select id="adiJobType" name="adiJobType" class="select" style="width: 136px;"></select></li>
			<li><label>Comment:</label><input type="text" class="text" id="adiRemarks" name="adiRemarks" /></li>
		  	<li><label>MAWB:</label><input type="text" class="text" id="adiMAWB" name="adiMAWB" /></li>
		  	<li><label>Flight:</label><input type="text" class="text" id="adiFlight" name="adiFlight" /></li>
			
			</ol>
		</div>
		
		<div id="adileftDiv" style="float: left; padding: 5px; width: 510px;">
			<ol>
				<li><label>Permit No:</label><input type="text" class="text" id="adiPermitNo" name="adiPermitNo" /></li>
				<li><label>Permit Date:</label><input type="text" class="text" id="adiPermitDate" name="adiPermitDate" /></li>
				<li><label>Cust Inv(USD):</label><input type="text" class="text" readonly id="adiCustInvValue" name="adiCustInvValue" readonly /></li>
				<li><label>Exchange:</label><input type="text" class="text" id="adiExchangeRate" name="adiExchangeRate" /></li>
				<li><label>Cust Inv(SGD):</label><input type="text" class="text" readonly id="adiInvoiceValueSGD" name="adiInvoiceValueSGD" readonly /></li>
				<li><label>GST Amount(SGD):</label><input type="text" class="text" readonly id="adiGSTAmount" name="adiGSTAmount" readonly /></li>
				<li><label>Dep Date:</label><input type="text" class="text" id="adiDepDate" name="adiDepDate" /></li>
				<li><label>Delivery From:</label><input type="text" class="text" id="adiDeliveryFrom" name="adiDeliveryFrom" /></li>
			</ol>
			<div class="clr"></div>
		</div>
		
		<div class="clr"></div>
		<div id="centerDiv" style="width:900px;" >
			<fieldset>
				<legend>DN Details</legend>
				<ol>
					<li>
						<table class="display" id="tblShipmentNumberEditing" ></table>
					</li>
				</ol>
			</fieldset>
		</div>
		
	</div>
	<div id="ADIdivNoAvailable">
		<fieldset style="width:880px;">
			<legend>GST Export Details</legend>
			<ol>
				<li>
					No Available Shipment Number
				</li>
			</ol>
		</fieldset>
	</div>
	
	
	<div id="divNoAvailable">
		<fieldset style="width:880px;">
			<legend>GST Export Details</legend>
			<ol>
				<li>
					No Available Picklist Number
				</li>
			</ol>
		</fieldset>
	</div>
</div>

</form>

<script>
ErrorKeys = new Object();
ErrorKeys['First'] = 'Beginning of the data';
ErrorKeys['Previous'] = 'Beginning of the data';
ErrorKeys['Last'] = 'Last data';
ErrorKeys['Next'] = 'Last data';
ErrorKeys['Search'] = 'Sorry invalid Shipment Number';
ErrorKeys['nodata'] = 'Sorry no available Reference Number';
ErrorKeys['shipmentinvalid'] = 'Sorry, Shipment Number not found';
ErrorKeys['ErrorSearchingShipment'] = 'Shipment Number is a required field in order to Search.';
ErrorKeys['InvalidSearchingShipment'] = 'Shipment Number failed to retrieve data or already processed.';


AddUpdateMSG = new Object();
AddUpdateMSG['successUPDATE'] = '<table><tr><td valign="top"><span class="ui-icon ui-icon-info" style="float:left; margin:0px 4px 4px 0px;"></span></td><td>Update successful for Export Reference: <strong><shipreference></strong></td></tr></table>';
AddUpdateMSG['errorUpdate'] = '<table><tr><td valign="top"><span class="ui-icon ui-icon-alert" style="float:left; margin:0px 4px 4px 0px;"></span></td><td>Failed: Unable to update Export Reference: <strong><shipreference></strong>.</td></tr><tr><td>&nbsp;</td><td>Please check your data and try again later.</td></tr></table>';
AddUpdateMSG['successADD'] = '<table><tr><td valign="top"><span class="ui-icon ui-icon-info" style="float:left; margin:0px 4px 4px 0px;"></span></td><td>New Export Reference: <strong><shipreference></strong> has been created.</td></tr></table>';
AddUpdateMSG['erroradd'] = '<table><tr><td valign="top"><span class="ui-icon ui-icon-alert" style="float:left; margin:0px 4px 4px 0px;"></span></td><td>Failed: Unable to create New Export Reference.</td></tr><tr><td>&nbsp;</td><td>Please check your data and try again later.</td></tr></table>';
AddUpdateMSG['errInvalidRequest'] = '<table><tr><td valign="top"><span class="ui-icon ui-icon-alert" style="float:left; margin:0px 4px 4px 0px;"></span></td><td>Failed: Invalid request.</td></tr><tr><td>&nbsp;</td><td>Please check your data and try again later.</td></tr></table>';
AddUpdateMSG['errCustInvNo'] = '<table><tr><td valign="top"><span class="ui-icon ui-icon-alert" style="float:left; margin:0px 4px 4px 0px;"></span></td><td>Failed: Cust Inv No is empty.</td></tr><tr><td>&nbsp;</td><td>Please check your data and try again later.</td></tr></table>';
AddUpdateMSG['errMasterData'] = '<table><tr><td valign="top"><span class="ui-icon ui-icon-alert" style="float:left; margin:0px 4px 4px 0px;"></span></td><td>Failed: Unable to retrieve master data.</td></tr><tr><td>&nbsp;</td><td>Please check your data and try again later.</td></tr></table>';
AddUpdateMSG['errSTGMSUpdate'] = '<table><tr><td valign="top"><span class="ui-icon ui-icon-alert" style="float:left; margin:0px 4px 4px 0px;"></span></td><td>Failed: Unable to update STGMS records.</td></tr><tr><td>&nbsp;</td><td>Please check your data and try again later.</td></tr></table>';
AddUpdateMSG['errSTGMSInsert'] = '<table><tr><td valign="top"><span class="ui-icon ui-icon-alert" style="float:left; margin:0px 4px 4px 0px;"></span></td><td>Failed: Unable to insert STGMS records.</td></tr><tr><td>&nbsp;</td><td>Please check your data and try again later.</td></tr></table>';
AddUpdateMSG['errDNnotExist'] = '<table><tr><td valign="top"><span class="ui-icon ui-icon-alert" style="float:left; margin:0px 4px 4px 0px;"></span></td><td>Failed: Unable to update due to Cust Inv No <strong><DNnotExist></strong> is not existing in WMS.</td></tr><tr><td>&nbsp;</td><td>Please check your data and try again later.</td></tr></table>';


var GSTForwarders = ['JSI','Freight Forwarder','Courier','Local Transporter'];
var JobTypeKeys = ["Export","Hand Carry","Local Delivery","Local Sales","RWK Out","RMA","Scrap","SAMPLES"];
var ADIJobTypeKeys = ["Export","Hand Carry","Local Delivery","RWK Out","RMA","Scrap","SAMPLES"];

var tblPicklistNumbers;
var tblShipmentNumberDetails;
var CurrentListArray;
var GSTRate = 0;
var CurrentAvaiLablePicklistPerCustomer;
var CurrentAvaiLablePicklistPerCustomerPrimary;
var ShipmentNumber;

var CurrentDate = new Date();
var tblPicklistNumbersEditing;
var tblShipmentNumberEditing;
var removeThisPacklist;

$(function() {
	$( "input:submit, input:button, button, .button").button();
	$("#divNewEditBody").hide();
	$('#txtDepDate').datepicker({
		dateFormat: 'mm/dd/yy',
		onSelect: function(date) {
            $('#slcIncoTerm').focus();
        }
		});
	$('#txtShipDate').datepicker({
		dateFormat: 'mm/dd/yy',
		onSelect: function(date) {
            $('#slcIncoTerm').focus();
        }
		});
	$('#txtPermitDate').datepicker({
		dateFormat: 'mm/dd/yy',
		onSelect: function(date) {
            //$('#slcIncoTerm').focus();
        }
		});
	$('#adiPermitDate').datepicker({
		//dateFormat: 'mm/dd/yy',
		dateFormat: 'dd/mm/yy',
		onSelect: function(date) {
            //$('#slcIncoTerm').focus();
        }
		});
	$('#adiDepDate').datepicker({
		//dateFormat: 'mm/dd/yy',
		dateFormat: 'dd/mm/yy',
		onSelect: function(date) {
            //$('#slcIncoTerm').focus();
        }
		});
	//$("#txtShipment").select2();
	
	$("#txtUSDUnitPrice").DecimalMask('9999999.99');
	$("#txtCustInvValue").DecimalMask('9999999.99');
	$("#txtExchangeRate").DecimalMask('9999999.9999999');
	$("#txtInvoiceValueSGD").DecimalMask('9999999.99');
	$("#txtInvoiceValueSGD").DecimalMask('9999999.99');
	
	jQuery.fn.exists = function(){return this.length>0;}
	jQuery.isJson = function(str) {
	 if (jQuery.trim(str) == '') return false;
	 str = str.replace(/\\./g, '@').replace(/"[^"\\\n\r]*"/g, '');
	 return (/^[,:{}\[\]0-9.\-+Eaeflnr-u \n\r\t]*$/).test(str);
	}
	
	Number.prototype.padLeft = function(base,chr){
	    var  len = (String(base || 10).length - String(this).length)+1;
	    return len > 0? new Array(len).join(chr || '0')+this : this;
	}
	
	CurrentDate = [(CurrentDate.getMonth() + 1).padLeft(),CurrentDate.getUTCDate().padLeft(),CurrentDate.getFullYear()].join('/');

	function NASort(a, b) {    
	    if (a.innerHTML == 'NA') {
	        return 1;   
	    }
	    else if (b.innerHTML == 'NA') {
	        return -1;   
	    }       
	    return (parseInt(a.innerHTML) > parseInt(b.innerHTML)) ? 1 : -1;
	};

	function ProcessRequest(data,gotoProcess){
		data.UserID='<?php echo (isset($_SESSION[LoginUserVar])) ? $_SESSION[LoginUserVar]: 'none';?>';
		$.ajax({
		 type: 'POST',
		 url: 'modules/gstExport/ExportGSTClass.php',
		 data: data,
		 success: function(data){
				var CallMeBaby = eval('(' + gotoProcess + ')');
						CallMeBaby(data);
		 },
		 beforeSend: function(){
				$.dimScreen(function() {
					$('#content').fadeIn();
				});
		 },
		 complete: function(){
			 $.dimScreenStop();
			 if (gotoProcess == 'prepareExportModule'){
			 	ProcessRequest({'todo':'DataSearch','ShipmentNumber':0,'SearchAction':'First'},'SearchListData');
			 }
		 }
		});
	}
	

	function msgbox(msg,focusField){
		$( "#dialog-msgbox:ui-dialog" ).dialog( "destroy" );
		$( "#dialog-msgbox" ).html(msg);
		$( "#dialog-msgbox" ).attr("title",'GST Export');
		$( "#dialog-msgbox" ).dialog('option', 'position', 'center');
		$( "#dialog-msgbox" ).dialog({
			modal: true,
			height:'auto',
			show:{ 
				effect:"blind", 
				speed:'slow' 
			},
			hide:'blind',
			buttons: {
				Ok: function() {
					$(this).dialog( "close" );
					if (focusField == 'UpdateReference' || focusField == 'ProcessData'){
							$("#btnCancel").click();
							ProcessRequest({'todo':'DataSearch','ShipmentNumber':$("#ShipmentNumber").val(),'SearchAction':'Search'},'SearchListData');
			 		} else if (focusField){
						$('#'+focusField).focus().select();
					}
				}
			}
		});
	}
	

	function adimsgbox(msg,focusField){
		$( "#dialog-msgbox:ui-dialog" ).dialog( "destroy" );
		$( "#dialog-msgbox" ).html(msg);
		$( "#dialog-msgbox" ).attr("title",'GST Export');
		$( "#dialog-msgbox" ).dialog('option', 'position', 'center');
		$( "#dialog-msgbox" ).dialog({
			modal: true,
			height:'auto',
			show:{ 
				effect:"blind", 
				speed:'slow' 
			},
			hide:'blind',
			buttons: {
				Ok: function() {
					$(this).dialog( "close" );
					if (focusField == 'UpdateADIReference' || focusField == 'ProcessADIData'){
							$("#btnCancel").click();
							//ProcessRequest({'todo':'DataSearch','ShipmentNumber':$("#ShipmentNumber").val(),'SearchAction':'Search'},'SearchListData');
			 		} else if (focusField){
						$('#'+focusField).focus().select();
					}
				}
			}
		});
	}
	
	function prepareExportModule(data){
		//var obj = $.parseJSON(data);
		var obj = new Object;
				obj = data;

		$.each(JobTypeKeys,function(a,b){
     $('#slcJobType')
         .append($("<option></option>")
         .attr("value",b)
         .text(b)); 
		});
		
		$.each(ADIJobTypeKeys,function(a,b){
     $('#adiJobType')
         .append($("<option></option>")
         .attr("value",b)
         .text(b)); 
		});
		
		var i = 0;
		
		$.each(obj[3],function(a,b){
			i=1;
			return false;
		});
		
		if (i == 1){
			GSTForwarders = [];
			$.each(obj[3],function(a,b){
				GSTForwarders.push(b);
			});
		}
		
		$.each(GSTForwarders,function(a,b){
     $('#slcForwarders')
         .append($("<option></option>")
         .attr("value",b)
         .text(b)); 
		});

		$.each(obj[0],function(a,b){
	     $('#slcCustomers')
	         .append($("<option></option>")
	         .attr("value",b['WawiIndex'])
	         .text(b['WawiAlias'])); 
		});
		
		$.each(obj[4],function(a,b){
		 $.each(b,function(c,d){
	     $('#slcDestination')
	         .append($("<option></option>")
	         .attr("value",d)
	         .text(d)); 
		 });
		});
		
		GSTRate = parseFloat(obj[1]);
	}
	
	$("#adimainDivLeft").hide();
	$("#adimainDivRight").hide();
	$("#adimainCenterDiv").hide();
	
	$('#isADI').change(function(){
		
		if ($(this).is(':checked')) {
			$("#mainDivLeft").hide();
			$("#mainDivRight").hide();
			$("#adimainDivLeft").show();
			$("#adimainDivRight").show();
			$("#adimainCenterDiv").show();
			
			//if (tblPicklistNumbers){
			//	tblPicklistNumbers.fnDestroy();
			//	tblPicklistNumbers.empty();
			//}
			
			tblShipmentNumberDetails = $('#tblShipmentNumberDetails').dataTable({
				'bJQueryUI': true,
				'bFilter': true,
				'bSortable' : false,
				'bPaginate': true,
				'aLengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
				'sPaginationType': 'full_numbers',
				"bAutoWidth" : false,
				"bScrollCollapse": true,  
				"sScrollX": "100%",  
				"bDestroy": true,      
				"aaData" : [],
				"aaSorting": [[1,'asc']],
				"aoColumns": [
					{"sTitle":"DN Number","sWidth":"90px","sClass":"alignme"},
					{"sTitle":"Tax Scheme","sWidth":"90px","sClass":"alignme"},
					{"sTitle":"Incoterm","sWidth":"80px","sClass":"alignme"},
					{"sTitle":"Ship To","sWidth":"160px","sClass":"alignme"},
					{"sTitle":"Material Number","sWidth":"120px","sClass":"alignme"},
					{"sTitle":"Shipped Quantity","sWidth":"100px","sClass":"alignme"},
					{"sTitle":"Unit Price (USD)","sWidth":"80px","sClass":"alignme"},
					{"sTitle":"Comm Inv Value of Shipment (USD)","sWidth":"120px","sClass":"alignme"},
					{"sTitle":"Vendor Invoice No","sWidth":"100px","sClass":"alignme"},
					{"sTitle":"Vendor/Supplier","sWidth":"100px","sClass":"alignme"}
				]
			});
			
			if (tblShipmentNumberDetails){
				tblShipmentNumberDetails.fnClearTable();
			}
			
			ProcessRequest({'todo':'DataADISearch','ShipmentNumber':0,'SearchAction':'First'},'SearchListADIData');
			
		} else {
			$("#adimainDivLeft").hide();
			$("#adimainDivRight").hide();
			$("#adimainCenterDiv").hide();
			$("#mainDivLeft").show();
			$("#mainDivRight").show();
			//if (tblShipmentNumberDetails){
			//	tblShipmentNumberDetails.fnDestroy();
			//	tblShipmentNumberDetails.empty();
			//}
			ProcessRequest({'todo':'DataSearch','ShipmentNumber':0,'SearchAction':'First'},'SearchListData');
		}
		
	});
	
	function SearchListADIData(data){
		
		//if (data == 'Search'){
		//	msgbox(ErrorKeys[data],'ShipReference');
		//	$("#ShipmentNumber").val(ShipmentNumber);
		//} else {
			
			if(data != 'nodata'){
				
				ShipmentNumber = data[0].ShipmentNumber;
				$("#ShipmentNumber").val(data[0].ShipmentNumber);
				
				$('#adidataDestination').val(data[0].ShippingCountry);
				$('#adidataForwarders').val(data[0].Forwarder);
				$('#adidataHAWB').val(data[0].HAWB);
				$('#adidataShipDate').val($.trim(data[0].CargoReleasedDate));
				
				$('#adidataPermitNo').val(data[0].PermitNo);
				$('#adidataPermitDate').val(data[0].PermitDate);
				$('#adidataJobType').val(data[0].JobType);
				$('#adidataRemarks').val($.trim(data[0].Comment));
				
				$('#adidataMAWB').val(data[0].MAWB);
				$('#adidataFlight').val(data[0].Flight);
				$('#adidataDepDate').val(data[0].DepDate);
				$('#adidataDeliveryFrom').val($.trim(data[0].DeliveryFrom));
				
				$("#adidataCustInvValue").val(parseFloat(data[0].CustInvValue).toFixed(2));
				$("#adidataExchangeRate").val(parseFloat(data[0].ExchangeRate).toFixed(6));
				$("#adidataInvoiceValueSGD").val(parseFloat(data[0].InvoiceValueSGD).toFixed(2));
				$("#adidataGSTAmount").val(parseFloat(data[0].GSTAmountValue).toFixed(2));
				
				if (tblShipmentNumberDetails){
					tblShipmentNumberDetails.fnClearTable();
				}
				
				$.each(data,function(a,b){
					
					tblShipmentNumberDetails.fnAddData([
						b.DN,
						b.TaxScheme,
						b.IncoTerm,
						b.ShipTo,
						b.Partnumber,
						b.TotalQty,
						b.UnitPrice,
						b.ShipmentValue,
						b.Invoice,
						b.Supplier
					]);
					
				});
				
				$("#ShipmentNumber").focus().select();
				
			} else {
				
				
				ShipmentNumber = null;
				$("#ShipmentNumber").val('');
				
				$('#adidataDestination').val('');
				$('#adidataForwarders').val('');
				$('#adidataHAWB').val(data[0].HAWB);
				$('#adidataShipDate').val('');
				
				$('#adidataPermitNo').val('');
				$('#adidataPermitDate').val('');
				$('#adidataJobType').val('');
				$('#adidataRemarks').val('');
				
				if (tblShipmentNumberDetails){
					tblShipmentNumberDetails.fnClearTable();
				}
				
				//msgbox(ErrorKeys['shipmentinvalid'],'ShipmentNumber');
				//$("#ShipmentNumber").val(ShipmentNumber);
				//$("#ShipmentNumber").focus().select();
			}
			
		//}
	}
	
	function SearchListData(data){
		if (data == 'Search'){
			msgbox(ErrorKeys[data],'ShipReference');
			$("#ShipmentNumber").val(ShipmentNumber);
		} else {
			//var obj = $.parseJSON(data);
			//console.log(data);
			var obj = data;
			ShipmentNumber = obj[0][0]['ShipmentNumber'];
			$.each(obj[0][0],function(c,d){
				if ($('#' + c).exists()) {
					if ($.trim(d) == ''){
						d = '-';
					}
					$('#' + c).val(d);
				}
			});
			
			$("#USDUnitPrice").val(parseFloat($("#USDUnitPrice").val()).toFixed(2));
			$("#CustInvValue").val(parseFloat($("#CustInvValue").val()).toFixed(2));
			$("#InvoiceValueSGD").val(parseFloat($("#InvoiceValueSGD").val()).toFixed(2));
			$("#GSTAmount").val(parseFloat((parseFloat($("#InvoiceValueSGD").val()) * GSTRate) / 100).toFixed(2));

			CurrentListArray = [];
			var tmpArrEachElements = [];
			var totalQty = 0;

			$.each(obj[1],function(a,b){
				if (b != 'nodata'){
					$.each(b,function(c,d){
						tmpArrEachElements = [];
						totalQty = totalQty + d['TotalQty'];
						$.each(d,function(f,g){
							tmpArrEachElements.push(g);
						});
						CurrentListArray.push(tmpArrEachElements);
					});
				}
			});

			//8304 multiple packlist
			UpdateAvailablePicklistOption(obj[2]);
			
			if (tblPicklistNumbers){
				tblPicklistNumbers.fnDestroy();
			}
			
			tblPicklistNumbers = $('#tblPicklistNumbers').dataTable({
					'bFilter': false,
					'bPaginate': false,
					'bJQueryUI': true,
					'aLengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
					'sPaginationType': 'full_numbers',
					"bScrollCollapse": true,      
					"bDestroy": true,      
					"aaData" : CurrentListArray,
					"aoColumns": [
						{ "sTitle": "Picklist #",'sClass':'alignme' },
						{ "sTitle": "TaxScheme",'sClass':'alignme' },
						{ "sTitle": "Total Qty",'sClass':'alignme' },
						{ "sTitle": "Value (USD)",'sClass':'alignme' }
					]
				});

				$("#tblPicklistNumbers_wrapper .fg-toolbar:first-child").html('Total Quantity: <strong>' + totalQty + '</strong>');
				$("#slcCustomers option").filter(function() {
				    return $(this).text() == $("#WawiAlias").val(); 
				}).attr('selected', true);
		}
	}
	
	function UpdateAvailablePicklistOption(PickListObj){
			CurrentAvaiLablePicklistPerCustomer = new Object;
			CurrentAvaiLablePicklistPerCustomerPrimary = new Object;
			$("#slcAvailablePlist").empty();
			if (PickListObj != 'nodata'){
				$.each(PickListObj,function(a,b){
					if (typeof CurrentAvaiLablePicklistPerCustomer[b[0]['PackListNumber']] == 'undefined'){
							CurrentAvaiLablePicklistPerCustomer[b[0]['PackListNumber']] = [];
							CurrentAvaiLablePicklistPerCustomerPrimary[b[0]['PackListNumber']] = [];
							$('#slcAvailablePlist')
							   .append($("<option></option>")
							   .attr("value",b[0]['PackListNumber'])
							   .text(b[0]['PackListNumber'])); 
					}
					
					$.each(b,function(c,d){
						CurrentAvaiLablePicklistPerCustomer[b[0]['PackListNumber']].push(d);
						CurrentAvaiLablePicklistPerCustomerPrimary[b[0]['PackListNumber']].push(d);
					})
				});
		
			$('#slcAvailablePlist option').sort(NASort).appendTo('#slcAvailablePlist');
			$('#slcAvailablePlist :first-child').attr('selected', 'selected');
		}
		
		if ($('#slcAvailablePlist option').size() < 1){
			$("#divNoAvailable").show();
			$("#divFullEdit").hide();
			$("#btnSaveUpdate").hide();
		} else {
			$("#divNoAvailable").hide();
			$("#divFullEdit").show();
			$("#btnSaveUpdate").show();
		}
	}
	
	function GetAvailablePacklistNumber(data){
		//var obj = $.parseJSON(data);
		var obj = data;
		UpdateAvailablePicklistOption(obj);
	}
	
	$("#btnFirst").click(function(){
		if ($('#isADI').is(':checked')) {
			ProcessRequest({'todo':'DataADISearch','ShipmentNumber':ShipmentNumber,'SearchAction':'First'},'SearchListADIData');
		} else {
			ProcessRequest({'todo':'DataSearch','ShipmentNumber':ShipmentNumber,'SearchAction':'First'},'SearchListData');
		}
	});

	$("#btnPrevious").click(function(){
		if ($('#isADI').is(':checked')) {
			ProcessRequest({'todo':'DataADISearch','ShipmentNumber':ShipmentNumber,'SearchAction':'Previous'},'SearchListADIData');
		} else {
			ProcessRequest({'todo':'DataSearch','ShipmentNumber':ShipmentNumber,'SearchAction':'Previous'},'SearchListData');
		}
	});
	
	$("#btnNext").click(function(){
		if ($('#isADI').is(':checked')) {
			ProcessRequest({'todo':'DataADISearch','ShipmentNumber':ShipmentNumber,'SearchAction':'Next'},'SearchListADIData');
		} else {
			ProcessRequest({'todo':'DataSearch','ShipmentNumber':ShipmentNumber,'SearchAction':'Next'},'SearchListData');
		}
	});
	
	$("#btnLast").click(function(){
		if ($('#isADI').is(':checked')) {
			ProcessRequest({'todo':'DataADISearch','ShipmentNumber':ShipmentNumber,'SearchAction':'Last'},'SearchListADIData');
		} else {
			ProcessRequest({'todo':'DataSearch','ShipmentNumber':ShipmentNumber,'SearchAction':'Last'},'SearchListData');
		}
	});
	
	$("#btnNew").click(function(){
		$("#slcCustomers").attr("disabled", false);
		$("#slcCustomers :first-child").attr('selected','selected');
		$("#divMainBody").slideUp();
		$("#divNewEditBody").slideDown();
		$("#btnSaveUpdate").val('Save');
		$('#EditShipmentNumber').hide();
		
		$.each($("#leftDiv ol").find("input").filter("[type=text]"),function(a,b){
			$(b).val('');
		});
		$.each($("#rightDiv ol").find("input").filter("[type=text]"),function(a,b){
			$(b).val('');
		});
		
		$.each($("#adileftDiv ol").find("input").filter("[type=text]"),function(a,b){
			$(b).val('');
		});
		$.each($("#adirightDiv ol").find("input").filter("[type=text]"),function(a,b){
			$(b).val('');
		});
		
		$("#slcDestination :first-child").attr('selected','selected');
		$("#slcForwarders :first-child").attr('selected','selected');
		$("#slcJobType :first-child").attr('selected','selected');

		$("#txtDepDate").val(CurrentDate);
		$("#txtShipDate").val(CurrentDate);
		
		$("#ADIdivNoAvailable").hide();
		$("#ADIdivFullEdit").hide();
		$("#txtShipment").val("").attr("readonly", false);
		$("#txtShipment").select2();
		
		if ($('#slcAvailablePlist option').size() < 1){
			$("#divNoAvailable").show();
			$("#divFullEdit").hide();
			$("#btnSaveUpdate").hide();
		} else {
			$("#divNoAvailable").hide();
			$("#divFullEdit").show();
			$("#btnSaveUpdate").show();
		}
		
		$("#txtUSDUnitPrice").val('0.00');
		$("#txtCustInvValue").val('0.00');
		$("#txtExchangeRate").val('1.00');
		$("#txtInvoiceValueSGD").val('0.00');
		$("#txtGSTAmount").val('0.00');
		
		if ($('#isADI').is(':checked')) {
			
			$("#divNoAvailable").hide();
			$("#divFullEdit").hide();
			$("#slcCustomers").val('ADISGWMS');
		
			$("#adiCustInvValue").val('0.00');
			$("#adiExchangeRate").val('1.00');
			$("#adiInvoiceValueSGD").val('0.00');
			$("#adiGSTAmount").val('0.00');
			
			ProcessRequest({'todo':'GetAvailableShipmentNumber','WawiIndex':$('#slcCustomers').val()},'GetAvailableShipmentNumber');
			
		}
		
	});
	
	function ADIGetEditShipment(data){
		if(data != 'nodata'){
			
			ShipmentNumber = data[0].ShipmentNumber;
			$("#ShipmentNumber").val(data[0].ShipmentNumber);
			
			$('#adiDestination').val(data[0].ShippingCountry);
			$('#adiForwarders').val(data[0].Forwarder);
			$('#adiHAWB').val(data[0].HAWB);
			$('#adiShipDate').val($.trim(data[0].CargoReleasedDate));
			
			$('#adiPermitNo').val(data[0].PermitNo);
			$('#adiPermitDate').val(data[0].PermitDate);
			$('#adiJobType').val(data[0].JobType);
			$('#adiRemarks').val($.trim(data[0].Comment));
			
			$('#adiMAWB').val(data[0].MAWB);
			$('#adiFlight').val(data[0].Flight);
			$('#adiDepDate').val(data[0].DepDate);
			$('#adiDeliveryFrom').val(data[0].DeliveryFrom);
			
			$("#adiCustInvValue").val(parseFloat(data[0].CustInvValue).toFixed(2));
			$("#adiExchangeRate").val(parseFloat(data[0].ExchangeRate).toFixed(6));
			$("#adiInvoiceValueSGD").val(parseFloat(data[0].InvoiceValueSGD).toFixed(2));
			$("#adiGSTAmount").val(parseFloat(data[0].GSTAmountValue).toFixed(2));
			
			if (tblShipmentNumberEditing){
				tblShipmentNumberEditing.fnDestroy();
			}
		
			tblShipmentNumberEditing = $('#tblShipmentNumberEditing').dataTable({
				//"sDom": '<"H">Tlfr<"F"tip>',
				'bJQueryUI': true,
				'bFilter': true,
				'bSortable' : false,
				'bPaginate': true,
				'aLengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
				'sPaginationType': 'full_numbers',
				"bAutoWidth" : false,
				"bScrollCollapse": true,  
				"sScrollX": "100%",  
				"bDestroy": true,      
				"aaData" : [],
				"aaSorting": [[1,'asc']],
				"aoColumns": [
					{"sTitle":"DN Number","sWidth":"90px","sClass":"alignme"},
					{"sTitle":"Tax Scheme","sWidth":"90px","sClass":"alignme"},
					{"sTitle":"Incoterm","sWidth":"80px","sClass":"alignme"},
					{"sTitle":"Ship To","sWidth":"160px","sClass":"alignme"},
					{"sTitle":"Material Number","sWidth":"120px","sClass":"alignme"},
					{"sTitle":"Shipped Quantity","sWidth":"100px","sClass":"alignme"},
					{"sTitle":"Unit Price (USD)","sWidth":"80px","sClass":"alignme"},
					{"sTitle":"Comm Inv Value of Shipment (USD)","sWidth":"120px","sClass":"alignme"},
					{"sTitle":"Vendor Invoice No","sWidth":"100px","sClass":"alignme"},
					{"sTitle":"Vendor/Supplier","sWidth":"100px","sClass":"alignme"}
				]
			});
			
			if (tblShipmentNumberEditing){
				tblShipmentNumberEditing.fnClearTable();
			}
			
			$.each(data,function(a,b){
				
				tblShipmentNumberEditing.fnAddData([
					b.DN,
					b.TaxScheme,
					b.IncoTerm,
					b.ShipTo,
					b.Partnumber,
					b.TotalQty,
					b.UnitPrice,
					b.ShipmentValue,
					b.Invoice,
					b.Supplier
				]);
				
			});
			
			$("#ShipmentNumber").focus().select();
			
		} else {
			msgbox(ErrorKeys['shipmentinvalid'],'ShipmentNumber');
			$("#ShipmentNumber").val(ShipmentNumber);
			$("#ShipmentNumber").focus().select();
		}
	}
	
	$("#btnEdit").click(function(){
		
		if ($('#isADI').is(':checked')) {
			
			if(ShipmentNumber != null){
			
				$("#divNoAvailable").hide();
				$("#divFullEdit").hide();
				$("#ADIdivNoAvailable").hide();
				$("#ADIdivFullEdit").show();
				$("#btnSaveUpdate").show();
				
				$('#EditShipmentNumber').hide();
				
				$("#btnSaveUpdate").val('Update');
				$("#divMainBody").slideUp();
				$("#divNewEditBody").slideDown();
				
				$("#slcCustomers").val('ADISGWMS').attr("disabled", true);
				
				if($('#txtShipment').hasClass("select2-hidden-accessible")) {
					$("#txtShipment").select2('destroy');
				}
				$("#txtShipment").hide();
				$("#txtShipmentEdit").show().val(ShipmentNumber).attr("readonly", true);
				
				ProcessRequest({'todo':'ADIGetEditShipment','ShipmentNumber':ShipmentNumber,'WawiIndex':$('#slcCustomers').val()},'ADIGetEditShipment');
				
			} else {
				msgbox("Please insert Shipment Number to edit.",'ShipmentNumber');
			}
			
		} else {
			
			$("#slcCustomers").attr("disabled", true);
			$('#EditShipmentNumber').show();
			$.each($("#mainDivLeft ol").find("input").filter("[type=text]"),function(a,b){
					var elemNametmp = 'txt' + $(b).attr('id');
					var inputValue = $.trim($(b).val());
					if ($('#' + elemNametmp).exists()) {
						if (inputValue == ''){
							inputValue = '-';
						}
						$('#' + elemNametmp).val(inputValue);
					}
			});
			
			$.each($("#mainDivRight ol").find("input").filter("[type=text]"),function(a,b){
					var elemNametmp = 'txt' + $(b).attr('id');
					var inputValue = $.trim($(b).val());
					if ($('#' + elemNametmp).exists()) {
						if (inputValue == ''){
							inputValue = '-';
						}
						$('#' + elemNametmp).val(inputValue);
					}
			});
			
			$('#txtShipmentNumber').val($('#ShipmentNumber').val());
			$("#slcDestination option:contains(" + $("#Destination").val() + ")").attr('selected', 'selected');
			$("#slcJobType option:contains(" + $("#JobType").val() + ")").attr('selected', 'selected');
			
			$("#slcForwarders option").filter(function() {
				return $(this).text() == $("#Forwarders").val(); 
			}).attr('selected', true);
			
			if (tblPicklistNumbersEditing){
				tblPicklistNumbersEditing.fnDestroy();
			}
			
			tblPicklistNumbersEditing = $('#tblPicklistNumbersEditing').dataTable({
						'bFilter': false,
						'bPaginate': false,
						'bJQueryUI': true,
						'aLengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
						'sPaginationType': 'full_numbers',
						"bScrollCollapse": true,      
						"bDestroy": true,      
						"aaData" : [],
						"aaSorting": [[1,'asc']],
						"aoColumns": [
							{ "sTitle": "Tool",'sClass':'alignme' },
							{ "sTitle": "Picklist #",'sClass':'alignme' },
							{ "sTitle": "TaxScheme",'sClass':'alignme','sWidth':'100' },
							{ "sTitle": "Total Qty",'sClass':'alignme','sWidth':'80px' },
							{ "sTitle": "Value (USD)",'sClass':'alignme','sWidth':'85px' }
						]
					});

			var qty = 0;
			var TmpBArray = [];
			$.each(tblPicklistNumbers.fnGetData(),function(a,b){
				qty = qty + b[2];
				TmpBArray.push("<span style=\"cursor:pointer\" onclick=\"removeThisPacklist(this," + b[0] + ")\">Remove</span>");
				$.each(b,function(c,d){
					TmpBArray.push(d);
				});
				
				var tmpRow = tblPicklistNumbersEditing.fnAddData(TmpBArray);
				tblPicklistNumbersEditing.fnGetNodes(tmpRow).cells[1].setAttribute("class", b[0] + ' alignme');
				tblPicklistNumbersEditing.fnGetNodes(tmpRow).cells[4].setAttribute("class", 'classEditUSDValue alignme');
				TmpBArray = [];
			});
					
			$("#tblPicklistNumbersEditing_wrapper .fg-toolbar:first-child").html('Total Quantity: <strong>' + qty + '</strong>');
			RefreshListReference();
			
		
			$("#ADIdivNoAvailable").hide();
			$("#ADIdivFullEdit").hide();
			
			$("#divNoAvailable").hide();
			$("#divFullEdit").show();
			$("#btnSaveUpdate").show();
			
			$("#btnSaveUpdate").val('Update');
			$("#divMainBody").slideUp();
			$("#divNewEditBody").slideDown();
			
		}
		
	});
	
	$("#btnCancel").click(function(){
		$("#divMainBody").slideDown();
		$("#divNewEditBody").slideUp();
		$("#txtShipment").show().html('');
		$("#txtShipmentEdit").hide().val('');
		CurrentAvaiLablePicklistPerCustomer = new Object();
		CurrentAvaiLablePicklistPerCustomer = CurrentAvaiLablePicklistPerCustomerPrimary;
	});
	
	$("#ShipmentNumber").keypress(function(event){
		if (event.keyCode == 13){
			if ($.trim($("#ShipmentNumber").val()) == ''){
				msgbox(ErrorKeys['ErrorSearchingShipment'],'ShipReference');
			} else {
				if ($('#isADI').is(':checked')) {
					ProcessRequest({'todo':'DataADISearch','ShipmentNumber':$("#ShipmentNumber").val(),'SearchAction':'Search'},'SearchListADIData');
				} else {
					ProcessRequest({'todo':'DataSearch','ShipmentNumber':$("#ShipmentNumber").val(),'SearchAction':'Search'},'SearchListData');
				}
			}
		}
	});
	
	$("#txtShipment").change(function(){
		if ($.trim($("#txtShipment").val()) == ''){
			msgbox(ErrorKeys['ErrorSearchingShipment'],'txtShipment');
		} else {
			ProcessRequest({'todo':'ADIGetShipment','ShipmentNumber':$("#txtShipment").val(),'WawiIndex':$('#slcCustomers').val()},'ADIGetShipment');
		}
	});
	//$("#txtShipment").keypress(function(event){
	//	if (event.keyCode == 13){
	//		if ($.trim($("#txtShipment").val()) == ''){
	//			msgbox(ErrorKeys['ErrorSearchingShipment'],'txtShipment');
	//		} else {
	//			ProcessRequest({'todo':'ADIGetShipment','ShipmentNumber':$("#txtShipment").val(),'WawiIndex':$('#slcCustomers').val()},'ADIGetShipment');
	//		}
	//	}
	//});
	
	function ADIGetShipment(data){
		var obj = data;
		
		if(obj != "nodata"){
			
			$('#adiDestination').val(data[0].ShippingCountry);
			$('#adiForwarders').val(data[0].Forwarder);
			$('#adiHAWB').val(data[0].HAWB);
			$('#adiShipDate').val($.trim(data[0].CargoReleasedDate));
			
			if (tblShipmentNumberEditing){
				tblShipmentNumberEditing.fnClearTable();
			}
			
			$.each(data,function(a,b){
				
				tblShipmentNumberEditing.fnAddData([
					b.DN,
					b.TaxScheme,
					b.IncoTerm,
					b.ShipTo,
					b.Partnumber,
					b.TotalQty,
					b.UnitPrice,
					b.ShipmentValue,
					b.Invoice,
					b.Supplier
				]);
				
			});
			
			
			//----GST Calculation----//
			
			var adiCustInvValue = parseFloat('0.00').toFixed(2);
			var adiInvoiceValueSGD = parseFloat('0.00').toFixed(2);
			var adiGSTAmount = parseFloat('0.00').toFixed(2);
			
			if ($.trim($('#adiExchangeRate').val()) == ''){
				$('#adiExchangeRate').val('1.00');
			}
			
			$("#adiCustInvValue").val('0.00');
			
			$.each(data,function(a,b){
				
				var smtValue = b.ShipmentValue;
				adiCustInvValue = parseFloat(adiCustInvValue) + parseFloat(smtValue);
				
			});
			
			adiInvoiceValueSGD = parseFloat(parseFloat($("#adiExchangeRate").val()) * parseFloat(adiCustInvValue));
			adiGSTAmount = parseFloat((parseFloat(adiInvoiceValueSGD) * GSTRate) / 100);
			
			$("#adiCustInvValue").val(parseFloat(adiCustInvValue).toFixed(2));
			$("#adiInvoiceValueSGD").val(parseFloat(adiInvoiceValueSGD).toFixed(2));
			$("#adiGSTAmount").val(parseFloat(adiGSTAmount).toFixed(2));
			
			$("#adiPermitNo").focus().select();
			
		} else {
			msgbox(ErrorKeys['InvalidSearchingShipment'],'txtShipment');
			return false;
		}
	}
	
	$("#adiExchangeRate").keypress(function(event){
		if (event.keyCode == 13){
			if ($.trim($("#adiExchangeRate").val()) == ''){
				$("#adiExchangeRate").val('1.00');
			}
			$("#adiInvoiceValueSGD").val(parseFloat(parseFloat($("#adiExchangeRate").val()) * parseFloat($("#adiCustInvValue").val())).toFixed(2));
			$("#adiGSTAmount").val(parseFloat((parseFloat($("#adiInvoiceValueSGD").val()) * GSTRate) / 100).toFixed(2));
		}
	});
	
	
	$("#slcCustomers").change(function(){
		
		if($("#slcCustomers").val() == 'ADISGWMS'){
			
			$("#divNoAvailable").hide();
			$("#divFullEdit").hide();
			
			ProcessRequest({'todo':'GetAvailableShipmentNumber','WawiIndex':$('#slcCustomers').val()},'GetAvailableShipmentNumber');
			
		} else {
			
			$("#ADIdivNoAvailable").hide();
			$("#ADIdivFullEdit").hide();
			
			if (tblPicklistNumbersEditing){
				tblPicklistNumbersEditing.fnDestroy();
			}
		
			tblPicklistNumbersEditing = $('#tblPicklistNumbersEditing').dataTable({
				'bFilter': false,
				'bPaginate': false,
				'bJQueryUI': true,
				'aLengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
				'sPaginationType': 'full_numbers',
				"bScrollCollapse": true,      
				"bDestroy": true,      
				"aaData" : [],
				"aaSorting": [[1,'asc']],
				"aoColumns": [
					{ "sTitle": "Tool",'sClass':'alignme' },
					{ "sTitle": "Picklist #",'sClass':'alignme' },
					{ "sTitle": "TaxScheme",'sClass':'alignme','sWidth':'100' },
					{ "sTitle": "Total Qty",'sClass':'alignme','sWidth':'80px' },
					{ "sTitle": "Value (USD)",'sClass':'alignme','sWidth':'85px' }
				]
			});
			
			$('#slcAvailablePlist').empty();
			if (tblPicklistNumbersEditing){
				tblPicklistNumbersEditing.fnClearTable();
			}
			
			ProcessRequest({'todo':'GetAvailablePacklistNumber','WawiIndex':$('#slcCustomers').val()},'GetAvailablePacklistNumber');
			
		}
		
	});
	
	function GetAvailableShipmentNumber(data){
		var obj = data;
		
		$('#txtShipment').html('');
		
		if(obj != "nodata"){
			
			$('#txtShipment').append($("<option></option>").attr("value","").text("Select here"))
			$.each(obj,function(a,b){
				console.log(b);
				$('#txtShipment')
					.append($("<option></option>")
					.attr("value",b['ShipmentNumber'])
					.text(b['ShipmentNumber'])); 
			});
			
			$("#ADIdivNoAvailable").hide();
			$("#ADIdivFullEdit").show();
			$("#btnSaveUpdate").show();
			
			
			if (tblShipmentNumberEditing){
				tblShipmentNumberEditing.fnDestroy();
			}
		
			tblShipmentNumberEditing = $('#tblShipmentNumberEditing').dataTable({
				//"sDom": '<"H">Tlfr<"F"tip>',
				'bJQueryUI': true,
				'bFilter': true,
				'bSortable' : false,
				'bPaginate': true,
				'aLengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
				'sPaginationType': 'full_numbers',
				"bAutoWidth" : false,
				"bScrollCollapse": true,  
				"sScrollX": "100%",  
				"bDestroy": true,      
				"aaData" : [],
				"aaSorting": [[1,'asc']],
				"aoColumns": [
					{"sTitle":"DN Number","sWidth":"90px","sClass":"alignme"},
					{"sTitle":"Tax Scheme","sWidth":"90px","sClass":"alignme"},
					{"sTitle":"Incoterm","sWidth":"80px","sClass":"alignme"},
					{"sTitle":"Ship To","sWidth":"160px","sClass":"alignme"},
					{"sTitle":"Material Number","sWidth":"120px","sClass":"alignme"},
					{"sTitle":"Shipped Quantity","sWidth":"100px","sClass":"alignme"},
					{"sTitle":"Unit Price (USD)","sWidth":"80px","sClass":"alignme"},
					{"sTitle":"Comm Inv Value of Shipment (USD)","sWidth":"120px","sClass":"alignme"},
					{"sTitle":"Vendor Invoice No","sWidth":"100px","sClass":"alignme"},
					{"sTitle":"Vendor/Supplier","sWidth":"100px","sClass":"alignme"}
				]
			});
			
			$('#slcAvailablePlist').empty();
			if (tblShipmentNumberEditing){
				tblShipmentNumberEditing.fnClearTable();
			}
			
		} else {
			$("#ADIdivFullEdit").hide();
			$("#ADIdivNoAvailable").show();
			$("#btnSaveUpdate").hide();
		}
		
	}
	
	$("#txtHAWB").keypress(function(event){
		if (event.keyCode == 13){
			if ($.trim($("#txtHAWB").val()) == ''){
				$("#txtHAWB").val('-');
			}
			$("#txtMAWB").focus().select();
		}
	});
	
	$("#txtMAWB").keypress(function(event){
		if (event.keyCode == 13){
			if ($.trim($("#txtMAWB").val()) == ''){
				$("#txtMAWB").val('-');
			}
			$("#txtFlight").focus().select();
		}
	});
	
	$("#txtFlight").keypress(function(event){
		if (event.keyCode == 13){
			if ($.trim($("#txtFlight").val()) == ''){
				$("#txtFlight").val('-');
			}
			$("#txtDepDate").focus().select();
		}
	});
	
	$("#slcJobType").change(function(){
		$("#txtRemarks").focus().select();
	});
	
	$("#txtRemarks").keypress(function(event){
		if (event.keyCode == 13){
			if ($.trim($("#txtRemarks").val()) == ''){
				$("#txtRemarks").val('-');
			}
			$("#txtCustInvNo").focus().select();
		}
	});

	$("#txtCustInvNo").keypress(function(event){
		if (event.keyCode == 13){
			if ($.trim($("#txtCustInvNo").val()) == ''){
				$("#txtCustInvNo").val('-');
			}
			$("#txtPermitNo").focus().select();
		}
	});
	
	$("#txtPermitNo").keypress(function(event){
		if (event.keyCode == 13){
			if ($.trim($("#txtPermitNo").val()) == ''){
				$("#txtPermitNo").val('-');
			}
			$("#txtUSDUnitPrice").focus().select();
		}
	});
	
	$("#txtUSDUnitPrice").keypress(function(event){
		if (event.keyCode == 13){
			if ($.trim($("#txtUSDUnitPrice").val()) == ''){
				$("#txtUSDUnitPrice").val('0.00');
			}
			$("#txtExchangeRate").focus().select();
		}
	});
	
	$("#txtExchangeRate").keypress(function(event){
		if (event.keyCode == 13){
			if ($.trim($("#txtExchangeRate").val()) == ''){
				$("#txtExchangeRate").val('1.00');
			}
			$("#txtExchangeRate").val(parseFloat($("#txtExchangeRate").val()).toFixed(5));
			updateUSDValueField();
			$("#btnAddToList").focus();
		}
	});
	
	$('#btnAddToList').click(function(){
		if ($.trim($('#slcAvailablePlist').val()) != ''){
			var arrTmp;
			var TmpCrntPacklist = $('#slcAvailablePlist').val();
			$.each(CurrentAvaiLablePicklistPerCustomer[TmpCrntPacklist],function(a,b){
				arrTmp = [];
				arrTmp.push("<span style=\"cursor:pointer\" onclick=\"removeThisPacklist(this," + TmpCrntPacklist + ")\">Remove</span>");
				$.each(b,function(c,d){
					arrTmp.push(d);
				});
				arrTmp.push('0.00');
				var tmpRow = tblPicklistNumbersEditing.fnAddData(arrTmp);
				tblPicklistNumbersEditing.fnGetNodes(tmpRow).cells[1].setAttribute("class", TmpCrntPacklist + ' alignme');
				tblPicklistNumbersEditing.fnGetNodes(tmpRow).cells[4].setAttribute("class", 'classEditUSDValue alignme');
			});
			ProcessRequest('todo=getPacklistData&slcCustomers='+$('#slcCustomers').val()+'&packlistnumber='+TmpCrntPacklist,'autoPoppulateData');
			RefreshListReference();
			delete CurrentAvaiLablePicklistPerCustomer[TmpCrntPacklist];
			$("#slcAvailablePlist option[value='" + TmpCrntPacklist + "']").remove();
			UpdateTableQty();
		}
	});
	
	function autoPoppulateData(data){
		$('#txtHAWB').val(data[0].hawb);
		$('#txtMAWB').val(data[0].mawb);
		$('#txtFlight').val(data[0].flight);
		$('#slcForwarders').val($.trim(data[0].forwardingagent));
		$('#slcDestination').val(data[0].destination);
		$('#txtPermitNo').val(data[0].permitno);
		$('#txtDepDate').val(data[0].etd);
		if(!$('#txtCustInvNo').val()){
			$('#txtCustInvNo').val(data[0].Customerreference);
		}else{
			$('#txtCustInvNo').val($('#txtCustInvNo').val()+'/'+data[0].Customerreference);
		}
	}
	
	function UpdateTableQty(){
		var qty = 0;
		$.each(tblPicklistNumbersEditing.fnGetNodes(),function(a,b){
			qty = qty + parseInt(tblPicklistNumbersEditing.fnGetData(a)[3]);
		});
		$("#tblPicklistNumbersEditing_wrapper .fg-toolbar:first-child").html('Total Quantity: <strong>' + qty + '</strong>');
	}
	
	function RefreshListReference(){
		$('.classEditUSDValue', tblPicklistNumbersEditing.fnGetNodes()).editable(
		 	function(value1, settings) {
				return value1;
		  },
		  	{"callback": function( sValue, y ) {
			  	if (sValue == ''){
			  		sValue = '0.00';
			  	}
			  	var aPos = tblPicklistNumbersEditing.fnGetPosition(this);
			  	tblPicklistNumbersEditing.fnUpdate(sValue,aPos[0],aPos[1],0,0);
			  	updateUSDValueField();
		  	},
		  	"width": "80px","height":"15px"}
		);
	}
	
	function updateUSDValueField(){
		
		if ($.trim($('#txtExchangeRate').val()) == ''){
			$('#txtExchangeRate').val('1.00');
		}
		
		if ($('#btnSaveUpdate').val() == 'Update' && parseInt($('#txtGSTAmount').val()) != 0 && tblPicklistNumbersEditing.fnSettings().fnRecordsTotal() == 0){
		} else {
			$("#txtCustInvValue").val('0.00');
			$.each($('.classEditUSDValue', tblPicklistNumbersEditing.fnGetNodes()),function(a,b){
				var tmpValue;
				if (!$.isNumeric($(b).html())){
					tmpValue = parseFloat('0.00').toFixed(2);
					tblPicklistNumbersEditing.fnUpdate(tmpValue, tblPicklistNumbersEditing.fnGetPosition(b)[0],4);
				} else {
					tmpValue = parseFloat($(b).html());
					tblPicklistNumbersEditing.fnUpdate(tmpValue.toFixed(2), tblPicklistNumbersEditing.fnGetPosition(b)[0],4);
				}
				$("#txtCustInvValue").val(parseFloat(parseFloat($("#txtCustInvValue").val()) + tmpValue).toFixed(2));
			});
		}
		$("#txtInvoiceValueSGD").val(parseFloat(parseFloat($("#txtExchangeRate").val()) * parseFloat($("#txtCustInvValue").val())).toFixed(2));
		$("#txtGSTAmount").val(parseFloat((parseFloat($("#txtInvoiceValueSGD").val()) * GSTRate) / 100).toFixed(2));
		
		//$("#tblPicklistNumbers_wrapper .fg-toolbar:first-child").html('Total Quantity: <strong>' + totalQty + '</strong>');
	}
	
	function ProcessData(data){
		//console.log(data);
		//var obj = jQuery.parseJSON(data);
		var obj = data;
		var msg = AddUpdateMSG[obj[1]];
		var passToMsgBox = '';
		
		if (obj[1] == 'successADD' || obj[1] == 'successUPDATE'){
			$("#ShipmentNumber").val(obj[0]);
			if (tblPicklistNumbersEditing.fnGetData().length > 0){
				var sendme = [];
				$.each(tblPicklistNumbersEditing.fnGetData(),function(a,b){
					sendme.push(b[1] + '-' + b[4] + '-' + b[2] + '-' + b[3] + '-' + $('#txtInvoiceValueSGD').val()); // picklist #,InvoiceValue,TaxScheme,Qty,InvoiceValueSGD
				});
				
				ProcessRequest({'todo':'UpdateReference','ShipReference':obj[0],'ListRef':sendme,'action':obj[1]},'UpdateReference');
				return false;
			}
			passToMsgBox = 'ProcessData';
		} else {
			passToMsgBox = 'ErrorProcessData';
		}
		
		msg = msg.replace('<shipreference>',obj[0]);
		if (obj[2] != undefined){
			msg = msg.replace('<DNnotExist>',obj[2]);
		}
		msgbox(msg,passToMsgBox);
	}

	function UpdateReference(data){
		//var obj = jQuery.parseJSON(data);
		var obj = data;
		var msg = AddUpdateMSG[obj[1]];
		msg = msg.replace('<shipreference>',obj[0]);
		msgbox(msg,'UpdateReference');
	}
	
	function UpdateReferenceNull(){
		updateUSDValueField();
		UpdateTableQty();
		tblPicklistNumbers.fnClearTable();
		var tmparray = [];
		var totalQty = 0;
		$.each(tblPicklistNumbersEditing.fnGetData(),function(a,b){
			totalQty = totalQty + b[3];
			tmparray.push(b[1]);
			tmparray.push(b[2]);
			tmparray.push(b[3]);
			tmparray.push(b[4]);
			tblPicklistNumbers.fnAddData(tmparray);
			tmparray = [];
		});
		$("#tblPicklistNumbers_wrapper .fg-toolbar:first-child").html('Total Quantity: <strong>' + totalQty + '</strong>');
	}
	
	$("#btnSaveUpdate").click(function(){
		// if success on saving CurrentAvaiLablePicklistPerCustomerPrimary = CurrentAvaiLablePicklistPerCustomer
		
		if($("#slcCustomers").val() == 'ADISGWMS'){
		
			if ($('#adiPermitNo').val()=='' || $('#adiPermitNo').val()=='-'){
				msgbox('Required Field: Please enter <strong>Permit No</strong>','adiPermitNo');
				return false;
			}
			
			if (tblShipmentNumberEditing){
				if (tblShipmentNumberEditing.fnGetData().length == 0){
					msgbox('Please retry to retrieve the Shipment DN Details.','txtShipment');
					return false;
				}
			}

			var Params = new Object;
			$.each($('.globalform div#ADIdivFullEdit').find('input,select').serializeArray(),function(a,b){
				if ($.trim(b.value) == ''){
					Params[b.name] = '-';
					$('#' + b.name).val('-');
				} else {
					Params[b.name] = b.value;
				}
			});
			
			Params['txtShipmentNumber'] = $("#txtShipment").val();
			Params['slcCustomers'] = $("#slcCustomers").val();
			Params['btnSaveUpdate'] = $("#btnSaveUpdate").val();
			Params['todo'] = 'ProcessADIData';
			ProcessRequest(Params,'ProcessADIData');
			
		} else {
		
			if ($('#txtCustInvNo').val()=='' || $('#txtCustInvNo').val()=='-'){
				msgbox('Required Field: Please enter <strong>Cust Inv No</strong>','txtCustInvNo');
				return false;
			}
			
			if (tblPicklistNumbersEditing){
				if (tblPicklistNumbersEditing.fnGetData().length == 0){
					msgbox('Required Field: Please select <strong>Picklist</strong>','slcAvailablePlist');
					return false;
				}
			}
			
			updateUSDValueField();

			var Params = new Object;
			$.each($('.globalform div#divFullEdit').find('input,select').serializeArray(),function(a,b){
				if ($.trim(b.value) == ''){
					Params[b.name] = '-';
					$('#' + b.name).val('-');
				} else {
					Params[b.name] = b.value;
				}
			});
			
			Params['txtShipmentNumber'] = $("#txtShipmentNumber").val();
			Params['slcCustomers'] = $("#slcCustomers").val();
			Params['btnSaveUpdate'] = $("#btnSaveUpdate").val();
			Params['todo'] = 'ProcessData';
			ProcessRequest(Params,'ProcessData');
			
		}
		
	});
	
	function ProcessADIData(data){
		//console.log(data);
		//var obj = jQuery.parseJSON(data);
		var obj = data;
		var msg = AddUpdateMSG[obj[1]];
		var passToMsgBox = '';
		
		if (obj[1] == 'successADD' || obj[1] == 'successUPDATE'){
			$("#ShipmentNumber").val(obj[0]);
			passToMsgBox = 'ProcessADIData';
		} else {
			passToMsgBox = 'ErrorProcessData';
		}
		
		msg = msg.replace('<shipreference>',obj[0]);
		adimsgbox(msg,passToMsgBox);
		
		if($("#slcCustomers").val() == 'ADISGWMS'){
			
			$("#isADI").prop('checked', true)
			$("#mainDivLeft").hide();
			$("#mainDivRight").hide();
			$("#adimainDivLeft").show();
			$("#adimainDivRight").show();
			$("#adimainCenterDiv").show();
			
			if (tblShipmentNumberDetails){
				tblShipmentNumberDetails.fnDestroy();
			}
			
			tblShipmentNumberDetails = $('#tblShipmentNumberDetails').dataTable({
				'bJQueryUI': true,
				'bFilter': true,
				'bSortable' : false,
				'bPaginate': true,
				'aLengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
				'sPaginationType': 'full_numbers',
				"bAutoWidth" : false,
				"bScrollCollapse": true,  
				"sScrollX": "100%",  
				"bDestroy": true,      
				"aaData" : [],
				"aaSorting": [[1,'asc']],
				"aoColumns": [
					{"sTitle":"DN Number","sWidth":"90px","sClass":"alignme"},
					{"sTitle":"Tax Scheme","sWidth":"90px","sClass":"alignme"},
					{"sTitle":"Incoterm","sWidth":"80px","sClass":"alignme"},
					{"sTitle":"Ship To","sWidth":"160px","sClass":"alignme"},
					{"sTitle":"Material Number","sWidth":"120px","sClass":"alignme"},
					{"sTitle":"Shipped Quantity","sWidth":"100px","sClass":"alignme"},
					{"sTitle":"Unit Price (USD)","sWidth":"80px","sClass":"alignme"},
					{"sTitle":"Comm Inv Value of Shipment (USD)","sWidth":"120px","sClass":"alignme"},
					{"sTitle":"Vendor Invoice No","sWidth":"100px","sClass":"alignme"},
					{"sTitle":"Vendor/Supplier","sWidth":"100px","sClass":"alignme"}
				]
			});
			
			if (tblShipmentNumberDetails){
				tblShipmentNumberDetails.fnClearTable();
			}
			
			ProcessRequest({'todo':'DataADISearch','ShipmentNumber':$("#ShipmentNumber").val(),'SearchAction':'Search'},'SearchListADIData');
		}
		
	}
	
	$('#txtsearchplist').keypress(function(event){
		$("#slcAvailablePlist option:contains(" + $("#txtsearchplist").val() + ")").attr('selected', 'selected');
	});
	
	removeThisPacklist = function(obj,ClassPicklist){
		$.each($('.' + ClassPicklist, tblPicklistNumbersEditing.fnGetNodes()),function(a,b){
			tblPicklistNumbersEditing.fnDeleteRow(tblPicklistNumbersEditing.fnGetPosition(b)[0]);
		});
		
		$('#slcAvailablePlist')
		   .append($("<option></option>")
		   .attr("value",ClassPicklist)
		   .text(ClassPicklist));
		$('#slcAvailablePlist option').sort(NASort).appendTo('#slcAvailablePlist');
		if (typeof CurrentAvaiLablePicklistPerCustomer[ClassPicklist] == 'undefined'){
			CurrentAvaiLablePicklistPerCustomer[ClassPicklist] = [];
		}
		CurrentAvaiLablePicklistPerCustomer[ClassPicklist] = CurrentAvaiLablePicklistPerCustomerPrimary[ClassPicklist];
		ProcessRequest({'todo':'UpdateReferenceNull','RefNum':ClassPicklist},'UpdateReferenceNull');
	}
	
	ProcessRequest({'todo':'prepareExportModule'},'prepareExportModule');
});	
</script>

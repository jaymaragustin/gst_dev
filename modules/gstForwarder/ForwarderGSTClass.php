<?php
if (isset($_POST['todo'])){
	require ('../../include/config.php');
	$ForwarderGSTClass = new ForwarderGSTClass($_REQUEST);
	
	if ($_POST['todo'] == 'AddForwarder'){
		echo $ForwarderGSTClass->AddForwarder();
	} elseif ($_POST['todo'] == 'ListAllForwarder'){
		echo $ForwarderGSTClass->ListAllForwarder();
	} elseif ($_POST['todo'] == 'ChangeStatus'){
		echo $ForwarderGSTClass->ChangeStatus();
	}
}

Class ForwarderGSTClass{
	var $PostVars;
	
	function __construct($vars){
		$this->PostVars = $vars;
		$serverConn = unserialize(base64_decode(SQL_CONN));
		$this->conn = mssql_connect($serverConn[0],$serverConn[1],$serverConn[2]);
		mssql_select_db("Import");
	}
	
	function mssqlQuotedString($str){
		$str = str_replace("\\", "\\\\", $str);
		$str = str_replace("'", "''", $str);
		
		return $str;
	}
	
	function ListAllForwarder(){
		$sql = mssql_query("select Forwarder,Case when [Status] = 1 then 'Deactivate' ELSE 'Activate' END as 'Status' from Forwarder");
		while($data[] = mssql_fetch_assoc($sql)){}
		array_pop($data);
		
		return json_encode($data);
	}
	
	function addForwarder(){
		if ($this->PostVars['Type'] == 'Update'){
			$sql = mssql_query("select Forwarder from Forwarder where Forwarder like '".$this->mssqlQuotedString($this->PostVars['Forwarder'])."'");
			if (mssql_num_rows($sql) > 0){
				return "updateexists";
			} else {
				if (mssql_query("update Forwarder set Forwarder = '".$this->mssqlQuotedString($this->PostVars['Forwarder'])."' where Forwarder like '".$this->mssqlQuotedString($this->PostVars['Old'])."'")){
					return "successUpdate";
				} else {
					return "errorUpdate";
				}
			}
		} else {
			$sql = mssql_query("select Forwarder from Forwarder where Forwarder like '".$this->mssqlQuotedString($this->PostVars['Forwarder'])."'");
			if (mssql_num_rows($sql) > 0){
				return "exists";
			} else {
				if (mssql_query("Insert into Forwarder (Forwarder) values ('".$this->mssqlQuotedString($this->PostVars['Forwarder'])."')")){
					return "success";
				} else {
					return "erroradd";
				}
			}
		}
	}
	
	function ChangeStatus(){
		$sql = mssql_fetch_assoc(mssql_query("select [Status] from Forwarder where Forwarder like '".$this->mssqlQuotedString($this->PostVars['Forwarder'])."'"));
		$status = $sql['Status'];
		$status = ($status == 1 ? 0 : 1);
		
		if (mssql_query("update Forwarder set [Status] = $status where Forwarder like '".$this->mssqlQuotedString($this->PostVars['Forwarder'])."'")){
			return "StatusChanged";
		} else {
			return "ErrorStatus";
		}
	}
	
}

?>
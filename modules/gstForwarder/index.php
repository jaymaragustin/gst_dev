<?php 
//error_reporting(E_ALL);
//ini_set("display_errors",1);
session_start();
require ('../../include/referer.checker.php');
require ('../../include/session.checker.php');

#style="border:1px solid #EEEEEE;"

?>
<h2><?php include '../../tpl/module_shortcut.php';?>Forwarder</h2> 

<style>
	.CenterMe{
		text-align:center;
	}
</style>

<form onsubmit="return false;" class="globalform"> 
<div id="divrightbar" style="float:left;">
		<fieldset>
			<legend>Forwarder</legend>
			<ol>
				<li><label>Forwarder:<input type="hidden" class="text" id="txtforwarderOLD" name="txtforwarderOLD" /></label></li>
				<li><input type="text" class="text" id="txtforwarder" name="txtforwarder" style="width:320px;" /></li>
				<li><input type="button" class="button" value="Save" id="btnSave" name="btnSave" style="float:right"/><input type="button" class="button" value="Cancel" id="btnCancel" name="btnCancel" style="float:right"/></li>
			</ol>
		</fieldset>
</div>
<div id="divLeftbar" style="width: 520px; float:left; margin-left:20px;">
<table class="display" id="tblForwarder"></table>
</div>
</form>

<script>
var tblForwarder;
var ListOfForwarder = [];
var Editme;
var changeStatus;
var listAdd = ['AIR DISPATCH','AIR MARKET','AIR SEA','ASPAC','Avion Shipping (s) Pte Ltd','Avnet Asia','Arrow Electronics Asia','AYZA','A & R Logistics Pte Ltd','Associated Carriage & Warehousing (M) Sdn Bhd','BALTRANS','BECHTRANS INT\'L','Best Support','CEVA','CJ GLS','CWE','DHL','DHL GLOBAL','DIMERCO','DACHSER','Eteq Component','EXPEDITORS','ELPIDA MEMORY INC','FEDEX','FEDEX TRADE','FEILI','FEILIKS','FREIGHT LINK','FNS','GLOBAL AIR','GREAT EXP','GEODIS WILSON','H & FRIENDS','HANDCARRY','Hauppauge Digital','HANG YANG','HANGKYU','HANSHIN','HAWK','HELLMAN','HTNS SG','Interhorizon Corporation','INTERGRATED FORWARDING','JAS','JSI','JNE SINGAPORE PTE LTD','JUST R TRANSPORT ENTERPRISE','K & N','K C DAT','KERRY LOGISTICS','KGL SG','KORCHINA','KWE JB','KWE SG','LEADER MUTUAL','LOCAL TRANSPORTER','Logico Express','Lynxemi','MENLO','MOL','MORRISON','MOVON SOLUTION PTE LTD','NAN AIRFREIGHT','NANZAS','NECNY','NEU MOVER','NEXSTAR ','NEW SIGN LOGISTICS - MY','NIPPON EXP','NISSIN','NNR GLOBAL','ONTIME','OVERSEAS COURIER SERVICES','PAN ASIA','PANALPINA','PANTOS','PNALPINA','PN SHIPPING','PVG','RESEARCH INC','ROUTER','RYDER','RICHARD ALAN','S NET','SAGAWA','SCHENKER','SDV','SILK EXP','SINTOP','SKILL EXP','SKY LEADER','SMD','SONY','SOONEST','SPEEDMARK','SPM REFINERY PTE LTD','SHINYANG','SHINJU LOGISTICS PTE LTD','Sri Bayanaemas Road Feeder service (s) Pte Ltd','STETS','SUN EXP','Supportive Enterprise','SWEE LONG','TECHNOCOM','TELFORD','TFS','TMC','TNT','TRANSPOLE LOGISTICS PTE LTD','TRANSPEED','TRANSPLACE','Tiong Nam Logistics (S) Pte Ltd','TIAN FANG/SEQUANS','Twin Star Courier','U FREIGHT','UPS','UPS SCS','UTI','VANTEC','Venture Corporation Ltd','VT LOGISTICS','WEISS','World Peace Intl (South Asia) Pte Ltd','XPRESS 21','YAMATO','YANTEC','YOUST','YUSEN','ZHANBEE'];

ForwarderMsgs = new Object();
ForwarderMsgs['exists'] = 'Forwarder already exists. Unable to add in.';
ForwarderMsgs['success'] = 'Forwarder successfully add to the database.';
ForwarderMsgs['erroradd'] = 'Unable to add Forwarder. Please refresh your browser and try again.';
ForwarderMsgs['updateexists'] = 'Forwarder already exists. Please check you Forwarder List.';
ForwarderMsgs['successUpdate'] = 'Forwarder successfully updated.';
ForwarderMsgs['errorUpdate'] = 'Unable to edit Forwarder. Please refresh your browser and try again.';
ForwarderMsgs['StatusChanged'] = 'Forwarder status successfully changed.';
ForwarderMsgs['ErrorStatus'] = 'Unable to change Forwarder Status. Please refresh your browser and try again.';

$(function() {
	$( "input:submit, input:button, button, .button").button();
	$("#btnCancel").hide();
	
	function InitTableForwarder(){
		if (tblForwarder){
			tblForwarder.fnDestroy();
		}
		
		tblForwarder = $('#tblForwarder').dataTable({
				'bFilter': true,
				'bPaginate': true,
				'bJQueryUI': true,
				'aLengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
				'sPaginationType': 'full_numbers',
				"bScrollCollapse": true,      
				"bDestroy": true,      
				"aaData" : ListOfForwarder,
				"aaSorting": [[2,'asc']],
				"aoColumns": [
					{ "sTitle": "Tools",sWidth: '80px',"sClass":"CenterMe" },
					{ "sTitle": "Status",sWidth: '80px',"sClass":"CenterMe" },
					{ "sTitle": "Forwarder" }
				]
			});
		$("#tblForwarder_filter label").css('width','195px');
	}
	
	InitTableForwarder();
	
	function msgbox(msg,focusField){
		$( "#dialog-msgbox:ui-dialog" ).dialog( "destroy" );
		$( "#dialog-msgbox" ).html(msg);
		$( "#dialog-msgbox" ).attr("title",'Forwarder');
		$( "#dialog-msgbox" ).dialog('option', 'position', 'center');
		$( "#dialog-msgbox" ).dialog({
			modal: true,
			height:'auto',
			show:{ 
				effect:"blind", 
				speed:'slow' 
			},
			hide:'blind',
			buttons: {
				Ok: function() {
					$(this).dialog( "close" );
					if (focusField){
							$('#'+focusField).focus().select();
					}
				}
			}
		});
	}
	
	function ProcessRequest(data,gotoProcess){
		$.ajax({
		 type: 'POST',
		 url: 'modules/gstForwarder/ForwarderGSTClass.php',
		 data: data,
		 success: function(data){
				var CallMeBaby = eval('(' + gotoProcess + ')');
						CallMeBaby(data);
		 },
		 beforeSend: function(){
				$.dimScreen(function() {
					$('#content').fadeIn();
				});
		 },
		 complete: function(){
			 $.dimScreenStop();
		 }
		});
	}
	
	Editme = function(tmpForwarder){
		$("#txtforwarder").val(tmpForwarder);
		$("#txtforwarderOLD").val(tmpForwarder);
		$("#btnSave").val('Update');
		$("#btnCancel").show();
	}
	
	changeStatus = function(tmpStatus,tmpForwarder){
		ProcessRequest({'todo':'ChangeStatus','Forwarder':tmpForwarder},'ChangeStatus');
	}
	
	function ChangeStatus(data){
		msgbox(ForwarderMsgs[data]);
		ProcessRequest({'todo':'ListAllForwarder'},'ListAllForwarder');
		$("#btnCancel").click();
	}
	
	function ListAllForwarder(data){
		var obj = $.parseJSON(data);
		ListOfForwarder = [];
		
		$.each(obj,function(a,b){
			ListOfForwarder.push(['<a href="javascript:void(0);" onclick="Editme(\''+ b['Forwarder'] +'\');">Edit</a>','<a href="javascript:void(0);" onclick="changeStatus(\''+ b['Status'] +'\',\'' + b['Forwarder'] + '\');">' + b['Status'] + '</a>',b['Forwarder']]);
		});
		InitTableForwarder();
	}
	
	function AddForwarder(data){
		msgbox(ForwarderMsgs[data]);
		if (data == 'success'){
			ListOfForwarder.push(['<a href="javascript:void(0);" onclick="Editme(\''+ $("#txtforwarder").val() +'\');">Edit</a>','<a href="javascript:void(0);" onclick="changeStatus(\'Deactivate\',\'' + $("#txtforwarder").val() + '\');">Deactivate</a>',$("#txtforwarder").val()]);
			InitTableForwarder();
			$("#txtforwarder").val('');
		} else if (data == 'successUpdate') {
			ProcessRequest({'todo':'ListAllForwarder'},'ListAllForwarder');
			$("#btnCancel").click();
		}
	}
	
	$("#btnSave").click(function(){
		$("#txtforwarder").val($.trim($("#txtforwarder").val()));
		if ($("#txtforwarder").val() == ''){
			msgbox('Please key in the Forwarder that you want to add.','txtforwarder');
		} else {
			ProcessRequest({'todo':'AddForwarder','Forwarder':$("#txtforwarder").val(),'Type':$("#btnSave").val(),'Old':$("#txtforwarderOLD").val()},'AddForwarder');
		}
	});
	
	$("#btnCancel").click(function(){
		$("#txtforwarder").val('');
		$("#btnSave").val('Save');
		$("#btnCancel").hide();
	});
	
	ProcessRequest({'todo':'ListAllForwarder'},'ListAllForwarder');

});
</script>
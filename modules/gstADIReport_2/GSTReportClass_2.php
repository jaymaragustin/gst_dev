<?php
error_reporting(-1);
ini_set('display_errors', 1);
ini_set('memory_limit', '1024M');

if (isset($_POST['todo'])){
	require ('../../include/config.php');
	$GSTReportClass = new GSTReportClass($_REQUEST);
	
	
	header('Content-Type: application/json');
	if ($_POST['todo'] == 'PrepareForm'){
		echo $GSTReportClass->PrepareForm();
	}elseif($_POST['todo'] == 'generateReport'){
		echo $GSTReportClass->generateReport();
	}elseif($_POST['todo'] == 'getEntryByReference'){
		echo $GSTReportClass->getEntryByReference();
	}
}

Class GSTReportClass{
	var $PostVars;
	function __construct($vars){
		$this->PostVars = $vars;
		$serverConn = unserialize(base64_decode(SQL_CONN));
		$this->conn = mssql_connect($serverConn[0],$serverConn[1],$serverConn[2]);
		mssql_select_db("Import");
		
		foreach($this->PostVars as $a => $b){
			$this->PostVars[$a] = $this->mssqlQuotedString($b);
		}
	}
	
	function mssqlQuotedString($str){
		$str = str_replace("\\", "\\\\", $str);
		$str = str_replace("'", "''", $str);
		
		return $str;
	}
	
	function PrepareForm(){
		mssql_select_db('MasterData');
		$sql = mssql_query("select ID,WawiIndex,WawiAlias from WawiCustomer Where [status] = 1 and GST = 1 order by WawiAlias");

		while($row = mssql_fetch_assoc($sql)){
			if (!isset($CustomerList[$row['WawiIndex']])){
				$CustomerList[$row['WawiIndex']] = array();
			}
			
			if($row['ID'] == 215){
				$row['WawiAlias'] = 'ADI SG';
			}
			$customer = ($row['WawiIndex'] == 'ADIDGTWMS' ? 'ADISGWMS' : $row['WawiIndex']);
			$CustomerList[$row['WawiIndex']] = array($customer,$row['WawiAlias']);
			$sqlTaxScheme = mssql_query("select TaxScheme from TaxScheme where [status] = 1 and WawiID = ".$row['ID']);
			if (mssql_num_rows($sqlTaxScheme) > 0){
				while($TaxSchemeList[$customer][] = mssql_fetch_assoc($sqlTaxScheme)){}
				array_pop($TaxSchemeList[$customer]);
			} else {
				$TaxSchemeList[$customer][0] = array('TaxScheme' => 'No TaxScheme');
			}
		}
		
		$data = array();
		array_push($data,$CustomerList);
		array_push($data,$TaxSchemeList);
		
		return json_encode($data);
	}
	
	function generateReport(){
		$data = array();
		
		array_push($data,$this->GetIncomingReport());
		array_push($data,$this->GetOutgoingReport());
		array_push($data,$this->getpicklistByReference());
		array_push($data,$this->shipmentSummary());
		
		//echo '<pre>'; print_r($data); echo '</pre>';
		
		return json_encode($data);
	}
	
	function GetIncomingReport(){
		
		extract($this->PostVars);
		mssql_select_db("ADISGWMS");
		$data = array();
		
		$chkall					= (trim($chkall) == "true") ? true:false;
		$chkIncomingDetailed	= (trim($chkIncomingDetailed) == "true") ? true:false;
		$chkIncomingSummary		= (trim($chkIncomingSummary) == "true") ? true:false;
		$chkOutgoingDetailed	= (trim($chkOutgoingDetailed) == "true") ? true:false;
		$chkOutgoingSummary		= (trim($chkOutgoingSummary) == "true") ? true:false;
		$chkShipmentPicklist	= (trim($chkShipmentPicklist) == "true") ? true:false;
		$chkShipmentSummary		= (trim($chkShipmentSummary) == "true") ? true:false;
		
		$searchColumn	= trim($searchColumn);
		$searchCon		= trim($searchCon);
		$searchInput1	= trim($searchInput1);
		$searchInput2	= trim($searchInput2);
		
		$incDetailedColumn = array(
			'VendorInvNo'	=> 'VendorInvNos',
			'TaxScheme'		=> 'InTaxRef',
			'HAWB'			=> 'HAWB',
			'Date'			=> 'ArrivalDate',
			'Permit'		=> 'PermitNo',
			'PartNoDesc'	=> 'Description',
			'ShipmentNo'	=> 'ShipReference',
			'SUNumber'		=> 'EntrySU'
		);
		
		$incSummaryColumn = array(
			'VendorInvNo'	=> 'VendorInvNo2',
			'TaxScheme'		=> 'InTaxRef',
			'HAWB'			=> 'HAWB',
			'Date'			=> 'ArrivalDate1',
			'Permit'		=> 'PermitNo',
			'PartNoDesc'	=> 'Description1',
			'ShipmentNo'	=> 'ShipReference',
			'SUNumber'		=> 'EntrySU'
		);
		
		$incDetailedFilter = "";
		$incSummaryFilter = "";
		
		if($searchCon == 'equalto'){
			$incDetailedFilter .= " ".$incDetailedColumn[$searchColumn]." = '".$searchInput1."'";
			$incSummaryFilter .= " ".$incSummaryColumn[$searchColumn]." = '".$searchInput1."'";
		} elseif($searchCon == 'notequalto'){
			$incDetailedFilter .= " ".$incDetailedColumn[$searchColumn]." != '".$searchInput1."'";
			$incSummaryFilter .= " ".$incSummaryColumn[$searchColumn]." != '".$searchInput1."'";
		} elseif($searchCon == 'contains'){
			$incDetailedFilter .= " ".$incDetailedColumn[$searchColumn]." LIKE '%".$searchInput1."%'";
			$incSummaryFilter .= " ".$incSummaryColumn[$searchColumn]." LIKE '%".$searchInput1."%'";
		} elseif($searchCon == 'greaterthan'){
			$incDetailedFilter .= " ".$incDetailedColumn[$searchColumn]." > '".$searchInput1."'";
			$incSummaryFilter .= " ".$incSummaryColumn[$searchColumn]." > '".$searchInput1."'";
		} elseif($searchCon == 'lessthan'){
			$incDetailedFilter .= " ".$incDetailedColumn[$searchColumn]." < '".$searchInput1."'";
			$incSummaryFilter .= " ".$incSummaryColumn[$searchColumn]." < '".$searchInput1."'";
		} elseif($searchCon == 'between'){
			$incDetailedFilter .= " ".$incDetailedColumn[$searchColumn]." BETWEEN '".$searchInput1."' AND '".$searchInput2."'";
			$incSummaryFilter .= " ".$incSummaryColumn[$searchColumn]." BETWEEN '".$searchInput1."' AND '".$searchInput2."'";
		}
		
		
		if($chkall || $chkIncomingDetailed){
			
			$sqlString = "SELECT * FROM [v_GSTReport_IncomingDetailed] WHERE ".$incDetailedFilter;
			$sql = mssql_query($sqlString);
			if (mssql_num_rows($sql) > 0){
				
				while($row[] = mssql_fetch_assoc($sql)){}
				array_pop($row);
				
				array_push($data,array($row));
				
			} else {
				array_push($data,array("nodata"));
			}
			
		} else {
			array_push($data,array("nodata"));
		}

		if($chkall || $chkIncomingSummary){
			
			$sqlString1 = "SELECT * FROM [v_GSTReport_IncomingSummary] WHERE ".$incSummaryFilter;//64514
			$sql1 = mssql_query($sqlString1);
			if (mssql_num_rows($sql1) > 0){
				
				while($row1[] = mssql_fetch_assoc($sql1)){}
				array_pop($row1);
				
				array_push($data,array($row1));
				
			} else {
				array_push($data,array("nodata"));
			}
			
		} else {
			array_push($data,array("nodata"));
		}
		
		return $data;
		
	}
	
	function GetOutgoingReport(){
		
		extract($this->PostVars);
		mssql_select_db("ADISGWMS");
		$data = array();
		
		$chkall					= (trim($chkall) == "true") ? true:false;
		$chkIncomingDetailed	= (trim($chkIncomingDetailed) == "true") ? true:false;
		$chkIncomingSummary		= (trim($chkIncomingSummary) == "true") ? true:false;
		$chkOutgoingDetailed	= (trim($chkOutgoingDetailed) == "true") ? true:false;
		$chkOutgoingSummary		= (trim($chkOutgoingSummary) == "true") ? true:false;
		$chkShipmentPicklist	= (trim($chkShipmentPicklist) == "true") ? true:false;
		$chkShipmentSummary		= (trim($chkShipmentSummary) == "true") ? true:false;
		
		$searchColumn	= trim($searchColumn);
		$searchCon		= trim($searchCon);
		$searchInput1	= trim($searchInput1);
		$searchInput2	= trim($searchInput2);
		
		$outDetailedColumn = array(
			'VendorInvNo'	=> 'DN',
			'TaxScheme'		=> 'OutTaxRef',
			'HAWB'			=> 'HAWB',
			'Date'			=> 'CargoReleasedDate',
			'Permit'		=> 'PermitNo',
			'PartNoDesc'	=> 'MaterialDescriptions',
			'ShipmentNo'	=> 'ShipmentRef',
			'SUNumber'		=> 'SUNumbers'
		);
		
		$outSummaryColumn = array(
			'VendorInvNo'	=> 'DN',
			'TaxScheme'		=> 'OutTaxRef',
			'HAWB'			=> 'HAWB',
			'Date'			=> 'CargoReleasedDate',
			'Permit'		=> 'PermitNo',
			'PartNoDesc'	=> 'MaterialDescriptions',
			'ShipmentNo'	=> 'ShipmentRef',
			'SUNumber'		=> 'SUNumbers'
		);
		
		$outDetailedFilter = "";
		$outSummaryFilter = "";
		
		if($searchCon == 'equalto'){
			$outDetailedFilter .= " ".$outDetailedColumn[$searchColumn]." = '".$searchInput1."'";
			$outSummaryFilter .= " ".$outSummaryColumn[$searchColumn]." = '".$searchInput1."'";
		} elseif($searchCon == 'notequalto'){
			$outDetailedFilter .= " ".$outDetailedColumn[$searchColumn]." != '".$searchInput1."'";
			$outSummaryFilter .= " ".$outSummaryColumn[$searchColumn]." != '".$searchInput1."'";
		} elseif($searchCon == 'contains'){
			$outDetailedFilter .= " ".$outDetailedColumn[$searchColumn]." LIKE '%".$searchInput1."%'";
			$outSummaryFilter .= " ".$outSummaryColumn[$searchColumn]." LIKE '%".$searchInput1."%'";
		} elseif($searchCon == 'greaterthan'){
			$outDetailedFilter .= " ".$outDetailedColumn[$searchColumn]." > '".$searchInput1."'";
			$outSummaryFilter .= " ".$outSummaryColumn[$searchColumn]." > '".$searchInput1."'";
		} elseif($searchCon == 'lessthan'){
			$outDetailedFilter .= " ".$outDetailedColumn[$searchColumn]." < '".$searchInput1."'";
			$outSummaryFilter .= " ".$outSummaryColumn[$searchColumn]." < '".$searchInput1."'";
		} elseif($searchCon == 'between'){
			$outDetailedFilter .= " ".$outDetailedColumn[$searchColumn]." BETWEEN '".$searchInput1."' AND '".$searchInput2."'";
			$outSummaryFilter .= " ".$outSummaryColumn[$searchColumn]." BETWEEN '".$searchInput1."' AND '".$searchInput2."'";
		}
		
		
		if($chkall || $chkOutgoingDetailed){
			
			$sqlString = "SELECT * FROM [v_GSTOutgoingDetailed] WHERE ".$outDetailedFilter;
			$sql = mssql_query($sqlString);
			if (mssql_num_rows($sql) > 0){
				
				//while($row[] = mssql_fetch_assoc($sql)){}
				//array_pop($row);
				
				$setLastRef = '';
				$eachRefCtr = 0;
				while($tmprow = mssql_fetch_assoc($sql)){
					
					if($eachRefCtr > 0){
						$tmprow['ShipTotalQty'] = '';
					}
					
					$rowShipRef = $tmprow['ShipmentRef'];
					if($setLastRef == '' || $setLastRef == $rowShipRef){
						$eachRefCtr++;
					} else {
						$eachRefCtr = 0;
					}
					$setLastRef = $tmprow['ShipmentRef'];
					
					$row[] = $tmprow;
				}
				
				array_push($data,array($row));
				
			} else {
				array_push($data,array("nodata"));
			}
			
		} else {
			array_push($data,array("nodata"));
		}
		

		if($chkall || $chkOutgoingSummary){
			
			$sqlString1 = "SELECT * FROM [v_GSTOutgoingSummary] WHERE ".$outSummaryFilter;
			$sql1 = mssql_query($sqlString1);
			if (mssql_num_rows($sql1) > 0){
				
				while($row1[] = mssql_fetch_assoc($sql1)){}
				array_pop($row1);
				
				array_push($data,array($row1));
				
			} else {
				array_push($data,array("nodata"));
			}
			
		} else {
			array_push($data,array("nodata"));
		}
		
		return $data;
		
	}

	function getpicklistByReference(){
		
		extract($this->PostVars);
		mssql_select_db("ADISGWMS");
		$data = array();
		
		$chkall					= (trim($chkall) == "true") ? true:false;
		$chkIncomingDetailed	= (trim($chkIncomingDetailed) == "true") ? true:false;
		$chkIncomingSummary		= (trim($chkIncomingSummary) == "true") ? true:false;
		$chkOutgoingDetailed	= (trim($chkOutgoingDetailed) == "true") ? true:false;
		$chkOutgoingSummary		= (trim($chkOutgoingSummary) == "true") ? true:false;
		$chkShipmentPicklist	= (trim($chkShipmentPicklist) == "true") ? true:false;
		$chkShipmentSummary		= (trim($chkShipmentSummary) == "true") ? true:false;
		
		$searchColumn	= trim($searchColumn);
		$searchCon		= trim($searchCon);
		$searchInput1	= trim($searchInput1);
		$searchInput2	= trim($searchInput2);
		
		$picklistColumn = array(
			'VendorInvNo'	=> 'CustomerReference',
			'TaxScheme'		=> 'OutTaxScheme',
			'HAWB'			=> 'HAWB',
			'Date'			=> 'ReleasedDate',
			'Permit'		=> 'OutPermit',
			'PartNoDesc'	=> 'OutPart',
			'ShipmentNo'	=> 'OutRef',
			'SUNumber'		=> 'OutEntry'
		);
		
		$picklistFilter = "";
		
		if($searchCon == 'equalto'){
			$picklistFilter .= " ".$picklistColumn[$searchColumn]." = '".$searchInput1."'";
		} elseif($searchCon == 'notequalto'){
			$picklistFilter .= " ".$picklistColumn[$searchColumn]." != '".$searchInput1."'";
		} elseif($searchCon == 'contains'){
			$picklistFilter .= " ".$picklistColumn[$searchColumn]." LIKE '%".$searchInput1."%'";
		} elseif($searchCon == 'greaterthan'){
			$picklistFilter .= " ".$picklistColumn[$searchColumn]." > '".$searchInput1."'";
		} elseif($searchCon == 'lessthan'){
			$picklistFilter .= " ".$picklistColumn[$searchColumn]." < '".$searchInput1."'";
		} elseif($searchCon == 'between'){
			$picklistFilter .= " ".$picklistColumn[$searchColumn]." BETWEEN '".$searchInput1."' AND '".$searchInput2."'";
		}
		
		
		if($chkall || $chkShipmentPicklist){
			
			$sqlString = "SELECT * FROM [v_GSTShipmentPicklist] WHERE ".$picklistFilter;
			$sql = mssql_query($sqlString);
			if (mssql_num_rows($sql) > 0){
				
				while($row[] = mssql_fetch_assoc($sql)){}
				array_pop($row);
				
				array_push($data,$row);
				
			} else {
				$data = "nodata";
			}
			
		} else {
			$data = "nodata";
		}
		
		return $data;
		
	}

	function shipmentSummary(){
		
		extract($this->PostVars);
		mssql_select_db("ADISGWMS");
		$data = array();
		
		$chkall					= (trim($chkall) == "true") ? true:false;
		$chkIncomingDetailed	= (trim($chkIncomingDetailed) == "true") ? true:false;
		$chkIncomingSummary		= (trim($chkIncomingSummary) == "true") ? true:false;
		$chkOutgoingDetailed	= (trim($chkOutgoingDetailed) == "true") ? true:false;
		$chkOutgoingSummary		= (trim($chkOutgoingSummary) == "true") ? true:false;
		$chkShipmentPicklist	= (trim($chkShipmentPicklist) == "true") ? true:false;
		$chkShipmentSummary		= (trim($chkShipmentSummary) == "true") ? true:false;
		
		$searchColumn	= trim($searchColumn);
		$searchCon		= trim($searchCon);
		$searchInput1	= trim($searchInput1);
		$searchInput2	= trim($searchInput2);
		
		$shipSummaryColumn = array(
			'VendorInvNo'	=> 'Invoice',
			'TaxScheme'		=> 'InTaxScheme',
			'HAWB'			=> 'AirwayBill',
			'Date'			=> 'ReceivedDate',
			'Permit'		=> 'InPermit',
			'PartNoDesc'	=> 'InPart',
			'ShipmentNo'	=> 'InRef',
			'SUNumber'		=> 'InEntry'
		);
		
		$shipSummaryFilter = "";
		
		if($searchCon == 'equalto'){
			$shipSummaryFilter .= " ".$shipSummaryColumn[$searchColumn]." = '".$searchInput1."'";
		} elseif($searchCon == 'notequalto'){
			$shipSummaryFilter .= " ".$shipSummaryColumn[$searchColumn]." != '".$searchInput1."'";
		} elseif($searchCon == 'contains'){
			$shipSummaryFilter .= " ".$shipSummaryColumn[$searchColumn]." LIKE '%".$searchInput1."%'";
		} elseif($searchCon == 'greaterthan'){
			$shipSummaryFilter .= " ".$shipSummaryColumn[$searchColumn]." > '".$searchInput1."'";
		} elseif($searchCon == 'lessthan'){
			$shipSummaryFilter .= " ".$shipSummaryColumn[$searchColumn]." < '".$searchInput1."'";
		} elseif($searchCon == 'between'){
			$shipSummaryFilter .= " ".$shipSummaryColumn[$searchColumn]." BETWEEN '".$searchInput1."' AND '".$searchInput2."'";
		}
		
		
		if($chkall || $chkShipmentSummary){
			
			$sqlString = "SELECT * FROM [v_GSTShipmentPicklist] WHERE ".$shipSummaryFilter;
			$sql = mssql_query($sqlString);
			if (mssql_num_rows($sql) > 0){
				
				while($row[] = mssql_fetch_assoc($sql)){}
				array_pop($row);
				
				array_push($data,$row);
				
			} else {
				$data = "nodata";
			}
			
		} else {
			$data = "nodata";
		}
		
		return $data;
		
	}
	
	function array_orderby(){
	    $args = func_get_args();
	    $data = array_shift($args);
	    foreach ($args as $n => $field) {
	        if (is_string($field)) {
	            $tmp = array();
	            foreach ($data as $key => $row)
	                $tmp[$key] = $row[$field];
	            $args[$n] = $tmp;
	            }
	    }
	    $args[] = &$data;
	    call_user_func_array('array_multisort', $args);
	    return array_pop($args);
	}
	
	function mssql_escape($str) {
		return str_replace("'", "''", $str);
	}
	
	function AnalyzeSQL($sql){
		if (mssql_num_rows($sql) > 0){
			while($row[] = mssql_fetch_assoc($sql)){}
			array_pop($row);
		} else {
			$row = 'nodata';
		}
		
		return $row;
	}

	function GSTRate(){
		$sql = mssql_query("SELECT Value FROM Setup WHERE Entry='GST'");
		if (mssql_num_rows($sql) > 0){
			$sql = mssql_fetch_assoc($sql);
			$GSTRate = $sql['Value'];
		} else {
			$GSTRate = 7;
		}
		
		return $GSTRate;
	}
	
}

?>
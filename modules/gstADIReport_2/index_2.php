<?php 
//error_reporting(E_ALL);
//ini_set("display_errors",1);

//add arrival date on the report
session_start();
require ('../../include/referer.checker.php');
require ('../../include/session.checker.php');

#style="border:1px solid #EEEEEE;"

?>

<script type='text/javascript' src='js/jquery.dataTables.columnFilter.js'></script>

<style>
	.alignme{
		text-align: center;
		white-space: nowrap;
	}
	.alignmewrap{
		text-align: center;
		word-wrap: break-word !important;
		white-space: normal !important;
	}
	table.display td{
		white-space: normal !important;
		word-wrap: break-word !important;
	}
	span.totalNos{ color: #CE2929; font-size: 12px; font-weight: bold;}
	.ui-tabs .ui-tabs-nav li a {
		font-size: 11px !important;
	}
</style>

<h2><?php include '../../tpl/module_shortcut.php';?>ADI GST Report</h2> 
<script src="modules/gstADIReport/jquery.fileDownload.js">
</script>

<form onsubmit="return false;" class="globalform">
<ol>
	<div style="float:left;">
		<li>
			<label style="width:100px;">Start Date</label>
			<input type="text" class="text" value="" readonly name="startDate" id="startDate" />
		</li>
		<li>
			<label style="width:100px;">End Date</label>
			<input type="text" class="text" value="" readonly name="endDate" id="endDate" />
		</li>
		<li>
			<label style="width:100px;">&nbsp;</label>
		</li>
	</div>
	<div style="float:left; margin-left: 15px;">
		<li style="display:none;">
			<label style="width:100px;">Customer</label>
			<select class="select" id="slcCustomer" name="slcCustomer"></select>
		</li>
		<li style="display:none;">
			<label style="width:100px;">Tax Reference</label>
			<select class="select" id="slcReference" name="slcReference"></select>
			<span><input type="checkbox" id="checkAll"/>all</span>
		</li>
		<li style="display: none;">
			<label style="width:100px;">IN.Shipment Ref#</label>
			<input type="text" class="text" id="shipmentReference" name="shipmentReference"/>
		</li>
		<li id="entry-show" style="display: none;">
			<label style="width:100px;">Entry Number</label>
			<select type="select"  id="entryNumber" name="entryNumber"></select>
		</li>
	</div>
	<div style="float:left; margin-left: 15px;">
		<li><label style="width:1px;">&nbsp;</label><input type="button" class="button" value="Generate Report" id="generateReport" /></li>
		<li><label style="width:1px;">&nbsp;</label><input type="button" class="button" value="Export to Excel" id="exportToExcel" /></li>
		<li><label style="width:1px;">&nbsp;</label></li>
	</div>
</ol>
</form>
<div class="clr"></div>
<br/>
<div id="DivTabs">
	<div id="tabs">
	  <ul>
	      <li><a href="#tabs-1">Incoming GST Detailed</a></li>
	      <li><a href="#tabs-2">Incoming GST Summary</a></li>
	      <li><a href="#tabs-3">Outgoing GST Detailed</a></li>
	      <li><a href="#tabs-4">Outgoing GST Summary</a></li>
	      <li><a href="#tabs-5">Shipment Picklist</a></li>
	      <li><a href="#tabs-6">Shipment Summary</a></li>
	  </ul>
	  <div id="tabs-1">
		<!--<input type="text" class="text" id="txtFilterThis" placeholder="Reference #" style="width:100px;">-->
	     <table class="display" id="tblIncomingDetailed">
			<thead>
				<tr>
					<th class="tblIncomingDetailed_header">S/No</th>
					<th class="tblIncomingDetailed_header">Customer</th>
					<th class="tblIncomingDetailed_header">Shipment #</th>
					<th class="tblIncomingDetailed_header">Shpmt Rcvd Date</th>
					<th class="tblIncomingDetailed_header">VendorInvNo.</th>
					<th class="tblIncomingDetailed_header">Total Qty</th>
					<th class="tblIncomingDetailed_header">Tax Reference</th>
					<th class="tblIncomingDetailed_header">Permit No.</th>
					<th class="tblIncomingDetailed_header">Exchange Rate</th>
					<th class="tblIncomingDetailed_header">Vendor Inv Value (USD)</th>
					<th class="tblIncomingDetailed_header">Vendor Inv Value (SGD)</th>
					<th class="tblIncomingDetailed_header">FrtCharges (SGD)</th>
					<th class="tblIncomingDetailed_header">InsCharges (SGD)</th>
					<th class="tblIncomingDetailed_header">CIF Amount (SGD)</th>
					<th class="tblIncomingDetailed_header">GST Amount (SGD)</th>
					<th class="tblIncomingDetailed_header">Origin Port</th>
					<th class="tblIncomingDetailed_header">Forwarders</th>
					<th class="tblIncomingDetailed_header">Vendor /Supplier</th>
					<th class="tblIncomingDetailed_header">HAWB No.</th>
					<th class="tblIncomingDetailed_header">MAWB No.</th>
					<th class="tblIncomingDetailed_header">TptMode</th>
					<th class="tblIncomingDetailed_header">Arrival Date</th>
					<th class="tblIncomingDetailed_header">Delivery To</th>
					<th class="tblIncomingDetailed_header">IncoTerms</th>
					<th class="tblIncomingDetailed_header">JobType</th>
					<th class="tblIncomingDetailed_header">Comment</th>
					<th class="tblIncomingDetailed_header">Incoming Reference</th>
					<th class="tblIncomingDetailed_header">Description</th>
					<th class="tblIncomingDetailed_header">SU/Entry#.</th>
					<th class="tblIncomingDetailed_header">Consignment order (Y/N)</th>
					<th class="tblIncomingDetailed_header">Previous SU# for consignment</th>
				</tr>
				<tr>
					<th>S/No</th>
					<th>Customer</th>
					<th>Shipment #</th>
					<th>Shpmt Rcvd Date</th>
					<th>VendorInvNo.</th>
					<th>Total Qty</th>
					<th>Tax Reference</th>
					<th>Permit No.</th>
					<th>Exchange Rate</th>
					<th>Vendor Inv Value (USD)</th>
					<th>Vendor Inv Value (SGD)</th>
					<th>FrtCharges (SGD)</th>
					<th>InsCharges (SGD)</th>
					<th>CIF Amount (SGD)</th>
					<th>GST Amount (SGD)</th>
					<th>Origin Port</th>
					<th>Forwarders</th>
					<th>Vendor /Supplier</th>
					<th>HAWB No.</th>
					<th>MAWB No.</th>
					<th>TptMode</th>
					<th>Arrival Date</th>
					<th>Delivery To</th>
					<th>IncoTerms</th>
					<th>JobType</th>
					<th>Comment</th>
					<th>Incoming Reference</th>
					<th>Description</th>
					<th>SU/Entry#.</th>
					<th>Consignment order (Y/N)</th>
					<th>Previous SU# for consignment</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
			<tfoot>
				<tr>
					<<!--td width="300px" style="width: 300px !important;">S/No</th>
					<td width="300px" style="width: 300px !important;">Customer</th>
					<td width="300px" style="width: 300px !important;">Shipment #</th>
					<td width="300px" style="width: 300px !important;">Shpmt Rcvd Date</th>-->
					<td>S/No</th>
					<td>Customer</th>
					<td>Shipment #</th>
					<td>Shpmt Rcvd Date</th>
					<td>VendorInvNo.</th>
					<td>Total Qty</th>
					<td>Tax Reference</th>
					<td>Permit No.</th>
					<td>Exchange Rate</th>
					<td>Vendor Inv Value (USD)</th>
					<td>Vendor Inv Value (SGD)</th>
					<td>FrtCharges (SGD)</th>
					<td>InsCharges (SGD)</th>
					<td>CIF Amount (SGD)</th>
					<td>GST Amount (SGD)</th>
					<td>Origin Port</th>
					<td>Forwarders</th>
					<td>Vendor /Supplier</th>
					<td>HAWB No.</th>
					<td>MAWB No.</th>
					<td>TptMode</th>
					<td>Arrival Date</th>
					<td>Delivery To</th>
					<td>IncoTerms</th>
					<td>JobType</th>
					<td>Comment</th>
					<td>Incoming Reference</th>
					<td>Description</th>
					<td>SU/Entry#.</th>
					<td>Consignment order (Y/N)</th>
					<td>Previous SU# for consignment</th>
				</tr>
			</tfoot>
		</table>
	  </div>
	  <div id="tabs-2">
	     <table class="display" id="tblIncomingSummary"></table>
	  </div>
	  <div id="tabs-3">
			<table class="display" id="tblOutgoingDetailed"></table> 
	  </div>
	  <div id="tabs-4">
			<table class="display" id="tblOutgoingSummary"></table>
	  </div>
	  <div id="tabs-5">
			<table class="display" id="tblPicklist"></table>
	  </div>
	  <div id="tabs-6">
			<input type="text" class="text" id="txtSReference" placeholder="Reference #" style="width:100px; display:none;">
			<input type="text" class="text" id="txtSEntry" placeholder="SU/Entry Number" style="width:100px; display:none;">
			<input type="text" class="text" id="txtSTaxScheme" placeholder="Tax Scheme" style="width:100px; display:none;">
			<input type="text" class="text" id="txtSIncoming" placeholder="Incoming" style="width:100px; display:none;">
			<input type="text" class="text" id="txtSOutgoing" placeholder="Outgoing" style="width:100px; display:none;">
			<input type="text" class="text" id="txtSBalance" placeholder="Balance" style="width:100px; display:none;">
			<table class="display" id="tblShipmentSummary"></table>
	  </div>
	</div>
</div>
<script>
ErrorKeys = new Object();
ErrorKeys['First'] = 'Beginning of the data';
ErrorKeys['Previous'] = 'Beginning of the data';
ErrorKeys['Last'] = 'Last data';
ErrorKeys['Next'] = 'Last data';
ErrorKeys['Search'] = 'Sorry invalid Shipment Number';
ErrorKeys['nodata'] = 'Sorry no available Reference Number';
ErrorKeys['ErrorSearchingShipment'] = 'Shipment Number is a required field in order to Search.';

var TaxReferenceList = new Object(); // get from masterdata - TaxScheme
var CustomerList = new Object();
var CurrentDate = new Date();

var tblIncomingDetailed;
var tblIncomingSummary;
var tblOutgoingDetailed;
var tblOutgoingSummary;
var tblPicklist;
var tblShipmentSummary;

$(function() {
	$("input:submit, input:button, button, .button").button();
	//$('#DivTabs').hide();
	
	$( "#startDate" ).datepicker({		
			dateFormat: 'mm/dd/yy',
	    defaultDate: "+1w",
	    changeMonth: true,
	    onClose: function( selectedDate ) {
	        $( "#endDate" ).datepicker( "option", "minDate", selectedDate );
	    }
	});
	
	$( "#endDate" ).datepicker({
			dateFormat: 'mm/dd/yy',
	    defaultDate: "+1w",
	    changeMonth: true,
	    onClose: function( selectedDate ) {
	        $( "#startDate" ).datepicker( "option", "maxDate", selectedDate );
	    }
	});

	tblIncomingDetailed = $('#tblIncomingDetailed').dataTable({
			"sDom": '<"H">Tlfr<"F"tip>',
			"oTableTools": {
				"aButtons": [
					{
						"sExtends":    "collection",
						"sButtonText": "Save As",
						"aButtons":    [ "xls" ]
					}
				]
			},
			//"bDeferRender": true,
			//"bAutoWidth": false,  
			//'aaSortingFixed': [[3,'asc'],[2,'asc']],
			'bFilter': true,
			'bSortable' : false,
			'bPaginate': true,
			'bJQueryUI': true,
			'aLengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
			'sPaginationType': 'full_numbers',
			"bScrollCollapse": true,  
			"sScrollX":"100%",    
			"bDestroy": true,
			"aoColumnDefs": [
				{ 'bSortable': true, 'aTargets': [ 3 ] },
				//{ "sWidth": "750px", "aTargets": [2] }
			],
			//"aaSorting": [[ 2, 'asc' ],[ 3, 'asc' ]],
			fnFooterCallback: function ( nRow, aaData, iStart, iEnd, aiDisplay ) {
				var iTotalQty = 0;
				var iVendorValUSD = 0;
				var iVendorValSGD = 0;
				var iFrtCharges = 0;
				var iInsCharges = 0;
				var iCIFAmount = 0;
				var iGSTAmount = 0;
				
				$.each(aaData,function(ikey,ivalue){
					iTotalQty += ivalue[5] != 0 ? parseInt(ivalue[5]) : 0;
					iVendorValUSD	+= ivalue[9] != 0 ? parseFloat(ivalue[9]) : 0;
					iVendorValSGD	+= ivalue[10] != 0 ? parseFloat(ivalue[10]) : 0;
					iFrtCharges		+= ivalue[11] != 0 ? parseFloat(ivalue[11]) : 0;
					iInsCharges		+= ivalue[12] != 0 ? parseFloat(ivalue[12]) : 0;
					iCIFAmount		+= ivalue[13] != 0 ? parseFloat(ivalue[13]) : 0;
					iGSTAmount		+= ivalue[14] != 0 ? parseFloat(ivalue[14]) : 0;
				});
				
				var nCells = nRow.getElementsByTagName('td');
				nCells[5].innerHTML  = 'Total Qty : <span class="totalNos">' + iTotalQty+'</span>';
				nCells[9].innerHTML  = 'Vendor Inv Value (USD) : <span class="totalNos">' + parseFloat(iVendorValUSD).toFixed(2)+'</span>';
				nCells[10].innerHTML = 'Vendor Inv Value (SGD) : <span class="totalNos">' + parseFloat(iVendorValSGD).toFixed(2)+'</span>';
				nCells[11].innerHTML = 'Total FrtCharges (SGD) : <span class="totalNos">' + parseFloat(iFrtCharges).toFixed(2)+'</span>';
				nCells[12].innerHTML = 'Total InsCharges (SGD) : <span class="totalNos">' + parseFloat(iInsCharges).toFixed(2)+'</span>';
				nCells[13].innerHTML = 'Total CIF Amount (SGD) : <span class="totalNos">' + parseFloat(iCIFAmount).toFixed(2)+'</span>';
				nCells[14].innerHTML = 'Total GST Amount (SGD) : <span class="totalNos">' + parseFloat(iGSTAmount).toFixed(2)+'</span>';			
			},
			fnDrawCallback: function( oSettings ) {
				$('#tblIncomingDetailed tr td').css('white-space','normal');
			}
		}).columnFilter({
			sPlaceHolder: "head:before"
            //aoColumns: [ { type: "input", values: [ 'Gecko', 'Trident', 'KHTML', 'Misc', 'Presto', 'Webkit', 'Tasman']  }
            //    ]
        });
		
		$(".tblIncomingDetailed_header").find('.filter_column').find("input").each(function() {
			
			var thisTextbox = $.trim($(this).val());
			
			if(thisTextbox == 'VendorInvNo.' || thisTextbox == 'Comment' || thisTextbox == 'Description' || thisTextbox == 'SU/Entry#.'){
				$(this).css("width", "350px");
			}
			
			if(thisTextbox == 'Previous SU# for consignment'){
				$(this).css("width", "250px");
			}
			
		});
		
	tblIncomingSummary = $('#tblIncomingSummary').dataTable({
			"sDom": '<"H">Tlfr<"F"tip>',
			"oTableTools": {
				"aButtons": [
					{
						"sExtends":    "collection",
						"sButtonText": "Save As",
						"aButtons":    [ "xls" ]
					}
				]
			},
			'bFilter': true,
			'bPaginate': true,
			'bJQueryUI': true,
			'aLengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
			'sPaginationType': 'full_numbers',
			"bScrollCollapse": true,  
			"sScrollX":"100%",    
			"bDestroy": true,
			"aoColumns": [
				{"sTitle":"S/No","sWidth":"80px","sClass":"alignme"},
				{"sTitle":"Customer","sWidth":"110px","sClass":"alignme"},
				{"sTitle":"Shipment #:","sWidth":"110px","sClass":"alignme"},
				{"sTitle":"Shpmt Rcvd Date","sWidth":"130px","sClass":"alignme"},
				{"sTitle":"Vendor/Supplier","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Vendor Inv No.","sWidth":"350px","sClass":"alignmewrap"},
				//{"sTitle":"Total Inv No.","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Tax Reference","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Permit No.","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Total Qty","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Vendor Inv Value (USD)","sWidth":"160px","sClass":"alignme"},
				{"sTitle":"Vendor Inv Value (SGD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Frt Charges (SGD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Ins Charges (SGD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"CIF Amount (SGD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"GST Amount (SGD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Job Type","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Comment","sWidth":"350px","sClass":"alignmewrap"},
				{"sTitle":"Description","sWidth":"350px","sClass":"alignmewrap"},
				{"sTitle":"SU/Entry#.","sWidth":"350px","sClass":"alignmewrap"},
				{"sTitle":"Consignment order (Y/N)","sWidth":"150px","sClass":"alignme"},
				{"sTitle":"Previous SU# for consignment","sWidth":"200px","sClass":"alignme"}
			],
			fnDrawCallback: function( oSettings ) {
				$('#tblIncomingSummary tr td').css('white-space','normal');
			}
		});

	tblOutgoingDetailed = $('#tblOutgoingDetailed').dataTable({
			"sDom": '<"H">Tlfr<"F"tip>',
			"oTableTools": {
				"aButtons": [
					{
						"sExtends":    "collection",
						"sButtonText": "Save As",
						"aButtons":    [ "xls" ]
					}
				]
			},
			'bFilter': true,
			'bSortable' : false,
			//'aaSortingFixed': [[4,'asc'],[2,'asc']],
			'bPaginate': true,
			'bJQueryUI': true,
			'aLengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
			'sPaginationType': 'full_numbers',
			"bScrollCollapse": true,  
			"sScrollX":"100%",    
			"bDestroy": true,
			"aoColumns": [
				{"sTitle":"S/No","sWidth":"80px","sClass":"alignme"},
				{"sTitle":"Customer","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Shipment #","sWidth":"90px","sClass":"alignme"},
				{"sTitle":"Packlist No #","sWidth":"130px","sClass":"alignme"},
				{"sTitle":"Shipment Release Date.","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Shipment No.","sWidth":"200px","sClass":"alignme"},
				{"sTitle":"Vendor Inv No.","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Vendor / Supplier","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Comm Inv No/DN.","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Total Qty","sWidth":"180px","sClass":"alignme"},
				{"sTitle":"Total Qty(Shipment)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Unit Price","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Out Tax Ref","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Permit #","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Exchange Rate","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Dest Port","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Forwarders","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Consignee","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"HAWB No.","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"MAWB No.","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Tpt Mode","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Dep Date","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Delivery From","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Inco Terms","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Job Type","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Comment","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Customer Inv Value(USD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Customer Inv Value(SGD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"GST Amount(SGD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"OUT SU #","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Description","sWidth":"140px","sClass":"alignme"}
			]
		});
		
	tblOutgoingSummary = $('#tblOutgoingSummary').dataTable({
			"sDom": '<"H">Tlfr<"F"tip>',
			"oTableTools": {
				"aButtons": [
					{
						"sExtends":    "collection",
						"sButtonText": "Save As",
						"aButtons":    [ "xls" ]
					}
				]
			},
			'bFilter': true,
			'bPaginate': true,
			'bJQueryUI': true,
			'aLengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
			'sPaginationType': 'full_numbers',
			"bScrollCollapse": true,  
			"sScrollX":"100%",    
			"bDestroy": true,
			"aoColumns": [
				{"sTitle":"S/No","sWidth":"80px","sClass":"alignme"},
				{"sTitle":"Customer","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Shipment #","sWidth":"90px","sClass":"alignme"},
				{"sTitle":"Shipment Release Date.","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Shipment No.","sWidth":"200px","sClass":"alignme"},
				{"sTitle":"Vendor Inv No.","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Vendor / Supplier","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Comm Inv No/DN.","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Total Qty","sWidth":"180px","sClass":"alignme"},
				{"sTitle":"Unit Price","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Out Tax Ref","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Permit #","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Exchange Rate","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Dest Port","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Forwarders","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Consignee","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"HAWB No.","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"MAWB No.","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Tpt Mode","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Dep Date","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Delivery From","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Inco Terms","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Job Type","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Comment","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Customer Inv Value(USD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Customer Inv Value(SGD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"GST Amount(SGD)","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"OUT SU #","sWidth":"140px","sClass":"alignme"},
				{"sTitle":"Description","sWidth":"140px","sClass":"alignme"}
			]
		});

	tblPicklist = $('#tblPicklist').dataTable({
			"sDom": '<"H">Tlfr<"F"tip>',
			"oTableTools": {
				"aButtons": [
					{
						"sExtends":    "collection",
						"sButtonText": "Save As",
						"aButtons":    [ "xls" ]
					}
				]
			},
			'bFilter': true,
			'bSortable' : false,
			//'aaSortingFixed': [[3,'asc'],[2,'asc']],
			'bPaginate': true,
			'bJQueryUI': true,
			'aLengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
			'sPaginationType': 'full_numbers',
			"bScrollCollapse": true,  
			"sScrollX":"100%",    
			"bDestroy": true,
			"aoColumns": [
				{"sTitle":"#","sWidth":"80px","sClass":"alignme"},
				{"sTitle":"Out Reference #","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Shipment Release Date","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Out WMS Reference","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Picklist No.","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"DN No.","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Unit Price (USD)","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Out HAWB No.","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Out Permit No.","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Part No.","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Tax Scheme","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Total Outgoing Qty","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Out Box Entry/SU#","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"In Reference#","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Shpmt Rcvd Date","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"In WMS Reference","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Vendor / Supplier Name","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Vendor Inv No","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"In HAWB No","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"In Permit No.","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Part No.","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Tax Scheme","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Incoming (QTY)","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"In Box Entry/SU#","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Balance (QTY)","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Bal Box Entry/SU#","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Previous SU#","sWidth":"120px","sClass":"alignme"}
			]
		});
		
	//$('#tblShipmentSummary').append("<tfoot><tr><td style='text-align:center'>#</td><td style='text-align:center'>Reference #</td><td style='text-align:center'>SU/Entry Number</td><td style='text-align:center'>Tax Scheme</td><td style='text-align:center'>Incoming</td><td style='text-align:center'>Outgoing</td><td style='text-align:center'>Balance</td></tr></tfoot>")
	tblShipmentSummary = $('#tblShipmentSummary').dataTable({
			"sDom": '<"H">Tlfr<"F"tip>',
			"oTableTools": {
				"aButtons": [
					{
						"sExtends":    "collection",
						"sButtonText": "Save As",
						"aButtons":    [ "xls" ]
					}
				]
			},
			'bFilter': true,
			'bSortable' : false,
			//'aaSortingFixed': [[3,'asc'],[2,'asc']],
			'bPaginate': true,
			'bJQueryUI': true,
			'aLengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
			'sPaginationType': 'full_numbers',
			"bScrollCollapse": true,  
			"sScrollX":"100%",    
			"bDestroy": true,
			fnFooterCallback: function ( nRow, aaData, iStart, iEnd, aiDisplay ) {
				var iTotal = 0;
				var iTotal2 = 0;
				for ( var i=iStart ; i<iEnd ; i++ )	{ 

					iTotal += aaData[ aiDisplay[i] ][4] != 0 ? aaData[ aiDisplay[i] ][4] : 0; 
					iTotal2 += aaData[ aiDisplay[i] ][4] != 0 ? aaData[ aiDisplay[i] ][5] : 0; 
				}
				
				//var nCells = nRow.getElementsByTagName('td');
				//nCells[4].innerHTML = 'Total Incoming: <span class="totalNos">' + iTotal+'</span>';
				//nCells[5].innerHTML = 'Total Outgoing: <span class="totalNos">' + iTotal2+'</span>';				
			},
			"aoColumns": [
				{"sTitle":"#","sWidth":"80px","sClass":"alignme"},
				{"sTitle":"In Reference#","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Shpmt Rcvd Date","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"In WMS Reference","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Vendor / Supplier Name","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Vendor Inv No","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"In HAWB No","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"In Permit No.","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Part No.","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Tax Scheme","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Incoming (QTY)","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"In Box Entry/SU#","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Out Reference #","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Shipment Release Date","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Out WMS Reference","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Picklist No.","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"DN No.","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Unit Price (USD)","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Out HAWB No.","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Out Permit No.","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Part No.","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Tax Scheme","sWidth":"100px","sClass":"alignme"},
				{"sTitle":"Total Outgoing Qty","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Out Box Entry/SU#","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Balance (QTY)","sWidth":"120px","sClass":"alignme"},
				{"sTitle":"Bal Box Entry/SU#","sWidth":"120px","sClass":"alignme"}
			],
			//fnRowCallback: function( nRow, aData, iDisplayIndex ) {
			//	$('td:eq(0)', nRow).attr('field','#');
			//	$('td:eq(1)', nRow).attr('field','Reference #');
			//	$('td:eq(2)', nRow).attr('field','SU/Entry Number');
			//	$('td:eq(3)', nRow).attr('field','Tax Scheme');
			//	$('td:eq(4)', nRow).attr('field','Incoming');
			//	$('td:eq(5)', nRow).attr('field','Outgoing');
			//	$('td:eq(6)', nRow).attr('field','Balance');
			//	return nRow;
			//},
			//fnPreDrawCallback: function(oSettings) {						
			//	$('#scanTable_filter input').attr('placeholder','Search');
			//},
			//fnDrawCallback: function( oSettings ) {
			//	
			//}	
		});

	$("#tabs").tabs({
		select: function(event, ui) {
			var selected = ui.panel.id;
			switch(selected){
				case 'tabs-1':
					var t = setTimeout(function(){$('#tblIncomingDetailed').dataTable().fnAdjustColumnSizing();},1);
				break;				
				case 'tabs-2':
					var t = setTimeout(function(){$('#tblIncomingSummary').dataTable().fnAdjustColumnSizing();},1);
				break;
				case 'tabs-3':
					var t = setTimeout(function(){$('#tblOutgoingDetailed').dataTable().fnAdjustColumnSizing();},1);
				break;
				case 'tabs-4':
					var t = setTimeout(function(){$('#tblOutgoingSummary').dataTable().fnAdjustColumnSizing();},1);
				break;	
				case 'tabs-5':
					var t = setTimeout(function(){$('#tblPicklist').dataTable().fnAdjustColumnSizing();},1);
				case 'tabs-6':
					var t = setTimeout(function(){$('#tblShipmentSummary').dataTable().fnAdjustColumnSizing();},1);
				break;					
			} 
		}
	});	
	jQuery.fn.exists = function(){return this.length>0;}
	jQuery.isJson = function(str) {
	 if (jQuery.trim(str) == '') return false;
	 str = str.replace(/\\./g, '@').replace(/"[^"\\\n\r]*"/g, '');
	 return (/^[,:{}\[\]0-9.\-+Eaeflnr-u \n\r\t]*$/).test(str);
	}
	
	Number.prototype.padLeft = function(base,chr){
	    var  len = (String(base || 10).length - String(this).length)+1;
	    return len > 0? new Array(len).join(chr || '0')+this : this;
	}

	CurrentDate = [(CurrentDate.getMonth() + 1).padLeft(),CurrentDate.getDate().padLeft(),CurrentDate.getFullYear()].join('/');
	$('#startDate').val(CurrentDate);
	$('#endDate').val(CurrentDate);
	
	/*
	var astr = '';
	$.each(tblIncomingDetailed.fnSettings().aoColumns,function(a,b){
		astr = astr + "'" + b.sTitle + "',";
	});
	console.log(astr); */

	function NASort(a, b) {    
	    if (a.innerHTML == 'NA') {
	        return 1;   
	    }
	    else if (b.innerHTML == 'NA') {
	        return -1;   
	    }       
	    return (parseInt(a.innerHTML) > parseInt(b.innerHTML)) ? 1 : -1;
	};

	function ProcessRequest(data,gotoProcess){
		$.ajax({
		 type: 'POST',
		 url: 'modules/gstADIReport/GSTReportClass.php',
		 data: data,
		 success: function(data){
				var CallMeBaby = eval('(' + gotoProcess + ')');
						CallMeBaby(data);
		 },
		 beforeSend: function(){
				$.dimScreen(function() {
					$('#content').fadeIn();
				});
		 },
		 complete: function(){
			 $.dimScreenStop();
		 }
		});
	}
	

	function msgbox(msg,focusField){
		$( "#dialog-msgbox:ui-dialog" ).dialog( "destroy" );
		$( "#dialog-msgbox" ).html(msg);
		$( "#dialog-msgbox" ).attr("title",'GST Report');
		$( "#dialog-msgbox" ).dialog('option', 'position', 'center');
		$( "#dialog-msgbox" ).dialog({
			modal: true,
			height:'auto',
			show:{ 
				effect:"blind", 
				speed:'slow' 
			},
			hide:'blind',
			buttons: {
				Ok: function() {
					$(this).dialog( "close" );
				}
			}
		});
	}
	
	function PrepareForm(obj){
		
		$.each(obj[0],function(a,b){
			//b[0] - wawiindex
			//b[1] - wawialias
			//CurrentAvaiLablePicklistPerCustomer[b[0]['PackListNumber']] = [];
			CustomerList[b[0]] = b[1];
		});
		TaxReferenceList['All'] = ['All'];
		$.each(obj[1],function(a,b){
			TaxReferenceList[a] = [];
			$.each(b,function(c,d){
				$.each(d,function(f,g){
					TaxReferenceList[a].push(g);
				});
			});
		});

		//$('#slcCustomer')
		//   .append($("<option></option>")
		//   .attr("value",'All')
		//   .text('All'));

		$('#slcCustomer')
		   .append($("<option></option>")
		   .attr("value",'ADISGWMS')
		   .text('ADI SG'));

		//$.each(CustomerList,function(a,b){
	    // $('#slcCustomer')
	    //     .append($("<option></option>")
	    //     .attr("value",a)
	    //     .text(b)); 
		//});
		$('#slcCustomer').change();
	}
	
	function generateReport(objAll){
		var obj = new Object(); //Incoming Data
		var obj1= new Object(); //Outgoing Data
		var tmpArray;
		var i = 1;
		
		tblIncomingDetailed.fnClearTable();
		tblIncomingSummary.fnClearTable();
		obj = objAll[0];
		
		if (obj != 'nodata'){
			tmpArray = [];
			$('#tabs > ul').tabs({ selected: 1 });
			$.each(obj[0],function(a,c){
				$.each(c,function(d,b){
					//tmpArray.push([i,b['WawiAlias'],b['ShipReference'],b['ArrivalDate'],b['VendorInvNo'],b['totalInvoice'],b['Quantity'],b['InTaxRef'],b['PermitNo'],b['ExchangeRate'],parseFloat(b['InvoiceValue']).toFixed(2),parseFloat(b['InvoiceValueSGD']).toFixed(2),parseFloat(b['FreightCharges']).toFixed(2),parseFloat(b['InsCharges']).toFixed(2),parseFloat(b['CIFAmount']).toFixed(2),parseFloat(b['GSTAmount']).toFixed(2),b['OriginPort'],b['Forwarders'],b['Supplier'],b['HAWB'],b['MAWB'],b['TptMode'],b['ArrivalDate'],b['JSI'],b['INCOTERM'],b['JobType'],b['Comment'],b['IncomingReference'],b['Description'],b['ReceiptDate']]);
					var customer = '';
					if(b['WawiAlias'] == 'ADI'){
						customer = 'ADI 3rd Floor';
					}else if(b['WawiAlias'] == 'ADI 3rd Floor'){
						customer = 'ADI 3rd Floor';
					}else{
						customer = b['WawiAlias']
					}
					tmpArray.push([i,customer,b['ShipReference'],b['ArrivalDate'],b['VendorInvNos'],b['suQuantity']/*b['Quantity']*/,b['InTaxRef'],b['PermitNo'],b['ExchangeRate'],parseFloat(b['InvoiceValue1']).toFixed(2),parseFloat(b['InvoiceValueSGD2']).toFixed(2),parseFloat(b['FreightCharges']).toFixed(2),parseFloat(b['InsCharges']).toFixed(2),parseFloat(b['CIFAmount2']).toFixed(2),parseFloat(b['GSTAmount2']).toFixed(2),b['OriginPort'],b['Forwarders'],b['Supplier'],b['HAWB'],b['MAWB'],b['TptMode'],b['ArrivalDate'],b['JSI'],b['INCOTERM'],b['JobType'],b['Comment'],b['IncomingReference'],b['Description'],b['EntrySU'],b['isConsigned'],b['SUConsignedHistory']]);
					i++;
				});
			});
			tblIncomingDetailed.fnAddData(tmpArray);
			
			$('#tabs > ul').tabs({ selected: 2 });
			tmpArray = [];
			i = 1;
			$.each(obj[1],function(a,c){
				$.each(c,function(d,b){
					//tmpArray.push([i,b['Customer'],b['ShipReference'],b['ArrivalDate'],b['Supplier'],b['VendorInvNo'],b['totalInvoice'],b['InTaxRef'],b['PermitNo'],parseFloat(b['InvoiceValue']).toFixed(2),parseFloat(b['InvoiceValueSGD']).toFixed(2),parseFloat(b['FreightCharges']).toFixed(2),parseFloat(b['InsCharges']).toFixed(2),parseFloat(b['CIFAmount']).toFixed(2),parseFloat(b['GSTAmount']).toFixed(2),b['JobType'],b['Comments'],b['Description']]);
					var customer = '';
					if(b['Customer'] == 'ADI'){
						customer = 'ADI 3rd Floor';
					}else if(b['Customer'] == 'ADI 3rd Floor'){
						customer = 'ADI 3rd Floor';
					}else{
						customer = b['Customer']
					}
					tmpArray.push([i,customer,b['ShipReference'],b['ArrivalDate1'],b['Supplier'],b['VendorInvNo2'],b['InTaxRef'],b['PermitNo'],b['ShipmentQty'],parseFloat(b['InvoiceValue']).toFixed(2),parseFloat(b['InvoiceValueSGD']).toFixed(2),parseFloat(b['FreightCharges']).toFixed(2),parseFloat(b['InsCharges']).toFixed(2),parseFloat(b['CIFAmount']).toFixed(2),parseFloat(b['GSTAmount']).toFixed(2),b['JobType'],b['Comments'],b['Description1'],b['EntrySU'],b['isConsigned'],b['SUConsignedHistory']]);
					i++;
				});
			});
			tblIncomingSummary.fnAddData(tmpArray);
		}

		obj1 = objAll[1];
		tblOutgoingDetailed.fnClearTable();
		tblOutgoingSummary.fnClearTable();
		
		if (obj1 != 'nodata'){
			tmpArray = [];
			i = 1;
			var outRef = '';
			var outTax = '';
			var outPick ='';
			$.each(obj1[0],function(c,d){
				//$.each(d,function(e,f){
					$.each(d,function(a,b){
						//console.log(b);
						//if(outRef == b['OutShipReference'] && outTax == b['TaxScheme'] && outPick ==b['PacklistNumber'])
						//{
						//	outRef = b['OutShipReference'];
						//	outTax = b['TaxScheme'];
						//	outPick = b['PacklistNumber'];
						//	tmpArray.push([i,b['WawiIndex'],b['OutShipReference'],b['PacklistNumber'],b['ShipDate1'],b['Invoice'],b['CustInvNo1'],b['TotalQty'],b['Supplier'],b['TaxScheme'],b['PermitNo'],b['ExchangeRate'],b['Destination'],b['Forwarders'],b['CustomerName1'],b['HAWB'],b['MAWB'],b['Flight'],b['DepDate1'],'JSI',b['IncoTerms'],b['JobType'],b['Remarks'],parseFloat(0).toFixed(2),parseFloat(0).toFixed(2),parseFloat(0).toFixed(2)]);
						//}
						//else
						//{
						//	if(outRef == b['OutShipReference'])
						//	{
						//		tmpArray.push([i,b['WawiIndex'],b['OutShipReference'],b['PacklistNumber'],b['ShipDate1'],b['Invoice'],b['CustInvNo1'],b['TotalQty'],b['Supplier'],b['TaxScheme'],b['PermitNo'],b['ExchangeRate'],b['Destination'],b['Forwarders'],b['CustomerName1'],b['HAWB'],b['MAWB'],b['Flight'],b['DepDate1'],'JSI',b['IncoTerms'],b['JobType'],b['Remarks'],parseFloat(b['CustInvValue']).toFixed(2),parseFloat(0).toFixed(2),parseFloat(0).toFixed(2)]);
						//	}
						//	else
						//	{
						//	tmpArray.push([i,b['WawiIndex'],b['OutShipReference'],b['PacklistNumber'],b['ShipDate1'],b['Invoice'],b['CustInvNo1'],b['TotalQty'],b['Supplier'],b['TaxScheme'],b['PermitNo'],b['ExchangeRate'],b['Destination'],b['Forwarders'],b['CustomerName1'],b['HAWB'],b['MAWB'],b['Flight'],b['DepDate1'],'JSI',b['IncoTerms'],b['JobType'],b['Remarks'],parseFloat(b['CustInvValue']).toFixed(2),parseFloat(b['InvoiceValueSGD']).toFixed(2),parseFloat(b['GstAmount']).toFixed(2)]);
						//	}
						//	outRef = b['OutShipReference'];
						//	outTax = b['TaxScheme'];
						//	outPick = b['PacklistNumber'];
						//}
						
						tmpArray.push([i,
									   b['Customer'],
									   b['ShipmentRef'],
									   b['PackListNumber'],
									   b['CargoReleasedDate'],
									   b['ShipmentNumber'],
									   b['CustInvoice'],
									   b['CustSupplier'],
									   b['DN'],
									   b['TotalQty'],
									   b['ShipTotalQty'],
									   b['UnitPrice'],
									   b['OutTaxRef'],
									   b['PermitNo'],
									   b['ExchangeRate'],
									   b['ShippingCountry'],
									   b['Forwarder'],
									   b['ShipTo'],
									   b['HAWB'],
									   b['MAWB'],
									   b['TptMode'],
									   b['DepDate'],
									   b['DeliveryFrom'],
									   b['IncoTerm'],
									   b['JobType'],
									   b['Comment'],
									   b['CustInvValue'],
									   b['InvoiceValueSGD'],
									   b['GSTAmountValue'],
									   b['SUNumbers'],
									   b['MaterialDescriptions']]);
						
						i++;
					});
				//});
			});
			tblOutgoingDetailed.fnAddData(tmpArray);
			
			tmpArray = [];
			i = 1;
			
			
			var outSR = '';
			var tax = '';
			$.each(obj1[1],function(c,d){
				//console.log(d);
				$.each(d,function(a,b){
					//if(outSR == b['OutShipReference']){
					//	outSR = b['OutShipReference'];
					//	tmpArray.push([i,b['WawiIndex'],b['OutShipReference'],b['PacklistNumber'],b['Invoice'],b['CustInvNo1'],b['TotalQty'],b['TaxScheme'],'0.00','0.00','0.00',b['JobType'],b['PermitNo'],b['HAWB'],b['MAWB'],b['Flight'],b['Remarks'],b['ShipDate1'],b['CustomerName1'],b['DepDate']]);
					//}else{
					//	outSR = b['OutShipReference'];
					//	tmpArray.push([i,b['WawiIndex'],b['OutShipReference'],b['PacklistNumber'],b['Invoice'],b['CustInvNo1'],b['TotalQty'],b['TaxScheme'],b['TotalCustInvValueUSD'],b['CustInvValueSGD'],b['GSTAmount'],b['JobType'],b['PermitNo'],b['HAWB'],b['MAWB'],b['Flight'],b['Remarks'],b['ShipDate1'],b['CustomerName1'],b['DepDate']]);
					//}
					
					tmpArray.push([i,
								   b['Customer'],
								   b['ShipmentRef'],
								   b['CargoReleasedDate'],
								   b['ShipmentNumber'],
								   b['CustInvoice'],
								   b['CustSupplier'],
								   b['DN'],
								   b['TotalQty'],
								   b['UnitPrice'],
								   b['OutTaxRef'],
								   b['PermitNo'],
								   b['ExchangeRate'],
								   b['ShippingCountry'],
								   b['Forwarder'],
								   b['ShipTo'],
								   b['HAWB'],
								   b['MAWB'],
								   b['TptMode'],
								   b['DepDate'],
								   b['DeliveryFrom'],
								   b['IncoTerm'],
								   b['JobType'],
								   b['Comment'],
								   b['CustInvValue'],
								   b['InvoiceValueSGD'],
								   b['GSTAmountValue'],
								   b['SUNumbers'],
								   b['MaterialDescriptions']]);
					
					i++;
				});
			});
			tblOutgoingSummary.fnAddData(tmpArray);
			
		}

		obj2 = objAll[2];

		tblPicklist.fnClearTable();
		if(obj2 != 'nodata'){
			tmpArray = [];
			i = 1;
			$.each(obj2,function(c,d){
				$.each(d,function(x,v){
					tmpArray.push([i,
								   v['OutRef'],
								   v['ReleasedDate'],
								   v['OutWMSRef'],
								   v['PackListnumber'],
								   v['CustomerReference'],
								   v['UnitPrice'],
								   v['HAWB'],
								   v['OutPermit'],
								   v['OutPart'],
								   v['OutTaxScheme'],
								   v['OutQty'],
								   v['OutEntry'],
								   v['InRef'],
								   v['ReceivedDate'],
								   v['InWMSRef'],
								   v['Supplier'],
								   v['Invoice'],
								   v['AirwayBill'],
								   v['InPermit'],
								   v['InPart'],
								   v['InTaxScheme'],
								   v['InQty'],
								   v['InEntry'],
								   v['SUBal'],
								   v['BalEntry'],
								   v['PreviousEntry']]);
						i++;
				});
			});
			tblPicklist.fnAddData(tmpArray);
		}

		obj3 = objAll[3];

		tblShipmentSummary.fnClearTable();
		if(obj3 != 'nodata'){
			tmpArray = [];
			i = 1;
			$.each(obj3,function(c,d){
				$.each(d,function(x,v){
					tmpArray.push([i,
								   v['InRef'],
								   v['ReceivedDate'],
								   v['InWMSRef'],
								   v['Supplier'],
								   v['Invoice'],
								   v['AirwayBill'],
								   v['InPermit'],
								   v['InPart'],
								   v['InTaxScheme'],
								   v['InQty'],
								   v['InEntry'],
								   v['OutRef'],
								   v['ReleasedDate'],
								   v['OutWMSRef'],
								   v['PackListnumber'],
								   v['CustomerReference'],
								   v['UnitPrice'],
								   v['HAWB'],
								   v['OutPermit'],
								   v['OutPart'],
								   v['OutTaxScheme'],
								   v['OutQty'],
								   v['OutEntry'],
								   v['SUBal'],
								   v['BalEntry'],
								   v['PreviousEntry']]);
						i++;
				});
			});
			tblShipmentSummary.fnAddData(tmpArray);
		}
	}

	$('#shipmentReference').keyup(function(event){
	
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if(keycode == '13'){
			$('#entry-show').css({'display':''});
			ProcessRequest('todo=getEntryByReference&shipmentReference='+$('#shipmentReference').val()+'&customer='+$('#slcCustomer').val(),'displayEntry');
		}

	});
	
	function displayEntry(data){
		$('#entryNumber').empty();
		$('#entryNumber').append($('<option>', { 
		        value: 'All',
		        text : 'All'
		    }));
		if(data != 'nodata'){
			$.each(data, function (i, item) {
			
			    $('#entryNumber').append($('<option>', { 
			        value: item.EntryNumber,
			        text : item.EntryNumber
			    }));
			});
		}
		
	}

	$('#slcCustomer').change(function(){
		$('#slcReference').empty();
		if (TaxReferenceList[$('#slcCustomer').val()] != 'No TaxScheme' && TaxReferenceList[$('#slcCustomer').val()].length > 1){
	     $('#slcReference')
	         .append($("<option></option>")
	         .attr("value",'All')
	         .text('All'));
		}
		
		$.each(TaxReferenceList[$('#slcCustomer').val()],function(a,b){
	     $('#slcReference')
	         .append($("<option></option>")
	         .attr("value",b)
	         .text(b));
		});
	});
	
	$('#generateReport').click(function(){
		var check = 'false';
		if ($('#checkAll').is(":checked"))
		{
			check = 'true';
			if($('#slcCustomer').val() != 'All'){
				ProcessRequest({'todo':'generateReport','startDate':$('#startDate').val(),'endDate':$('#endDate').val(),'slcCustomer':$('#slcCustomer').val(),'slcReference':$('#slcReference').val(),'ShipReference':$('#txtSReference').val(),'EntryNumber':$('#txtSEntry').val(),'TaxScheme':$('#txtSTaxScheme').val(),'Incoming':$('#txtSIncoming').val(),'Outgoing':$('#txtSOutgoing').val(),'Balance':$('#txtSBalance').val(),'Search':check},'generateReport');
			}else{
				alert('Please choose customer');
			}
		}
		else{
			ProcessRequest({'todo':'generateReport','startDate':$('#startDate').val(),'endDate':$('#endDate').val(),'slcCustomer':$('#slcCustomer').val(),'slcReference':$('#slcReference').val(),'ShipReference':$('#txtSReference').val(),'EntryNumber':$('#txtSEntry').val(),'TaxScheme':$('#txtSTaxScheme').val(),'Incoming':$('#txtSIncoming').val(),'Outgoing':$('#txtSOutgoing').val(),'Balance':$('#txtSBalance').val(),'Search':check},'generateReport');
		}

		//console.log(check);
		
	});

	//$('#txtFilterThis').keyup(function(event){
	//	tblIncomingDetailed.fnFilter($(this).val(), 4);
	//});
	
	$('#txtFilterThis').on('keyup change clear',function(){
		tblIncomingDetailed.fnFilter($(this).val(), 4);
	});
	
	$('#exportToExcel').click(function(e){
		
		
		//console.log($(".tblIncomingDetailed_header").find('.filter_column'));
		//console.log($(".tblIncomingDetailed_header").find('.filter_column').find("input").val());
		//$(".tblIncomingDetailed_header").find('.filter_column').find("input").each(function() {
		//	console.log($(this).val());
		//	if($(this).val() == 'VendorInvNo.'){
		//		console.log($(this).val());
		//		$(this).css("width", "350px");
		//	}
		//	//$("#txtname").css("width", textsize);
		//	// console.log($(this).val(i));
		//	// console.log(i);
		//	// i++;
		//});
		
		//tblIncomingDetailed.fnFilter('8002255559', 4);
		//tblIncomingDetailed.fnDraw()
		//console.log(tblIncomingDetailed);
		//console.log(tblIncomingSummary);
		//console.log(tblIncomingDetailed.fnSettings());
		//console.log(tblIncomingDetailedSetting.fnRecordsDisplay);
		//console.log(tblIncomingDetailedSetting.fnFooterCallback);
		//console.log(tblIncomingDetailedSetting.asDataSearch);
		//console.log(tblIncomingDetailedSetting.aiDisplay);
		//console.log(tblIncomingDetailed.fnGetData());
		//console.log(tblIncomingDetailedArray);
		//console.log(tblShipmentSummary.fnGetData());
		//return false;
		
		var tblIncomingDetailedSetting	 = tblIncomingDetailed.fnSettings();
		var tblIncomingDetailedArray = [];
		$.each(tblIncomingDetailedSetting.aiDisplay,function(ikey,arrayKey){
			tblIncomingDetailedArray.push(tblIncomingDetailed.fnGetData()[arrayKey]);
		});
		
		var tblIncomingSummarySetting	 = tblIncomingSummary.fnSettings();
		var tblIncomingSummaryArray = [];
		$.each(tblIncomingSummarySetting.aiDisplay,function(ikey,arrayKey){
			tblIncomingSummaryArray.push(tblIncomingSummary.fnGetData()[arrayKey]);
		});
		
		var tblOutgoingDetailedSetting	 = tblOutgoingDetailed.fnSettings();
		var tblOutgoingDetailedArray = [];
		$.each(tblOutgoingDetailedSetting.aiDisplay,function(ikey,arrayKey){
			tblOutgoingDetailedArray.push(tblOutgoingDetailed.fnGetData()[arrayKey]);
		});
		
		var tblOutgoingSummarySetting	 = tblOutgoingSummary.fnSettings();
		var tblOutgoingSummaryArray = [];
		$.each(tblOutgoingSummarySetting.aiDisplay,function(ikey,arrayKey){
			tblOutgoingSummaryArray.push(tblOutgoingSummary.fnGetData()[arrayKey]);
		});
		
		var tblPicklistSetting	 = tblPicklist.fnSettings();
		var tblPicklistArray = [];
		$.each(tblPicklistSetting.aiDisplay,function(ikey,arrayKey){
			tblPicklistArray.push(tblPicklist.fnGetData()[arrayKey]);
		});
		
		var tblShipmentSummarySetting	 = tblShipmentSummary.fnSettings();
		var tblShipmentSummaryArray = [];
		$.each(tblShipmentSummarySetting.aiDisplay,function(ikey,arrayKey){
			tblShipmentSummaryArray.push(tblShipmentSummary.fnGetData()[arrayKey]);
		});
		
		
		var tblIncomingDetailedCount = tblIncomingDetailedArray.length;
		var tblIncomingSummaryCount  = tblIncomingSummaryArray.length;
		var tblOutgoingDetailedCount = tblOutgoingDetailedArray.length;
		var tblOutgoingSummaryCount	 = tblOutgoingSummaryArray.length;
		var tblPicklistCount	 	 = tblPicklistArray.length;
		var tblShipmentSummaryCount	 = tblShipmentSummaryArray.length;
		
		if (tblIncomingDetailedCount == 0 && tblIncomingSummaryCount == 0 && tblOutgoingDetailedCount == 0 && tblOutgoingSummaryCount == 0 && tblPicklistCount == 0 && tblShipmentSummaryCount == 0){
			msgbox('No data to Export');
		} else {
			$.fileDownload('modules/gstADIReport/ExportToExcel.php', { 	httpMethod : "POST",
																		data: { StartDate : $('#startDate').val(),
																		EndDate : $('#endDate').val(),
																		TaxRef : $('#slcReference').val(),
																		Customer : $('#slcCustomer').val(),
																		tblIncomingDetailed : tblIncomingDetailedArray,
																		tblIncomingSummary : tblIncomingSummaryArray,
																		tblOutgoingDetailed : tblOutgoingDetailedArray,
																		tblOutgoingSummary : tblOutgoingSummaryArray,
																		tblPicklistCount: tblPicklistArray,
																		tblShipmentSummaryCount: tblShipmentSummaryArray
																	 }});
		}
		
	});
	
	tblIncomingDetailed.fnDraw();
	tblIncomingSummary.fnDraw();
	tblOutgoingDetailed.fnDraw();
	tblOutgoingSummary.fnDraw();
	tblPicklist.fnDraw();
	tblShipmentSummary.fnDraw();
	
	ProcessRequest({'todo':'PrepareForm'},'PrepareForm');
	
});	
</script>
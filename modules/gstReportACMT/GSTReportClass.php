<?php
ini_set('memory_limit', '1024M');

if (isset($_POST['todo'])){
	require ('../../include/config.php');
	$GSTReportClass = new GSTReportClass($_REQUEST);
	
	
	header('Content-Type: application/json');
	if ($_POST['todo'] == 'PrepareForm'){
		echo $GSTReportClass->PrepareForm();
	}elseif($_POST['todo'] == 'generateReport'){
		echo $GSTReportClass->generateReport();
	}
}

Class GSTReportClass{
	var $PostVars;
	function __construct($vars){
		$this->PostVars = $vars;
		$serverConn = unserialize(base64_decode(SQL_CONN));
		$this->conn = mssql_connect($serverConn[0],$serverConn[1],$serverConn[2]);
		mssql_select_db("Import");
		
		foreach($this->PostVars as $a => $b){
			$this->PostVars[$a] = $this->mssqlQuotedString($b);
		}
	}
	
	function mssqlQuotedString($str){
		$str = str_replace("\\", "\\\\", $str);
		$str = str_replace("'", "''", $str);
		
		return $str;
	}
	
	function PrepareForm(){
		mssql_select_db('MasterData');
		$sql = mssql_query("select ID,GSTIndex AS WawiIndex,WawiAlias from WawiCustomer Where [status] = 1 and GST = 1 and id = 215 order by WawiAlias");

		while($row = mssql_fetch_assoc($sql)){
			if (!isset($CustomerList[$row['WawiIndex']])){
				$CustomerList[$row['WawiIndex']] = array();
			}
			
			if($row['ID'] == 215){
				$row['WawiAlias'] = 'ADI SG';
			}
			
			$CustomerList[$row['WawiIndex']] = array($row['WawiIndex'],$row['WawiAlias']);
			$sqlTaxScheme = mssql_query("select TaxScheme from TaxScheme where [status] = 1 and WawiID = ".$row['ID']);
			if (mssql_num_rows($sqlTaxScheme) > 0){
				while($TaxSchemeList[$row['WawiIndex']][] = mssql_fetch_assoc($sqlTaxScheme)){}
				array_pop($TaxSchemeList[$row['WawiIndex']]);
			} else {
				$TaxSchemeList[$row['WawiIndex']][0] = array('TaxScheme' => 'No TaxScheme');
			}
		}
		
		$data = array();
		array_push($data,$CustomerList);
		array_push($data,$TaxSchemeList);
		
		return json_encode($data);
	}
	
	function generateReport(){
		$data = array();
		
		array_push($data,$this->GetIncomingReport());
		array_push($data,$this->GetOutgoingReport());
		array_push($data,$this->GetInventoryReport());
		array_push($data,$this->GetFilingReport());
		array_push($data,$this->GetFilingValueReport());
		
		return json_encode($data);
	}
	
	function GetIncomingReport(){
		$sqlString = "SELECT CONVERT(VARCHAR, [Month], 101)[Date], [Invoice], [Quantity] FROM [ADISGWMS]..v_GSTinACMTreport WHERE [Month] BETWEEN '".$this->PostVars['startDate']."' AND '".$this->PostVars['endDate']."' ORDER BY [Invoice] ASC";
		
		$sql = mssql_query($sqlString);
		
		if (mssql_num_rows($sql) > 0){
			
			while($dataArr[] = mssql_fetch_assoc($sql));
			array_pop($dataArr);
			
			$data = $dataArr;
			
			return $data;
		} else {
			return 'nodata';
		}
	}
	
	function GetOutgoingReport(){
		$sqlString = "SELECT CONVERT(VARCHAR, [Date], 101)[Date], [Invoice], [DN], [Qty][Quantity] FROM [ADISGWMS]..v_GSToutACMTreport ORDER BY [DN] ASC";
		
		$sql = mssql_query($sqlString);
		
		if (mssql_num_rows($sql) > 0){
			
			while($dataArr[] = mssql_fetch_assoc($sql));
			array_pop($dataArr);
			
			$data = $dataArr;
			
			return $data;
		} else {
			return 'nodata';
		}
	}
	
	function GetInventoryReport(){
		$sqlString = "SELECT * FROM [ADISGWMS]..v_GST_INV_ACMTreport";
		
		//if (trim($this->PostVars['slcCustomer']) != 'All'){
		//	$sqlString.=" and Customer='".trim($this->PostVars['slcCustomer'])."'";
		//}
		
		//if (trim($this->PostVars['slcReference']) != 'All' and trim($this->PostVars['slcReference']) != 'No TaxScheme'){
		//	$sqlString.=" and InTaxRef='".trim($this->PostVars['slcReference'])."'";
		//}
		
		$sqlString.=" Where QtyBal > 0";
		$sqlString.=" Order By Invoice ASC";
		
		$sql = mssql_query($sqlString);
		
		if (mssql_num_rows($sql) > 0){
			
			while($dataArr[] = mssql_fetch_assoc($sql));
			array_pop($dataArr);
			
			$data = $dataArr;
			
			return $data;
		} else {
			return 'nodata';
		}
	}
	
	function GetFilingReport(){
		//$sqlString = "SELECT CONVERT(VARCHAR, [RelDate], 101)[RelDate],[ShipmentNo],[DN],[Qty],[TaxScheme],SUBSTRING([Route],3,2)[DestinationCountry]
		//				FROM [ADISGWMS]..GSTFiling
		//				LEFT OUTER JOIN [ADISGWMS]..[Order] ON CAST([Order].[OrderNumber] AS VARCHAR) = CAST([GSTFiling].[DN] AS VARCHAR)
		//				WHERE [RelDate] BETWEEN '".$this->PostVars['startDate']."' AND '".$this->PostVars['endDate']."'
		//				ORDER BY [DN]";
		
		$sqlString = "SELECT CONVERT(VARCHAR, [RelDate], 101)[RelDate],[ShipmentNo],[DN],[Qty],[TaxScheme],[Route][DestinationCountry]
						FROM [ADISGWMS]..GSTFiling
						WHERE [RelDate] BETWEEN '".$this->PostVars['startDate']."' AND '".$this->PostVars['endDate']."'
						ORDER BY [ShipmentNo] ASC";
		
		$sql = mssql_query($sqlString);
		
		if (mssql_num_rows($sql) > 0){
			
			while($dataArr[] = mssql_fetch_assoc($sql));
			array_pop($dataArr);
			
			$data = $dataArr;
			
			return $data;
		} else {
			return 'nodata';
		}
	}
	
	function GetFilingValueReport(){
		$sqlString = "SELECT * FROM [ADISGWMS]..GSTFilingVal ORDER BY [ShipmentNo] ASC";
		
		$sql = mssql_query($sqlString);
		
		if (mssql_num_rows($sql) > 0){
			
			while($dataArr[] = mssql_fetch_assoc($sql));
			array_pop($dataArr);
			
			$data = $dataArr;
			
			return $data;
		} else {
			return 'nodata';
		}
	}
	
	function array_orderby(){
	    $args = func_get_args();
	    $data = array_shift($args);
	    foreach ($args as $n => $field) {
	        if (is_string($field)) {
	            $tmp = array();
	            foreach ($data as $key => $row)
	                $tmp[$key] = $row[$field];
	            $args[$n] = $tmp;
	            }
	    }
	    $args[] = &$data;
	    call_user_func_array('array_multisort', $args);
	    return array_pop($args);
	}
	
	function mssql_escape($str) {
    return str_replace("'", "''", $str);
	}
	
	function AnalyzeSQL($sql){
		if (mssql_num_rows($sql) > 0){
			while($row[] = mssql_fetch_assoc($sql)){}
			array_pop($row);
		} else {
			$row = 'nodata';
		}
		
		return $row;
	}
	
}

?>
<?php 
//error_reporting(E_ALL);
//ini_set("display_errors",1);

//add arrival date on the report
session_start();
require ('../../include/referer.checker.php');
require ('../../include/session.checker.php');

#style="border:1px solid #EEEEEE;"

?>

<style>
	.alignme{
		text-align:center;
		white-space: nowrap;
	}
	table.display td{
		white-space: wrap;
		word-wrap:break-word;
	}
</style>

<h2><?php include '../../tpl/module_shortcut.php';?>GST Report (ACMT)</h2> 
<script src="modules/gstReport/jquery.fileDownload.js">
</script>

<form onsubmit="return false;" class="globalform">
<ol>
	<div style="float:left;">
		<li>
			<label style="width:100px;">Start Date</label>
			<input type="text" class="text" value="" readonly name="startDate" id="startDate" />
		</li>
		<li>
			<label style="width:100px;">End Date</label>
			<input type="text" class="text" value="" readonly name="endDate" id="endDate" />
		</li>
	</div>
	<div style="float:left; margin-left: 15px;">
		<li>
			<label style="width:100px;">Customer</label>
			<select class="select" id="slcCustomer" name="slcCustomer"></select>
		</li>
		<li style="display: none;">
			<label style="width:100px;">Tax Reference</label>
			<select class="select" id="slcReference" name="slcReference"></select>
		</li>
	</div>
	<div style="float:left; margin-left: 15px;">
		<li><label style="width:10px;">&nbsp;</label><input type="button" class="button" style="width:150px;" value="Generate Report" id="generateReport" /></li>
		<li><label style="width:10px;">&nbsp;</label><input type="button" class="button" style="width:150px;" value="Export to Excel" id="exportToExcel" /></li>
	</div>
</ol>
</form>
<div class="clr"></div>
<br/>
<div id="DivTabs">
	<div id="tabs">
	  <ul>
	      <li><a href="#tabs-1">Incoming Report</a></li>
	      <li><a href="#tabs-2">Outgoing Report</a></li>
	      <li style="display:none"><a href="#tabs-3">Inventory Report</a></li>
	      <li><a href="#tabs-4">Shipping Report</a></li>
	      <li><a href="#tabs-5">Summary Report</a></li>
	  </ul>
	  <div id="tabs-1">
	     <table class="display" id="tblIncoming"></table>
	  </div>
	  <div id="tabs-2">
	     <table class="display" id="tblIOutgoing"></table>
	  </div>
	  <div id="tabs-3" style="display:none">
			<table class="display" id="tblIInventory"></table> 
	  </div>
	  <div id="tabs-4">
			<table class="display" id="tblFiling"></table>
	  </div>
	  <div id="tabs-5">
			<table class="display" id="tblFilingValue"></table>
	  </div>
	</div>
</div>
<script>
ErrorKeys = new Object();
ErrorKeys['First'] = 'Beginning of the data';
ErrorKeys['Previous'] = 'Beginning of the data';
ErrorKeys['Last'] = 'Last data';
ErrorKeys['Next'] = 'Last data';
ErrorKeys['Search'] = 'Sorry invalid Shipment Number';
ErrorKeys['nodata'] = 'Sorry no available Reference Number';
ErrorKeys['ErrorSearchingShipment'] = 'Shipment Number is a required field in order to Search.';

var TaxReferenceList = new Object(); // get from masterdata - TaxScheme
var CustomerList = new Object();
var CurrentDate = new Date();

var tblIncoming;
var tblIOutgoing;
var tblIInventory;
var tblFiling;

$(function() {
	$("input:submit, input:button, button, .button").button();
	//$('#DivTabs').hide();
	
	$( "#startDate" ).datepicker({		
			dateFormat: 'mm/dd/yy',
	    defaultDate: "+1w",
	    changeMonth: true,
	    onClose: function( selectedDate ) {
	        $( "#endDate" ).datepicker( "option", "minDate", selectedDate );
	    }
	});
	
	$( "#endDate" ).datepicker({
			dateFormat: 'mm/dd/yy',
	    defaultDate: "+1w",
	    changeMonth: true,
	    onClose: function( selectedDate ) {
	        $( "#startDate" ).datepicker( "option", "maxDate", selectedDate );
	    }
	});

	tblIncoming = $('#tblIncoming').dataTable({
			"sDom": '<"H">Tlfr<"F"tip>',
			"oTableTools": {
				"aButtons": [
					{
						"sExtends":    "collection",
						"sButtonText": "Save As",
						"aButtons":    [ "xls" ]
					}
				]
			},
			'bFilter': true,
			'bSortable' : false,
			//'aaSortingFixed': [[3,'asc'],[2,'asc']],
			'bPaginate': true,
			'bJQueryUI': true,
			'aLengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
			'sPaginationType': 'full_numbers',
			"bScrollCollapse": true,  
			"sScrollX":"100%",    
			"bDestroy": true,
			"aoColumns": [
				{"sTitle":"S/No","sWidth":"10%","sClass":"alignme"},
				{"sTitle":"Date","sWidth":"20%","sClass":"alignme"},
				{"sTitle":"Invoice #","sWidth":"35%","sClass":"alignme"},
				{"sTitle":"Quantity","sWidth":"35%","sClass":"alignme"}
			]
		});

	tblIOutgoing = $('#tblIOutgoing').dataTable({
			"sDom": '<"H">Tlfr<"F"tip>',
			"oTableTools": {
				"aButtons": [
					{
						"sExtends":    "collection",
						"sButtonText": "Save As",
						"aButtons":    [ "xls" ]
					}
				]
			},
			'bFilter': true,
			'bPaginate': true,
			'bJQueryUI': true,
			'aLengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
			'sPaginationType': 'full_numbers',
			"bScrollCollapse": true,  
			"sScrollX":"100%",    
			"bDestroy": true,
			"aoColumns": [
				{"sTitle":"S/No","sWidth":"10%","sClass":"alignme"},
				{"sTitle":"Date","sWidth":"20%","sClass":"alignme"},
				{"sTitle":"Incoming Invoice #","sWidth":"25%","sClass":"alignme"},
				{"sTitle":"DN #","sWidth":"25%","sClass":"alignme"},
				{"sTitle":"Quantity","sWidth":"20%","sClass":"alignme"}
			]
		});

	tblIInventory = $('#tblIInventory').dataTable({
			"sDom": '<"H">Tlfr<"F"tip>',
			"oTableTools": {
				"aButtons": [
					{
						"sExtends":    "collection",
						"sButtonText": "Save As",
						"aButtons":    [ "xls" ]
					}
				]
			},
			'bFilter': true,
			'bSortable' : false,
			//'aaSortingFixed': [[4,'asc'],[2,'asc']],
			'bPaginate': true,
			'bJQueryUI': true,
			'aLengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
			'sPaginationType': 'full_numbers',
			"bScrollCollapse": true,  
			"sScrollX":"100%",    
			"bDestroy": true,
			"aoColumns": [
				{"sTitle":"S/No","sWidth":"10%","sClass":"alignme"},
				{"sTitle":"Incoming Invoice #","sWidth":"45%","sClass":"alignme"},
				{"sTitle":"Quantity","sWidth":"45%","sClass":"alignme"}
			]
		});
		
	tblFiling = $('#tblFiling').dataTable({
			"sDom": '<"H">Tlfr<"F"tip>',
			"oTableTools": {
				"aButtons": [
					{
						"sExtends":    "collection",
						"sButtonText": "Save As",
						"aButtons":    [ "xls" ]
					}
				]
			},
			'bFilter': true,
			'bPaginate': true,
			'bJQueryUI': true,
			'aLengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
			'sPaginationType': 'full_numbers',
			"bScrollCollapse": true,  
			"sScrollX":"100%",    
			"bDestroy": true,
			"aoColumns": [
				{"sTitle":"S/No","sWidth":"10%","sClass":"alignme"},
				{"sTitle":"Cargo Release Date","sWidth":"15%","sClass":"alignme"},
				{"sTitle":"Shipment No","sWidth":"20%","sClass":"alignme"},
				{"sTitle":"DN #","sWidth":"15%","sClass":"alignme"},
				{"sTitle":"Quantity","sWidth":"15%","sClass":"alignme"},
				{"sTitle":"TaxScheme","sWidth":"15%","sClass":"alignme"},
				{"sTitle":"Job Type","sWidth":"10%","sClass":"alignme"}
			]
		});
		
	tblFilingValue = $('#tblFilingValue').dataTable({
			"sDom": '<"H">Tlfr<"F"tip>',
			"oTableTools": {
				"aButtons": [
					{
						"sExtends":    "collection",
						"sButtonText": "Save As",
						"aButtons":    [ "xls" ]
					}
				]
			},
			'bFilter': true,
			'bPaginate': true,
			'bJQueryUI': true,
			'aLengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
			'sPaginationType': 'full_numbers',
			"bScrollCollapse": true,  
			"sScrollX":"100%",    
			"bDestroy": true,
			"aoColumns": [
				{"sTitle":"S/No","sWidth":"10%","sClass":"alignme"},
				{"sTitle":"Shipment #","sWidth":"25%","sClass":"alignme"},
				{"sTitle":"TaxScheme","sWidth":"20%","sClass":"alignme"},
				{"sTitle":"CI Value (USD)","sWidth":"15%","sClass":"alignme"},
				{"sTitle":"Declared Value (SGD)","sWidth":"15%","sClass":"alignme"},
				{"sTitle":"Job Type","sWidth":"15%","sClass":"alignme"}
			]
		});

	$("#tabs").tabs({
		select: function(event, ui) {
			var selected = ui.panel.id;
			switch(selected){
				case 'tabs-1':
					var t = setTimeout(function(){$('#tblIncoming').dataTable().fnAdjustColumnSizing();},1);
				break;				
				case 'tabs-2':
					var t = setTimeout(function(){$('#tblIOutgoing').dataTable().fnAdjustColumnSizing();},1);
				break;
				case 'tabs-3':
					var t = setTimeout(function(){$('#tblIInventory').dataTable().fnAdjustColumnSizing();},1);
				break;
				case 'tabs-4':
					var t = setTimeout(function(){$('#tblFiling').dataTable().fnAdjustColumnSizing();},1);
				break;	
				case 'tabs-5':
					var t = setTimeout(function(){$('#tblFilingValue').dataTable().fnAdjustColumnSizing();},1);
				break;						
			} 
		}
	});	
	jQuery.fn.exists = function(){return this.length>0;}
	jQuery.isJson = function(str) {
	 if (jQuery.trim(str) == '') return false;
	 str = str.replace(/\\./g, '@').replace(/"[^"\\\n\r]*"/g, '');
	 return (/^[,:{}\[\]0-9.\-+Eaeflnr-u \n\r\t]*$/).test(str);
	}
	
	Number.prototype.padLeft = function(base,chr){
	    var  len = (String(base || 10).length - String(this).length)+1;
	    return len > 0? new Array(len).join(chr || '0')+this : this;
	}

	CurrentDate = [(CurrentDate.getMonth() + 1).padLeft(),CurrentDate.getDate().padLeft(),CurrentDate.getFullYear()].join('/');
	$('#startDate').val(CurrentDate);
	$('#endDate').val(CurrentDate);

	function NASort(a, b) {    
	    if (a.innerHTML == 'NA') {
	        return 1;   
	    }
	    else if (b.innerHTML == 'NA') {
	        return -1;   
	    }       
	    return (parseInt(a.innerHTML) > parseInt(b.innerHTML)) ? 1 : -1;
	};

	function ProcessRequest(data,gotoProcess){
		$.ajax({
		 type: 'POST',
		 url: 'modules/gstReportACMT/GSTReportClass.php',
		 data: data,
		 success: function(data){
				var CallMeBaby = eval('(' + gotoProcess + ')');
						CallMeBaby(data);
		 },
		 beforeSend: function(){
				$.dimScreen(function() {
					$('#content').fadeIn();
				});
		 },
		 complete: function(){
			 $.dimScreenStop();
		 }
		});
	}
	

	function msgbox(msg,focusField){
		$( "#dialog-msgbox:ui-dialog" ).dialog( "destroy" );
		$( "#dialog-msgbox" ).html(msg);
		$( "#dialog-msgbox" ).attr("title",'GST Report');
		$( "#dialog-msgbox" ).dialog('option', 'position', 'center');
		$( "#dialog-msgbox" ).dialog({
			modal: true,
			height:'auto',
			show:{ 
				effect:"blind", 
				speed:'slow' 
			},
			hide:'blind',
			buttons: {
				Ok: function() {
					$(this).dialog( "close" );
				}
			}
		});
	}
	
	function PrepareForm(obj){
		
		$.each(obj[0],function(a,b){
			//b[0] - wawiindex
			//b[1] - wawialias
			//CurrentAvaiLablePicklistPerCustomer[b[0]['PackListNumber']] = [];
			CustomerList[b[0]] = b[1];
		});
		TaxReferenceList['All'] = ['All'];
		$.each(obj[1],function(a,b){
			TaxReferenceList[a] = [];
			$.each(b,function(c,d){
				$.each(d,function(f,g){
					TaxReferenceList[a].push(g);
				});
			});
		});

		//$('#slcCustomer')
		//   .append($("<option></option>")
		//   .attr("value",'All')
		//   .text('All'));

		$.each(CustomerList,function(a,b){
	     $('#slcCustomer')
	         .append($("<option></option>")
	         .attr("value",a)
	         .text(b)); 
		});
		$('#slcCustomer').change();
	}
	
	function generateReport(objAll){
		
		var obj = new Object(); //Incoming Data
		var obj1= new Object(); //Outgoing Data
		var obj2= new Object(); //Inventory Data
		var obj3= new Object(); //Filing Data
		var obj4= new Object(); //Filing Value Data
		var tmpArray;
		var i = 1;
		
		
		tblIncoming.fnClearTable();
		tblIOutgoing.fnClearTable();
		tblIInventory.fnClearTable();
		tblFiling.fnClearTable();
		tblFilingValue.fnClearTable();
		
		
		obj = objAll[0];
		i = 1;
		if (obj != 'nodata'){
			$('#tabs > ul').tabs({ selected: 1 });
			tmpArray = [];
			$.each(obj,function(a,b){
				tmpArray.push([i,b['Date'],b['Invoice'],b['Quantity']]);
				//tmpArray.push([i,b['Month'],b['Invoice'],parseFloat(b['InvoiceValue']).toFixed(2),parseFloat(b['InvoiceValueSGD']).toFixed(2)]);
				i++;
			});
			tblIncoming.fnAddData(tmpArray);
		}
		
		
		obj1 = objAll[1];
		i = 1;
		if (obj1 != 'nodata'){
			tmpArray = [];
			$.each(obj1,function(c,d){
				tmpArray.push([i,d['Date'],d['Invoice'],d['DN'],d['Quantity']]);
				i++;
			});
			tblIOutgoing.fnAddData(tmpArray);
		}
		
		
		obj2 = objAll[2];
		i = 1;
		if (obj2 != 'nodata'){
			tmpArray = [];
			$.each(obj2,function(e,f){
				tmpArray.push([i,f['Invoice'],f['QtyBal']]);
				i++;
			});
			tblIInventory.fnAddData(tmpArray);
		}
		
		
		obj3 = objAll[3];
		i = 1;
		if (obj3 != 'nodata'){
			tmpArray = [];
			$.each(obj3,function(g,h){
				
				//var Route = 'Export';
				//if($.trim(h['DestinationCountry']) == 'SG'){
				//	Route = 'Local';
				//} else if($.trim(h['DestinationCountry']) == ''){
				//	Route = '';
				//}
				
				tmpArray.push([i,h['RelDate'],h['ShipmentNo'],h['DN'],h['Qty'],h['TaxScheme'],h['DestinationCountry']]);
				i++;
			});
			tblFiling.fnAddData(tmpArray);
		}
		
		
		obj4 = objAll[4];
		i = 1;
		if (obj4 != 'nodata'){
			tmpArray = [];
			$.each(obj4,function(j,k){
				tmpArray.push([i,k['ShipmentNo'],k['TaxScheme'],k['USDVal'],k['DecVal'],k['Route']]);
				i++;
			});
			tblFilingValue.fnAddData(tmpArray);
		}
		
	}
	
	$('#slcCustomer').change(function(){
		$('#slcReference').empty();
		if (TaxReferenceList[$('#slcCustomer').val()] != 'No TaxScheme' && TaxReferenceList[$('#slcCustomer').val()].length > 1){
	     $('#slcReference')
	         .append($("<option></option>")
	         .attr("value",'All')
	         .text('All'));
		}
		
		$.each(TaxReferenceList[$('#slcCustomer').val()],function(a,b){
	     $('#slcReference')
	         .append($("<option></option>")
	         .attr("value",b)
	         .text(b));
		});
	});
	
	$('#generateReport').click(function(){
		ProcessRequest({'todo':'generateReport','startDate':$('#startDate').val(),'endDate':$('#endDate').val(),'slcCustomer':$('#slcCustomer').val(),'slcReference':$('#slcReference').val()},'generateReport');
	});
	
	$('#exportToExcel').click(function(e){
		var tblIncomingCount = tblIncoming.fnGetData().length;
		var tblIOutgoingCount  = tblIOutgoing.fnGetData().length;
		var tblFilingCount	 = tblFiling.fnGetData().length;
		var tblFilingValueCount	 = tblFilingValue.fnGetData().length;
		
		if (tblIncomingCount == 0 && tblIOutgoingCount == 0 && tblFilingCount == 0 && tblFilingValueCount == 0){
			msgbox('No data to Export');
		} else {
			$.fileDownload('modules/gstReportACMT/ExportToExcel.php', { httpMethod : "POST", data: { StartDate : $('#startDate').val(), EndDate : $('#endDate').val(), TaxRef : $('#slcReference').val(), Customer : $('#slcCustomer').val(), tblIncoming : tblIncoming.fnGetData(),tblIOutgoing : tblIOutgoing.fnGetData(),tblFiling : tblFiling.fnGetData(),tblFilingValue : tblFilingValue.fnGetData() }});
		}
		
	});
	
	ProcessRequest({'todo':'PrepareForm'},'PrepareForm');
	
});	
</script>
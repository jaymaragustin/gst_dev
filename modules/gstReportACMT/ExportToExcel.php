<?php
//ExportToExcel.php

#error_reporting(E_ALL);
#ini_set('display_errors',1);
ini_set('memory_limit', '1024M');

require('../../../widgets/Excel/PHPExcel.php');
$phpExcel = new PHPExcel();

$styleArrayHeader = array(
	'font' => array(
		'bold' => true,
	)
);

$ActiveSheet = $phpExcel->setActiveSheetIndex(0);
$ActiveSheet->setTitle("Incoming Report");
printHeaderDetails($ActiveSheet,$styleArrayHeader,$_POST['StartDate'],$_POST['EndDate'],$_POST['TaxRef'],$_POST['Customer'],'INCOMING REPORT');
$tblIncomingHeader = array('SERIAL#','Date','Invoice #','Quantity');
PrintRow($ActiveSheet,$tblIncomingHeader,1,9,$styleArrayHeader,PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
PrintRow($ActiveSheet,$_POST['tblIncoming'],1,10,'',PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$phpExcel->createSheet();
$ActiveSheet = $phpExcel->setActiveSheetIndex(1);
$ActiveSheet->setTitle("Outgoing Report");
printHeaderDetails($ActiveSheet,$styleArrayHeader,$_POST['StartDate'],$_POST['EndDate'],$_POST['TaxRef'],$_POST['Customer'],'OUTGOING REPORT');
$tblIOutgoingHeader = array('SERIAL#','Date','Incoming Invoice #','DN #','Quantity');
PrintRow($ActiveSheet,$tblIOutgoingHeader,1,9,$styleArrayHeader,PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
PrintRow($ActiveSheet,$_POST['tblIOutgoing'],1,10,'',PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$phpExcel->createSheet();
$ActiveSheet = $phpExcel->setActiveSheetIndex(2);
$ActiveSheet->setTitle("Shipping Report");
printHeaderDetails($ActiveSheet,$styleArrayHeader,$_POST['StartDate'],$_POST['EndDate'],$_POST['TaxRef'],$_POST['Customer'],'SHIPPING REPORT');
$tblFilingHeader = array('SERIAL#','Cargo Release Date','Shipment #','DN #','Quantity','TaxScheme','Job Type');
PrintRow($ActiveSheet,$tblFilingHeader,1,9,$styleArrayHeader,PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
PrintRow($ActiveSheet,$_POST['tblFiling'],1,10,'',PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$phpExcel->createSheet();
$ActiveSheet = $phpExcel->setActiveSheetIndex(3);
$ActiveSheet->setTitle("Summary Report");
printHeaderDetails($ActiveSheet,$styleArrayHeader,$_POST['StartDate'],$_POST['EndDate'],$_POST['TaxRef'],$_POST['Customer'],'SUMMARY REPORT');
$tblFilingValueHeader = array('SERIAL#','Shipment #','TaxScheme','CI Value (USD)','Declared Value (SGD)','Job Type');
PrintRow($ActiveSheet,$tblFilingValueHeader,1,9,$styleArrayHeader,PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
PrintRow($ActiveSheet,$_POST['tblFilingValue'],1,10,'',PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

function printHeaderDetails($ActiveSheet,$Style,$StartDate,$EndDate,$TaxRef,$Customer,$TabTitle){
	
	$ActiveSheet->mergeCells("A1:D1");
	$ActiveSheet->setCellValue("A1", $TabTitle);
	$ActiveSheet->getStyle("A1")->applyFromArray($Style); //Title
	
	$ActiveSheet->setCellValue("C3", "Printed: ".date("m/d/Y"));
	$ActiveSheet->setCellValue("C4", "Time: ".date("h:i:s a")); 
	
	$ActiveSheet->setCellValue("B3", "Start Date: $StartDate");
	$ActiveSheet->setCellValue("B4", "End Date: $EndDate");
	
	$ActiveSheet->setCellValue("B6", "Customer: ADI SG");
	$ActiveSheet->setCellValue("B7", "In Tax Ref: ACMT");
}

function msort($array, $key, $sort_flags = SORT_REGULAR) {
    if (is_array($array) && count($array) > 0) {
        if (!empty($key)) {
            $mapping = array();
            foreach ($array as $k => $v) {
                $sort_key = '';
                if (!is_array($key)) {
                    $sort_key = $v[$key];
                } else {
                    // @TODO This should be fixed, now it will be sorted as string
                    foreach ($key as $key_key) {
                        $sort_key .= $v[$key_key];
                    }
                    $sort_flags = SORT_STRING;
                }
                $mapping[$k] = $sort_key;
            }
            asort($mapping, $sort_flags);
            $sorted = array();
            foreach ($mapping as $k => $v) {
                $sorted[] = $array[$k];
            }
            return $sorted;
        }
    }
    return $array;
}

function PrintRow($ActiveSheet,$StringArray,$IntAsciiChar,$StartRow,$Style,$Alignment){
	$InitialKeyAscii = $IntAsciiChar;
	$InitialRow			 = $StartRow;
	
	$Serialno				 = 1;
	
	$AsciiChar = $IntAsciiChar;
	foreach($StringArray as $keyStr => $keyValue){
		if (is_array($keyValue)){
			$AsciiChar = $IntAsciiChar;
			foreach($keyValue as $KeyValue1){
				
				if (trim($KeyValue1) != ''){
					if (isValidDateTime($KeyValue1)){
						$KeyValue1 = date("d/m/Y",strtotime($KeyValue1));
					}
					
					if ($AsciiChar == 1){
						$ActiveSheet->setCellValue(getNameFromNumber($AsciiChar).$StartRow,$Serialno);
						$Serialno++;
					} else {
						$ActiveSheet->setCellValue(getNameFromNumber($AsciiChar).$StartRow,$KeyValue1);
						//$Serialno
					}
					
					
					if(is_numeric($KeyValue1) and !is_float($KeyValue1) and $KeyValue1 > 99999){
						$ActiveSheet->getStyle(getNameFromNumber($AsciiChar).$StartRow)->getNumberFormat()->setFormatCode('#0');
					}
				}
				$AsciiChar++;
			}
			$StartRow++;
		} else {
			$ActiveSheet->setCellValue(getNameFromNumber($AsciiChar).$StartRow, $keyValue);
			$AsciiChar++;
		}
	}
	if (isset($Style) and strlen($Style) > 0){
		$ActiveSheet->getStyle(getNameFromNumber($InitialKeyAscii).$InitialRow.':'.getNameFromNumber($AsciiChar).$StartRow)->applyFromArray($Style);
	}
	
	$nCols = COUNT($StringArray);
	foreach (range(0, $nCols) as $col) {
		$ActiveSheet->getColumnDimensionByColumn($col)->setAutoSize(true);
	}
	
	if (isset($Alignment)){
		$ActiveSheet->getStyle(getNameFromNumber($InitialKeyAscii).$InitialRow.':'.getNameFromNumber($AsciiChar).$StartRow)->getAlignment()->setHorizontal($Alignment);
	}
}

function getNameFromNumber($num) {
    $numeric = ($num - 1) % 26;
    $letter = chr(65 + $numeric);
    $num2 = intval(($num - 1) / 26);
    if ($num2 > 0) {
        return getNameFromNumber($num2) . $letter;
    } else {
        return $letter;
    }
}

function isValidDateTime($dateTime){
    if (preg_match("/^(\d{4})-(\d{2})-(\d{2}) ([01][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/", $dateTime, $matches)) {
        if (checkdate($matches[2], $matches[3], $matches[1])) {
            return true;
        }
    }

    return false;
}

$phpExcel->setActiveSheetIndex(0);

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="GSTReport.xls"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel5');
$objWriter->save('php://output');


exit;
?>
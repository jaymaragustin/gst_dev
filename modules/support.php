<?php 
session_start();
require ('../include/referer.checker.php');
require ('../include/session.checker.php'); 
?>
<h2>Support</h2> 
<form action="#" enctype="multipart/form-data" method="post" id="contactForm" name="contactForm" class="globalform">
    <fieldset style="width:96%">
		<legend>Send an email</legend>
		<ol>
			<li>
				<label for="subject">Subject</label>
				<input id="subject" name="subject" class="text" style="width:300px" />
				<span class="req">*</span>
			</li>
			<li>
				<label for="category">Category</label>
				<select id="category" class="select" style="width:305px" name="category">
					<option value="General Inquiry">General Inquiry</option>
					<option value="Application Error/Issue">Application Error/Issue</option>
					<option value="Network/Internet Connection">Network/Internet Connection</option>
					<option value="Request">Request</option>
				</select>
				<span class="req">*</span>
			</li>
			<li>
				<label for="customer">Wawi Customer</label>
				<select id="customer" class="select" style="width:305px" name="customer">
				<?php
					echo '<option value="All Customer">All Customer</option>';
					if(is_array($_SESSION['WawiList']) && count($_SESSION['WawiList']) > 0){
						foreach($_SESSION['WawiList'] as $value){
							echo '<option value="'.$value['WawiAlias'].'">'.$value['WawiAlias'].'</option>';
						}
					}
				?>
				</select>
			</li>
			<li>
				<label for="message">Your Message</label>
				<textarea id="message" style="width:80%;height:200px;font-family: 'Open Sans',Arial,Tahoma;font-size: 13px;" name="message"></textarea>
				<span class="req">*</span>
			</li>
			<li id="fileattachment">
				<label for="attachment">File Attachment(s)</label>
				<input type="file" name="attachment[]" id="attachment" />
			</li>
			<li class="buttons">
				<label>&nbsp;</label>
				<input type="button" id="submit-btn"  value="Send message" style="margin-bottom: 20px;" />
			</li>
		</ol>
	</fieldset>
	<span style="font-weight: bold; float: left; font-size: 15px; margin: 10px 0px 0px;">OR</span>
	<fieldset style="width:96%">
		<legend>Call us at</legend>
		<br><br>
		<p>Development Team @ 6545-4041 <em style="font-size: 10px;">ext</em> <strong>179</strong></p> 
		<p>Support Team @ 6545-4041 <em style="font-size: 10px;">ext</em> <strong>176</strong></p> 
		<br><br>
	</fieldset> 
	<div class="clr"></div>
</form>
<br><br>
<div class="clr"></div>
<script>
$(function() {
	$( "input:submit, input:reset, input:button, button").button();
	//$( "input:file").prettyFileInput();
	
	function msgbox(msg,title,elem){
		$( "#dialog-msgbox:ui-dialog" ).dialog( "destroy" );
		$( "#dialog-msgbox" ).html(msg);
		$( "#dialog-msgbox" ).attr("title",title);
		$( "#dialog-msgbox" ).dialog({
			modal: true,
			height:'auto',
			show:{ 
				effect:"blind", 
				speed:'slow' 
			},
			hide:'blind',
			buttons: {
				Ok: function() {
					$( this ).dialog( "close" );
					if (elem){
						$('#'+elem).focus().select();
					}
				}
			}
		});
	}
	
	function ProcessRequest(data,gotoProcess){
		 $.ajax({
			 type: 'POST',
			 url: 'process.php',
			 data: data,
			 success: function(data){
				 
				 switch(gotoProcess){
				 
					 case 'ContactSupportCallback':
						ContactSupportCallback(data);
					 break; 
					 
				 }
			 },
			 beforeSend: function(){
				$.dimScreen(1000, 0.5, function() {
					$('#content').fadeIn();
				});
			   },
			 complete: function(){
				 $.dimScreenStop(); 
			   }
		 });
	}
	
	
	
	
	$('#attachment').MultiFile({
		max: 15,
		accept: 'gif|jpg|jpeg|png|bmp|swf|txt|doc|docx|xls|xlsx|csv|pdf'
	});	
	
	
	
	$('#submit-btn').click(function(){
			var c_rules =	[
					"required,subject,Kindly state the subject.",
					"required,category,Kindly select the category.",
					"required,message,Message should not be empty."
					];
			rsv.customErrorHandler = formError;	
			rsv.onCompleteHandler = formSubmit;
			rsv.validate(document.forms['contactForm'],c_rules);
		
	});
	
	function formSubmit(theForm){
		$.fn.MultiFile.disableEmpty();
		var options = {
            iframe:        true,  
            success:       ContactSupportCallback,
            url:           'process.php',
			data: { user: '<?php echo $_SESSION['iWMS-User'] ?>',type: 'ContactSupport' },
			beforeSubmit: function(arr, $form, options) {
				$.dimScreen(1000, 0.5, function() {
					$('#content').fadeIn();
				});
			}
        };
        $(theForm).ajaxSubmit(options);
	}
	
	function formError(f, errorInfo){		
		var fieldName;
		if(errorInfo.length > 0){
			if (errorInfo[0][0].type == undefined)
				fieldName = errorInfo[0][0][0].name;
			else
				fieldName = errorInfo[0][0].name;
				
			msgbox('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0px 4px 4px 0px;"></span>'+errorInfo[0][1]+'</p>','iWMS Alert',fieldName);
		}
	}
	
	function ContactSupportCallback(data){
		$.dimScreenStop();
		console.log(data); 
		$.fn.MultiFile.reEnableEmpty(); 
		if(data == 'success'){
			msgbox('<p><span class="ui-icon ui-icon-info" style="float:left; margin:0px 4px 4px 0px;"></span>Message Sent. Support team will response to you shortly.</p>','iWMS Confirmation','subject');
			$('#contactForm').clearForm();
			$('a.MultiFile-remove').click();
		}else{
			msgbox('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0px 4px 4px 0px;"></span>Error: Message sending failed.</p>','iWMS Alert','subject');
			$('.buttons #submit-btn').val('Resend');
			$('.buttons').find('#reset').remove();
			$('.buttons').append('<input type="reset" id="reset" value="Clear">');
			$("input:reset").click(function(){ $('a.MultiFile-remove').click(); }).button();
		}
	}
	 
	$.fn.clearForm = function() {
      return this.each(function() {
        var type = this.type, tag = this.tagName.toLowerCase();
        if (tag == 'form')
          return $(':input',this).clearForm();
        if (type == 'text' || type == 'password' || tag == 'textarea')
          this.value = '';
        else if (type == 'checkbox' || type == 'radio')
          this.checked = false;
        else if (tag == 'select')
          this.selectedIndex = 0;
      });
    };
	
	
	
	
});
</script>

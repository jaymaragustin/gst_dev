<?php 
session_start();
require '../../include/referer.checker.php';
require '../../include/session.checker.php';
?>
<h2><?php include '../../tpl/module_shortcut.php';?>Putaway</h2>
<?php if (isset($_SESSION['defaultWawiID']) && $_SESSION['defaultWawiID'] && isset($_SESSION['defaultWawiName']) && $_SESSION['defaultWawiName']){?>
<form action="#" method="post" id="globalform">
<ol>
    
    <li class="putawayfields" style="display:none">
    <label>Location:</label> 
        <input type="text" value="" class="text" id="location" />
        <span class="ui-icon ui-icon-locked" title="toggle to unlock/lock location field" id="location_lock" style="float:left; margin:3px 4px 0px 0px; cursor:pointer"></span><span id="scan-remarks">
    </li>
    <li class="putawayfields" style="display:none">
    <label>Entry Number:</label> 
        <input type="text" value="" class="text int" id="entrynumber" />
        <span id="status"></span>
    </li>
	<br><br><br>
    <li class="putawayfields" style="display:none;margin-top:10px;">
		<label>&nbsp;</label>
	   <input type="button" id="submit-btn" value="Add To List" />
       <!-- <input type="button" id="clear-btn" value="Clear" /> -->
        <div class="clr"></div>
    </li>
</ol>
</form>

<div class="clr"></div>
<br>
<div id="putaway-right">
    <table cellpadding="0" cellspacing="0" border="0" class="display" id="scantable">
        <thead>
            <tr><th width="80">#</th><th width="20">&nbsp;</th><th>Location</th><th>Entry Number</th><th>Remarks</th></tr>
        </thead>
        <tbody></tbody>
        <tfoot>
            <tr><th>#</th><th>&nbsp;</th><th>Location</th><th>Entry Number</th><th>Remarks</th></tr>
        </tfoot>
    </table>
</div>
        
<div class="clr"></div>


<script>
var fn_deleteRow;

$(function() {
	$( "input:submit, input:button, button").button();
	
	var scantable;
	scantable = $('#scantable').dataTable( {
		"sDom": '<"H">lTfr<"F"tip>',
		"oTableTools": {
			"aButtons": [
				"copy",
				{
					"sExtends":    "text",
						"sButtonText": "Confirm Putaway",
						"fnClick": function ( nButton, oConfig, oFlash ) {						
							
							var data = scantable.fnGetData();
							var datastring = '';
							$.each(data,function(i,e){
								datastring += "&putawaydata[]="+e[2]+'|'+e[3];
							});
							
							
							var len = datastring.length;
							if (len){
								
									ProcessRequest('WawiID=<?php echo $_SESSION['defaultWawiID'];?>'+datastring+'&type=Putaway','Putaway');
								
							}else{
								msgbox('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0px 4px 4px 0px;"></span>No data found.</p>','iWMS Alert',0);
							}
						}
				},{
					"sExtends":    "text",
						"sButtonText": "Clear",
						"fnClick": function ( nButton, oConfig, oFlash ) {							
							ctr=1;
							rowdata=[];
							
							scantable.fnClearTable();
							$('#entrynumber').val('');
							
							if ($('#location_lock').hasClass('ui-icon-unlocked')){
								$('#location').val('').select().focus();
							}else{
								$("#entrynumber").select().focus();
							}		
						}
				}
			]
		},
		'bFilter': true,
		'bPaginate': true,
		'bJQueryUI': true,
		'aLengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
		'sPaginationType': 'full_numbers',
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"aaSorting": [ [0,'desc'] ],
		
		"fnFooterCallback": function ( nRow, aaData, iStart, iEnd, aiDisplay ) {
			var iTotal = 0;
			for ( var i=iStart ; i<iEnd ; i++ )
			{
				iTotal += 1;
			}
			
			var nCells = nRow.getElementsByTagName('th');
			nCells[0].innerHTML = 'Total: ' + iTotal;

		}
		
	} );

	$('#scantable').css("white-space", "nowrap");
	
	var ctr=1;
	var rowdata = new Array;
	
	function fnClickAddRow(location,entrynumber,remarks) {
		
		var trash = '<span class="ui-icon ui-icon-trash" onclick="fn_deleteRow(this);" title="click to remove from list" style="float:left; margin:0px 4px 4px 0px; cursor:pointer;"></span>';
		
		switch(remarks){
			case 'success':
				remarks = '<span class="success">'+remarks+'</span>';
			break;
			
			case 'failed':
			case 'entry not found':
				remarks = '<span class="error">'+remarks+'</span>';
			break;
		}
		
		if($.inArray(entrynumber,rowdata) >= 0){
			scantable.fnUpdate( [$.inArray(entrynumber,rowdata)+1,trash, location,entrynumber,remarks ], $.inArray(entrynumber,rowdata), 0 );
		}else{
			rowdata.push(entrynumber);
			scantable.fnAddData( [ctr++,trash, location,entrynumber,remarks ]);
		}
	}
	
	fn_deleteRow = function(obj){
		var row = scantable.fnGetPosition($(obj).closest('tr').get(0));
		scantable.fnDeleteRow(row);
		rowdata.splice(row,1);
		if (rowdata.length==0){
			ctr = 1;
		}
	}
	
	
	function msgbox(msg,title,elem){
		$( "#dialog-msgbox:ui-dialog" ).dialog( "destroy" );
		$( "#dialog-msgbox" ).html(msg);
		$("#dialog-msgbox").dialog('option', 'position', 'center');
		$( "#dialog-msgbox" ).attr("title",title);
		$( "#dialog-msgbox" ).dialog({
			modal: true,
			height:'auto',
			show:{ 
				effect:"blind", 
				speed:'fast' 
			},
			hide:'blind',
			buttons: {
				Ok: function() {
					$( this ).dialog( "close" );
					if (elem){
						$('#'+elem).focus().select();
					}
				}
			}
		});
	}		
	
	function ProcessRequest(data,gotoProcess){
		 $.ajax({
			 type: 'POST',
			 url: 'process.php',
			 data: data,
			 success: function(data){
				 
				 switch(gotoProcess){
	 
					 case 'GetWawiCustomer':
						GetWawiCustomer(data);
					 break; 
					 
					 case 'Putaway':
						Putaway(data);
					 break;
				 }
			 },
			 beforeSend: function(){
				$.dimScreen(100, 0.3, function() {
					$('#content').fadeIn();
				});
			   },
			 complete: function(){
					$.dimScreenStop();
			   }
		 });
	}
	
	
	/*-----------------------------------------------START PUTAWAY--------------------------------------------------*/
	$(function() {
		$(".int").keydown(function(event) {
			// Allow only backspace, delete, enter, tab
			if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 13 || event.keyCode == 9 ) {
				//
			}else {
				// Ensure that it is a number and stop the keypress
				if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
					event.preventDefault(); 
				}   
			}
		});		
		
	});
	
	function is_int(value){ 
	  if((parseFloat(value) == parseInt(value)) && !isNaN(value)){
		  return true;
	  } else { 
		  return false;
	  }
	}

	
	$("#location").keypress(function(event) {
	  if ( event.which == 13 ) {
		  var Location = $.trim($(this).val());
		  if (!Location || Location==''){
			  msgbox('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0px 4px 4px 0px;"></span>Please enter valid Location</p>','iWMS Alert','location');
		  }else{
			 $("#entrynumber").focus().select();
		  }
	  }
	});
	
	$("#entrynumber").keypress(function(event) {
	  if ( event.which == 13 ) {
		  var EntryNumber = $.trim($(this).val());
		  if (EntryNumber && is_int(EntryNumber)){
			$("#submit-btn").click();
			if ($('#location_lock').hasClass('ui-icon-unlocked')){
				$("#location").focus().select();
			}else{
				$("#entrynumber").focus().select();
			}
		  }else{
			  msgbox('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0px 4px 4px 0px;"></span>Please enter valid Entry Number</p>','iWMS Alert','entrynumber');
		  }
	  }
	});
	
	$('.putawayfields').slideDown('slow');
	
	$('#submit-btn').click(function(){
		var Location = $.trim($('#location').val());
		var EntryNumber = $.trim($('#entrynumber').val());
		var Remarks = '';

		if (!Location || Location==''){
			msgbox('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0px 4px 4px 0px;"></span>Please enter valid Location</p>','iWMS Alert','location');
			return false;
		}
		
		if (!EntryNumber || EntryNumber=='' || !is_int(EntryNumber)){
			msgbox('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0px 4px 4px 0px;"></span>Please enter valid Entry Number</p>','iWMS Alert','entrynumber');
			return false;
		}

		fnClickAddRow(Location, EntryNumber,Remarks);
		
		
	});
	
	// $('#clear-btn').click(function(){
		// ctr=1;
		// rowdata=[];
		
		// scantable.fnClearTable();
		// $('#entrynumber').val('');
		
		// if ($('#location_lock').hasClass('ui-icon-unlocked')){
			// $('#location').val('').select().focus();
		// }else{
			// $("#entrynumber").select().focus();
		// }
		
		
	// });
	
	
	
	
	function Putaway(data){
		
		//return;
		if (data=='no db'){			
			msgbox('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0px 4px 4px 0px;"></span>Customer not available. Please try again later.</p>','iWMS Alert',0);
		}else if (data=='no data request'){
			msgbox('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0px 4px 4px 0px;"></span>No data is being passed. Please scan data.</p>','iWMS Alert',0);
		}else if (data){
			
			var obj = jQuery.parseJSON(data);
			if (obj.length){
				
				for(var i=0; i<obj.length; i++){
					fnClickAddRow(obj[i][0], obj[i][1], obj[i][2]);
				}
			}			
			

		}else{
			msgbox('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0px 4px 4px 0px;"></span>Unexpected error occured. Please try again later.</p>','iWMS Alert','entrynumber');
		}
	}
	
	$('#location').select().focus();
	
	$('#location_lock').click(function(){
		if ($(this).hasClass('ui-icon-unlocked')){
			$(this).removeClass('ui-icon-unlocked');
			$(this).addClass('ui-icon-locked');
		}else{
			$(this).removeClass('ui-icon-locked');
			$(this).addClass('ui-icon-unlocked');
		}
	});
	
	/*-----------------------------------------------END PUTAWAY--------------------------------------------------*/

});
</script>
<?php 
}else{
	?>
	<p><span class="ui-icon ui-icon-alert" style="float:left; margin:3px 4px 4px 0px;"></span>Please select Wawi Customer</td>
	<?php
}?>
<?php 
session_start();
require ('../../include/referer.checker.php');
require ('../../include/session.checker.php');
?>
<style type="text/css">
	.globalform input.text {
    width: 190px;
	}
	
	.globalform{
		 font-size:12px;
	}
</style>
<h2><?php include '../../tpl/module_shortcut.php';?>Incoming</h2> 

<?php if (isset($_SESSION['defaultProgramID']) && $_SESSION['defaultProgramID'] && isset($_SESSION['defaultProgramName']) && $_SESSION['defaultProgramName']){
$wawiid = $_SESSION['defaultProgramID'];

?>

<table id="tblDetails" width="756" border="0" cellpadding="0" cellspacing="0" class="globalform" style="border-spacing:2px; margin: 15px auto;">
  <tr>
    <td><strong>Customer:</strong></td>
    <td colspan="4"><select class="select" name="clsCustomerName" id="clsCustomerName"></select></td>
  </tr>
  <tr>
    <td width="14%"><strong>Incoming Ref#.:</strong></td>
    <td width="28%"><input class="text" type="text" name="txtrefnum" id="txtrefnum" tabindex="1" /></td>
    <td width="17%">&nbsp;</td>
    <td width="19%"><strong>Tax Class:</strong></td>
    <td width="22%"><input class="text" type="text" name="txttaxclass" id="txttaxclass" tabindex="5" /></td>
  </tr>
  <tr>
    <td><strong>DC Job No:</strong></td>
    <td><input class="text" type="text" name="txtdcjob" id="txtdcjob" tabindex="2" /></td>
    <td>&nbsp;</td>
    <td><strong>Total Outer Boxes:</strong></td>
    <td><input class="text" type="text" name="txttotalouter" id="txttotalouter" tabindex="6" /></td>
  </tr>
  <tr>
    <td><strong>PO No:</strong></td>
    <td><input class="text" type="text" name="txtpcno" id="txtpcno" tabindex="3"  /></td>
    <td>&nbsp;</td>
    <td><strong>Total Gross Weight:</strong></td>
    <td><input class="text" type="text" name="txttotalgross" id="txttotalgross" tabindex="7" /></td>
  </tr>
  <tr>
    <td><strong>Total Billing:</strong></td>
    <td><input class="text" type="text" name="txttotalbilling" id="txttotalbilling" tabindex="4" /></td>
    <td>&nbsp;</td>
    <td><strong>Remarks:</strong></td>
    <td><input class="text" type="text" name="txtremarks" id="txtremarks" tabindex="8" /></td>
  </tr>
  <tr>
    <td colspan="5" align="right">
    	<input type="button" name="btncancel" id="btncancel" value="Cancel"  />&nbsp;&nbsp;&nbsp;
    	<input type="button" name="btnDelete" id="btnDelete" value="Delete" />&nbsp;&nbsp;&nbsp;
    	<input type="button" name="btnsubmit" id="btnsubmit" value="Add New >>" />
    </td>
  </tr>
</table>
<div id="myTabs">
  <ul>
      <li><a href="#tab1" onclick="javascript:void(0);">Unattended</a></li>
      <li><a href="#tab2" onclick="javascript:void(0);">Attended</a></li>
  </ul>
  <div id="tab1">
	<form class="globalform">
		<label>Select Date : </label>
		<input type="text" class="text date-pick" name="datepacked1" id="datepacked1" value="<?php echo date('m/d/Y');?>" />
	</form>
  	<table class="display" id="tblunattended"></table> 
  </div>
  <div id="tab2">
	<form class="globalform">
		<label>Select Date : </label>
		<input type="text" class="text date-pick" name="datepacked" id="datepacked" value="<?php echo date('m/d/Y');?>" />
	</form>
  	<table class="display" id="tblattended"></table>
  </div>
</div>
<script>
$(function() {
	$( "input:submit, input:button, button, .button").button();
	$("#btncancel").hide();
	$("#btnDelete").hide();
	$("#myTabs").tabs({
		select: function(event, ui) {
			 $('.DTTT_collection').remove();
			var selected = ui.panel.id;
			switch(selected){ 
				case 'tab1':
					ProcessRequest({'todo':'GetListOfUnattendedRef','WawiIndex':$('#clsCustomerName').val(),'clsCustomerName':$('#clsCustomerName').val(),'datepacked':$('#datepacked1').val()},'GetListOfUnattendedRef'); 
				break;				
				case 'tab2':
					ProcessRequest({'todo':'GetListOfAttendedRef','WawiIndex':$('#clsCustomerName').val(),'clsCustomerName':$('#clsCustomerName').val(),'datepacked':$('#datepacked').val()},'GetListOfAttendedRef'); 
				break;						
			}
		}
	});
	$(".date-pick").datepicker({dateFormat:"mm/dd/yy"});
	
	function msgbox(msg,elem){ 
		$( "#dialog-msgbox:ui-dialog" ).dialog( "destroy" );
		$( "#dialog-msgbox" ).html(msg);
		$( "#dialog-msgbox" ).attr("title",'GST Incoming Module');
		$( "#dialog-msgbox" ).dialog('option', 'position', 'center');
		$( "#dialog-msgbox" ).dialog({
			modal: true,
			height:'auto',
			show:{ 
				effect:"blind", 
				speed:'slow' 
			},
			hide:'blind',
			buttons: {
				Ok: function() {
					$(this).dialog( "close" );
					if (elem){
						$('#'+elem).focus().select();
					}
				}
			}
		});
	}
	
	function msgboxYesNo(msg,gotoProcess){
		$( "#dialog-msgbox:ui-dialog" ).dialog( "destroy" );
		$( "#dialog-msgbox" ).html(msg);
		$( "#dialog-msgbox" ).attr("title",'Billing Incoming Module');
		$( "#dialog-msgbox" ).dialog('option', 'position', 'center');
		$( "#dialog-msgbox" ).dialog({
			modal: true,
			height:'auto',
			show:{ 
				effect:"blind", 
				speed:'slow' 
			},
			hide:'blind',
			buttons: {
				Yes: function() {
					$(this).dialog( "close" );
					if (gotoProcess == 'DeleteDatanow'){
						DeleteDatanow();
					}
				},
				No: function() {
					$(this).dialog( "close" );
				}
			}
		});
	}
	
	function ProcessRequest(data,gotoProcess){
			var urlString = 'modules/billingincoming/bIncoming.php';

			if (data['todo'] == 'AddUpdateData' || data['todo'] == 'DeleteDatanow'){
				urlString = 'modules/billingincoming/bIncoming.php?' + $("#tblDetails").find('input,select').serialize()
			}

		 $.extend(data,{UserID: '<?php echo $_SESSION[LoginUserVar]; ?>'});

		 $.ajax({
			 type: 'POST',
			 url: urlString,
			 data: data,
			 success: function(data){
			 	var CallMeBaby = eval('(' + gotoProcess + ')');
						CallMeBaby(data);
			 },
			 beforeSend: function(){
				$.dimScreen(10, 0.2, function() {
					$('#content').fadeIn();
				});
			 },
			 complete: function(){
				 $.dimScreenStop();
			 }
		 });
	}
	
	function GetAllCustomer(data){
		var obj = jQuery.parseJSON(data);
		
		$.each(obj, function(key, value) {
		     $('#clsCustomerName')
		         .append($("<option></option>")
		         .attr("value",value['WawiIndex'])
		         .text(value['WawiAlias']));
		});
		
		//ProcessRequest({'todo':'GetIncomingList','WawiIndex':$('#clsCustomerName').val()},'GetIncomingList');
		ProcessRequest({'todo':'GetListOfUnattendedRef','WawiIndex':$('#clsCustomerName').val(),'clsCustomerName':$('#clsCustomerName').val()},'GetListOfUnattendedRef');
		
	}
	 
	function GetIncomingList(data){
		if (data == "no data"){
			msgbox("No available GST data for <strong>" + $('#clsCustomerName option:selected').text() + "</strong>",'txtrefnum');
		} else {
			var obj = jQuery.parseJSON(data);
			console.log(obj);
		}
		$("#txtrefnum").focus();
	}
	
	function CheckCustRef(data){
		if (data == 'err1'){
			msgbox("Sorry cannot find the Ref #: " + $("#txtrefnum").val() + "<br />Please ask the warehouse to receive first.",'txtrefnum');
		} else {
			$("#txtdcjob").focus();

			if (data == 'no data'){
				console.log('add new');
				$("#txtrefnum").removeAttr('readonly');
			} else {
				var obj = jQuery.parseJSON(data);
				obj = obj[0];

				$("#txtdcjob").val(obj['JobNo']);
				$("#txtpcno").val(obj['PONo']);
				$("#txttotalbilling").val(obj['TotalBilling']);
				$("#txttaxclass").val(obj['TaxClass']);
				$("#txttotalouter").val(obj['TotalOuterBoxes']);
				$("#txttotalgross").val(obj['TotalGrossWeight']);
				$("#txtremarks").val(obj['Remarks']);
				
				$("#btnsubmit").val('Update');
				$("#btncancel").show();
				$("#btnDelete").show();
				$("#txtrefnum").attr('readonly','true');
			}
		}
	}
	
	function AddUpdateData(data){
		if (data == 'err1'){
			msgbox("Sorry cannot find the Ref #: " + $("#txtrefnum").val() + "<br />Please ask the warehouse to receive first.",'txtrefnum');
		} else if (data == 'addnewsuccess'){
			$("#btncancel").click();
			msgbox('New Incoming Reference has been added.','txtrefnum');
		} else if(data == 'addnewsuccess'){
			msgbox('Unable to add New Incoming Reference. <br />Please check your data and try again','txtrefnum');
		} else if(data == 'updatesuccess'){
			$("#btncancel").click();
			msgbox('Updating of Incoming Reference success','txtrefnum');
		} else if(data == 'updatefailed'){
			msgbox('Unable to Update Incoming Reference. <br />Please check your data and try again','txtrefnum');
		}
	}
	
	function DeleteDatanow(){
		ProcessRequest({'todo':'DeleteDatanow'},'DeleteDataResult');
	}
	
	function DeleteDataResult(data){
		$("#btncancel").click();
		if (data == 'deleted1'){
			msgbox("Sorry unable to delete Incoming Reference No. (err code 1)",'txtrefnum');
		} else if (data == 'deleted2'){
			msgbox("Sorry unable to delete Incoming Reference No. (err code 2)",'txtrefnum');
		} else if (data == 'successdeleted'){
			msgbox("Incoming Reference No. has been deleted",'txtrefnum');
		}
	}
	
	$('#clsCustomerName').change(function(){
		$("#btncancel").click();
		//ProcessRequest({'todo':'GetIncomingList','WawiIndex':$(this).val()},'GetIncomingList');
		ProcessRequest({'todo':'GetListOfUnattendedRef','WawiIndex':$('#clsCustomerName').val(),'clsCustomerName':$('#clsCustomerName').val(),'datepacked':$('#datepacked1').val()},'GetListOfUnattendedRef'); 
		ProcessRequest({'todo':'GetListOfAttendedRef','WawiIndex':$('#clsCustomerName').val(),'clsCustomerName':$('#clsCustomerName').val(),'datepacked':$('#datepacked').val()},'GetListOfAttendedRef'); 
	});
	
	$("#txtrefnum").keypress(function(event){
		if (event.keyCode == 13){
			if ($.trim($(this).val()) == ''){
				msgbox("All fields are required. Remarks field not included.","txtrefnum");
				$(this).focus();
			} else {
				ProcessRequest({'todo':'CheckCustRef','WawiIndex':$('#clsCustomerName').val(),'RefNumber':$("#txtrefnum").val()},'CheckCustRef');
			}
		} 
	});
	
	$("#btnsubmit").click(function(){
		if($.trim($("#txtrefnum").val()) == ''){
			msgbox("All fields are required. Remarks field not included.",'txtrefnum');
		} else if($.trim($("#txtdcjob").val()) == ''){
			msgbox("All fields are required. Remarks field not included.",'txtdcjob');
		} else if($.trim($("#txtpcno").val()) == ''){
			msgbox("All fields are required. Remarks field not included.",'txtpcno');
		} else if($.trim($("#txttotalbilling").val()) == ''){
			msgbox("All fields are required. Remarks field not included.",'txttotalbilling');
		} else if($.trim($("#txttaxclass").val()) == ''){
			msgbox("All fields are required. Remarks field not included.",'txttaxclass');
		} else if($.trim($("#txttotalouter").val()) == ''){
			msgbox("All fields are required. Remarks field not included.",'txttotalouter');
		} else if($.trim($("#txttotalouter").val()) < 1){
			msgbox("Total Outer Box should not be less than 1",'txttotalouter');
		} else if($.trim($("#txttotalgross").val()) < 1){
			msgbox("Total Gross Weight should not be less than 1",'txttotalgross');
		} else if($.trim($("#txttotalbilling").val()) < 0){
			msgbox("Total Billing should not be less than 0",'txttotalbilling');
		} else if($.trim($("#txttotalouter").val()) > 9999){
			msgbox("Please check Total Outer Box",'txttotalouter');
		} else {
			ProcessRequest({'todo':'AddUpdateData'},'AddUpdateData');
		}
	});
	
	$("#btncancel").click(function(){
		$("#txtrefnum").removeAttr('readonly');
		$("#btnsubmit").val('Add New >>');
		$(this).hide();
		$("#btnDelete").hide();
		$("#tblDetails").find("input[type=text]").val('');
		$("#txtrefnum").focus();
	});
	
	$("#txtdcjob").keypress(function(event){
		if(event.keyCode == 13){
			if($.trim($(this).val()) == ''){
				msgbox("All fields are required. Remarks field not included.",'txtdcjob');
			} else {
				$("#txtpcno").select().focus();
			}
		}
	});
	
	$("#txtpcno").keypress(function(event){
		if(event.keyCode == 13){
			if($.trim($(this).val()) == ''){
				msgbox("All fields are required. Remarks field not included.",'txtpcno');
			} else {
				$("#txttotalbilling").select().focus();
			}
		}
	});
	
	$("#txttotalbilling").keypress(function(event){
		var val = $(this).val();
		if(event.keyCode == 13){
			if($.trim(val) == ''){
				msgbox("All fields are required. Remarks field not included.",'txttotalbilling');
			} else {
				$("#txttaxclass").select().focus();
			}
		}
	});
	
	$("#txttaxclass").keypress(function(event){
		if(event.keyCode == 13){
			if($.trim($(this).val()) == ''){
				msgbox("All fields are required. Remarks field not included.",'txttaxclass');
			} else {
				$("#txttotalouter").select().focus();
			}
		}
	});
	
	$("#txttotalouter").keypress(function(event){
		if(event.keyCode == 13){
			if($.trim($(this).val()) == ''){
				msgbox("All fields are required. Remarks field not included.",'txttotalouter');
			} else {
				$("#txttotalgross").select().focus();
			}
		}
	});
	
	$("#txttotalgross").keypress(function(event){
		var val = $(this).val();
		
		if(event.keyCode == 13){
			if($.trim(val) == ''){
				msgbox("All fields are required. Remarks field not included.",'txttotalgross');
			} else {
				$("#txtremarks").select().focus();
			}
		}
	});
	
	$("#txtremarks").keypress(function(event){
		if(event.keyCode == 13){
			$("#btnsubmit").click();
		}
	});
	
	$("#btnDelete").click(function(){
		msgboxYesNo("Are you sure you want to delete Incoming Ref: <strong>" + $("#txtrefnum").val() + "</strong>?",'DeleteDatanow');
	});
	
	ProcessRequest({'todo':'GetAllCustomer'},'GetAllCustomer');

	$("#txttotalouter").IntegerMask();
	$("#txttotalgross").DecimalMask('99999.99');
	$("#txttotalbilling").DecimalMask('9999999.99');
	
	/* GET UNATTENDED DATA */	
	function GetListOfUnattendedRef(data){
		if(data != 'no data'){
			var dataobj = $.parseJSON(data);
			var data = [];
			$.each(dataobj, function (i, e) { 
				var arr = $.map(e,function(value, key){ return value;});
				arr.push('<a href="javascript:void(0)" class="add-value" ref="'+e.ForwarderRef+'">Add</a>');   
				data.push(arr); 
			}); 
		}else{ 
			var data = [];
		} 
		 	
		var tblunattended = $('#tblunattended').dataTable( {		
				"sDom": '<"H">Tlfr<"F"tip>',
				"oTableTools": {
					"aButtons": [
						"copy",
						{
							"sExtends":    "collection",
							"sButtonText": "Save As",
							"aButtons":    [ "csv", "xls", "pdf" ]
						}
					] 
				},
				'bFilter': true,
				'bPaginate': true,
				'bJQueryUI': true,
				'aLengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
				'sPaginationType': 'full_numbers',
				"sScrollX": "100%", 
				"bScrollCollapse": true,      
				"bDestroy": true,      
				"aaData" : data,
				"aoColumns": [
					{ "sTitle": "Receive Date" },
					{ "sTitle": "Incoming Reference" },
					{ "sTitle": "Action" }
				]
			} ); 
		$('.add-value').die().live('click',function(){
			$("#txtrefnum").val($(this).attr('ref'));
			ProcessRequest({'todo':'CheckCustRef','WawiIndex':$('#clsCustomerName').val(),'RefNumber':$(this).attr('ref')},'CheckCustRef');
		});
	}
	
	/* GET ATTENDED DATA */	
	function GetListOfAttendedRef(data){
		if(data != 'no data'){
			var dataobj = $.parseJSON(data);
			var data = [];
			$.each(dataobj, function (i, e) { 
				var arr = $.map(e,function(value, key){ return value;});
				arr.push('<a href="javascript:void(0)" class="update-value" ref="'+e.ForwarderRef+'">Update</a>');   
				data.push(arr); 
			}); 
		}else{ 
			var data = [];
		} 
		 	
		var tblattended = $('#tblattended').dataTable( {		
				"sDom": '<"H">Tlfr<"F"tip>',
				"oTableTools": {
					"aButtons": [
						"copy",
						{
							"sExtends":    "collection",
							"sButtonText": "Save As",
							"aButtons":    [ "csv", "xls", "pdf" ]
						}
					] 
				},
				'bFilter': true,
				'bPaginate': true,
				'bJQueryUI': true,
				'aLengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
				'sPaginationType': 'full_numbers',
				"sScrollX": "100%", 
				"bScrollCollapse": true,      
				"bDestroy": true,      
				"aaData" : data,
				"aoColumns": [
					{ "sTitle": "Receive Date" },
					{ "sTitle": "Incoming Reference" },
					{ "sTitle": "Action" }
				]
			} ); 
		$('.update-value').die().live('click',function(){
			$("#txtrefnum").val($(this).attr('ref'));
			ProcessRequest({'todo':'CheckCustRef','WawiIndex':$('#clsCustomerName').val(),'RefNumber':$(this).attr('ref')},'CheckCustRef');
		});
	}
	
	$('#datepacked1').change(function(){
		ProcessRequest({'todo':'GetListOfUnattendedRef','WawiIndex':$('#clsCustomerName').val(),'clsCustomerName':$('#clsCustomerName').val(),'datepacked':$('#datepacked1').val()},'GetListOfUnattendedRef'); 
	});
	$('#datepacked').change(function(){
		ProcessRequest({'todo':'GetListOfAttendedRef','WawiIndex':$('#clsCustomerName').val(),'clsCustomerName':$('#clsCustomerName').val(),'datepacked':$('#datepacked').val()},'GetListOfAttendedRef'); 
	}); 
	
});
</script>
<?php 
}else{
	?>
	<p><span class="ui-icon ui-icon-alert" style="float:left; margin:3px 4px 4px 0px;"></span>Please select Wawi Customer</td>
	<?php
}?>

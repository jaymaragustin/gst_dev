<?php
if (isset($_POST['todo'])){
	require ('../../include/config.php');
	$BillingIcoming = new BillingIcoming($_REQUEST);
	
	if ($_POST['todo'] == 'GetAllCustomer'){
		echo $BillingIcoming->GetAllCustomer();
	} elseif ($_POST['todo'] == 'GetIncomingList'){
		echo $BillingIcoming->GetIncomingList();
	} elseif ($_POST['todo'] == 'CheckCustRef'){
		echo $BillingIcoming->CheckCustRef();
	} elseif ($_POST['todo'] == 'AddUpdateData'){
		echo $BillingIcoming->AddUpdateData();
	} elseif ($_POST['todo'] == 'DeleteDatanow'){
		echo $BillingIcoming->DeleteDatanow();
	} elseif ($_POST['todo'] == 'GetListOfUnattendedRef'){
		echo $BillingIcoming->GetListOfUnattendedRef(); 
	}elseif ($_POST['todo'] == 'GetListOfAttendedRef'){
		echo $BillingIcoming->GetListOfAttendedRef(); 
	}
}

class BillingIcoming{
	var $PostVars;
	
	function __construct($vars){
		$this->PostVars = $vars;
		$serverConn = unserialize(base64_decode(SQL_CONN));
		$this->conn = mssql_connect($serverConn[0],$serverConn[1],$serverConn[2]);
	}
	
	function GetAllCustomer(){
		mssql_select_db("MasterData");
		$sql = mssql_query("select WawiIndex,WawiAlias from WawiCustomer where [status] = 1");
		while($row[]=mssql_fetch_assoc($sql));
		array_pop($row);

		return json_encode($row);
	}
	
	function GetWawiID(){
		mssql_select_db("MasterData");
		$sql = mssql_fetch_assoc(mssql_query("select ID from WawiCustomer where [WawiIndex] = '".$this->PostVars['WawiIndex']."'"));
		
		return $sql['ID'];
	}

	function GetIncomingList(){
		mssql_select_db("Import");
		$sql = mssql_query("select * from Import_Billing where WawiIndex = '".$this->PostVars['WawiIndex']."'");
		
		if (mssql_num_rows($sql) < 1){
			return "no data";
		} else {
			while($row[]=mssql_fetch_assoc($sql));
			array_pop($row);
	
			return json_encode($row);
		}
	}

	function CheckCustRef(){
		mssql_select_db("MasterData");
		
		$dbname = mssql_fetch_assoc(mssql_query("select DBName from wawicustomer where WawiIndex = '".$this->PostVars['WawiIndex']."'"));
		mssql_select_db($dbname['DBName']);
		
		$sql = mssql_query("select * from [Entry] where forwarderref = '".$this->PostVars['RefNumber']."'");
		
		if (mssql_num_rows($sql) < 1){
			return "err1";
		} else {
			mssql_select_db("Import");
			$sql = mssql_query("select * from Import_Billing where WawiIndex = '".$this->PostVars['WawiIndex']."' and IncomingReference = '".$this->PostVars['RefNumber']."'");
			
			if (mssql_num_rows($sql) < 1){
				return "no data";
			} else {
				while($row[]=mssql_fetch_assoc($sql));
				array_pop($row);
		
				return json_encode($row);
			}
			
		}
		
	}
	
	function GetListOfUnattendedRef(){		
		mssql_select_db("MasterData");		
		$dbname = mssql_fetch_assoc(mssql_query("select DBName from wawicustomer where WawiIndex = '".$this->PostVars['WawiIndex']."'"));
		mssql_select_db("Import");
		$res = mssql_query("exec billingIncomingList '".$this->PostVars['datepacked']."','".$this->PostVars['clsCustomerName']."','$dbname[DBName]','UNATTENDED'");
		if(mssql_num_rows($res) > 0){
			while($row[] = mssql_fetch_assoc($res));
				array_pop($row);		
			return json_encode($row);
		}else{
			return 'no data';
		}
	}
	
	function GetListOfAttendedRef(){		
		mssql_select_db("MasterData");		
		$dbname = mssql_fetch_assoc(mssql_query("select DBName from wawicustomer where WawiIndex = '".$this->PostVars['WawiIndex']."'"));
		mssql_select_db("Import");
		
		$res = mssql_query("exec billingIncomingList '".$this->PostVars['datepacked']."','".$this->PostVars['clsCustomerName']."','$dbname[DBName]','ATTENDED'");
		if(mssql_num_rows($res) > 0){
			while($row[] = mssql_fetch_assoc($res));
				array_pop($row);		
			return json_encode($row); 
		}else{
			return 'no data'; 
		}
	}
	
	
	
	function AddUpdateData(){
		$this->PostVars['WawiIndex'] = $this->PostVars['clsCustomerName'];
		$this->PostVars['RefNumber'] = $this->PostVars['txtrefnum'];
		
		foreach ($this->PostVars as $key => $value){
			if(!is_numeric($this->PostVars[$key])){
				$this->PostVars[$key] = $this->mssqlQuotedString($value);
			}
		}
		
		$wawiID = $this->GetWawiID();
		
		$checkCustRef = $this->CheckCustRef();
		
		if ($checkCustRef == 'err1'){
			return 'err1';
		} else {
			if ($checkCustRef == 'no data'){
				// add new
				mssql_select_db("Import");
				$this->TransactionLog($this->PostVars['WawiIndex'],$wawiID,$this->PostVars['txtrefnum'],$this->PostVars['UserID'],'ADD','N/A','BILLING IMPORT');
				
				if (mssql_query("Insert into Import_Billing (IncomingReference,WawiIndex,WawiID,JobNo,TaxClass,PONo,TotalOuterBoxes,TotalGrossWeight,TotalBilling,Remarks,Username,UserUpdated,DateUpdated) values ('".$this->PostVars['txtrefnum']."','".$this->PostVars['WawiIndex']."',".$wawiID.",'".$this->PostVars['txtdcjob']."','".$this->PostVars['txttaxclass']."','".$this->PostVars['txtpcno']."','".$this->PostVars['txttotalouter']."','".$this->PostVars['txttotalgross']."','".$this->PostVars['txttotalbilling']."','".$this->PostVars['txtremarks']."','".$this->PostVars['UserID']."','".$this->PostVars['UserID']."',getdate())")){
					return 'addnewsuccess';
				} else {
					return 'addnewfailed';
				}
			} else {
				// update
				mssql_select_db("Import");
				$this->TransactionLog($this->PostVars['WawiIndex'],$wawiID,$this->PostVars['txtrefnum'],$this->PostVars['UserID'],'UPDATE','N/A','BILLING');
				
				if (mssql_query("Update Import_Billing set JobNo='".$this->PostVars['txtdcjob']."',TaxClass='".$this->PostVars['txttaxclass']."',PONo='".$this->PostVars['txtpcno']."',TotalOuterBoxes='".$this->PostVars['txttotalouter']."',TotalGrossWeight='".$this->PostVars['txttotalgross']."',TotalBilling='".$this->PostVars['txttotalbilling']."',Remarks='".$this->PostVars['txtremarks']."',UserUpdated='".$this->PostVars['UserID']."',DateUpdated=getdate() where IncomingReference = '".$this->PostVars['txtrefnum']."' and WawiIndex='".$this->PostVars['WawiIndex']."'")){
					return 'updatesuccess';
				} else {
					return 'updatefailed';
				}
			}
		}
	}
	
	function DeleteDatanow(){
		$this->PostVars['WawiIndex'] = $this->PostVars['clsCustomerName'];
		$this->PostVars['RefNumber'] = $this->PostVars['txtrefnum'];
		
		foreach ($this->PostVars as $key => $value){
			if(!is_numeric($this->PostVars[$key])){
				$this->PostVars[$key] = $this->mssqlQuotedString($value);
			}
		}
		
		$wawiID = $this->GetWawiID();
		
		mssql_select_db("Import");
		mssql_query("BEGIN TRAN");
		if ($this->TransactionLog($this->PostVars['WawiIndex'],$wawiID,$this->PostVars['txtrefnum'],$this->PostVars['UserID'],'DELETE','N/A','BILLING IMPORT')){
			if (mssql_query("delete from Import_Billing where IncomingReference = '".$this->PostVars['txtrefnum']."' and WawiIndex = '".$this->PostVars['WawiIndex']."'")){
				mssql_query("COMMIT");
				return 'successdeleted';
			} else {
				mssql_query("ROLLBACK");
				return 'deleted2';
			}
		} else {
			mssql_query("ROLLBACK");
			return 'deleted1';
		}
	}
	
	function mssqlQuotedString($str){
		$str = str_replace("\\", "\\\\", $str);
		$str = str_replace("'", "''", $str);
		
		return $str;
	}
	
	function TransactionLog($WawiIndex,$WawiID,$Reference,$UserID,$TransactionType,$Details,$Module){
		$Details = $this->mssqlQuotedString($Details);
		if (mssql_query("Insert into TransactionLogs (WawiIndex,WawiID,Reference,UserID,TrasactionDate,TransactionType,Details,Module) values ('$WawiIndex',$WawiID,'$Reference','$UserID',GETDATE(),'$TransactionType','$Details','$Module')")){
			return true;
		} else {
			return false;
		}
	}
	
}

?>
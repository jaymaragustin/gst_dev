<?php 
//error_reporting(E_ALL);
//ini_set("display_errors",1);
session_start();
require ('../../include/referer.checker.php');
require ('../../include/session.checker.php');
?>
<h2><?php include '../../tpl/module_shortcut.php';?>Subsidiary Export Certificate</h2> 
<div class="list">
	<table class="display" id="tblSubCertList">
		<thead><tr></tr></thead>
		<tbody></tbody>
		<tfoot><tr></tr></tfoot>
	</table> 
</div>
<div id="dialog-form" style="display:none">
	<form class="globalform" id="newSubCertForm" name="newSubCertForm" style="border: none">
		<em style="font-size: 13px; color:#888"><span class="req">*</span> required field</em>
		<br class="clear" />
		<ol>
			<div class="lfpane">
				<li class="even">
					<label>Customer <select id="CustomerList" name="CustomerList" class="select opt-list"><option value="">Select Here</option></select><span class="req">*</span></label>		
					<br class="clear" />
					<input type="text" id="CustomerName" name="CustomerName" class="required text" placeholder="Name" />
					<br class="clear" />
					<textarea id="CustomerAddress" name="CustomerAddress" rows="4" class="required textarea" placeholder="Address"></textarea>
				</li>
				<li class="even">
					<label>Order Number<span class="req">*</span></label>	
					<br class="clear" />
					<input type="text" id="OrderNumber" name="OrderNumber" class="required text" />							
				</li>
				<li class="even">
					<label>Master Airway Bills<span class="req">*</span></label>		
					<br class="clear" />
					<input type="text" id="MAWB" name="MAWB" class="required text" />							
				</li>
				<li class="even">
					<label>Flight Number<span class="req">*</span></label>		
					<br class="clear" />
					<input type="text" id="FlightNumber" name="FlightNumber" class="required text" />							
				</li>
				<li class="even">
					<label>Departure Date<span class="req">*</span></label>		
					<br class="clear" />
					<input type="text" id="ETD" name="ETD" class="required text" />							
				</li>
				<li class="even">
					<label>Export Permit Number<span class="req">*</span></label>		
					<br class="clear" />
					<input type="text" id="PermitNumber" name="PermitNumber" class="required text" />							
				</li>
				<li class="even">
					<label>Export Permit Date<span class="req">*</span></label>		
					<br class="clear" />
					<input type="text" id="PermitDate" name="PermitDate" class="required text" />							
				</li>
			</div>
			<div class="lfpane">
				<li class="even">
					<label>Exporter<select id="ExportList" name="ExportList" class="select opt-list"><option value="">Select Here</option></select><span class="req">*</span></label>						
					<br class="clear" />
					<input type="text" id="ExportName" name="ExportName" class="required text" placeholder="Name" />
					<br class="clear" />
					<textarea id="ExportAddress" name="ExportAddress" rows="4" class="required textarea" placeholder="Address"></textarea>
				</li>
				<li class="even">
					<label>Mode<select id="ExportModeList" name="List" class="select opt-list"><option value="">Select Here</option></select><span class="req">*</span></label>
					<br class="clear" />
					<input type="text" id="ExportMode" name="ExportMode" class="required text" />							
				</li>
				<li class="even">
					<label>Description<select id="GoodDescriptionList" name="GoodDescriptionList" class="select opt-list"><option value="">Select Here</option></select><span class="req">*</span></label>		
					<br class="clear" />
					<input type="text" id="GoodDescription" name="GoodDescription" class="required text" />	
				</li>
				<li class="even">
					<label>Total Quantity<span class="req">*</span>	</label>		
					<br class="clear" />
					<input type="text" id="Quantity" name="Quantity" class="required text" />						
				</li>
				<li class="even">
					<label>Total Value</label>		
					<br class="clear" />
					<input type="text" id="GoodValue" name="GoodValue" class="text" />
				</li>
				<li class="even">
					<label>No. of Cartons<span class="req">*</span></label>		
					<br class="clear" />
					<input type="text" id="Cartons" name="Cartons" class="required text" />							
				</li>
				<li class="even">
					<label>No. of Pallets<span class="req">*</span></label>		
					<br class="clear" />
					<input type="text" id="Pallets" name="Pallets" class="required text" />							
				</li>
			</div>			
		</ol>
		<br class="clear">
	</form>
</div>
<style>
<!--
	.dataTables_length{ width: 130px }
	.dataTables_scrollBody{ min-height: 280px; background: #DDDDDD; }
	.globalform label{ width: 300px;}
	.globalform li{ float: left; margin-bottom: 10px;}
	.text{  background-color: #fffbe5 !important; width: 300px !important; }
	.textarea{  background-color: #fffbe5 !important; width: 300px !important; }
	.opt-list{ background-color: #ffffff !important; float: right !important; width: 220px; }
	
-->
</style>
<script type="text/javascript">
var urlWorker = 'modules/gstSubCert/SubCertClass.php';
var tblSubCertList;
var contentDialog;
var WMSID = '<?php echo $_SESSION['defaultWawiID'];?>';
var UserID = '<?php echo $_SESSION['iWMS-User'];?>';
$(function() {
$.getScript('js/jslibrary.js',function(){
	
	msgbox = function(_title,_content,_focus,_confirm,_modal,_width,_height){
		_title = _title ? _title : 'Alert Message';
		_focus = _focus ? _focus : false;
		_width = _width ? _width : 350;
		_height = _height ? _height : 250;
		_modal = _modal ? _modal : true;
		_is_confirm = count(_confirm) > 0 ? true : false;
		if(_is_confirm){
			var _buttons = { 'Yes' : function() {
									$(this).dialog("close");
									var theFunction = eval('(' + _confirm[0] + ')');
										theFunction(_confirm[1]);
								},
							  'No': function() {
									$(this).dialog("close");
								}
							}
		}else{
			var _buttons = { 'Close': function() {
									$(this).dialog("close");									
								}
							}
		}
		
		
		$("#dialog-msgbox").empty().html(_content).dialog({
            modal: _modal,
            title: _title,
			show: {effect:"blind",duration:300},
            hide: "scale",
            buttons:_buttons,
            width:_width,
            height:_height,
			close:function(){
				$(this).dialog("destroy");
				if(_focus) $('#'+_focus).focus().select();
			}
        });
			
	}
	
	ProcessRequest = function(postURL,postData,postProcess){
		postData.WMSID = WMSID;
		postData.UserID = UserID;
		$.ajax({
			type: 'POST',
			url: postURL,
			data: postData,
			success: function(data){
				if (typeof postProcess == 'function') {
					postProcess(data);
				}else{
					var theFunction = eval('(' + postProcess + ')');
					theFunction(data);
				}				
			},
			beforeSend: function(){
				if(!postData.dashboard)
					$.dimScreen(300, 0.3, function(){});
			},
			complete: function(){
				if($.active >= 1)
					$.dimScreenStop();
			},
			error: function(request){
				$.dimScreenStop();
				if(request.status == 500 && request.statusText == 'Internal Server Error'){					
						msgbox('Error Message','<p class="error">Could not load page.<br>Please contact support.</p>');				
				}
			}
		 });
	}
	
	$("input:submit, input:button, button, .button").button();
	$('body > div#dialog-form').remove();
	
	var tblFields = {
		'[Index]': 'Index',												
		'Name1': 'Customer Name',												
		'Name2': 'Address Line 1',						
		'Street': 'Address Line 2',						
		'City':'City',
		'County':'County',
		'State':'State',
		'Country': 'Country',
		'Zip':'Postal',
		'[Index] AS IDS': 'Action'
	};
	
	$.each(tblFields,function(i,e){
		$('#tblSubCertList thead tr, #tblSubCertList tfoot tr').append('<th>'+e+'</th>');
	});
	
	// Table Initialization
	tblSubCertList = $('#tblSubCertList').dataTable( {
		sDom : '<"H"lTfr>t<"F"ip>',
		oTableTools: {
            aButtons: [
				{
					"sExtends": "copy",
					"bFooter" : false,
                    "mColumns": [ 0, 1, 2, 3, 4, 5, 6, 7, 8 ]
				},
				{
					"sExtends": "text",
					"sButtonText": "Reload",
					"fnClick" : function () {
						tblSubCertList.fnReloadAjaxUpdated();	
					}
				},
				{
					"sExtends": "text",
					"sButtonText": "New Sub Cert",
					"fnClick" : function () {
						addNewCustomer();
					}
				}
			
			]
        },
		bJQueryUI: true,
		bAutoWidth: false,
		sScrollX: "100%",
		aLengthMenu: [10, 25, 50, 100, 200, 500, 1000 ],
		sPaginationType: 'full_numbers',
		bDestroy: true,      
		bProcessing: true,
		bServerSide: false,
		sAjaxSource: urlWorker + '?fn=GetCustomerList',
		fnServerData: function ( sSource, aoData, fnCallback ) {
			aoData.push( { "name": "Fields", "value": implode(",",array_keys(tblFields)) } );			
			aoData.push( { "name": "WMSID", "value": WMSID } );			
			aoData.push( { "name": "UserID", "value": UserID } );			
			$.ajax( {
				dataType: "json",
				type: "POST",
				url: sSource,
				data: aoData,
				success: fnCallback,
				error: function(request){
					if(request.status == 500 && request.statusText == 'Internal Server Error'){					
							msgbox('Error Message','<p class="error">Could not load page.<br>Please contact support.</p>');				
					}
				}
			});
		},
		aoColumns:[
			{ sWidth:50 },
			{ sWidth:200 },
			{ sWidth:300 },			
			{ sWidth:300 },			
			{ sWidth:100 },
			{ sWidth:100 },
			{ sWidth:100 },
			{ sWidth:100 },
			{ sWidth:100 },
			{ sWidth:70 }
		]
	});
	
	//$('#tblSubCertList,.display').css("white-space", "nowrap");
	
	validateEntries = function(){
		var _return = true;
		$.each($('#newSubCertForm .required').serializeArray(),function(i,e){
			if($.trim(e.value) == ''){
				_return =  false;
			}
		});
		return _return;
	}
	
	addNewCustomer = function(){
		$("#dialog-form").dialog({
            modal: true,
            title: 'New Customer Information</span>',
			show: {effect:"blind",duration:300},
            hide: "scale",
			width:1024,
            height:$(window).height() -5,
			open: function(event,ui){				
				$('#newSubCertForm :input').clearForm();
			},
			close: function( event, ui ){
				$(this).dialog('destroy');				
			},
			buttons: { 
				'Save': function() {
					var _main = $(this);
					if(validateEntries()){
							ProcessRequest(urlWorker + '?fn=SaveCustomer',{'FormData':$('#newSubCertForm').serializeArray()},function(data){
								if(data.success){
									msgbox('Success','<p class="success">'+data.message+'</p>');				
									_main.dialog("close");
									tblSubCertList.fnReloadAjaxUpdated();	
								}else{						
									msgbox('Error','<p class="error">'+data.message+'</p>');
								}					
							});
					}else{
						msgbox('Error','<p class="error">Please enter value on required field(s).</p>');
					}
												
				},
				'Cancel': function() {
					$(this).dialog("close");									
				}
			}
        });
	}
	
	todo = function(action,id){
		switch(action){
			case 'delete':
					var confirm  = ["deleteCustomer",id];
					msgbox('Confirm Deletion','<p class="warning">Are you sure you want to delete this customer?</p>',0,confirm);				
				break;
			case 'edit':
					ProcessRequest(urlWorker + '?fn=GetCustomerInfo',{'ID':id},'editCustomerCallback'); 
				break;
		}
	}
	
	editCustomerCallback = function(data){
		$("#dialog-form").dialog({
            modal: true,
            title: 'Update Customer Information</span>',
			show: {effect:"blind",duration:300},
            hide: "scale",
			width:1024,
            height:$(window).height() + 50,
			open: function(event,ui){				
				$('#newSubCertForm :input').clearForm();			
				$('#CIndex').val(data.CIndex);				
				$('#ShipTo').val(data.Name1);				
				$('#AddressLine1').val(data.Name2);				
				$('#AddressLine2').val(data.Street);				
				$('#ZipCode').val(data.Zip);
				$('#City').val(data.City);
				$('#Country').val(data.Country);
				$('#County').val(data.County);				
				$('#State').val(data.State);				
				
			},
			close: function( event, ui ){
				$(this).dialog('destroy');				
			},
			buttons: { 
				'Save Changes': function() {
					var _main = $(this);
					if(validateEntries()){
						ProcessRequest(urlWorker + '?fn=UpdateCustomer',{'FormData':$('#newSubCertForm').serializeArray()},function(data){
							if(data.success){
								msgbox('Success','<p class="success">'+data.message+'</p>');				
								_main.dialog("close");
								tblSubCertList.fnReloadAjaxUpdated();	
							}else{						
								msgbox('Error','<p class="error">'+data.message+'</p>');	
							}					
						});								
					}else{
						msgbox('Error','<p class="error">Please enter value on required field(s).</p>');
					}
				},
				'Cancel': function() {
					$(this).dialog("close");									
				}
			}
        });
	}
	
	deleteCustomer = function(id){		
		ProcessRequest(urlWorker + '?fn=DeleteCustomer',{'ID':id},function(data){
			if(data.success){
				msgbox('Successful','<span class="success">'+data.message+'</span>');	
				tblSubCertList.fnReloadAjaxUpdated();				
			}else{
				msgbox('Error','<span class="error">'+data.message+'</span>');							
			}
		});
	}
	
	ProcessRequest(urlWorker + '?fn=LoadSTGMMCountry',{},function(res){
		if(res.length > 0){
			$('#Country').html('<option value="">Select Here</option>');
			$.each(res,function(i,e){
				$('#Country').append('<option value="'+e.country_code+'">'+e.country_name+'</option>');				
			});
		}else{	
			$('#Country').html('<option value="">No Available Country</option>');
		}	
	});
	
});
});
</script>
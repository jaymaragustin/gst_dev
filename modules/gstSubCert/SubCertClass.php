<?php
error_reporting(1);
ini_set('display_errors',E_ALL);
if (isset($_REQUEST['WMSID'])){
	session_start();
	include('../../include/referer.checker.php');
	$SubCertClass = new SubCertClass($_REQUEST);
	$cleanedPostData = $SubCertClass->PostVars;
	$fn = isset($cleanedPostData['fn']) ? $cleanedPostData['fn']: false;	
	if($fn){
		$SubCertClass->$fn($cleanedPostData);	
	}
	
}

class SubCertClass{
	public $PostVars;
	public $conn;
	public $CurrentDBName;
	public $CurrentWMSIndex;
	public $CurrentWMSID;
	public $UserID;
	
	function __construct($vars){
		if (is_array($vars) && count($vars) > 1){	
			array_walk_recursive($vars,array($this,'cleanData'));
			$this->PostVars = $vars;
			$this->CurrentWMSID = $this->PostVars['WMSID'];
			if (isset($this->PostVars['UserID']) || (strlen($this->PostVars['UserID']) > 1)){
				$this->UserID = $this->PostVars['UserID'];
			} else {
				$this->UserID = "N/A";
			}
			
			$serverConn = unserialize(base64_decode(SQL_CONN));		
			$this->conn = mssql_connect($serverConn[0],$serverConn[1],$serverConn[2]);			
			if(!$this->conn){	
				return false;
			}			
			$this->CurrentDBName = $this->GetWawiDBName();
			$this->CurrentWMSIndex = $this->GetWawiIndex();			
					
			if(mssql_select_db($this->CurrentDBName)){
				$this->LogMetofile("[Database Connection : connected to ".$this->CurrentDBName."]");					
			}else{
				$this->LogMetofile("[Database Connection : unable to select database ".$this->CurrentDBName."]");			
				return false;
			}
		}		
	}
	
	function GetWawiDBName(){	
		$sql = mssql_fetch_assoc(mssql_query("SELECT DBName FROM MasterData.dbo.WawiCustomer WHERE ID = ".$this->CurrentWMSID));
		$DBname = $sql['DBName'];
		mssql_free_result($sql);
		return $DBname;
	}
	
	function GetWawiIndex(){	
		$sql = mssql_fetch_assoc(mssql_query("SELECT WawiIndex FROM MasterData.dbo.WawiCustomer WHERE ID = ".$this->CurrentWMSID));
		$WMSIndex = $sql['WawiIndex'];
		mssql_free_result($sql);
		return $WMSIndex;
	}
	
	function LogMetofile($stringData){		
		$LogFile = "logs/".$this->CurrentDBName."_".date("Y-m-d").".txt";
		$fh = fopen($LogFile, 'a+');
		$stringData = "[Date: ".date("Y-m-d H:i:s")."]\t[Userid:".$this->UserID."]\t[TransactionID:".mktime()."]\t$stringData\r\n";
		fwrite($fh, $stringData) or die('could not write');
		fclose($fh);
	}
	
	function cleanData(&$str) {
		 $str = trim(preg_replace('/\s+/',' ',htmlspecialchars_decode(strip_tags($str))));
	}
	
	function convertToDate($date) {
		$date = explode("/",$date);
		return $date[2]."-".$date[1]."-".$date[0];
	}
	
	function mssql_escape_string($string_to_escape){
		$replaced_string = str_replace("'","''",$string_to_escape);
		return $replaced_string;
	}
	
	/********************************************** MAIN FUNCTION ******************************************************/
	
	function getCustomerList($post){
		extract($post);		
		$DBName = $this->CurrentDBName;
		$data = array("sEcho"=>(isset($sEcho) ? $sEcho : 0),"iTotalRecords"=>0,"iTotalDisplayRecords"=>0,"aaData"=>array());
		$query = mssql_query("SELECT $Fields FROM $DBName.dbo.Customer");
		$numrows = mssql_num_rows($query);
		if($numrows > 0){
			$data['iTotalRecords'] = $numrows;
			$data['iTotalDisplayRecords'] = $numrows;
			while($row = mssql_fetch_assoc($query)){
				switch($this->CurrentWMSID){
					case 151 : //ImpinjMYWMS
					case 154 : //ImpinjPDX
					case 159 : //ImpinjPDXTest
					case 161 : //ImpinjMyTest
						$rawData = explode('|',$row['City']);
						$row['City'] 	= isset($rawData[0]) ? $rawData[0] : '';
						$row['County'] 	= isset($rawData[1]) ? $rawData[1] : '';
						$row['State'] 	= isset($rawData[2]) ? $rawData[2] : '';
						break; 
				}
				
				$ID = $row['IDS'];
				$row['IDS'] = '<a href="javascript:void(0)" class="edit-btn" onclick="todo(\'edit\',\''.$ID.'\');" title="Edit" >Edit</a>
								 <a href="javascript:void(0)" class="delete-btn" onclick="todo(\'delete\',\''.$ID.'\');" title="Delete" >Delete</a>';
				$data['aaData'][] = array_values($row);				
			}	
		}	
		
		header('Content-Type: application/json');
		echo json_encode($data);		
	}
	
	function getCustomerIndex(){
		$DBName = $this->CurrentDBName;
		$query = mssql_query("SELECT MAX(CONVERT(INT,[Index])) + 1 as CIndex FROM $DBName.dbo.[Customer] WHERE ISNUMERIC([Index]) = 1 ");
		$row = mssql_fetch_row($query);
		$CIndex = is_null($row['0']) ? 1 : $row['0'];		
		return $CIndex;
	}
	
	function checkCustomerExist($Name,$Address,$CIndex){
		$DBName = $this->CurrentDBName;
		$sWhere = "Name1 = '$Name' AND ( Name2 = '$Address' OR Street = '$Address' )";
		if($CIndex){
			$sWhere .= " AND [Index] <> $CIndex ";		
		}
		$query = mssql_query("SELECT [Index] FROM $DBName.dbo.[Customer] WHERE $sWhere");
		if(mssql_num_rows($query) > 0){
			return true;
		}else{
			return false;
		}
	}
	
	function saveCustomer($post){
		extract($post); 		
		$DBName = $this->CurrentDBName;
		$return = array('success'=>false,'message'=>NULL);
		$newCustomerIndex = $this->getCustomerIndex();
		if(isset($FormData) && is_array($FormData)){
			foreach($FormData as $data){
				$$data['name'] = $data['value'];
			}
		}
		
		$CustomerData = array(
			'Index'		=>	$newCustomerIndex,
			'Name1'		=>	$ShipTo,
			'Name2'		=>	$AddressLine1,
			'Street'	=>	$AddressLine2,
			'Zip'		=>	$ZipCode,
			'County'	=>	$County,
			'State'		=>	$State,
			'Country'	=>	$Country
		);
		
		switch($this->CurrentWMSID){
			case 151 : //ImpinjMYWMS
			case 154 : //ImpinjPDX
			case 159 : //ImpinjPDXTest
			case 161 : //ImpinjMyTest
				$CustomerData['City'] = $City."|".$County."|".$State;
				break; 
			default	 : $CustomerData['City'] = $City;
				break;
		}		
		
		if($this->checkCustomerExist($ShipTo,$AddressLine1,false)){
			$return['message'] = 'Customer is already exist.';
		}else{
			$query = mssql_query("SELECT [Index] FROM $DBName.dbo.[Customer] WHERE [Index] = '$newCustomerIndex'");
			if(mssql_num_rows($query) <= 0){
				$insert = mssql_query("INSERT INTO $DBName.dbo.[Customer]([".implode("],[",array_keys($CustomerData))."]) VALUES('".implode("','",array_map(array('SubCertClass','mssql_escape_string'),array_values($CustomerData)))."')");
				if($insert && mssql_rows_affected($this->conn)){
					$return['success'] = true;
					$return['message'] = 'New customer information has been added successfully.';					
				}else{
					$return['message'] = 'Unable to save new customer information. Please contact support.';					
				}
			}else{
				$return['message'] = 'Customer index is already exist.';
			}			
		}		
		
		header('Content-Type: application/json');
		echo json_encode($return);				
	}	
	
	function updateCustomer($post){
		extract($post); 		
		$DBName = $this->CurrentDBName;
		$return = array('success'=>false,'message'=>NULL);
		if(isset($FormData) && is_array($FormData)){
			foreach($FormData as $data){
				$$data['name'] = $data['value'];
			}
		}		
		$CustomerData = array(
			'Name1'		=>	$ShipTo,
			'Name2'		=>	$AddressLine1,
			'Street'	=>	$AddressLine2,
			'Zip'		=>	$ZipCode,
			'County'	=>	$County,
			'State'		=>	$State,
			'Country'	=>	$Country
		);
		
		switch($this->CurrentWMSID){
			case 151 : //ImpinjMYWMS
			case 154 : //ImpinjPDX
			case 159 : //ImpinjPDXTest
			case 161 : //ImpinjMyTest
				$CustomerData['City'] = $City."|".$County."|".$State;
				break; 
			default	 : $CustomerData['City'] = $City;
				break;
		}		
		
		if($this->checkCustomerExist($ShipTo,$AddressLine1,$CIndex)){
			$return['message'] = 'Customer Info is already exist.';
		}else{
			$query = mssql_query("SELECT [Index] FROM $DBName.dbo.[Customer] WHERE [Index] = '$CIndex'");
			if(mssql_num_rows($query) > 0){
				$CustomerDataSet = array();
				foreach($CustomerData as $setKey => $setVal){
					$CustomerDataSet[] = "$setKey = '".$this->mssql_escape_string($setVal)."'";
				}
				$update = mssql_query("UPDATE $DBName.dbo.[Customer] SET ".implode(",",$CustomerDataSet)." WHERE [Index] = '$CIndex'");
				if($update && mssql_rows_affected($this->conn)){
					$return['success'] = true;
					$return['message'] = 'Customer info has been updated successfully.';					
				}else{
					$return['message'] = 'Unable to update customer info. Please contact support.';					
				}
			}else{
				$return['message'] = 'Customer info does not exist.';
			}			
		}		
		
		header('Content-Type: application/json');
		echo json_encode($return);		
	}
	
	function deleteCustomer($post){		
		extract($post);
		$DBName = $this->CurrentDBName;
		$return = array('success'=>false,'message'=>NULL);
		$CIndex = $ID; 
		$query = mssql_query("SELECT [Index] FROM $DBName.dbo.[Customer] WHERE [Index] = '$CIndex'");
		if(mssql_num_rows($query) > 0){
			$delete = mssql_query("DELETE FROM $DBName.dbo.[Customer] WHERE [Index] = '$CIndex'");
			if($delete && mssql_rows_affected($this->conn)){
				$return['success'] = true;
				$return['message'] = 'Customer info has been deleted successfully.';					
			}else{
				$return['message'] = 'Unable to delete customer info. Please contact support.';					
			}
		}else{
			$return['message'] = 'Customer info does not exist.';
		}	
		
		header('Content-Type: application/json');
		echo json_encode($return);	
	}
	
	 function LoadSTGMMCountry(){    
        $res = mssql_query("SELECT country_code , country_name FROM STGMM.dbo.Country;");    
        $result = array();
		if(mssql_num_rows($res) > 0){
			while($row[] = mssql_fetch_assoc($res));
			array_pop($row);
            $result = $row;
        }  
		
        header('Content-Type: application/json');
		echo json_encode($result);			
    }
	
	function getCustomerInfo($post){
		extract($post);
		$DBName = $this->CurrentDBName;
		$query = mssql_query("SELECT [Index] AS CIndex,Name1,Name2,Street,Country,Zip,City,County,State FROM  $DBName.dbo.[Customer] WHERE [Index] = '$ID' ");
		$row = mssql_fetch_assoc($query);		
		switch($this->CurrentWMSID){
			case 151 : //ImpinjMYWMS
			case 154 : //ImpinjPDX
			case 159 : //ImpinjPDXTest
			case 161 : //ImpinjMyTest
				$rawData = explode('|',$row['City']);
				$row['City'] 	= isset($rawData[0]) ? $rawData[0] : '';
				$row['County'] 	= isset($rawData[1]) ? $rawData[1] : '';
				$row['State'] 	= isset($rawData[2]) ? $rawData[2] : '';
				break; 
		}
		
		
		header('Content-Type: application/json');
		echo json_encode($row);	
	}
	
	function __destruct() {
       if ($this->conn){
		   mssql_close($this->conn);
	   }
    }
}
?>
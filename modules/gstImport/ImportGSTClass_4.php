<?php
if (isset($_POST['todo'])){
	require ('../../include/config.php');
	$ImportGSTClass = new ImportGSTClass($_REQUEST);
	
	if ($_POST['todo'] == 'DataSearch'){
		echo $ImportGSTClass->DataSearch();
	} else if ($_POST['todo'] == 'CustomerList'){
		echo $ImportGSTClass->CustomerList();
	} else if ($_POST['todo'] == 'GetRefNumber'){
		echo $ImportGSTClass->GetRefNumber();
	} else if ($_POST['todo'] == 'ProcessData'){
		echo $ImportGSTClass->ProcessData();
	} else if ($_POST['todo'] == 'UpdateReference'){
		echo $ImportGSTClass->UpdateReference();
	} else if ($_POST['todo'] == 'UpdateReferenceNull'){
		echo $ImportGSTClass->UpdateReferenceNull();
	} else if ($_POST['todo'] == 'GetRefInvoiceData') {
		echo $ImportGSTClass->GetRefInvoiceData();
	} else if ($_POST['todo'] == 'GetInvData') {
		echo $ImportGSTClass->GetInvData();
	} else if ($_POST['todo'] == 'GetInvRec') {
		echo $ImportGSTClass->GetInvRec();
	}else if ($_POST['todo'] == 'GetEntryData') {
		echo $ImportGSTClass->GetEntryData();
	}
}

Class ImportGSTClass{
	var $PostVars;
	function __construct($vars){
		$this->PostVars = $vars;
		$serverConn = unserialize(base64_decode(SQL_CONN));
		$this->conn = mssql_connect($serverConn[0],$serverConn[1],$serverConn[2]);
		mssql_select_db("Import");
	}
	
	function CustomerList(){
		mssql_select_db('MasterData');
		$data = array();
		
		$sql = mssql_query("select WawiAlias,GSTIndex AS WawiIndex,DBname from wawicustomer where [status] = 1 order by WawiAlias ASC");
		while($CustomerList[] = mssql_fetch_assoc($sql)){}
		array_pop($CustomerList);
		
		$listCurrency = $this->ListCurrency();

		mssql_select_db("Import");
		
		$this->PostVars['WawiIndex'] = $CustomerList[0]['WawiIndex'];
		$RefList = $this->GetRefNumber();
		
		$sql = mssql_query("select top 2 ShipReference from v_ol_ShipImport");
		
		if(mssql_num_rows($sql) > 0){
			$chkrecords = 'data';
		} else {
			$chkrecords = 'nodata';
		}
		
		/*
		unset($sql);
		$sql = mssql_query("select Forwarder from Forwarder where [Status] = 1");
		while($ListofFrowarder[] = mssql_fetch_assoc($sql)){}
		array_pop($ListofFrowarder);
		*/
		unset($sql);
		$sql = mssql_query("select Supplier from Supplier where [Status] = 1 order by Supplier ASC");
		while($ListofSupplier[] = mssql_fetch_assoc($sql)){}
		array_pop($ListofSupplier);
		
		array_push($data,$CustomerList);
		array_push($data,$RefList);
		array_push($data,$listCurrency);
		array_push($data,$this->GSTRate());
		array_push($data,$chkrecords);
		//array_push($data,$ListofFrowarder);
		array_push($data,$ListofSupplier);
		
		return json_encode($data);
	}
	
	function ListCurrency(){
		$sql = mssql_query("SELECT DISTINCT Currency FROM Country ORDER BY Currency");
		if (mssql_num_rows($sql) > 0){
			while($CurrencyList[] = mssql_fetch_assoc($sql)){}
			array_pop($CurrencyList);
		} else {
			$CurrencyList = array(array('Currency' => 'SGD'),array('Currency' => 'USD'));
		}
		
		return $CurrencyList;
	}
	
	function GSTRate(){
		$sql = mssql_query("SELECT Value FROM Setup WHERE Entry='GST'");
		if (mssql_num_rows($sql) > 0){
			$sql = mssql_fetch_assoc($sql);
			$GSTRate = $sql['Value'];
		} else {
			$GSTRate = 7;
		}
		
		return $GSTRate;
	}
	
	function mssqlQuotedString($str){
		$str = str_replace("\\", "\\\\", $str);
		$str = str_replace("'", "''", $str);
		
		return $str;
	}
	
	function DataSearch(){
		if ($this->PostVars['SearchAction'] == 'First'){ // First run
			$sql = mssql_query("select convert(text,VendorInvNo) as VendorInvNo1,* from v_ol_ShipImport where ShipReference = (select top 1 ShipReference from v_ol_ShipImport order by ShipReference ASC)");
		} else if($this->PostVars['SearchAction'] == 'Next'){
			$sql = mssql_query("select convert(text,VendorInvNo) as VendorInvNo1,* from v_ol_ShipImport where ShipReference = (select top 1 ShipReference from v_ol_ShipImport where ShipReference > ".$this->PostVars['ShipReference']." order by ShipReference ASC)");
		} else if($this->PostVars['SearchAction'] == 'Previous'){
			$sql = mssql_query("select convert(text,VendorInvNo) as VendorInvNo1,* from v_ol_ShipImport where ShipReference = (select top 1 ShipReference from v_ol_ShipImport where ShipReference < ".$this->PostVars['ShipReference']." order by ShipReference DESC)");
		} else if($this->PostVars['SearchAction'] == 'Last'){
			$sql = mssql_query("select convert(text,VendorInvNo) as VendorInvNo1,* from v_ol_ShipImport where ShipReference = (select top 1 ShipReference from v_ol_ShipImport order by ShipReference DESC)");
		} else if($this->PostVars['SearchAction'] == 'Search'){
			$sql = mssql_query("select convert(text,VendorInvNo) as VendorInvNo1,* from v_ol_ShipImport where ShipReference = ".$this->PostVars['ShipReference']);
		}

		if (mssql_num_rows($sql) < 1){
			return $this->PostVars['SearchAction'];
		} else {
			$data = array();
			while($row = mssql_fetch_assoc($sql)){
				$row1 = mssql_fetch_assoc(mssql_query("select sum(Quantity) as QtyFromEntry from ".$row['EntryTable']." where ForwarderRef = '".$row['Reference']."'"));
				$row['QtyFromEntry'] = $row1['QtyFromEntry'];
				array_push($data,$row);
			}
			
			//array_push($data,$str);
			
			return json_encode($data);
		}
	}
	
	function GetRefNumber(){
		$DBname = $this->GetQuantityCustomer();
		mssql_select_db("Import");
		$WawiIndex = $this->PostVars['WawiIndex'];
		// $sql = "SELECT ie.ForwarderRef as 'Reference',sum(ie.Quantity) as 'TotalQty' 
		// 		FROM Import i, {$DBname}.dbo.Entry ie 
		// 		WHERE i.WawiIndex = '{$WawiIndex}' 
		// 			AND i.ShipReference is null 
		// 			AND convert(varchar,ltrim(rtrim(ie.ForwarderRef))) = convert(varchar,ltrim(rtrim(i.Reference))) 
		// 			AND (YEAR(ie.EntryDate) = Year(Getdate()) or YEAR(ie.EntryDate) = Year(Getdate())-1)
		// 		GROUP BY ie.ForwarderRef ORDER BY ie.ForwarderRef ASC";

		$sql = "SELECT ie.ForwarderRef as 'Reference',sum(ie.Quantity) as 'TotalQty' 
		FROM Import i, {$DBname}.dbo.Entry ie  
		WHERE i.WawiIndex = '{$WawiIndex}' 
			AND i.ShipReference is null 
			AND convert(varchar,ltrim(rtrim(ie.ForwarderRef))) = convert(varchar,ltrim(rtrim(i.Reference))) 
			AND (YEAR(ie.EntryDate) >= '2020' )
		GROUP BY ie.ForwarderRef ORDER BY ie.ForwarderRef ASC";

		$sql = mssql_query($sql);

		if (mssql_num_rows($sql) < 1){
			$RefList = 'NoRef';
			return $RefList;
		} else {
			while($RefList[] = mssql_fetch_assoc($sql)){}
			array_pop($RefList);
			if ($_POST['todo'] == 'GetRefNumber'){
				return json_encode($RefList);
			} else {
				return $RefList;
			}
		}
	}

	function GetInvData(){
		$DBname = $this->GetQuantityCustomer();
		mssql_select_db("Import");
		
		$sql = "SELECT ie.ForwarderRef as 'Reference',sum(ie.Quantity) as 'TotalQty' ,ie.Invoice
				FROM Import i WITH (NOLOCK), {$DBname}.dbo.Entry ie  WITH (NOLOCK)
				WHERE i.WawiIndex = '".$this->PostVars['WawiIndex']."' 
					AND ie.ForwarderRef = '".$this->PostVars['Reference']."' 
					AND i.ShipReference is null 
					AND ie.MasterBox is null
					AND convert(varchar,ltrim(rtrim(ie.ForwarderRef))) = convert(varchar,ltrim(rtrim(i.Reference))) 
					AND (YEAR(ie.EntryDate) = Year(Getdate()) or YEAR(ie.EntryDate) = Year(Getdate())-1)
				GROUP BY ie.ForwarderRef,ie.Invoice 
				ORDER BY ie.ForwarderRef ASC";

		$query = mssql_query($sql);

		if (mssql_num_rows($query) < 1){
			$RefList = 'NoRef';
			return $RefList;
		} else {
			while($RefList[] = mssql_fetch_assoc($query)){}
			array_pop($RefList);
			if ($_POST['todo'] == 'GetInvData'){
				return json_encode($RefList);
			} else {
				return $RefList;
			}
		}
	}

	function GetEntryData(){
		$DBname = $this->GetQuantityCustomer();
		mssql_select_db("Import");
		
		if($DBname == 'HorizonTechnologyWMS' || $DBName == 'ADIWMS'){
			$comment = 'ie.Remarks';
		}else{
			$comment = 'ie.Comment';
		}



		$sql = "SELECT (
					CASE 
						WHEN 'ADISGWMS' = '{$DBname}'
							THEN ie.forwarderRef
						WHEN 'ADIWMS' = '{$DBname}'
							THEN ie.forwarderRef
						ELSE ie.forwarderRef
					END
				) as incomingref,
				".$comment." as Comment,
				ie.SupplyingCountry,
				convert(varchar,ie.scanDate,23) as ReceiptDate,
				ie.SupplyingCountry
				FROM Import i WITH (NOLOCK), {$DBname}.dbo.Entry ie  WITH (NOLOCK)
				WHERE i.WawiIndex = '".$this->PostVars['WawiIndex']."' 
					AND ie.ForwarderRef = '".$this->PostVars['Reference']."' 
					AND i.ShipReference is null 
					AND convert(varchar,ltrim(rtrim(ie.ForwarderRef))) = convert(varchar,ltrim(rtrim(i.Reference))) 
					AND (YEAR(ie.EntryDate) = Year(Getdate()) or YEAR(ie.EntryDate) = Year(Getdate())-1)
				ORDER BY ie.ForwarderRef,ie.Comment,ie.SupplyingCountry,ie.Forwarder,ie.Flight ASC";
		$query = mssql_query($sql);

		if (mssql_num_rows($query) < 1){
			$RefList = 'NoRef';
			return $RefList;
		} else {
			while($RefList[] = mssql_fetch_assoc($query)){}
			array_pop($RefList);
			if ($_POST['todo'] == 'GetEntryData'){

				return json_encode($RefList);
			} else {

				return $RefList;
			}
		}
	}


	function GetInvRec(){
		$DBname = $this->GetQuantityCustomer();
		mssql_select_db("Import");
		
		 $sql = mssql_query("Select ie.ForwarderRef as 'Reference',sum(ie.Quantity) as 'TotalQty' ,ie.Invoice , ISNULL((SELECT InvoiceValue FROM [Import_Invoice] WHERE WawiIndex = '".$this->PostVars['WawiIndex']."' AND convert(varchar,ltrim(rtrim(Reference))) = convert(varchar,ltrim(rtrim(ie.ForwarderRef))) AND convert(varchar,ltrim(rtrim(Invoice))) collate SQL_Latin1_General_CP1_CI_AS = convert(varchar,ltrim(rtrim(ie.Invoice)))),0) as 'InvoiceValue'
							from Import i WITH (NOLOCK),".$DBname.".dbo.Entry ie WITH (NOLOCK)
							where i.WawiIndex = '".$this->PostVars['WawiIndex']."' and ie.ForwarderRef in ('".implode("','",$this->PostVars['Reference'])."') and 
							convert(varchar,ltrim(rtrim(ie.ForwarderRef))) = convert(varchar,ltrim(rtrim(i.Reference))) 
							and (YEAR(ie.EntryDate) = Year(Getdate()) or YEAR(ie.EntryDate) = Year(Getdate())-1)
							GROUP BY ie.ForwarderRef,ie.Invoice ORDER BY ie.ForwarderRef ASC");

		
		if (mssql_num_rows($sql) < 1){
			$RefList = 'NoRef';
			return $RefList;
		} else {
			while($RefList[] = mssql_fetch_assoc($sql)){}
			array_pop($RefList);
			if ($_POST['todo'] == 'GetInvRec'){
				return json_encode($RefList);
			} else {
				return $RefList;
			}
		}
	}

	function GetRefInvoiceData(){
		$DBname = $this->GetQuantityCustomer();
		mssql_select_db("Import");


		$hawb = ($DBname == 'JohnstechWMS' || $DBname == 'CavendishWMS' ? 'PRODUCTOF as HAWB':'HAWB');

		$comment = ($DBname == 'HorizonTechnologyWMS' || $DBname == 'ADISGWMS' ? 'Remarks as Comment':'Comment');

		$fields = array(
			'ForwarderRef,AirwayBill,TaxScheme,MAWB,Flight,Supplier,Forwarder,'.$comment,
			'ForwarderRef,'.$hawb.',TaxScheme,MAWB,Flight,Supplier,Forwarder,'.$comment,
			'ForwarderRef,TaxScheme',
		);

		foreach ($fields as $key => $value) {
			$sql = mssql_query("SELECT TOP 1 {$value}
					FROM {$DBname}.dbo.[Entry] 
					WHERE ForwarderRef = '".$this->PostVars['Reference']."' ");

			if (mssql_num_rows($sql) > 0){
				$arrData = array();

				if($key == 0) {
					$arrData['HAWB'] 	  = mssql_result($sql,0,"AirwayBill");
					$arrData['TaxScheme'] = mssql_result($sql,0,"TaxScheme");
					$arrData['MAWB'] 	  = mssql_result($sql,0,"MAWB");
					$arrData['Flight'] 	  = mssql_result($sql,0,"Flight");
					$arrData['Supplier']  = strtoupper(mssql_result($sql,0,"Supplier"));
					$arrData['Forwarder']  = strtoupper(mssql_result($sql,0,"Forwarder"));
					$arrData['Comment']  = mssql_result($sql,0,"Comment");
				}

				if($key == 1) {
					$arrData['HAWB'] = mssql_result($sql,0,"HAWB");
					$arrData['TaxScheme'] = mssql_result($sql,0,"TaxScheme");
					$arrData['MAWB'] 	  = mssql_result($sql,0,"MAWB");
					$arrData['Flight'] 	  = mssql_result($sql,0,"Flight");
					$arrData['Supplier']  = strtoupper(mssql_result($sql,0,"Supplier"));
					$arrData['Forwarder']  = strtoupper(mssql_result($sql,0,"Forwarder"));
					$arrData['Comment']  = mssql_result($sql,0,"Comment");
				}

				if($key == 2) {
					$arrData['HAWB'] = '';
					$arrData['TaxScheme'] = mssql_result($sql,0,"TaxScheme");
				}

				return json_encode($arrData);
			}
		}

		$RefList = 'no data';
		return $RefList;
	}
	
	function ProcessData(){
		if ($this->PostVars['todochk'] == 'Update'){
			return $this->UpdateGST();
		} else {
			return $this->AddNewGST();
		}
	}
	
	function UpdateGST(){
		$data = array($this->PostVars['txtShipReference']);

		$xplode = explode(',',$this->PostVars['txtIncomingReference']);

		if(is_array($xplode)){
			$implode = array_unique($xplode);

			$incoming = implode(',',$implode);
		}else{
			$incoming = $xplode[0];
		}


		if (mssql_query("update Shipment set [VendorInvNo]='".$this->PostVars['txtVendorInvNo']."',[InTaxRef]='".$this->PostVars['slctax']."',[PermitNo]='".$this->PostVars['txtPermitNo']."',[ExchangeRate]='".$this->PostVars['txtExchangeRate']."',[Currency]='".$this->PostVars['slcCurrency']."',[OriginPort]='".$this->PostVars['txtOriginPort']."',[Supplier]='".$this->mssqlQuotedString($this->PostVars['slcGSTSupplier'])."',[HAWB]='".$this->PostVars['txtHAWB']."',[MAWB]='".$this->PostVars['txtMAWB']."',[TptMode]='".$this->PostVars['txtTptMode']."',[ArrivalDate]='".$this->PostVars['txtArrivalDate']."',[INCOTERM]='".$this->PostVars['slcIncoTerm']."',[Forwarders]='".$this->PostVars['slcForwarders']."',[Comments]='".$this->mssqlQuotedString($this->PostVars['txtComments'])."',[JobType]='".$this->PostVars['slcJobType']."',[Customer]='".$this->PostVars['slcCustomerList1']."',[InvoiceValueSGD]=".$this->PostVars['txtInvoiceValueSGD'].",[FreightCharges]=".$this->PostVars['txtFreightCharges'].",[InsCharges]=".$this->PostVars['txtInsCharges'].",[CIFAmount]=".$this->PostVars['txtCIFAmount'].",[GSTAmount]=".$this->PostVars['txtGSTAmount'].",
			[IncomingReference] ='".$incoming."',[Description] = '".$this->mssqlQuotedString($this->PostVars['txtDescription'])."',[SupplyingCountry] = '".$this->PostVars['txtSupplyingCountry']."',[ReceiptDate] = '".$this->PostVars['txtReceiptDate']."' where ShipReference = ".$this->PostVars['txtShipReference'])){
			mssql_query("update [Import] set WawiIndex='".$this->PostVars['slcCustomerList1']."' where ShipReference = ".$this->PostVars['txtShipReference']);
			array_push($data,'successUPDATE');
		} else {
			array_push($data,'errorUpdate');
		}
		return json_encode($data);
	}
	
	function AddNewGST(){
		mssql_select_db("Import");
		mssql_query("BEGIN TRAN");
		$sqlNewShipRec = mssql_fetch_assoc(mssql_query("SELECT Value FROM Setup WHERE Entry='ShipCounter'"));
		$ShipReference = $sqlNewShipRec['Value'] + 1;
		mssql_query("Update Setup set Value = Value + 1 WHERE Entry='ShipCounter'");
		unset($sqlNewShipRec);
		
		$xplode = explode(',',$this->PostVars['txtIncomingReference']);

		
		if(is_array($xplode)){
			$implode = array_unique($xplode);

			$incoming = implode(',',$implode);
		}else{
			$incoming = $xplode[0];
		}


		$data = array($ShipReference);

		$sql = "Insert into Shipment ([ShipReference],[EnterDate],[VendorInvNo],[InTaxRef],[PermitNo],[ExchangeRate],[Currency],[OriginPort],[Supplier],[HAWB],
			[MAWB],[TptMode],[ArrivalDate],[INCOTERM],[Forwarders],[Comments],[JobType],[Customer],[InvoiceValueSGD],[FreightCharges],[InsCharges],[CIFAmount],[GSTAmount],[IncomingReference],[Description],[SupplyingCountry],[ReceiptDate]) 
			values ($ShipReference,GetDate(),'".$this->PostVars['txtVendorInvNo']."','".$this->PostVars['slctax']."','".$this->PostVars['txtPermitNo']."',
			'".$this->PostVars['txtExchangeRate']."','".$this->PostVars['slcCurrency']."','".$this->PostVars['txtOriginPort']."',
			'".$this->mssqlQuotedString($this->PostVars['slcGSTSupplier'])."','".$this->PostVars['txtHAWB']."',
			'".$this->PostVars['txtMAWB']."','".$this->PostVars['txtTptMode']."','".$this->PostVars['txtArrivalDate']."',
			'".$this->PostVars['slcIncoTerm']."','".$this->PostVars['slcForwarders']."','".$this->mssqlQuotedString($this->PostVars['txtComments'])."',
			'".$this->PostVars['slcJobType']."','".$this->PostVars['WawiIndex']."',".$this->PostVars['txtInvoiceValueSGD'].",
			".$this->PostVars['txtFreightCharges'].",".$this->PostVars['txtInsCharges'].",".$this->PostVars['txtCIFAmount'].",".$this->PostVars['txtGSTAmount'].",
			'".$incoming."','".$this->mssqlQuotedString($this->PostVars['txtDescription'])."','".$this->PostVars['txtSupplyingCountry']."',
			'".$this->PostVars['txtReceiptDate']."')";

		if (mssql_query($sql)){
			mssql_query("COMMIT");
			array_push($data,'successADD');
		} else {
			mssql_query("ROLLBACK");
			array_push($data,'erroradd');
		}
		return json_encode($data);
	}
	
	function GetQuantityCustomer(){
		mssql_select_db("MasterData");
		$sql = mssql_fetch_assoc(mssql_query("select DBName from WawiCustomer where [GSTIndex] = '".$this->PostVars['WawiIndex']."'"));
			
		return $sql['DBName'];
	}


	
	function UpdateReference(){
		//32965
		mssql_select_db("Import");
		$sqlShipment = mssql_fetch_assoc(mssql_query("select Customer from Shipment where ShipReference=".$this->PostVars['ShipReference']));
		$Customer = $sqlShipment['Customer'];
		
		$ListRefToUpdate = $this->PostVars['ListRef'];
		foreach($ListRefToUpdate as $RefNum){
			$RefArray = explode("-",$RefNum);
			//2014-04-29 updated due to primary key and incoming reference is duplicate in two different WMS
			mssql_query("Update Import set ShipReference=".$this->PostVars['ShipReference'].",InvoiceValue=".$RefArray[1]." where Reference=".$RefArray[0]." and WawiIndex='".$Customer."';");
			//mssql_query("Update Import set ShipReference=".$this->PostVars['ShipReference'].",WawiIndex='".$Customer."',InvoiceValue=".$RefArray[1]." where Reference=".$RefArray[0]);
		}


		$ListInvToUpdate = $this->PostVars['InvList'];
		foreach($ListInvToUpdate as $InvNum){
			$InvArray = explode("|",$InvNum);
			
			$checkIfExist = mssql_query("SELECT * FROM [Import_Invoice] WHERE Reference = ".$InvArray[0]." AND Invoice='".$InvArray[1]."' AND WawiIndex = '".$Customer."' ");
			
			if(mssql_num_rows($checkIfExist) > 0){
				mssql_query("UPDATE [Import_Invoice] set Quantity=".$InvArray[2].",InvoiceValue=".$InvArray[3]." WHERE Reference=".$InvArray[0]." AND Invoice='".$InvArray[1]."' AND WawiIndex='".$Customer."';");
			}else{
				mssql_query("INSERT INTO [Import_Invoice] (Reference,Invoice,Quantity,InvoiceValue,WawiIndex) VALUES (".$InvArray[0].",'".$InvArray[1]."',".$InvArray[2].",'".$InvArray[3]."','".$Customer."') ");
			}	
			
		}

		
		$data = array($this->PostVars['ShipReference'],$this->PostVars['action']);
		return json_encode($data);
	}
	
	function UpdateReferenceNull(){
		mssql_select_db("Import");

		mssql_query("Update Import set InvoiceValue='0.000000',ShipReference=Null where Reference = ".$this->PostVars['RefNum']." and WawiIndex='".$this->PostVars['WawiIndex']."' ");
		
		// if($this->PostVars['WawiIndex'] == 'ADISGWMS')
		mssql_query("DELETE FROM Import_Invoice where Reference = ".$this->PostVars['RefNum']." and WawiIndex='".$this->PostVars['WawiIndex']."' ");
	}
}

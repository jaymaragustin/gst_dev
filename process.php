<?php 
session_start();
include('include/referer.checker.php');
include('include/include.php');

if (isset($_POST['type']) && $_POST['type']){
	switch($_POST['type']){
		case 'GetWawiCustomer': /*get data fields [ID,WawiIndex,DBName]*/
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$data = $iWMS->GetWawiCustomer($_POST['WawiID']);
			$iWMS->CloseMyConnection();
			echo ($data=='no data' || count($data) < 1) ? 'no data': json_encode($data);
		break;
		
		case 'GetWawiASReportList': /*get data fields [ReportColumns,ReportName]*/
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$data = $iWMS->GetWawiASReportList($_POST['WawiID']);
			$iWMS->CloseMyConnection();
			echo ($data=='no data' || count($data) < 1) ? 'no data': json_encode($data);
		break;
		
		case 'GetASFilterFields': /*get data column names of the view*/
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$data = $iWMS->GetASFilterFields($_POST['WawiID'],$_POST['view']);
			$iWMS->CloseMyConnection();
			echo ($data=='no data' || count($data) < 1) ? 'no data': json_encode($data);
		break;
		
		case 'GetASReportData': /*get data from report*/		

			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			
			$data = $iWMS->GetASReportData($_POST['WawiID'],$_POST['view'],$_POST['field'],$_POST['filter'],$_POST['reportname'],$_POST['range_from'],$_POST['range_to'],$_POST['sort_by'],$_POST['sort_type']);
			//echo $data;
			$iWMS->CloseMyConnection();
			if ($data=='no db'){
				$result = 'no db';
			}else if ($data=='no data'){
				$result = 'no data';
			}else{
				if (is_array($data)){
					$result = json_encode($data);
				}else{
					$result = $data;
				}
			}
			echo $result;

		break;
		
		case 'GetWawiReportList': /*get data fields [ReportColumns,ReportName]*/
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$data = $iWMS->GetWawiReportList($_POST['WawiID']);
			$iWMS->CloseMyConnection();
			echo ($data=='no data' || count($data) < 1) ? 'no data': json_encode($data);
		break;
		
		case 'GetFilterFields': /*get data column names of the view*/
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$data = $iWMS->GetFilterFields($_POST['WawiID'],$_POST['view']);
			$iWMS->CloseMyConnection();
			echo ($data=='no data' || count($data) < 1) ? 'no data': json_encode($data);
		break;
		
		case 'GetReportData': /*get data from report*/		

			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			
			$data = $iWMS->GetReportData($_POST['WawiID'],$_POST['view'],$_POST['field'],$_POST['filter'],$_POST['reportname'],$_POST['range_from'],$_POST['range_to'],$_POST['sort_by'],$_POST['sort_type']);
			//echo $data;
			$iWMS->CloseMyConnection();
			if ($data=='no db'){
				$result = 'no db';
			}else if ($data=='no data'){
				$result = 'no data';
			}else{
				if (is_array($data)){
					$result = json_encode($data);
				}else{
					$result = $data;
				}
			}
			echo $result;

		break;
		
		case 'GetPicklistInfo': 
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			
			if ($DB!='no data' && count($DB)){
				
				$data = $iWMS->GetPicklistInfo($DB[0]['DBName'],$_POST['PacklistNumber']);
				$iWMS->CloseMyConnection();
				
				if ($data=='no data'){
					echo 'no data';
				}else{
					echo ($data=='no data' || count($data) < 1) ? 'no data': json_encode($data);
				}
			}else{
				echo 'no db';
			}
			//echo $msg;
			
		
		break;
		
		case 'UndoNormalScanOut': 
			$iWMS = new iWMS($_SESSION[LoginUserVar]);

			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			
			if ($DB!='no data' && count($DB)){
				$result = $iWMS->UndoNormalScanOut($DB[0]['DBName'],$_POST['PacklistNumber']);
			}else{
				$result = 'no db';
			}

			$iWMS->CloseMyConnection();
			echo $result;
		break;
		
		case 'GetEntryStockIndex': 
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->GetEntryStockIndex($DB[0]['DBName']);
			}else{
				$data = 'no data';
			}
			
			
			$iWMS->CloseMyConnection();
			echo ($data=='no data' || count($data) < 1) ? 'no data': json_encode($data);
		break;
		
		case 'ResetCycleCount': 
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->ResetCycleCount($DB[0]['DBName'],$_POST['StockIndex']);
			}else{
				$data = 'no data';
			}
			
			
			$iWMS->CloseMyConnection();
			echo $data;
		break;
		
		case 'GetLastCycleCount': 
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->GetLastCycleCount($DB[0]['DBName'],$_POST['StockIndex']);
			}else{
				$data = 'no db';
			}
			
			
			$iWMS->CloseMyConnection();
			echo $data;
		break;
		
		//
		
		case 'GetReceivingFields': 
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$data = $iWMS->GetReceivingFields($_POST['WawiID']);
			$iWMS->CloseMyConnection();
			
			//echo '<pre>';print_r($data);echo '</pre>';
			
			$result = '';
			$script = '';
			
			if ($data != 'no data'){
				
				for ($i=0;$i<count($data);$i++){
					
					$result .= '<li><label style="width:150px;">' . $data[$i]["FieldAlias"] . '<span id="' . $data[$i]["FieldName"] . '-misc"></span></label>';
					
					//0 means unlimited
					$maxLength = ($data[$i]["FieldLimit"]) ? ' maxlength="'.$data[$i]["FieldLimit"].'" ': '';
					//$readOnly = ($data[$i]['FieldType']=='datetime') ? ' readonly="readonly" ':'';
					$readOnly = '';
					$title = ' title="' . $data[$i]["FieldAlias"] . '" ';
					$first = ($i==0) ? ' first': '';
					$retain = ($data[$i]["RetainInfo"] && $data[$i]["Visibility"]) ? ' retain': '';
					$required = ($data[$i]["RequiredField"]) ? ' required': ''; 
					$lockicon = ($data[$i]["RetainInfo"]) ? 'ui-icon-locked': 'ui-icon-unlocked';
					$locktooltip = ($lockicon=='ui-icon-locked') ? 'click to unlock this field': 'click to lock this field';
					
					
					switch ($data[$i]["ElementType"]){
						case 'select':
							$result .= '<select id="'.$data[$i]["FieldName"].'" name="'.$data[$i]["FieldName"].'" class="select '.$data[$i]['FieldType'].$first.$retain.$required.'" '.$title.'>';
							if (isset($data[$i]["Options"]) && count($data[$i]["Options"])){

								$result .= '<option value="">Select here</option>';	
								for ($j=0;$j<count($data[$i]["Options"]);$j++){
									if(strlen(trim($data[$i]['Options'][$j]['Index'])) > 0)
									{
										switch($_POST['WawiID']){
											
											case 28:
											case 53:
											case 55:
												switch($data[$i]["FieldName"]){
													case 'CountryOfOrigin':
														//$result .= '<optgroup label="'.$data[$i]['Options'][$j]['Index'].'"><option value="'.$data[$i]['Options'][$j]['Index'].'">'.$data[$i]['Options'][$j]['Description'].'</option></optgroup>';
														$result .= '<option value="'.strtoupper($data[$i]['Options'][$j]['Description']).'">'.strtoupper($data[$i]['Options'][$j]['Description']).'</option>';
													break;
													
													default:
														$result .= '<option value="'.$data[$i]['Options'][$j]['Index'].'">'.$data[$i]['Options'][$j]['Index'].'</option>';
													break;
												}
											break;
											
											default:
												$result .= '<option value="'.$data[$i]['Options'][$j]['Index'].'">'.$data[$i]['Options'][$j]['Index'].'</option>';
											break;
										}

										
									}
								}
	
							}else{
								$result .= '<option value="">No Available '.$data[$i]["FieldAlias"].'</option>';
							} 
							
							$result .= "</select>";
						break;
						
						case 'textarea':
						case 'input':
							$class = 'class="text '.$data[$i]['FieldType'].$first.$retain.$required.'"';
							$class .= ($data[$i]["FieldType"]=='datetime') ? ' datetime':'';
							$result .= '<input type="' . $data[$i]["ElementProperty"] . '" '.$class.' name="' . $data[$i]["FieldName"] . '" id="' . $data[$i]["FieldName"] . '" '.$maxLength.' '.$readOnly.' '.$title.'/>';
						break;
					}
					
					//$result .= '<span class="ui-icon '.$lockicon.'" onclick="if ($(this).hasClass(\'ui-icon-unlocked\')){ $(this).removeClass(\'ui-icon-unlocked\'); $(this).addClass(\'ui-icon-locked\'); $(this).prev().addClass(\'retain\'); $(this).attr(\'title\',\'click to unlock this field\'); }else{$(this).removeClass(\'ui-icon-locked\'); $(this).addClass(\'ui-icon-unlocked\'); $(this).prev().removeClass(\'retain\'); $(this).attr(\'title\',\'click to lock this field\');} " style="cursor:pointer" title="'.$locktooltip.'"></span>';
					
					$result .= ($data[$i]["Visibility"]) ? '<span class="ui-icon '.$lockicon.'" onclick="if ($(this).hasClass(\'ui-icon-unlocked\')){ $(this).removeClass(\'ui-icon-unlocked\'); $(this).addClass(\'ui-icon-locked\'); $(this).prev().addClass(\'retain\'); $(this).attr(\'title\',\'click to unlock this field\'); }else{$(this).removeClass(\'ui-icon-locked\'); $(this).addClass(\'ui-icon-unlocked\'); $(this).prev().removeClass(\'retain\'); $(this).attr(\'title\',\'click to lock this field\');} " style="cursor:pointer" title="'.$locktooltip.'"></span>': '';
					
					/*SCRIPT TO FOCUS ON NEXT FIELD WHEN ENTER KEY IS PRESSED*/
					if ($i<count($data)-1){
						$script .='<script>
						$(function() {
							$("#'.$data[$i]["FieldName"].'").keypress(function(event) {
							  if ( event.which == 13 ) {
								  $("#'.$data[$i+1]["FieldName"].'").focus().select();
								  if ("'.$data[$i]["ElementType"].'"=="select"){
									  event.preventDefault();
								  }
							  }
							});							
						})
						</script>';
					}else{
						$script .='<script>
						$(function() {
							$("#'.$data[$i]["FieldName"].'").keypress(function(event) {
							  if ( event.which == 13 ) {
								  if ("'.$data[$i]["ElementType"].'"=="select"){
									  event.preventDefault();
								  }
								 $("#submit-btn").click();
							  }
							});
						})
						</script>';
					}
					
					$result .= '</li>';
				}
				
			}else{
				$result = 'no data';
			}
			
			echo $result.$script;
			
		break;
		
		case 'SaveReceiving':
			//echo '<pre>'; print_r($_POST); echo '</pre>';die();
		
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			
			if ($DB!='no data' && count($DB)){
				
				$valid = 1;
				
				
				if (!isset($_POST['EntryNumber'])){
					$valid = 0;
					$result = 9;
				}else
				
				if (!isset($_POST['Quantity'])){
					$valid = 0;
					$result = 10;
				}else
				
				if (!isset($_POST['StockIndex'])){
					$valid = 0;
					$result = 12;
				}else
				
				if (!isset($_POST['Partnumber']) || !$_POST['Partnumber']){
					$valid = 0;
					$result = 13;
				}else
				
				if (!isset($_POST['StockIndex']) || !$_POST['StockIndex']){
					$valid = 0;
					$result = 14;
				}
				
				
				
				if ($valid){
					$dataPOST = $_POST;

					$result = $iWMS->SaveReceiving($dataPOST,$DB[0]);
				}
				
			}else{
				$result = 1;
			}
			
			$iWMS->CloseMyConnection();
			echo $result;
		break;
		
		case 'SaveCustomReceiving':		
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);			
			if ($DB!='no db' && count($DB)){				
				$result = $iWMS->SaveCustomReceiving($_POST,$DB[0]);				
			}else{
				$result = 1;
			}			
			$iWMS->CloseMyConnection();
			echo $result;
		break;
		
		case 'RetainReceivingField':

			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$data = $iWMS->RetainReceivingField($_POST['WawiID']);
		
			$iWMS->CloseMyConnection();
			
			//echo '<pre>'; print_r($data); echo '</pre>';
		break;
		
		case 'Putaway':

			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			
			if ($DB!='no data' && count($DB)){
				$dataCallback = array();
				if (count($_POST)){
					foreach($_POST['putawaydata'] as $values){
						$temp = explode('|',$values);
						$dataCallback[] = $iWMS->Putaway($DB[0]['DBName'],$temp[0],$temp[1]);
					}
					$result = json_encode($dataCallback);
				}else{
					$result = 'no data request';
				}

			}else{
				$result = 'no db';
			}
			
			$iWMS->CloseMyConnection();		
			echo $result;
		break;
		
		case 'GetEntryParts':

			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			
			if ($DB!='no data' && count($DB)){
				$parts = $iWMS->GetEntryParts($DB[0]['DBName']);
				//echo '<pre>'; print_r($result); echo '</pre>';
				$result = ($parts=='no data' || count($parts) < 1) ? 'no data': json_encode($parts);
			}else{
				$result = 'no data';
			}
			
			$iWMS->CloseMyConnection();
			echo $result;

		break;
		
		case 'GetEntryLot':
			//echo '<pre>'; print_r($_POST); echo '</pre>';
			
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->GetEntryLot($DB[0]['DBName'],$_POST['partnumber']);
				$result = ($data=='no data' || count($data) < 1) ? 'no data': json_encode($data);
			}else{
				$result = 'no data';
			}
			
			$iWMS->CloseMyConnection();
			echo $result;
		break;
		
		case 'GetEntryNumber':
			
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->GetEntryNumber($DB[0]['DBName'],$_POST['partnumber']);
				$result = ($data=='no data' || count($data) < 1) ? 'no data': json_encode($data);
			}else{
				$result = 'no data';
			}
			
			$iWMS->CloseMyConnection();
			echo $result;
		break;
		
		case 'GetEntryNumberbyLot':
			
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->GetEntryNumberbyLot($DB[0]['DBName'],$_POST['partnumber'],$_POST['lotnumber']);
				$result = ($data=='no data' || count($data) < 1) ? 'no data': json_encode($data);
			}else{
				$result = 'no data';
			}
			
			$iWMS->CloseMyConnection();
			echo $result;
		break;
		
		case 'GetEntrySum':
			
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->GetEntrySum($DB[0]['DBName'],$_POST['partnumber'],$_POST['lotnumber'],$_POST['entrynumber']);
				$result = $data['SumPart'];
			}else{
				$result = 'no data';
			}
			
			$iWMS->CloseMyConnection();
			echo $result;
		break;
		
		case 'EntryComment':
		
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			
			if ($DB!='no data' && count($DB)){
				$result = $iWMS->EntryComment($DB[0]['DBName'],$_POST['partnumber'],$_POST['lotnumber'],$_POST['entrynumber'],$_POST['comment'],$_POST['block']);
			}else{
				$result = 'no data';
			}
			
			$iWMS->CloseMyConnection();
			echo $result;
			
		break;
		
		case 'GetDivision':

			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->GetDivision($DB[0]['DBName']);
				$result = ($data=='no data' || count($data) < 1) ? 'no data': json_encode($data);
			}else{
				$result = 'no data';
			}
			
			$iWMS->CloseMyConnection();
			echo $result;
			
		break;
		
		case 'GetPartPerDivision':

			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->GetPartPerDivision($DB[0]['DBName'],$_POST['division']);
				$result = ($data=='no data' || count($data) < 1) ? 'no data': json_encode($data);
			}else{
				$result = 'no data';
			}
			
			$iWMS->CloseMyConnection();
			echo $result;
			
		break;
		
		case 'GetEntryTransferList':

			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->GetEntryTransferList($DB[0]['DBName'],$_POST['division'],$_POST['partnumber']);
				$result = ($data=='no data' || count($data) < 1) ? 'no data': json_encode($data);
				
			}else{
				$result = 'no data';
			}
			
			$iWMS->CloseMyConnection();
			echo $result;
			
		break;
		
		case 'EntryTransfer':

			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			
			if ($DB!='no data' && count($DB)){
				//echo '<pre>'; print_r($_POST); echo '</pre>';
				$data = $iWMS->EntryTransfer($DB[0]['DBName'],$_POST['selectedEntry'],$_POST['division'],$_POST['todivision']);
			}else{
				$data = 'no db';
			}
			
			$iWMS->CloseMyConnection();
			echo $data;
			
		break;
		
		case 'ModifyEntryData':

			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->ModifyEntryData($DB[0]['DBName'],$_POST['entrynumber'],$_POST['description']);
			}else{
				$data = 'no db';
			}
			
			$iWMS->CloseMyConnection();
			echo $data;
			
		break;
		
		
		case 'GetEntryData':

			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->GetEntryData($_POST['WawiID'],$DB[0]['DBName'],$_POST['action'],$_POST['entrynumber']);
				if (is_array($data)){
					$data[0]['EntryDate'] = $data[0]['DateFormatted'];
				}
				
				$result = ($data=='no data' || count($data) < 1) ? 'no data': json_encode($data);
			}else{
				$result = 'no db';
			}
			
			$iWMS->CloseMyConnection();
			echo $result;
			
		break;
		
		case 'CheckPackListScan':
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->CheckPackListScan($DB[0]['DBName'],$_POST['packlistnumber']);
			}else{
				$data = 'no db';
			}
			
			$iWMS->CloseMyConnection();
			echo $data;
		
		break;
		
		case 'CheckScanOutComplete':
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->CheckScanOutComplete($DB[0]['DBName'],$_POST['packlistnumber'],0);
			}else{
				$data = 'no db';
			}
			
			$iWMS->CloseMyConnection();
			echo $data;
		
		break;
		
		case 'GetScanOut':
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->GetScanOut($DB[0]['DBName'],$_POST['packlistnumber']);
				$result = ($data=='no data' || count($data) < 1) ? 'no data': json_encode($data);
			}else{
				$result = 'no db';
			}
			
			$iWMS->CloseMyConnection();
			echo $result;
		
		break;
		
		case 'GetBoxWeight':
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->GetBoxWeight($DB[0]['DBName'],$_POST['packlistnumber']);
				$result = ($data=='no data' || count($data) < 1) ? 'no data': json_encode($data);
			}else{
				$result = 'no db';
			}
			
			$iWMS->CloseMyConnection();
			echo $result;
		
		break;
		
		case 'GetBoxWeightByBN':
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->GetBoxWeightByBN($DB[0]['DBName'],$_POST['boxnumber']);
				$result = ($data=='no data' || count($data) < 1) ? 'no data': json_encode($data);
			}else{
				$result = 'no db';
			}
			
			$iWMS->CloseMyConnection();
			echo $result;
		
		break;
		
		/*
		case 'CheckBoxweightScan':
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			
			//echo '<pre>'; print_r($_POST); echo '</pre>';
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->CheckBoxweightScan($DB[0]['DBName'],$_POST['ordernumber']);
				$result = ($data=='no data' || count($data) < 1) ? 'no data': json_encode($data);
			}else{
				$result = 'no db';
			}
			
			$iWMS->CloseMyConnection();
			
			//echo '<pre>'; print_r($result); echo '</pre>';
			
			echo $result;
		
		break;
		*/
		case 'GetDateCode':
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			
			
			if ($DB!='no data' && count($DB)){
				//echo '<pre>'; print_r($_POST); echo '</pre>';
				$data = $iWMS->GetDateCode($DB[0]['DBName'],$_POST['entrynumber'],$_POST['requesttype']);
			}else{
				$data = 'no db';
			}
			
			$iWMS->CloseMyConnection();
			echo $data;
		
		break;
		
		/* Normal Scanout : Get dimension.*/
		case 'GetDimension' :
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$data = $iWMS->GetDimension($_POST['WawiID']);			
			
			$iWMS->CloseMyConnection();	
			echo json_encode($data);

		break;
		
		case 'NormalScanOut':
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);

			if ($DB!='no data' && count($DB)){
				$data = $iWMS->NormalScanOut($DB[0]['DBName'],$_POST);
			}else{
				$data = 'no db';
			}
			
			$iWMS->CloseMyConnection();
			echo $data;
		
		break;
		
		/* Change Partnumber : Get the existing partnumber - Marketing */
		case 'GetPartNumberM':

			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			
			if ($DB!='no data' && count($DB)){
				$partnos = $iWMS->GetPartNumberM($DB[0]['DBName']);
				// echo '<pre>'; print_r($result); echo '</pre>';
				$result = ($partnos=='no data' || count($partnos) < 1) ? 'no data': json_encode($partnos);
			}else{
				$result = 'no data';
			}
			
			$iWMS->CloseMyConnection();
			echo $result;
			
			
		break;
		
		/* Change Partnumber : Get the existing partnumber - Partnumber */
		case 'GetPartNumberP':

			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			
			if ($DB!='no data' && count($DB)){
				$partnos = $iWMS->GetPartNumberP($DB[0]['DBName']);
				// echo '<pre>'; print_r($result); echo '</pre>';
				$result = ($partnos=='no data' || count($partnos) < 1) ? 'no data': json_encode($partnos);
			}else{
				$result = 'no data';
			}
			
			$iWMS->CloseMyConnection();
			echo $result;
			
			
		break;
		
		/* Change Partnumber : Get the existing partnumber - SupplierPartnumber */
		case 'GetPartNumberS':

			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			
			if ($DB!='no data' && count($DB)){
				$partnos = $iWMS->GetPartNumberS($DB[0]['DBName']);
				$result = ($partnos=='no data' || count($partnos) < 1) ? 'no data': json_encode($partnos);
			}else{
				$result = 'no data';
			}
			
			$iWMS->CloseMyConnection();
			echo $result;
			
			
		break;
		
		/* Change Partnumber : Get the VSC Partnumber - Marketing */
		case 'GetSupplierPartNumber':
			
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			
			if ($DB!='no data' && count($DB)){
				$entry = $iWMS->GetSupplierPartNumber($DB[0]['DBName'],$_POST['partnumber']);
				$result = ($entry=='no data' || count($entry) < 1) ? 'no data': json_encode($entry);
			}else{
				$result = 'no data';
			}
			
			$iWMS->CloseMyConnection();
			echo $result;
		break;
		
		/* Change Partnumber : Get the entry number - Partnumber */
		case 'GetEntryNumberP':
			
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			
			if ($DB!='no data' && count($DB)){
				$entry = $iWMS->GetEntryNumberP($DB[0]['DBName'],$_POST['partnumber']);
				$result = ($entry=='no data' || count($entry) < 1) ? 'no data': json_encode($entry);
			}else{
				$result = 'no data';
			}
			
			$iWMS->CloseMyConnection();
			echo $result;
		break;
		
		/* Change Partnumber : Get the entry number - Supplier */
		case 'GetEntryNumberS':
			
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			
			if ($DB!='no data' && count($DB)){
				$entry = $iWMS->GetEntryNumberS($DB[0]['DBName'],$_POST['partnumber']);
				$result = ($entry=='no data' || count($entry) < 1) ? 'no data': json_encode($entry);
			}else{
				$result = 'no data';
			}
			
			$iWMS->CloseMyConnection();
			echo $result;
		break;
		
		/* Change Partnumber : Get the new partnumber */
		case 'GetNewPartNumber':
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			if ($DB!='no data' && count($DB)){
				$entry = $iWMS->GetNewPartNumber($DB[0]['DBName'],$_POST['partnumber']);
				$result = ($entry=='no data' || count($entry) < 1) ? 'no data': json_encode($entry);
			}else{
				$result = 'no data';
			}
			 
			$iWMS->CloseMyConnection();
			echo $result;
		break;
		
		/* Change Partnumber : Update the partnumber - Marketing*/
		case 'UpdatePartNumberM':
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			if ($DB!='no data' && count($DB)){
				$entry = $iWMS->UpdatePartNumberM($DB[0]['DBName'],$_POST['partnumber'],$_POST['SupplierPartNumber']);
				$result = ($entry=='no data' || count($entry) < 1) ? 'no data': $entry;
			}else{
				$result = 'no data';
			}
			
			$iWMS->CloseMyConnection();
			
			echo $result;
		break;
		
		/* Change Partnumber : Update the partnumber - Partnumber*/
		case 'UpdatePartNumberP':
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			if ($DB!='no data' && count($DB)){
				$entry = $iWMS->UpdatePartNumberP($DB[0]['DBName'],$_POST['partnumber'],$_POST['txtentrynumber'],$_POST['NewPartNumber']);
				$result = ($entry=='no data' || count($entry) < 1) ? 'no data': $entry;
			}else{
				$result = 'no data';
			}
			
			$iWMS->CloseMyConnection();
			
			echo $result;
		break;
		
		/* Change Partnumber : Update the partnumber - Supplier*/
		case 'UpdatePartNumberS':
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			if ($DB!='no data' && count($DB)){
				$entry = $iWMS->UpdatePartNumberS($DB[0]['DBName'],$_POST['partnumber'],$_POST['txtentrynumber'],$_POST['NewPartNumber']);
				$result = ($entry=='no data' || count($entry) < 1) ? 'no data': $entry;
			}else{
				$result = 'no data';
			}
			
			$iWMS->CloseMyConnection();
			
			echo $result;
		break;
		
		/* Flight Detail : Get Uploaded Excel Data*/
		case 'GetFlightDetailExcelData':
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			if ($DB!='no data' && count($DB)){
				$entry = $iWMS->GetFlightDetailExcelData($DB[0]['DBName'],$_POST['filename']);
				$result = ($entry=='no data' || count($entry) < 1) ? 'no data': json_encode($entry);
			}else{
				$result = 'no data';
			}
			
			$iWMS->CloseMyConnection();
			
			echo $result;
		break;
		
		/* Flight Detail : Update flight detail */
		case 'UpdateFlightDetail':
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			if ($DB!='no data' && count($DB)){
				$entry = $iWMS->UpdateFlightDetail($DB[0]['DBName'],$_POST);
				$result = ($entry=='no data' || count($entry) < 1) ? 'no data' : $entry;					
			}else{
				$result = 'no data';
			}			
			$iWMS->CloseMyConnection();	
			
			//echo $result;
		break;
		
		/* Flight Detail : Update flight data */
		case 'UpdateFlightData'	:
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			if ($DB!='no data' && count($DB)){
				$entry = $iWMS->UpdateFlightData($DB[0]['DBName'],$_POST);
				if($entry =='no data' || count($entry) < 1){ return 'no data'; }
				else if($entry =='1'){ $result = 'success'; }
				else if($entry =='2'){ $result = 'failed'; }
				else if(is_array($entry)){ $result = json_encode($entry); } 
				else{ $result = $entry; }
				
			}else{
				$result = 'no data';
			}			
			$iWMS->CloseMyConnection();	
			
			echo $result;
			
		break;
		
		/* Flight Detail : Get shipping method */
		case 'GetShippingMethod'	:
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			if ($DB!='no data' && count($DB)){
				$entry = $iWMS->GetShippingMethod($DB[0]['DBName']);
				$result = ($entry=='no data' || count($entry) < 1) ? 'no data': json_encode($entry);					
				
			}else{
				$result = 'no data';
			}			
			$iWMS->CloseMyConnection();	
			
			echo $result;
			
		break;
		
		/* Flight Detail : Get shipping method */
		case 'GetUpdatedFlightData' :
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			if ($DB!='no data' && count($DB)){
				$entry = $iWMS->GetUpdatedFlightData($DB[0]['DBName'],$_POST['packlistnumber']);
				$result = ($entry=='no data' || count($entry) < 1) ? 'no data': json_encode($entry);				
			}else{
				$result = 'no data';
			}			
			$iWMS->CloseMyConnection();	
			
			echo $result;
		break;
		
		/* Consol Scanout : Get latest consol number*/
		case 'GetLatestConsolNumber' :
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			if ($DB!='no data' && count($DB)){
				$entry = $iWMS->GetLatestConsolNumber($DB[0]['DBName']);				
				$result = ($entry=='no data' || count($entry) < 1) ? 'no data': $entry;				
			}else{
				$result = 'no data';
			}			
			$iWMS->CloseMyConnection();	
			
			echo $result;
		break;
		
		/* Consol Scanout : Generate consol number*/
		case 'GenerateConsolNumber' :
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			if ($DB!='no data' && count($DB)){
				$entry = $iWMS->GenerateConsolNumber($DB[0]['DBName']);				
				$result = ($entry=='no data' || count($entry) < 1) ? 'no data': $entry;				
			}else{
				$result = 'no data';
			}			
			$iWMS->CloseMyConnection();	
			
			echo $result;
		break;
		
		/* Consol Scanout : Get dimension type.*/
		case 'GetDimensionType' :
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			if ($DB!='no data' && count($DB)){
				$entry = $iWMS->GetDimensionType($DB[0]['DBName']);				
				$result = ($entry=='no data' || count($entry) < 1) ? 'no data': json_encode($entry);				
			}else{
				$result = 'no data';
			}			
			$iWMS->CloseMyConnection();	
			
			echo $result;
		break;
		
		/* Consol Scanout : Save data */
		case 'ConsolScanout' :
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			if ($DB!='no data' && count($DB)){
				$entry = $iWMS->ConsolScanout($DB[0]['DBName'],$_POST,$_SESSION[LoginUserVar]);				
				$result = ($entry=='no data' || count($entry) < 1) ? 'no data': $entry;				
			}else{
				$result = 'no data';
			}			
			$iWMS->CloseMyConnection();	
			
			echo $result;
		break;
		
		/* Undo Consol Scanout : revert consol data */
		case 'UndoConsolScanout' :
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			
			if ($DB!='no data' && count($DB)){
				$entry = $iWMS->UndoConsolScanout($DB[0]['DBName'],$_POST);				
				$result = ($entry=='no data' || count($entry) < 1) ? 'no data': $entry;				
			}else{
				$result = 'no data';
			}			
			$iWMS->CloseMyConnection();	
			
			echo $result;
		break;
		
		/* Undo Book: search the thru data */
		case 'GetPicklistData' :
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			
			if ($DB!='no data' && count($DB)){
				$entry = $iWMS->GetPicklistData($DB[0]['DBName'],$_POST['picklistnum']);				
				$result = ($entry=='no data' || count($entry) < 1) ? 'no data': json_encode($entry);				
			}else{
				$result = 'no data';
			}			
			$iWMS->CloseMyConnection();	
			
			echo $result;
		break;
		
		/* Undo Book: get packlist position */
		case 'GetPackListPosition' :
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			if ($DB!='no data' && count($DB)){
				$entry = $iWMS->GetPackListPosition($DB[0]['DBName'],$_POST['picklistnum']);				
				$result = ($entry=='no data' || count($entry) < 1) ? 'no data': json_encode($entry);				
			}else{
				$result = 'no data';
			}			
			$iWMS->CloseMyConnection();	
			
			echo $result;
		break;
		
		/* Undo Book: undo booking data */
		case 'UndoBooking' :
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			if ($DB!='no data' && count($DB)){
				$entry = $iWMS->UndoBooking($DB[0]['DBName'],$_POST);				
				$result = ($entry=='no data' || count($entry) < 1) ? 'no data': $entry;				
			}else{
				$result = 'no data';
			}			
			$iWMS->CloseMyConnection();	
			
			echo $result;
		break;
		
		/* Cycle Count : get partnumber by entry number */
		case 'GetPNByEntryNumber' :
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			if ($DB!='no data' && count($DB)){
				$entry = $iWMS->GetPNByEntryNumber($DB[0]['DBName'],$_POST['entrynumber']);				
				$result = ($entry=='no data' || count($entry) < 1) ? 'no data': json_encode($entry);				
			}else{
				$result = 'no data';
			}			
			$iWMS->CloseMyConnection();	
			
			echo $result;
		break;
		
		/* Cycle Count : update entry */
		case 'UpdateCycleCount' :
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			if ($DB!='no data' && count($DB)){
				$entry = $iWMS->UpdateCycleCount($DB[0]['DBName'],$_POST,$_SESSION[LoginUserVar]);				
				$result = ($entry=='no data' || count($entry) < 1) ? 'no data': $entry;				
			}else{
				$result = 'no data';
			}			
			$iWMS->CloseMyConnection();	
			
			echo $result;
		break;
		
		/* Cycle Count : update entry */
		case 'RecordCycleCount' :
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			if ($DB!='no data' && count($DB)){
				$entry = $iWMS->RecordCycleCount($DB[0]['DBName'],$_POST);				
				$result = ($entry=='no data' || count($entry) < 1) ? 'no data': $entry;				
			}else{
				$result = 'no db';
			}			
			$iWMS->CloseMyConnection();	
			
			echo $result;
		break;
		
		case 'GetReceivingByEntry' :
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			if ($DB!='no data' && count($DB)){
				$entry = $iWMS->GetReceivingByEntry($DB[0]['DBName'],$_POST['entrynumber']);				
				$data = ($entry=='no data' || count($entry) < 1) ? 'no data': json_encode($entry);		
			}else{
				$data = 'no db';
			}			
			$iWMS->CloseMyConnection();	
			
			echo $data;
		break;
		
		case 'LogMeIn' :

			$iWMS = new iWMS($_POST['username']);
			$data = $iWMS->LogMeIn($_POST['username'],$_POST['password']);	
			$iWMS->CloseMyConnection();	
			echo $data;			
		break;
		
		case 'CheckFirstLogin' :
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$data = $iWMS->CheckFirstLogin($_SESSION[LoginUserVar]);	
			$iWMS->CloseMyConnection();	
			echo $data;			
		break; 
		case 'UpdateUserFirstLogin' :
			$iWMS = new iWMS($_POST['username']);
			$data = $iWMS->UpdateUserFirstLogin($_POST['username'],$_POST);	
			$iWMS->CloseMyConnection();	
			echo $data;			
		break;
		
		case 'GetGSTModules':
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$data = $iWMS->GetGSTModules($_SESSION[LoginUserVar],$_SESSION['defaultProgramID']);	
			$iWMS->CloseMyConnection();	
			echo ($data=='no data' || count($data) < 1) ? 'no data': json_encode($data);
		break;
		
		/* Support : send email to support */
		case 'ContactSupport' :
			$iWMS = new iWMS($_POST['user']);
			$result = $iWMS->ContactSupport($_POST,$_FILES,$_POST['user']);				
			$iWMS->CloseMyConnection();				
			echo $result;
		break;		
		/* I forgot */
		case 'iForgot' :
			$iWMS = new iWMS($_POST['username']);
			$data = $iWMS->iForgot($_POST['username']);	
			$iWMS->CloseMyConnection();	
			echo $data;			
		break;
		
		case 'UndoReceiving':

			$iWMS = new iWMS($_SESSION[LoginUserVar]);
		
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			if ($DB!='no data' && count($DB)){
				$result = $iWMS->UndoReceiving($DB[0]['DBName'],$_POST['entrynumber']);								
			}else{
				$result = 'no db';
			}			
		
			$iWMS->CloseMyConnection();
			
			echo $result;
		break;
		/* Parse barcode string*/
		case 'Parse2DBarcodeReceiving' :
			$iWMS = new iWMS($_SESSION[LoginUserVar]);		
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			if ($DB!='no data' && count($DB)){
				$result = $iWMS->Parse2DBarcodeReceiving($DB[0]['DBName'],$_POST['barcode']);	
				$result = json_encode($result);
			}else{
				$result = 'no db';
			}					
			$iWMS->CloseMyConnection();			
			echo $result;
		break;
		
		/* Get receiving supplier data */
		case 'GetReceivingSupplier' :
			$iWMS = new iWMS($_SESSION[LoginUserVar]);		
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			if ($DB!='no data' && count($DB)){
				$result = $iWMS->GetReceivingSupplier($DB[0]['DBName']);	
				$result = ($result == 'no data') ? 'no data' : json_encode($result);
			}else{
				$result = 'no db';
			}					
			$iWMS->CloseMyConnection();			
			echo $result;
		break;
		
		/* Get receiving stock index */
		case 'GetReceivingStockIndex' :
			$iWMS = new iWMS($_SESSION[LoginUserVar]);		
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			if ($DB!='no data' && count($DB)){
				$result = $iWMS->GetReceivingStockIndex($DB[0]['DBName']);	
				$result = ($result == 'no data') ? 'no data' : json_encode($result);
			}else{
				$result = 'no db';
			}					
			$iWMS->CloseMyConnection();			
			echo $result;
		break;
		
		/* Get receiving tax scheme */
		case 'GetReceivingTaxScheme' :
			$iWMS = new iWMS($_SESSION[LoginUserVar]);		
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			if ($DB!='no data' && count($DB)){
				$result = $iWMS->GetReceivingTaxScheme($DB[0]['DBName']);	
				$result = ($result == 'no data') ? 'no data' : json_encode($result);
			}else{
				$result = 'no db';
			}					
			$iWMS->CloseMyConnection();			
			echo $result;
		break;
		
		/* Save 2D receiving details */
		case 'Save2DReceiving' :
			$iWMS = new iWMS($_SESSION[LoginUserVar]);		
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			if ($DB!='no data' && count($DB)){
				$result = $iWMS->Save2DReceiving($DB[0]['DBName'],$_POST,$_SESSION[LoginUserVar]);	
			}else{
				$result = 'no db';
			}					
			$iWMS->CloseMyConnection();			
			echo $result;
		break;
		
		/* Get Cycle Count Report */
		case 'GetCycleCountReport' :
			$iWMS = new iWMS($_SESSION[LoginUserVar]);		
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->GetCycleCountReport($DB,$_POST['stockindex'],$_POST['filter'],$_POST['scandate']);	
				$result = ($data == 'no data') ? 'no data' : json_encode($data);
			}else{
				$result = 'no db';
			}					
			$iWMS->CloseMyConnection();			
			echo $result;
		break;
		
		/* requestreport */
		case 'RequestEmailReport' :
	
			$iWMS = new iWMS($_SESSION[LoginUserVar]);		
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			if ($DB!='no data' && count($DB)){

				switch ($_POST['rptType'])	{
										
					case 1:
					case 3:
						$params = array("filter" => $_POST['filter'], "range_from" => $_POST['range_from'], "range_to" => $_POST['range_to'], "field" => $_POST['field'], "sort_by" => $_POST['sort_by'], "sort_type" => $_POST['sort_type'], "DBName" => $DB[0]['DBName'], "WawiID" => $_POST['WawiID'], "WawiIndex" => $DB[0]['WawiIndex'], "reportname" => $_POST['reportname'], "rptType" => $_POST['rptType']);
					break;
					
					case 2:
						$params = array("DBName" => $DB[0]['DBName'],
						"stockindex" => $_POST['stockindex'],
						"scandate" => $_POST['scandate'],
						"filter" => $_POST['filter'],
						"WawiIndex" => $DB[0]['WawiIndex'],
						"rptType" => $_POST['rptType']);
					break;
				}
				
				$result = $iWMS->RequestEmailReport($params,0);	
				
			}else{
				$result = 'no db';
			}					
			$iWMS->CloseMyConnection();			
			echo $result;
		break;

		
		/* Unlock Order */
		case 'UnlockOrder' :
			$iWMS = new iWMS($_SESSION[LoginUserVar]);		
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			if ($DB!='no data' && count($DB)){
				$result = $iWMS->UnlockOrder($DB[0]['DBName'],$_POST['ordernumber']);	
			}else{
				$result = 'no db';
			}					
			$iWMS->CloseMyConnection();			
			echo $result;
		break;
		
		/* Get Stocks */
		case 'GetStocks' :
			$iWMS = new iWMS($_SESSION[LoginUserVar]);		
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->GetStocks($DB[0]['DBName'],$_POST['stock']);	
				$result = ($data == 'no data') ? 'no data' : json_encode($data);
			}else{
				$result = 'no db';
			}					
			$iWMS->CloseMyConnection();			
			echo $result;
		break;
		
		/* Update Stock Index */
		case 'UpdateStockIndex' :
			$iWMS = new iWMS($_SESSION[LoginUserVar]);		
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			if ($DB!='no data' && count($DB)){
				$result = $iWMS->UpdateStockIndex($DB[0]['DBName'],$_POST);	
			}else{
				$result = 'no db';
			}					
			$iWMS->CloseMyConnection();	
			
			//echo '<pre>'; print_r($_POST); echo '</pre>';
					
			echo $result;
		break;
		
		/* Save Stock Index */
		case 'SaveStockIndex' :
			$iWMS = new iWMS($_SESSION[LoginUserVar]);		
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			if ($DB!='no data' && count($DB)){
				$result = $iWMS->SaveStockIndex($DB[0]['DBName'],$_POST);	
			}else{
				$result = 'no db';
			}					
			$iWMS->CloseMyConnection();			
			echo $result;
		break;
		
		/* Check Entry Number */
		case 'CheckEntryNumber' :
			$iWMS = new iWMS($_SESSION[LoginUserVar]);		
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->CheckEntryNumber($DB[0]['DBName'],$_POST['EntryNumber']);	
				$result = ($data == 'no data') ? 'no data' : json_encode($data);
			}else{
				$result = 'no db';
			}					
			$iWMS->CloseMyConnection();			
			echo $result;
		break;
		
		/* Get Customer Info */
		case 'GetCustomer' :
			$iWMS = new iWMS($_SESSION[LoginUserVar]);		
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->GetCustomer($DB[0]['DBName'],$_POST['index']);	
				$result = ($data == 'no data') ? 'no data' : json_encode($data);
			}else{
				$result = 'no db';
			}					
			$iWMS->CloseMyConnection();			
			echo $result;
		break;
		
		/* Save Customer */
		case 'SaveCustomer' :
			$iWMS = new iWMS($_SESSION[LoginUserVar]);		
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			if ($DB!='no data' && count($DB)){
				$result = $iWMS->SaveCustomer($DB[0]['DBName'],$_POST);	
			}else{
				$result = 'no db';
			}					
			$iWMS->CloseMyConnection();			
			echo $result;
		break;
		
		/* Update Customer */
		case 'UpdateCustomer' :
			$iWMS = new iWMS($_SESSION[LoginUserVar]);		
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			if ($DB!='no data' && count($DB)){
				$result = $iWMS->UpdateCustomer($DB[0]['DBName'],$_POST);	
			}else{
				$result = 'no db';
			}					
			$iWMS->CloseMyConnection();			
			echo $result;
		break;
		
		case 'SellingPriceGroups' :
			$iWMS = new iWMS($_SESSION[LoginUserVar]);		
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->SellingPriceGroups($DB[0]['DBName']);	
				$result = ($data == 'no data') ? 'no data' : json_encode($data);
			}else{
				$result = 'no db';
			}					
			$iWMS->CloseMyConnection();			
			echo $result;
		break;
		
		case 'PaymentTerms' :
			$iWMS = new iWMS($_SESSION[LoginUserVar]);		
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->PaymentTerms($DB[0]['DBName']);	
				$result = ($data == 'no data') ? 'no data' : json_encode($data);
			}else{
				$result = 'no db';
			}					
			$iWMS->CloseMyConnection();			
			echo $result;
		break;
		
		/* Get Partnumber details in parts table */
		case 'GetPartNumber':

			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			
			/*------------------------------+
			| $requestType values are		|
			| 1 = direct request			|
			| 2 = request from a function	|
			| $partnumber = if null then all|
			+------------------------------*/
			$requestType = 1;
			
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->GetPartNumber($_POST['Partnumber'],$DB[0]['DBName'],$requestType,$_POST['fields']);
				$result = ($data=='no data' || count($data) < 1) ? 'no data': json_encode($data);
			}else{
				$result = 'no db';
			}
			
			$iWMS->CloseMyConnection();
			echo $result;
			
		break;
		
		/* Save Parts information */
		case 'SaveParts' :
			$iWMS = new iWMS($_SESSION[LoginUserVar]);		
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->SaveParts($DB[0]['DBName'],$_POST);	
				$result = $data;
			}else{
				$result = 'no db';
			}					
			$iWMS->CloseMyConnection();			
			echo $result;
		break;
		
		/* Update Parts information */
		case 'UpdateParts' :
			$iWMS = new iWMS($_SESSION[LoginUserVar]);		
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->UpdateParts($DB[0]['DBName'],$_POST);	
				$result = $data;
			}else{
				$result = 'no db';
			}					
			$iWMS->CloseMyConnection();			
			echo $result;
		break;
		
		/* Get the quantity list based on partnumber */
		case 'GetQuantityList':
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);			
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->GetQuantityList($DB[0]['DBName'],$_POST['Partnumber']);
				$result = ($data=='no data' || count($data) < 1) ? 'no data': json_encode($data);
			}else{
				$result = 'no db';
			}			
			$iWMS->CloseMyConnection();
			echo $result;			
		break;
		
		/* Get the Part in group based on partnumber */
		case 'GetPartinGroup':
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);			
			if ($DB!='no data' && count($DB)){ 
				$data = $iWMS->GetPartinGroup($DB[0]['DBName'],$_POST['Partnumber']);
				$result = ($data=='no data' || count($data) < 1) ? 'no data': json_encode($data);
			}else{
				$result = 'no db';
			}			
			$iWMS->CloseMyConnection();
			echo $result;			
		break;
		
		/* Get supplier information */
		case 'GetSupplier':
		
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			
			/*------------------------------+
			| $requestType values are		|
			| 1 = direct request			|
			| 2 = request from a function	|
			| $index = if null then all|
			+------------------------------*/
			$requestType = 1;
			
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->GetSupplier($_POST['Index'],$DB[0]['DBName'],$requestType,$_POST['fields']);
				$result = ($data=='no data' || count($data) < 1) ? 'no data': json_encode($data);
			}else{ 
				$result = 'no db';
			}
			
			$iWMS->CloseMyConnection();
			echo $result;
			
		break;
		/* Save Supplier information */
		case 'SaveSupplier' :
			$iWMS = new iWMS($_SESSION[LoginUserVar]);		
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->SaveSupplier($DB[0]['DBName'],$_POST);	
				$result = $data;
			}else{
				$result = 'no db';
			}					
			$iWMS->CloseMyConnection();			
			echo $result;
		break;
		/* Update Supplier information */
		case 'UpdateSupplier' :
			$iWMS = new iWMS($_SESSION[LoginUserVar]);		
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']); 
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->UpdateSupplier($DB[0]['DBName'],$_POST);	
				$result = $data;
			}else{
				$result = 'no db';
			}					
			$iWMS->CloseMyConnection();			
			echo $result;
		break;
		
		case 'UpdateBoxWeight':
			$iWMS = new iWMS($_SESSION[LoginUserVar]);		
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']); 
			if ($DB!='no data' && count($DB)){
				if (is_numeric($_POST['value'])){
					$data = $iWMS->UpdateBoxWeight($DB[0]['DBName'],$_POST['packlistnumber'],$_POST['boxnumber'],$_POST['oldweight'],$_POST['value'],$_POST['WawiID']);	
					$result = ($data=='success') ? $_POST['value']: $data;
				}else{
					$result = 'invalid';
				}
			}else{
				$result = 'no db';
			}					
			$iWMS->CloseMyConnection();		
			//echo '<pre>'; print_r($_POST); echo '</pre>';
			echo $result;
		break;
		
		/* Get Entry Data Field */
		case 'GetEntryDataFields': 
			//error_reporting(E_ALL);
			//ini_set('display_errors',1);
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$data = $iWMS->GetEntryDataFields($_POST['WawiID']);
			$iWMS->CloseMyConnection(); 
			
			//echo '<pre>';print_r($data);echo '</pre>';
			
			$result = '';
			$script = '';
			
			if ($data != 'no data'){
				
				for ($i=0;$i<count($data);$i++){
					
					$result .= '<li><label style="width:150px;">' . $data[$i]["FieldAlias"] . '<span id="' . $data[$i]["FieldName"] . '-misc"></span></label>';
					
					//0 means unlimited
					$maxLength = ($data[$i]["FieldLimit"]) ? ' maxlength="'.$data[$i]["FieldLimit"].'" ': '';
					$readOnly = ($data[$i]['FieldType']=='datetime') ? ' readonly="readonly" ':'';
					$title = ' title="' . $data[$i]["FieldAlias"] . '" ';
					$first = ($i==0) ? ' first': '';
					$retain = ($data[$i]["RetainInfo"]) ? ' retain': '';
					$required = ($data[$i]["RequiredField"]) ? ' required': '';
					$lockicon = ($data[$i]["RetainInfo"]) ? 'ui-icon-locked': 'ui-icon-unlocked';
					$locktooltip = ($lockicon=='ui-icon-locked') ? 'click to unlock this field': 'click to lock this field';
					
					switch ($data[$i]["ElementType"]){ 
						case 'select':
							$result .= '<select id="'.$data[$i]["FieldName"].'" name="'.$data[$i]["FieldName"].'" class="select '.$data[$i]['FieldType'].$first.$retain.$required.'" '.$title.'>';
							if (isset($data[$i]["Options"]) && count($data[$i]["Options"])){
								/*
								$result .= '<option value="">Select here</option>';	
								for ($j=0;$j<count($data[$i]["Options"]);$j++){
									if(strlen(trim($data[$i]['Options'][$j]['Index'])) > 0)
									$result .= '<option value="'.$data[$i]['Options'][$j]['Index'].'">'.$data[$i]['Options'][$j]['Index'].'</option>';
								}
								*/
								$result .= '<option value="">Select here</option>';	
								for ($j=0;$j<count($data[$i]["Options"]);$j++){
									if(strlen(trim($data[$i]['Options'][$j]['Index'])) > 0)
									{
										switch($_POST['WawiID']){
											
											case 28:
											case 53:
											case 55:
												switch($data[$i]["FieldName"]){
													case 'CountryOfOrigin':
														$result .= '<option value="'.strtoupper($data[$i]['Options'][$j]['Description']).'">'.strtoupper($data[$i]['Options'][$j]['Description']).'</option>';
													break;
													
													default:
														$result .= '<option value="'.$data[$i]['Options'][$j]['Index'].'">'.$data[$i]['Options'][$j]['Index'].'</option>';
													break;
												}
											break;
											
											default:
												$result .= '<option value="'.$data[$i]['Options'][$j]['Index'].'">'.$data[$i]['Options'][$j]['Index'].'</option>';
											break;
										}

										
									}
								}
	
							}else{
								$result .= '<option value="">No Available '.$data[$i]["FieldAlias"].'</option>';
							}
							
							$result .= "</select>";
						break;
						
						case 'textarea':
						case 'input':
							$class = 'class="text '.$data[$i]['FieldType'].$first.$retain.$required.'"';
							$class .= ($data[$i]["FieldType"]=='datetime') ? ' datetime':'';
							$result .= '<input type="' . $data[$i]["ElementProperty"] . '" '.$class.' name="' . $data[$i]["FieldName"] . '" id="' . $data[$i]["FieldName"] . '" '.$maxLength.' '.$readOnly.' '.$title.'/>';
						break;
					}								
					
					/*SCRIPT TO FOCUS ON NEXT FIELD WHEN ENTER KEY IS PRESSED*/
					if ($i<count($data)-1){
						$script .='<script>
						$(function() {
							$("#'.$data[$i]["FieldName"].'").keypress(function(event) {
							  if ( event.which == 13 ) {
								  $("#'.$data[$i+1]["FieldName"].'").focus().select();
								  if ("'.$data[$i]["ElementType"].'"=="select"){
									  event.preventDefault();
								  }
							   }
							});							
						})
						</script>';
					}else{
						$script .='<script>
						$(function() {
							$("#'.$data[$i]["FieldName"].'").keypress(function(event) {
							  if ( event.which == 13 ) {
								  if ("'.$data[$i]["ElementType"].'"=="select"){
									  event.preventDefault();
								  }
								  $("#submit-btn").click();
							  }
							});
						})
						</script>';
					}
					
					$result .= '</li>';
				}
				
			}else{
				$result = 'no data';
			}
			
			echo $result.$script;
			
		break;
		/* Update entry data*/
		case 'UpdateEntryData':

			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->UpdateEntryData($DB[0],$_POST);
			}else{
				$data = 'no db';
			} 
			
			$iWMS->CloseMyConnection();
			echo $data;
			
		break;
		/* Send 3B2 For Marvell */
		case 'Send3B2':
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);			
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->Send3B2($DB[0],$_POST);
			}else{
				$data = 'no db';
			} 			
			$iWMS->CloseMyConnection();
			echo $data;			
		break; 	
		/* Update Yen Value */
		case 'YenValueUpdate':
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);			
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->YenValueUpdate($DB[0],$_POST);
			}else{
				$data = 'no db';
			} 			
			$iWMS->CloseMyConnection();
			echo $data;			
		break; 	
		/* Update Max Temp */
		case 'MaxTempUpdate':
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);			
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->MaxTempUpdate($DB[0],$_POST);
			}else{
				$data = 'no db';
			} 			
			$iWMS->CloseMyConnection();
			echo $data;			
		break; 		
		/* Label Logo Update */
		case 'LabelLogoUpdate':
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);			
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->LabelLogoUpdate($DB[0],$_POST);
			}else{
				$data = 'no db';
			} 			
			$iWMS->CloseMyConnection();
			echo $data;			
		break; 
/* QA Check Order */
		case 'QACheckOrder':
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);			
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->QACheckOrder($DB[0],$_POST);
			}else{
				$data = 'no db';
			} 			
			$iWMS->CloseMyConnection();
			echo $data;			
		break; 
		/* QA Check Inner Label */
		case 'QACheckInnerLabel':
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);			
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->QACheckInnerLabel($DB[0],$_POST);
			}else{
				$data = 'no db';
			} 			
			$iWMS->CloseMyConnection();
			echo $data;			
		break; 
		/* QA Check Outer Label */
		case 'QACheckOuterLabel':
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);			
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->QACheckOuterLabel($DB[0],$_POST);
			}else{
				$data = 'no db';
			} 			
			$iWMS->CloseMyConnection();
			echo $data;			
		break; 
		/* QA Check All Inner Label */
		case 'QACheckAllInnerLabel':
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);			
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->QACheckAllInnerLabel($DB[0],$_POST);
				$data = ($data=='no data' || count($data) < 1) ? 'no data': json_encode($data);
			}else{
				$data = 'no db';
			} 			
			$iWMS->CloseMyConnection();
			echo $data;			
		break; 
		/* QA Check All Outer Label */
		case 'QACheckAllOuterLabel':
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);			
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->QACheckAllOuterLabel($DB[0],$_POST);
				$data = ($data=='no data' || count($data) < 1) ? 'no data': json_encode($data);
			}else{
				$data = 'no db';
			} 			
			$iWMS->CloseMyConnection();
			echo $data;			
		break;
		/* Advance Receiving : Check invoice number */
		case 'CheckInvoiceNo':
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);			
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->CheckInvoiceNo($DB[0],$_POST);
				$data = ($data=='no data' || count($data) < 1) ? 'no data': json_encode($data);
			}else{
				$data = 'no db';
			} 			
			$iWMS->CloseMyConnection();
			echo $data;			
		break; 
		/* Advance Receiving : Add/Edit/Delete Invoice*/
		case 'AdvanceReceivingAction':
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);			
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->AdvanceReceivingAction($DB[0],$_POST);
			}else{
				$data = 'no db';
			} 			
			$iWMS->CloseMyConnection();
			echo $data;			
		break; 
		/* Advance Receiving Detail : Get Details*/
		case 'GetAdvReceivingDetails':
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);			
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->GetAdvReceivingDetails($DB[0],$_POST);
				$data = ($data=='no data' || count($data) < 1) ? 'no data': json_encode($data);
			}else{
				$data = 'no db';
			} 			
			$iWMS->CloseMyConnection();
			echo $data;			
		break; 		
		/* Advance Receiving Detail : Add/Edit/Delete Details*/
		case 'AdvanceReceivingDetailAction':
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);			
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->AdvanceReceivingDetailAction($DB[0],$_POST);
			}else{
				$data = 'no db';
			} 			
			$iWMS->CloseMyConnection();
			echo $data;			
		break; 
		/* Advance Receiving : Get Shippers*/
		case 'GetAdvReceivingShippers':
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);			
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->GetAdvReceivingShippers($DB[0],$_POST);
				$data = ($data=='no data' || count($data) < 1) ? 'no data': json_encode($data);
			}else{
				$data = 'no db';
			} 			
			$iWMS->CloseMyConnection();
			echo $data;			
		break;
		/* Advance Receiving : Job/Permit Allocation */
		case 'UpdateARJobPermit':
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);			
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->UpdateARJobPermit($DB[0],$_POST);
			}else{
				$data = 'no db';
			} 			
			$iWMS->CloseMyConnection();
			echo $data;			
		break;
		/* Labeling : Get Packlist Number By Order Number*/
		case 'GetPacklistByOrderNo':
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);			
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->GetPacklistByOrderNo($DB[0],$_POST);
				$data = ($data=='no data' || count($data) < 1) ? 'no data': json_encode($data);
			}else{
				$data = 'no db';
			} 			
			$iWMS->CloseMyConnection();
			echo $data;			
		break;
		
		/* Labeling : Print request */
		case 'LabelPrintRequest':
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);			
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->LabelPrintRequest($DB[0],$_POST);
			}else{
				$data = 'no db';
			}  			
			$iWMS->CloseMyConnection();
			echo $data;	 		
		break; 
		
		/* Update Part Price */
		case 'PartPriceUpdate':
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);			
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->PartPriceUpdate($DB[0],$_POST);
			}else{
				$data = 'no db';
			} 			
			$iWMS->CloseMyConnection();
			echo $data;			
		break; 
				
		/* Save custom scanout */
		case 'CustomScanout':
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);

			if ($DB!='no data' && count($DB)){
				$data = $iWMS->CustomScanout($DB[0]['DBName'],$_POST);
				$data = is_array($data) ? json_encode($data) : $data; 
			}else{
				$data = 'no db';
			}
			
			$iWMS->CloseMyConnection();
			echo $data;
		
		break;
		
		/*for new scanout 2012-06-01*/
		case 'UpdateScanOutBoxDataTable':
			switch ($_POST['field']){
				case 'mEditWeight':
					if (!is_numeric($_POST['value']) || $_POST['value']<=0){
						echo 'invalid';
					}else{
						echo $_POST['value'];
					}
				break;
				
				case 'mEditDtype':
					echo $_POST['value'];
				break;
				
				case 'mEditQty':
					if (!is_numeric($_POST['value']) || $_POST['value']<=0){
						echo 'invalid';
					}else{
						echo $_POST['value'];
					}
				break;
				
				default:
					echo '';
				break;
			}
		break;
		
		/*for new scanout 2012-06-03*/
		case 'GetScanOutStatus':
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			
			if ($DB!='no data' && count($DB)){
				
				switch ($_POST['category']){
					case 1:	//
					break;
					
					case 2:	//
					break;
					
					case 3:
					
						if (trim($_POST['ordernumber'])=="" || (is_numeric(trim($_POST['ordernumber'])) && trim($_POST['ordernumber'])>0)){
							$data = $iWMS->GetScanOutStatus($DB[0]['DBName'],$_POST);

							if (is_array($data)){
								$data = json_encode($data);
							}
							
						}else{
							$data = 'Invalid Order Number';
						}
					break;
					
					default:
						$data='invalid request';
					break;
				}
				
				
			}else{
				$data = 'no db';
			}
	
			$iWMS->CloseMyConnection();
			echo $data;
		break;
		
		/*for new scanout 2012-06-01*/	
		case 'RecordScanOut':
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			
			if ($DB!='no data' && count($DB)){
				
				switch ($_POST['category']){
					case 1:	break;
					
					case 2:	break;
					case 3:
						if (trim($_POST['ordernumber'])=="" || (is_numeric(trim($_POST['ordernumber'])) && trim($_POST['ordernumber'])>0)){
							
							$_POST['FetchData']=0;
							
							$data = $iWMS->GetScanOutStatus($DB[0]['DBName'],$_POST);

							switch ($data[0]['result']){
								case 'new':
								case 'partial':	
								case 'complete':
									for($i=0;$i<count($_POST['sInner']); $i++){
										$temp = explode('|',$_POST['sInner'][$i]);
										$innerArray[$i]['sn'] = $temp[0];
										$innerArray[$i]['entrynumber'] = $temp[1];
										$innerArray[$i]['quantity'] = $temp[2];
										$innerArray[$i]['boxnumber'] = $temp[3];
										for ($j=0;$j<count($innerArray); $j++){
											if ($innerArray[$j]['boxnumber']==$boxArray[$i]['boxnumber']){
												$innerArray[$j]['carton']=($i+1);
											}
										}
									}
									
									for($i=0,$j=0;$i<count($_POST['sBox']); $i++){
										$temp = explode('|',$_POST['sBox'][$i]);
										$boxArray[$i]['boxnumber'] = $temp[0];
										$boxArray[$i]['weight'] = $temp[1];
										$boxArray[$i]['dimensiontype'] = $temp[2];	
										
										for ($j=0;$j<count($innerArray); $j++){
											if ($innerArray[$j]['boxnumber']==$boxArray[$i]['boxnumber']){
												$innerArray[$j]['carton']=($i+1);
											}
										}
									}
									
									$data = $iWMS->RecordScanOut($DB[0]['DBName'],$_POST['ordernumber'],$_POST['category'],$boxArray,$innerArray,$_SESSION[LoginUserVar],$_POST['netweight'],$_POST['WawiID']);
			
									$result = (is_array($data)) ? json_encode($data): $data;

								break;
								
								case 'no booking found':
								case 'order number does not exist':
									$result = $data[0]['result'];
								break;
								
								default:
									$result = 'invalid request';
								break;
							}
							
						}else{
							$result = 'Invalid Order Number';
						}
					break;
					
					default:
						$result='invalid request';
					break;
				}
			}else{
				$result = 'no db';
			}
	
			$iWMS->CloseMyConnection();
			echo $result;
		break;
		
		/*for new scanout 2012-06-01*/
		case 'UndoScanOut':
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			
			if ($DB!='no data' && count($DB)){
				
				$result = $iWMS->UndoScanOut($DB[0]['DBName'],$_POST['ordernumber'],$_SESSION[LoginUserVar]);
				
			}else{
				$result = 'no db';
			}
	
			$iWMS->CloseMyConnection();
			echo $result;
		break;
		
		case 'CheckBooking':
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_POST['WawiID']);
			
			if ($DB!='no data' && count($DB)){
				$data = $iWMS->CheckBooking($DB[0]['DBName'],$_POST['ordernumber'],$_POST['entrynumber'],$_POST['quantity'],$_SESSION[LoginUserVar]);
				$result = is_array($data) ? json_encode($data) : $data; 
			}else{
				$result = 'no db';
			}
	
			$iWMS->CloseMyConnection();
			echo $result;
		break;
				
	}
}else if(isset($_GET['type']) && $_GET['type']){
	switch($_GET['type']){
	/* Flight Detail : Upload File  */
		case 'FlightDetailUploadFile' :
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
			$DB = $iWMS->GetWawiCustomer($_GET['WawiID']);
			if ($DB!='no data' && count($DB)){
				$entry = $iWMS->FlightDetailUploadFile($DB[0]['DBName'],array('xlsx','xls'),basename($_GET['qqfile']),'modules/flightdetail/uploads/');
				$result = ($entry=='no data' || count($entry) < 1) ? 'no data': json_encode($entry);
			}else{
				$result = 'no data';
			}
			
			$iWMS->CloseMyConnection();
			
			echo $result;
			
		break;
	}
	
}else{
	session_destroy();
	include(ROOT.'/404.php');
	die();
}

?>
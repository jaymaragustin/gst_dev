<?php 
$web_url=WEB_URL;

if ($web_url=='WEB_URL' || $web_url='' || $web_url=NULL){
	include('include/config.php');
	$web_url=WEB_URL;
}

?>
<h2><a href="<?php echo $web_url;?>">Invalid Request</a></h2>
<p>We are sorry but your request is invalid.</p>
<p>You can return to the homepage by clicking <a href="<?php echo $web_url;?>">here</a></p>
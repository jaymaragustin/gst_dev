// JavaScript Document
$(function() {

	$('.menu a').click(function() {//alert($(this).attr('href'));
		if ($(this).attr('href')!='#'){
			loadContent(this.href);
			return false;
		}
	});
	
	
	function loadContent(href){ 
		$.ajax({
			 type: 'POST',
			 url: href,
			 success: function(data){
				var content = data;
				
				if (content=='logout'){
					$(window.location).attr('href', 'index.php');
					return false;
				}
				
				$(".content").html(content);
			 },
			 beforeSend: function(){
				Dim(1);
			   },
			 complete: function(){
				Dim(0);
			   }
		 });
	 }	
	 
	 function Dim(value){
		 if (value==1)
			 $.dimScreen(200, 0.2, function() {
				//$('.content').fadeIn();
			});
		 else{
			 $.dimScreenStop();
		 }
	 } 
	 //loadContent('home.php');	
	
	$.fn.clearForm = function() {
      return this.each(function() {
        var type = this.type, tag = this.tagName.toLowerCase();
        if (tag == 'form')
          return $(':input',this).clearForm();
        if (type == 'text' || type == 'password' || tag == 'textarea')
          this.value = '';
        else if (type == 'checkbox' || type == 'radio')
          this.checked = false;
        else if (tag == 'select')
          this.selectedIndex = 0;
      });
    };
	
	// jQuery.fn.DecimalMask = function () {
	  // return this.each(function () {
		// $(this).keypress(function (e) {
		  // var keynum;
		  // if (window.event) // IE
		  // {
			// keynum = e.keyCode;
		  // }
		  // else if (e.which) // Netscape/Firefox/Opera
		  // {
			// keynum = e.which;
		  // }
		  // if (typeof keynum == 'undefined') return true;
		  // else if (keynum == 46 && $(this).val().indexOf(".") > -1) return false; //already has a decimal point
		  // else return ((keynum > 47 && keynum < 58) || keynum == 13 || keynum == 8 || keynum == 9 || keynum == 46 || keynum == 45); //allow ony number keys or keypad number keys
		// });
	  // });
	// };
	jQuery.fn.DecimalMask = function (mask){
		
		if (!mask || !mask.match){
		  mask = 9999999.99999; // default mask
		}

		var
		  v,
		  is = (function(){v = mask.match(/[0-9]{1,}/); return v!== null ? v[0].length : 0})(),
		  ds = (function(){v = mask.match(/[0-9]{1,}$/); return v !== null ? v[0].length : 0})(),
		  sep = (function(){v = mask.match(/,|\./); return v !== null ? v[0] : null})(),
		  matcher = null,
		  tester = null,
		  events = /.*MSIE 8.*|.*MSIE 7.*|.*MSIE 6.*|.*MSIE 5.*/.test(navigator.userAgent) ? 'keyup propertychange paste' : 'input paste';
		
		if (sep === null){
		  tester = new RegExp('^[0-9]{0,'+is+'}$');
		  matcher = new RegExp('[0-9]{0,'+is+'}','g');
		}else{
		  tester = new RegExp('^[0-9]{0,'+is+'}'+(sep === '.' ? '\\.' : ',')+'[0-9]{0,'+ds+'}$|^[0-9]{0,'+is+'}'+(sep === '.' ? '\\.' : ',')+'$|^[0-9]{0,'+is+'}$');
		  matcher = new RegExp('[0-9]{0,'+is+'}'+(sep === '.' ? '\\.' : ',')+'[0-9]{0,'+ds+'}|[0-9]{0,'+is+'}'+(sep === '.' ? '\\.' : ',')+'|[0-9]{0,'+is+'}','g');
		}
			
		function handler(e){
		  var self = $(e.currentTarget);
		  if (self.val() !== e.data.ov){
			if (!tester.test(self.val())){
			  var r = self.val().match(matcher);
			  self.val(r === null ? '' : r[0]);
			}
			ov = e.data.ov = self.val();
		  }
		}
		
		$(this)
		  .attr('maxlength', (is + ds + (sep === null ? 0 : 1)))
		  .val($(this).val().replace('.',sep))
		  .bind(events,{ov:$(this).val()},handler);
	}
		
	jQuery.fn.IntegerMask = function () {
	  return this.each(function () {
		$(this).keypress(function (e) {
		  var keynum;
		  if (window.event) // IE
		  {
			keynum = e.keyCode;
		  }
		  else if (e.which) // Netscape/Firefox/Opera
		  {
			keynum = e.which;
		  }
		  if (typeof keynum == 'undefined') return true;
		  else return ((keynum > 47 && keynum < 58) || keynum == 13 || keynum == 8 || keynum == 9 || keynum == 45);
		});
	  });
	};
	
	$(".int").keydown(function(event) {
		
		// Allow only backspace, delete, enter, tab
		if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 13 || event.keyCode == 9 ) {
			//
		}else {
			// Ensure that it is a number and stop the keypress
			if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
				event.preventDefault(); 
			}   
		}
	});	
	
});



<?php
session_start(); 
$iWMS = new iWMS($_SESSION[LoginUserVar]);
//$_SESSION['ProgramList'] = array(array('ID'=>'1','ProgramName'=>'GST'),array('ID'=>'2','ProgramName'=>'Billing'));
$_SESSION['ProgramList'] = $iWMS->GetProgramList($_SESSION[LoginUserVar]);
?>
<div class="menu">
    <div id="templatemo_menu" class="ddsmoothmenu">
        <ul>
            <li class="left-curve"><a href="#">&nbsp;</a></li>
            <li class="firstmenu"><a id="menu_item_1" href="home.php">Home</a></li>
            <?php
			
            if (!isset($_SESSION[LoginUserVar]) || !$_SESSION[LoginUserVar] || !isset($_SESSION[LoginSession]) || $_SESSION[LoginSession]!=LoginSessionValue ){
                echo '';
            }else{
				
				if (count($_SESSION['ProgramList'])){
					echo '<li><a id="menu_item_2" href="modules/programs.php">Programs</a><ul>';
					for ($i=0;$i<count($_SESSION['ProgramList']);$i++){
						echo '<li><a href="modules/modules.php?ID='.$_SESSION['ProgramList'][$i]['ID'].'&ProgramName='.$_SESSION['ProgramList'][$i]['ProgramName'].'">'.$_SESSION['ProgramList'][$i]['ProgramName'].'</a></li>';
					}
					echo '</ul></li>';
				} 
				
				echo '
				<li class="lastmenu"><a id="menu_item_3" href="modules/support.php">Support</a></li>  
                ';
            }
            ?> 
            <li>
                <?php 
                if (!isset($_SESSION[LoginUserVar]) || !$_SESSION[LoginUserVar] || !isset($_SESSION[LoginSession]) || $_SESSION[LoginSession]!=LoginSessionValue ){ 
                    echo '<a id="menu_item_4" href="modules/login.php">Login</a>';
                }else{
                    echo '<a id="menu_item_5" href="modules/logout.php">Logout</a>';
                }
                ?>
                
            
            </li>
        </ul>
        <br style="clear: left" />
    </div> <!-- end of templatemo_menu -->
</div>
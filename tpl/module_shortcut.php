<?php 
if (isset($_SESSION['defaultProgramName']) && $_SESSION['defaultProgramName'] && isset($_SESSION['defaultProgramID']) && $_SESSION['defaultProgramID']){
	echo '<a style="text-decoration:none; color:#417684;" href="modules/programs.php" id="customerlist">Programs</a> &raquo; ';
	echo $_SESSION['defaultProgramName'].' : ';
	echo '<a style="text-decoration:none; color:#417684;" href="modules/modules.php?ID='.$_SESSION['defaultProgramID'].'&ProgramName='.$_SESSION['defaultProgramName'].'" class="back_to_modules">Modules</a> &raquo; ';

	echo '<a style="float: right;" class="back_to_modules link" href="modules/modules.php?ID='.$_SESSION['defaultProgramID'].'&ProgramName='.$_SESSION['defaultProgramName'].'" title="Back to modules">Back to Modules</a> ';
}
?>
<script>
$(function() {
	function loadContent(href){ 
		$.ajax({
			 type: 'POST',
			 url: href,
			 success: function(data){
				var content = data;
				$(".content").html(content);
			 },
			 beforeSend: function(){
				Dim(1);
			   },
			 complete: function(){
				Dim(0);
			   }
		 });
	 }	
	 
	 $('.back-btn').button();
	 
	 function Dim(value){
		 if (value==1)
			 $.dimScreen(1000, 0.5, function() {
			});
		 else{
			 $.dimScreenStop();
		 }
	 } 
	
	$("a.back_to_modules").click(function(){
		if ($(this).attr('href')!='#'){
			loadContent(this.href);
			$("#back_link").remove('a.back_to_modules');
			return false;
		}
	});
	
	$("#customerlist").click(function(){
		if ($(this).attr('href')!='#'){
			loadContent(this.href);
			$("#back_link").html('');
			return false;
		}
	});

	if($('#back_link a.back_to_modules').length == 0)
		$("a.back_to_modules.link").appendTo("#back_link");
	else
		$("h2 a.back_to_modules.link").remove();
	
});
</script>
<div class="header">
    <div class="header_resize">
		<?php include('logo.php');?>
        <?php include('menu.php');?>
		<?php if(isset($_SESSION[LoginUserVar])){ 
			$iWMS = new iWMS($_SESSION[LoginUserVar]);
		?>
			<h3 class="header-subtitle">Working as <a href="#" id="userName" ><?php echo $iWMS->getUserName($_SESSION[LoginUserVar]);?></a>!</h3>
            <h3 id="back_link" style="float:right"></h3>
		<?php }?>
        <div class="clr"></div>
    </div>
    <div class="headert_text_resize">
        <div class="headert_text"></div>
        <div class="clr"></div>
    </div> 
</div>
<script>
$(function(){
	$('#userName').click(function(){
		$('.content').load('modules/user.php',{username:'<?php echo $_SESSION[LoginUserVar];?>'});	
	});
	
	$('.menu #templatemo_menu a, #userName').click(function(){
		$('#back_link').html('');
	});
});
</script>
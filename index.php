<?php 
// error_reporting(E_ALL);
// ini_set('display_errors',1);   
if (function_exists('ob_gzhandler') && !ini_get('zlib.output_compression')){
	ob_start('ob_gzhandler');
}else{
	ob_start();
}
session_start();
require('include/include.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>JSI GST Online</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link href="images/gst.png" rel="apple-touch-icon" />
<link rel="shortcut icon" href="images/gst.png" />

<?php
for ($i=0; $i<count($CSS);$i++){ echo '<link rel="stylesheet" href="'.$CSS[$i].'">';}
require('include/js.php'); 
?>

 
</head>
<body height="100%" width="100%" >
<div class="main">
  <?php 
  if(isset($_SESSION[LoginUserVar]) && isset($_SESSION['ProgramList'])){
  	$_SESSION['ProgramList'] = NULL;
	$iWMS = new iWMS($_SESSION[LoginUserVar]);
  	$_SESSION['ProgramList'] = $iWMS->GetProgramList($_SESSION[LoginUserVar]);
  }else{ 
	session_destroy();
  }
  include('tpl/header.php');
  include('tpl/body.php');
  include('tpl/footer.php');
  include('tpl/dialog-msg.php');
  ?>
</div>
</body>
</html>
<script>
$(function() {
	function loadContent(href){ 
		$.ajax({
			 type: 'POST',
			 url: href,
			 success: function(data){
				var content = data;
				
				if (content=='logout'){
					$(window.location).attr('href', 'index.php');
					return false;
				}
				
				$(".content").html(content);
			 },
			 beforeSend: function(){
				Dim(1);
			   },
			 complete: function(){
				Dim(0);
			   }
		 });
	 }	
	 function Dim(value){
		 if (value==1)
			 $.dimScreen(300, 0.5, function() {
				//$('.content').fadeIn();
			});
		 else{
			 $.dimScreenStop();
		 }
	 } 
<?php 
if (isset($_SESSION[LoginUserVar]) && $_SESSION[LoginUserVar] && isset($_SESSION['ProgramList']) && $_SESSION['ProgramList']){
	echo "loadContent('modules/programs.php');";
}else{
	echo "loadContent('modules/login.php');";
}
?>
});
</script>
<?php	//var_dump($_SESSION['ProgramList']); ?>
